<?php

class HomeAgentController extends BaseController {

    public function showHomeAgent() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertCompanyUser') {
                return $this->insertCompanyUser();
            }
        }
        return View::make('agent.homeAgent')
                        ->withToogle('agent')->withAktif('homeAgent');
    }

    public function showContactAgent() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            return $this->contactUs();
        }
        return View::make('agent-contact')->withAktif('none')->withToogle('none');
    }

    public function showProfileAgent() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get("jenis") == "updatePassword") {
                return $this->updatePasswordAgent();
            } else if (Input::get("jenis") == "updateProfile") {
                return $this->updateProfileAgent();
            }
        }
        return View::make('agent.profileAgent')->withAktif('profileAgent')->withToogle('agent');
    }

    public function showCommissionAgent() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get("jenis") == "reportWithdraw") {
                return $this->reportWithdraw();
            }
        }
        return View::make('agent.commissionAgent')->withAktif('commissionAgent')->withToogle('agent');
    }

    public function showWithdrawAgent() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get("jenis") == "submitWithdraw") {
                return $this->submitWithdraw();
            }
        }
        return View::make('agent.withdrawAgent')->withAktif('withdrawAgent')->withToogle('agent');
    }

    public function submitWithdraw() {
        $rule = array(
            "WithdrawNominal" => "required|numeric"
        );
        $data = Input::all();

        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            return View::make('agent.withdrawAgent')
                            ->withToogle('agent')->withAktif('withdrawAgent')
                            ->withMessages('gagalInsert')
                            ->withError($validator->messages());
        } else {
            $saldo = $this->getSaldoWithdraw();
            if (Input::get("WithdrawNominal") > $saldo) {
                return View::make('agent.withdrawAgent')
                                ->withToogle('agent')->withAktif('withdrawAgent')
                                ->withMessages('gagalWithdraw');
            } else {
                $agent = User::find(Input::get("AgentInternalID"));
                $year = substr(date("Y"), 2, 2);
                $month = str_pad(date("m"), 2, '0', STR_PAD_LEFT);
                $date = $agent->UserID . "-" . $month . $year . '-';
                $withdrawId = Withdraw::getNextIDWithDraw($date, $agent->InternalID);

                $withdraw = new Withdraw();
                $withdraw->AgentInternalID = $agent->InternalID;
                $withdraw->WithdrawDate = date("Y-m-d");
                $withdraw->WithdrawID = $withdrawId;
                $withdraw->WithdrawNominal = Input::get("WithdrawNominal");
                $withdraw->Status = 0;
                $withdraw->UserRecord = Auth::user()->UserName;
                $withdraw->UserModified = 0;
                $withdraw->Remark = "-";
                $withdraw->save();

                $data["UserID"] = $agent->UserID;
                $data["UserName"] = $agent->UserName;
                $data["Email"] = $agent->Email;
                $data["Phone"] = $agent->Phone;
                $data["AccountNumber"] = $agent->AccountNumber;
                $data["AccountName"] = $agent->AccountName;
                $data["BankAccount"] = $agent->BankAccount;

                $data["WithdrawDate"] = date('d M Y', strtotime($withdraw->WithdrawDate));
                $data["WithdrawID"] = $withdraw->WithdrawID;
                $data["WithdrawNominal"] = number_format($withdraw->WithdrawNominal, 2, '.', ',');

                Mail::send('emails.emailWithdrawToSU', $data, function($message) {
                    $message->to("salmon.support@salmonacc.com", "Salmon Accounting")->subject('Withdraw Information');
                });

                return View::make('agent.withdrawAgent')
                                ->withMessages('suksesInsertWithdraw')
                                ->withToogle('agent')->withAktif('withdrawAgent');
            }
        }
    }

    public function contactUs() {
        //rule
        $rule = array(
            'Subject' => 'required',
            'message' => 'required'
        );

        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            return View::make('agent-contact')
                            ->withAktif('none')
                            ->withToogle('none')
                            ->withMessages('gagalContact')
                            ->withError($validator->messages());
        } else {
            $user = User::find(Auth::user()->InternalID);
            $data = [];
            $data["userName"] = $user->UserName;
            $data["email"] = $user->Email;
            $data["subject"] = Input::get("Subject");
            $data["pesan"] = Input::get("message");

            Mail::send('emails.feedbackUser', $data, function($message) {
                $message->to("support@salmonacc.com", "Salmon Support Team")->subject(Input::get("Subject"));
            });

            return View::make('agent-contact')->withAktif('none')->withToogle('none')->withMessages('suksesContact');
        }
    }

//========================Ajax===================================
    public function getDataHistory() {
        $payment = Payment::where("CompanyInternalID", Input::get("id"))->OrderBy("PaymentDate", 'desc')->get();
        $count = 1;
        foreach ($payment as $value) {
            ?>
            <tr>
                <td><?php
                    echo $count;
                    $count++;
                    ?></td>
                <td><?php echo date('d M Y', strtotime($value->PaymentDate)); ?></td>
                <td><?php echo $value->PaymentID; ?></td>
            </tr>
            <?php
        }
    }

    public function getSaldoWithdraw() {
        $harga = 0;
        $totalKomisi = 0;
        $totalWithdraw = 0;
        $total = 0;
        foreach (Payment::join("m_company", "m_company.InternalID", "=", "CompanyInternalID")->where("m_company.AgentInternalID", Auth::user()->InternalID)->get() as $dataWithdraw) {
            if ($dataWithdraw->PackageInternalID == 1) {
                $harga = 350000;
            } else {
                $harga = 1000000;
            }
            $totalKomisi += $harga * ($dataWithdraw->AgentCommission / 100);
        }
        foreach (Withdraw::where("AgentInternalID", Auth::user()->InternalID)->get() as $withdraw) {
            $totalWithdraw += $withdraw->WithdrawNominal;
        }
        $total = $totalKomisi - $totalWithdraw;
        return $total;
    }

//=======================/Ajax===================================
    public function insertCompanyUser() {
        //rule
        $rule = array(
            'Package' => 'required',
            'CompanyID' => 'required|max:200|unique:m_company,CompanyID',
            'CompanyName' => 'required|max:200',
            'Address' => 'max:1000',
            'City' => 'max:1000',
            'Phone' => 'max:200',
            'Fax' => 'max:200',
            'EmailCompany' => 'required|email|max:200|unique:m_company,Email',
            'Logo' => 'image',
            'UserID' => 'required|max:16|unique:m_user,UserID',
            'Name' => 'required|max:200',
            'Password' => 'required|max:16',
            'PhoneNumber' => 'required|max:200',
            'Email' => 'required|email|max:200|unique:m_user,Email',
            'Region' => 'required'
        );
        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('agent.homeAgent')
                            ->withToogle('agent')->withAktif('homeAgent')
                            ->withMessages('gagalInsert')
                            ->withError($validator->messages());
        } else {
            $path = NULL;
            $input = Input::file('Logo');
            if ($input != '') {
                if (Input::hasFile('Logo') && Input::file('Logo')->getSize() <= 2000000) {
                    $destination = base_path() . '/uploaded_img/';
                    $filename = str_random(6) . '_' . date('d') . date('m') . date('y') . '_' . Input::file('Logo')->getClientOriginalName();
                    Input::file('Logo')->move($destination, $filename);
                    $height = Image::make(sprintf($destination . '%s', $filename))->height();
                    $width = Image::make(sprintf($destination . '%s', $filename))->width();
                    $heightResize = null;
                    $widthResize = null;
                    $countResize = 0;
                    if ($height > 200) {
                        $heightResize = 200;
                        $countResize = 1;
                    }
                    if ($width > 705) {
                        $widthResize = 705;
                        $countResize = 1;
                    }
                    if ($countResize == 1) {
                        $image = Image::make(sprintf($destination . '%s', $filename))->resize($widthResize, $heightResize)->save();
                    }
                    $path = '/uploaded_img/' . $filename;
                } else {
                    return View::make('agent.homeAgent')
                                    ->withToogle('agent')->withAktif('homeAgent')
                                    ->withMessages('gagalUpload');
                }
            }
            //valid
            $halaman = Input::get("Package");
            if ($halaman == 'Starter') {
                $package = 1;
            } else {
                $package = 3;
            }
            $company = new Company;
            $company->CompanyID = Input::get('CompanyID');
            $company->CompanyName = Input::get('CompanyName');
            $company->Address = Input::get('Address');
            $company->City = Input::get('City');
            $company->TaxID = Input::get('taxID');
            $company->Phone = Input::get('Phone');
            $company->Fax = Input::get('Fax');
            $company->Email = Input::get('EmailCompany');
            $company->RegionInternalID = Input::get('Region');
            if (Input::get('Region') == -1) {
                $company->RegionInternalID = NULL;
            }
            $company->Logo = $path;
            $company->Status = 1;
            $company->PackageInternalID = $package;
            $company->AgentInternalID = Auth::user()->InternalID;
            $company->StatusPayment = 0;
            $company->UserRecord = '-';
            $company->UserModified = '0';
            $company->Remark = '-';
            $company->save();

            $user = new User;
            $user->UserID = Input::get('UserID');
            $user->UserName = Input::get('Name');
            $user->Phone = Input::get('PhoneNumber');
            $user->Email = Input::get('Email');
            $user->UserPwd = Hash::make(Input::get('Password'));
            $user->CompanyInternalID = $company->InternalID;
            $user->Picture = NULL;
            $user->Status = 0;
            $user->UserRecord = "-";
            $user->UserModified = "0";
            $user->Remark = "-";
            $user->save();
            $this->activationEmail($user->InternalID);
            $this->sendInvoicePayment($company->InternalID, $halaman);
            if (Input::get('Region') != -1) {
                $currency = new Currency;
                $currency->CurrencyID = Region::find(Input::get('Region'))->Currency->CurrencyID;
                $currency->CurrencyName = Region::find(Input::get('Region'))->Currency->CurrencyName;
                $currency->Default = '1';
                $currency->Rate = '1';
                $currency->UserRecord = $user->UserName;
                $currency->CompanyInternalID = $company->InternalID;
                $currency->UserModified = "0";
                $currency->Remark = '-';
                $currency->save();
            }
            
            $array = explode(',', Package::find($package)->MatrixNotIn);

            foreach (Matrix::whereNotIn('MatrixID', $array)->get() as $data) {
                $userDetail = new UserDetail();
                $userDetail->UserInternalID = $user->InternalID;
                $userDetail->MatrixInternalID = $data->InternalID;
                $userDetail->save();
            }
            return View::make('agent.homeAgent')
                            ->withToogle('agent')->withAktif('homeAgent')
                            ->withMessages('suksesInsert')
                            ->withCongratulation('yes');
        }
    }

    public function activationEmail($UserInternalID) {
        $data = [];
        $data["userID"] = Input::get('UserID');
        $data["companyName"] = Input::get('CompanyName');
        $data["companyEmail"] = Input::get('EmailCompany');
        $data["userInternalID"] = $UserInternalID;

        $plainText = $data["userID"] . '---;---' . $data["userInternalID"];
        $enkrip = Hash::make($plainText);
        $enkrip = myEncryptEmail($enkrip);
        $data["link"] = "http://application.salmonacc.com/checkActivationAccount/" . $enkrip;

        Mail::send('emails.email', $data, function($message) {
            $message->to(Input::get('EmailCompany'), Input::get('CompanyName'))->subject('Activation Account');
        });
    }

    public function sendInvoicePayment($CompanyInternalID, $halaman) {
        $data = [];
        $data["companyID"] = Input::get('CompanyID');
        $data["companyName"] = Input::get('CompanyName');
        $data["address"] = Input::get('Address');
        $year = substr(date("Y"), 2, 2);
        $month = str_pad(date("m"), 2, '0', STR_PAD_LEFT);
        $date = $data["companyID"] . "-" . $month . $year . '-';

        $data["invoiceID"] = Invoice::getNextIDInvoice($date, $data["companyID"]);
        $data["companyEmail"] = Input::get('EmailCompany');

        //insert invoice id
        $invoice = new Invoice();
        $invoice->CompanyInternalID = $CompanyInternalID;
        $invoice->InvoiceDate = date("Y-m-d");
        $invoice->InvoiceID = $data["invoiceID"];
        $invoice->Status = 0;
        $invoice->UserRecord = "Server";
        $invoice->UserModified = 0;
        $invoice->Remark = "-";
        $invoice->save();

        if ($halaman == 'Starter') {
            $data["paket"] = "Starter";
            $data["paketHarga"] = "350.000";
            $data["paketKuota"] = "1Gb";
        } elseif ($halaman == 'Premium') {
            $data["paket"] = "Premium";
            $data["paketHarga"] = "500.000";
            $data["paketKuota"] = "2Gb";
        } else {
            $data["paket"] = "Enterprise";
            $data["paketHarga"] = "1.000.000";
            $data["paketKuota"] = "5Gb";
        }

        Mail::send('emails.invoicePayment', $data, function($message) {
            $message->to(Input::get('EmailCompany'), Input::get('CompanyName'))->subject('Invoice Payment');
        });
    }

    public function updateProfileAgent() {
        //rule
        $rule = array(
            'Name' => 'required|max:200',
            'Phone' => 'required|max:200',
            'AccountName' => 'required|max:200',
            'AccountNumber' => 'required|max:200',
            'BankAccount' => 'required|max:200',
            'ProfilePicture' => 'image'
        );
        //cek Email sama enggak
        $emailLama = User::find(Auth::user()->InternalID)->Email;
        $data = Input::all();
        if (strtoupper(Input::get('Email')) == strtoupper($emailLama)) {
            $rule2 = array(
                'Email' => 'required|max:200|email'
            );
            $rule = array_merge($rule, $rule2);
        } else {
            $rule2 = array(
                'Email' => 'required|max:200|email|unique:m_user,Email'
            );
            $rule = array_merge($rule, $rule2);
        }

        //validasi
        $validator = Validator::make(Input::all(), $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('agent.profileAgent')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('agent')->withAktif('profileAgent');
        } else {
            $path = NULL;
            $input = Input::file('ProfilePicture');
            if ($input != '') {
                if (Input::hasFile('ProfilePicture') && Input::file('ProfilePicture')->getSize() <= 2000000) {
                    $destination = base_path() . '/uploaded_img/';
                    $filename = str_random(6) . '_' . date('d') . date('m') . date('y') . '_' . Input::file('ProfilePicture')->getClientOriginalName();
                    Input::file('ProfilePicture')->move($destination, $filename);
                    $height = Image::make(sprintf($destination . '%s', $filename))->height();
                    $width = Image::make(sprintf($destination . '%s', $filename))->width();
                    $heightResize = null;
                    $widthResize = null;
                    $countResize = 0;
                    if ($height > 180) {
                        $heightResize = 180;
                        $countResize = 1;
                    }
                    if ($width > 160) {
                        $widthResize = 160;
                        $countResize = 1;
                    }
                    if ($countResize == 1) {
                        $image = Image::make(sprintf($destination . '%s', $filename))->resize($widthResize, $heightResize)->save();
                    }
                    $path = '/uploaded_img/' . $filename;
                } else {
                    return View::make('agent.profileAgent')
                                    ->withMessages('gagalUpload')
                                    ->withToogle('agent')->withAktif('profileAgent');
                }
            }
            //valid
            $user = User::find(Auth::user()->InternalID);
            if ($user->CompanyInternalID == Auth::user()->Company->InternalID) {
                $user->UserName = Input::get('Name');
                if (Auth::user()->Company->InternalID == '-1') {
                    $user->CompanyInternalID = Input::get('company');
                } else {
                    $user->CompanyInternalID = Auth::user()->Company->InternalID;
                }
                $user->Phone = Input::get('Phone');
                $user->AccountName = Input::get('AccountName');
                $user->AccountNumber = Input::get('AccountNumber');
                $user->BankAccount = Input::get('BankAccount');
                $user->Email = Input::get('Email');
                $user->UserModified = Auth::user()->UserID;
                if ($path != NULL) {
                    if ($user->Picture != NULL) {
                        $destination = public_path();
                        File::delete(sprintf($destination . '%s', $user->Picture));
                    }
                    $user->Picture = $path;
                }
                $user->Remark = Input::get('remark');
                $user->save();
                $messages = 'suksesUpdate';
                Session::flash('messages', $messages);
                return Redirect::Route('showProfileAgent');
            } else {
                return View::make('agent.profileAgent')
                                ->withMessages('accessDenied')
                                ->withToogle('agent')->withAktif('profileAgent');
            }
        }
    }

    public function updatePasswordAgent() {
        //rule
        $rule = array(
            'Password' => 'required|max:16'
        );
        //validasi
        $validator = Validator::make(Input::all(), $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('agent.profileAgent')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('agent')->withAktif('profileAgent');
        } else {
            if (!Hash::check(Input::get('PasswordLama'), Auth::user()->UserPwd)) {
                return View::make('agent.profileAgent')
                                ->withMessages('gagalPassword')
                                ->withError($validator->messages())
                                ->withToogle('agent')->withAktif('profileAgent');
            }
            $path = NULL;
            //valid
            $user = User::find(Auth::user()->InternalID);
            if ($user->CompanyInternalID == Auth::user()->Company->InternalID) {
                $user->UserPwd = Hash::make(Input::get('Password'));
                $user->save();
                $messages = 'suksesUpdatePassword';
                Session::flash('messages', $messages);
                return Redirect::Route('showProfileAgent');
            } else {
                return View::make('agent.profileAgent')
                                ->withMessages('accessDenied')
                                ->withToogle('agent')->withAktif('profileAgent');
            }
        }
    }

    public function exportCompanyAgent() {
        Excel::create('Company_agent', function($excel) {
            $excel->sheet('Company_agent', function($sheet) {
                $sheet->mergeCells('B1:M1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Company");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Company Name");
                $sheet->setCellValueByColumnAndRow(3, 2, "Company ID");
                $sheet->setCellValueByColumnAndRow(4, 2, "Address");
                $sheet->setCellValueByColumnAndRow(5, 2, "City");
                $sheet->setCellValueByColumnAndRow(6, 2, "Region");
                $sheet->setCellValueByColumnAndRow(7, 2, "TaxID");
                $sheet->setCellValueByColumnAndRow(8, 2, "Phone");
                $sheet->setCellValueByColumnAndRow(9, 2, "Fax");
                $sheet->setCellValueByColumnAndRow(10, 2, "Record");
                $sheet->setCellValueByColumnAndRow(11, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(12, 2, "Remark");
                $row = 3;
                foreach (Company::where("AgentInternalID", Auth::user()->InternalID)->get() as $data) {
                    if ($data->RegionInternalID == null) {
                        $region = "Not Register";
                    } else {
                        $region = $data->Region->RegionName;
                    }
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, $data->CompanyName);
                    $sheet->setCellValueByColumnAndRow(3, $row, "`" . $data->CompanyID);
                    $sheet->setCellValueByColumnAndRow(4, $row, $data->Address);
                    $sheet->setCellValueByColumnAndRow(5, $row, $data->City);
                    $sheet->setCellValueByColumnAndRow(6, $row, $region);
                    if ($data->TaxID == '...-.') {
                        $sheet->setCellValueByColumnAndRow(7, $row, '');
                    } else {
                        $sheet->setCellValueByColumnAndRow(7, $row, $data->TaxID);
                    }
                    $sheet->setCellValueByColumnAndRow(8, $row, '`' . $data->Phone);
                    $sheet->setCellValueByColumnAndRow(9, $row, '`' . $data->Fax);
                    $sheet->setCellValueByColumnAndRow(10, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(11, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(12, $row, $data->Remark);
                    $row++;
                }
                $row--;
                $sheet->setBorder('B2:M' . $row, 'thin');
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B2:M2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B3:M' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
            });
        })->export('xls');
    }

    public function reportWithdraw() {
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $html = '
            <html>
                <head>
                    <style>
                        table{
                            border-spacing: 0;
                        }      
                        th{
                            padding: 3px;
                             border-spacing: 0;
                            border-bottom: 0px solid #ddd;
                            text-align: center;
                        }
                        td{
                            padding: 8px;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:80%; bottom: -63px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 128px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">Agent - ' . Auth::user()->UserName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Agent ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->UserID . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Email</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Email . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Commission Agent Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d M Y", strtotime(Input::get('sDate'))) . ' to ' . date("d M Y", strtotime(Input::get('eDate'))) . '</span><br><br>';
        //Payment
        $totalPayment = 0;
        $payment = Payment::join('m_company', 'm_company.InternalID', '=', 'CompanyInternalID')
                ->where('AgentInternalID', Auth::user()->InternalID)
                ->whereBetween('PaymentDate', Array($start, $end))
                ->get();
        $html.= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Income | Company Payment</span>';
        if (count($payment) > 0) {
            $html.= '<table width="698px"  style="margin-top: 18px; clear: both;  top: 78px; padding-left:13px;">';
            foreach ($payment as $dataPayment) {
                $company = Company::find($dataPayment->CompanyInternalID);
                $nominalPaket = 0;
                if ($company->PackageInternalID == 1) {
                    $nominalPaket = 350000;
                } else {
                    $nominalPaket = 1000000;
                }
                $payment = $nominalPaket * $dataPayment->AgentCommission / 100;
                $totalPayment += $payment;
                $text = number_format($payment, '2', '.', ',');
                $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-m-Y", strtotime($dataPayment->PaymentDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $dataPayment->PaymentID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $dataPayment->CompanyID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $dataPayment->CompanyName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $dataPayment->AgentCommission . '%</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $text . '</td>
                            </tr>';
            }
            $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif; margin: 5px !important;" colspan="6"><hr></td>
                            </tr>';
            $html.='</table>';

            $text = number_format($totalPayment, '2', '.', ',');
            $html.= '<table width="698px" style="clear: both; top: 78px; padding-left:20px;">';
            $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;" colspan="2" width="50%"></td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="30%">Total Income : </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right" width="20%">' . $text . '</td>
                            </tr>';
            $html.='</table>';
        } else {
            $html.= '<span style="padding-left:13px; font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no Income | Company Payment</span>';
        }

        //Withdraw
        $totalWithdraw = 0;
        $withdraw = Withdraw::where('AgentInternalID', Auth::user()->InternalID)
                ->whereBetween('WithdrawDate', Array($start, $end))
                ->get();
        $html.= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Outcome | Agent Withdraw</span>';
        if (count($withdraw) > 0) {
            $html.= '<table width="698px"  style="margin-top: 18px; clear: both;  top: 78px; padding-left:13px;">';
            foreach ($withdraw as $dataWithdraw) {
                $status = 'Pending';
                if ($dataWithdraw->Status == 1) {
                    $status = 'Completed';
                }
                $totalWithdraw += $dataWithdraw->WithdrawNominal;
                $text = number_format($dataWithdraw->WithdrawNominal, '2', '.', ',');
                $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-m-y", strtotime($dataWithdraw->WithdrawDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $dataWithdraw->WithdrawID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $status . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $text . '</td>
                            </tr>';
            }
            $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif; margin: 5px !important;" colspan="4"><hr></td>
                            </tr>';
            $html.='</table>';

            $text = number_format($totalWithdraw, '2', '.', ',');
            $html.= '<table width="698px" style="clear: both; top: 78px; padding-left:20px;">';
            $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;" colspan="2" width="50%"></td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="30%">Total Outcome : </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right" width="20%">' . $text . '</td>
                            </tr>';
            $html.='</table>';
        } else {
            $html.= '<span style="padding-left:13px; font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no Income | Company Payment</span>';
        }

        //Ending
        $totalSaldo = $totalPayment - $totalWithdraw;

        $text = number_format($totalSaldo, '2', '.', ',');
        if ($totalSaldo < 0) {
            $text = '(' . number_format($totalSaldo * -1, '2', '.', ',') . ')';
        }
        $html.= '<table width="698px" style="clear: both;  top: 78px;padding-left:8px;">';
        $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;margin: 5px !important;" colspan="4"><hr style="height:0.1px;"></td>
                            </tr>';
        $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;" colspan="2" width="50%"></td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="30%">Total Saldo : </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right" width="20%">' . $text . '</td>
                            </tr>';
        $html.='</table>';
        $html.='</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('withdraw_report');
    }

}
