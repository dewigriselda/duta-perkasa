<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class BankController extends BaseController {

    public function showBank() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertBank') {
                return $this->insertBank();
            }
            if (Input::get('jenis') == 'updateBank') {
                return $this->updateBank();
            }
            if (Input::get('jenis') == 'deleteBank') {
                return $this->deleteBank();
            }
        }
        return View::make('coa.bank')
                        ->withToogle('master')->withAktif('bank');
    }

    public function insertBank() {
        $data = Input::all();
        $rule = array(
            "BankID" => 'required|max:200|unique:m_bank,BankID,NULL,BankID,CompanyInternalID,' . Auth::user()->Company->InternalID . '',
            "BankName" => 'required'
//            "remark" => 'required'
        );
        $validator = Validator::make($data, $rule);
        if ($validator->Fails()) {
            return View::make('coa.bank')
                            ->withMessages("gagalInsert")
                            ->withErrors($validator->messages())
                            ->withToogle('master')->withAktif('bank');
        } else {
            $bank = new Bank();
            $bank->BankID = Input::get('BankID');
            $bank->BankName = Input::get('BankName');
            $bank->Remark = Input::get('remark');
            $bank->CompanyInternalID = Auth::user()->Company->InternalID;
            $bank->UserModified = "0";
            $bank->UserRecord = Auth::user()->UserID;
            $bank->save();
            return Redirect::route('showBank')->with('messages','suksesInsert');
        }
    }

    public function updateBank() {
        $data = Input::all();
        $rule = array(
            "BankName" => 'required'
//            "remark" => 'required'
        );
        $validator = Validator::make($data, $rule);
        if ($validator->Fails()) {
            return View::make('coa.bank')
                            ->withMessages("gagalInsert")
                            ->withErrors($validator->messages())
                            ->withToogle('master')->withAktif('bank');
        } else {
            $bank = Bank::find(Input::get('InternalID'));
            $bank->BankName = Input::get('BankName');
            $bank->Remark = Input::get('remark');
            $bank->CompanyInternalID = Auth::user()->Company->InternalID;
            $bank->UserModified = Auth::user()->UserID;
            $bank->save();

            return Redirect::route('showBank')->with('messages','suksesUpdate');
        }
    }

    public function deleteBank() {
        //cek di giro?
        $Bank = DB::table('t_journal_header')->where('BankInternalID', Input::get('InternalID'))->first();
        if (is_null($Bank)) {
            //tidak ada, maka boleh hapus
            $bank = Bank::find(Input::get('InternalID'));
            if ($bank->CompanyInternalID == Auth::user()->Company->InternalID) {
                $bank->delete();
                return View::make('coa.bank')
                                ->withMessages('suksesDelete')
                                ->withToogle('master')->withAktif('bank');
            } else {
                return View::make('coa.bank')
                                ->withMessages('accessDenied')
                                ->withToogle('master')->withAktif('bank');
            }
        } else {
            //ada maka tidak dihapus
            return View::make('coa.bank')
                            ->withMessages('gagalDelete')
                            ->withToogle('master')->withAktif('bank');
        }
    }

    public function exportBank() {
        Excel::create('Master_Bank', function($excel) {
            $excel->sheet('Master_Bank', function($sheet) {
                $sheet->mergeCells('B1:G1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Master Bank");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Bank ID");
                $sheet->setCellValueByColumnAndRow(3, 2, "Bank Name");
                $sheet->setCellValueByColumnAndRow(4, 2, "Record");
                $sheet->setCellValueByColumnAndRow(5, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(6, 2, "Remark");
                $row = 3;
                foreach (Bank::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, "`" . $data->BankID);
                    $sheet->setCellValueByColumnAndRow(3, $row, $data->BankName);
                    $sheet->setCellValueByColumnAndRow(4, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(5, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(6, $row, $data->Remark);
                    $row++;
                }

                if (Bank::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() <= 0) {
                    $sheet->mergeCells('B3:G3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "No data available in table");

                    $sheet->cells('B3:G3', function($cells) {
                        $cells->setAlignment('center');
                    });
                    $sheet->setBorder('B3:G' . $row, 'thin');
                }

                $row--;
                $sheet->setBorder('B2:G' . $row, 'thin');
                $sheet->cells('B2:G2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:G' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
            });
        })->export('xls');
    }

}
