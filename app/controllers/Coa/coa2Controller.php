<?php

class Coa2Controller extends BaseController {

    public function showCoa2() {
        return View::make('coa2');
    }

    public static function insertCoa2() {
        //rule
        $rule = array(
            'AccID' => 'required|max:200',
            'AccName' => 'required|max:200',
            'remark' => 'required|max:1000',
            '0' => 'unique:m_coa2,ACC2ID,NULL,ACC2ID,CompanyInternalID,' . Auth::user()->Company->InternalID . ''
        );
        $messages = array(
            '0.unique' => 'Account ID has already been taken.',
            'AccName.required' => 'Account name field is required.',
            'AccName.max' => 'Account name may not be greater than 200 characters.',
            'AccID.required' => 'Account ID field is required.',
            'AccID.max' => 'Account ID may not be greater than 200 characters.'
        );

        //validasi
        $data = Input::all();
        array_push($data, Input::get('parentid2') . Input::get('AccID'));
        $validator = Validator::make($data, $rule, $messages);
        if ($validator->fails()) {
            //tidak valid
            return View::make('coa.coaLevel')
                            ->withMessages('gagalInsert')
                            ->withError($validator->messages())
                            ->withToogle('accounting')->withAktif('coaLevel');
        } else {
            //valid
            $coa2 = new Coa2;
            $coa2->ACC2ID = Input::get('parentid2') . Input::get('AccID');
            $coa2->ACC2Name = Input::get('AccName');
            $coa2->UserRecord = Auth::user()->UserID;
            $coa2->CompanyInternalID = Auth::user()->Company->InternalID;
            $coa2->UserModified = '0';
            $coa2->Remark = Input::get('remark');
            $coa2->save();

            return View::make('coa.coaLevel')
                            ->withMessages('suksesInsert')
                            ->withToogle('accounting')->withAktif('coaLevel');
        }
    }

    static function updateCoa2() {
        //rule
        $rule = array(
            'AccName' => 'required|max:200',
            'remark' => 'required|max:1000'
        );
        $messages = array(
            'AccName.required' => 'Account name field is required.',
            'AccName.max' => 'Account name may not be greater than 200 characters.',
        );

        //validasi
        $validator = Validator::make(Input::all(), $rule, $messages);
        if ($validator->fails()) {
            //tidak valid
            return View::make('coa.coaLevel')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('accounting')->withAktif('coaLevel');
        } else {
            //valid
            $coa2 = Coa2::find(Input::get('InternalID'));
            if ($coa2->CompanyInternalID == Auth::user()->Company->InternalID) {
                $coa2->ACC2Name = Input::get('AccName');
                $coa2->UserModified = Auth::user()->UserID;
                $coa2->Remark = Input::get('remark');
                $coa2->save();
                return View::make('coa.coaLevel')
                                ->withMessages('suksesUpdate')
                                ->withToogle('accounting')->withAktif('coaLevel');
            } else {
                return View::make('coa.coaLevel')
                                ->withMessages('accessDenied')
                                ->withToogle('accounting')->withAktif('coaLevel');
            }
        }
    }

    static function deleteCoa2() {
        //cek ID coa2 sudah ada di table coa atau tidak
        $coa = DB::table('m_coa')->where('ACC2InternalID', Input::get('InternalID'))->first();
        $coa3 = Coa3::coa3inCoa2(Input::get('AccID'));
        if (is_null($coa) && count($coa3) <= 0) {
            //tidak ada maka boleh dihapus
            $coa2 = Coa2::find(Input::get('InternalID'));
            if ($coa2->CompanyInternalID == Auth::user()->Company->InternalID) {
                $coa2->delete();
                return View::make('coa.coaLevel')
                                ->withMessages('suksesDelete')
                                ->withToogle('accounting')->withAktif('coaLevel');
            } else {
                return View::make('coa.coaLevel')
                                ->withMessages('accessDenied')
                                ->withToogle('accounting')->withAktif('coaLevel');
            }
        } else {
            //ada maka tidak dihapus
            return View::make('coa.coaLevel')
                            ->withMessages('gagalDelete')
                            ->withToogle('accounting')->withAktif('coaLevel');
        }
    }

}
