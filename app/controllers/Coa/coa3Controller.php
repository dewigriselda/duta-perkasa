<?php

class Coa3Controller extends BaseController {

    public function showCoa3() {
        return View::make('coa3');
    }

    static function insertCoa3() {
        //rule
        $rule = array(
            'AccID' => 'required|max:200',
            'AccName' => 'required|max:200',
            'remark' => 'required|max:1000',
            '0' => 'unique:m_coa3,ACC3ID,NULL,ACC3ID,CompanyInternalID,' . Auth::user()->Company->InternalID . ''
        );
        $messages = array(
            '0.unique' => 'Account ID has already been taken.',
            'AccName.required' => 'Account name field is required.',
            'AccName.max' => 'Account name may not be greater than 200 characters.',
            'AccID.required' => 'Account ID field is required.',
            'AccID.max' => 'Account ID may not be greater than 200 characters.'
        );

        //validasi        
        $data = Input::all();
        array_push($data, Input::get('parentid3') . Input::get('AccID'));
        $validator = Validator::make($data, $rule, $messages);
        if ($validator->fails()) {
            //tidak valid
            return View::make('coa.coaLevel')
                            ->withMessages('gagalInsert')
                            ->withError($validator->messages())
                            ->withToogle('accounting')->withAktif('coaLevel');
        } else {
            //valid
            $coa3 = new Coa3;
            $coa3->ACC3ID = Input::get('parentid3') . Input::get('AccID');
            $coa3->ACC3Name = Input::get('AccName');
            $coa3->UserRecord = Auth::user()->UserID;
            $coa3->CompanyInternalID = Auth::user()->Company->InternalID;
            $coa3->UserModified = '0';
            $coa3->Remark = Input::get('remark');
            $coa3->save();

            return View::make('coa.coaLevel')
                            ->withMessages('suksesInsert')
                            ->withToogle('accounting')->withAktif('coaLevel');
        }
    }

    static function updateCoa3() {
        //rule
        $rule = array(
            'AccName' => 'required|max:200',
            'remark' => 'required|max:1000'
        );
        $messages = array(
            'AccName.required' => 'Account name field is required.',
            'AccName.max' => 'Account name may not be greater than 200 characters.',
        );

        //validasi
        $validator = Validator::make(Input::all(), $rule, $messages);
        if ($validator->fails()) {
            //tidak valid
            return View::make('coa.coaLevel')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('accounting')->withAktif('coaLevel');
        } else {
            //valid
            $coa3 = Coa3::find(Input::get('InternalID'));
            if ($coa3->CompanyInternalID == Auth::user()->Company->InternalID) {
                $coa3->ACC3Name = Input::get('AccName');
                $coa3->UserModified = Auth::user()->UserID;
                $coa3->Remark = Input::get('remark');
                $coa3->save();
                return View::make('coa.coaLevel')
                                ->withMessages('suksesUpdate')
                                ->withToogle('accounting')->withAktif('coaLevel');
            } else {
                return View::make('coa.coaLevel')
                                ->withMessages('accessDenied')
                                ->withToogle('accounting')->withAktif('coaLevel');
            }
        }
    }

    static function deleteCoa3() {
        //cek ID coa3 ada di tabel m_coa atau tidak
        $coa = DB::table('m_coa')->where('ACC3InternalID', Input::get('InternalID'))->first();
        $coa4 = Coa4::coa4inCoa3(Input::get('AccID'));
        if (is_null($coa) && count($coa4) <= 0) {
            //tidak ada maka data boleh dihapus
            $coa3 = Coa3::find(Input::get('InternalID'));
            if ($coa3->CompanyInternalID == Auth::user()->Company->InternalID) {
                $coa3->delete();
                return View::make('coa.coaLevel')
                                ->withMessages('suksesDelete')
                                ->withToogle('accounting')->withAktif('coaLevel');
            } else {
                return View::make('coa.coaLevel')
                                ->withMessages('accessDenied')
                                ->withToogle('accounting')->withAktif('coaLevel');
            }
        } else {
            //ada maka data tidak boleh di hapus
            return View::make('coa.coaLevel')
                            ->withMessages('gagalDelete')
                            ->withToogle('accounting')->withAktif('coaLevel');
        }
    }

}
