<?php

class Coa6Controller extends BaseController {

    public function showCoa6() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertCoa') {
                return $this->insertCoa6();
            }
            if (Input::get('jenis') == 'updateCoa') {
                return $this->updateCoa6();
            }
            if (Input::get('jenis') == 'deleteCoa') {
                return $this->deleteCoa6();
            }
            if (Input::get('jenis') == 'importCOA6') {
                return $this->importCoa6();
            }
        }
        return View::make('coa.coa6')
                        ->withToogle('accounting')->withAktif('coa6');
    }

    static function insertCoa6() {
        //rule
        $rule = array(
            'AccID' => 'required|max:200|unique:m_coa6,ACC6ID,NULL,ACC6ID,CompanyInternalID,' . Auth::user()->Company->InternalID . '',
            'AccName' => 'required|max:200',
            'Address' => 'max:1000',
            'City' => 'max:1000',
            'Block' => 'max:200',
            'AddressNumber' => 'max:200',
            'RT' => 'max:200',
            'RW' => 'max:200',
            'District' => 'max:200',
            'Subdistrict' => 'max:200',
            'Province' => 'max:200',
            'PostalCode' => 'max:200',
            'Origin' => 'max:200',
            'Phone' => 'max:200',
            'Fax' => 'max:200',
            'CreditLimit' => 'numeric',
            'Type' => 'required|max:100',
            'ContactPerson' => 'max:200',
            'remark' => 'max:1000'
        );
        $messages = array(
            'AccID.unique' => 'Account ID has already been taken.',
            'AccID.required' => 'Account ID field is required.',
            'AccID.max' => 'Account ID may not be greater than 200 characters.',
            'AccName.required' => 'Account name field is required.',
            'AccName.max' => 'Account name may not be greater than 200 characters.'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule, $messages);
        if ($validator->fails()) {
            //tidak valid
            return View::make('coa.coa6')
                            ->withMessages('gagalInsert')
                            ->withError($validator->messages())
                            ->withToogle('accounting')->withAktif('coa6');
        } else {
            //valid
            $taxID = "";
            if (Input::get("taxID") == "") {
                $taxID = "...-.";
            } else {
                $taxID = Input::get("taxID");
            }
            $coa6 = new Coa6;
            $coa6->ACC6ID = Input::get('AccID');
            $coa6->ACC6Name = Input::get('AccName');
            $coa6->Address = Input::get('Address');
            $coa6->City = Input::get('City');
            $coa6->Block = Input::get('Block');
            $coa6->AddressNumber = Input::get('AddressNumber');
            $coa6->RT = Input::get('RT');
            $coa6->RW = Input::get('RW');
            $coa6->District = Input::get('District');
            $coa6->Subdistrict = Input::get('Subdistrict');
            $coa6->Province = Input::get('Province');
            $coa6->PostalCode = Input::get('PostalCode');
            $coa6->TaxID = $taxID;
            $coa6->Origin = Input::get('Origin');
            $coa6->Email = Input::get('Email');
            $coa6->Phone = Input::get('Phone');
            $coa6->CustomerManager = Input::get('customerManager');
            $coa6->SalesAssistant = Input::get('salesAssistant');
            $coa6->Fax = Input::get('Fax');
            $coa6->CreditLimit = Input::get('CreditLimit');
            $coa6->Type = Input::get('Type');
            if (Input::get('Type') == "c") {
                $coa6->typeUser = Input::get('userType');
//                $coa6->COAInternalID = 375;
                $coa6->COAInternalID = 53;
            }
            if (Input::get('Type') == "s") {
                $coa6->typeUser = Input::get('userType2');
//                $coa6->COAInternalID = 374;
                $coa6->COAInternalID = 15;
            }
            $coa6->ContactPerson = Input::get('ContactPerson');
            $coa6->UserRecord = Auth::user()->UserID;
            $coa6->CompanyInternalID = Auth::user()->Company->InternalID;
            $coa6->UserModified = '0';
            $coa6->Remark = Input::get('remark');
            $coa6->save();

            return View::make('coa.coa6')
                            ->withMessages('suksesInsert')
                            ->withToogle('accounting')->withAktif('coa6');
        }
    }

    function updateCoa6() {
        //rule
        $rule = array(
            'AccName' => 'required|max:200',
            'Address' => 'max:1000',
            'City' => 'max:1000',
            'Block' => 'max:200',
            'AddressNumber' => 'max:200',
            'RT' => 'max:200',
            'RW' => 'max:200',
            'District' => 'max:200',
            'Subdistrict' => 'max:200',
            'Province' => 'max:200',
            'PostalCode' => 'max:200',
            'Origin' => 'max:200',
            'Phone' => 'max:200',
            'Fax' => 'max:200',
            'CreditLimit' => 'numeric',
            'ContactPerson' => 'max:200',
            'remark' => 'max:1000'
        );
        $messages = array(
            'AccName.required' => 'Account name field is required.',
            'AccName.max' => 'Account name may not be greater than 200 characters.'
        );

        //validasi
        $validator = Validator::make(Input::all(), $rule, $messages);
        if ($validator->fails()) {
            //tidak valid
            return View::make('coa.coa6')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('accounting')->withAktif('coa6');
        } else {
            //valid
            $coa6 = Coa6::find(Input::get('InternalID'));
            if ($coa6->CompanyInternalID == Auth::user()->Company->InternalID) {
                $taxID = "";
                if (Input::get("taxID") == "") {
                    $taxID = "...-.";
                } else {
                    $taxID = Input::get("taxID");
                }
                $coa6->ACC6Name = Input::get('AccName');
                $coa6->Address = Input::get('Address');
                $coa6->City = Input::get('City');
                $coa6->Block = Input::get('Block');
                $coa6->AddressNumber = Input::get('AddressNumber');
                $coa6->RT = Input::get('RT');
                $coa6->RW = Input::get('RW');
                $coa6->District = Input::get('District');
                $coa6->Subdistrict = Input::get('Subdistrict');
                $coa6->Province = Input::get('Province');
                $coa6->PostalCode = Input::get('PostalCode');
                $coa6->TaxID = $taxID;
                $coa6->Origin = Input::get('Origin');
                $coa6->Email = Input::get('Email');
                $coa6->Phone = Input::get('Phone');
                $coa6->CustomerManager = Input::get('customerManager');
                $coa6->SalesAssistant = Input::get('salesAssistant');
                $coa6->Fax = Input::get('Fax');
                if (Input::get('typeCust') == "c") {
                    $coa6->typeUser = Input::get('userType');
                }
                if (Input::get('typeCust') == "s") {
                    $coa6->typeUser = Input::get('userType2');
                }
                $coa6->CreditLimit = Input::get('CreditLimit');
                $coa6->ContactPerson = Input::get('ContactPerson');
                $coa6->UserModified = Auth::user()->UserID;
                $coa6->Remark = Input::get('remark');
                $coa6->save();
                return View::make('coa.coa6')
                                ->withMessages('suksesUpdate')
                                ->withToogle('accounting')->withAktif('coa6');
            } else {
                return View::make('coa.coa6')
                                ->withMessages('accessDenied')
                                ->withToogle('accounting')->withAktif('coa6');
            }
        }
    }

    function deleteCoa6() {
        //cek apakah ID coa6 ada di tabel m_coa atau tidak
        $coa = DB::table('m_coa')->where('ACC6InternalID', Input::get('InternalID'))->first();
        //cek apakah ID coa6 ada di tabel sales header atau tidak
        $sales = DB::table('t_sales_header')->where('ACC6InternalID', Input::get('InternalID'))->first();
        //cek apakah ID coa6 ada di tabel sales return header atau tidak
        $salesReturn = DB::table('t_salesreturn_header')->where('ACC6InternalID', Input::get('InternalID'))->first();
        //cek apakah ID coa6 ada di tabel sales order header atau tidak
        $salesOrder = DB::table('t_salesorder_header')->where('ACC6InternalID', Input::get('InternalID'))->first();
        //cek apakah ID coa6 ada di tabel sales order header atau tidak
        $quotation = DB::table('t_quotation_header')->where('ACC6InternalID', Input::get('InternalID'))->first();
        //cek apakah ID coa6 ada di tabel sales order header atau tidak
        $shipping = DB::table('t_shipping_header')->where('ACC6InternalID', Input::get('InternalID'))->first();
        //cek apakah ID coa6 ada di tabel purchase header atau tidak
        $purchase = DB::table('t_purchase_header')->where('ACC6InternalID', Input::get('InternalID'))->first();
        //cek apakah ID coa6 ada di tabel purchase header atau tidak
        $mrv = DB::table('t_mrv_header')->where('ACC6InternalID', Input::get('InternalID'))->first();
        //cek apakah ID coa6 ada di tabel purchase return header atau tidak
        $purchaseReturn = DB::table('t_purchasereturn_header')->where('ACC6InternalID', Input::get('InternalID'))->first();
        //cek apakah ID coa6 ada di tabel purchase order header atau tidak
        $purchaseOrder = DB::table('t_purchaseorder_header')->where('ACC6InternalID', Input::get('InternalID'))->first();
        //cek apakah ID coa6 ada di tabel purchase order header atau tidak
        $topup = DB::table('m_topup')->where('ACC6InternalID', Input::get('InternalID'))->first();
        if (is_null($coa) && is_null($sales) && is_null($salesReturn) && is_null($salesOrder) && is_null($purchase) && is_null($purchaseReturn) && is_null($purchaseOrder) && is_null($quotation) && is_null($shipping) && is_null($mrv)&& is_null($topup)) {
            //tidak ada maka data boleh dihapus
            $coa6 = Coa6::find(Input::get('InternalID'));
            if ($coa6->CompanyInternalID == Auth::user()->Company->InternalID) {
                $coa6->delete();
                return View::make('coa.coa6')
                                ->withMessages('suksesDelete')
                                ->withToogle('accounting')->withAktif('coa6');
            } else {
                return View::make('coa.coa6')
                                ->withMessages('accessDenied')
                                ->withToogle('accounting')->withAktif('coa6');
            }
        } else {
            //ada maka data tidak boleh dihapus
            return View::make('coa.coa6')
                            ->withMessages('gagalDelete')
                            ->withToogle('accounting')->withAktif('coa6');
        }
    }

    public function importCoa6() {
        Excel::selectSheetsByIndex(0)->load(Input::file('importcoa'), function($reader) {
            $result = $reader->all();
            foreach ($result as $row) {
                $custID = str_replace("`", "", $row['customer_id']);
                $customer = Coa6::where('ACC6ID', $custID)->first();
                if (!empty($customer)) {
                    $customer->ACC6Name = $row['customer_name'];
                    $customer->Address = $row['address'];
                    $customer->City = $row['city'];
                    $customer->Block = $row['block'];
                    $customer->AddressNumber = $row['number'];
                    $customer->RT = $row['rt'];
                    $customer->RW = $row['rw'];
                    $customer->District = $row['district'];
                    $customer->Subdistrict = $row['subdistrict'];
                    $customer->Province = $row['province'];
                    $customer->PostalCode = $row['postalcode'];
                    $customer->TaxID = $row['taxid'];
                    $customer->Origin = $row['origin'];
                    $customer->Phone = $row['phone'];
                    $customer->Fax = $row['fax'];
                    $customer->CreditLimit = $row['credit_limit'];
                    $customer->ContactPerson = $row['contact_person'];
                    $customer->UserModified = Auth::user()->UserID;
                    $customer->Remark = $row['remark'];
                    $customer->save();
                } else {
                    if ($row['taxid'] == "") {
                        $taxID = "...-.";
                    } else {
                        $taxID = $row['taxid'];
                    }
                    $coa6 = new Coa6;
                    $coa6->ACC6ID = $row['customer_id'];
                    $coa6->ACC6Name = $row['customer_name'];
                    $coa6->Address = $row['address'];
                    $coa6->City = $row['city'];
                    $coa6->Block = $row['block'];
                    $coa6->AddressNumber = $row['number'];
                    $coa6->RT = $row['rt'];
                    $coa6->RW = $row['rw'];
                    $coa6->District = $row['district'];
                    $coa6->Subdistrict = $row['subdistrict'];
                    $coa6->Province = $row['province'];
                    $coa6->PostalCode = $row['postalcode'];
                    $coa6->TaxID = $taxID;
                    $coa6->Origin = $row['origin'];
                    $coa6->Phone = $row['phone'];
                    $coa6->Fax = $row['fax'];
                    $coa6->CreditLimit = $row['credit_limit'];
                    $coa6->Type = "c";
                    $coa6->ContactPerson = $row['contact_person'];
                    $coa6->UserRecord = Auth::user()->UserID;
                    $coa6->CompanyInternalID = Auth::user()->Company->InternalID;
                    $coa6->UserModified = '0';
                    $coa6->Remark = $row['remark'];
                    $coa6->save();
                }
            }
        });
        Excel::selectSheetsByIndex(1)->load(Input::file('importcoa'), function($reader) {
            $result = $reader->all();
            foreach ($result as $row) {
                $suppID = str_replace("`", "", $row['supplier_id']);
                $supplier = Coa6::where('ACC6ID', $suppID)->first();
                if (!empty($supplier)) {
                    $supplier->ACC6Name = $row['supplier_name'];
                    $supplier->Address = $row['address'];
                    $supplier->City = $row['city'];
                    $supplier->Block = $row['block'];
                    $supplier->AddressNumber = $row['number'];
                    $supplier->RT = $row['rt'];
                    $supplier->RW = $row['rw'];
                    $supplier->District = $row['district'];
                    $supplier->Subdistrict = $row['subdistrict'];
                    $supplier->Province = $row['province'];
                    $supplier->PostalCode = $row['postalcode'];
                    $supplier->TaxID = $row['taxid'];
                    $supplier->Origin = $row['origin'];
                    $supplier->Phone = $row['phone'];
                    $supplier->Fax = $row['fax'];
                    $supplier->CreditLimit = $row['credit_limit'];
                    $supplier->ContactPerson = $row['contact_person'];
                    $supplier->UserModified = Auth::user()->UserID;
                    $supplier->Remark = $row['remark'];
                    $supplier->save();
                } else {
                    if ($row['taxid'] == "") {
                        $taxID = "...-.";
                    } else {
                        $taxID = $row['taxid'];
                    }
                    $coa6 = new Coa6;
                    $coa6->ACC6ID = $suppID;
                    $coa6->ACC6Name = $row['supplier_name'];
                    $coa6->Address = $row['address'];
                    $coa6->City = $row['city'];
                    $coa6->Block = $row['block'];
                    $coa6->AddressNumber = $row['number'];
                    $coa6->RT = $row['rt'];
                    $coa6->RW = $row['rw'];
                    $coa6->District = $row['district'];
                    $coa6->Subdistrict = $row['subdistrict'];
                    $coa6->Province = $row['province'];
                    $coa6->PostalCode = $row['postalcode'];
                    $coa6->TaxID = $taxID;
                    $coa6->Origin = $row['origin'];
                    $coa6->Phone = $row['phone'];
                    $coa6->Fax = $row['fax'];
                    $coa6->CreditLimit = $row['credit_limit'];
                    $coa6->Type = "s";
                    $coa6->ContactPerson = $row['contact_person'];
                    $coa6->UserRecord = Auth::user()->UserID;
                    $coa6->CompanyInternalID = Auth::user()->Company->InternalID;
                    $coa6->UserModified = '0';
                    $coa6->Remark = $row['remark'];
                    $coa6->save();
                }
            }
        });
        return View::make('coa.coa6')
                        ->withMessages('suksesUpdate')
                        ->withToogle('accounting')->withAktif('coa6');
    }

    public function exportExcel() {
        Excel::create('coa_level6', function($excel) {
            $excel->sheet('coa_level6_customer', function($sheet) {
                $sheet->mergeCells('B1:W1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Customer");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Customer Name");
                $sheet->setCellValueByColumnAndRow(3, 2, "Customer ID");
                $sheet->setCellValueByColumnAndRow(4, 2, "Address");
                $sheet->setCellValueByColumnAndRow(5, 2, "City");
                $sheet->setCellValueByColumnAndRow(6, 2, "Block");
                $sheet->setCellValueByColumnAndRow(7, 2, "Number");
                $sheet->setCellValueByColumnAndRow(8, 2, "RT");
                $sheet->setCellValueByColumnAndRow(9, 2, "RW");
                $sheet->setCellValueByColumnAndRow(10, 2, "Subdistrict");
                $sheet->setCellValueByColumnAndRow(11, 2, "District");
                $sheet->setCellValueByColumnAndRow(12, 2, "Province");
                $sheet->setCellValueByColumnAndRow(13, 2, "PostalCode");
                $sheet->setCellValueByColumnAndRow(14, 2, "TaxID");
                $sheet->setCellValueByColumnAndRow(15, 2, "Origin");
                $sheet->setCellValueByColumnAndRow(16, 2, "Phone");
                $sheet->setCellValueByColumnAndRow(17, 2, "Fax");
                $sheet->setCellValueByColumnAndRow(18, 2, "Credit Limit");
                $sheet->setCellValueByColumnAndRow(19, 2, "Contact Person");
                $sheet->setCellValueByColumnAndRow(20, 2, "Record");
                $sheet->setCellValueByColumnAndRow(21, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(22, 2, "Remark");
                $row = 3;
                foreach (Coa6::where("Type", "c")
                        ->where("InternalID", "!=", "0")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, $data->ACC6Name);
                    $sheet->setCellValueByColumnAndRow(3, $row, "`" . $data->ACC6ID);
                    $sheet->setCellValueByColumnAndRow(4, $row, $data->Address);
                    $sheet->setCellValueByColumnAndRow(5, $row, $data->City);
                    $sheet->setCellValueByColumnAndRow(6, $row, $data->Block);
                    $sheet->setCellValueByColumnAndRow(7, $row, $data->AddressNumber);
                    $sheet->setCellValueByColumnAndRow(8, $row, $data->RT);
                    $sheet->setCellValueByColumnAndRow(9, $row, $data->RW);
                    $sheet->setCellValueByColumnAndRow(10, $row, $data->Subdistrict);
                    $sheet->setCellValueByColumnAndRow(11, $row, $data->District);
                    $sheet->setCellValueByColumnAndRow(12, $row, $data->Province);
                    $sheet->setCellValueByColumnAndRow(13, $row, $data->PostalCode);
                    if ($data->TaxID == '...-.') {
                        $sheet->setCellValueByColumnAndRow(14, $row, '');
                    } else {
                        $sheet->setCellValueByColumnAndRow(14, $row, $data->TaxID);
                    }
                    $sheet->setCellValueByColumnAndRow(15, $row, $data->Origin);
                    $sheet->setCellValueByColumnAndRow(16, $row, '`' . $data->Phone);
                    $sheet->setCellValueByColumnAndRow(17, $row, '`' . $data->Fax);
                    $sheet->setCellValueByColumnAndRow(18, $row, number_format($data->CreditLimit, 2, '.', ','));
                    $sheet->setCellValueByColumnAndRow(19, $row, $data->ContactPerson);
                    $sheet->setCellValueByColumnAndRow(20, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(21, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(22, $row, $data->Remark);
                    $row++;
                }

                if (Coa6::where("Type", "c")
                                ->where("InternalID", "!=", "0")->where('CompanyInternalID', Auth::user()->Company->InternalID)->count() <= 0) {
                    $sheet->mergeCells('B3:W3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "No data available in table customer");

                    $sheet->cells('B3:W3', function($cells) {
                        $cells->setAlignment('center');
                    });

                    $sheet->setBorder('B3:W' . $row, 'thin');
                }
                $row--;
                $sheet->setBorder('B2:W' . $row, 'thin');
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B2:W2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B3:W' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
                $sheet->cells('S3:S' . $row, function($cells) {
                    $cells->setAlignment('right');
                });
            });
            $excel->sheet('coa_level6_supplier', function($sheet) {
                $sheet->mergeCells('B1:W1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Supplier");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Supplier Name");
                $sheet->setCellValueByColumnAndRow(3, 2, "Supplier ID");
                $sheet->setCellValueByColumnAndRow(4, 2, "Address");
                $sheet->setCellValueByColumnAndRow(5, 2, "City");
                $sheet->setCellValueByColumnAndRow(6, 2, "Block");
                $sheet->setCellValueByColumnAndRow(7, 2, "Number");
                $sheet->setCellValueByColumnAndRow(8, 2, "RT");
                $sheet->setCellValueByColumnAndRow(9, 2, "RW");
                $sheet->setCellValueByColumnAndRow(10, 2, "Subdistrict");
                $sheet->setCellValueByColumnAndRow(11, 2, "District");
                $sheet->setCellValueByColumnAndRow(12, 2, "Province");
                $sheet->setCellValueByColumnAndRow(13, 2, "PostalCode");
                $sheet->setCellValueByColumnAndRow(14, 2, "TaxID");
                $sheet->setCellValueByColumnAndRow(15, 2, "Origin");
                $sheet->setCellValueByColumnAndRow(16, 2, "Phone");
                $sheet->setCellValueByColumnAndRow(17, 2, "Fax");
                $sheet->setCellValueByColumnAndRow(18, 2, "Credit Limit");
                $sheet->setCellValueByColumnAndRow(19, 2, "Contact Person");
                $sheet->setCellValueByColumnAndRow(20, 2, "Record");
                $sheet->setCellValueByColumnAndRow(21, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(22, 2, "Remark");
                $row = 3;
                foreach (Coa6::where("Type", "s")
                        ->where("InternalID", "!=", "0")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, $data->ACC6Name);
                    $sheet->setCellValueByColumnAndRow(3, $row, "`" . $data->ACC6ID);
                    $sheet->setCellValueByColumnAndRow(4, $row, $data->Address);
                    $sheet->setCellValueByColumnAndRow(5, $row, $data->City);
                    $sheet->setCellValueByColumnAndRow(6, $row, $data->Block);
                    $sheet->setCellValueByColumnAndRow(7, $row, $data->AddressNumber);
                    $sheet->setCellValueByColumnAndRow(8, $row, $data->RT);
                    $sheet->setCellValueByColumnAndRow(9, $row, $data->RW);
                    $sheet->setCellValueByColumnAndRow(10, $row, $data->Subdistrict);
                    $sheet->setCellValueByColumnAndRow(11, $row, $data->District);
                    $sheet->setCellValueByColumnAndRow(12, $row, $data->Province);
                    $sheet->setCellValueByColumnAndRow(13, $row, $data->PostalCode);
                    if ($data->TaxID == '...-.') {
                        $sheet->setCellValueByColumnAndRow(14, $row, '');
                    } else {
                        $sheet->setCellValueByColumnAndRow(14, $row, $data->TaxID);
                    }
                    $sheet->setCellValueByColumnAndRow(15, $row, $data->Origin);
                    $sheet->setCellValueByColumnAndRow(16, $row, '`' . $data->Phone);
                    $sheet->setCellValueByColumnAndRow(17, $row, '`' . $data->Fax);
                    $sheet->setCellValueByColumnAndRow(18, $row, number_format($data->CreditLimit, 2, '.', ','));
                    $sheet->setCellValueByColumnAndRow(19, $row, $data->ContactPerson);
                    $sheet->setCellValueByColumnAndRow(20, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(21, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(22, $row, $data->Remark);
                    $row++;
                }

                if (Coa6::where("Type", "s")
                                ->where("InternalID", "!=", "0")->where('CompanyInternalID', Auth::user()->Company->InternalID)->count() <= 0) {
                    $sheet->mergeCells('B3:W3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "No data available in table supplier");

                    $sheet->cells('B3:W3', function($cells) {
                        $cells->setAlignment('center');
                    });

                    $sheet->setBorder('B3:W' . $row, 'thin');
                }

                $row--;
                $sheet->setBorder('B2:W' . $row, 'thin');
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B2:W2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B3:W' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:W' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
                $sheet->cells('S3:S' . $row, function($cells) {
                    $cells->setAlignment('right');
                });
            });
        })->export('xls');
    }

    public function customerDetail($customer) {
        $c = Coa6::where('ACC6ID', $customer)->first()->InternalID;
        $name = Coa6::where('ACC6ID', $customer)->first()->ACC6Name;

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertDetail') {
                return $this->insertDetail();
            }
            if (Input::get('jenis') == 'updateDetail') {
                return $this->updateDetail();
            }
            if (Input::get('jenis') == 'deleteDetail') {
                return $this->deleteDetail($c);
            }
        }

        return View::make('coa.customerdetail')->withCustomer($name)->withId($c)
                        ->withToogle('accounting')->withAktif('coa6');
    }

    public function insertDetail() {
        //rule
        $rule = array(
            //'AccID' => 'required|max:200|unique:m_coa6,ACC6ID,NULL,ACC6ID,CompanyInternalID,' . Auth::user()->Company->InternalID . '',
            'DetailName' => 'required|max:200',
            'Address' => 'max:1000',
            'City' => 'max:1000',
            'Block' => 'max:200',
            'AddressNumber' => 'max:200',
            'RT' => 'max:200',
            'RW' => 'max:200',
            'District' => 'max:200',
            'Subdistrict' => 'max:200',
            'Province' => 'max:200',
            'PostalCode' => 'max:200',
            'Phone' => 'max:200',
            'Fax' => 'max:200',
            'remark' => 'max:1000'
        );
        $messages = array(
            //'AccID.unique' => 'Account ID has already been taken.',
            'DetailID.max' => 'Account ID may not be greater than 200 characters.',
            'DetailName.required' => 'Account name field is required.',
            'DetailName.max' => 'Account name may not be greater than 200 characters.'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule, $messages);
        if ($validator->fails()) {
            //tidak valid
            return View::make('coa.customerdetail')
                            ->withMessages('gagalInsert')->withId(Input::get('id'))
                            ->withError($validator->messages())
                            ->withToogle('accounting')->withAktif('coa6');
        } else {
            //valid
            $taxID = "";
            if (Input::get("taxID") == "") {
                $taxID = "...-.";
            } else {
                $taxID = Input::get("taxID");
            }

            $detailID = autoDetailID(Input::get('id'));

            $coa6 = new CustomerDetail();
            $coa6->CustomerDetailID = $detailID;
            $coa6->ACC6InternalID = Input::get('id');
            $coa6->DetailName = Input::get('DetailName');
            $coa6->AreaInternalID = Input::get('Area');
            $coa6->Address = Input::get('Address');
            $coa6->City = Input::get('City');
            $coa6->Block = Input::get('Block');
            $coa6->AddressNumber = Input::get('AddressNumber');
            $coa6->NpwpAddress = Input::get('NpwpAddress');
            $coa6->TaxID = $taxID;
            $coa6->RT = Input::get('RT');
            $coa6->RW = Input::get('RW');
            $coa6->District = Input::get('District');
            $coa6->Subdistrict = Input::get('Subdistrict');
//            if (Input::get('centralizationTax') == 1) {
//                $coa6->TaxCentralization = Input::get('centralizationTax');
//            } else {
//                $coa6->TaxCentralization = 0;
//            }
            $coa6->Province = Input::get('Province');
            $coa6->PostalCode = Input::get('PostalCode');
            $coa6->Phone = Input::get('Phone');
            $coa6->Email = Input::get('Email');
            $coa6->Fax = Input::get('Fax');
            $coa6->UserRecord = Auth::user()->UserID;
            $coa6->CompanyInternalID = Auth::user()->Company->InternalID;
            $coa6->UserModified = '0';
            $coa6->Remark = Input::get('remark');
            $coa6->save();

            $name = Coa6::find(Input::get('id'))->ACC6Name;

            return View::make('coa.customerdetail')
                            ->withMessages('suksesInsert')->withCustomer($name)->withId($coa6->ACC6InternalID)
                            ->withToogle('accounting')->withAktif('coa6');
        }
    }

    public function updateDetail() {
        //rule
        $rule = array(
            'DetailName' => 'required|max:200',
            'Address' => 'max:1000',
            'City' => 'max:1000',
            'Block' => 'max:200',
            'AddressNumber' => 'max:200',
            'RT' => 'max:200',
            'RW' => 'max:200',
            'District' => 'max:200',
            'Subdistrict' => 'max:200',
            'Province' => 'max:200',
            'PostalCode' => 'max:200',
            'Phone' => 'max:200',
            'Fax' => 'max:200',
            'remark' => 'max:1000'
        );
        $messages = array(
            //'AccID.unique' => 'Account ID has already been taken.',
            //            'DetailID.required' => 'Account ID field is required.',
            //            'DetailID.max' => 'Account ID may not be greater than 200 characters.',
            'DetailName.required' => 'Account name field is required.',
            'DetailName.max' => 'Account name may not be greater than 200 characters.'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule, $messages);
        if ($validator->fails()) {
            //tidak valid
            return View::make('coa.customerdetail')
                            ->withMessages('gagalInsert')->withId(Input::get('InternalID'))
                            ->withError($validator->messages())
                            ->withToogle('accounting')->withAktif('coa6');
        } else {
            //valid
            $taxID = "";
            if (Input::get("taxID") == "") {
                $taxID = "...-.";
            } else {
                $taxID = Input::get("taxID");
            }
            $coa6 = CustomerDetail::find(Input::get('InternalID'));
            $coa6->DetailName = Input::get('DetailName');
            $coa6->AreaInternalID = Input::get('Area');
            $coa6->Address = Input::get('Address');
            $coa6->City = Input::get('City');
            $coa6->Block = Input::get('Block');
            $coa6->AddressNumber = Input::get('AddressNumber');
            $coa6->NpwpAddress = Input::get('NpwpAddress');
            $coa6->TaxID = $taxID;
            $coa6->RT = Input::get('RT');
            $coa6->RW = Input::get('RW');
            $coa6->District = Input::get('District');
            $coa6->Subdistrict = Input::get('Subdistrict');
//            if (Input::get('centralizationTax') == 1) {
//                $coa6->TaxCentralization = Input::get('centralizationTax');
//            } else {
//                $coa6->TaxCentralization = 0;
//            }
            $coa6->Province = Input::get('Province');
            $coa6->PostalCode = Input::get('PostalCode');
            $coa6->Phone = Input::get('Phone');
            $coa6->Email = Input::get('Email');
            $coa6->Fax = Input::get('Fax');
            $coa6->UserRecord = Auth::user()->UserID;
            $coa6->CompanyInternalID = Auth::user()->Company->InternalID;
            $coa6->UserModified = '0';
            $coa6->Remark = Input::get('remark');
            $coa6->save();

            $name = Coa6::find($coa6->ACC6InternalID)->ACC6Name;

            return View::make('coa.customerdetail')
                            ->withMessages('suksesUpdate')->withCustomer($name)->withId($coa6->ACC6InternalID)
                            ->withToogle('accounting')->withAktif('coa6');
        }
    }

    function deleteDetail($c) {
        //cek apakah ID coa6 ada di tabel sales order header atau tidak
        $salesOrder = DB::table('t_salesorder_header')->where('CustomerDetailInternalID', Input::get('InternalID'))->first();
        //cek apakah ID coa6 ada di tabel purchase header atau tidak
        $quot = DB::table('t_quotation_header')->where('CustomerDetailInternalID', Input::get('InternalID'))->first();
        if (is_null($salesOrder) && is_null($quot)) {
            //tidak ada maka data boleh dihapus
            $coa6 = CustomerDetail::find(Input::get('InternalID'));
            if ($coa6->CompanyInternalID == Auth::user()->Company->InternalID) {
                $coa6->delete();
                return View::make('coa.customerdetail')->withId($c)
                                ->withToogle('accounting')->withAktif('coa6')
                                ->withMessages('suksesDelete');
            } else {
                return View::make('coa.customerdetail')->withId($c)
                                ->withToogle('accounting')->withAktif('coa6')
                                ->withMessages('accessDenied');
            }
        } else {
            //ada maka data tidak boleh dihapus
            return View::make('coa.customerdetail')->withId($c)
                            ->withToogle('accounting')->withAktif('coa6')
                            ->withMessages('gagalDelete');
        }
    }

    public function exportDetail($id) {
        $customer = Coa6::where('ACC6ID', $id)->first();
        Excel::create('customer_detail', function($excel) use ($customer) {
            $excel->sheet('customer_detail', function($sheet) use ($customer) {
                $sheet->mergeCells('B1:S1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Customer Detail " . $customer->ACC6Name);
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Detail ID");
                $sheet->setCellValueByColumnAndRow(3, 2, "Detail Name");
                $sheet->setCellValueByColumnAndRow(4, 2, "Address");
                $sheet->setCellValueByColumnAndRow(5, 2, "City");
                $sheet->setCellValueByColumnAndRow(6, 2, "Block");
                $sheet->setCellValueByColumnAndRow(7, 2, "Number");
                $sheet->setCellValueByColumnAndRow(8, 2, "RT");
                $sheet->setCellValueByColumnAndRow(9, 2, "RW");
                $sheet->setCellValueByColumnAndRow(10, 2, "Subdistrict");
                $sheet->setCellValueByColumnAndRow(11, 2, "District");
                $sheet->setCellValueByColumnAndRow(12, 2, "Province");
                $sheet->setCellValueByColumnAndRow(13, 2, "PostalCode");
                $sheet->setCellValueByColumnAndRow(14, 2, "Phone");
                $sheet->setCellValueByColumnAndRow(15, 2, "Fax");
                $sheet->setCellValueByColumnAndRow(16, 2, "Record");
                $sheet->setCellValueByColumnAndRow(17, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(18, 2, "Remark");
                $row = 3;
                foreach (CustomerDetail::where("ACC6InternalID", $customer->InternalID)->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, "`" . $data->CustomerDetailID);
                    $sheet->setCellValueByColumnAndRow(3, $row, $data->DetailName);
                    $sheet->setCellValueByColumnAndRow(4, $row, $data->Address);
                    $sheet->setCellValueByColumnAndRow(5, $row, $data->City);
                    $sheet->setCellValueByColumnAndRow(6, $row, $data->Block);
                    $sheet->setCellValueByColumnAndRow(7, $row, $data->AddressNumber);
                    $sheet->setCellValueByColumnAndRow(8, $row, $data->RT);
                    $sheet->setCellValueByColumnAndRow(9, $row, $data->RW);
                    $sheet->setCellValueByColumnAndRow(10, $row, $data->Subdistrict);
                    $sheet->setCellValueByColumnAndRow(11, $row, $data->District);
                    $sheet->setCellValueByColumnAndRow(12, $row, $data->Province);
                    $sheet->setCellValueByColumnAndRow(13, $row, $data->PostalCode);
                    $sheet->setCellValueByColumnAndRow(14, $row, '`' . $data->Phone);
                    $sheet->setCellValueByColumnAndRow(15, $row, '`' . $data->Fax);
                    $sheet->setCellValueByColumnAndRow(16, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(17, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(18, $row, $data->Remark);
                    $row++;
                }

                if (CustomerDetail::where("ACC6InternalID", $customer->InternalID)->where('CompanyInternalID', Auth::user()->Company->InternalID)->count() <= 0) {
                    $sheet->mergeCells('B3:S3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "No data available in table customer detail");

                    $sheet->cells('B3:S3', function($cells) {
                        $cells->setAlignment('center');
                    });

                    $sheet->setBorder('B3:S' . $row, 'thin');
                }
                $row--;
                $sheet->setBorder('B2:S' . $row, 'thin');
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B2:S2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B3:S' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
            });
        })->export('xls');
    }

    public function CustomerDataBackup() {
        $table = 'm_coa6';
        $primaryKey = 'InternalID';
        $columns = array(
            array('db' => 'ACC6ID', 'dt' => 0),
            array('db' => 'typeUser', 'dt' => 1, 'formatter' => function($d, $row) {
                    if ($d == "er") {
                        return "END USER";
                    } else if ($d == "sr") {
                        return "SELLER";
                    } else {
                        return '&nbsp;';
                    }
                }),
            array('db' => 'ACC6Name', 'dt' => 2),
            array('db' => 'Address', 'dt' => 3),
            array('db' => 'City', 'dt' => 4),
            array('db' => 'PostalCode', 'dt' => 5),
            array('db' => 'TaxID', 'dt' => 6, 'formatter' => function($d, $row) {
                    if ($d == "..-." || $d == "...-.") {
                        return '&nbsp;';
                    } else {
                        return $d;
                    }
                }),
            array('db' => 'Phone', 'dt' => 7),
            array('db' => 'CreditLimit', 'dt' => 8,
                'formatter' => function($d, $row) {
                    return number_format($d, 2, '.', ',');
                }
            ),
            array('db' => 'InternalID', 'dt' => 9, 'formatter' => function( $d, $row ) {
                    $data = Coa6::find($d);
                    $data->dtRecordformat = date("d-m-Y H:i:s", strtotime($data->dtRecord));
                    $data->dtModifformat = date("d-m-Y H:i:s", strtotime($data->dtModified));
                    $data->Remark = str_replace("\r\n", " ", $data->Remark);
                    $arrData = array($data);
                    $tamp = myEscapeStringData($arrData);
                    $return = "<button id='btn-" . $data->ACC6ID . "' data-target='#m_coaUpdate' data-all='" . $tamp . "'
                                                        data-level='6' data-toggle='modal' role='dialog' onclick='updateCustomer(this)'
                                                        class='btn btn-pure-xs btn-xs btn-edit'>
                                                    <span class='glyphicon glyphicon-edit'></span>
                               </button>";
                    $return .= " <button data-target='#m_coaDelete' onclick='deleteCustomer(this)' data-internal='" . $data->InternalID . "'  data-toggle='modal' role='dialog'
                                                        data-id='" . $data->ACC6ID . "' data-name='" . $data->ACC6Name . "' data-level='5' class='btn btn-pure-xs btn-xs btn-delete'>
                                                    <span class='glyphicon glyphicon-trash'></span>
                                  </button>";

                    if (checkModul('O04')) {
                        $return .= "<a href='" . Route("historyCustomer", $data->ACC6ID) . "' target='_blank'>
                                                    <button id='btn-" . $data->ACC6ID . "-print'
                                                            class='btn btn-pure-xs btn-xs btn-price'>
                                                        History
                                                    </button>
                                                </a>";
                    }
                    $return .= "<a href='" . Route('showCustomerDetail', $data->ACC6ID) . "'>
                                        <button id='btn-" . $data->ACC6ID . "-detail'
                                                class='btn btn-pure-xs btn-xs btn-detail' title='Detail'>
                                            <span class='glyphicon glyphicon-zoom-in'></span>
                                        </button>
                                    </a>";
                    return $return;
                },
                        'field' => 'InternalID')
                );
                $sql_details = getConnection();
                require('ssp.class.php');
                $ID_CLIENT_VALUE = Auth::user()->CompanyInternalID;
                $extraCondition = 'Type = "c" and CompanyInternalID=' . $ID_CLIENT_VALUE;


                echo json_encode(
                        SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $extraCondition, $join = NULL));
            }

            public function SupplierDataBackup() {
                $table = 'm_coa6';
                $primaryKey = 'InternalID';
                $columns = array(
                    array('db' => 'ACC6ID', 'dt' => 0),
                    array('db' => 'typeUser', 'dt' => 1, 'formatter' => function($d, $row) {
                            if ($d == "im") {
                                return "IMPORT";
                            } else if ($d == "lo") {
                                return "LOCAL";
                            } else {
                                return '&nbsp;';
                            }
                        }),
                    array('db' => 'ACC6Name', 'dt' => 2),
                    array('db' => 'Address', 'dt' => 3),
                    array('db' => 'City', 'dt' => 4),
                    array('db' => 'PostalCode', 'dt' => 5),
                    array('db' => 'TaxID', 'dt' => 6, 'formatter' => function($d, $row) {
                            if ($d == "..-." || $d == "...-.") {
                                return '&nbsp;';
                            } else {
                                return $d;
                            }
                        }),
                    array('db' => 'Phone', 'dt' => 7),
                    array('db' => 'CreditLimit', 'dt' => 8,
                        'formatter' => function($d, $row) {
                            return number_format($d, 2, '.', ',');
                        }
                    ),
                    array('db' => 'InternalID', 'dt' => 9, 'formatter' => function( $d, $row ) {
                            $data = Coa6::find($d);
                            $data->dtRecordformat = date("d-m-Y H:i:s", strtotime($data->dtRecord));
                            $data->dtModifformat = date("d-m-Y H:i:s", strtotime($data->dtModified));
                            $data->Remark = str_replace("\r\n", " ", $data->Remark);
                            $arrData = array($data);
                            $tamp = myEscapeStringData($arrData);
                            $return = "<button id='btn-" . $data->ACC6ID . "' data-target='#m_coaUpdate' data-all='" . $tamp . "'
                                                        data-level='6' data-toggle='modal' role='dialog' onclick='updateCustomer(this)'
                                                        class='btn btn-pure-xs btn-xs btn-edit'>
                                                    <span class='glyphicon glyphicon-edit'></span>
                               </button>";
                            $return .= " <button data-target='#m_coaDelete' onclick='deleteCustomer(this)' data-internal='" . $data->InternalID . "'  data-toggle='modal' role='dialog'
                                                        data-id='" . $data->ACC6ID . "' data-name='" . $data->ACC6Name . "' data-level='5' class='btn btn-pure-xs btn-xs btn-delete'>
                                                    <span class='glyphicon glyphicon-trash'></span>
                                  </button>";

                            if (checkModul('O04')) {
                                $return .= "<a href='" . Route("historySupplier", $data->ACC6ID) . "' target='_blank'>
                                                    <button id='btn-" . $data->ACC6ID . "-print'
                                                            class='btn btn-pure-xs btn-xs btn-price'>
                                                        History
                                                    </button>
                                                </a>";
                            }
                            return $return;
                        },
                                'field' => 'InternalID')
                        );
                        $sql_details = getConnection();
                        require('ssp.class.php');
                        $ID_CLIENT_VALUE = Auth::user()->CompanyInternalID;
                        $extraCondition = 'Type = "s" and CompanyInternalID=' . $ID_CLIENT_VALUE;


                        echo json_encode(
                                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $extraCondition, $join = NULL));
                    }

                    public function IndustryDataBackup() {
                        $table = 'm_coa6';
                        $primaryKey = 'InternalID';
                        $columns = array(
                            array('db' => 'ACC6ID', 'dt' => 0),
                            array('db' => 'ACC6Name', 'dt' => 1),
                            array('db' => 'Address', 'dt' => 2),
                            array('db' => 'City', 'dt' => 3),
                            array('db' => 'PostalCode', 'dt' => 4),
                            array('db' => 'TaxID', 'dt' => 5, 'formatter' => function($d, $row) {
                                    if ($d == "..-." || $d == "...-.") {
                                        return '&nbsp;';
                                    } else {
                                        return $d;
                                    }
                                }),
                            array('db' => 'Phone', 'dt' => 6),
                            array('db' => 'CreditLimit', 'dt' => 7,
                                'formatter' => function($d, $row) {
                                    return number_format($d, 2, '.', ',');
                                }
                            ),
                            array('db' => 'InternalID', 'dt' => 8, 'formatter' => function( $d, $row ) {
                                    $data = Coa6::find($d);
                                    $data->dtRecordformat = date("d-m-Y H:i:s", strtotime($data->dtRecord));
                                    $data->dtModifformat = date("d-m-Y H:i:s", strtotime($data->dtModified));
                                    $data->Remark = str_replace("\r\n", " ", $data->Remark);
                                    $arrData = array($data);
                                    $tamp = myEscapeStringData($arrData);
                                    $return = "<button id='btn-" . $data->ACC6ID . "' data-target='#m_coaUpdate' data-all='" . $tamp . "'
                                                        data-level='6' data-toggle='modal' role='dialog' onclick='updateCustomer(this)'
                                                        class='btn btn-pure-xs btn-xs btn-edit'>
                                                    <span class='glyphicon glyphicon-edit'></span>
                               </button>";
                                    $return .= " <button data-target='#m_coaDelete' onclick='deleteCustomer(this)' data-internal='" . $data->InternalID . "'  data-toggle='modal' role='dialog'
                                                        data-id='" . $data->ACC6ID . "' data-name='" . $data->ACC6Name . "' data-level='5' class='btn btn-pure-xs btn-xs btn-delete'>
                                                    <span class='glyphicon glyphicon-trash'></span>
                                  </button>";

                                    if (checkModul('O04')) {
//                                $return.="<a href='" . Route("historyIndustry", $data->ACC6ID) . "' target='_blank'>
//                                                    <button id='btn-" . $data->ACC6ID . "-print'
//                                                            class='btn btn-pure-xs btn-xs btn-price'>
//                                                        History
//                                                    </button>
//                                                </a>";
                                    }
                                    return $return;
                                },
                                        'field' => 'InternalID')
                                );
                                $sql_details = getConnection();
                                require('ssp.class.php');
                                $ID_CLIENT_VALUE = Auth::user()->CompanyInternalID;
                                $extraCondition = 'Type = "i" and CompanyInternalID=' . $ID_CLIENT_VALUE;


                                echo json_encode(
                                        SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $extraCondition, $join = NULL));
                            }

                            public function historyCustomer($id) {
                                $coa6 = Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->where('ACC6ID', $id)->first();
                                $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 420px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">History Sales Customer - ' . $coa6->ACC6Name . ' (' . $coa6->ACC6ID . ')</h5>';
                                $html .= '<table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Qty</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Price</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc (%)</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                                $dateMin2Bulan = date("Y-m-d", strtotime("-2 month", strtotime(date('Y-m-d'))));
                                if (SalesDetail::join('t_sales_header', 't_sales_header.InternalID', '=', 't_sales_detail.SalesInternalID')
                                                ->where('ACC6InternalID', $coa6->InternalID)->where('SalesDate', '>', $dateMin2Bulan)->count() > 0) {
                                    foreach (SalesDetail::join('t_sales_header', 't_sales_header.InternalID', '=', 't_sales_detail.SalesInternalID')
                                            ->where('ACC6InternalID', $coa6->InternalID)->where('SalesDate', '>', $dateMin2Bulan)->orderBy('SalesDate', 'desc')->get() as $data) {
                                        $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->SalesID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . Inventory::find($data->InventoryInternalID)->InventoryID . ' ' . Inventory::find($data->InventoryInternalID)->InventoryName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Qty . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Discount . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->DiscountNominal . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
                                    }
                                } else {
                                    $html .= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: center">There is no sales registered.</td>
                        </tr>';
                                }

                                $html .= '</tbody>
            </table>';
                                $html .= '</div>
                    </div>
                </body>
            </html>';
                                return PDF ::load($html, 'A4', 'portrait')->download('history_customer');
                            }

                            public function historySupplier($id) {
                                $coa6 = Coa6::where("Type", "s")->where('CompanyInternalID', Auth::user()->Company->InternalID)->where('ACC6ID', $id)->first();
                                $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 420px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">History Purchase Supplier - ' . $coa6->ACC6Name . ' (' . $coa6->ACC6ID . ')</h5>';
                                $html .= '<table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Purchase ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Qty</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Price</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc (%)</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                                $dateMin2Bulan = date("Y-m-d", strtotime("-2 month", strtotime(date('Y-m-d'))));
                                if (PurchaseDetail::join('t_purchase_header', 't_purchase_header.InternalID', '=', 't_purchase_detail.PurchaseInternalID')
                                                ->where('ACC6InternalID', $coa6->InternalID)->where('PurchaseDate', '>', $dateMin2Bulan)->count() > 0) {
                                    foreach (PurchaseDetail::join('t_purchase_header', 't_purchase_header.InternalID', '=', 't_purchase_detail.PurchaseInternalID')
                                            ->where('ACC6InternalID', $coa6->InternalID)->where('PurchaseDate', '>', $dateMin2Bulan)->orderBy('PurchaseDate', 'desc')->get() as $data) {
                                        $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->PurchaseID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . Inventory::find($data->InventoryInternalID)->InventoryID . ' ' . Inventory::find($data->InventoryInternalID)->InventoryName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Qty . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Discount . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->DiscountNominal . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
                                    }
                                } else {
                                    $html .= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: center">There is no purchase registered.</td>
                        </tr>';
                                }

                                $html .= '</tbody>
            </table>';
                                $html .= '</div>
                    </div>
                </body>
            </html>';
                                return PDF ::load($html, 'A4', 'portrait')->download('history_supplier');
                            }

                        }
                        