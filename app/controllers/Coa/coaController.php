<?php

class CoaController extends BaseController {

    public function showCoa() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertCoa') {
                return $this->insertCoa();
            }
            if (Input::get('jenis') == 'updateCoa') {
                return $this->updateCoa();
            }
            if (Input::get('jenis') == 'deleteCoa') {
                return $this->deleteCoa();
            }
            if (Input::get('jenis') == 'coaReport') {
                if(Input::get("downloadType") == "view"){
                    return $this->reportCoaView();
                }
                else if (Input::get('downloadType') == 'pdf') {
                    return $this->reportCoa();
                } else {
                    return $this->reportCoaExcel();
                }
            }
        }
        return View::make('coa.coa')
                        ->withToogle('accounting')->withAktif('coa');
    }

    public static function insertCoa() {
        //rule
        $rule = array(
            'coa1' => 'required',
            'coa2' => 'required',
            'coa3' => 'required',
            'coa4' => 'required',
            'coa5' => 'required',
            'coa6' => 'required',
            'CoaName' => 'required|max:200',
            'Type' => 'required',
            'Balance' => 'required',
            'Header1' => 'required|max:500',
            'Header2' => 'required|max:500',
            'Header3' => 'required|max:500',
            'HeaderCashFlow1' => 'required|max:500',
            'HeaderCashFlow2' => 'required|max:500',
            'HeaderCashFlow3' => 'required|max:500',
            'remark' => 'required|max:1000',
            '0' => 'unique:m_coa,InternalID'
        );
        $messages = array(
            '0.unique' => 'Account ID has already been taken.',
            'CoaName.required' => 'Account name field is required.',
            'CoaName.max' => 'Account name may not be greater than 200 characters.'
        );

        //validasi
        $coa1 = explode('---;---', Input::get('coa1'))[2];
        $coa2 = explode('---;---', Input::get('coa2'))[2];
        $coa3 = explode('---;---', Input::get('coa3'))[2];
        $coa4 = explode('---;---', Input::get('coa4'))[2];
        $coa5 = explode('---;---', Input::get('coa5'))[2];
        $coa6 = explode('---;---', Input::get('coa6'))[2];
        $data = Input::all();
        array_push($data, Coa::getInternalID($coa1, $coa2, $coa3, $coa4, $coa5, $coa6));
        $validator = Validator::make($data, $rule, $messages);
        if ($validator->fails()) {
            //tidak valid
            return View::make('coa.coa')
                            ->withMessages('gagalInsert')
                            ->withError($validator->messages())
                            ->withToogle('accounting')->withAktif('coa');
        } else {
            //valid
            $coa = new Coa;
            $coa->ACC1InternalID = $coa1;
            $coa->ACC2InternalID = $coa2;
            $coa->ACC3InternalID = $coa3;
            $coa->ACC4InternalID = $coa4;
            $coa->ACC5InternalID = $coa5;
            $coa->ACC6InternalID = $coa6;
            $coa->COAName = Input::get('CoaName');
            $coa->Flag = Input::get('Type');
            $coa->Header1 = Input::get('Header1');
            $coa->Header2 = Input::get('Header2');
            $coa->Header3 = Input::get('Header3');
            $coa->HeaderCashFlow1 = Input::get('HeaderCashFlow1');
            $coa->HeaderCashFlow2 = Input::get('HeaderCashFlow2');
            $coa->HeaderCashFlow3 = Input::get('HeaderCashFlow3');
            $coa->Persediaan = 0;
            $coa->InitialBalance = str_replace(',', '', Input::get('Balance'));
            if (Input::get('Stock') == '1') {
                $coa->Persediaan = '1';
            }
            $coa->UserRecord = Auth::user()->UserID;
            $coa->CompanyInternalID = Auth::user()->Company->InternalID;
            $coa->UserModified = "0";
            $coa->Remark = Input::get('remark');
            $coa->save();

            return View::make('coa.coa')
                            ->withMessages('suksesInsert')
                            ->withToogle('accounting')->withAktif('coa');
        }
    }

    function updateCoa() {
        //rule
        $rule = array(
            'CoaName' => 'required|max:200',
            'Header1' => 'required|max:500',
            'Header2' => 'required|max:500',
            'Header3' => 'required|max:500',
            'HeaderCashFlow1' => 'required|max:500',
            'HeaderCashFlow2' => 'required|max:500',
            'HeaderCashFlow3' => 'required|max:500',
            'Balance' => 'required',
            'remark' => 'required|max:1000'
        );
        $messages = array(
            'CoaName.required' => 'Account name field is required.',
            'CoaName.max' => 'Account name may not be greater than 200 characters.'
        );
        //validasi
        $validator = Validator::make(Input::all(), $rule, $messages);
        if ($validator->fails()) {
            //tidak valid
            return View::make('coa.coa')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('accounting')->withAktif('coa');
        } else {
            //valid
            $coa = Coa::find(Input::get('InternalID'));
            if ($coa->CompanyInternalID == Auth::user()->Company->InternalID) {
                $coa->COAName = Input::get('CoaName');
                $coa->Header1 = Input::get('Header1');
                $coa->Header2 = Input::get('Header2');
                $coa->Header3 = Input::get('Header3');
                $coa->HeaderCashFlow1 = Input::get('HeaderCashFlow1');
                $coa->HeaderCashFlow2 = Input::get('HeaderCashFlow2');
                $coa->HeaderCashFlow3 = Input::get('HeaderCashFlow3');
                $coa->Persediaan = 0;
                $coa->InitialBalance = str_replace(',', '', Input::get('Balance'));
                if (Input::get('Stock') == '1') {
                    $coa->Persediaan = '1';
                }
                $coa->UserModified = Auth::user()->UserID;
                $coa->Remark = Input::get('remark');
                $coa->save();
                return View::make('coa.coa')
                                ->withMessages('suksesUpdate')
                                ->withToogle('accounting')->withAktif('coa');
            } else {
                return View::make('coa.coa')
                                ->withMessages('accessDenied')
                                ->withToogle('accounting')->withAktif('coa');
            }
        }
    }

    function deleteCoa() {
        $coa = Coa::find(Input::get('InternalID'));
        $ACC = explode(".", Input::get('AccID'));
        $slip = DB::table('m_slip')
                ->where('ACC1InternalID', $ACC[0])
                ->where('ACC2InternalID', $ACC[1])
                ->where('ACC3InternalID', $ACC[2])
                ->where('ACC4InternalID', $ACC[3])
                ->where('ACC5InternalID', $ACC[4])
                ->where('ACC6InternalID', $ACC[5])
                ->first();
        //cek coa ada di master slip atau tidak
        $journal = DB::table('t_journal_detail')
                ->where('ACC1InternalID', $ACC[0])
                ->where('ACC2InternalID', $ACC[1])
                ->where('ACC3InternalID', $ACC[2])
                ->where('ACC4InternalID', $ACC[3])
                ->where('ACC5InternalID', $ACC[4])
                ->where('ACC6InternalID', $ACC[5])
                ->first();
        //cek coa ada di master journal detail atau tidak
        $inventoryType = DB::table('m_inventorytype')
                ->where('ACC1InternalID', $ACC[0])
                ->where('ACC2InternalID', $ACC[1])
                ->where('ACC3InternalID', $ACC[2])
                ->where('ACC4InternalID', $ACC[3])
                ->where('ACC5InternalID', $ACC[4])
                ->where('ACC6InternalID', $ACC[5])
                ->first();
        //cek coa ada di master depreciation atau tidak
        $default = DB::table('s_default')
                ->where('ACC1InternalID', $ACC[0])
                ->where('ACC2InternalID', $ACC[1])
                ->where('ACC3InternalID', $ACC[2])
                ->where('ACC4InternalID', $ACC[3])
                ->where('ACC5InternalID', $ACC[4])
                ->where('ACC6InternalID', $ACC[5])
                ->first();
        //cek coa ada di default atau tidak
        $depreciation1 = DB::table('m_depreciation_header')
                ->where('ACC1InternalIDDebet', $ACC[0])
                ->where('ACC2InternalIDDebet', $ACC[1])
                ->where('ACC3InternalIDDebet', $ACC[2])
                ->where('ACC4InternalIDDebet', $ACC[3])
                ->where('ACC5InternalIDDebet', $ACC[4])
                ->where('ACC6InternalIDDebet', $ACC[5])
                ->first();
        //cek coa ada di master depreciation atau tidak
        $depreciation2 = DB::table('m_depreciation_header')
                ->where('ACC1InternalIDCredit', $ACC[0])
                ->where('ACC2InternalIDCredit', $ACC[1])
                ->where('ACC3InternalIDCredit', $ACC[2])
                ->where('ACC4InternalIDCredit', $ACC[3])
                ->where('ACC5InternalIDCredit', $ACC[4])
                ->where('ACC6InternalIDCredit', $ACC[5])
                ->first();
        //cek coa ada di master depreciation atau tidak
        $gdepreciation1 = DB::table('m_groupdepreciation')
                ->where('ACC1InternalIDDebet', $ACC[0])
                ->where('ACC2InternalIDDebet', $ACC[1])
                ->where('ACC3InternalIDDebet', $ACC[2])
                ->where('ACC4InternalIDDebet', $ACC[3])
                ->where('ACC5InternalIDDebet', $ACC[4])
                ->where('ACC6InternalIDDebet', $ACC[5])
                ->first();
        //cek coa ada di master group depreciation atau tidak
        $gdepreciation2 = DB::table('m_groupdepreciation')
                ->where('ACC1InternalIDCredit', $ACC[0])
                ->where('ACC2InternalIDCredit', $ACC[1])
                ->where('ACC3InternalIDCredit', $ACC[2])
                ->where('ACC4InternalIDCredit', $ACC[3])
                ->where('ACC5InternalIDCredit', $ACC[4])
                ->where('ACC6InternalIDCredit', $ACC[5])
                ->first();
        //cek coa ada di master group depreciation atau tidak
        if (is_null($slip) && is_null($journal) && is_null($inventoryType) && is_null($gdepreciation2) && is_null($gdepreciation1) && is_null($depreciation2) && is_null($depreciation1) && is_null($default)) {
            //tidak ada maka boleh dihapus
            if ($coa->CompanyInternalID == Auth::user()->Company->InternalID) {
                $coa->delete();
                return View::make('coa.coa')
                                ->withMessages('suksesDelete')
                                ->withToogle('accounting')->withAktif('coa');
            } else {
                return View::make('coa.coa')
                                ->withMessages('accessDenied')
                                ->withToogle('accounting')->withAktif('coa');
            }
        } else {
            //ada maka tidak dihapus
            return View::make('coa.coa')
                            ->withMessages('gagalDelete')
                            ->withToogle('accounting')->withAktif('coa');
        }
    }

     public function reportCoaView(){
        $coa = Coa::find(Input::get('InternalID'));
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $totalSO = 0;
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Account Summary Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span>';
        $hitung = 0;
        $tampTotalDebet = 0;
        $tampTotalCredit = 0;
        if (JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                        ->where("ACC1InternalID", $coa->ACC1InternalID)
                        ->where("ACC2InternalID", $coa->ACC2InternalID)
                        ->where("ACC3InternalID", $coa->ACC3InternalID)
                        ->where("ACC4InternalID", $coa->ACC4InternalID)
                        ->where("t_journal_detail.ACC5InternalID", $coa->ACC5InternalID)
                        ->where("t_journal_detail.ACC6InternalID", $coa->ACC6InternalID)
                        ->where('JournalDate', '>=', $start)
                        ->where('JournalDate', '<=', $end)
                        ->where('Status',0)
                        ->count() > 0) {
            $initialBalance = (JournalDetail::getJournalInitialBalanceCoa($coa, $start));
            $html .= '<table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=9>' . $coa->COAName . ', initial balance : ' . $initialBalance . '</th>
                                        </tr>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Journal ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Currency</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Rate</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Notes</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Debet </th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Credit </th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Debet (Rp)</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Credit (Rp)</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            foreach (JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                    ->where("ACC1InternalID", $coa->ACC1InternalID)
                    ->where("ACC2InternalID", $coa->ACC2InternalID)
                    ->where("ACC3InternalID", $coa->ACC3InternalID)
                    ->where("ACC4InternalID", $coa->ACC4InternalID)
                    ->where("t_journal_detail.ACC5InternalID", $coa->ACC5InternalID)
                    ->where("t_journal_detail.ACC6InternalID", $coa->ACC6InternalID)
                    ->where('JournalDate', '>=', $start)
                    ->where('JournalDate', '<=', $end)
                    ->where('Status',0)
                    ->where('t_journal_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->get() as $dataCoa) {
                $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . JournalHeader::find($dataCoa->JournalInternalID)->JournalID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime(JournalHeader::find($dataCoa->JournalInternalID)->JournalDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . Currency::find($dataCoa->CurrencyInternalID)->CurrencyName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($dataCoa->CurrencyRate, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $dataCoa->Notes . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($dataCoa->JournalDebetMU, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($dataCoa->JournalCreditMU, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($dataCoa->JournalDebet, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($dataCoa->JournalCredit, '2', '.', ',') . '</td>
                            </tr>';
                $tampTotalDebet += $dataCoa->JournalDebet;
                $tampTotalCredit += $dataCoa->JournalCredit;
                $hitung++;
            }
            $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right" colspan="7">Total </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($tampTotalDebet, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($tampTotalCredit, '2', '.', ',') . '</td>
                            </tr>';
            $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right" colspan="7">Initial Balance </td>
                                
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right" colspan="2">' . number_format($initialBalance, '2', '.', ',') . '</td>
                                
                            </tr>';
            $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right" colspan="7">End Balance </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right" colspan="2">' . number_format($initialBalance + $tampTotalDebet - $tampTotalCredit, '2', '.', ',') . '</td>
                                
                            </tr>';
            $html .= '</tbody>
                        </table>';
        }

        if ($hitung == 0) {
            $html .= '<br><span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no journal from this account.</span>';
        }
        $html .= '</div>
                    </div>
                </body>
            </html>';
        return $html;
    }
    
    public function reportCoa() {
        $coa = Coa::find(Input::get('InternalID'));
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $totalSO = 0;
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Account Summary Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span>';
        $hitung = 0;
        $tampTotalDebet = 0;
        $tampTotalCredit = 0;
        if (JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                        ->where("ACC1InternalID", $coa->ACC1InternalID)
                        ->where("ACC2InternalID", $coa->ACC2InternalID)
                        ->where("ACC3InternalID", $coa->ACC3InternalID)
                        ->where("ACC4InternalID", $coa->ACC4InternalID)
                        ->where("t_journal_detail.ACC5InternalID", $coa->ACC5InternalID)
                        ->where("t_journal_detail.ACC6InternalID", $coa->ACC6InternalID)
                        ->where('JournalDate', '>=', $start)
                        ->where('JournalDate', '<=', $end)
                        ->count() > 0) {
            $initialBalance = (JournalDetail::getJournalInitialBalanceCoa($coa, $start));
            $html .= '<table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=9>' . $coa->COAName . ', initial balance : ' . $initialBalance . '</th>
                                        </tr>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Journal ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Currency</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Rate</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Notes</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Debet </th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Credit </th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Debet (Rp)</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Credit (Rp)</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            foreach (JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                    ->where("ACC1InternalID", $coa->ACC1InternalID)
                    ->where("ACC2InternalID", $coa->ACC2InternalID)
                    ->where("ACC3InternalID", $coa->ACC3InternalID)
                    ->where("ACC4InternalID", $coa->ACC4InternalID)
                    ->where("t_journal_detail.ACC5InternalID", $coa->ACC5InternalID)
                    ->where("t_journal_detail.ACC6InternalID", $coa->ACC6InternalID)
                    ->where('JournalDate', '>=', $start)
                    ->where('JournalDate', '<=', $end)
                    ->where('t_journal_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->get() as $dataCoa) {
                $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . JournalHeader::find($dataCoa->JournalInternalID)->JournalID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime(JournalHeader::find($dataCoa->JournalInternalID)->JournalDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . Currency::find($dataCoa->CurrencyInternalID)->CurrencyName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($dataCoa->CurrencyRate, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $dataCoa->Notes . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($dataCoa->JournalDebetMU, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($dataCoa->JournalCreditMU, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($dataCoa->JournalDebet, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($dataCoa->JournalCredit, '2', '.', ',') . '</td>
                            </tr>';
                $tampTotalDebet += $dataCoa->JournalDebet;
                $tampTotalCredit += $dataCoa->JournalCredit;
                $hitung++;
            }
            $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right" colspan="7">Total </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($tampTotalDebet, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($tampTotalCredit, '2', '.', ',') . '</td>
                            </tr>';
            $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right" colspan="7">Initial Balance </td>
                                
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right" colspan="2">' . number_format($initialBalance, '2', '.', ',') . '</td>
                                
                            </tr>';
            $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right" colspan="7">End Balance </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right" colspan="2">' . number_format($initialBalance + $tampTotalDebet - $tampTotalCredit, '2', '.', ',') . '</td>
                                
                            </tr>';
            $html .= '</tbody>
                        </table>';
        }

        if ($hitung == 0) {
            $html .= '<br><span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no journal from this account.</span>';
        }
        $html .= '</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('account_summary');
    }

    public function reportCoaExcel() {
        Excel::create('Account_Summary_Report', function($excel) {
            $excel->sheet('Account_Summary_Report', function($sheet) {
                $coa = Coa::find(Input::get('InternalID'));
                $startT = explode('-', Input::get('sDate'));
                $endT = explode('-', Input::get('eDate'));
                $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
                $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
                $totalSO = 0;
                $hitung = 0;
                $data = JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                        ->where("ACC1InternalID", $coa->ACC1InternalID)
                        ->where("ACC2InternalID", $coa->ACC2InternalID)
                        ->where("ACC3InternalID", $coa->ACC3InternalID)
                        ->where("ACC4InternalID", $coa->ACC4InternalID)
                        ->where("t_journal_detail.ACC5InternalID", $coa->ACC5InternalID)
                        ->where("t_journal_detail.ACC6InternalID", $coa->ACC6InternalID)
                        ->where('JournalDate', '>=', $start)
                        ->where('JournalDate', '<=', $end)
                        ->get();
                $sheet->mergeCells('B1:J1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Account Summary Report");
                $sheet->mergeCells('B2:J2');
                $sheet->setCellValueByColumnAndRow(1, 2, "Account : " . $coa->COAName);
                $sheet->mergeCells('B3:J3');
                $sheet->setCellValueByColumnAndRow(1, 3, "Initial Balance : " . JournalDetail::getJournalInitialBalanceCoa($coa, $start));
                $sheet->mergeCells('B4:J4');
                $sheet->setCellValueByColumnAndRow(1, 4, "Period : " . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))));
                $sheet->setCellValueByColumnAndRow(1, 5, "Journal ID");
                $sheet->setCellValueByColumnAndRow(2, 5, "Date");
                $sheet->setCellValueByColumnAndRow(3, 5, "Currency");
                $sheet->setCellValueByColumnAndRow(4, 5, "Rate");
                $sheet->setCellValueByColumnAndRow(5, 5, "Notes");
                $sheet->setCellValueByColumnAndRow(6, 5, "Debet");
                $sheet->setCellValueByColumnAndRow(7, 5, "Credit");
                $sheet->setCellValueByColumnAndRow(8, 5, "Debet (Rp)");
                $sheet->setCellValueByColumnAndRow(9, 5, "Credit (Rp)");
                $row = 6;
                $tampTotalDebet = 0;
                $tampTotalCredit = 0;
                if (count($data) > 0) {
                    foreach ($data as $dataCoa) {
                        $sheet->setCellValueByColumnAndRow(1, $row, JournalHeader::find($dataCoa->JournalInternalID)->JournalID);
                        $sheet->setCellValueByColumnAndRow(2, $row, date("d-M-Y", strtotime(JournalHeader::find($dataCoa->JournalInternalID)->JournalDate)));
                        $sheet->setCellValueByColumnAndRow(3, $row, Currency::find($dataCoa->CurrencyInternalID)->CurrencyName);
                        $sheet->setCellValueByColumnAndRow(4, $row, number_format($dataCoa->CurrencyRate, '2', '.', ','));
                        $sheet->setCellValueByColumnAndRow(5, $row, $dataCoa->Notes);
                        $sheet->setCellValueByColumnAndRow(6, $row, number_format($dataCoa->JournalDebetMU, '2', '.', ','));
                        $sheet->setCellValueByColumnAndRow(7, $row, number_format($dataCoa->JournalCreditMU, '2', '.', ','));
                        $sheet->setCellValueByColumnAndRow(8, $row, number_format($dataCoa->JournalDebet, '2', '.', ','));
                        $sheet->setCellValueByColumnAndRow(9, $row, number_format($dataCoa->JournalCredit, '2', '.', ','));
                        $row++;
                        $tampTotalDebet += $dataCoa->JournalDebet;
                        $tampTotalCredit += $dataCoa->JournalCredit;
                    }
                } else {
                    $sheet->mergeCells('B' . $row . ':J' . $row);
                    $sheet->setCellValueByColumnAndRow(1, $row, "There is no journal from this account.");
                    $row++;
                }

                $sheet->mergeCells('B' . $row . ':J' . $row);
                $sheet->mergeCells('B' . ($row + 1) . ':H' . ($row + 1));
                $sheet->setCellValueByColumnAndRow(1, $row + 1, "Total ");
                $sheet->setCellValueByColumnAndRow(7, $row + 1, number_format($tampTotalDebet, '2', '.', ','));
                $sheet->setCellValueByColumnAndRow(8, $row + 1, number_format($tampTotalCredit, '2', '.', ','));

                $sheet->setBorder('B2:J' . ($row + 1), 'thin');
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B' . ($row + 1), function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('right');
                    $cells->setFontWeight('bold');
                });
                $sheet->cells('B5:J5', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                });
                $sheet->cells('E6:J' . ($row + 1), function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('right');
                });
            });
        })->export('xls');
    }
    
    public function exportExcel() {
        Excel::create('COA_Master', function($excel) {
            $excel->sheet('COA_Master', function($sheet) {
                $sheet->mergeCells('B1:I1');
                $sheet->setCellValueByColumnAndRow(1, 1, "COA Master");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Account Name");
                $sheet->setCellValueByColumnAndRow(3, 2, "Account ID");
                $sheet->setCellValueByColumnAndRow(4, 2, "Type");
                $sheet->setCellValueByColumnAndRow(5, 2, "Stock");
                $sheet->setCellValueByColumnAndRow(6, 2, "Record");
                $sheet->setCellValueByColumnAndRow(7, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(8, 2, "Remark");
                $row = 3;
                foreach (Coa::showCoa() as $data) {
                    $tipe = '';
                    if ($data->Flag == 0) {
                        $tipe = 'Balance';
                    } else {
                        $tipe = 'Profit and Loss';
                    }
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, $data->COAName);
                    $sheet->setCellValueByColumnAndRow(3, $row, "`" . Coa::formatCoa($data->ACC1ID, $data->ACC2ID, $data->ACC3ID, $data->ACC4ID, $data->ACC5ID, $data->ACC6ID, '0'));
                    $sheet->setCellValueByColumnAndRow(4, $row, $tipe);
                    $sheet->setCellValueByColumnAndRow(5, $row, $data->Persediaan);
                    $sheet->setCellValueByColumnAndRow(6, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(7, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(8, $row, $data->Remark);
                    $row++;
                }

                if (Coa::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count() <= 0) {
                    $sheet->mergeCells('B3:I3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "There is no COA Master");
                    $sheet->cells('B3:I3', function($cells) {
                        $cells->setAlignment('center');
                    });
                    $sheet->setBorder('B3:I' . $row, 'thin');
                }

                $row--;
                $sheet->setBorder('B2:I' . $row, 'thin');
                $sheet->cells('B2:I2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:I' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
                $sheet->cells('F3:F' . $row, function($cells) {
                    $cells->setAlignment('right');
                });
            });
        })->export('xls');
    }

}
