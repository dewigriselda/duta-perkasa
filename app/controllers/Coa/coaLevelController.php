<?php

class CoaLevelController extends BaseController {

    public function showCoaLevel() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertCoa1') {
                return Coa1Controller::insertCoa1();
            }
            if (Input::get('jenis') == 'insertCoa2') {
                return Coa2Controller::insertCoa2();
            }
            if (Input::get('jenis') == 'insertCoa3') {
                return Coa3Controller::insertCoa3();
            }
            if (Input::get('jenis') == 'insertCoa4') {
                return Coa4Controller::insertCoa4();
            }
            if (Input::get('jenis') == 'updateCoa1') {
                return Coa1Controller::updateCoa1();
            }
            if (Input::get('jenis') == 'updateCoa2') {
                return Coa2Controller::updateCoa2();
            }
            if (Input::get('jenis') == 'updateCoa3') {
                return Coa3Controller::updateCoa3();
            }
            if (Input::get('jenis') == 'updateCoa4') {
                return Coa4Controller::updateCoa4();
            }
            if (Input::get('jenis') == 'deleteCoa1') {
                return Coa1Controller::deleteCoa1();
            }
            if (Input::get('jenis') == 'deleteCoa2') {
                return Coa2Controller::deleteCoa2();
            }
            if (Input::get('jenis') == 'deleteCoa3') {
                return Coa3Controller::deleteCoa3();
            }
            if (Input::get('jenis') == 'deleteCoa4') {
                return Coa4Controller::deleteCoa4();
            }
        }
        return View::make('coa.coaLevel')
                        ->withToogle('accounting')->withAktif('coaLevel');
    }

    public function exportExcel() {
        Excel::create('COA_Level', function($excel) {
            $excel->sheet('COA_Level', function($sheet) {
                $sheet->mergeCells('B1:E1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Coa Level 1-4");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Account Name");
                $sheet->setCellValueByColumnAndRow(3, 2, "Account ID");
                $sheet->setCellValueByColumnAndRow(4, 2, "Account Level");
                $row = 3;
                foreach (Coa1::orderBy('ACC1ID', 'asc')->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, $data->ACC1Name);
                    $sheet->setCellValueByColumnAndRow(3, $row, "`" . $data->ACC1ID);
                    $sheet->setCellValueByColumnAndRow(4, $row, 1);
                    $row++;
                    foreach (Coa2::coa2inCoa1($data->ACC1ID) as $data2) {
                        $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                        $sheet->setCellValueByColumnAndRow(2, $row, $data2->ACC2Name);
                        $sheet->setCellValueByColumnAndRow(3, $row, "`" . $data2->ACC2ID);
                        $sheet->setCellValueByColumnAndRow(4, $row, 2);
                        $row++;
                        foreach (Coa3::coa3inCoa2($data2->ACC2ID) as $data3) {
                            $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                            $sheet->setCellValueByColumnAndRow(2, $row, $data3->ACC3Name);
                            $sheet->setCellValueByColumnAndRow(3, $row, "`" . $data3->ACC3ID);
                            $sheet->setCellValueByColumnAndRow(4, $row, 3);
                            $row++;
                            foreach (Coa4::coa4inCoa3($data3->ACC3ID) as $data4) {
                                $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                                $sheet->setCellValueByColumnAndRow(2, $row, $data4->ACC4Name);
                                $sheet->setCellValueByColumnAndRow(3, $row, "`" . $data4->ACC4ID);
                                $sheet->setCellValueByColumnAndRow(4, $row, 4);
                                $row++;
                            }
                        }
                    }
                }
                if (Coa1::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() <= 0) {
                    $sheet->mergeCells('B3:E3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "No data available in table");

                    $sheet->cells('B3:E3', function($cells) {
                        $cells->setAlignment('center');
                    });
                    
                    $sheet->setBorder('B3:E' . $row, 'thin');
                }
                
                $row--;
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B2:E2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B3:E' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
                $sheet->cells('D3:D' . $row, function($cells) {
                    $cells->setAlignment('left');
                });
                $sheet->cells('E3:E' . $row, function($cells) {
                    $cells->setAlignment('right');
                });
                $sheet->setBorder('B2:E' . $row, 'thin');
            });
        })->export('xls');
    }

}
