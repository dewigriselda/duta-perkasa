<?php

class CostController extends BaseController {

    public function showCost() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertCost') {
                return $this->insertCost();
            } else if (Input::get('jenis') == 'updateCost') {
                return $this->updateCost();
            } else if (Input::get('jenis') == 'deleteCost') {
                return $this->deleteCost();
            } else if (Input::get('jenis') == 'cost') {
                return $this->reportCost();
            } else if (Input::get('jenis') == 'costMU') {
                return $this->reportCostMU();
            }
        }
        return View::make('coa.cost')
                        ->withToogle('accounting')->withAktif('cost');
    }

    public static function insertCost() {
        //rule
        $rule = array(
            'CostID' => 'required|max:200|unique:m_cost,CostID,NULL,CostID,CompanyInternalID,' . Auth::user()->Company->InternalID . '',
            'CostName' => 'required|max:200',
            'Type' => 'required',
            'coa' => 'required',
            'currency' => 'required',
            'remark' => 'required|max:1000'
        );
        $messages = array(
            'coa.required' => 'Account field is required.'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule, $messages);
        if ($validator->fails()) {
            //tidak valid
            return View::make('coa.cost')
                            ->withMessages('gagalInsert')
                            ->withError($validator->messages())
                            ->withToogle('accounting')->withAktif('cost')
                            ->withErrors($validator);
        } else {
            //valid
            $cost = new Cost;
            $cost->CostID = Input::get('CostID');
            $cost->CostName = Input::get('CostName');
            $coa = Coa::find(Input::get('coa'));
            $cost->ACC1InternalID = $coa->ACC1InternalID;
            $cost->ACC2InternalID = $coa->ACC2InternalID;
            $cost->ACC3InternalID = $coa->ACC3InternalID;
            $cost->ACC4InternalID = $coa->ACC4InternalID;
            $cost->ACC5InternalID = $coa->ACC5InternalID;
            $cost->ACC6InternalID = $coa->ACC6InternalID;
            $cost->Flag = Input::get('Type');
            $cost->CurrencyInternalID = Input::get('currency');
            $cost->UserRecord = Auth::user()->UserID;
            $cost->CompanyInternalID = Auth::user()->Company->InternalID;
            $cost->UserModified = "0";
            $cost->Remark = Input::get('remark');
            $cost->save();

            return View::make('coa.cost')
                            ->withMessages('suksesInsert')
                            ->withToogle('accounting')->withAktif('cost');
        }
    }

    static function updateCost() {
        //rule
        $rule = array(
            'CostName' => 'required|max:200',
            'Type' => 'required',
            'coa' => 'required',
            'currency' => 'required',
            'remark' => 'required|max:1000'
        );
        $messages = array(
            'coa.required' => 'Account field is required.'
        );

        //validasi
        $validator = Validator::make(Input::all(), $rule, $messages);
        if ($validator->fails()) {
            //tidak valid
            return View::make('coa.cost')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('accounting')->withAktif('cost');
        } else {
            //valid
            $cost = Cost::find(Input::get('InternalID'));
            if ($cost->CompanyInternalID == Auth::user()->Company->InternalID) {
                $cost->CostName = Input::get('CostName');
                $coa = Coa::find(Input::get('coa'));
                $cost->ACC1InternalID = $coa->ACC1InternalID;
                $cost->ACC2InternalID = $coa->ACC2InternalID;
                $cost->ACC3InternalID = $coa->ACC3InternalID;
                $cost->ACC4InternalID = $coa->ACC4InternalID;
                $cost->ACC5InternalID = $coa->ACC5InternalID;
                $cost->ACC6InternalID = $coa->ACC6InternalID;
                $cost->Flag = Input::get('Type');
                $cost->CurrencyInternalID = Input::get('currency');
                $cost->UserModified = Auth::user()->UserID;
                $cost->Remark = Input::get('remark');
                $cost->save();
                return View::make('coa.cost')
                                ->withMessages('suksesUpdate')
                                ->withToogle('accounting')->withAktif('cost');
            } else {
                return View::make('coa.cost')
                                ->withMessages('accessDenied')
                                ->withToogle('accounting')->withAktif('cost');
            }
        }
    }

    static function deleteCost() {
        $journal = DB::table('t_journal_header')->where('CostInternalID', Input::get('InternalID'))->first();
        //cek cost ada di pakai atau tidak
        if (is_null($journal)) {
            //tidak ada maka boleh dihapus
            $cost = Cost::find(Input::get('InternalID'));
            if ($cost->CompanyInternalID == Auth::user()->Company->InternalID) {
                $cost->delete();
                return View::make('coa.cost')
                                ->withMessages('suksesDelete')
                                ->withToogle('accounting')->withAktif('cost');
            } else {
                return View::make('coa.cost')
                                ->withMessages('accessDenied')
                                ->withToogle('accounting')->withAktif('cost');
            }
        } else {
            //ada maka tidak dihapus
            return View::make('coa.cost')
                            ->withMessages('gagalDelete')
                            ->withToogle('accounting')->withAktif('cost');
        }
    }

    public function exportExcel() {
        Excel::create('Master_Cost', function($excel) {
            $excel->sheet('Master_Cost', function($sheet) {
                $sheet->mergeCells('B1:K1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Master Cost");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Cost Name");
                $sheet->setCellValueByColumnAndRow(3, 2, "Cost ID");
                $sheet->setCellValueByColumnAndRow(4, 2, "Account Name");
                $sheet->setCellValueByColumnAndRow(5, 2, "Account ID");
                $sheet->setCellValueByColumnAndRow(6, 2, "Type");
                $sheet->setCellValueByColumnAndRow(7, 2, "Currency");
                $sheet->setCellValueByColumnAndRow(8, 2, "Record");
                $sheet->setCellValueByColumnAndRow(9, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(10, 2, "Remark");
                $row = 3;
                foreach (Cost::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                    $coa = Cost::coa($data->InternalID);
                    $data->coaID = $coa[0]->COAInternalID;
                    $currency = Cost::find($data->InternalID)->Currency;
                    $tipe = '';
                    if ($data->Flag == 0) {
                        $tipe = 'Biaya Impor';
                    } else if ($data->Flag == 1) {
                        $tipe = 'Harga Barang';
                    } else if ($data->Flag == 2) {
                        $tipe = 'Piutang giro';
                    } else {
                        $tipe = 'Hutang giro';
                    }
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, $data->CostName);
                    $sheet->setCellValueByColumnAndRow(3, $row, "`" . $data->CostID);
                    $sheet->setCellValueByColumnAndRow(4, $row, $coa[0]->COAName);
                    $sheet->setCellValueByColumnAndRow(5, $row, "`" . Coa::formatCoa($data->ACC1InternalID, $data->ACC2InternalID, $data->ACC3InternalID, $data->ACC4InternalID, $data->ACC5InternalID, $data->ACC6InternalID, 1));
                    $sheet->setCellValueByColumnAndRow(6, $row, $tipe);
                    $sheet->setCellValueByColumnAndRow(7, $row, $currency->CurrencyName);
                    $sheet->setCellValueByColumnAndRow(8, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(9, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(10, $row, $data->Remark);
                    $row++;
                }

                if (Cost::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() <= 0) {
                    $sheet->mergeCells('B3:K3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "No data available in table");

                    $sheet->cells('B3:K3', function($cells) {
                        $cells->setAlignment('center');
                    });

                    $sheet->setBorder('B3:K' . $row, 'thin');
                }


                $row--;
                $sheet->setBorder('B2:K' . $row, 'thin');
                $sheet->cells('B2:K2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:K' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
            });
        })->export('xls');
    }

    public function reportCost() {
        $Initialdebet = 0;
        $Initialkredit = 0;
        $debet = 0;
        $kredit = 0;
        $Totaldebet = 0;
        $Totalkredit = 0;
        $cost = Cost::find(Input::get('cost'));
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $DetailData = Cost::reportCost($cost, $start, $end);
        $Initial = Cost::reportCostInitial($cost, $start, $end);
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Cost Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Cost  ' . $cost->CostID . ' ' . $cost->CostName . '</span><br>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Account ' . Coa::formatCoa($cost->ACC1InternalID, $cost->ACC2InternalID, $cost->ACC3InternalID, $cost->ACC4InternalID, $cost->ACC5InternalID, $cost->ACC6InternalID, 1) . ' ' . Coa::find(Coa::getInternalID($cost->ACC1InternalID, $cost->ACC2InternalID, $cost->ACC3InternalID, $cost->ACC4InternalID, $cost->ACC5InternalID, $cost->ACC6InternalID))->COAName . '</span><br>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span><br><br>';
        if (count($DetailData) <= 0) {
            $html.= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;"> There is no cost activity in this table</span>';
        } else {
            $html.= '<table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th width=15% style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Debet MU</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Credit MU</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Currency</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; width: 10%;">From</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; width: 10%;">Transaction</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Debet</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Credit</th>
                                        </tr>
                                    </thead>
                                    <tbody>';

            foreach ($DetailData as $journal) {
                $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($journal->JournalHeader->JournalDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($journal->JournalDebetMU, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($journal->JournalCreditMU, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: left">' . $journal->Currency->CurrencyName . ' ('.number_format($journal->CurrencyRate, '2', '.', ',').')</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: left; width: 10%;">' . $journal->JournalFrom . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: left; width: 10%;">' . ($journal->TransactionID == '' ? $journal->Notes : $journal->TransactionID) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format(($journal->JournalDebet), '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format(($journal->JournalCredit), '2', '.', ',') . '</td>
                            </tr>';
                $debet += $journal->JournalDebet;
                $kredit += $journal->JournalCredit;
            }
            foreach ($Initial as $journal) {
                $Initialdebet += $journal->JournalDebet;
                $Initialkredit += $journal->JournalCredit;
            }
            $Totaldebet = $Initialdebet + $debet;
            $Totalkredit = $Initialkredit + $kredit;

            $currency = Currency::where('CompanyInternalID', Auth::user()->Company->InternalID)->where('Default', 1)->first();

            $html.= '
            <tr>
                <td colspan="8" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: left"><hr/></td>
            </tr>
            
            <tr>
                <td colspan="4" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700;"></td>
                <td colspan="2" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: left"> Currency Default </td>
                <td colspan="2" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right">' . $currency->CurrencyID . ' ' . $currency->CurrencyName . '</td>
            </tr>
            
            <tr>
                <td colspan="4" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700;"></td>
                <td colspan = "2" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: left">Initial Balance </td>
                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right">' . ($Initialdebet <= $Initialkredit ? number_format(0, '2', '.', ',') : number_format(($Initialdebet - $Initialkredit), '2', '.', ',')) . '</td>
                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right">' . '(' . ($Initialdebet >= $Initialkredit ? number_format(0, '2', '.', ',') : number_format(($Initialkredit - $Initialdebet), '2', '.', ',')) . ')' . '</td>
            </tr>
            
            <tr>
                <td colspan="4" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700;"></td>
                <td colspan = "2" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: left"> Mutation </td>
                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right">' . number_format($debet, '2', '.', ',') . '</td>
                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right">' . '(' . number_format($kredit, '2', '.', ',') . ')' . '</td>
            </tr>
            
            <tr>
                <td colspan="4" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700;"></td>
                <td colspan = "2" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: left"> End Balance </td>
                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right">' . ($Totaldebet <= $Totalkredit ? number_format(0, '2', '.', ',') : number_format(($Totaldebet - $Totalkredit), '2', '.', '.')) . '</td>
                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right">' . '(' . ($Totaldebet >= $Totalkredit ? number_format(0, '2', '.', ',') : number_format(($Totalkredit - $Totaldebet), '2', '.', ',')) . ')' . '</td>
            </tr>
                            ';
            $html.= '</tbody>
            </table>';
        }
        $html.='
                    </div>
                </body>
            </html>';
        return PDF::load($html, 'A4', 'potrait')->download('cost_report');
    }

    public function reportCostMU() {
        $cost = Cost::find(Input::get('cost'));
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $default = $cost->Currency->Default;
        $dataCost = JournalDetail::reportCostMU($cost, $start, $end, $default);
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Cost Report MU</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Cost ' . $cost->CostID . ' ' . $cost->CostName . '</span><br>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Account ' . Coa::formatCoa($cost->ACC1InternalID, $cost->ACC2InternalID, $cost->ACC3InternalID, $cost->ACC4InternalID, $cost->ACC5InternalID, $cost->ACC6InternalID, 1) . ' ' . Coa::find(Coa::getInternalID($cost->ACC1InternalID, $cost->ACC2InternalID, $cost->ACC3InternalID, $cost->ACC4InternalID, $cost->ACC5InternalID, $cost->ACC6InternalID))->COAName . '</span><br>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span><br><br>';
        if (count($dataCost) > 0) {
            $html.= '<table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th width=15% style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">From</th>
                                            <th width=15% style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Transaction</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Currency</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Debet</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Credit</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            $totalDebet = 0;
            $totalCredit = 0;
            foreach ($dataCost as $data) {
                $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->JournalDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->JournalFrom . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . ($data->TransactionID == '' ? $data->Notes : $data->TransactionID) . '</td>'
                        . '     <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $cost->Currency->CurrencyName . '</td>';
                if ($default == 0) {
                    $html.= '<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->JournalDebetMU, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">(' . number_format($data->JournalCreditMU, '2', '.', ',') . ')</td>
                            </tr>';
                    $totalDebet += $data->JournalDebetMU;
                    $totalCredit += $data->JournalCreditMU;
                } else {
                    $html.= '<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->JournalDebet, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">(' . number_format($data->JournalCredit, '2', '.', ',') . ')</td>
                            </tr>';
                    $totalDebet += $data->JournalDebet;
                    $totalCredit += $data->JournalCredit;
                }
            }
            $initialDebet = JournalDetail::reportCostInitialDebet($cost, $start, $default);
            $initialCredit = JournalDetail::reportCostInitialCredit($cost, $start, $default);
            if ($initialDebet - $initialCredit < 0) {
                $initialCredit = ($initialDebet - $initialCredit) * -1;
                $initialDebet = 0;
            } else {
                $initialDebet = $initialDebet - $initialCredit;
                $initialCredit = 0;
            }
            $endDebet = $initialDebet + $totalDebet;
            $endCredit = $initialCredit + $totalCredit;
            if ($endDebet - $endCredit < 0) {
                $endCredit = $endDebet - $endCredit * -1;
                $endDebet = 0;
            } else {
                $endDebet = $endDebet - $endCredit;
                $endCredit = 0;
            }
            $html.= '<tr>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: right;" colspan=6><hr></td>
                    </tr>
                    <tr>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=3></td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;">Initial Balance</td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: right;">' . number_format($initialDebet, '2', '.', ',') . '</td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: right;">(' . number_format($initialCredit, '2', '.', ',') . ')</td>
                    </tr>
                    <tr>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=3></td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;">Mutation</td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: right;">' . number_format($totalDebet, '2', '.', ',') . '</td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: right;">(' . number_format($totalCredit, '2', '.', ',') . ')</td>
                    </tr>
                    <tr>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=3></td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;">End Balance</td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: right;">' . number_format($endDebet, '2', '.', ',') . '</td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: right;">(' . number_format($endCredit, '2', '.', ',') . ')</td>
                    </tr>
                </tbody>
            </table>';
        } else {
            $html.= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no cost activity in this period.</span><br><br>';
        }
        $html.='
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('cost_reportMU');
    }

}