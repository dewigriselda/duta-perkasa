<?php

class JournalController extends BaseController {

    public function showJournal() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteJournal') {
                return $this->deleteJournal();
            }
            if (Input::get('jenis') == 'report_receivable') {
                return $this->getReceivable();
            }
            if (Input::get('jenis') == 'overdue_payment') {
                return $this->getOverduePaymentReport();
            }
            if (Input::get('jenis') == 'report_aging_piutang') {
                return $this->agingPiutang();
            }
            if (Input::get('jenis') == 'report_aging_hutang') {
                return $this->agingHutang();
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('slipID') != '') {
            $data = JournalHeader::advancedSearch(Input::get('slipID'), Input::get('typeTransaction'), Input::get('startDate'), Input::get('endDate'));
            return View::make('coa.journalSearch')
                            ->withToogle('accounting')->withAktif('journal')
                            ->withData($data);
        }
        return View::make('coa.journal')
                        ->withToogle('accounting')->withAktif('journal');
    }

    public function deleteJournal() {
        //cek apakah ID ada di tabel t_journal_detail atau tidak
        //$journal = DB::table('t_journal_detail')->where('JournalInternalID', Input::get('InternalID'))->first();
        $journal = null;
        if (is_null($journal)) {
            //tidak ada maka data boleh dihapus
            $journal = JournalHeader::find(Input::get('InternalID'));
            if ($journal->CompanyInternalID == Auth::user()->Company->InternalID) {
                //cek journal termasuk transaksi atau tidak
                if ($journal->TransactionID == NULL || substr($journal->TransactionID, 0, 2) == 'PA') {
                    //hapus detil
                    $detilData = JournalHeader::find(Input::get('InternalID'))->journalDetail;
                    foreach ($detilData as $value) {
                        $detil = JournalDetail::find($value->InternalID);
                        $detilCopy = new JournalDetailHistory();
                        $detilCopy->InternalID = $detil->InternalID;
                        $detilCopy->JournalInternalID = $detil->JournalInternalID;
                        $detilCopy->JournalIndex = $detil->JournalIndex;
                        $detilCopy->JournalNotes = $detil->JournalNotes;
                        $detilCopy->JournalDebet = $detil->JournalDebet;
                        $detilCopy->JournalCredit = $detil->JournalCredit;
                        $detilCopy->CurrencyInternalID = $detil->CurrencyInternalID;
                        $detilCopy->CurrencyRate = $detil->CurrencyRate;
                        $detilCopy->JournalDebetMU = $detil->JournalDebetMU;
                        $detilCopy->JournalCreditMU = $detil->JournalCreditMU;
                        $detilCopy->JournalTransactionID = $detil->JournalTransactionID;
                        $detilCopy->ACC1InternalID = $detil->ACC1InternalID;
                        $detilCopy->ACC2InternalID = $detil->ACC2InternalID;
                        $detilCopy->ACC3InternalID = $detil->ACC3InternalID;
                        $detilCopy->ACC4InternalID = $detil->ACC4InternalID;
                        $detilCopy->ACC5InternalID = $detil->ACC5InternalID;
                        $detilCopy->ACC6InternalID = $detil->ACC6InternalID;
                        $detilCopy->COAName = $detil->COAName;
                        $detilCopy->dtRecord = $detil->dtRecord;
                        $detilCopy->dtModified = $detil->dtModified;
                        $detilCopy->UserRecord = $detil->UserRecord;
                        $detilCopy->UserModified = $detil->UserModified;
                        $detilCopy->Remark = $detil->Remark;
                        $detilCopy->save();
                        $detil->delete();
                    }
                    //hapus journal
                    $journal = JournalHeader::find(Input::get('InternalID'));
                    $journal->Status = 1;
                    $journal->save();
                    $messages = 'suksesDelete';
                } else {
                    $messages = 'journalTransaksi';
                }
            } else {
                Session::flash('messages', $messages);
                $messages = 'accessDenied';
            }
        } else {
            //ada maka data tidak boleh dihapus
            $messages = 'gagalDelete';
        }
        $data = JournalHeader::advancedSearch(Input::get('slipID'), Input::get('typeTransaction'), Input::get('startDate'), Input::get('endDate'));
        return View::make('coa.journalSearch')
                        ->withToogle('accounting')->withAktif('journal')
                        ->withMessages($messages)
                        ->withData($data);
    }

    public function journalDetail($id) {
        $id = JournalHeader::getIdjournal($id);
        $header = JournalHeader::find($id);
        $detail = JournalHeader::find($id)->journalDetail()
                        ->orderBy(DB::raw('JournalIndex = 1000'))
                        ->orderBy('JournalTransactionID', 'asc')->get();
        if (Input::get('jenis') == 'report_receivable') {
            return $this->getReceivable();
        }
        if ($header->Status == 1) {
            $detail = JournalDetailHistory::where('JournalInternalID', $id)
                            ->orderBy(DB::raw('JournalIndex = 1000'))
                            ->orderBy('JournalTransactionID', 'asc')->get();
        }
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            return View::make('coa.journalDetail')
                            ->withToogle('accounting')->withAktif('journal')
                            ->withHeader($header)
                            ->withDetail($detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showJournal');
        }
    }

    public function journalNew($jenis) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'report_receivable') {
                return $this->getReceivable();
            } else {
                return $this->insertJournal($jenis);
            }
        }
        $cari = $this->createTextCari($jenis, 0);
        $tipe = $this->getTipe($jenis);
        return View::make('coa.journalNew')
                        ->withToogle('accounting')->withAktif('journal')
                        ->withJenis($jenis)
                        ->withTipe($tipe)
                        ->withJournal($cari);
    }

    public function journalNewMemorial() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'report_receivable') {
                return $this->getReceivable();
            } else {
                return $this->insertJournalMemorial();
            }
        }
        $cari = $this->createTextCari('Memorial', 0);
        $tipe = $this->getTipe('Memorial');
        return View::make('coa.journalNewMemorial')
                        ->withToogle('accounting')->withAktif('journal')
                        ->withJenis('Memorial')
                        ->withTipe($tipe)
                        ->withJournal($cari);
    }

    public function journalUpdate($id) {
        $id = JournalHeader::getIdjournal($id);
        $header = JournalHeader::find($id);
        if ($header->TransactionID == NULL || substr($header->TransactionID, 0, 2) == 'PA') {
            if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
                if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                    if (Input::get('jenis') == 'report_receivable') {
                        return $this->getReceivable();
                    } else {
                        return $this->updateJournal($id);
                    }
                }
                $detail = JournalHeader::find($id)->journalDetail()
                                ->orderBy(DB::raw('JournalIndex = 1000'))
                                ->orderBy('JournalTransactionID', 'asc')->get();
                $cari = $this->createTextCari($header->JournalType, 0);
                $tipe = $this->getTipe($header->JournalType);
                return View::make('coa.journalUpdate')
                                ->withToogle('accounting')->withAktif('journal')
                                ->withHeader($header)
                                ->withDetail($detail)
                                ->withTipe($tipe)
                                ->withJournal($cari);
            } else {
                $messages = 'accessDenied';
                Session::flash('messages', $messages);
                return Redirect::Route('showJournal');
            }
        } else {
            $messages = 'journalTransaksi';
            Session::flash('messages', $messages);
            return Redirect::Route('showJournal');
        }
    }

    public function insertJournal($jenis) {
        //rule
        $rule = array(
            'date' => 'required',
            'from' => 'required|max:1000',
            'journalNotes' => 'required|max:2000',
            'slip' => 'required',
            'department' => 'required',
            'coa5' => 'required',
            'remark' => 'max:1000',
            'currency' => 'required',
            'rate' => 'required',
            'coa' => 'required',
            'notes' => 'required'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalInsert';
            $error = $validator->messages();
        } else {
            //valid
            $tampTransactionID = '';
            //insert header
            $header = new JournalHeader;
            $slip = explode('---;---', Input::get('slip'));
            $cari = $this->createTextCari($jenis, 1);
            $date = explode('-', Input::get('date'));
            $yearDigit = substr($date[2], 2);
            $cari .= '-' . $date[1] . $yearDigit;
            $tipe = $this->getTipe($jenis);
            $tampSlipID = Slip::find($slip[0]);
            $header->JournalID = JournalHeader::getNextIDJournal($cari . '-' . $tampSlipID->SlipID . '-');
            $header->JournalDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $header->JournalType = Input::get('journalType');
            $header->JournalFrom = Input::get('from');
            $header->Notes = Input::get('journalNotes');
            $header->DepartmentInternalID = Input::get('department');
            $header->BankInternalID = Input::get('bank');
            $header->ACC6InternalID = Input::get('coa6');
            $header->SlipInternalID = $slip[0];
            $header->TransactionID = NULL;
            $header->ACC5InternalID = Input::get('coa5');
            $header->Lock = '0';
            $header->Check = '0';
            $header->Flag = '0';
            $header->UserRecord = Auth::user()->UserID;
            $header->CompanyInternalID = Auth::user()->Company->InternalID;
            $header->UserModified = '0';
            $header->Remark = Input::get('remark');
            $header->save();

            if ($jenis == 'Piutang Giro' || $jenis == 'Hutang Giro') {
                $header->GiroNumber = Input::get('giroNumber');
                $header->GiroDate = date('Y-m-d', strtotime(Input::get('giroDate')));
                $header->GiroDueDate = date('Y-m-d', strtotime(Input::get('giroDueDate')));
                $header->save();
            }

            //insert detail
            $tampKredit = 0;
            $tampDebet = 0;
            $tampRate = 0;
            $tampNomor = 0;
            $countAdaJournalSesuai = 0;
            for ($a = 0; $a < count(Input::get('coa')); $a++) {
                $debetValue = str_replace(',', '', Input::get('Debet_value')[$a]);
                $kreditValue = str_replace(',', '', Input::get('Kredit_value')[$a]);
                if ($debetValue != 0 || $kreditValue != 0) {
                    $detail = new JournalDetail();
                    $detail->JournalInternalID = $header->InternalID;
                    $detail->JournalIndex = $a + 1;
                    $detail->JournalNotes = Input::get('notes')[$a];
                    $detail->JournalDebetMU = $debetValue;
                    $detail->JournalCreditMU = $kreditValue;
                    $currency = explode('---;---', Input::get('currency'));
                    $detail->CurrencyInternalID = $currency[0];
                    $detail->CurrencyRate = str_replace(',', '', Input::get('rate'));
                    $detail->JournalDebet = $debetValue * str_replace(',', '', Input::get('rate'));
                    $tampDebet += $debetValue;
                    $detail->JournalCredit = $kreditValue * str_replace(',', '', Input::get('rate'));
                    $tampKredit += $kreditValue;
                    $tampRate = str_replace(',', '', Input::get('rate'));
                    if (Input::get('JournalTransactionID')[$a] == '-1') {
                        $detail->JournalTransactionID = NULL;
                    } else {
                        $detail->JournalTransactionID = Input::get('JournalTransactionID')[$a];
                        $tampTransactionID .= ', ' . Input::get('JournalTransactionID')[$a];
                    }
                    $coa = Coa::find(Input::get('coa')[$a]);
                    $detail->ACC1InternalID = $coa->ACC1InternalID;
                    $detail->ACC2InternalID = $coa->ACC2InternalID;
                    $detail->ACC3InternalID = $coa->ACC3InternalID;
                    $detail->ACC4InternalID = $coa->ACC4InternalID;
                    $detail->ACC5InternalID = $coa->ACC5InternalID;
                    $detail->ACC6InternalID = $coa->ACC6InternalID;
                    $detail->COAName = $coa->COAName;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = '0';
                    $detail->save();
                    $tampNomor = $a;
                    $countAdaJournalSesuai++;
                }
            }
            if ($tampTransactionID != '') {
                $tampTransactionID = substr($tampTransactionID, 1);
                $header = JournalHeader::find($header->InternalID);
                $header->TransactionID = 'PA ' . $tampTransactionID;
                $header->save();
            }
            if ($tampKredit >= $tampDebet) {
                $tampDebet = $tampKredit - $tampDebet;
                $tampKredit = 0;
            } else {
                $tampKredit = $tampDebet - $tampKredit;
                $tampDebet = 0;
            }
            if ($countAdaJournalSesuai != 0) {
                $detail = new JournalDetail();
                $detail->JournalInternalID = $header->InternalID;
                $detail->JournalIndex = 1000;
                $detail->JournalNotes = 'Journal Auto ' . Input::get('journalNotes');
                $detail->JournalDebetMU = $tampDebet;
                $detail->JournalCreditMU = $tampKredit;
                $currency = explode('---;---', Input::get('currency'));
                $detail->CurrencyInternalID = $currency[0];
                $detail->CurrencyRate = str_replace(',', '', Input::get('rate'));
                $detail->JournalDebet = $tampDebet * str_replace(',', '', Input::get('rate'));
                $detail->JournalCredit = $tampKredit * str_replace(',', '', Input::get('rate'));
                $detail->JournalTransactionID = NULL;
                $coa = Slip::coa($slip[0]);
                $coa = Coa::find($coa[0]->COAInternalID);
                $detail->ACC1InternalID = $coa->ACC1InternalID;
                $detail->ACC2InternalID = $coa->ACC2InternalID;
                $detail->ACC3InternalID = $coa->ACC3InternalID;
                $detail->ACC4InternalID = $coa->ACC4InternalID;
                $detail->ACC5InternalID = $coa->ACC5InternalID;
                $detail->ACC6InternalID = $coa->ACC6InternalID;
                $detail->COAName = $coa->COAName;
                $detail->UserRecord = Auth::user()->UserID;
                $detail->UserModified = '0';
                $detail->save();
            }

            $messages = 'suksesInsert';
            $error = '';
        }
        $cari = $this->createTextCari($jenis, 0);
        $tipe = $this->getTipe($jenis);
        return View::make('coa.journalNew')
                        ->withToogle('accounting')->withAktif('journal')
                        ->withJenis($jenis)
                        ->withTipe($tipe)
                        ->withJournal($cari)
                        ->withError($error)
                        ->withMessages($messages);
    }

    public function updateJournal($id) {
        //tipe
        $headerUpdate = JournalHeader::find($id);
        $detailUpdate = JournalHeader::find($id)->journalDetail()->orderBy('JournalIndex', 'asc')->get();
        $cari = $this->createTextCari($headerUpdate->JournalType, 0);
        $tipe = $this->getTipe($headerUpdate->JournalType);

        //rule
        $rule = array(
            'from' => 'required|max:1000',
            'journalNotes' => 'required|max:2000',
            'department' => 'required',
            'coa5' => 'required',
            'remark' => 'required|max:1000',
            'currency' => 'required',
            'rate' => 'required',
            'coa' => 'required',
            'notes' => 'required'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalUpdate';
            $error = $validator->messages();
        } else {
            $tampTransactionID = '';

            $header = JournalHeader::find(Input::get('JournalInternalID'));
            $header->JournalFrom = Input::get('from');
            $header->Notes = Input::get('journalNotes');
            $header->DepartmentInternalID = Input::get('department');
            $header->TransactionID = NULL;
            $header->ACC5InternalID = Input::get('coa5');
            $header->UserModified = Auth::user()->UserID;
            $header->Remark = Input::get('remark');
            $header->save();

            //delete journal detail -- nantinya insert ulang
            JournalDetail::where('JournalInternalID', '=', Input::get('JournalInternalID'))->update(array('is_deleted' => 1));
            //insert ulang
            $tampKredit = 0;
            $tampDebet = 0;
            $tampRate = 0;
            $tampNomor = 0;
            $countAdaJournalSesuai = 0;
            for ($a = 0; $a < count(Input::get('coa')); $a++) {
                $debetValue = str_replace(',', '', Input::get('Debet_value')[$a]);
                $kreditValue = str_replace(',', '', Input::get('Kredit_value')[$a]);
                if ($debetValue != 0 || $kreditValue != 0) {
                    $detail = new JournalDetail();
                    $detail->JournalInternalID = Input::get('JournalInternalID');
                    $detail->JournalIndex = $a + 1;
                    $detail->JournalNotes = Input::get('notes')[$a];
                    $detail->JournalDebetMU = $debetValue;
                    $detail->JournalCreditMU = $kreditValue;
                    $currency = explode('---;---', Input::get('currency'));
                    $detail->CurrencyInternalID = $currency[0];
                    $detail->CurrencyRate = str_replace(',', '', Input::get('rate'));
                    $detail->JournalDebet = $debetValue * str_replace(',', '', Input::get('rate'));
                    $tampDebet += $debetValue;
                    $detail->JournalCredit = $kreditValue * str_replace(',', '', Input::get('rate'));
                    $tampKredit += $kreditValue;
                    $tampRate = str_replace(',', '', Input::get('rate'));
                    if (Input::get('JournalTransactionID')[$a] == '-1') {
                        $detail->JournalTransactionID = NULL;
                    } else {
                        $detail->JournalTransactionID = Input::get('JournalTransactionID')[$a];
                        $tampTransactionID .= ', ' . Input::get('JournalTransactionID')[$a];
                    }
                    $coa = Coa::find(Input::get('coa')[$a]);
                    $detail->ACC1InternalID = $coa->ACC1InternalID;
                    $detail->ACC2InternalID = $coa->ACC2InternalID;
                    $detail->ACC3InternalID = $coa->ACC3InternalID;
                    $detail->ACC4InternalID = $coa->ACC4InternalID;
                    $detail->ACC5InternalID = $coa->ACC5InternalID;
                    $detail->ACC6InternalID = $coa->ACC6InternalID;
                    $detail->COAName = $coa->COAName;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = Auth::user()->UserID;
                    $detail->save();
                    $tampNomor = $a;
                    $countAdaJournalSesuai++;
                }
            }
            if ($tampTransactionID != '') {
                $tampTransactionID = substr($tampTransactionID, 1);
                $header = JournalHeader::find(Input::get('JournalInternalID'));
                $header->TransactionID = 'PA ' . $tampTransactionID;
                $header->save();
            }
            if ($tampKredit >= $tampDebet) {
                $tampDebet = $tampKredit - $tampDebet;
                $tampKredit = 0;
            } else {
                $tampKredit = $tampDebet - $tampKredit;
                $tampDebet = 0;
            }
            if ($tipe == 'Memo') {
                $countAdaJournalSesuai = 0;
            }
            if ($countAdaJournalSesuai != 0) {
                $detail = new JournalDetail();
                $detail->JournalInternalID = Input::get('JournalInternalID');
                $detail->JournalIndex = 1000;
                $detail->JournalNotes = 'Journal Auto ' . Input::get('journalNotes');
                $detail->JournalDebetMU = $tampDebet;
                $detail->JournalCreditMU = $tampKredit;
                $currency = explode('---;---', Input::get('currency'));
                $detail->CurrencyInternalID = $currency[0];
                $detail->CurrencyRate = str_replace(',', '', Input::get('rate'));
                $detail->JournalDebet = $tampDebet * str_replace(',', '', Input::get('rate'));
                $detail->JournalCredit = $tampKredit * str_replace(',', '', Input::get('rate'));
                $detail->JournalTransactionID = NULL;
                $slip = explode('---;---', Input::get('slip'));
                $coa = Slip::coa($slip[0]);
                $coa = Coa::find($coa[0]->COAInternalID);
                $detail->ACC1InternalID = $coa->ACC1InternalID;
                $detail->ACC2InternalID = $coa->ACC2InternalID;
                $detail->ACC3InternalID = $coa->ACC3InternalID;
                $detail->ACC4InternalID = $coa->ACC4InternalID;
                $detail->ACC5InternalID = $coa->ACC5InternalID;
                $detail->ACC6InternalID = $coa->ACC6InternalID;
                $detail->COAName = $coa->COAName;
                $detail->UserRecord = Auth::user()->UserID;
                $detail->UserModified = Auth::user()->UserID;
                $detail->save();
            }

            JournalDetail::where('JournalInternalID', '=', Input::get('JournalInternalID'))->where('is_deleted', 1)->delete();
            $messages = 'suksesUpdate';
            $error = '';
        }

        //tipe
        $header = JournalHeader::find($id);
        $detail = JournalHeader::find($id)->journalDetail()
                        ->orderBy(DB::raw('JournalIndex = 1000'))
                        ->orderBy('JournalTransactionID', 'asc')->get();
        $cari = $this->createTextCari($header->JournalType, 0);
        $tipe = $this->getTipe($header->JournalType);
        return View::make('coa.journalUpdate')
                        ->withToogle('accounting')->withAktif('journal')
                        ->withHeader($header)
                        ->withDetail($detail)
                        ->withTipe($tipe)
                        ->withJournal($cari)
                        ->withError($error)
                        ->withMessages($messages);
    }

    public function insertJournalMemorial() {
        //rule
        $rule = array(
            'date' => 'required',
            'from' => 'required|max:1000',
            'journalNotes' => 'required|max:2000',
            'department' => 'required',
            'coa5' => 'required',
            'remark' => 'required|max:1000',
            'currency' => 'required',
            'rate' => 'required',
            'coa' => 'required',
            'notes' => 'required'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalInsert';
            $error = $validator->messages();
        } else {
            //valid
            //insert header
            $header = new JournalHeader;
            $cari = $this->createTextCari('Memorial', 1);
            $date = explode('-', Input::get('date'));
            $yearDigit = substr($date[2], 2);
            $cari .= '-' . $date[1] . $yearDigit;
            $tipe = $this->getTipe('Memorial');
            $header->JournalID = JournalHeader::getNextIDJournal($cari . '-');
            $header->JournalDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $header->JournalType = Input::get('journalType');
            $header->JournalFrom = Input::get('from');
            $header->Notes = Input::get('journalNotes');
            $header->DepartmentInternalID = Input::get('department');
            $header->SlipInternalID = Null;
            $header->TransactionID = NULL;
            $header->ACC5InternalID = Input::get('coa5');
            $header->Lock = '0';
            $header->Check = '0';
            $header->Flag = '0';
            $header->UserRecord = Auth::user()->UserID;
            $header->CompanyInternalID = Auth::user()->Company->InternalID;
            $header->UserModified = '0';
            $header->Remark = Input::get('remark');
            $header->save();

            //insert detail
            $tampKredit = 0;
            $tampDebet = 0;
            $tampRate = 0;
            $tampNomor = 0;
            for ($a = 0; $a < count(Input::get('coa')); $a++) {
                $debetValue = str_replace(',', '', Input::get('Debet_value')[$a]);
                $kreditValue = str_replace(',', '', Input::get('Kredit_value')[$a]);
                if ($debetValue != 0 || $kreditValue != 0) {
                    $detail = new JournalDetail();
                    $detail->JournalInternalID = $header->InternalID;
                    $detail->JournalIndex = $a + 1;
                    $detail->JournalNotes = Input::get('notes')[$a];
                    $detail->JournalDebetMU = $debetValue;
                    $detail->JournalCreditMU = $kreditValue;
                    $currency = explode('---;---', Input::get('currency'));
                    $detail->CurrencyInternalID = $currency[0];
                    $detail->CurrencyRate = str_replace(',', '', Input::get('rate'));
                    $detail->JournalDebet = $debetValue * str_replace(',', '', Input::get('rate'));
                    $tampDebet += $debetValue;
                    $detail->JournalCredit = $kreditValue * str_replace(',', '', Input::get('rate'));
                    $tampKredit += $kreditValue;
                    $tampRate = str_replace(',', '', Input::get('rate'));
                    $detail->JournalTransactionID = NULL;
                    $coa = Coa::find(Input::get('coa')[$a]);
                    $detail->ACC1InternalID = $coa->ACC1InternalID;
                    $detail->ACC2InternalID = $coa->ACC2InternalID;
                    $detail->ACC3InternalID = $coa->ACC3InternalID;
                    $detail->ACC4InternalID = $coa->ACC4InternalID;
                    $detail->ACC5InternalID = $coa->ACC5InternalID;
                    $detail->ACC6InternalID = $coa->ACC6InternalID;
                    $detail->COAName = $coa->COAName;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = '0';
                    $detail->save();
                    $tampNomor = $a;
                }
            }
            $messages = 'suksesInsert';
            $error = '';
        }
        $cari = $this->createTextCari('Memorial', 0);
        $tipe = $this->getTipe('Memorial');
        return View::make('coa.journalNewMemorial')
                        ->withToogle('accounting')->withAktif('journal')
                        ->withJenis('Memorial')
                        ->withTipe($tipe)
                        ->withJournal($cari)
                        ->withError($error)
                        ->withMessages($messages);
    }

    function createTextCari($jenis, $tipe) {
        if ($jenis == 'Bank In' || $jenis == 'Cash In') {
            if ($jenis == 'Bank In') {
                $cari = 'BI';
            } else {
                $cari = 'CI';
            }
        } else if ($jenis == 'Bank Out' || $jenis == 'Cash Out') {
            if ($jenis == 'Bank Out') {
                $cari = 'BO';
            } else {
                $cari = 'CO';
            }
        } else if ($jenis == 'Hutang Giro' || $jenis == 'Piutang Giro') {
            if ($jenis == 'Hutang Giro') {
                $cari = 'HG';
            } else {
                $cari = 'PG';
            }
        } else {
            $cari = 'ME';
        }
        if ($tipe == 0) {
            $cari .= '-' . date('m') . date('y');
        }
        return $cari;
    }

    function getTipe($jenis) {
        if ($jenis == 'Bank In' || $jenis == 'Cash In') {
            $tipe = 'In';
        } else if ($jenis == 'Bank Out' || $jenis == 'Cash Out') {
            $tipe = 'Out';
        } else if ($jenis == 'Piutang Giro' || $jenis == 'Hutang Giro') {
            $tipe = 'Giro';
        } else {
            $tipe = 'Memo';
        }
        return $tipe;
    }

    public function formatCariIDJournalMemorial() {
        $date = explode('-', Input::get('date'));
        $tipe = Input::get('tipe');
        $cari = $this->createTextCari($tipe, 1);
        $yearDigit = substr($date[2], 2);
        $cari .= '-' . $date[1] . $yearDigit;
        echo JournalHeader::getNextIDJournal($cari . '-');
    }

    public function formatCariIDJournal() {
        $date = explode('-', Input::get('date'));
        $tipe = Input::get('tipe');
        $cari = $this->createTextCari($tipe, 1);
        $yearDigit = substr($date[2], 2);
        $cari .= '-' . $date[1] . $yearDigit;
        $tampSlip = explode('---;---', Input::get('slip'));
        $slipSelected = $tampSlip[0];
        foreach (Slip::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $slip) {
            ?>
            <option <?php if ($slip->InternalID == $slipSelected) echo 'selected' ?> value="<?php echo $slip->InternalID . '---;---' . JournalHeader::getNextIDJournal($cari . '-' . $slip->SlipID . '-'); ?>">
                <?php echo $slip->SlipID . ' ' . $slip->SlipName ?>
            </option>
            <?php
        }
    }

    public function agingHutang() {
        Excel::create('PayableAge_Report', function($excel) {
            $excel->sheet('PayableAge_Report', function($sheet) {
                $sdate = explode("-", Input::get('sDate'));
                $edate = explode("-", Input::get('eDate'));
                $startDate = $sdate[2] . "-" . $sdate[1] . "-" . $sdate[0];
                $endDate = $edate[2] . "-" . $edate[1] . "-" . $edate[0];

                $sheet->mergeCells('B1:I1');
                $sheet->cells('B1:I1', function($cells) {
                    $cells->setFontWeight('bold');
                });
                $sheet->setCellValueByColumnAndRow(1, 1, "Payable Age By Supplier");
                $row = 2;
                $sTotal = 0;
                $customer = Coa6::where('Type', 's')->get();
                if (count($customer) > 0) {
                    foreach ($customer as $cust) {
//                        $detail = PurchaseHeader::getPurchasePayable3Param($startDate, $endDate, $cust->InternalID);
                        $detail = PurchaseAddHeader::getPurchasePayableSupplier($startDate, $endDate, $cust->InternalID);
                        $detailReturn = PurchaseReturnHeader::advancedSearch("-1", "", Input::get('sDate'), Input::get('eDate'));
                        if (count($detail) > 0) {
                            $sheet->mergeCells("B" . $row . ":G" . $row);
                            $sheet->cells("B" . $row . ":G" . $row, function($cells) {
                                $cells->setFontWeight("bold");
                            });
                            $sheet->setCellValueByColumnAndRow(1, $row, "Supplier : (" . $cust->ACC6ID . ") " . $cust->ACC6Name);
                            $sheet->setBorder('B' . $row . ':G' . $row, 'thin');
                            $row++;

                            $sheet->setcellValueByColumnAndRow(1, $row, "Purchase Date");
                            $sheet->setcellValueByColumnAndRow(2, $row, "Purchase Payable Number");
                            $sheet->setcellValueByColumnAndRow(3, $row, "Purchase Payable Age");
                            $sheet->setcellValueByColumnAndRow(4, $row, "Grand Total");
                            $sheet->setcellValueByColumnAndRow(5, $row, "Balance");
                            $sheet->setcellValueByColumnAndRow(6, $row, "Paid Off");
                            $sheet->setBorder('B' . $row . ':G' . $row, 'thin');
                            $row++;
                            $balance = 0;
                            $jumlah = 0;
                            foreach ($detail as $data) {
                                $tgl1 = date_create(date("d-m-Y", strtotime($data->PurchaseDate)));
                                $tgl2 = date_create(date("d-m-Y"));
                                $diff = date_diff($tgl1, $tgl2);
                                $sheet->setcellValueByColumnAndRow(1, $row, date("d-m-Y", strtotime($data->Date)));
                                $sheet->setcellValueByColumnAndRow(2, $row, $data->PurchaseID);
                                $sheet->setcellValueByColumnAndRow(3, $row, $diff->format("%a days"));
                                $sheet->setcellValueByColumnAndRow(4, $row, number_format($data->GrandTotal, 2, '.', ','));
                                $sheet->setcellValueByColumnAndRow(5, $row, number_format($data->GrandTotal - JournalDetail::where('JournalTransactionID', $data->ID)->sum('JournalCreditMU'), '2', '.', ','));
                                $sheet->setcellValueByColumnAndRow(6, $row, number_format($data->GrandTotal - ($data->GrandTotal - JournalDetail::where('JournalTransactionID', $data->ID)->sum('JournalCreditMU')), '2', '.', ','));
                                $sheet->setBorder('B' . $row . ':G' . $row, 'thin');
                                $row++;
                                $jumlah += $data->GrandTotal;
                                $balance += $data->GrandTotal - JournalDetail::where('JournalTransactionID', $data->ID)->sum('JournalCreditMU');
                            }
                            $sheet->mergeCells('B' . $row . ':E' . $row);
                            $sheet->setcellValueByColumnAndRow(1, $row, "Total Balance");
                            $sheet->setcellValueByColumnAndRow(5, $row, number_format($balance, 2, ".", ","));
                            $sheet->setBorder('B' . $row . ':G' . $row, 'thin');
                            $row += 2;
                            $sTotal += $balance;
                        }
                    }
                }
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });

                $sheet->mergeCells('B' . $row . ':E' . $row);
                $sheet->setcellValueByColumnAndRow(1, $row, "Summary Total");
                $sheet->setcellValueByColumnAndRow(5, $row, number_format($sTotal, 2, ".", ","));
                $sheet->setBorder('B' . $row . ':G' . $row, 'thin');
            });
        })->export('xls');
    }

    public function agingPiutang() {
        Excel::create('Aging_Piutang_Report', function($excel) {
            $excel->sheet('Aging_Piutang_Report', function($sheet) {
                $tempo = Input::get('tempo');
                $edate = explode("-", Input::get('eDate'));
                $endDate = $edate[2] . "-" . $edate[1] . "-" . $edate[0];
                $sTotal = 0;

                $sheet->mergeCells('B1:M1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Aging Piutang Report");
                $sheet->cells('B1:M1', function($cells) {
                    $cells->setFontWeight('bold');
                });
                $sheet->setCellValueByColumnAndRow(1, 2, "Period :");
                $sheet->setCellValueByColumnAndRow(2, 2, date("d-M-Y", strtotime(Input::get('eDate'))));

                $hitung = 0;
                $i = 1;
                $today = date('Y-m-d');
                $collect = array();
                $long = 0;
                $col1 = 0;
                $col2 = 0;
                $col3 = 0;
                $col4 = 0;
                $col5 = 0;
                $col6 = 0;
                $col7 = 0;
                $totalAmount = 0;
                $totalSeluruh = 0;

                $row = 4;
                $i = 1;

                $customer = Coa6::where('Type', "c")->get();
                if (count($customer) > 0) {
                    foreach ($customer as $cust) {
                        $collect = array();
                        $col1 = 0;
                        $col2 = 0;
                        $col3 = 0;
                        $col4 = 0;
                        $col5 = 0;
                        $col6 = 0;
                        $col7 = 0;
                        $tSebelum = 0;
                        $tSesudah = 0;
                        $tAll = 0;
                        $totalAmount = 0;
                        if ($tempo == 'all') {
                            $detail = SalesHeader::join('m_coa6', 'm_coa6.InternalID', '=', 't_sales_header.ACC6InternalID')
                                    ->where('t_sales_header.ACC6InternalID', $cust->InternalID)
                                    ->whereRaw("CAST(t_sales_header.SalesDate as Date) <= '" . $endDate . "'")
                                    ->get();
                        } else if ($tempo == 'sebelum') {
                            $detail = SalesHeader::join('m_coa6', 'm_coa6.InternalID', '=', 't_sales_header.ACC6InternalID')
                                    ->where('t_sales_header.ACC6InternalID', $cust->InternalID)
                                    ->whereRaw("CAST(t_sales_header.SalesDate as Date) <= '" . $endDate . "' AND (DATEDIFF('" . $endDate . "', t_sales_header.SalesDate) <= t_sales_header.LongTermCustomer)")
                                    ->get();
                        } else if ($tempo == 'sesudah') {
                            $detail = SalesHeader::join('m_coa6', 'm_coa6.InternalID', '=', 't_sales_header.ACC6InternalID')
                                    ->where('t_sales_header.ACC6InternalID', $cust->InternalID)
                                    ->whereRaw("CAST(t_sales_header.SalesDate as Date) <= '" . $endDate . "' AND (DATEDIFF('" . $endDate . "', t_sales_header.SalesDate) > t_sales_header.LongTermCustomer)")
                                    ->get();
                        }

                        if (count($detail) > 0) {

                            $arcol = array();
                            $key = 0;
                            $hitung = 0;
                            foreach ($detail as $key => $data) {
                                $col1 = 0;
                                $col2 = 0;
                                $col3 = 0;
                                $col4 = 0;
                                $col5 = 0;
                                $col6 = 0;
                                if ($data->GrandTotal - JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                                                ->where('JournalTransactionID', $data->SalesID)
                                                ->whereRaw("CAST(t_journal_header.JournalDate as Date) <= '" . $endDate . "'")
                                                ->sum('JournalCreditMU') != 0) {
                                    $hitung++;
                                    if ($hitung == 1) {
                                        $sheet->setBorder('B' . $row . ':M' . $row, 'thin');
                                        $sheet->setCellValueByColumnAndRow(1, $row, "ID Customer");
                                        $sheet->setCellValueByColumnAndRow(2, $row, "Customer Name");
                                        $sheet->setCellValueByColumnAndRow(3, $row, "ID SI");
                                        $sheet->setCellValueByColumnAndRow(4, $row, "Date");
                                        $sheet->setCellValueByColumnAndRow(5, $row, "Term");
                                        $sheet->setCellValueByColumnAndRow(6, $row, "0 - 30");
                                        $sheet->setCellValueByColumnAndRow(7, $row, "31 - 60");
                                        $sheet->setCellValueByColumnAndRow(8, $row, "61 - 90");
                                        $sheet->setCellValueByColumnAndRow(9, $row, "91 - 120");
                                        $sheet->setCellValueByColumnAndRow(10, $row, "121 - 150");
                                        $sheet->setCellValueByColumnAndRow(11, $row, "151 - 180");
                                        $sheet->setCellValueByColumnAndRow(12, $row, "> 180");
                                        $sheet->cells('B' . $row . ':M' . $row, function($cells) {
                                            $cells->setBackground('#eaf6f7');
                                            $cells->setValignment('middle');
                                            $cells->setAlignment('center');
                                            $cells->setFontWeight('bold');
                                            $cells->setFontSize('12');
                                        });
                                        $row++;
                                    }

                                    date_default_timezone_set("Asia/Bangkok");
                                    $now = date_create($endDate);
                                    $salesDate = date_create($data->SalesDate);
                                    $diff = $now->diff($salesDate)->format("%a");

                                    if ($diff >= 0 && $diff < 31) {
                                        $col1 = $data->GrandTotal - JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                                                        ->where('JournalTransactionID', $data->SalesID)
                                                        ->whereRaw("CAST(t_journal_header.JournalDate as Date) <= '" . $endDate . "'")
                                                        ->sum('JournalCreditMU');
                                    } else if ($diff >= 31 && $diff < 61) {
                                        $col2 = $data->GrandTotal - JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                                                        ->where('JournalTransactionID', $data->SalesID)
                                                        ->whereRaw("CAST(t_journal_header.JournalDate as Date) <= '" . $endDate . "'")
                                                        ->sum('JournalCreditMU');
                                    } else if ($diff >= 61 && $diff < 91) {
                                        $col3 = $data->GrandTotal - JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                                                        ->where('JournalTransactionID', $data->SalesID)
                                                        ->whereRaw("CAST(t_journal_header.JournalDate as Date) <= '" . $endDate . "'")
                                                        ->sum('JournalCreditMU');
                                    } else if ($diff >= 91 && $diff < 121) {
                                        $col4 = $data->GrandTotal - JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                                                        ->where('JournalTransactionID', $data->SalesID)
                                                        ->whereRaw("CAST(t_journal_header.JournalDate as Date) <= '" . $endDate . "'")
                                                        ->sum('JournalCreditMU');
                                    } else if ($diff >= 121 && $diff < 151) {
                                        $col5 = $data->GrandTotal - JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                                                        ->where('JournalTransactionID', $data->SalesID)
                                                        ->whereRaw("CAST(t_journal_header.JournalDate as Date) <= '" . $endDate . "'")
                                                        ->sum('JournalCreditMU');
                                    } else if ($diff >= 151 && $diff <= 180) {
                                        $col6 = $data->GrandTotal - JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                                                        ->where('JournalTransactionID', $data->SalesID)
                                                        ->whereRaw("CAST(t_journal_header.JournalDate as Date) <= '" . $endDate . "'")
                                                        ->sum('JournalCreditMU');
                                    } else if ($diff > 180) {
                                        $col7 = $data->GrandTotal - JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                                                        ->where('JournalTransactionID', $data->SalesID)
                                                        ->whereRaw("CAST(t_journal_header.JournalDate as Date) <= '" . $endDate . "'")
                                                        ->sum('JournalCreditMU');
                                    }
                                    $totalAmountTemp = $col1 + $col2 + $col3 + $col4 + $col5 + $col6 + $col7;
                                    if ($data->GrandTotal - JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                                                    ->where('JournalTransactionID', $data->SalesID)
                                                    ->whereRaw("CAST(t_journal_header.JournalDate as Date) <= '" . $endDate . "'")
                                                    ->sum('JournalCreditMU') != 0) {

                                        $totalAmount += $col1 + $col2 + $col3 + $col4 + $col5 + $col6 + $col7;
                                        $totalSeluruh += $totalAmountTemp;

                                        $sheet->setBorder('B' . $row . ':M' . $row, 'thin');
                                        $sheet->setCellValueByColumnAndRow(1, $row, $cust->ACC6ID);
                                        $sheet->setCellValueByColumnAndRow(2, $row, $cust->ACC6Name);
                                        $sheet->setCellValueByColumnAndRow(3, $row, $data->SalesID);
                                        $sheet->setCellValueByColumnAndRow(4, $row, date("d-m-Y", strtotime($data->SalesDate)));
                                        $sheet->setCellValueByColumnAndRow(5, $row, $data->LongTerm);
                                        $sheet->setCellValueByColumnAndRow(6, $row, number_format($col1, "0", ".", ","));
                                        $sheet->setCellValueByColumnAndRow(7, $row, number_format($col2, "0", ".", ","));
                                        $sheet->setCellValueByColumnAndRow(8, $row, number_format($col3, "0", ".", ","));
                                        $sheet->setCellValueByColumnAndRow(9, $row, number_format($col4, "0", ".", ","));
                                        $sheet->setCellValueByColumnAndRow(10, $row, number_format($col5, "0", ".", ","));
                                        $sheet->setCellValueByColumnAndRow(11, $row, number_format($col6, "0", ".", ","));
                                        $sheet->setCellValueByColumnAndRow(12, $row, number_format($col6, "0", ".", ","));
                                        $sheet->cells('K' . $row . ':M' . $row, function($cells) {
                                            $cells->setAlignment('right');
                                        });
                                        $row++;
                                    }
                                }
                            }
                            if ($hitung > 0) {
                                $sheet->setBorder('B' . $row . ':M' . $row, 'thin');
                                $sheet->mergeCells('B' . $row . ':L' . $row);
                                $sheet->setCellValueByColumnAndRow(1, $row, "Total :");
                                $sheet->setCellValueByColumnAndRow(12, $row, number_format($totalAmount, '2', '.', ','));
                                $sheet->cells('B' . $row . ':M' . $row, function($cells) {
                                    $cells->setAlignment('right');
                                    $cells->setFontWeight('bold');
                                });
                                $row += 2;
                            }
                        }
                    }
                }
                $sheet->setBorder('B' . $row . ':Q' . $row, 'thin');
                $sheet->mergeCells('B' . $row . ':P' . $row);
                $sheet->setCellValueByColumnAndRow(1, $row, 'Total Amount Seluruh');
                $sheet->setCellValueByColumnAndRow(16, $row, number_format($totalSeluruh, '2', '.', ','));
            });
        })->export('xls');
    }

    function journalPrint($id) {
        $id = JournalHeader::getIdjournal($id);
        $header = JournalHeader::find($id);
        $detail = JournalHeader::find($id)->journalDetail()->get();
        if ($header->Lock == 1) {
            $status = 'Lock';
        } else if ($header->Check == 1) {
            $status = 'Lock and Check';
        } else {
            $status = 'Not lock';
        }
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            if ($header->JournalType == "Memorial") {
                $slip = "-";
                $slipText = "-";
            } else {
                $slip = $header->Slip;
                $slipText = $slip->SlipID . ' ' . $slip->SlipName;
            }
            $department = $header->Department;
            $coa5 = $header->Coa5;
            $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 420px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Journal</h5>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 18px; width: 100%;">
                            <table>
                            <tr>
                            <td width="425px">
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Journal ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $header->JournalID . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->JournalDate)) . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Type</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $header->JournalType . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">From</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $header->JournalFrom . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Status</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $status . '</td>
                                 </tr>
                                </table>
                                </td>
                                <td>
                                <table>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Department</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $department->DepartmentName . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Slip</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $slipText . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Transaction</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . ($header->TransactionID != '' ? $header->TransactionID : '-') . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Coa5</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . ($coa5->ACC5ID == '0' ? : $coa5->ACC5ID . ' ' . $coa5->ACC5Name) . '</td>
                                     </tr>
                                </table></td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Account</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Debet MU</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Credit MU</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Currency</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Rate</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Debet</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Credit</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            $totalDebet = 0;
            $totalCredit = 0;
            if (count($detail) > 0) {
                foreach ($detail as $data) {
                    $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . Coa::formatCoa($data->ACC1InternalID, $data->ACC2InternalID, $data->ACC3InternalID, $data->ACC4InternalID, $data->ACC5InternalID, $data->ACC6InternalID, 1) . ' ' . $data->COAName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->JournalDebetMU, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->JournalCreditMU, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->Currency->CurrencyName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->CurrencyRate, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->JournalDebet, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->JournalCredit, '2', '.', ',') . '</td>
                            </tr>';
                    $totalDebet += $data->JournalDebet;
                    $totalCredit += $data->JournalCredit;
                }
            } else {
                $html .= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;  text-align: center">There is no account journal registered in this journal.</td>
                        </tr>';
            }
            $html .= '<tr>
                                <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;"><hr></td>
                    </tr>
                    <tr>
                                <td colspan="4" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;"></td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: center">Total </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right">' . number_format($totalDebet, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right">' . number_format($totalCredit, '2', '.', ',') . '</td>
                    </tr>
                            </tbody>
                            </table>
                    </div>
                </body>
            </html>';
            return PDF ::load($html, 'A4', 'portrait')->show();
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showSales');
        }
    }

    function getOverduePaymentReport() {
        $CustomerInternalID = "%";
        $nameCustomer = "All";
        if (Input::get("coa6InternalID") != -1) {
            $CustomerInternalID = Input::get("coa6InternalID");
            $nameCustomer = Coa6::find($CustomerInternalID)->ACC6ID . "_" . Coa6::find($CustomerInternalID)->ACC6Name;
        }
        $startT = explode('-', Input::get('sDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];

//        $detail = SalesHeader::where(DB::raw('DATE_ADD(SalesDate,INTERVAL LongTerm DAY) < '.$start))->get();
        $detail = SalesHeader::getOverduePayment($start, $CustomerInternalID);
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 420px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Overdue Payment Report</h5>  
                        <h5 style="font-family: helvetica,sans-serif;font-size: 13px; text-align: center;">Customer : ' . $nameCustomer . '</h5>  
                            <table class="tableBorder" width="100%"  style="margin-top: 3px; clear: both;  top: 18px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Due Date</th>';
        if ($nameCustomer == "All")
            $html.='<th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Customer</th>';
        $html .='<th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales ID</th>
                                          <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">PO Customer</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Balance</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Customer Manager</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales Assistant</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
        if (count($detail) > 0) {
            foreach ($detail as $data) {
                $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-m-Y", strtotime($data->Date)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-m-Y", strtotime("+" . $data->LongTerm . " day", strtotime($data->Date))) . '</td>';
                if ($nameCustomer == "All")
                    $html .='<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . Coa6::find($data->ACC6InternalID)->ACC6Name . '</td>';
                $html .='<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->ID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . SalesOrderHeader::find($data->SalesOrderInternalID)->POCustomer . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->GrandTotal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->GrandTotal - JournalDetail::where('JournalTransactionID', $data->ID)->sum('JournalCreditMU'), '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . SalesMan::find(SalesOrderHeader::find($data->SalesOrderInternalID)->SalesManInternalID)->SalesManName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->GrandTotal - JournalDetail::where('JournalTransactionID', $data->ID)->sum('JournalCreditMU'), '2', '.', ',') . '</td>
                            </tr>';
            }
        } else {
            $html .= '<tr>
                            <td colspan="8" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;  text-align: center">There is no Receivable.</td>
                        </tr>';
        }
        $html .= '               </tbody>
                            </table>
                    </div>
                </body>
            </html>';
        //download and show
        return PDF ::load($html, 'A4', 'portrait')->show('overdue_payment');
    }

    public function getReceivable() {
        $CustomerInternalID = "%";
        $nameCustomer = "All_Supplier_and_Customer";
        if (Input::get("coa6InternalID") != -1) {
            $CustomerInternalID = Input::get("coa6InternalID");
            $nameCustomer = Coa6::find($CustomerInternalID)->ACC6ID . "_" . Coa6::find($CustomerInternalID)->ACC6Name;
        }
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];

        $detail = SalesHeader::getSalesReceivable3Param($start, $end, $CustomerInternalID);
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 100px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <!--<img src = "' . substr(Auth::user()->Company->Logo, 1) . '">-->
                            <div style="width: 420px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Receivable ' . $nameCustomer . ' </h5>  
                            <table class="tableBorder" width="100%"  style="margin-top: 3px; clear: both;  top: 18px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Customer</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Phone</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Due Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Currency</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Balance</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
        if (count($detail) > 0) {
            foreach ($detail as $data) {
                $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->coa6 . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . Coa6::find($data->ACC6InternalID)->Phone . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->ID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-m-Y", strtotime($data->Date)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-m-Y", strtotime("+" . $data->LongTerm . " day", strtotime($data->Date))) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->CurrencyName . ' (' . number_format($data->CurrencyRate, '2', '.', ',') . ')</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->GrandTotal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->GrandTotal - JournalDetail::where('JournalTransactionID', $data->ID)->sum('JournalCreditMU'), '2', '.', ',') . '</td>
                            </tr>';
            }
        } else {
            $html .= '<tr>
                            <td colspan="8" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;  text-align: center">There is no Receivable.</td>
                        </tr>';
        }
        $html .= '               </tbody>
                            </table>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('receivable_report');
    }

    public function getPayable() {
        $detail = PurchaseHeader::getPurchasePayable();
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 420px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Payable</h5>  
                            <table class="tableBorder" width="100%"  style="margin-top: 3px; clear: both;  top: 18px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Supplier</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Phone</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Purchase ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Due Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Currency</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Balance</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
        if (count($detail) > 0) {
            foreach ($detail as $data) {
                $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->coa6 . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . Coa6::find($data->ACC6InternalID)->Phone . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->ID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-m-Y", strtotime($data->Date)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-m-Y", strtotime("+" . $data->LongTerm . " day", strtotime($data->Date))) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->CurrencyName . ' (' . number_format($data->CurrencyRate, '2', '.', ',') . ')</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->GrandTotal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->GrandTotal - JournalDetail::where('JournalTransactionID', $data->ID)->sum('JournalDebetMU'), '2', '.', ',') . '</td>
                            </tr>';
            }
        } else {
            $html .= '<tr>
                            <td colspan="8" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;  text-align: center">There is no Payable.</td>
                        </tr>';
        }
        $html .= '               </tbody>
                            </table>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('payable_report');
    }

    static function journalDataBackup($data) {
        $explode = explode('---;---', $data);
        $slip = $explode[0];
        $jenis = $explode[1];
        $start = $explode[2];
        $end = $explode[3];

        $where = '';
        if ($slip != '-1' && $slip != '') {
            $where .= 'SlipInternalID = "' . $slip . '" ';
        }
        if ($jenis != '-1' && $jenis != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'JournalType = "' . $jenis . '" ';
        }
        if ($start != '' && $end != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $start = explode('-', $start);
            $end = explode('-', $end);
            $startSearch = $start[2] . '-' . $start[1] . '-' . $start[0];
            $endSearch = $end[2] . '-' . $end[1] . '-' . $end[0];
            $where .= 'JournalDate between "' . $startSearch . ' 00:00:00" AND "' . $endSearch . ' 23:59:59"';
        }

        $table = 't_journal_header';
        $primaryKey = 't_journal_header`.`InternalID';
        $columns = array(
            array('db' => 'JournalID', 'dt' => 0),
            array('db' => 'JournalType', 'dt' => 1),
            array(
                'db' => 'JournalDate',
                'dt' => 2,
                'formatter' => function( $d, $row ) {
                    return date("d-m-Y", strtotime($d));
                }
            ),
            array('db' => 'DepartmentName', 'dt' => 3),
            array('db' => 'SlipName', 'dt' => 4),
            array('db' => 'ACC5Name', 'dt' => 5),
            array('db' => 'TransactionID', 'dt' => 6),
            array('db' => 't_journal_header`.`InternalID', 'dt' => 7, 'formatter' => function( $d, $row ) {
                    $data = JournalHeader::find($d);
                    $action = '<td class="text-center">
                                    <a href="' . Route('journalDetail', $data->JournalID) . '">
                                        <button id="btn-' . $data->JournalID . '-detail"
                                                class="btn btn-pure-xs btn-xs btn-detail">
                                            <span class="glyphicon glyphicon-zoom-in"></span>
                                        </button>
                                    </a>';
                    if (($data->TransactionID == 'No Transaction' || substr($data->TransactionID, 0, 2) == 'PA' || $data->TransactionID == '') && $data->Status == 0) {
                        $action .= '<a href="' . Route('journalUpdate', $data->JournalID) . '">
                                        <button id="btn-' . $data->JournalID . '-update"
                                                class="btn btn-pure-xs btn-xs btn-edit">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </button>
                                    </a>';
                        $action .= '<button data-target="#m_journalDelete" data-internal="' . $data->InternalID . '"  data-toggle="modal" role="dialog"
                                           onclick="deleteAttach(this)" data-id="' . $data->JournalID . '" data-name=' . $data->JournalID . ' class="btn btn-pure-xs btn-xs btn-delete">
                                        void
                                    </button>';
                    } else {
                        $action .= '<button disabled class="btn btn-pure-xs btn-xs btn-edit"><span class="glyphicon glyphicon-edit"></span></button>';
                        $action .= '<button disabled class="btn btn-pure-xs btn-xs btn-delete">void</button>';
                    }
                    return $action;
                },
                'field' => 't_journal_header`.`InternalID')
        );

        $sql_details = getConnection();

        require('ssp.class.php');
        $ID_CLIENT_VALUE = Auth::user()->CompanyInternalID;
        if ($where != '') {
            $extraCondition = $where . ' AND ' . 't_journal_header.CompanyInternalID = "' . Auth::user()->Company->InternalID . '" AND t_journal_header.Status = 0';
        } else {
            $extraCondition = 't_journal_header.CompanyInternalID = "' . Auth::user()->Company->InternalID . '" AND t_journal_header.Status = 0';
        }
        $join = ' INNER JOIN m_department d on d.InternalID = t_journal_header.DepartmentInternalID '
                . 'LEFT JOIN m_slip s on s.InternalID = t_journal_header.SlipInternalID '
                . 'left JOIN m_coa5 c on c.InternalID = t_journal_header.ACC5InternalID ';

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $extraCondition, $join));
    }

    static function journalDataBackupVoid($data) {
        $explode = explode('---;---', $data);
        $slip = $explode[0];
        $jenis = $explode[1];
        $start = $explode[2];
        $end = $explode[3];

        $where = '';
        if ($slip != '-1' && $slip != '') {
            $where .= 'SlipInternalID = "' . $slip . '" ';
        }
        if ($jenis != '-1' && $jenis != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'JournalType = "' . $jenis . '" ';
        }
        if ($start != '' && $end != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $start = explode('-', $start);
            $end = explode('-', $end);
            $startSearch = $start[2] . '-' . $start[1] . '-' . $start[0];
            $endSearch = $end[2] . '-' . $end[1] . '-' . $end[0];
            $where .= 'JournalDate between "' . $startSearch . ' 00:00:00" AND "' . $endSearch . ' 23:59:59"';
        }

        $table = 't_journal_header';
        $primaryKey = 't_journal_header`.`InternalID';
        $columns = array(
            array('db' => 'JournalID', 'dt' => 0),
            array('db' => 'JournalType', 'dt' => 1),
            array(
                'db' => 'JournalDate',
                'dt' => 2,
                'formatter' => function( $d, $row ) {
                    return date("d-m-Y", strtotime($d));
                }
            ),
            array('db' => 'DepartmentName', 'dt' => 3),
            array('db' => 'SlipName', 'dt' => 4),
            array('db' => 'ACC5Name', 'dt' => 5),
            array('db' => 'TransactionID', 'dt' => 6),
            array('db' => 't_journal_header`.`InternalID', 'dt' => 7, 'formatter' => function( $d, $row ) {
                    $data = JournalHeader::find($d);
                    $action = '<td class="text-center">
                                    <a href="' . Route('journalDetail', $data->JournalID) . '">
                                        <button id="btn-' . $data->JournalID . '-detail"
                                                class="btn btn-pure-xs btn-xs btn-detail">
                                            <span class="glyphicon glyphicon-zoom-in"></span>
                                        </button>
                                    </a>';
//                    if (($data->TransactionID == 'No Transaction' || substr($data->TransactionID, 0, 2) == 'PA' || $data->TransactionID == '') && $data->Status == 0) {
//                        $action.='<a href="' . Route('journalUpdate', $data->JournalID) . '">
//                                        <button id="btn-' . $data->JournalID . '-update"
//                                                class="btn btn-pure-xs btn-xs btn-edit">
//                                            <span class="glyphicon glyphicon-edit"></span>
//                                        </button>
//                                    </a>';
//                        $action.='<button data-target="#m_journalDelete" data-internal="' . $data->InternalID . '"  data-toggle="modal" role="dialog"
//                                           onclick="deleteAttach(this)" data-id="' . $data->JournalID . '" data-name=' . $data->JournalID . ' class="btn btn-pure-xs btn-xs btn-delete">
//                                        void
//                                    </button>';
//                    } else {
//                        $action.='<button disabled class="btn btn-pure-xs btn-xs btn-edit"><span class="glyphicon glyphicon-edit"></span></button>';
//                        $action.='<button disabled class="btn btn-pure-xs btn-xs btn-delete">void</button>';
//                    }
                    return $action;
                },
                'field' => 't_journal_header`.`InternalID')
        );

        $sql_details = getConnection();

        require('ssp.class.php');
        $ID_CLIENT_VALUE = Auth::user()->CompanyInternalID;
        if ($where != '') {
            $extraCondition = $where . ' AND ' . 't_journal_header.CompanyInternalID = "' . Auth::user()->Company->InternalID . '" AND t_journal_header.Status = 1';
        } else {
            $extraCondition = 't_journal_header.CompanyInternalID = "' . Auth::user()->Company->InternalID . '" AND t_journal_header.Status = 1';
        }
        $join = ' INNER JOIN m_department d on d.InternalID = t_journal_header.DepartmentInternalID '
                . 'LEFT JOIN m_slip s on s.InternalID = t_journal_header.SlipInternalID '
                . 'left JOIN m_coa5 c on c.InternalID = t_journal_header.ACC5InternalID ';

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $extraCondition, $join));
    }

    public function getDataAccount() {
        $jenis = Input::get('jenis');
        $jenisTransaksi = 'Purchase';
        $player = 'Supplier';
        $jenisAccount = 'Payable';
        $dataAccount = PurchaseHeader::getPurchasePayable();
        $dataAccount2 = PurchaseReturnHeader::getPurchaseReturnReceivable();
//        $dataAccount3 = TopUp::getTopUpPayable();
        if ($jenis == 'Bank Out') {
            $dataAccount3 = JournalHeader::getHutangGiro();
        } else {
            $dataAccount3 = array();
        }
        $jenisDebetCredit = 'Debet';
        $jenisDebetCreditRetur = 'Credit';
        if ($jenis == 'Cash In' || $jenis == 'Bank In' || $jenis == 'Piutang Giro') {
            $jenisTransaksi = 'Sales';
            $player = 'Customer';
            $jenisAccount = 'Receivable';
            $dataAccount = SalesHeader::getSalesReceivable();
            $dataAccount2 = SalesReturnHeader::getSalesReturnReceivable();
//            $dataAccount3 = TopUp::getTopUpReceivable();
            $jenisDebetCredit = 'Credit';
            $jenisDebetCreditRetur = 'Debet';
            if ($jenis == 'Bank In') {
                //ambil piutang giro
                $dataAccount3 = JournalHeader::getPiutangGiro();
            } else {
                $dataAccount3 = array();
            }
        }
        $countAccount = 0;
        foreach ($dataAccount as $data) {
            ?>
            <tr>
                <th width="3%" class="text-center">
                    <input class="checkAccount" id="check-<?php echo $countAccount ?>" type="checkbox" value="<?php echo $data->ID ?>">
                </th>
                <td width="12%"><?php echo $data->ID ?></td>
                <td width="15%"><?php echo $data->coa6 ?></td>
                <td width="12%"><?php echo date("d-m-Y", strtotime($data->Date)) ?></td>
                <td width="12%"><?php echo date("d-m-Y", strtotime("+" . $data->LongTerm . " day", strtotime($data->Date))) ?></td>
                <td><?php echo number_format(floor($data->DownPayment * 1.1), '2', '.', ',') ?></td>
                <td><?php echo number_format(floor($data->GrandTotal + ($data->DownPayment * 1.1)), '2', '.', ',') ?></td>
                <td id="check-<?php echo $countAccount ?>-nominal-balance"><?php echo number_format($data->GrandTotal - JournalDetail::where('JournalTransactionID', $data->ID)->sum('Journal' . $jenisDebetCredit . 'MU'), '2', '.', ','); ?></td>
                <td><?php echo $data->CurrencyName ?></td>
            <input id="check-<?php echo $countAccount ?>-currency" type="hidden" value="<?php echo $data->CurrencyInternalID ?>">
            <td><?php echo number_format($data->CurrencyRate, '2', '.', ',') ?></td>
            <input id="check-<?php echo $countAccount ?>-rate" type="hidden" value="<?php echo $data->CurrencyRate ?>">
            <td class='text-right'><input readonly="" type="text" class="numajaDesimal nominal right" name="nominal" value="<?php echo number_format($data->GrandTotal - JournalDetail::where('JournalTransactionID', $data->ID)->sum('Journal' . $jenisDebetCredit . 'MU'), '2', '.', ','); ?>" id="check-<?php echo $countAccount ?>-nominal" style="width:110px"></td>
            </tr>
            <?php
            $countAccount++;
        }
        foreach ($dataAccount2 as $data) {
            $salesIDReal = substr($data->ID, 7);
            if ($jenisTransaksi == 'Sales') {
                $cek = SalesHeader::where('SalesID', $salesIDReal)->pluck('isCash');
            } else {
                $cek = PurchaseHeader::where('PurchaseID', $salesIDReal)->pluck('isCash');
            }
            if ($cek == 1) {
                ?>
                <tr>
                    <th width="3%" class="text-center">
                        <input class="checkAccount" id="check-<?php echo $countAccount ?>" type="checkbox" value="<?php echo $data->ID ?>">
                    </th>
                    <td width="12%" id="check-<?php echo $countAccount ?>-textID"><?php echo $data->ID ?></td>
                    <td width="15%"><?php echo $data->coa6 ?></td>
                    <td width="12%"><?php echo date("d-m-Y", strtotime($data->Date)) ?></td>
                    <td width="12%"><?php echo date("d-m-Y", strtotime("+" . $data->LongTerm . " day", strtotime($data->Date))) ?></td>
                    <td><?php echo number_format(0, '2', '.', ',') ?></td>
                    <td><?php echo number_format($data->GrandTotal, '2', '.', ',') ?></td>
                    <td id="check-<?php echo $countAccount ?>-nominal-balance"><?php echo number_format($data->GrandTotal - JournalDetail::where('JournalTransactionID', $data->ID)->sum('Journal' . $jenisDebetCreditRetur . 'MU'), '2', '.', ','); ?></td>
                    <td><?php echo $data->CurrencyName ?></td>
                <input id="check-<?php echo $countAccount ?>-currency" type="hidden" value="<?php echo $data->CurrencyInternalID ?>">
                <td><?php echo number_format($data->CurrencyRate, '2', '.', ',') ?></td>
                <input id="check-<?php echo $countAccount ?>-rate" type="hidden" value="<?php echo $data->CurrencyRate ?>">
                <td class='text-right'><input readonly="" type="text" class="numajaDesimal nominal right" name="nominal" value="<?php echo number_format($data->GrandTotal - JournalDetail::where('JournalTransactionID', $data->ID)->sum('Journal' . $jenisDebetCredit . 'MU'), '2', '.', ','); ?>" id="check-<?php echo $countAccount ?>-nominal" style="width:110px"></td>
                </tr>
                <?php
                $countAccount++;
            }
        }
        foreach ($dataAccount3 as $data) {
            ?>
            <tr>
                <th width="3%" class="text-center">
                    <input class="checkAccount" data-account="<?php echo $data->COAName ?>" data-coa="<?php echo Coa::getInternalID($data->ACC1InternalID,$data->ACC2InternalID,$data->ACC3InternalID,$data->ACC4InternalID,$data->ACC5InternalID,$data->ACC6InternalID) ?>"  id="check-<?php echo $countAccount ?>" type="checkbox" value="<?php echo $data->JournalID ?>">
                </th>
                <td width="12%"><?php echo $data->ID ?></td>
                <td width="15%"><?php echo $data->coa6 ?></td>
                <td width="12%"><?php echo date("d-m-Y", strtotime($data->Date)) ?></td>
                <td width="12%"><?php echo date("d-m-Y", strtotime($data->DateAkhir)) ?></td>
                <td><?php echo number_format(0, '2', '.', ',') ?></td>
                <td><?php echo number_format($data->GrandTotal, '2', '.', ',') ?></td>
                <td id="check-<?php echo $countAccount ?>-nominal-balance"><?php echo number_format($data->GrandTotal, '2', '.', ',') ?></td>
                <td><?php echo $data->CurrencyName ?></td>
            <input id="check-<?php echo $countAccount ?>-currency" type="hidden" value="<?php echo $data->CurrencyInternalID ?>">
            <td><?php echo number_format($data->CurrencyRate, '2', '.', ',') ?></td>
            <input id="check-<?php echo $countAccount ?>-rate" type="hidden" value="<?php echo $data->CurrencyRate ?>">
            <td class='text-right'><input type="text" class="numajaDesimal nominal right" name="nominal" value="<?php echo number_format($data->GrandTotal - JournalDetail::where('JournalTransactionID', $data->ID)->sum('Journal' . $jenisDebetCredit . 'MU'), '2', '.', ','); ?>" id="check-<?php echo $countAccount ?>-nominal" style="width:110px"></td>
            </tr>
            <?php
            $countAccount++;
        }
    }

}
