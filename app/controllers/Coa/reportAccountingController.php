<?php

class ReportAccountingController extends BaseController {

    public function showReportAccounting() {
        $yearMin = JournalHeader::getYearMin();
        $yearMax = date("Y");
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'balance') {
                return $this->balanceReport();
            }
            if (Input::get('jenis') == 'profitLoss') {
                return $this->profitReport();
            }
            if (Input::get('jenis') == 'general') {
                return $this->generalReport();
            }
            if (Input::get('jenis') == 'detail') {
                return $this->detailReport();
            }
            if (Input::get('jenis') == 'purchasedetail') {
                return $this->purchaseDetailReport();
            }
            if (Input::get('jenis') == 'salesReturn') {
                return $this->salesReturnDetailReport();
            }
            if (Input::get('jenis') == 'purchaseReturn') {
                return $this->purchaseReturnDetailReport();
            }
            if (Input::get('jenis') == 'subdiary_ledger') {
                return $this->reportSubsidiary();
            }
        }
        return View::make('coa.report')
                        ->withToogle('accounting')->withAktif('reportAccounting')
                        ->withYearmin($yearMin)
                        ->withYearmax($yearMax);
    }

    public function balanceReport() {
        $bulan = Input::get('month');
        $tahun = Input::get('year');
        $zero = Input::get('ZeroBalance');
        $date = date("Y-m-d", strtotime("+1 month", strtotime(date($tahun . '-' . str_pad($bulan, 2, '0', STR_PAD_LEFT) . '-01'))));
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:80%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Balance Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("M", strtotime($tahun . '-' . $bulan . '-01')) . ' ' . $tahun . '</span><br><br>';
        $header1 = Coa::where('CompanyInternalID', Auth::user()->CompanyInternalID)
                        ->where('Flag', 0)->select('Header1')
                        ->distinct('Header1')->get();
        foreach ($header1 as $dataHeader1) { //Loop Header 1    
            $tamp = '';
            $count2 = 0;
            $total1 = 0;
            $tamp .= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">' . $dataHeader1->Header1 . '</span><br><br>';
            $header2 = Coa::where('CompanyInternalID', Auth::user()->CompanyInternalID)
                            ->where('Flag', 0)->where('Header1', $dataHeader1->Header1)
                            ->select('Header2')
                            ->distinct('Header2')->get();
            foreach ($header2 as $dataHeader2) { //Loop Header 2 berdasarkan Header 1
                $tamp2 = '';
                $count3 = 0;
                $tamp2 .= '<span style="padding-left:23px;font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">' . $dataHeader2->Header2 . '</span>';
                $total2 = 0;
                $header3 = Coa::where('CompanyInternalID', Auth::user()->CompanyInternalID)
                                ->where('Flag', 0)->where('Header1', $dataHeader1->Header1)
                                ->where('Header2', $dataHeader2->Header2)->select('Header3')
                                ->distinct('Header3')->orderBy('Header3')->get();
                $tamp2 .= '<table class="tableBorder"  style="width: 95%; margin-top: 18px; clear: both; position: relative; left: 3%">';
                foreach ($header3 as $dataHeader3) { //Loop Header 3 berdasarkan Header 1 & 2
                    $debet = Coa::getJournalDebetValue($dataHeader1->Header1, $dataHeader2->Header2, $dataHeader3->Header3, $date);
                    $credit = Coa::getJournalCreditValue($dataHeader1->Header1, $dataHeader2->Header2, $dataHeader3->Header3, $date);
                    $sumInitialValue = Coa::where('CompanyInternalID', Auth::user()->CompanyInternalID)->where('Header1', $dataHeader1->Header1)
                                    ->where('Flag', 0)->where('Header2', $dataHeader2->Header2)->where('Header3', $dataHeader3->Header3)->sum('InitialBalance');
                    if (($debet != 0 || $credit != 0 || $sumInitialValue != 0) || $zero == 1) {
                        $total3 = $debet - $credit + $sumInitialValue;
                        $text = number_format($total3, '2', '.', ',');
                        if ($total3 < 0) {
                            $text = '(' . number_format($total3 * -1, '2', '.', ',') . ')';
                        }
                        //text -> Total Header 3
                        $total2 += $total3;
                        $tamp2 .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $dataHeader3->Header3 . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $text . '</td>
                            </tr>';
                        $count3++;
                    }
                }
                if ($count3 != 0) {
                    $tamp2 .= '<tr>
                                <td style="font-family: helvetica,sans-serif; margin: 5px !important;" colspan="2"><hr></td>
                            </tr>';
                    $tamp2 .= '</table>';
                    $text = number_format($total2, '2', '.', ',');
                    if ($total2 < 0) {
                        $text = '(' . number_format($total2 * -1, '2', '.', ',') . ')';
                    }
                    $tamp2 .= '<table class="tableBorder"  style="width: 95%; margin-bottom: 18px; clear: both; position: relative; left: 3%">';
                    $tamp2 .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="80%">Total ' . $dataHeader2->Header2 . ' : </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . $text . '</td>
                            </tr>';
                    $tamp2 .= '</table>';
                    $tamp .= $tamp2;
                    $count2++;
                    //text -> Total Header 2
                    $total1 += $total2;
                }
            }
            if ($count2 != 0) {
                $text = number_format($total1, '2', '.', ',');
                if ($total1 < 0) {
                    $text = '(' . number_format($total1 * -1, '2', '.', ',') . ')';
                }
                $html .= $tamp;
                $html .= '<table width="698px" style="clear: both; padding-left:-1px;">';
                $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;margin: 5px !important;" colspan="2"><hr style="height:0.1px;"></td>
                            </tr>';
                $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="80%">Total ' . $dataHeader1->Header1 . ' : </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . $text . '</td>
                            </tr>';
                $html .= '</table>';
            }
        }
        $html .= '</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('balance_report');
    }

    public function profitReport() {
        $bulan = Input::get('month');
        $tahun = Input::get('year');
        $zero = Input::get('ZeroProfitLoss');
        $date = date("Y-m-d", strtotime("+1 month", strtotime(date($tahun . '-' . str_pad($bulan, 2, '0', STR_PAD_LEFT) . '-01'))));
        $html = '
            <html>
                <head>
                    <style>
                        table{
                            border-spacing: 0;
                        }      
                        th{
                            padding: 3px;
                             border-spacing: 0;
                            border-bottom: 0px solid #ddd;
                            text-align: center;
                        }
                        td{
                            padding: 3px;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:80%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Profit and Loss Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("M", strtotime($tahun . '-' . $bulan . '-01')) . ' ' . $tahun . '</span><br><br>';
        $totalSemua = 0;
        $header1 = Coa::where('CompanyInternalID', Auth::user()->CompanyInternalID)
                        ->where('Flag', 1)->select('Header1')
                        ->distinct('Header1')->get();
        foreach ($header1 as $dataHeader1) { //Loop Header 1
            $tamp = '';
            $count2 = 0;
            $total1 = 0;
            $tamp .= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">' . $dataHeader1->Header1 . '</span><br><br>';
            $header2 = Coa::where('CompanyInternalID', Auth::user()->CompanyInternalID)
                            ->where('Flag', 1)->where('Header1', $dataHeader1->Header1)
                            ->select('Header2')
                            ->distinct('Header2')->get();
            foreach ($header2 as $dataHeader2) { //Loop Header 2 berdasarkan Header 1
                $tamp2 = '';
                $count3 = 0;
                $tamp2 .= '<span style="padding-left:23px;font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">' . $dataHeader2->Header2 . '</span>';
                $total2 = 0;
                $header3 = Coa::where('CompanyInternalID', Auth::user()->CompanyInternalID)
                                ->where('Flag', 1)->where('Header1', $dataHeader1->Header1)
                                ->where('Header2', $dataHeader2->Header2)->select('Header3')
                                ->distinct('Header3')->orderBy('Header3')->get();
                $tamp2 .= '<table width="698px"  style="margin-top: 18px; clear: both;  top: 78px; padding-left:48px;">';
                foreach ($header3 as $dataHeader3) { //Loop Header 3 berdasarkan Header 1 & 2
                    $debet = Coa::getJournalDebetValueProfit($dataHeader1->Header1, $dataHeader2->Header2, $dataHeader3->Header3, $date);
                    $credit = Coa::getJournalCreditValueProfit($dataHeader1->Header1, $dataHeader2->Header2, $dataHeader3->Header3, $date);
                    $sumInitialValue = Coa::where('CompanyInternalID', Auth::user()->CompanyInternalID)->where('Header1', $dataHeader1->Header1)
                                    ->where('Flag', 1)->where('Header2', $dataHeader2->Header2)->where('Header3', $dataHeader3->Header3)->sum('InitialBalance');
                    if (($debet != 0 || $credit != 0 || $sumInitialValue != 0) || $zero == 1) {
                        $total3 = $credit - $debet + $sumInitialValue;
                        $text = number_format($total3, '2', '.', ',');
                        if ($total3 < 0) {
                            $text = '(' . number_format($total3 * -1, '2', '.', ',') . ')';
                        }
                        //text -> Total Header 3
                        $total2 += $total3;
                        $tamp2 .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $dataHeader3->Header3 . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;" colspan="2"></td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $text . '</td>
                            </tr>';
                        $count3++;
                    }
                }
                if ($count3 != 0) {
                    $tamp2 .= '<tr>
                                <td style="font-family: helvetica,sans-serif; margin: 5px !important;" colspan="4"><hr></td>
                            </tr>';
                    $tamp2 .= '</table>';
                    $text = number_format($total2, '2', '.', ',');
                    if ($total2 < 0) {
                        $text = '(' . number_format($total2 * -1, '2', '.', ',') . ')';
                    }
                    $tamp2 .= '<table width="698px" style="clear: both;  top: 78px; padding-left:20px;">';
                    $tamp2 .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;" colspan="2" width="50%"></td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="30%">Total ' . $dataHeader2->Header2 . ' : </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . $text . '</td>
                            </tr>';
                    $tamp2 .= '</table>';
                    //text -> Total Header 2
                    $total1 += $total2;
                    $tamp .= $tamp2;
                    $count2++;
                }
            }
            if ($count2 != 0) {
                $text = number_format($total1, '2', '.', ',');
                if ($total1 < 0) {
                    $text = '(' . number_format($total1 * -1, '2', '.', ',') . ')';
                }
                $html .= $tamp;
                $html .= '<table width="698px" style="clear: both;  top: 78px;padding-left:-1px;">';
                $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;margin: 5px !important;" colspan="4"><hr style="height:0.1px;"></td>
                            </tr>';
                $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;" colspan="2" width="50%"></td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="30%">Total ' . $dataHeader1->Header1 . ' : </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . $text . '</td>
                            </tr>';
                $html .= '</table>';
                $totalSemua += $total1;
            }
        }
        $text = number_format($totalSemua, '2', '.', ',');
        if ($totalSemua < 0) {
            $text = '(' . number_format($totalSemua * -1, '2', '.', ',') . ')';
        }
        $html .= '<table width="698px" style="clear: both;  top: 78px;padding-left:-1px;">
            <tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;" colspan="2" width="50%"></td>
                                <td style="font-family: helvetica,sans-serif;font-size: 14px; margin: 5px !important; font-weight: 700; text-align: right" width="30%">Total Profit : </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 14px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . $text . '</td>
                            </tr>';
        $html .= '</table>';
        $html .= '</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('profit_report');
    }

    public function generalReport() {
        $month = str_pad(Input::get("month"), 2, '0', STR_PAD_LEFT);
        $year = Input::get("year");
        $zero = Input::get('ZeroGeneralLedger');
        $totalDebet = 0;
        $totalCredit = 0;
        $totalInitial = 0;
        $name = "General Ledger Report";
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">' . $name . '</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">' . $name . '</span><br>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period    : ' . date('M', strtotime($year . '-' . $month . '-01')) . ' ' . $year . '</span>
                            <table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Account ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Account Name</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Initial Balance</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Debet</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Credit</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">End Balance</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
        if (Coa::where("CompanyInternalID", Auth::user()->Company->InternalID)->count() > 0) {
            foreach (Coa::where("CompanyInternalID", Auth::user()->Company->InternalID)->orderBy('COAName')->get() as $coa) {
                $debet = JournalDetail::getJournalDebetGeneral($coa, $month, $year);
                $credit = JournalDetail::getJournalCreditGeneral($coa, $month, $year);
                $initial = JournalDetail::getJournalInitialBalanceGeneral($coa, $month, $year);
                $totalDebet += $debet;
                $totalCredit += $credit;
                $totalInitial += $initial;
                if (($debet != 0 || $credit != 0 || $initial != 0) || $zero == 1) {
                    $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . Coa::formatCoa($coa->ACC1InternalID, $coa->ACC2InternalID, $coa->ACC3InternalID, $coa->ACC4InternalID, $coa->ACC5InternalID, $coa->ACC6InternalID, 1) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $coa->COAName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . (JournalDetail::getJournalInitialBalanceGeneral($coa, $month, $year) < 0 ? "(" . number_format((-1 * JournalDetail::getJournalInitialBalanceGeneral($coa, $month, $year)), 2, ".", ",") . ")" : number_format(JournalDetail::getJournalInitialBalanceGeneral($coa, $month, $year), 2, ".", ",")) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . (JournalDetail::getJournalDebetGeneral($coa, $month, $year) < 0 ? "(" . number_format((-1 * JournalDetail::getJournalDebetGeneral($coa, $month, $year)), 2, ".", ",") . ")" : number_format(JournalDetail::getJournalDebetGeneral($coa, $month, $year), 2, ".", ",")) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . (JournalDetail::getJournalCreditGeneral($coa, $month, $year) < 0 ? "(" . number_format((-1 * JournalDetail::getJournalCreditGeneral($coa, $month, $year)), 2, ".", ",") . ")" : number_format(JournalDetail::getJournalCreditGeneral($coa, $month, $year), 2, ".", ",")) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . ((JournalDetail::getJournalInitialBalanceGeneral($coa, $month, $year) + JournalDetail::getJournalDebetGeneral($coa, $month, $year) - JournalDetail::getJournalCreditGeneral($coa, $month, $year)) < 0 ? "(" . number_format((-1 * (JournalDetail::getJournalInitialBalanceGeneral($coa, $month, $year) + JournalDetail::getJournalDebetGeneral($coa, $month, $year) - JournalDetail::getJournalCreditGeneral($coa, $month, $year))), 2, ".", ",") . ")" : number_format((JournalDetail::getJournalInitialBalanceGeneral($coa, $month, $year) + JournalDetail::getJournalDebetGeneral($coa, $month, $year) - JournalDetail::getJournalCreditGeneral($coa, $month, $year)), 2, ".", ",")) . '</td>
                            </tr>';
                }
            }

            $html .= '<tr> <td colspan="6"><hr></td>
                        </tr>
                    <tr>
                                <td colspan="2" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right;"> Total </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right">' . ($totalInitial < 0 ? "(" . number_format((-1 * $totalInitial), 2, ".", ",") . ")" : number_format($totalInitial, 2, ".", ",")) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right">' . ($totalDebet < 0 ? "(" . number_format((-1 * $totalDebet), 2, ".", ",") . ")" : number_format($totalDebet, 2, ".", ",")) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right">' . ($totalCredit < 0 ? "(" . number_format((-1 * $totalCredit), 2, ".", ",") . ")" : number_format($totalCredit, 2, ".", ",")) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right">' . (($totalInitial + $totalDebet - $totalCredit) < 0 ? "(" . number_format((-1 * ($totalInitial + $totalDebet - $totalCredit)), 2, ".", ",") . ")" : number_format(($totalInitial + $totalDebet - $totalCredit), 2, ".", ",")) . '</td>
                    </tr>';
        } else {
            $html .= '<tr>
                            <td colspan="6" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: center">There is no COA account.</td>
                        </tr>';
        }
        $html .= '</tbody>
                            </div>
                    </div>
                </body>
            </html>';
        return PDF::load($html, 'A4', 'portrait')->download('general_report');
    }

    public function detailReport() {
        Excel::create('Sales_Report', function($excel) {
            $excel->sheet('Sales_Report', function($sheet) {
                $startT = explode('-', Input::get('sDate'));
                $endT = explode('-', Input::get('eDate'));
                $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
                $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
                $customer = Input::get('customer');
                $salesman = Input::get('salesman');
                $type = Input::get('type');
                $payment = Input::get('payment');
                if ($payment == "all") {
                    $payname = "All";
                } else if ($payment == "complete") {
                    $payname = "Complete";
                } else if ($payment == "paid") {
                    $payname = "Paid";
                } else {
                    $payname = "No Paid";
                }
                $total1 = $total2 = $total3 = $total4 = $total5 = $total6 = 0;
                //customer
                $c = '';
                //salesman
                $p = '';
                //cek PPN dan non PPN
                $ci = '';
                $v = '';
                $py = '';
                $sttus = '';
                if ($customer != 'all') {
                    $cusname = Coa6::find($customer)->ACC6Name;
                    $c = " AND m_coa6.InternalID = $customer";
                } else {
                    $cusname = "All";
                }
                if ($salesman != 'all') {
                    $salname = SalesMan::find($salesman)->SalesManName;
                    $p = " AND m_sales_man.InternalID = $salesman";
                } else {
                    $salname = "All";
                }
                if (Auth::user()->SeeNPPN == 1) {
                    //NON PPN 
                    $ci = " AND t_sales_header.VAT = 0";
                } else {
                    //PPN 
                    $ci = " AND t_sales_header.VAT = 1";
                }
                if ($payment != 'all') {
                    $sttus = ' AND t_sales_header.`GrandTotal`*t_sales_header.`CurrencyRate` > (Select IFNULL(SUM(td.JournalCredit),0) From t_journal_detail td Where td.JournalTransactionID = t_sales_header.`SalesID`)';
                }

                $totalPO = 0;
                if ($type == "Detail")
                    $sheet->mergeCells('B1:T1');
                else
                    $sheet->mergeCells('B1:N1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Sales Report By " . $type);
                $sheet->mergeCells('C2:D2');
                $sheet->setCellValueByColumnAndRow(1, 2, "Period :");
                $sheet->setCellValueByColumnAndRow(2, 2, $start . ' to ' . $end);
                $sheet->setCellValueByColumnAndRow(1, 3, "Customer :");
                $sheet->setCellValueByColumnAndRow(2, 3, $cusname);
                $sheet->setCellValueByColumnAndRow(1, 4, "Salesman :");
                $sheet->setCellValueByColumnAndRow(2, 4, $salname);
                $sheet->setCellValueByColumnAndRow(1, 5, "AR Payment :");
                $sheet->setCellValueByColumnAndRow(2, 5, $payname);
                $hitung = 0;
                $row = 6;
                if ($type == "Detail") {
                    $sheet->cells('B' . $row . ':T' . $row, function($cells) {
                        $cells->setBackground('#eaf6f7');
                        $cells->setValignment('middle');
                        $cells->setAlignment('center');
                    });
                    $sheet->setBorder('B' . $row . ':T' . $row, 'thin');
                    $sheet->setCellValueByColumnAndRow(1, $row, "Date");
                    $sheet->setCellValueByColumnAndRow(2, $row, "Invoice No");
                    $sheet->setCellValueByColumnAndRow(3, $row, "Customer");
                    $sheet->setCellValueByColumnAndRow(4, $row, "Tax Number");
                    $sheet->setCellValueByColumnAndRow(5, $row, "Sales Man");
                    $sheet->setCellValueByColumnAndRow(6, $row, "Product");
                    $sheet->setCellValueByColumnAndRow(7, $row, "Quantity");
                    $sheet->setCellValueByColumnAndRow(8, $row, "UOM");
                    $sheet->setCellValueByColumnAndRow(9, $row, "Nominal");
                    $sheet->setCellValueByColumnAndRow(10, $row, "Discount");
                    $sheet->setCellValueByColumnAndRow(11, $row, "Discount Nominal");
                    $sheet->setCellValueByColumnAndRow(12, $row, "Total");
                    $sheet->setCellValueByColumnAndRow(13, $row, "Discount Global");
                    $sheet->setCellValueByColumnAndRow(14, $row, "DP (%)");
                    $sheet->setCellValueByColumnAndRow(15, $row, "DP");
                    $sheet->setCellValueByColumnAndRow(16, $row, "GrandTotal");
                    $sheet->setCellValueByColumnAndRow(17, $row, "VAT");
                    $sheet->setCellValueByColumnAndRow(18, $row, "GrandTotal(VAT)");
                    $sheet->setCellValueByColumnAndRow(19, $row, "AR Payment");
                } else {
                    $sheet->cells('B' . $row . ':N' . $row, function($cells) {
                        $cells->setBackground('#eaf6f7');
                        $cells->setValignment('middle');
                        $cells->setAlignment('center');
                    });
                    $sheet->setBorder('B' . $row . ':N' . $row, 'thin');
                    $sheet->setCellValueByColumnAndRow(1, $row, "Date");
                    $sheet->setCellValueByColumnAndRow(2, $row, "Invoice No");
                    $sheet->setCellValueByColumnAndRow(3, $row, "Customer");
                    $sheet->setCellValueByColumnAndRow(4, $row, "Tax Number");
                    $sheet->setCellValueByColumnAndRow(5, $row, "Sales Man");
                    $sheet->setCellValueByColumnAndRow(6, $row, "Total");
                    $sheet->setCellValueByColumnAndRow(7, $row, "Discount Global");
                    $sheet->setCellValueByColumnAndRow(8, $row, "DP (%)");
                    $sheet->setCellValueByColumnAndRow(9, $row, "DP");
                    $sheet->setCellValueByColumnAndRow(10, $row, "GrandTotal");
                    $sheet->setCellValueByColumnAndRow(11, $row, "VAT");
                    $sheet->setCellValueByColumnAndRow(12, $row, "GrandTotal(VAT)");
                    $sheet->setCellValueByColumnAndRow(13, $row, "AR Payment");
                }
                $row++;
                if ($type == "Detail") {
                    // DETAIL 

                    if ($payment == 'complete') {
                        $model = DB::select("SELECT t_sales_header.*, `t_salesorder_header`.`isCash` as `isCash`,`t_sales_detail`.`Qty` as `Qty`,`m_sales_man`.`SalesManName` as `SalesManName`, m_inventory.InternalID as InventoryID,m_inventory.`InventoryName` as `InventoryName`, 
                    `m_uom`.`UomID` as `UomID`, `t_sales_detail`.`SubTotal` as `SubTotal`, 
                    `m_coa6`.`ACC6Name` as `CustomerName`, `t_salesorder_header`.`DownPayment` as `dpSO`,
                    t_sales_detail.Price as price, t_sales_detail.Vat as Vat, 
                    t_sales_detail.Discount, t_sales_detail.DiscountNominal  FROM
                                t_salesorder_header  inner join `t_salesorder_detail` on `t_salesorder_detail`.`SalesOrderInternalID` = `t_salesorder_header`.`InternalID` 
                                inner join `m_coa6` on `m_coa6`.`InternalID` = `t_salesorder_header`.`ACC6InternalID` 
                                inner join `m_sales_man` on `t_salesorder_header`.`SalesManInternalID` = `m_sales_man`.`InternalID` 
                                inner join `m_inventory` on `m_inventory`.`InternalID` = `t_salesorder_detail`.`InventoryInternalID` 
                                inner join `m_uom` on `m_uom`.`InternalID` = `t_salesorder_detail`.`UomInternalID` 
                                inner join t_sales_header on t_sales_header.SalesOrderInternalID = t_salesorder_header.InternalID 
                                inner join t_sales_detail on t_sales_detail.SalesInternalID = t_sales_header.InternalID and t_sales_detail.InventoryInternalID = m_inventory.InternalID WHERE  
										  (t_sales_header.GrandTotal <= (SELECT IFNULL(SUM(jd.JournalCredit),0) 
                                FROM t_journal_detail jd WHERE jd.JournalTransactionID = t_sales_header.SalesID)) AND `t_sales_header`.`SalesDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_sales_header.SalesID ASC");
                    } else if ($payment == 'no_paid') {
                        $model = DB::select("SELECT t_sales_header.*, `t_salesorder_header`.`isCash` as `isCash`,`t_sales_detail`.`Qty` as `Qty`, m_inventory.InternalID as InventoryID,m_inventory.`InventoryName` as `InventoryName`, 
                    `m_uom`.`UomID` as `UomID`,`m_sales_man`.`SalesManName` as `SalesManName`, `t_sales_detail`.`SubTotal` as `SubTotal`, 
                    `m_coa6`.`ACC6Name` as `CustomerName`, `t_salesorder_header`.`DownPayment` as `dpSO`,
                    t_sales_detail.Price as price, t_sales_detail.Vat as Vat, 
                    t_sales_detail.Discount, t_sales_detail.DiscountNominal  FROM
                                t_salesorder_header  inner join `t_salesorder_detail` on `t_salesorder_detail`.`SalesOrderInternalID` = `t_salesorder_header`.`InternalID` 
                                inner join `m_coa6` on `m_coa6`.`InternalID` = `t_salesorder_header`.`ACC6InternalID` 
                                inner join `m_sales_man` on `t_salesorder_header`.`SalesManInternalID` = `m_sales_man`.`InternalID`                                 
                                inner join `m_inventory` on `m_inventory`.`InternalID` = `t_salesorder_detail`.`InventoryInternalID` 
                                inner join `m_uom` on `m_uom`.`InternalID` = `t_salesorder_detail`.`UomInternalID` 
                                inner join t_sales_header on t_sales_header.SalesOrderInternalID = t_salesorder_header.InternalID 
                                inner join t_sales_detail on t_sales_detail.SalesInternalID = t_sales_header.InternalID and t_sales_detail.InventoryInternalID = m_inventory.InternalID WHERE  
										  (t_sales_header.GrandTotal > (SELECT IFNULL(SUM(jd.JournalCredit),0) 
                                FROM t_journal_detail jd WHERE jd.JournalTransactionID = t_sales_header.SalesID)) AND t_sales_header.isCash in(1,4) AND `t_sales_header`.`SalesDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_sales_header.SalesID ASC");
                    } else if ($payment == 'all') {
                        $model = DB::select("SELECT t_sales_header.*, `t_salesorder_header`.`isCash` as `isCash`,`t_sales_detail`.`Qty` as `Qty`, m_inventory.InternalID as InventoryID,m_inventory.`InventoryName` as `InventoryName`, 
                    `m_uom`.`UomID` as `UomID`,`m_sales_man`.`SalesManName` as `SalesManName`, `t_sales_detail`.`SubTotal` as `SubTotal`, 
                    `m_coa6`.`ACC6Name` as `CustomerName`, `t_salesorder_header`.`DownPayment` as `dpSO`,
                    t_sales_detail.Price as price, t_sales_detail.Vat as Vat, 
                    t_sales_detail.Discount, t_sales_detail.DiscountNominal  FROM
                                t_salesorder_header  inner join `t_salesorder_detail` on `t_salesorder_detail`.`SalesOrderInternalID` = `t_salesorder_header`.`InternalID` 
                                inner join `m_coa6` on `m_coa6`.`InternalID` = `t_salesorder_header`.`ACC6InternalID` 
                                inner join `m_sales_man` on `t_salesorder_header`.`SalesManInternalID` = `m_sales_man`.`InternalID`                                 
                                inner join `m_inventory` on `m_inventory`.`InternalID` = `t_salesorder_detail`.`InventoryInternalID` 
                                inner join `m_uom` on `m_uom`.`InternalID` = `t_salesorder_detail`.`UomInternalID` 
                                inner join t_sales_header on t_sales_header.SalesOrderInternalID = t_salesorder_header.InternalID 
                                inner join t_sales_detail on t_sales_detail.SalesInternalID = t_sales_header.InternalID and t_sales_detail.InventoryInternalID = m_inventory.InternalID WHERE  
										     `t_sales_header`.`SalesDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_sales_header.SalesID ASC");
                    } else if ($payment == 'paid') {
                        $model = DB::select("SELECT t_sales_header.*, `t_salesorder_header`.`isCash` as `isCash`,`t_sales_detail`.`Qty` as `Qty`,`m_sales_man`.`SalesManName` as `SalesManName`, m_inventory.InternalID as InventoryID,m_inventory.`InventoryName` as `InventoryName`, 
                    `m_uom`.`UomID` as `UomID`, `t_sales_detail`.`SubTotal` as `SubTotal`, 
                    `m_coa6`.`ACC6Name` as `CustomerName`, `t_salesorder_header`.`DownPayment` as `dpSO`,
                    t_sales_detail.Price as price, t_sales_detail.Vat as Vat, 
                    t_sales_detail.Discount, t_sales_detail.DiscountNominal  FROM
                                t_salesorder_header  inner join `t_salesorder_detail` on `t_salesorder_detail`.`SalesOrderInternalID` = `t_salesorder_header`.`InternalID` 
                                inner join `m_coa6` on `m_coa6`.`InternalID` = `t_salesorder_header`.`ACC6InternalID` 
                                inner join `m_sales_man` on `t_salesorder_header`.`SalesManInternalID` = `m_sales_man`.`InternalID` 
                                inner join `m_inventory` on `m_inventory`.`InternalID` = `t_salesorder_detail`.`InventoryInternalID` 
                                inner join `m_uom` on `m_uom`.`InternalID` = `t_salesorder_detail`.`UomInternalID` 
                                inner join t_sales_header on t_sales_header.SalesOrderInternalID = t_salesorder_header.InternalID 
                                inner join t_sales_detail on t_sales_detail.SalesInternalID = t_sales_header.InternalID and t_sales_detail.InventoryInternalID = m_inventory.InternalID WHERE  
										  ((SELECT IFNULL(SUM(jd.JournalCredit),0) 
                                FROM t_journal_detail jd WHERE jd.JournalTransactionID = t_sales_header.SalesID) > 0) AND `t_sales_header`.`SalesDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_sales_header.SalesID ASC");
                    }
                } else {
                    // SUMMARY

                    if ($payment == 'complete') {
                        $model = DB::select("SELECT t_sales_header.*, `t_salesorder_header`.`isCash` as `isCash`,`m_sales_man`.`SalesManName` as `SalesManName`, `m_coa6`.`ACC6Name` as `CustomerName` , `t_salesorder_header`.`DownPayment` as `dpSO` 
                   FROM t_salesorder_header  inner join `m_coa6` on `m_coa6`.`InternalID` = `t_salesorder_header`.`ACC6InternalID` 
                                inner join `m_sales_man` on `t_salesorder_header`.`SalesManInternalID` = `m_sales_man`.`InternalID` 
                                inner join t_sales_header on t_sales_header.SalesOrderInternalID = t_salesorder_header.InternalID 
                                 WHERE (t_sales_header.GrandTotal <= (SELECT IFNULL(SUM(jd.JournalCredit),0) 
                                FROM t_journal_detail jd WHERE jd.JournalTransactionID = t_sales_header.SalesID)) AND `t_sales_header`.`SalesDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_sales_header.SalesID ASC");
                    } else if ($payment == 'no_paid') {
                        $model = DB::select("SELECT t_sales_header.*, `t_salesorder_header`.`isCash` as `isCash`,`m_sales_man`.`SalesManName` as `SalesManName`, `m_coa6`.`ACC6Name` as `CustomerName` , `t_salesorder_header`.`DownPayment` as `dpSO` FROM
                                 t_salesorder_header inner join `m_coa6` on `m_coa6`.`InternalID` = `t_salesorder_header`.`ACC6InternalID` 
                                inner join `m_sales_man` on `t_salesorder_header`.`SalesManInternalID` = `m_sales_man`.`InternalID`                                                               
                                inner join t_sales_header on t_sales_header.SalesOrderInternalID = t_salesorder_header.InternalID 
                                WHERE (t_sales_header.GrandTotal > (SELECT IFNULL(SUM(jd.JournalCredit),0) 
                                FROM t_journal_detail jd WHERE jd.JournalTransactionID = t_sales_header.SalesID)) AND t_sales_header.isCash in(1,4) AND `t_sales_header`.`SalesDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_sales_header.SalesID ASC");
                    } else if ($payment == 'all') {
                        $model = DB::select("SELECT t_sales_header.*, `t_salesorder_header`.`isCash` as `isCash`,`m_sales_man`.`SalesManName` as `SalesManName`, `m_coa6`.`ACC6Name` as `CustomerName`,  `t_salesorder_header`.`DownPayment` as `dpSO`
                   FROM t_salesorder_header inner join `m_coa6` on `m_coa6`.`InternalID` = `t_salesorder_header`.`ACC6InternalID` 
                                inner join `m_sales_man` on `t_salesorder_header`.`SalesManInternalID` = `m_sales_man`.`InternalID`                                 
                                inner join t_sales_header on t_sales_header.SalesOrderInternalID = t_salesorder_header.InternalID 
                               WHERE `t_sales_header`.`SalesDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_sales_header.SalesID ASC");
                    } else if ($payment == 'paid') {
                        $model = DB::select("SELECT t_sales_header.*, `t_salesorder_header`.`isCash` as `isCash`,`m_sales_man`.`SalesManName` as `SalesManName`,`m_coa6`.`ACC6Name` as `CustomerName`, `t_salesorder_header`.`DownPayment` as `dpSO` 
                   FROM t_salesorder_header  inner join `m_coa6` on `m_coa6`.`InternalID` = `t_salesorder_header`.`ACC6InternalID` 
                                inner join `m_sales_man` on `t_salesorder_header`.`SalesManInternalID` = `m_sales_man`.`InternalID` 
                                inner join t_sales_header on t_sales_header.SalesOrderInternalID = t_salesorder_header.InternalID 
                                WHERE ((SELECT IFNULL(SUM(jd.JournalCredit),0) 
                                FROM t_journal_detail jd WHERE jd.JournalTransactionID = t_sales_header.SalesID) > 0) AND `t_sales_header`.`SalesDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_sales_header.SalesID ASC");
                    }
                }

                $xx = $yy = 0;
                if (count($model) > 0) {
                    $checklast = 0;
                    $totalVATDP = 0;
                    $noinvoice = '';
                    $rowawal = $row;
                    foreach ($model as $a) {
                        if ($noinvoice != $a->SalesID) {
                            $tulis = "ya";
                            $rowawal = $row;
                        } else {
                            $tulis = "tidak";
                        }
                        $noinvoice = $a->SalesID;
                        $TotalDP = 0;
                        $py = SalesHeader::salesReceivableReport2($a->SalesID, $payment);
                        $sum = SalesAddDetail::where('SalesInternalID', $a->InternalID)->sum('SubTotal');
                        $detail = SalesAddDetail::where('SalesInternalID', $a->InternalID)->get();
                        if ($type == "Detail") {
                            $total = $a->SubTotal;
                        } else {
                            if ($a->VAT == 1) {
                                $total = $a->GrandTotal / 1.1;
                            } else {
                                $total = $a->GrandTotal;
                            }
                        }
                        if ($sum != 0)
                            $discountGlobal = $a->DiscountGlobal;
//                            $discountGlobal = ($a->DiscountGlobal * $total) / $sum;
                        else
                            $discountGlobal = 0;
                        $grand = ceil($sum) - $discountGlobal;
                        $vatHeader = $grand - $a->DownPayment;
                        $vatgrand = floor($vatHeader * 0.1);
                        $grandHeader = ceil($sum) - $a->DiscountGlobal;
                        $ppnHeader = floor($grandHeader * 0.1);
                        if ($grandHeader != 0)
                            $ppn = round(($total * $ppnHeader) / $grandHeader, 0);
                        else
                            $ppn = 0;
                        if ($sum != 0)
                            $DP = round(($total * $a->DownPayment) / ceil($sum));
                        else
                            $DP = 0;
                        $TotalDP = ceil($grand - $a->DownPayment);
//                        if ($TotalDP % 10 != 0)
//                            $TotalDP++;
                        $vatdp = floor($TotalDP * 0.1);
                        $totalVATDP += $vatdp;

                        $checklast++;
                        if ($checklast == count($detail)) {
                            $checklast = 0;

                            if ($vatgrand != $totalVATDP) {
                                $selisih = $vatgrand - $totalVATDP;
                                $vatdp = $vatdp + $selisih;
                                $totalVATDP = 0;
                            } else {
                                $totalVATDP = 0;
                            }
                        }
                        $grandTotalDP = round($TotalDP + $vatdp);

                        if ($type == "Detail") {
                            $sheet->setBorder('B' . $row . ':T' . $row, 'thin');
                            if ($tulis == "ya") {
                                if ($xx != 0)
                                    $row++;
                                else
                                    $xx++;
                                $sheet->setCellValueByColumnAndRow(1, $row, date("d-M-y", strtotime($a->SalesDate)));
                                $sheet->setCellValueByColumnAndRow(2, $row, $a->SalesID);
                                $sheet->setCellValueByColumnAndRow(3, $row, $a->CustomerName);
                                $sheet->setCellValueByColumnAndRow(4, $row, $a->TaxNumber);
                                $sheet->setCellValueByColumnAndRow(5, $row, $a->SalesManName);
                                $sheet->setBorder('B' . $row . ':T' . $row, 'thin');
                                $row++;
                                $totgrand = 0;
                            }
                            $sheet->setCellValueByColumnAndRow(6, $row, $a->InventoryName);
                            $sheet->setCellValueByColumnAndRow(7, $row, number_format($a->Qty, '3', '.', ','));
                            $sheet->setCellValueByColumnAndRow(8, $row, $a->UomID);
                            $sheet->setCellValueByColumnAndRow(9, $row, number_format($a->price, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(10, $row, number_format($a->Discount, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(11, $row, number_format($a->DiscountNominal, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(12, $row, number_format(ceil($a->SubTotal), '2', '.', ','));
                            $totgrand += ceil($a->SubTotal);
                            if ($tulis == "ya") {
                                if ($yy != 0)
                                    $rowawal++;
                                else
                                    $yy++;
                                $sheet->setCellValueByColumnAndRow(12, $rowawal, number_format($totgrand, '2', '.', ','));
                                $sheet->setCellValueByColumnAndRow(13, $rowawal, number_format($discountGlobal, '2', '.', ','));
                                $sheet->setCellValueByColumnAndRow(14, $rowawal, number_format($a->dpSO, '2', '.', ','));
                                $sheet->setCellValueByColumnAndRow(15, $rowawal, number_format($a->DownPayment, '2', '.', ','));
                                $sheet->setCellValueByColumnAndRow(16, $rowawal, number_format($TotalDP, '2', '.', ','));
                                $sheet->setCellValueByColumnAndRow(17, $rowawal, number_format($vatdp, '2', '.', ','));
                                $sheet->setCellValueByColumnAndRow(18, $rowawal, number_format($grandTotalDP, '2', '.', ','));
                                $sheet->setCellValueByColumnAndRow(19, $rowawal, $py);
                                $sheet->setBorder('B' . $row . ':T' . $row, 'thin');
                                $total1 += $totgrand;
                                $total2 += $discountGlobal;
                                $total3 += $a->DownPayment;
                                $total4 += $TotalDP;
                                $total5 += $vatdp;
                                $total6 += $grandTotalDP;
                            }
                        } else {
                            $total1 += ceil($sum);
                            $total2 += $discountGlobal;
                            $total3 += $a->DownPayment;
                            $total4 += $TotalDP;
                            $total5 += $vatdp;
                            $total6 += $grandTotalDP;
                            $sheet->setBorder('B' . $row . ':N' . $row, 'thin');
                            $sheet->setCellValueByColumnAndRow(1, $row, date("d-M-y", strtotime($a->SalesDate)));
                            $sheet->setCellValueByColumnAndRow(2, $row, $a->SalesID);
                            $sheet->setCellValueByColumnAndRow(3, $row, $a->CustomerName);
                            $sheet->setCellValueByColumnAndRow(4, $row, $a->TaxNumber);
                            $sheet->setCellValueByColumnAndRow(5, $row, $a->SalesManName);
                            $sheet->setCellValueByColumnAndRow(6, $row, number_format(ceil($sum), '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(7, $row, number_format($discountGlobal, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(8, $row, number_format($a->dpSO, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(9, $row, number_format($a->DownPayment, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(10, $row, number_format($TotalDP, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(11, $row, number_format($vatdp, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(12, $row, number_format($grandTotalDP, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(13, $row, $py);
                        }
                        $row++;
                        $hitung++;
                    }
                }

                if ($hitung == 0) {
                    if ($type == "Detail")
                        $sheet->mergeCells('B' . $row . ':T' . $row);
                    else
                        $sheet->mergeCells('B' . $row . ':N' . $row);
                    $sheet->setCellValueByColumnAndRow(1, $row, "There is no report.");
                }
                $sheet->cells('B2:B5', function($cells) {
                    $cells->setAlignment('left');
                    $cells->setFontWeight('bold');
                });
                if ($type == "Detail") {
                    $sheet->setCellValueByColumnAndRow(1, $row, "Total");
                    $sheet->setCellValueByColumnAndRow(12, $row, number_format($total1, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(13, $row, number_format($total2, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(15, $row, number_format($total3, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(16, $row, number_format($total4, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(17, $row, number_format($total5, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(18, $row, number_format($total6, '2', '.', ','));
                    $sheet->cells('B6:T6', function($cells) {
                        $cells->setAlignment('middle');
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('J7:S' . $row, function($cells) {
                        $cells->setAlignment('right');
                    });
                    $sheet->cells('B' . $row, function($cells) {
                        $cells->setAlignment('middle');
                        $cells->setFontWeight('bold');
                    });
                } else {
                    $sheet->setCellValueByColumnAndRow(1, $row, "Total");
                    $sheet->setCellValueByColumnAndRow(6, $row, number_format($total1, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(7, $row, number_format($total2, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(9, $row, number_format($total3, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(10, $row, number_format($total4, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(11, $row, number_format($total5, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(12, $row, number_format($total6, '2', '.', ','));
                    $sheet->cells('B6:N6', function($cells) {
                        $cells->setAlignment('middle');
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('G7:M' . $row, function($cells) {
                        $cells->setAlignment('right');
                    });
                    $sheet->cells('B' . $row, function($cells) {
                        $cells->setAlignment('middle');
                        $cells->setFontWeight('bold');
                    });
                }
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
//                $sheet->cells('B3:G' . $row, function($cells) {
//                    $cells->setAlignment('left');
//                    $cells->setValignment('middle');
//                });
//                $sheet->cells('B3:B' . $row, function($cells) {
//                    $cells->setAlignment('left');
//                });
            });
        })->export('xls');
    }

    public function purchaseDetailReport() {
        Excel::create('Purchase_Report', function($excel) {
            $excel->sheet('Purchase_Report', function($sheet) {
                $startT = explode('-', Input::get('sDate'));
                $endT = explode('-', Input::get('eDate'));
                $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
                $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
                $customer = Input::get('supplier');
                $tax = Input::get('tax');
                $type = Input::get('type');
                $payment = Input::get('payment');
                if ($payment == "all") {
                    $payname = "All";
                } else if ($payment == "complete") {
                    $payname = "Complete";
                } else if ($payment == "paid") {
                    $payname = "Paid";
                } else {
                    $payname = "No Paid";
                }
                $total1 = $total2 = $total3 = $total4 = $total5 = $total6 = 0;
                //customer
                $c = '';
                //salesman
                $p = '';
                //cek PPN dan non PPN
                $ci = '';
                $v = '';
                $py = '';
                $sttus = '';
                if ($customer != 'all') {
                    $cusname = Coa6::find($customer)->ACC6Name;
                    $c = " AND m_coa6.InternalID = $customer";
                } else {
                    $cusname = "All";
                }
                if ($tax != 'all') {
                    if ($tax == 0)
                        $salname = "Non PPN";
                    else
                        $salname = "PPN";
                    $p = " AND t_purchase_header.VAT = $tax";
                } else {
                    $salname = "All";
                }
//                if (Auth::user()->SeeNPPN == 1) {
//                    //NON PPN 
//                    $ci = " AND t_purchase_header.VAT = 0";
//                } else {
//                    //PPN 
//                    $ci = " AND t_purchase_header.VAT = 1";
//                }
                if ($payment != 'all') {
                    $sttus = ' AND t_purchase_header.`GrandTotal`*t_purchase_header.`CurrencyRate` > (Select IFNULL(SUM(td.JournalDebet),0) From t_journal_detail td Where td.JournalTransactionID = t_purchase_header.`PurchaseID`)';
                }

                $totalPO = 0;
                if ($type == "Detail")
                    $sheet->mergeCells('B1:T1');
                else
                    $sheet->mergeCells('B1:N1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Purchase Report By " . $type);
                $sheet->mergeCells('C2:D2');
                $sheet->setCellValueByColumnAndRow(1, 2, "Period :");
                $sheet->setCellValueByColumnAndRow(2, 2, $start . ' to ' . $end);
                $sheet->setCellValueByColumnAndRow(1, 3, "Supplier :");
                $sheet->setCellValueByColumnAndRow(2, 3, $cusname);
                $sheet->setCellValueByColumnAndRow(1, 4, "Tax :");
                $sheet->setCellValueByColumnAndRow(2, 4, $salname);
                $sheet->setCellValueByColumnAndRow(1, 5, "AR Payment :");
                $sheet->setCellValueByColumnAndRow(2, 5, $payname);
                $hitung = 0;
                $row = 6;
                if ($type == "Detail") {
                    $sheet->cells('B' . $row . ':T' . $row, function($cells) {
                        $cells->setBackground('#eaf6f7');
                        $cells->setValignment('middle');
                        $cells->setAlignment('center');
                    });
                    $sheet->setBorder('B' . $row . ':T' . $row, 'thin');
                    $sheet->setCellValueByColumnAndRow(1, $row, "Date");
                    $sheet->setCellValueByColumnAndRow(2, $row, "Invoice No");
                    $sheet->setCellValueByColumnAndRow(3, $row, "Supplier");
                    $sheet->setCellValueByColumnAndRow(4, $row, "Tax Number");
                    $sheet->setCellValueByColumnAndRow(5, $row, "Tax");
                    $sheet->setCellValueByColumnAndRow(6, $row, "Product");
                    $sheet->setCellValueByColumnAndRow(7, $row, "Quantity");
                    $sheet->setCellValueByColumnAndRow(8, $row, "UOM");
                    $sheet->setCellValueByColumnAndRow(9, $row, "Nominal");
                    $sheet->setCellValueByColumnAndRow(10, $row, "Discount");
                    $sheet->setCellValueByColumnAndRow(11, $row, "Discount Nominal");
                    $sheet->setCellValueByColumnAndRow(12, $row, "Total");
                    $sheet->setCellValueByColumnAndRow(13, $row, "Discount Global");
                    $sheet->setCellValueByColumnAndRow(14, $row, "DP (%)");
                    $sheet->setCellValueByColumnAndRow(15, $row, "DP");
                    $sheet->setCellValueByColumnAndRow(16, $row, "GrandTotal");
                    $sheet->setCellValueByColumnAndRow(17, $row, "VAT");
                    $sheet->setCellValueByColumnAndRow(18, $row, "GrandTotal(VAT)");
                    $sheet->setCellValueByColumnAndRow(19, $row, "AR Payment");
                } else {
                    $sheet->cells('B' . $row . ':N' . $row, function($cells) {
                        $cells->setBackground('#eaf6f7');
                        $cells->setValignment('middle');
                        $cells->setAlignment('center');
                    });
                    $sheet->setBorder('B' . $row . ':N' . $row, 'thin');
                    $sheet->setCellValueByColumnAndRow(1, $row, "Date");
                    $sheet->setCellValueByColumnAndRow(2, $row, "Invoice No");
                    $sheet->setCellValueByColumnAndRow(3, $row, "Supplier");
                    $sheet->setCellValueByColumnAndRow(4, $row, "Tax Number");
                    $sheet->setCellValueByColumnAndRow(5, $row, "Tax");
                    $sheet->setCellValueByColumnAndRow(6, $row, "Total");
                    $sheet->setCellValueByColumnAndRow(7, $row, "Discount Global");
                    $sheet->setCellValueByColumnAndRow(8, $row, "DP (%)");
                    $sheet->setCellValueByColumnAndRow(9, $row, "DP");
                    $sheet->setCellValueByColumnAndRow(10, $row, "GrandTotal");
                    $sheet->setCellValueByColumnAndRow(11, $row, "VAT");
                    $sheet->setCellValueByColumnAndRow(12, $row, "GrandTotal(VAT)");
                    $sheet->setCellValueByColumnAndRow(13, $row, "AR Payment");
                }
                $row++;
                if ($type == "Detail") {
                    // DETAIL 

                    if ($payment == 'complete') {
                        $model = DB::select("SELECT t_purchase_header.*, `t_purchaseorder_header`.`isCash` as `isCash`,`t_purchase_detail`.`Qty` as `Qty`, m_inventory.InternalID as InventoryID,m_inventory.`InventoryName` as `InventoryName`, 
                    `m_uom`.`UomID` as `UomID`, `t_purchase_detail`.`SubTotal` as `SubTotal`, 
                    `m_coa6`.`ACC6Name` as `CustomerName`, `t_purchaseorder_header`.`DownPayment` as `dpSO`,
                    t_purchase_detail.Price as price, t_purchase_detail.Vat as Vat, 
                    t_purchase_detail.Discount, t_purchase_detail.DiscountNominal  FROM
                                t_purchaseorder_header  inner join `t_purchaseorder_detail` on `t_purchaseorder_detail`.`PurchaseOrderInternalID` = `t_purchaseorder_header`.`InternalID` 
                                inner join `m_coa6` on `m_coa6`.`InternalID` = `t_purchaseorder_header`.`ACC6InternalID` 
                                inner join `m_inventory` on `m_inventory`.`InternalID` = `t_purchaseorder_detail`.`InventoryInternalID` 
                                inner join `m_uom` on `m_uom`.`InternalID` = `t_purchaseorder_detail`.`UomInternalID` 
                                inner join t_mrv_header on t_mrv_header.PurchaseOrderInternalID = t_purchaseorder_header.InternalID 
                                inner join t_purchase_header on t_purchase_header.MrvInternalID = t_mrv_header.InternalID 
                                inner join t_purchase_detail on t_purchase_detail.PurchaseInternalID = t_purchase_header.InternalID and t_purchase_detail.InventoryInternalID = m_inventory.InternalID WHERE  
										  (t_purchase_header.GrandTotal <= (SELECT IFNULL(SUM(jd.JournalDebet),0) 
                                FROM t_journal_detail jd WHERE jd.JournalTransactionID = t_purchase_header.PurchaseID)) AND `t_purchase_header`.`PurchaseDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_purchase_header.PurchaseID ASC");
                    } else if ($payment == 'no_paid') {
                        $model = DB::select("SELECT t_purchase_header.*, `t_purchaseorder_header`.`isCash` as `isCash`,`t_purchase_detail`.`Qty` as `Qty`, m_inventory.InternalID as InventoryID,m_inventory.`InventoryName` as `InventoryName`, 
                    `m_uom`.`UomID` as `UomID`, `t_purchase_detail`.`SubTotal` as `SubTotal`, 
                    `m_coa6`.`ACC6Name` as `CustomerName`, `t_purchaseorder_header`.`DownPayment` as `dpSO`,
                    t_purchase_detail.Price as price, t_purchase_detail.Vat as Vat, 
                    t_purchase_detail.Discount, t_purchase_detail.DiscountNominal  FROM
                                t_purchaseorder_header  inner join `t_purchaseorder_detail` on `t_purchaseorder_detail`.`PurchaseOrderInternalID` = `t_purchaseorder_header`.`InternalID` 
                                inner join `m_coa6` on `m_coa6`.`InternalID` = `t_purchaseorder_header`.`ACC6InternalID`    
                                inner join `m_inventory` on `m_inventory`.`InternalID` = `t_purchaseorder_detail`.`InventoryInternalID` 
                                inner join `m_uom` on `m_uom`.`InternalID` = `t_purchaseorder_detail`.`UomInternalID` 
                                inner join t_mrv_header on t_mrv_header.PurchaseOrderInternalID = t_purchaseorder_header.InternalID 
                                inner join t_purchase_header on t_purchase_header.MrvInternalID = t_mrv_header.InternalID 
                                inner join t_purchase_detail on t_purchase_detail.PurchaseInternalID = t_purchase_header.InternalID and t_purchase_detail.InventoryInternalID = m_inventory.InternalID WHERE  
										  (t_purchase_header.GrandTotal > (SELECT IFNULL(SUM(jd.JournalDebet),0) 
                                FROM t_journal_detail jd WHERE jd.JournalTransactionID = t_purchase_header.PurchaseID)) AND t_purchase_header.isCash in(1,4) AND `t_purchase_header`.`PurchaseDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_purchase_header.PurchaseID ASC");
                    } else if ($payment == 'all') {
                        $model = DB::select("SELECT t_purchase_header.*, `t_purchaseorder_header`.`isCash` as `isCash`,`t_purchase_detail`.`Qty` as `Qty`, m_inventory.InternalID as InventoryID,m_inventory.`InventoryName` as `InventoryName`, 
                    `m_uom`.`UomID` as `UomID`, `t_purchase_detail`.`SubTotal` as `SubTotal`, 
                    `m_coa6`.`ACC6Name` as `CustomerName`, `t_purchaseorder_header`.`DownPayment` as `dpSO`,
                    t_purchase_detail.Price as price, t_purchase_detail.Vat as Vat, 
                    t_purchase_detail.Discount, t_purchase_detail.DiscountNominal  FROM
                                t_purchaseorder_header  inner join `t_purchaseorder_detail` on `t_purchaseorder_detail`.`PurchaseOrderInternalID` = `t_purchaseorder_header`.`InternalID` 
                                inner join `m_coa6` on `m_coa6`.`InternalID` = `t_purchaseorder_header`.`ACC6InternalID`                              
                                inner join `m_inventory` on `m_inventory`.`InternalID` = `t_purchaseorder_detail`.`InventoryInternalID` 
                                inner join `m_uom` on `m_uom`.`InternalID` = `t_purchaseorder_detail`.`UomInternalID` 
                                inner join t_mrv_header on t_mrv_header.PurchaseOrderInternalID = t_purchaseorder_header.InternalID 
                                inner join t_purchase_header on t_purchase_header.MrvInternalID = t_mrv_header.InternalID 
                                inner join t_purchase_detail on t_purchase_detail.PurchaseInternalID = t_purchase_header.InternalID and t_purchase_detail.InventoryInternalID = m_inventory.InternalID WHERE  
										     `t_purchase_header`.`PurchaseDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_purchase_header.PurchaseID ASC");
                    } else if ($payment == 'paid') {
                        $model = DB::select("SELECT t_purchase_header.*, `t_purchaseorder_header`.`isCash` as `isCash`,`t_purchase_detail`.`Qty` as `Qty`, m_inventory.InternalID as InventoryID,m_inventory.`InventoryName` as `InventoryName`, 
                    `m_uom`.`UomID` as `UomID`, `t_purchase_detail`.`SubTotal` as `SubTotal`, 
                    `m_coa6`.`ACC6Name` as `CustomerName`, `t_purchaseorder_header`.`DownPayment` as `dpSO`,
                    t_purchase_detail.Price as price, t_purchase_detail.Vat as Vat, 
                    t_purchase_detail.Discount, t_purchase_detail.DiscountNominal  FROM
                                t_purchaseorder_header  inner join `t_purchaseorder_detail` on `t_purchaseorder_detail`.`PurchaseOrderInternalID` = `t_purchaseorder_header`.`InternalID` 
                                inner join `m_coa6` on `m_coa6`.`InternalID` = `t_purchaseorder_header`.`ACC6InternalID` 
                                inner join `m_inventory` on `m_inventory`.`InternalID` = `t_purchaseorder_detail`.`InventoryInternalID` 
                                inner join `m_uom` on `m_uom`.`InternalID` = `t_purchaseorder_detail`.`UomInternalID` 
                               inner join t_mrv_header on t_mrv_header.PurchaseOrderInternalID = t_purchaseorder_header.InternalID 
                                inner join t_purchase_header on t_purchase_header.MrvInternalID = t_mrv_header.InternalID 
                                inner join t_purchase_detail on t_purchase_detail.PurchaseInternalID = t_purchase_header.InternalID and t_purchase_detail.InventoryInternalID = m_inventory.InternalID WHERE  
										  ((SELECT IFNULL(SUM(jd.JournalDebet),0) 
                                FROM t_journal_detail jd WHERE jd.JournalTransactionID = t_purchase_header.PurchaseID) > 0) AND `t_purchase_header`.`PurchaseDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_purchase_header.PurchaseID ASC");
                    }
                } else {
                    // SUMMARY

                    if ($payment == 'complete') {
                        $model = DB::select("SELECT t_purchase_header.*, `t_purchaseorder_header`.`isCash` as `isCash`, `m_coa6`.`ACC6Name` as `CustomerName` , `t_purchaseorder_header`.`DownPayment` as `dpSO` 
                   FROM t_purchaseorder_header  inner join `m_coa6` on `m_coa6`.`InternalID` = `t_purchaseorder_header`.`ACC6InternalID` 
                                inner join t_mrv_header on t_mrv_header.PurchaseOrderInternalID = t_purchaseorder_header.InternalID 
                                inner join t_purchase_header on t_purchase_header.MrvInternalID = t_mrv_header.InternalID 
                                 WHERE (t_purchase_header.GrandTotal <= (SELECT IFNULL(SUM(jd.JournalDebet),0) 
                                FROM t_journal_detail jd WHERE jd.JournalTransactionID = t_purchase_header.PurchaseID)) AND `t_purchase_header`.`PurchaseDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_purchase_header.PurchaseID ASC");
                    } else if ($payment == 'no_paid') {
                        $model = DB::select("SELECT t_purchase_header.*, `t_purchaseorder_header`.`isCash` as `isCash`, `m_coa6`.`ACC6Name` as `CustomerName` , `t_purchaseorder_header`.`DownPayment` as `dpSO` FROM
                                 t_purchaseorder_header inner join `m_coa6` on `m_coa6`.`InternalID` = `t_purchaseorder_header`.`ACC6InternalID`                  
                                inner join t_mrv_header on t_mrv_header.PurchaseOrderInternalID = t_purchaseorder_header.InternalID 
                                inner join t_purchase_header on t_purchase_header.MrvInternalID = t_mrv_header.InternalID 
                                WHERE (t_purchase_header.GrandTotal > (SELECT IFNULL(SUM(jd.JournalDebet),0) 
                                FROM t_journal_detail jd WHERE jd.JournalTransactionID = t_purchase_header.PurchaseID)) AND t_purchase_header.isCash in(1,4) AND `t_purchase_header`.`PurchaseDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_purchase_header.PurchaseID ASC");
                    } else if ($payment == 'all') {
                        $model = DB::select("SELECT t_purchase_header.*, `t_purchaseorder_header`.`isCash` as `isCash`, `m_coa6`.`ACC6Name` as `CustomerName`,  `t_purchaseorder_header`.`DownPayment` as `dpSO`
                   FROM t_purchaseorder_header inner join `m_coa6` on `m_coa6`.`InternalID` = `t_purchaseorder_header`.`ACC6InternalID`                                
                                inner join t_mrv_header on t_mrv_header.PurchaseOrderInternalID = t_purchaseorder_header.InternalID 
                                inner join t_purchase_header on t_purchase_header.MrvInternalID = t_mrv_header.InternalID 
                               WHERE `t_purchase_header`.`PurchaseDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_purchase_header.PurchaseID ASC");
                    } else if ($payment == 'paid') {
                        $model = DB::select("SELECT t_purchase_header.*, `t_purchaseorder_header`.`isCash` as `isCash`,`m_coa6`.`ACC6Name` as `CustomerName`, `t_purchaseorder_header`.`DownPayment` as `dpSO` 
                   FROM t_purchaseorder_header  inner join `m_coa6` on `m_coa6`.`InternalID` = `t_purchaseorder_header`.`ACC6InternalID` 
                                inner join t_mrv_header on t_mrv_header.PurchaseOrderInternalID = t_purchaseorder_header.InternalID 
                                inner join t_purchase_header on t_purchase_header.MrvInternalID = t_mrv_header.InternalID 
                                WHERE ((SELECT IFNULL(SUM(jd.JournalDebet),0) 
                                FROM t_journal_detail jd WHERE jd.JournalTransactionID = t_purchase_header.PurchaseID) > 0) AND `t_purchase_header`.`PurchaseDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_purchase_header.PurchaseID ASC");
                    }
                }

                $xx = $yy = 0;
                if (count($model) > 0) {
                    $checklast = 0;
                    $totalVATDP = 0;
                    $noinvoice = '';
                    $rowawal = $row;
                    foreach ($model as $a) {
                        if ($noinvoice != $a->PurchaseID) {
                            $tulis = "ya";
                            $rowawal = $row;
                        } else {
                            $tulis = "tidak";
                        }
                        $noinvoice = $a->PurchaseID;
                        $TotalDP = 0;
                        $py = PurchaseHeader::purchasePayableReport2($a->PurchaseID, $payment);
                        $sum = PurchaseAddDetail::where('PurchaseInternalID', $a->InternalID)->sum('SubTotal');
                        $detail = PurchaseAddDetail::where('PurchaseInternalID', $a->InternalID)->get();
                        if ($type == "Detail") {
                            $total = $a->SubTotal;
                        } else {
                            if ($a->VAT == 1) {
                                $total = $a->GrandTotal / 1.1;
                            } else {
                                $total = $a->GrandTotal;
                            }
                        }
                        if ($sum != 0)
                            $discountGlobal = $a->DiscountGlobal;
//                            $discountGlobal = ($a->DiscountGlobal * $total) / $sum;
                        else
                            $discountGlobal = 0;
                        $sum = ceil($sum);
                        $grand = $sum - $discountGlobal;
                        $vatHeader = $grand - $a->DownPayment;
                        $vatgrand = floor($vatHeader * 0.1);
                        $grandHeader = $sum - $a->DiscountGlobal;
                        $ppnHeader = floor($grandHeader * 0.1);
                        if ($grandHeader != 0)
                            $ppn = round(($total * $ppnHeader) / $grandHeader, 0);
                        else
                            $ppn = 0;
                        if ($sum != 0)
                            $DP = round(($total * $a->DownPayment) / $sum);
                        else
                            $DP = 0;
                        $TotalDP = ceil($grand - $a->DownPayment);
//                        if ($TotalDP % 10 != 0)
//                            $TotalDP++;
                        $vatdp = floor($TotalDP * 0.1);
                        $totalVATDP += $vatdp;

                        $checklast++;
                        if ($checklast == count($detail)) {
                            $checklast = 0;

                            if ($vatgrand != $totalVATDP) {
                                $selisih = $vatgrand - $totalVATDP;
                                $vatdp = $vatdp + $selisih;
                                $totalVATDP = 0;
                            } else {
                                $totalVATDP = 0;
                            }
                        }
                        $grandTotalDP = round($TotalDP + $vatdp);

                        if ($type == "Detail") {
                            $sheet->setBorder('B' . $row . ':T' . $row, 'thin');
                            if ($tulis == "ya") {
                                if ($xx != 0)
                                    $row++;
                                else
                                    $xx++;
                                $sheet->setCellValueByColumnAndRow(1, $row, date("d-M-y", strtotime($a->PurchaseDate)));
                                $sheet->setCellValueByColumnAndRow(2, $row, $a->PurchaseID);
                                $sheet->setCellValueByColumnAndRow(3, $row, $a->CustomerName);
                                $sheet->setCellValueByColumnAndRow(4, $row, $a->TaxNumber);
                                if ($a->VAT == 1)
                                    $sheet->setCellValueByColumnAndRow(5, $row, "PPN");
                                else
                                    $sheet->setCellValueByColumnAndRow(5, $row, "Non PPN");
                                $sheet->setBorder('B' . $row . ':T' . $row, 'thin');
                                $row++;
                                $totgrand = 0;
                            }
                            $sheet->setCellValueByColumnAndRow(6, $row, $a->InventoryName);
                            $sheet->setCellValueByColumnAndRow(7, $row, number_format($a->Qty, '3', '.', ','));
                            $sheet->setCellValueByColumnAndRow(8, $row, $a->UomID);
                            $sheet->setCellValueByColumnAndRow(9, $row, number_format($a->price, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(10, $row, number_format($a->Discount, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(11, $row, number_format($a->DiscountNominal, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(12, $row, number_format(ceil($a->SubTotal), '2', '.', ','));
                            $totgrand += ceil($a->SubTotal);
                            if ($tulis == "ya") {
                                if ($yy != 0)
                                    $rowawal++;
                                else
                                    $yy++;
                                $sheet->setCellValueByColumnAndRow(12, $rowawal, number_format($totgrand, '2', '.', ','));
                                $sheet->setCellValueByColumnAndRow(13, $rowawal, number_format($discountGlobal, '2', '.', ','));
                                $sheet->setCellValueByColumnAndRow(14, $rowawal, number_format($a->dpSO, '2', '.', ','));
                                $sheet->setCellValueByColumnAndRow(15, $rowawal, number_format($a->DownPayment, '2', '.', ','));
                                $sheet->setCellValueByColumnAndRow(16, $rowawal, number_format($TotalDP, '2', '.', ','));
                                $sheet->setCellValueByColumnAndRow(17, $rowawal, number_format($vatdp, '2', '.', ','));
                                $sheet->setCellValueByColumnAndRow(18, $rowawal, number_format($grandTotalDP, '2', '.', ','));
                                $sheet->setCellValueByColumnAndRow(19, $rowawal, $py);
                                $sheet->setBorder('B' . $row . ':T' . $row, 'thin');
                                $total1 += $totgrand;
                                $total2 += $discountGlobal;
                                $total3 += $a->DownPayment;
                                $total4 += $TotalDP;
                                $total5 += $vatdp;
                                $total6 += $grandTotalDP;
                            }
                        } else {
                            $total1 += $sum;
                            $total2 += $discountGlobal;
                            $total3 += $a->DownPayment;
                            $total4 += $TotalDP;
                            $total5 += $vatdp;
                            $total6 += $grandTotalDP;
                            $sheet->setBorder('B' . $row . ':N' . $row, 'thin');
                            $sheet->setCellValueByColumnAndRow(1, $row, date("d-M-y", strtotime($a->PurchaseDate)));
                            $sheet->setCellValueByColumnAndRow(2, $row, $a->PurchaseID);
                            $sheet->setCellValueByColumnAndRow(3, $row, $a->CustomerName);
                            $sheet->setCellValueByColumnAndRow(4, $row, $a->TaxNumber);
                            if ($a->VAT == 1)
                                $sheet->setCellValueByColumnAndRow(5, $row, "PPN");
                            else
                                $sheet->setCellValueByColumnAndRow(5, $row, "Non PPN");
                            $sheet->setCellValueByColumnAndRow(6, $row, number_format($sum, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(7, $row, number_format($discountGlobal, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(8, $row, number_format($a->dpSO, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(9, $row, number_format($a->DownPayment, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(10, $row, number_format($TotalDP, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(11, $row, number_format($vatdp, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(12, $row, number_format($grandTotalDP, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(13, $row, $py);
                        }
                        $row++;
                        $hitung++;
                    }
                }

                if ($hitung == 0) {
                    if ($type == "Detail")
                        $sheet->mergeCells('B' . $row . ':T' . $row);
                    else
                        $sheet->mergeCells('B' . $row . ':N' . $row);
                    $sheet->setCellValueByColumnAndRow(1, $row, "There is no report.");
                }
                $sheet->cells('B2:B5', function($cells) {
                    $cells->setAlignment('left');
                    $cells->setFontWeight('bold');
                });
                if ($type == "Detail") {
                    $sheet->setCellValueByColumnAndRow(1, $row, "Total");
                    $sheet->setCellValueByColumnAndRow(12, $row, number_format($total1, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(13, $row, number_format($total2, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(15, $row, number_format($total3, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(16, $row, number_format($total4, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(17, $row, number_format($total5, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(18, $row, number_format($total6, '2', '.', ','));
                    $sheet->cells('B6:T6', function($cells) {
                        $cells->setAlignment('middle');
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('B' . $row, function($cells) {
                        $cells->setAlignment('middle');
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('J7:S' . $row, function($cells) {
                        $cells->setAlignment('right');
                    });
                } else {
                    $sheet->setCellValueByColumnAndRow(1, $row, "Total");
                    $sheet->setCellValueByColumnAndRow(6, $row, number_format($total1, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(7, $row, number_format($total2, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(9, $row, number_format($total3, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(10, $row, number_format($total4, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(11, $row, number_format($total5, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(12, $row, number_format($total6, '2', '.', ','));
                    $sheet->cells('B6:N6', function($cells) {
                        $cells->setAlignment('middle');
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('B' . $row, function($cells) {
                        $cells->setAlignment('middle');
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('G7:M' . $row, function($cells) {
                        $cells->setAlignment('right');
                    });
                }
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });

//                $sheet->cells('B3:G' . $row, function($cells) {
//                    $cells->setAlignment('left');
//                    $cells->setValignment('middle');
//                });
//                $sheet->cells('B3:B' . $row, function($cells) {
//                    $cells->setAlignment('left');
//                });
            });
        })->export('xls');
    }

    public function salesReturnDetailReport() {
        Excel::create('Sales_Return_Report', function($excel) {
            $excel->sheet('Sales_Return_Report', function($sheet) {
                $startT = explode('-', Input::get('sDate'));
                $endT = explode('-', Input::get('eDate'));
                $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
                $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
                $customer = Input::get('customer');
//                $salesman = Input::get('salesman');
                $type = Input::get('type');
                $payment = Input::get('payment');
                if ($payment == "all") {
                    $payname = "All";
                } else if ($payment == "complete") {
                    $payname = "Complete";
                } else if ($payment == "paid") {
                    $payname = "Paid";
                } else {
                    $payname = "No Paid";
                }
                $total1 = $total2 = $total3 = $total4 = $total5 = $total6 = 0;
                //customer
                $c = '';
                //salesman
                $p = '';
                //cek PPN dan non PPN
                $ci = '';
                $v = '';
                $py = '';
                $sttus = '';
                if ($customer != 'all') {
                    $cusname = Coa6::find($customer)->ACC6Name;
                    $c = " AND m_coa6.InternalID = $customer";
                } else {
                    $cusname = "All";
                }
//                if ($salesman != 'all') {
//                    $salname = SalesMan::find($salesman)->SalesManName;
//                    $p = " AND m_sales_man.InternalID = $salesman";
//                } else {
//                    $salname = "All";
//                }
                if (Auth::user()->SeeNPPN == 1) {
                    //NON PPN 
                    $ci = " AND t_salesreturn_header.VAT = 0";
                } else {
                    //PPN 
                    $ci = " AND t_salesreturn_header.VAT = 1";
                }
                if ($payment != 'all') {
                    $sttus = ' AND t_salesreturn_header.`GrandTotal`*t_salesreturn_header.`CurrencyRate` > (Select IFNULL(SUM(td.JournalCredit),0) From t_journal_detail td Where td.JournalTransactionID = t_salesreturn_header.`SalesReturnID`)';
                }

                $totalPO = 0;
                if ($type == "Detail")
                    $sheet->mergeCells('B1:P1');
                else
                    $sheet->mergeCells('B1:J1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Sales Return Report By " . $type);
                $sheet->mergeCells('C2:D2');
                $sheet->setCellValueByColumnAndRow(1, 2, "Period :");
                $sheet->setCellValueByColumnAndRow(2, 2, $start . ' to ' . $end);
                $sheet->setCellValueByColumnAndRow(1, 3, "Customer :");
                $sheet->setCellValueByColumnAndRow(2, 3, $cusname);
//                $sheet->setCellValueByColumnAndRow(1, 4, "Salesman :");
//                $sheet->setCellValueByColumnAndRow(2, 4, $salname);
                $sheet->setCellValueByColumnAndRow(1, 4, "AR Payment :");
                $sheet->setCellValueByColumnAndRow(2, 4, $payname);
                $hitung = 0;
                $row = 5;
                if ($type == "Detail") {
                    $sheet->cells('B' . $row . ':P' . $row, function($cells) {
                        $cells->setBackground('#eaf6f7');
                        $cells->setValignment('middle');
                        $cells->setAlignment('center');
                    });
                    $sheet->setBorder('B' . $row . ':P' . $row, 'thin');
                    $sheet->setCellValueByColumnAndRow(1, $row, "Date");
                    $sheet->setCellValueByColumnAndRow(2, $row, "Retur No");
                    $sheet->setCellValueByColumnAndRow(3, $row, "Customer");
//                    $sheet->setCellValueByColumnAndRow(4, $row, "Tax Number");
//                    $sheet->setCellValueByColumnAndRow(5, $row, "Sales Man");
                    $sheet->setCellValueByColumnAndRow(4, $row, "Product");
                    $sheet->setCellValueByColumnAndRow(5, $row, "Quantity");
                    $sheet->setCellValueByColumnAndRow(6, $row, "UOM");
                    $sheet->setCellValueByColumnAndRow(7, $row, "Nominal");
                    $sheet->setCellValueByColumnAndRow(8, $row, "Discount");
                    $sheet->setCellValueByColumnAndRow(9, $row, "Discount Nominal");
                    $sheet->setCellValueByColumnAndRow(10, $row, "Total");
                    $sheet->setCellValueByColumnAndRow(11, $row, "Discount Global");
//                    $sheet->setCellValueByColumnAndRow(12, $row, "DP (%)");
//                    $sheet->setCellValueByColumnAndRow(13, $row, "DP");
                    $sheet->setCellValueByColumnAndRow(12, $row, "GrandTotal");
                    $sheet->setCellValueByColumnAndRow(13, $row, "VAT");
                    $sheet->setCellValueByColumnAndRow(14, $row, "GrandTotal(VAT)");
                    $sheet->setCellValueByColumnAndRow(15, $row, "AR Payment");
                } else {
                    $sheet->cells('B' . $row . ':J' . $row, function($cells) {
                        $cells->setBackground('#eaf6f7');
                        $cells->setValignment('middle');
                        $cells->setAlignment('center');
                    });
                    $sheet->setBorder('B' . $row . ':J' . $row, 'thin');
                    $sheet->setCellValueByColumnAndRow(1, $row, "Date");
                    $sheet->setCellValueByColumnAndRow(2, $row, "Invoice No");
                    $sheet->setCellValueByColumnAndRow(3, $row, "Customer");
//                    $sheet->setCellValueByColumnAndRow(4, $row, "Tax Number");
//                    $sheet->setCellValueByColumnAndRow(5, $row, "Sales Man");
                    $sheet->setCellValueByColumnAndRow(4, $row, "Total");
                    $sheet->setCellValueByColumnAndRow(5, $row, "Discount Global");
//                    $sheet->setCellValueByColumnAndRow(6, $row, "DP (%)");
//                    $sheet->setCellValueByColumnAndRow(7, $row, "DP");
                    $sheet->setCellValueByColumnAndRow(6, $row, "GrandTotal");
                    $sheet->setCellValueByColumnAndRow(7, $row, "VAT");
                    $sheet->setCellValueByColumnAndRow(8, $row, "GrandTotal(VAT)");
                    $sheet->setCellValueByColumnAndRow(9, $row, "AR Payment");
                }
                $row++;
                if ($type == "Detail") {
                    // DETAIL 

                    if ($payment == 'complete') {
                        $model = DB::select("SELECT t_salesreturn_header.*,`t_salesreturn_detail`.`Qty` as `Qty`, m_inventory.InternalID as InventoryID,m_inventory.`InventoryName` as `InventoryName`, 
                    `m_uom`.`UomID` as `UomID`, `t_salesreturn_detail`.`SubTotal` as `SubTotal`, 
                    `m_coa6`.`ACC6Name` as `CustomerName`,
                    t_salesreturn_detail.Price as price, t_salesreturn_detail.Vat as Vat, 
                    t_salesreturn_detail.Discount, t_salesreturn_detail.DiscountNominal  FROM
                                t_salesreturn_header  
                                inner join t_salesreturn_detail on t_salesreturn_detail.SalesReturnInternalID = t_salesreturn_header.InternalID 
                                inner join `m_coa6` on `m_coa6`.`InternalID` = `t_salesreturn_header`.`ACC6InternalID` 
                                inner join `m_inventory` on `m_inventory`.`InternalID` = `t_salesreturn_detail`.`InventoryInternalID` 
                                inner join `m_uom` on `m_uom`.`InternalID` = `t_salesreturn_detail`.`UomInternalID` 
                                WHERE t_salesreturn_detail.InventoryInternalID = m_inventory.InternalID and 
										  (t_salesreturn_header.GrandTotal <= (SELECT IFNULL(SUM(jd.JournalCredit),0) 
                                FROM t_journal_detail jd WHERE jd.JournalTransactionID = t_salesreturn_header.SalesReturnID)) AND `t_salesreturn_header`.`SalesReturnDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_salesreturn_header.SalesReturnID ASC");
                    } else if ($payment == 'no_paid') {
                        $model = DB::select("SELECT t_salesreturn_header.*,`t_salesreturn_detail`.`Qty` as `Qty`, m_inventory.InternalID as InventoryID,m_inventory.`InventoryName` as `InventoryName`, 
                    `m_uom`.`UomID` as `UomID`, `t_salesreturn_detail`.`SubTotal` as `SubTotal`, 
                    `m_coa6`.`ACC6Name` as `CustomerName`,
                    t_salesreturn_detail.Price as price, t_salesreturn_detail.Vat as Vat, 
                    t_salesreturn_detail.Discount, t_salesreturn_detail.DiscountNominal  FROM
                                t_salesreturn_header  
                               inner join t_salesreturn_detail on t_salesreturn_detail.SalesReturnInternalID = t_salesreturn_header.InternalID 
                                inner join `m_coa6` on `m_coa6`.`InternalID` = `t_salesreturn_header`.`ACC6InternalID` 
                                inner join `m_inventory` on `m_inventory`.`InternalID` = `t_salesreturn_detail`.`InventoryInternalID` 
                                inner join `m_uom` on `m_uom`.`InternalID` = `t_salesreturn_detail`.`UomInternalID` 
                                WHERE t_salesreturn_detail.InventoryInternalID = m_inventory.InternalID and   
										  (t_salesreturn_header.GrandTotal > (SELECT IFNULL(SUM(jd.JournalCredit),0) 
                                FROM t_journal_detail jd WHERE jd.JournalTransactionID = t_salesreturn_header.SalesReturnID)) AND t_salesreturn_header.isCash in(1,4) AND `t_salesreturn_header`.`SalesReturnDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_salesreturn_header.SalesReturnID ASC");
                    } else if ($payment == 'all') {
                        $model = DB::select("SELECT t_salesreturn_header.*,`t_salesreturn_detail`.`Qty` as `Qty`, m_inventory.InternalID as InventoryID,m_inventory.`InventoryName` as `InventoryName`, 
                    `m_uom`.`UomID` as `UomID`, `t_salesreturn_detail`.`SubTotal` as `SubTotal`, 
                    `m_coa6`.`ACC6Name` as `CustomerName`,
                    t_salesreturn_detail.Price as price, t_salesreturn_detail.Vat as Vat, 
                    t_salesreturn_detail.Discount, t_salesreturn_detail.DiscountNominal  FROM
                                t_salesreturn_header  
                               inner join t_salesreturn_detail on t_salesreturn_detail.SalesReturnInternalID = t_salesreturn_header.InternalID 
                                inner join `m_coa6` on `m_coa6`.`InternalID` = `t_salesreturn_header`.`ACC6InternalID` 
                                inner join `m_inventory` on `m_inventory`.`InternalID` = `t_salesreturn_detail`.`InventoryInternalID` 
                                inner join `m_uom` on `m_uom`.`InternalID` = `t_salesreturn_detail`.`UomInternalID` 
                                WHERE t_salesreturn_detail.InventoryInternalID = m_inventory.InternalID and   
										     `t_salesreturn_header`.`SalesReturnDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_salesreturn_header.SalesReturnID ASC");
                    } else if ($payment == 'paid') {
                        $model = DB::select("SELECT t_salesreturn_header.*,`t_salesreturn_detail`.`Qty` as `Qty`, m_inventory.InternalID as InventoryID,m_inventory.`InventoryName` as `InventoryName`, 
                    `m_uom`.`UomID` as `UomID`, `t_salesreturn_detail`.`SubTotal` as `SubTotal`, 
                    `m_coa6`.`ACC6Name` as `CustomerName`,
                    t_salesreturn_detail.Price as price, t_salesreturn_detail.Vat as Vat, 
                    t_salesreturn_detail.Discount, t_salesreturn_detail.DiscountNominal  FROM
                                t_salesreturn_header  
                                inner join t_salesreturn_detail on t_salesreturn_detail.SalesReturnInternalID = t_salesreturn_header.InternalID 
                                inner join `m_coa6` on `m_coa6`.`InternalID` = `t_salesreturn_header`.`ACC6InternalID` 
                                inner join `m_inventory` on `m_inventory`.`InternalID` = `t_salesreturn_detail`.`InventoryInternalID` 
                                inner join `m_uom` on `m_uom`.`InternalID` = `t_salesreturn_detail`.`UomInternalID` 
                                WHERE t_salesreturn_detail.InventoryInternalID = m_inventory.InternalID and   
										  ((SELECT IFNULL(SUM(jd.JournalCredit),0) 
                                FROM t_journal_detail jd WHERE jd.JournalTransactionID = t_salesreturn_header.SalesReturnID) > 0) AND `t_salesreturn_header`.`SalesReturnDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_salesreturn_header.SalesReturnID ASC");
                    }
                } else {
                    // SUMMARY

                    if ($payment == 'complete') {
                        $model = DB::select("SELECT t_salesreturn_header.*, `m_coa6`.`ACC6Name` as `CustomerName`  
                   FROM t_salesreturn_header  inner join `m_coa6` on `m_coa6`.`InternalID` = `t_salesreturn_header`.`ACC6InternalID` 
                                 WHERE (t_salesreturn_header.GrandTotal <= (SELECT IFNULL(SUM(jd.JournalCredit),0) 
                                FROM t_journal_detail jd WHERE jd.JournalTransactionID = t_salesreturn_header.SalesReturnID)) AND `t_salesreturn_header`.`SalesReturnDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_salesreturn_header.SalesReturnID ASC");
                    } else if ($payment == 'no_paid') {
                        $model = DB::select("SELECT t_salesreturn_header.*, `m_coa6`.`ACC6Name` as `CustomerName`  FROM
                                 t_salesreturn_header inner join `m_coa6` on `m_coa6`.`InternalID` = `t_salesreturn_header`.`ACC6InternalID`                                                                
                                WHERE (t_salesreturn_header.GrandTotal > (SELECT IFNULL(SUM(jd.JournalCredit),0) 
                                FROM t_journal_detail jd WHERE jd.JournalTransactionID = t_salesreturn_header.SalesReturnID)) AND t_salesreturn_header.isCash in(1,4) AND `t_salesreturn_header`.`SalesReturnDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_salesreturn_header.SalesReturnID ASC");
                    } else if ($payment == 'all') {
                        $model = DB::select("SELECT t_salesreturn_header.*, `m_coa6`.`ACC6Name` as `CustomerName`
                   FROM t_salesreturn_header inner join `m_coa6` on `m_coa6`.`InternalID` = `t_salesreturn_header`.`ACC6InternalID`                               
                               WHERE `t_salesreturn_header`.`SalesReturnDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_salesreturn_header.SalesReturnID ASC");
                    } else if ($payment == 'paid') {
                        $model = DB::select("SELECT t_salesreturn_header.*,`m_coa6`.`ACC6Name` as `CustomerName` 
                   FROM t_salesreturn_header  inner join `m_coa6` on `m_coa6`.`InternalID` = `t_salesreturn_header`.`ACC6InternalID` 
                                WHERE ((SELECT IFNULL(SUM(jd.JournalCredit),0) 
                                FROM t_journal_detail jd WHERE jd.JournalTransactionID = t_salesreturn_header.SalesReturnID) > 0) AND `t_salesreturn_header`.`SalesReturnDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_salesreturn_header.SalesReturnID ASC");
                    }
                }

                $xx = $yy = 0;
                if (count($model) > 0) {
                    $checklast = 0;
                    $totalVATDP = 0;
                    $noinvoice = '';
                    $rowawal = $row;
                    foreach ($model as $a) {
                        if ($noinvoice != $a->SalesReturnID) {
                            $tulis = "ya";
                            $rowawal = $row;
                        } else {
                            $tulis = "tidak";
                        }
                        $noinvoice = $a->SalesReturnID;
                        $TotalDP = 0;
                        $py = SalesReturnHeader::salesReceivableReport($a->SalesReturnID, $payment);
                        $sum = SalesReturnDetail::where('SalesReturnInternalID', $a->InternalID)->sum('SubTotal');
                        $detail = SalesReturnDetail::where('SalesReturnInternalID', $a->InternalID)->get();
                        if ($type == "Detail") {
                            $total = $a->SubTotal;
                        } else {
                            if ($a->VAT == 1) {
                                $total = $a->GrandTotal / 1.1;
                            } else {
                                $total = $a->GrandTotal;
                            }
                        }
                        if ($sum != 0)
                            $discountGlobal = $a->DiscountGlobal;
//                            $discountGlobal = ($a->DiscountGlobal * $total) / $sum;
                        else
                            $discountGlobal = 0;
                        $grand = ceil($sum) - $discountGlobal;
                        $vatHeader = $grand ;
                        $vatgrand = floor($vatHeader * 0.1);
                        $grandHeader = ceil($sum) - $a->DiscountGlobal;
                        $ppnHeader = floor($grandHeader * 0.1);
                        if ($grandHeader != 0)
                            $ppn = round(($total * $ppnHeader) / $grandHeader, 0);
                        else
                            $ppn = 0;
                       
                        $TotalDP = ceil($grand);
//                        if ($TotalDP % 10 != 0)
//                            $TotalDP++;
                        $vatdp = floor($TotalDP * 0.1);
                        $totalVATDP += $vatdp;

                        $checklast++;
                        if ($checklast == count($detail)) {
                            $checklast = 0;

                            if ($vatgrand != $totalVATDP) {
                                $selisih = $vatgrand - $totalVATDP;
                                $vatdp = $vatdp + $selisih;
                                $totalVATDP = 0;
                            } else {
                                $totalVATDP = 0;
                            }
                        }
                        $grandTotalDP = round($TotalDP + $vatdp);

                        if ($type == "Detail") {
                            $sheet->setBorder('B' . $row . ':P' . $row, 'thin');
                            if ($tulis == "ya") {
                                if ($xx != 0)
                                    $row++;
                                else
                                    $xx++;
                                $sheet->setCellValueByColumnAndRow(1, $row, date("d-M-y", strtotime($a->SalesReturnDate)));
                                $sheet->setCellValueByColumnAndRow(2, $row, $a->SalesReturnID);
                                $sheet->setCellValueByColumnAndRow(3, $row, $a->CustomerName);
//                                $sheet->setCellValueByColumnAndRow(4, $row, $a->TaxNumber);
//                                $sheet->setCellValueByColumnAndRow(5, $row, $a->SalesManName);
                                $sheet->setBorder('B' . $row . ':P' . $row, 'thin');
                                $row++;
                                $totgrand = 0;
                            }
                            $sheet->setCellValueByColumnAndRow(4, $row, $a->InventoryName);
                            $sheet->setCellValueByColumnAndRow(5, $row, number_format($a->Qty, '3', '.', ','));
                            $sheet->setCellValueByColumnAndRow(6, $row, $a->UomID);
                            $sheet->setCellValueByColumnAndRow(7, $row, number_format($a->price, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(8, $row, number_format($a->Discount, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(9, $row, number_format($a->DiscountNominal, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(10, $row, number_format(ceil($a->SubTotal), '2', '.', ','));
                            $totgrand += ceil($a->SubTotal);
                            if ($tulis == "ya") {
                                if ($yy != 0)
                                    $rowawal++;
                                else
                                    $yy++;
                                $sheet->setCellValueByColumnAndRow(10, $rowawal, number_format($totgrand, '2', '.', ','));
                                $sheet->setCellValueByColumnAndRow(11, $rowawal, number_format($discountGlobal, '2', '.', ','));
//                                $sheet->setCellValueByColumnAndRow(14, $rowawal, number_format($a->dpSO, '2', '.', ','));
//                                $sheet->setCellValueByColumnAndRow(15, $rowawal, number_format($a->DownPayment, '2', '.', ','));
                                $sheet->setCellValueByColumnAndRow(12, $rowawal, number_format($TotalDP, '2', '.', ','));
                                $sheet->setCellValueByColumnAndRow(13, $rowawal, number_format($vatdp, '2', '.', ','));
                                $sheet->setCellValueByColumnAndRow(14, $rowawal, number_format($grandTotalDP, '2', '.', ','));
                                $sheet->setCellValueByColumnAndRow(15, $rowawal, $py);
                                $sheet->setBorder('B' . $row . ':P' . $row, 'thin');
                                $total1 += $totgrand;
                                $total2 += $discountGlobal;
                                $total3 += 0;
                                $total4 += $TotalDP;
                                $total5 += $vatdp;
                                $total6 += $grandTotalDP;
                            }
                        } else {
                            $total1 += ceil($sum);
                            $total2 += $discountGlobal;
                            $total3 += 0;
                            $total4 += $TotalDP;
                            $total5 += $vatdp;
                            $total6 += $grandTotalDP;
                            $sheet->setBorder('B' . $row . ':J' . $row, 'thin');
                            $sheet->setCellValueByColumnAndRow(1, $row, date("d-M-y", strtotime($a->SalesReturnDate)));
                            $sheet->setCellValueByColumnAndRow(2, $row, $a->SalesReturnID);
                            $sheet->setCellValueByColumnAndRow(3, $row, $a->CustomerName);
//                            $sheet->setCellValueByColumnAndRow(4, $row, $a->TaxNumber);
//                            $sheet->setCellValueByColumnAndRow(5, $row, $a->SalesManName);
                            $sheet->setCellValueByColumnAndRow(4, $row, number_format(ceil($sum), '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(5, $row, number_format($discountGlobal, '2', '.', ','));
//                            $sheet->setCellValueByColumnAndRow(8, $row, number_format($a->dpSO, '2', '.', ','));
//                            $sheet->setCellValueByColumnAndRow(9, $row, number_format($a->DownPayment, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(6, $row, number_format($TotalDP, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(7, $row, number_format($vatdp, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(8, $row, number_format($grandTotalDP, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(9, $row, $py);
                        }
                        $row++;
                        $hitung++;
                    }
                }

                if ($hitung == 0) {
                    if ($type == "Detail")
                        $sheet->mergeCells('B' . $row . ':P' . $row);
                    else
                        $sheet->mergeCells('B' . $row . ':J' . $row);
                    $sheet->setCellValueByColumnAndRow(1, $row, "There is no report.");
                }
                $sheet->cells('B2:B5', function($cells) {
                    $cells->setAlignment('left');
                    $cells->setFontWeight('bold');
                });
                if ($type == "Detail") {
                    $sheet->setCellValueByColumnAndRow(1, $row, "Total");
                    $sheet->setCellValueByColumnAndRow(10, $row, number_format($total1, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(11, $row, number_format($total2, '2', '.', ','));
//                    $sheet->setCellValueByColumnAndRow(15, $row, number_format($total3, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(12, $row, number_format($total4, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(13, $row, number_format($total5, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(14, $row, number_format($total6, '2', '.', ','));
                    $sheet->cells('B5:P5', function($cells) {
                        $cells->setAlignment('middle');
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('H6:O' . $row, function($cells) {
                        $cells->setAlignment('right');
                    });
                    $sheet->cells('B' . $row, function($cells) {
                        $cells->setAlignment('middle');
                        $cells->setFontWeight('bold');
                    });
                } else {
                    $sheet->setCellValueByColumnAndRow(1, $row, "Total");
                    $sheet->setCellValueByColumnAndRow(4, $row, number_format($total1, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(5, $row, number_format($total2, '2', '.', ','));
//                    $sheet->setCellValueByColumnAndRow(9, $row, number_format($total3, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(6, $row, number_format($total4, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(7, $row, number_format($total5, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(8, $row, number_format($total6, '2', '.', ','));
                    $sheet->cells('B5:J5', function($cells) {
                        $cells->setAlignment('middle');
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('E6:I' . $row, function($cells) {
                        $cells->setAlignment('right');
                    });
                    $sheet->cells('B' . $row, function($cells) {
                        $cells->setAlignment('middle');
                        $cells->setFontWeight('bold');
                    });
                }
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
//                $sheet->cells('B3:G' . $row, function($cells) {
//                    $cells->setAlignment('left');
//                    $cells->setValignment('middle');
//                });
//                $sheet->cells('B3:B' . $row, function($cells) {
//                    $cells->setAlignment('left');
//                });
            });
        })->export('xls');
    }
    
    public function purchaseReturnDetailReport() {
        Excel::create('Purchase_Return_Report', function($excel) {
            $excel->sheet('Purchase_Return_Report', function($sheet) {
                $startT = explode('-', Input::get('sDate'));
                $endT = explode('-', Input::get('eDate'));
                $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
                $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
                $supplier = Input::get('supplier');
//                $salesman = Input::get('salesman');
                $type = Input::get('type');
                $tax = Input::get('tax');
                $payment = Input::get('payment');
                if ($payment == "all") {
                    $payname = "All";
                } else if ($payment == "complete") {
                    $payname = "Complete";
                } else if ($payment == "paid") {
                    $payname = "Paid";
                } else {
                    $payname = "No Paid";
                }
                $total1 = $total2 = $total3 = $total4 = $total5 = $total6 = 0;
                //supplier
                $c = '';
                //salesman
                $p = '';
                //cek PPN dan non PPN
                $ci = '';
                $v = '';
                $py = '';
                $sttus = '';
                if ($supplier != 'all') {
                    $cusname = Coa6::find($supplier)->ACC6Name;
                    $c = " AND m_coa6.InternalID = $supplier";
                } else {
                    $cusname = "All";
                }
//                if ($salesman != 'all') {
//                    $salname = SalesMan::find($salesman)->SalesManName;
//                    $p = " AND m_sales_man.InternalID = $salesman";
//                } else {
//                    $salname = "All";
//                }
//                if (Auth::user()->SeeNPPN == 1) {
//                    //NON PPN 
//                    $ci = " AND t_purchasereturn_header.VAT = 0";
//                } else {
//                    //PPN 
//                    $ci = " AND t_purchasereturn_header.VAT = 1";
//                }
                if ($payment != 'all') {
                    $sttus = ' AND t_purchasereturn_header.`GrandTotal`*t_purchasereturn_header.`CurrencyRate` > (Select IFNULL(SUM(td.JournalCredit),0) From t_journal_detail td Where td.JournalTransactionID = t_purchasereturn_header.`PurchaseReturnID`)';
                }
                
                if ($tax != 'all') {
                    if ($tax == 0)
                        $salname = "Non PPN";
                    else
                        $salname = "PPN";
                    $p = " AND t_purchasereturn_header.VAT = $tax";
                } else {
                    $salname = "All";
                }

                $totalPO = 0;
                if ($type == "Detail")
                    $sheet->mergeCells('B1:P1');
                else
                    $sheet->mergeCells('B1:J1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Purchase Return Report By " . $type);
                $sheet->mergeCells('C2:D2');
                $sheet->setCellValueByColumnAndRow(1, 2, "Period :");
                $sheet->setCellValueByColumnAndRow(2, 2, $start . ' to ' . $end);
                $sheet->setCellValueByColumnAndRow(1, 3, "Supplier :");
                $sheet->setCellValueByColumnAndRow(2, 3, $cusname);
                $sheet->setCellValueByColumnAndRow(1, 4, "Tax :");
                $sheet->setCellValueByColumnAndRow(2, 4, $salname);
                $sheet->setCellValueByColumnAndRow(1, 5, "AR Payment :");
                $sheet->setCellValueByColumnAndRow(2, 5, $payname);
                $hitung = 0;
                $row = 6;
                if ($type == "Detail") {
                    $sheet->cells('B' . $row . ':P' . $row, function($cells) {
                        $cells->setBackground('#eaf6f7');
                        $cells->setValignment('middle');
                        $cells->setAlignment('center');
                    });
                    $sheet->setBorder('B' . $row . ':P' . $row, 'thin');
                    $sheet->setCellValueByColumnAndRow(1, $row, "Date");
                    $sheet->setCellValueByColumnAndRow(2, $row, "Retur No");
                    $sheet->setCellValueByColumnAndRow(3, $row, "Supplier");
//                    $sheet->setCellValueByColumnAndRow(4, $row, "Tax Number");
//                    $sheet->setCellValueByColumnAndRow(5, $row, "Purchase Man");
                    $sheet->setCellValueByColumnAndRow(4, $row, "Product");
                    $sheet->setCellValueByColumnAndRow(5, $row, "Quantity");
                    $sheet->setCellValueByColumnAndRow(6, $row, "UOM");
                    $sheet->setCellValueByColumnAndRow(7, $row, "Nominal");
                    $sheet->setCellValueByColumnAndRow(8, $row, "Discount");
                    $sheet->setCellValueByColumnAndRow(9, $row, "Discount Nominal");
                    $sheet->setCellValueByColumnAndRow(10, $row, "Total");
                    $sheet->setCellValueByColumnAndRow(11, $row, "Discount Global");
//                    $sheet->setCellValueByColumnAndRow(12, $row, "DP (%)");
//                    $sheet->setCellValueByColumnAndRow(13, $row, "DP");
                    $sheet->setCellValueByColumnAndRow(12, $row, "GrandTotal");
                    $sheet->setCellValueByColumnAndRow(13, $row, "VAT");
                    $sheet->setCellValueByColumnAndRow(14, $row, "GrandTotal(VAT)");
                    $sheet->setCellValueByColumnAndRow(15, $row, "AR Payment");
                } else {
                    $sheet->cells('B' . $row . ':J' . $row, function($cells) {
                        $cells->setBackground('#eaf6f7');
                        $cells->setValignment('middle');
                        $cells->setAlignment('center');
                    });
                    $sheet->setBorder('B' . $row . ':J' . $row, 'thin');
                    $sheet->setCellValueByColumnAndRow(1, $row, "Date");
                    $sheet->setCellValueByColumnAndRow(2, $row, "Invoice No");
                    $sheet->setCellValueByColumnAndRow(3, $row, "Supplier");
//                    $sheet->setCellValueByColumnAndRow(4, $row, "Tax Number");
//                    $sheet->setCellValueByColumnAndRow(5, $row, "Purchase Man");
                    $sheet->setCellValueByColumnAndRow(4, $row, "Total");
                    $sheet->setCellValueByColumnAndRow(5, $row, "Discount Global");
//                    $sheet->setCellValueByColumnAndRow(6, $row, "DP (%)");
//                    $sheet->setCellValueByColumnAndRow(7, $row, "DP");
                    $sheet->setCellValueByColumnAndRow(6, $row, "GrandTotal");
                    $sheet->setCellValueByColumnAndRow(7, $row, "VAT");
                    $sheet->setCellValueByColumnAndRow(8, $row, "GrandTotal(VAT)");
                    $sheet->setCellValueByColumnAndRow(9, $row, "AR Payment");
                }
                $row++;
                if ($type == "Detail") {
                    // DETAIL 

                    if ($payment == 'complete') {
                        $model = DB::select("SELECT t_purchasereturn_header.*,`t_purchasereturn_detail`.`Qty` as `Qty`, m_inventory.InternalID as InventoryID,m_inventory.`InventoryName` as `InventoryName`, 
                    `m_uom`.`UomID` as `UomID`, `t_purchasereturn_detail`.`SubTotal` as `SubTotal`, 
                    `m_coa6`.`ACC6Name` as `CustomerName`,
                    t_purchasereturn_detail.Price as price, t_purchasereturn_detail.Vat as Vat, 
                    t_purchasereturn_detail.Discount, t_purchasereturn_detail.DiscountNominal  FROM
                                t_purchasereturn_header  
                                inner join t_purchasereturn_detail on t_purchasereturn_detail.PurchaseReturnInternalID = t_purchasereturn_header.InternalID 
                                inner join `m_coa6` on `m_coa6`.`InternalID` = `t_purchasereturn_header`.`ACC6InternalID` 
                                inner join `m_inventory` on `m_inventory`.`InternalID` = `t_purchasereturn_detail`.`InventoryInternalID` 
                                inner join `m_uom` on `m_uom`.`InternalID` = `t_purchasereturn_detail`.`UomInternalID` 
                                WHERE t_purchasereturn_detail.InventoryInternalID = m_inventory.InternalID and 
										  (t_purchasereturn_header.GrandTotal <= (SELECT IFNULL(SUM(jd.JournalCredit),0) 
                                FROM t_journal_detail jd WHERE jd.JournalTransactionID = t_purchasereturn_header.PurchaseReturnID)) AND `t_purchasereturn_header`.`PurchaseReturnDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_purchasereturn_header.PurchaseReturnID ASC");
                    } else if ($payment == 'no_paid') {
                        $model = DB::select("SELECT t_purchasereturn_header.*,`t_purchasereturn_detail`.`Qty` as `Qty`, m_inventory.InternalID as InventoryID,m_inventory.`InventoryName` as `InventoryName`, 
                    `m_uom`.`UomID` as `UomID`, `t_purchasereturn_detail`.`SubTotal` as `SubTotal`, 
                    `m_coa6`.`ACC6Name` as `CustomerName`,
                    t_purchasereturn_detail.Price as price, t_purchasereturn_detail.Vat as Vat, 
                    t_purchasereturn_detail.Discount, t_purchasereturn_detail.DiscountNominal  FROM
                                t_purchasereturn_header  
                               inner join t_purchasereturn_detail on t_purchasereturn_detail.PurchaseReturnInternalID = t_purchasereturn_header.InternalID 
                                inner join `m_coa6` on `m_coa6`.`InternalID` = `t_purchasereturn_header`.`ACC6InternalID` 
                                inner join `m_inventory` on `m_inventory`.`InternalID` = `t_purchasereturn_detail`.`InventoryInternalID` 
                                inner join `m_uom` on `m_uom`.`InternalID` = `t_purchasereturn_detail`.`UomInternalID` 
                                WHERE t_purchasereturn_detail.InventoryInternalID = m_inventory.InternalID and   
										  (t_purchasereturn_header.GrandTotal > (SELECT IFNULL(SUM(jd.JournalCredit),0) 
                                FROM t_journal_detail jd WHERE jd.JournalTransactionID = t_purchasereturn_header.PurchaseReturnID)) AND t_purchasereturn_header.isCash in(1,4) AND `t_purchasereturn_header`.`PurchaseReturnDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_purchasereturn_header.PurchaseReturnID ASC");
                    } else if ($payment == 'all') {
                        $model = DB::select("SELECT t_purchasereturn_header.*,`t_purchasereturn_detail`.`Qty` as `Qty`, m_inventory.InternalID as InventoryID,m_inventory.`InventoryName` as `InventoryName`, 
                    `m_uom`.`UomID` as `UomID`, `t_purchasereturn_detail`.`SubTotal` as `SubTotal`, 
                    `m_coa6`.`ACC6Name` as `CustomerName`,
                    t_purchasereturn_detail.Price as price, t_purchasereturn_detail.Vat as Vat, 
                    t_purchasereturn_detail.Discount, t_purchasereturn_detail.DiscountNominal  FROM
                                t_purchasereturn_header  
                               inner join t_purchasereturn_detail on t_purchasereturn_detail.PurchaseReturnInternalID = t_purchasereturn_header.InternalID 
                                inner join `m_coa6` on `m_coa6`.`InternalID` = `t_purchasereturn_header`.`ACC6InternalID` 
                                inner join `m_inventory` on `m_inventory`.`InternalID` = `t_purchasereturn_detail`.`InventoryInternalID` 
                                inner join `m_uom` on `m_uom`.`InternalID` = `t_purchasereturn_detail`.`UomInternalID` 
                                WHERE t_purchasereturn_detail.InventoryInternalID = m_inventory.InternalID and   
										     `t_purchasereturn_header`.`PurchaseReturnDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_purchasereturn_header.PurchaseReturnID ASC");
                    } else if ($payment == 'paid') {
                        $model = DB::select("SELECT t_purchasereturn_header.*,`t_purchasereturn_detail`.`Qty` as `Qty`, m_inventory.InternalID as InventoryID,m_inventory.`InventoryName` as `InventoryName`, 
                    `m_uom`.`UomID` as `UomID`, `t_purchasereturn_detail`.`SubTotal` as `SubTotal`, 
                    `m_coa6`.`ACC6Name` as `CustomerName`,
                    t_purchasereturn_detail.Price as price, t_purchasereturn_detail.Vat as Vat, 
                    t_purchasereturn_detail.Discount, t_purchasereturn_detail.DiscountNominal  FROM
                                t_purchasereturn_header  
                                inner join t_purchasereturn_detail on t_purchasereturn_detail.PurchaseReturnInternalID = t_purchasereturn_header.InternalID 
                                inner join `m_coa6` on `m_coa6`.`InternalID` = `t_purchasereturn_header`.`ACC6InternalID` 
                                inner join `m_inventory` on `m_inventory`.`InternalID` = `t_purchasereturn_detail`.`InventoryInternalID` 
                                inner join `m_uom` on `m_uom`.`InternalID` = `t_purchasereturn_detail`.`UomInternalID` 
                                WHERE t_purchasereturn_detail.InventoryInternalID = m_inventory.InternalID and   
										  ((SELECT IFNULL(SUM(jd.JournalCredit),0) 
                                FROM t_journal_detail jd WHERE jd.JournalTransactionID = t_purchasereturn_header.PurchaseReturnID) > 0) AND `t_purchasereturn_header`.`PurchaseReturnDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_purchasereturn_header.PurchaseReturnID ASC");
                    }
                } else {
                    // SUMMARY

                    if ($payment == 'complete') {
                        $model = DB::select("SELECT t_purchasereturn_header.*, `m_coa6`.`ACC6Name` as `CustomerName`  
                   FROM t_purchasereturn_header  inner join `m_coa6` on `m_coa6`.`InternalID` = `t_purchasereturn_header`.`ACC6InternalID` 
                                 WHERE (t_purchasereturn_header.GrandTotal <= (SELECT IFNULL(SUM(jd.JournalCredit),0) 
                                FROM t_journal_detail jd WHERE jd.JournalTransactionID = t_purchasereturn_header.PurchaseReturnID)) AND `t_purchasereturn_header`.`PurchaseReturnDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_purchasereturn_header.PurchaseReturnID ASC");
                    } else if ($payment == 'no_paid') {
                        $model = DB::select("SELECT t_purchasereturn_header.*, `m_coa6`.`ACC6Name` as `CustomerName`  FROM
                                 t_purchasereturn_header inner join `m_coa6` on `m_coa6`.`InternalID` = `t_purchasereturn_header`.`ACC6InternalID`                                                                
                                WHERE (t_purchasereturn_header.GrandTotal > (SELECT IFNULL(SUM(jd.JournalCredit),0) 
                                FROM t_journal_detail jd WHERE jd.JournalTransactionID = t_purchasereturn_header.PurchaseReturnID)) AND t_purchasereturn_header.isCash in(1,4) AND `t_purchasereturn_header`.`PurchaseReturnDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_purchasereturn_header.PurchaseReturnID ASC");
                    } else if ($payment == 'all') {
                        $model = DB::select("SELECT t_purchasereturn_header.*, `m_coa6`.`ACC6Name` as `CustomerName`
                   FROM t_purchasereturn_header inner join `m_coa6` on `m_coa6`.`InternalID` = `t_purchasereturn_header`.`ACC6InternalID`                               
                               WHERE `t_purchasereturn_header`.`PurchaseReturnDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_purchasereturn_header.PurchaseReturnID ASC");
                    } else if ($payment == 'paid') {
                        $model = DB::select("SELECT t_purchasereturn_header.*,`m_coa6`.`ACC6Name` as `CustomerName` 
                   FROM t_purchasereturn_header  inner join `m_coa6` on `m_coa6`.`InternalID` = `t_purchasereturn_header`.`ACC6InternalID` 
                                WHERE ((SELECT IFNULL(SUM(jd.JournalCredit),0) 
                                FROM t_journal_detail jd WHERE jd.JournalTransactionID = t_purchasereturn_header.PurchaseReturnID) > 0) AND `t_purchasereturn_header`.`PurchaseReturnDate` between '$start' and '$end' $c $ci $v $p ORDER BY t_purchasereturn_header.PurchaseReturnID ASC");
                    }
                }

                $xx = $yy = 0;
                if (count($model) > 0) {
                    $checklast = 0;
                    $totalVATDP = 0;
                    $noinvoice = '';
                    $rowawal = $row;
                    foreach ($model as $a) {
                        if ($noinvoice != $a->PurchaseReturnID) {
                            $tulis = "ya";
                            $rowawal = $row;
                        } else {
                            $tulis = "tidak";
                        }
                        $noinvoice = $a->PurchaseReturnID;
                        $TotalDP = 0;
                        $py = PurchaseReturnHeader::salesReceivableReport($a->PurchaseReturnID, $payment);
                        $sum = PurchaseReturnDetail::where('PurchaseReturnInternalID', $a->InternalID)->sum('SubTotal');
                        $detail = PurchaseReturnDetail::where('PurchaseReturnInternalID', $a->InternalID)->get();
                        if ($type == "Detail") {
                            $total = $a->SubTotal;
                        } else {
                            if ($a->VAT == 1) {
                                $total = $a->GrandTotal / 1.1;
                            } else {
                                $total = $a->GrandTotal;
                            }
                        }
                        if ($sum != 0)
                            $discountGlobal = $a->DiscountGlobal;
//                            $discountGlobal = ($a->DiscountGlobal * $total) / $sum;
                        else
                            $discountGlobal = 0;
                        $grand = ceil($sum) - $discountGlobal;
                        $vatHeader = $grand ;
                        $vatgrand = floor($vatHeader * 0.1);
                        $grandHeader = ceil($sum) - $a->DiscountGlobal;
                        $ppnHeader = floor($grandHeader * 0.1);
                        if ($grandHeader != 0)
                            $ppn = round(($total * $ppnHeader) / $grandHeader, 0);
                        else
                            $ppn = 0;
                       
                        $TotalDP = ceil($grand);
//                        if ($TotalDP % 10 != 0)
//                            $TotalDP++;
                        $vatdp = floor($TotalDP * 0.1);
                        $totalVATDP += $vatdp;

                        $checklast++;
                        if ($checklast == count($detail)) {
                            $checklast = 0;

                            if ($vatgrand != $totalVATDP) {
                                $selisih = $vatgrand - $totalVATDP;
                                $vatdp = $vatdp + $selisih;
                                $totalVATDP = 0;
                            } else {
                                $totalVATDP = 0;
                            }
                        }
                        $grandTotalDP = round($TotalDP + $vatdp);

                        if ($type == "Detail") {
                            $sheet->setBorder('B' . $row . ':P' . $row, 'thin');
                            if ($tulis == "ya") {
                                if ($xx != 0)
                                    $row++;
                                else
                                    $xx++;
                                $sheet->setCellValueByColumnAndRow(1, $row, date("d-M-y", strtotime($a->PurchaseReturnDate)));
                                $sheet->setCellValueByColumnAndRow(2, $row, $a->PurchaseReturnID);
                                $sheet->setCellValueByColumnAndRow(3, $row, $a->CustomerName);
//                                $sheet->setCellValueByColumnAndRow(4, $row, $a->TaxNumber);
//                                $sheet->setCellValueByColumnAndRow(5, $row, $a->PurchaseManName);
                                $sheet->setBorder('B' . $row . ':P' . $row, 'thin');
                                $row++;
                                $totgrand = 0;
                            }
                            $sheet->setCellValueByColumnAndRow(4, $row, $a->InventoryName);
                            $sheet->setCellValueByColumnAndRow(5, $row, number_format($a->Qty, '3', '.', ','));
                            $sheet->setCellValueByColumnAndRow(6, $row, $a->UomID);
                            $sheet->setCellValueByColumnAndRow(7, $row, number_format($a->price, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(8, $row, number_format($a->Discount, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(9, $row, number_format($a->DiscountNominal, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(10, $row, number_format(ceil($a->SubTotal), '2', '.', ','));
                            $totgrand += ceil($a->SubTotal);
                            if ($tulis == "ya") {
                                if ($yy != 0)
                                    $rowawal++;
                                else
                                    $yy++;
                                $sheet->setCellValueByColumnAndRow(10, $rowawal, number_format($totgrand, '2', '.', ','));
                                $sheet->setCellValueByColumnAndRow(11, $rowawal, number_format($discountGlobal, '2', '.', ','));
//                                $sheet->setCellValueByColumnAndRow(14, $rowawal, number_format($a->dpSO, '2', '.', ','));
//                                $sheet->setCellValueByColumnAndRow(15, $rowawal, number_format($a->DownPayment, '2', '.', ','));
                                $sheet->setCellValueByColumnAndRow(12, $rowawal, number_format($TotalDP, '2', '.', ','));
                                $sheet->setCellValueByColumnAndRow(13, $rowawal, number_format($vatdp, '2', '.', ','));
                                $sheet->setCellValueByColumnAndRow(14, $rowawal, number_format($grandTotalDP, '2', '.', ','));
                                $sheet->setCellValueByColumnAndRow(15, $rowawal, $py);
                                $sheet->setBorder('B' . $row . ':P' . $row, 'thin');
                                $total1 += $totgrand;
                                $total2 += $discountGlobal;
                                $total3 += 0;
                                $total4 += $TotalDP;
                                $total5 += $vatdp;
                                $total6 += $grandTotalDP;
                            }
                        } else {
                            $total1 += ceil($sum);
                            $total2 += $discountGlobal;
                            $total3 += 0;
                            $total4 += $TotalDP;
                            $total5 += $vatdp;
                            $total6 += $grandTotalDP;
                            $sheet->setBorder('B' . $row . ':J' . $row, 'thin');
                            $sheet->setCellValueByColumnAndRow(1, $row, date("d-M-y", strtotime($a->PurchaseReturnDate)));
                            $sheet->setCellValueByColumnAndRow(2, $row, $a->PurchaseReturnID);
                            $sheet->setCellValueByColumnAndRow(3, $row, $a->CustomerName);
//                            $sheet->setCellValueByColumnAndRow(4, $row, $a->TaxNumber);
//                            $sheet->setCellValueByColumnAndRow(5, $row, $a->PurchaseManName);
                            $sheet->setCellValueByColumnAndRow(4, $row, number_format(ceil($sum), '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(5, $row, number_format($discountGlobal, '2', '.', ','));
//                            $sheet->setCellValueByColumnAndRow(8, $row, number_format($a->dpSO, '2', '.', ','));
//                            $sheet->setCellValueByColumnAndRow(9, $row, number_format($a->DownPayment, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(6, $row, number_format($TotalDP, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(7, $row, number_format($vatdp, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(8, $row, number_format($grandTotalDP, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(9, $row, $py);
                        }
                        $row++;
                        $hitung++;
                    }
                }

                if ($hitung == 0) {
                    if ($type == "Detail")
                        $sheet->mergeCells('B' . $row . ':P' . $row);
                    else
                        $sheet->mergeCells('B' . $row . ':J' . $row);
                    $sheet->setCellValueByColumnAndRow(1, $row, "There is no report.");
                }
                $sheet->cells('B2:B5', function($cells) {
                    $cells->setAlignment('left');
                    $cells->setFontWeight('bold');
                });
                if ($type == "Detail") {
                    $sheet->setCellValueByColumnAndRow(1, $row, "Total");
                    $sheet->setCellValueByColumnAndRow(10, $row, number_format($total1, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(11, $row, number_format($total2, '2', '.', ','));
//                    $sheet->setCellValueByColumnAndRow(15, $row, number_format($total3, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(12, $row, number_format($total4, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(13, $row, number_format($total5, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(14, $row, number_format($total6, '2', '.', ','));
                    $sheet->cells('B5:P5', function($cells) {
                        $cells->setAlignment('middle');
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('H6:O' . $row, function($cells) {
                        $cells->setAlignment('right');
                    });
                    $sheet->cells('B' . $row, function($cells) {
                        $cells->setAlignment('middle');
                        $cells->setFontWeight('bold');
                    });
                } else {
                    $sheet->setCellValueByColumnAndRow(1, $row, "Total");
                    $sheet->setCellValueByColumnAndRow(4, $row, number_format($total1, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(5, $row, number_format($total2, '2', '.', ','));
//                    $sheet->setCellValueByColumnAndRow(9, $row, number_format($total3, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(6, $row, number_format($total4, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(7, $row, number_format($total5, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(8, $row, number_format($total6, '2', '.', ','));
                    $sheet->cells('B5:J5', function($cells) {
                        $cells->setAlignment('middle');
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('E6:I' . $row, function($cells) {
                        $cells->setAlignment('right');
                    });
                    $sheet->cells('B' . $row, function($cells) {
                        $cells->setAlignment('middle');
                        $cells->setFontWeight('bold');
                    });
                }
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
//                $sheet->cells('B3:G' . $row, function($cells) {
//                    $cells->setAlignment('left');
//                    $cells->setValignment('middle');
//                });
//                $sheet->cells('B3:B' . $row, function($cells) {
//                    $cells->setAlignment('left');
//                });
            });
        })->export('xls');
    }

    public function salesReturnDetailReport_lama() {
        Excel::create('Sales_Return_Report_by_Detail', function($excel) {
            $excel->sheet('SalesReturnReportByDetail', function($sheet) {
                $startT = explode('-', Input::get('sDate'));
                $endT = explode('-', Input::get('eDate'));
                $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
                $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
                $customer = Input::get('customer');
                $project = Input::get('project');
                $city = Input::get('city');
                $variety = Input::get('variety');
                $payment = Input::get('payment');
                $c = '';
                $p = '';
                $ci = '';
                $v = '';
                $py = '';
                $sttus = '';
                if ($customer != 'all') {
                    $c = " AND m_customer_type.InternalID = $customer";
                    //print_r($model);exit();
                }
                if ($city != 'all') {
                    $ci = " AND m_area.InternalID = $city ";
                }
                if ($variety != 'all') {
                    $v = " AND m_inventory.VarietyInternalID = $variety";
                }
                if ($project != 'all') {
                    $p = "t_salesorder_header.ProjectType = $project";
                }
                if ($payment != 'all') {
                    $sttus = ' AND t_salesreturn_header.`GrandTotal`*t_salesreturn_header.`CurrencyRate` > (Select IFNULL(SUM(td.JournalCredit),0) From t_journal_detail td Where td.JournalTransactionID = t_salesreturn_header.`SalesID`)';
                }

                $totalPO = 0;
                $sheet->mergeCells('B1:R1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Sales Report By Detail");
                $sheet->mergeCells('C2:D2');
                $sheet->setCellValueByColumnAndRow(1, 2, "Period :");
                $sheet->setCellValueByColumnAndRow(2, 2, $start . ' to ' . $end);
                $hitung = 0;
                $row = 3;
                $sheet->cells('B' . $row . ':R' . $row, function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('B' . $row . ':R' . $row, 'thin');
                $sheet->setCellValueByColumnAndRow(1, $row, "Date");
                $sheet->setCellValueByColumnAndRow(2, $row, "No Shipping");
                $sheet->setCellValueByColumnAndRow(3, $row, "No Sales");
                $sheet->setCellValueByColumnAndRow(4, $row, "Shipment");
                $sheet->setCellValueByColumnAndRow(5, $row, "Customer");
                $sheet->setCellValueByColumnAndRow(6, $row, "Customer Type");
                $sheet->setCellValueByColumnAndRow(7, $row, "Project Type");
                $sheet->setCellValueByColumnAndRow(8, $row, "Outlet");
                $sheet->setCellValueByColumnAndRow(9, $row, "Area Kirim");
                $sheet->setCellValueByColumnAndRow(10, $row, "TOP");
                $sheet->setCellValueByColumnAndRow(11, $row, "Product");
                $sheet->setCellValueByColumnAndRow(12, $row, "Quantity");
                $sheet->setCellValueByColumnAndRow(13, $row, "UOM");
                $sheet->setCellValueByColumnAndRow(14, $row, "Nominal");
                $sheet->setCellValueByColumnAndRow(15, $row, "PPN");
                $sheet->setCellValueByColumnAndRow(16, $row, "Total");
                $sheet->setCellValueByColumnAndRow(17, $row, "AR Payment");
                $row = 4;

                $model = DB::select("select `t_salesreturn_header`.`SalesReturnDate` as `tanggal`, `t_salesorder_header`.`ProjectType` as `project`, `m_outlet`.`OutletName` as `OutletName`, t_salesreturn_header.SalesReturnID as SalesOrderID,"
                                . " `m_area`.`AreaName` as `AreaName`, m_inventory.InternalID as InventoryID , `t_sales_header`.`LongTerm` as `LongTerm`, `t_salesorder_header`.`isCash` as `isCash`, `t_sales_detail`.`Qty` as `Qty`,"
                                . " `m_inventory`.`InventoryName` as `InventoryName`, `m_uom`.`UomID` as `UomID`, `t_sales_detail`.`SubTotal` as `SubTotal`, `m_coa6`.`ACC6Name` as `CustomerName`, t_shipping_header.ShippingID as ShippingID, t_shipping_header.Type as TypeShipping,"
                                . " `m_customer_type`.`CustomerTypeName` as `CustomerTypeName`,m_variety.VarietyName as VarietyName,m_variety.InternalID as VarietyInternalID from `t_salesorder_header` "
                                . "inner join `t_salesorder_detail` on `t_salesorder_detail`.`SalesOrderInternalID` = `t_salesorder_header`.`InternalID` "
                                . "inner join t_mod_header on t_mod_header.SalesOrderInternalID = t_salesorder_header.InternalID "
                                . "inner join t_shipping_header on t_shipping_header.ModInternalID = t_mod_header.InternalID "
                                . "inner join `m_coa6` on `m_coa6`.`InternalID` = `t_salesorder_header`.`ACC6InternalID` "
                                . "inner join `m_customer_type` on `m_customer_type`.`InternalID` = `m_coa6`.`CustomerTypeInternalID` "
                                . "inner join `m_area` on `t_salesorder_header`.`AreaInternalID` = `m_area`.`InternalID` "
                                . "inner join `m_inventory` on `m_inventory`.`InternalID` = `t_salesorder_detail`.`InventoryInternalID` "
                                . "inner join `m_outlet` on `m_outlet`.`InternalID` = `t_salesorder_header`.`OutletInternalID` "
                                . "inner join `m_uom` on `m_uom`.`InternalID` = `t_salesorder_detail`.`UomInternalID` inner join m_variety on m_variety.InternalID = m_inventory.VarietyInternalID "
                                . "inner join t_sales_header on t_sales_header.ShippingInternalID = t_shipping_header.InternalID "
                                . "inner join t_sales_detail on t_sales_detail.SalesInternalID = t_sales_header.InternalID "
                                . "Inner join t_salesreturn_header on t_salesreturn_header.SalesInternalID = t_sales_header.InternalID"
                                . " where `t_salesreturn_header`.`SalesReturnDate` between '$start' and '$end' $c $ci $v $p $sttus");
                $arrayNama = [];
                $arrayQty = [];
                $arrayNominal = [];
                $arrayVariety = [];
                $arrayUOM = [];
                $arrayPPN = [];
                $arrayTotal = [];
                $totalNominal = 0;
                $totalPPN = 0;
                $grandTotal = 0;
                if (count($model) > 0) {
                    foreach ($model as $a) {
                        $py = SalesReturnHeader::salesReceivableReport($a->SalesOrderID, $payment);
//                        dd($py);
                        if ($payment == 'all') {
                            $ppn = $a->SubTotal * 0.1;
                            $total = $a->SubTotal + $ppn;
                            $top = '';
                            if ($a->isCash == 1) {
                                $top = $a->LongTerm;
                            } else if ($a->isCash == 2) {
                                $top = 'CBD';
                            } else if ($a->isCash == 3) {
                                $top = 'Deposito';
                            }
                            $shipping = '';
                            if ($a->TypeShipping == 2) {
                                $shipping = 'Loco';
                            } else {
                                $shipping = 'Franco';
                            }
                            if (isset($arrayQty[$a->VarietyInternalID])) {
                                $arrayQty[$a->VarietyInternalID] += $a->Qty;
                                $arrayNominal[$a->VarietyInternalID] += $a->SubTotal;
                                $arrayPPN[$a->VarietyInternalID] += $a->SubTotal * 0.1;
                                $arrayTotal[$a->VarietyInternalID] += $a->SubTotal + ($a->SubTotal * 0.1);
                            } else {
                                $arrayQty[$a->VarietyInternalID] = $a->Qty;
                                $arrayNominal[$a->VarietyInternalID] = $a->SubTotal;
                                $arrayPPN[$a->VarietyInternalID] = $a->SubTotal * 0.1;
                                $arrayTotal[$a->VarietyInternalID] = ($a->SubTotal + ($a->SubTotal * 0.1));
                            }
                            $arrayNama[$a->VarietyInternalID] = $a->InventoryName;
                            $arrayVariety[$a->VarietyInternalID] = $a->VarietyName;
                            $arrayUOM[$a->VarietyInternalID] = $a->UomID;

                            $sheet->setBorder('B' . $row . ':R' . $row, 'thin');
                            $sheet->setCellValueByColumnAndRow(1, $row, date("d-M-y", strtotime($a->tanggal)));
                            $sheet->setCellValueByColumnAndRow(2, $row, $a->ShippingID);
                            $sheet->setCellValueByColumnAndRow(3, $row, $a->SalesOrderID);
                            $sheet->setCellValueByColumnAndRow(4, $row, $shipping);
                            $sheet->setCellValueByColumnAndRow(5, $row, $a->CustomerName);
                            $sheet->setCellValueByColumnAndRow(6, $row, $a->CustomerTypeName);
                            $sheet->setCellValueByColumnAndRow(7, $row, $a->project);
                            $sheet->setCellValueByColumnAndRow(8, $row, $a->OutletName);
                            $sheet->setCellValueByColumnAndRow(9, $row, $a->AreaName);
                            $sheet->setCellValueByColumnAndRow(10, $row, $top);
                            $sheet->setCellValueByColumnAndRow(11, $row, $a->InventoryName);
                            $sheet->setCellValueByColumnAndRow(12, $row, number_format($a->Qty, '3', '.', ','));
                            $sheet->setCellValueByColumnAndRow(13, $row, $a->UomID);
                            $sheet->setCellValueByColumnAndRow(14, $row, number_format($a->SubTotal, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(15, $row, number_format($ppn, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(16, $row, number_format($total, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(17, $row, $py);

                            $row++;
                            $hitung++;
                        } else if ($payment == "paid") {
                            if ($py == "Paid") {
                                $ppn = $a->SubTotal * 0.1;
                                $total = $a->SubTotal + $ppn;
                                $top = '';
                                if ($a->isCash == 1) {
                                    $top = $a->LongTerm;
                                } else if ($a->isCash == 2) {
                                    $top = 'CBD';
                                } else if ($a->isCash == 3) {
                                    $top = 'Deposito';
                                }
                                $shipping = '';
                                if ($a->TypeShipping == 2) {
                                    $shipping = 'Loco';
                                } else {
                                    $shipping = 'Franco';
                                }


                                if (isset($arrayQty[$a->VarietyInternalID])) {
                                    $arrayQty[$a->VarietyInternalID] += $a->Qty;
                                    $arrayNominal[$a->VarietyInternalID] += $a->SubTotal;
                                    $arrayPPN[$a->VarietyInternalID] += $a->SubTotal * 0.1;
                                    $arrayTotal[$a->VarietyInternalID] += $a->SubTotal + ($a->SubTotal * 0.1);
                                } else {
                                    $arrayQty[$a->VarietyInternalID] = $a->Qty;
                                    $arrayNominal[$a->VarietyInternalID] = $a->SubTotal;
                                    $arrayPPN[$a->VarietyInternalID] = $a->SubTotal * 0.1;
                                    $arrayTotal[$a->VarietyInternalID] = ($a->SubTotal + ($a->SubTotal * 0.1));
                                }
                                $arrayNama[$a->VarietyInternalID] = $a->InventoryName;
                                $arrayVariety[$a->VarietyInternalID] = $a->VarietyName;
                                $arrayUOM[$a->VarietyInternalID] = $a->UomID;



                                $sheet->setBorder('B' . $row . ':R' . $row, 'thin');
                                $sheet->setCellValueByColumnAndRow(1, $row, date("d-M-y", strtotime($a->tanggal)));
                                $sheet->setCellValueByColumnAndRow(2, $row, $a->ShippingID);
                                $sheet->setCellValueByColumnAndRow(3, $row, $a->SalesOrderID);
                                $sheet->setCellValueByColumnAndRow(4, $row, $shipping);
                                $sheet->setCellValueByColumnAndRow(5, $row, $a->CustomerName);
                                $sheet->setCellValueByColumnAndRow(6, $row, $a->CustomerTypeName);
                                $sheet->setCellValueByColumnAndRow(7, $row, $a->project);
                                $sheet->setCellValueByColumnAndRow(8, $row, $a->OutletName);
                                $sheet->setCellValueByColumnAndRow(9, $row, $a->AreaName);
                                $sheet->setCellValueByColumnAndRow(10, $row, $top);
                                $sheet->setCellValueByColumnAndRow(11, $row, $a->InventoryName);
                                $sheet->setCellValueByColumnAndRow(12, $row, $a->Qty);
                                $sheet->setCellValueByColumnAndRow(13, $row, $a->UomID);
                                $sheet->setCellValueByColumnAndRow(14, $row, $a->SubTotal);
                                $sheet->setCellValueByColumnAndRow(15, $row, $ppn);
                                $sheet->setCellValueByColumnAndRow(16, $row, $total);

                                $sheet->setCellValueByColumnAndRow(17, $row, $py);

                                $row++;
                                $hitung++;
                            }
                        } else if ($payment == "no_paid") {
                            if ($py == "No paid") {
                                $ppn = $a->SubTotal * 0.1;
                                $total = $a->SubTotal + $ppn;
                                $top = '';
                                if ($a->isCash == 1) {
                                    $top = $a->LongTerm;
                                } else if ($a->isCash == 2) {
                                    $top = 'CBD';
                                } else if ($a->isCash == 3) {
                                    $top = 'Deposito';
                                }
                                $shipping = '';
                                if ($a->TypeShipping == 2) {
                                    $shipping = 'Loco';
                                } else {
                                    $shipping = 'Franco';
                                }


                                if (isset($arrayQty[$a->VarietyInternalID])) {
                                    $arrayQty[$a->VarietyInternalID] += $a->Qty;
                                    $arrayNominal[$a->VarietyInternalID] += $a->SubTotal;
                                    $arrayPPN[$a->VarietyInternalID] += $a->SubTotal * 0.1;
                                    $arrayTotal[$a->VarietyInternalID] += $a->SubTotal + ($a->SubTotal * 0.1);
                                } else {
                                    $arrayQty[$a->VarietyInternalID] = $a->Qty;
                                    $arrayNominal[$a->VarietyInternalID] = $a->SubTotal;
                                    $arrayPPN[$a->VarietyInternalID] = $a->SubTotal * 0.1;
                                    $arrayTotal[$a->VarietyInternalID] = ($a->SubTotal + ($a->SubTotal * 0.1));
                                }
                                $arrayNama[$a->VarietyInternalID] = $a->InventoryName;
                                $arrayVariety[$a->VarietyInternalID] = $a->VarietyName;
                                $arrayUOM[$a->VarietyInternalID] = $a->UomID;



                                $sheet->setBorder('B' . $row . ':R' . $row, 'thin');
                                $sheet->setCellValueByColumnAndRow(1, $row, date("d-M-y", strtotime($a->tanggal)));
                                $sheet->setCellValueByColumnAndRow(2, $row, $a->ShippingID);
                                $sheet->setCellValueByColumnAndRow(3, $row, $a->SalesOrderID);
                                $sheet->setCellValueByColumnAndRow(4, $row, $shipping);
                                $sheet->setCellValueByColumnAndRow(5, $row, $a->CustomerName);
                                $sheet->setCellValueByColumnAndRow(6, $row, $a->CustomerTypeName);
                                $sheet->setCellValueByColumnAndRow(7, $row, $a->project);
                                $sheet->setCellValueByColumnAndRow(8, $row, $a->OutletName);
                                $sheet->setCellValueByColumnAndRow(9, $row, $a->AreaName);
                                $sheet->setCellValueByColumnAndRow(10, $row, $top);
                                $sheet->setCellValueByColumnAndRow(11, $row, $a->InventoryName);
                                $sheet->setCellValueByColumnAndRow(12, $row, $a->Qty);
                                $sheet->setCellValueByColumnAndRow(13, $row, $a->UomID);
                                $sheet->setCellValueByColumnAndRow(14, $row, $a->SubTotal);
                                $sheet->setCellValueByColumnAndRow(15, $row, $ppn);
                                $sheet->setCellValueByColumnAndRow(16, $row, $total);

                                $sheet->setCellValueByColumnAndRow(17, $row, $py);

                                $row++;
                                $hitung++;
                            }
                        } else if ($payment == "complete") {
                            if ($py == "Complete") {
                                $ppn = $a->SubTotal * 0.1;
                                $total = $a->SubTotal + $ppn;
                                $top = '';
                                if ($a->isCash == 1) {
                                    $top = $a->LongTerm;
                                } else if ($a->isCash == 2) {
                                    $top = 'CBD';
                                } else if ($a->isCash == 3) {
                                    $top = 'Deposito';
                                }
                                $shipping = '';
                                if ($a->TypeShipping == 2) {
                                    $shipping = 'Loco';
                                } else {
                                    $shipping = 'Franco';
                                }


                                if (isset($arrayQty[$a->VarietyInternalID])) {
                                    $arrayQty[$a->VarietyInternalID] += $a->Qty;
                                    $arrayNominal[$a->VarietyInternalID] += $a->SubTotal;
                                    $arrayPPN[$a->VarietyInternalID] += $a->SubTotal * 0.1;
                                    $arrayTotal[$a->VarietyInternalID] += $a->SubTotal + ($a->SubTotal * 0.1);
                                } else {
                                    $arrayQty[$a->VarietyInternalID] = $a->Qty;
                                    $arrayNominal[$a->VarietyInternalID] = $a->SubTotal;
                                    $arrayPPN[$a->VarietyInternalID] = $a->SubTotal * 0.1;
                                    $arrayTotal[$a->VarietyInternalID] = ($a->SubTotal + ($a->SubTotal * 0.1));
                                }
                                $arrayNama[$a->VarietyInternalID] = $a->InventoryName;
                                $arrayVariety[$a->VarietyInternalID] = $a->VarietyName;
                                $arrayUOM[$a->VarietyInternalID] = $a->UomID;



                                $sheet->setBorder('B' . $row . ':R' . $row, 'thin');
                                $sheet->setCellValueByColumnAndRow(1, $row, date("d-M-y", strtotime($a->tanggal)));
                                $sheet->setCellValueByColumnAndRow(2, $row, $a->ShippingID);
                                $sheet->setCellValueByColumnAndRow(3, $row, $a->SalesOrderID);
                                $sheet->setCellValueByColumnAndRow(4, $row, $shipping);
                                $sheet->setCellValueByColumnAndRow(5, $row, $a->CustomerName);
                                $sheet->setCellValueByColumnAndRow(6, $row, $a->CustomerTypeName);
                                $sheet->setCellValueByColumnAndRow(7, $row, $a->project);
                                $sheet->setCellValueByColumnAndRow(8, $row, $a->OutletName);
                                $sheet->setCellValueByColumnAndRow(9, $row, $a->AreaName);
                                $sheet->setCellValueByColumnAndRow(10, $row, $top);
                                $sheet->setCellValueByColumnAndRow(11, $row, $a->InventoryName);
                                $sheet->setCellValueByColumnAndRow(12, $row, $a->Qty);
                                $sheet->setCellValueByColumnAndRow(13, $row, $a->UomID);
                                $sheet->setCellValueByColumnAndRow(14, $row, $a->SubTotal);
                                $sheet->setCellValueByColumnAndRow(15, $row, $ppn);
                                $sheet->setCellValueByColumnAndRow(16, $row, $total);

                                $sheet->setCellValueByColumnAndRow(17, $row, $py);

                                $row++;
                                $hitung++;
                            }
                        }
                    }

                    foreach ($arrayQty as $key => $qty) {
                        $totalPPN += $arrayPPN[$key];
                        $totalNominal += $arrayNominal[$key];
                        $grandTotal += $arrayTotal[$key];
                        $sheet->setBorder('B' . $row . ':Q' . $row, 'double');
                        $sheet->mergeCells('B' . $row . ':L' . $row);
                        $sheet->cells('B' . $row . ':R' . $row, function($cells) {
                            ;
                            $cells->setAlignment('right');
                            $cells->setFontWeight('bold');
                        });
                        $sheet->setCellValueByColumnAndRow(1, $row, 'Total ' . $arrayVariety[$key]);
                        $sheet->setCellValueByColumnAndRow(12, $row, ($arrayQty[$key] < 0 ? "(" . number_format((-1 * $arrayQty[$key]), 2, ".", ",") . ")" : number_format($arrayQty[$key], 2, ".", ",")));
                        $sheet->setCellValueByColumnAndRow(13, $row, $arrayUOM[$key]);
                        $sheet->setCellValueByColumnAndRow(14, $row, ($arrayNominal[$key] < 0 ? "(" . number_format((-1 * $arrayNominal[$key]), 2, ".", ",") . ")" : number_format($arrayNominal[$key], 2, ".", ",")));
                        $sheet->setCellValueByColumnAndRow(15, $row, ($arrayPPN[$key] < 0 ? "(" . number_format((-1 * $arrayPPN[$key]), 2, ".", ",") . ")" : number_format($arrayPPN[$key], 2, ".", ",")));
                        $sheet->setCellValueByColumnAndRow(16, $row, ($arrayTotal[$key] < 0 ? "(" . number_format((-1 * $arrayTotal[$key]), 2, ".", ",") . ")" : number_format($arrayTotal[$key], 2, ".", ",")));
                        $sheet->setCellValueByColumnAndRow(17, $row, '');
                        //$sheet->mergeCells('B' . $row . ':R11');

                        $row++;
                    }




                    $sheet->setBorder('B' . $row . ':Q' . $row, 'double');
                    $sheet->mergeCells('B' . $row . ':N' . $row);
                    $sheet->setCellValueByColumnAndRow(1, $row, 'Total All Product ');
                    $sheet->setCellValueByColumnAndRow(14, $row, ($totalNominal < 0 ? "(" . number_format((-1 * $totalNominal), 2, ".", ",") . ")" : number_format($totalNominal, 2, ".", ",")));
                    $sheet->setCellValueByColumnAndRow(15, $row, ($totalPPN < 0 ? "(" . number_format((-1 * $totalPPN), 2, ".", ",") . ")" : number_format($totalPPN, 2, ".", ",")));
                    $sheet->setCellValueByColumnAndRow(16, $row, ($grandTotal < 0 ? "(" . number_format((-1 * $grandTotal), 2, ".", ",") . ")" : number_format($grandTotal, 2, ".", ",")));
                    $sheet->cells('B' . $row . ':R' . $row, function($cells) {
                        ;
                        $cells->setAlignment('right');
                        $cells->setFontWeight('bold');
                    });
                }

                if ($hitung == 0) {
                    $sheet->mergeCells('B' . $row . ':R' . $row);
                    $sheet->setCellValueByColumnAndRow(1, $row, "There is no report.");
                    $sheet->cells('B4', function($cells) {
                        $cells->setAlignment('center');
                        $cells->setFontWeight('bold');
                    });
                }
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:G' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('left');
                });
            });
        })->export('xls');
    }

    public function reportSubsidiary() {
        Excel::create('Report_Ledger_Subsidiary', function($excel) {
            $excel->sheet('LedgerSubsidiary', function($sheet) {
                $startT = explode('-', Input::get('sDate'));
                $endT = explode('-', Input::get('eDate'));
                $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
                $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
                $account = Input::get('account');
//                dd($account);
                $a = '';
                if ($account[0] != "all") {
                    $account = implode(",", $account);
                    $a = " AND m_coa.InternalID IN (" . $account . ")";
                }
                $totalDebet = 0;
                $totalCredit = 0;
                $totalInitial = 0;
                $totalEnd = 0;
                $sheet->mergeCells('B1:I1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Report Subsidiary Ledger");
                $sheet->mergeCells('C2:D2');
                $sheet->setCellValueByColumnAndRow(1, 2, "Period :" . $start . ' to ' . $end);

                $hitung = 0;
                $row = 4;
                $nilaiDebet = 0;
                $nilaiKredit = 0;
                $nilaiinitial = 0;
                $nilaiEnd = 0;
                $nilaiBalance = 0;
                $nilaiinitialDebet = 0;
                $nilaiinitialKredit = 0;
                if (Coa::where("CompanyInternalID", Auth::user()->Company->InternalID)->count() > 0) {
                    foreach (Coa::selectRaw("m_coa.* "
                                    . " , if (m_coa4.ACC4ID = 0 or m_coa4.ACC4ID is null, "
                                    . "         if (m_coa3.ACC3ID = 0 , "
                                    . "                if ( m_coa2.ACC2ID = 0 "
                                    . "                     , m_coa1.ACC1ID "
                                    . "                 , m_coa2.ACC2ID ) "
                                    . "             , m_coa3.ACC3ID ) "
                                    . "         , m_coa4.ACC4ID ) orderbys")
                            ->leftjoin("m_coa1", "m_coa.ACC1InternalID", '=', "m_coa1.InternalID")
                            ->leftjoin("m_coa2", "m_coa.ACC2InternalID", '=', "m_coa2.InternalID")
                            ->leftjoin("m_coa3", "m_coa.ACC3InternalID", '=', "m_coa3.InternalID")
                            ->leftjoin("m_coa4", "m_coa.ACC4InternalID", '=', "m_coa4.InternalID")
                            ->leftjoin("m_coa5", "m_coa.ACC5InternalID", '=', "m_coa5.InternalID")
                            ->leftjoin("m_coa6", "m_coa.ACC6InternalID", '=', "m_coa6.InternalID")
                            ->whereRaw("m_coa.CompanyInternalID =" . Auth::user()->Company->InternalID . " $a")
                            ->orderByRaw(DB::raw('REPLACE(orderbys , "." ,"") asc'))
                            ->get() as $coa) {
                        // dd($coa);
                        $idAccount = Coa::formatCoa($coa->ACC1InternalID, $coa->ACC2InternalID, $coa->ACC3InternalID, $coa->ACC4InternalID, $coa->ACC5InternalID, $coa->ACC6InternalID, 1);
                        $nilaiDebet = 0;
                        $nilaiKredit = 0;
                        $nilaiinitial = 0;
                        $nilaiEnd = 0;
                        $nilaiBalance = 0;
                        $nilaiinitialDebet = 0;
                        $nilaiinitialKredit = 0;
                        $sheet->setCellValueByColumnAndRow(1, $row, "Account ID : " . $idAccount . ' ' . $coa->COAName);
                        $row++;
                        $sheet->cells('B' . $row . ':J' . $row, function($cells) {
                            $cells->setBackground('#eaf6f7');
                            $cells->setValignment('middle');
                            $cells->setAlignment('center');
                        });
                        $sheet->setBorder('B' . $row . ':J' . $row, 'thin');
                        $sheet->setCellValueByColumnAndRow(1, $row, "Date");
                        $sheet->setCellValueByColumnAndRow(2, $row, "Journal ID");
                        $sheet->setCellValueByColumnAndRow(3, $row, "Description");
                        $sheet->setCellValueByColumnAndRow(4, $row, "Notes");
                        $sheet->setCellValueByColumnAndRow(5, $row, "Transaction ID");
                        $sheet->setCellValueByColumnAndRow(6, $row, "Journal Transaction ID");
                        $sheet->setCellValueByColumnAndRow(7, $row, "Debet");
                        $sheet->setCellValueByColumnAndRow(8, $row, "Credit");
                        $sheet->setCellValueByColumnAndRow(9, $row, "Balance");
                        $row++;
                        $journal = JournalDetail::getJournalDetail($coa, $start, $end);
                        $initial = JournalDetail::getJournalInitialBalanceCoa($coa, $start);
                        $nilaiinitial = $initial;
                        $nilaiBalance = $initial;
                        $initial = ($initial < 0 ? "(" . number_format(-1 * $initial, 2, ".", ",") . ")" : number_format($initial, 2, ".", ","));
                        $sheet->setBorder('B' . $row . ':J' . $row, 'thin');
                        $sheet->setCellValueByColumnAndRow(1, $row, date("d-M-y", strtotime($start)));
                        $sheet->setCellValueByColumnAndRow(2, $row, '');
                        $sheet->setCellValueByColumnAndRow(3, $row, 'Initial Balance');
                        $sheet->setCellValueByColumnAndRow(4, $row, '');
                        $sheet->setCellValueByColumnAndRow(5, $row, '');
                        $sheet->setCellValueByColumnAndRow(6, $row, '');
                        if ($nilaiinitial > 0) {
                            $sheet->setCellValueByColumnAndRow(7, $row, $initial);
                            $sheet->setCellValueByColumnAndRow(8, $row, '');
                            $sheet->setCellValueByColumnAndRow(9, $row, $initial);
                        } else {
                            $sheet->setCellValueByColumnAndRow(7, $row, '');
                            $sheet->setCellValueByColumnAndRow(8, $row, $initial);
                            $sheet->setCellValueByColumnAndRow(9, $row, $initial);
                        }
                        $row++;
                        if (count($journal) > 0) {
                            foreach ($journal as $a) {
                                $nilaiBalance += ( $a->JournalDebetMU - $a->JournalCreditMU);
                                //$nilaiinitialDebet += $nilaiBalance;
                                $nilaiDebet += $a->JournalDebetMU;
                                $nilaiKredit += $a->JournalCreditMU;
                                $lastrow = $sheet->getHighestRow();
                                $sheet->getStyle('D1:F' . $lastrow)->getAlignment()->setWrapText(true);
                                $sheet->setWidth('A', 10);
                                $sheet->setWidth('B', 20);
                                $sheet->setWidth('C', 25);
                                $sheet->setWidth('D', 30);
                                $sheet->setWidth('E', 30);
                                $sheet->setWidth('F', 30);
                                $sheet->setWidth('G', 30);
                                $sheet->setWidth('H', 30);
                                $sheet->setWidth('I', 30);
                                $sheet->setWidth('J', 30);
                                $sheet->setBorder('B' . $row . ':J' . $row, 'thin');
                                $sheet->setCellValueByColumnAndRow(1, $row, date("d-M-y", strtotime($a->JournalDate)));
                                $sheet->setCellValueByColumnAndRow(2, $row, $a->JournalID);
                                $sheet->setCellValueByColumnAndRow(3, $row, $a->Notes);
                                $sheet->setCellValueByColumnAndRow(4, $row, $a->JournalNotes);
                                $sheet->setCellValueByColumnAndRow(5, $row, $a->TransactionID);
                                $sheet->setCellValueByColumnAndRow(6, $row, $a->JournalTransactionID);
                                $sheet->setCellValueByColumnAndRow(7, $row, number_format($a->JournalDebetMU, 2, ".", ","));
                                $sheet->setCellValueByColumnAndRow(8, $row, number_format($a->JournalCreditMU, 2, ".", ","));
                                $sheet->setCellValueByColumnAndRow(9, $row, number_format($nilaiBalance, 2, ".", ","));
                                $row++;
                            }
                        }
                        // dd($ini);
                        $nilaiEnd = $nilaiinitial + $nilaiDebet - $nilaiKredit;
                        $sheet->setBorder('B' . $row . ':J' . $row, 'thin');

                        $nilaiEnd = ($nilaiEnd < 0 ? "(" . number_format(-1 * $nilaiEnd, 2, ".", ",") . ")" : number_format($nilaiEnd, 2, ".", ","));
                        $sheet->setBorder('B' . $row . ':J' . $row, 'thin');
                        $sheet->setCellValueByColumnAndRow(1, $row, date("d-M-y", strtotime($end)));
                        $sheet->setCellValueByColumnAndRow(2, $row, '');
                        $sheet->setCellValueByColumnAndRow(3, $row, 'End Balance');
                        $sheet->setCellValueByColumnAndRow(4, $row, '');
                        $sheet->setCellValueByColumnAndRow(5, $row, '');
                        $sheet->setCellValueByColumnAndRow(6, $row, '');
                        if ($nilaiBalance > 0) {
                            $sheet->setCellValueByColumnAndRow(7, $row, $nilaiEnd);
                            $sheet->setCellValueByColumnAndRow(8, $row, '');
                        } else {
                            $sheet->setCellValueByColumnAndRow(7, $row, '');
                            $sheet->setCellValueByColumnAndRow(8, $row, $nilaiEnd);
                        }

                        $sheet->setCellValueByColumnAndRow(9, $row, number_format($nilaiBalance, 2, ".", ","));
                        $row++;
                        $row++;
                    }
                }
                $sheet->cells('H6:J' . $row, function($cells) {
                    $cells->setAlignment('right');
                });
            });
        })->export('xls');
    }

}
