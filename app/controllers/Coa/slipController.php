<?php

class SlipController extends BaseController {

    public function showSlip() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertSlip') {
                return $this->insertSlip();
            } else if (Input::get('jenis') == 'updateSlip') {
                return $this->updateSlip();
            } else if (Input::get('jenis') == 'deleteSlip') {
                return $this->deleteSlip();
            } else if (Input::get('jenis') == 'slip') {
                return $this->reportSlip();
            } else if (Input::get('jenis') == 'slipMU') {
                return $this->reportSlipMU();
            }
        }
        return View::make('coa.slip')
                        ->withToogle('accounting')->withAktif('slip');
    }

    public static function insertSlip() {
        //rule
        $rule = array(
            'SlipID' => 'required|max:200|unique:m_slip,SlipID,NULL,SlipID,CompanyInternalID,' . Auth::user()->Company->InternalID . '',
            'SlipName' => 'required|max:200',
            'Type' => 'required',
            'coa' => 'required',
            'currency' => 'required',
            'remark' => 'required|max:1000'
        );
        $messages = array(
            'coa.required' => 'Account field is required.'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule, $messages);
        if ($validator->fails()) {
            //tidak valid
            return View::make('coa.slip')
                            ->withMessages('gagalInsert')
                            ->withError($validator->messages())
                            ->withToogle('accounting')->withAktif('slip')
                            ->withErrors($validator);
        } else {
            //valid
            $slip = new Slip;
            $slip->SlipID = Input::get('SlipID');
            $slip->SlipName = Input::get('SlipName');
            $coa = Coa::find(Input::get('coa'));
            $slip->ACC1InternalID = $coa->ACC1InternalID;
            $slip->ACC2InternalID = $coa->ACC2InternalID;
            $slip->ACC3InternalID = $coa->ACC3InternalID;
            $slip->ACC4InternalID = $coa->ACC4InternalID;
            $slip->ACC5InternalID = $coa->ACC5InternalID;
            $slip->ACC6InternalID = $coa->ACC6InternalID;
            $slip->Flag = Input::get('Type');
            $slip->CurrencyInternalID = Input::get('currency');
            $slip->UserRecord = Auth::user()->UserID;
            $slip->CompanyInternalID = Auth::user()->Company->InternalID;
            $slip->UserModified = "0";
            $slip->Remark = Input::get('remark');
            $slip->save();

            return View::make('coa.slip')
                            ->withMessages('suksesInsert')
                            ->withToogle('accounting')->withAktif('slip');
        }
    }

    static function updateSlip() {
        //rule
        $rule = array(
            'SlipName' => 'required|max:200',
            'Type' => 'required',
            'coa' => 'required',
            'currency' => 'required',
            'remark' => 'required|max:1000'
        );
        $messages = array(
            'coa.required' => 'Account field is required.'
        );

        //validasi
        $validator = Validator::make(Input::all(), $rule, $messages);
        if ($validator->fails()) {
            //tidak valid
            return View::make('coa.slip')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('accounting')->withAktif('slip');
        } else {
            //valid
            $slip = Slip::find(Input::get('InternalID'));
            if ($slip->CompanyInternalID == Auth::user()->Company->InternalID) {
                $slip->SlipName = Input::get('SlipName');
                $coa = Coa::find(Input::get('coa'));
                $slip->ACC1InternalID = $coa->ACC1InternalID;
                $slip->ACC2InternalID = $coa->ACC2InternalID;
                $slip->ACC3InternalID = $coa->ACC3InternalID;
                $slip->ACC4InternalID = $coa->ACC4InternalID;
                $slip->ACC5InternalID = $coa->ACC5InternalID;
                $slip->ACC6InternalID = $coa->ACC6InternalID;
                $slip->Flag = Input::get('Type');
                $slip->CurrencyInternalID = Input::get('currency');
                $slip->UserModified = Auth::user()->UserID;
                $slip->Remark = Input::get('remark');
                $slip->save();
                return View::make('coa.slip')
                                ->withMessages('suksesUpdate')
                                ->withToogle('accounting')->withAktif('slip');
            } else {
                return View::make('coa.slip')
                                ->withMessages('accessDenied')
                                ->withToogle('accounting')->withAktif('slip');
            }
        }
    }

    static function deleteSlip() {
        $journal = DB::table('t_journal_header')->where('SlipInternalID', Input::get('InternalID'))->first();
        //cek slip ada di pakai atau tidak
        if (is_null($journal)) {
            //tidak ada maka boleh dihapus
            $slip = Slip::find(Input::get('InternalID'));
            if ($slip->CompanyInternalID == Auth::user()->Company->InternalID) {
                $slip->delete();
                return View::make('coa.slip')
                                ->withMessages('suksesDelete')
                                ->withToogle('accounting')->withAktif('slip');
            } else {
                return View::make('coa.slip')
                                ->withMessages('accessDenied')
                                ->withToogle('accounting')->withAktif('slip');
            }
        } else {
            //ada maka tidak dihapus
            return View::make('coa.slip')
                            ->withMessages('gagalDelete')
                            ->withToogle('accounting')->withAktif('slip');
        }
    }

    public function exportExcel() {
        Excel::create('Master_Slip', function($excel) {
            $excel->sheet('Master_Slip', function($sheet) {
                $sheet->mergeCells('B1:K1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Master Slip");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Slip Name");
                $sheet->setCellValueByColumnAndRow(3, 2, "Slip ID");
                $sheet->setCellValueByColumnAndRow(4, 2, "Account Name");
                $sheet->setCellValueByColumnAndRow(5, 2, "Account ID");
                $sheet->setCellValueByColumnAndRow(6, 2, "Type");
                $sheet->setCellValueByColumnAndRow(7, 2, "Currency");
                $sheet->setCellValueByColumnAndRow(8, 2, "Record");
                $sheet->setCellValueByColumnAndRow(9, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(10, 2, "Remark");
                $row = 3;
                foreach (Slip::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                    $coa = Slip::coa($data->InternalID);
                    $data->coaID = $coa[0]->COAInternalID;
                    $currency = Slip::find($data->InternalID)->Currency;
                    $tipe = '';
                    if ($data->Flag == 0) {
                        $tipe = 'Kas';
                    } else if ($data->Flag == 1) {
                        $tipe = 'Bank';
                    } else if ($data->Flag == 2) {
                        $tipe = 'Piutang giro';
                    } else {
                        $tipe = 'Hutang giro';
                    }
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, $data->SlipName);
                    $sheet->setCellValueByColumnAndRow(3, $row, "`" . $data->SlipID);
                    $sheet->setCellValueByColumnAndRow(4, $row, $coa[0]->COAName);
                    $sheet->setCellValueByColumnAndRow(5, $row, "`" . Coa::formatCoa($data->ACC1InternalID, $data->ACC2InternalID, $data->ACC3InternalID, $data->ACC4InternalID, $data->ACC5InternalID, $data->ACC6InternalID, 1));
                    $sheet->setCellValueByColumnAndRow(6, $row, $tipe);
                    $sheet->setCellValueByColumnAndRow(7, $row, $currency->CurrencyName);
                    $sheet->setCellValueByColumnAndRow(8, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(9, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(10, $row, $data->Remark);
                    $row++;
                }

                if (Slip::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() <= 0) {
                    $sheet->mergeCells('B3:K3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "No data available in table");

                    $sheet->cells('B3:K3', function($cells) {
                        $cells->setAlignment('center');
                    });

                    $sheet->setBorder('B3:K' . $row, 'thin');
                }


                $row--;
                $sheet->setBorder('B2:K' . $row, 'thin');
                $sheet->cells('B2:K2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:K' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
            });
        })->export('xls');
    }

    public function reportSlip_lama() {
        $Initialdebet = 0;
        $Initialkredit = 0;
        $debet = 0;
        $kredit = 0;
        $Totaldebet = 0;
        $Totalkredit = 0;
        $slip = Slip::find(Input::get('slip'));
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $DetailData = Slip::reportSlip($slip, $start, $end);
        $Initial = Slip::reportSlipInitial($slip, $start, $end);
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Slip Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Slip  ' . $slip->SlipID . ' ' . $slip->SlipName . '</span><br>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Account ' . Coa::formatCoa($slip->ACC1InternalID, $slip->ACC2InternalID, $slip->ACC3InternalID, $slip->ACC4InternalID, $slip->ACC5InternalID, $slip->ACC6InternalID, 1) . ' ' . Coa::find(Coa::getInternalID($slip->ACC1InternalID, $slip->ACC2InternalID, $slip->ACC3InternalID, $slip->ACC4InternalID, $slip->ACC5InternalID, $slip->ACC6InternalID))->COAName . '</span><br>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span><br><br>';
        if (count($DetailData) <= 0) {
            $html.= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;"> There is no slip activity in this table</span>';
        } else {
            $html.= '<table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th width=15% style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Debet MU</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Credit MU</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Currency</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; width: 10%;">From</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; width: 10%;">Transaction</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Debet</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Credit</th>
                                        </tr>
                                    </thead>
                                    <tbody>';

            foreach ($DetailData as $journal) {
                $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($journal->JournalHeader->JournalDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($journal->JournalDebetMU, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($journal->JournalCreditMU, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: left">' . $journal->Currency->CurrencyName . ' (' . number_format($journal->CurrencyRate, '2', '.', ',') . ')</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: left; width: 10%;">' . $journal->JournalFrom . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: left; width: 10%;">' . ($journal->TransactionID == '' ? $journal->Notes : $journal->TransactionID) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format(($journal->JournalDebet), '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format(($journal->JournalCredit), '2', '.', ',') . '</td>
                            </tr>';
                $debet += $journal->JournalDebet;
                $kredit += $journal->JournalCredit;
            }
            foreach ($Initial as $journal) {
                $Initialdebet += $journal->JournalDebet;
                $Initialkredit += $journal->JournalCredit;
            }
            $Totaldebet = $Initialdebet + $debet;
            $Totalkredit = $Initialkredit + $kredit;

            $currency = Currency::where('CompanyInternalID', Auth::user()->Company->InternalID)->where('Default', 1)->first();

            $html.= '
            <tr>
                <td colspan="8" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: left"><hr/></td>
            </tr>
            
            <tr>
                <td colspan="4" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700;"></td>
                <td colspan="2" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: left"> Currency Default </td>
                <td colspan="2" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right">' . $currency->CurrencyID . ' ' . $currency->CurrencyName . '</td>
            </tr>
            
            <tr>
                <td colspan="4" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700;"></td>
                <td colspan = "2" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: left">Initial Balance </td>
                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right">' . ($Initialdebet <= $Initialkredit ? number_format(0, '2', '.', ',') : number_format(($Initialdebet - $Initialkredit), '2', '.', ',')) . '</td>
                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right">' . '(' . ($Initialdebet >= $Initialkredit ? number_format(0, '2', '.', ',') : number_format(($Initialkredit - $Initialdebet), '2', '.', ',')) . ')' . '</td>
            </tr>
            
            <tr>
                <td colspan="4" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700;"></td>
                <td colspan = "2" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: left"> Mutation </td>
                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right">' . number_format($debet, '2', '.', ',') . '</td>
                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right">' . '(' . number_format($kredit, '2', '.', ',') . ')' . '</td>
            </tr>
            
            <tr>
                <td colspan="4" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700;"></td>
                <td colspan = "2" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: left"> End Balance </td>
                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right">' . ($Totaldebet <= $Totalkredit ? number_format(0, '2', '.', ',') : number_format(($Totaldebet - $Totalkredit), '2', '.', '.')) . '</td>
                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right">' . '(' . ($Totaldebet >= $Totalkredit ? number_format(0, '2', '.', ',') : number_format(($Totalkredit - $Totaldebet), '2', '.', ',')) . ')' . '</td>
            </tr>
                            ';
            $html.= '</tbody>
            </table>';
        }
        $html.='
                    </div>
                </body>
            </html>';
        return PDF::load($html, 'A4', 'potrait')->download('slip_report');
    }

    public function reportSlipMU_lama() {
        $slip = Slip::find(Input::get('slip'));
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $default = $slip->Currency->Default;
        $dataSlip = JournalDetail::reportSlipMU($slip, $start, $end, $default);
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Slip Report MU</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Slip ' . $slip->SlipID . ' ' . $slip->SlipName . '</span><br>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Account ' . Coa::formatCoa($slip->ACC1InternalID, $slip->ACC2InternalID, $slip->ACC3InternalID, $slip->ACC4InternalID, $slip->ACC5InternalID, $slip->ACC6InternalID, 1) . ' ' . Coa::find(Coa::getInternalID($slip->ACC1InternalID, $slip->ACC2InternalID, $slip->ACC3InternalID, $slip->ACC4InternalID, $slip->ACC5InternalID, $slip->ACC6InternalID))->COAName . '</span><br>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span><br><br>';
        if (count($dataSlip) > 0) {
            $html.= '<table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th width=15% style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">From</th>
                                            <th width=15% style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Transaction</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Currency</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Debet</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Credit</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            $totalDebet = 0;
            $totalCredit = 0;
            foreach ($dataSlip as $data) {
                $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->JournalDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->JournalFrom . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . ($data->TransactionID == '' ? $data->Notes : $data->TransactionID) . '</td>'
                        . '     <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $slip->Currency->CurrencyName . '</td>';
                if ($default == 0) {
                    $html.= '<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->JournalDebetMU, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">(' . number_format($data->JournalCreditMU, '2', '.', ',') . ')</td>
                            </tr>';
                    $totalDebet += $data->JournalDebetMU;
                    $totalCredit += $data->JournalCreditMU;
                } else {
                    $html.= '<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->JournalDebet, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">(' . number_format($data->JournalCredit, '2', '.', ',') . ')</td>
                            </tr>';
                    $totalDebet += $data->JournalDebet;
                    $totalCredit += $data->JournalCredit;
                }
            }
            $initialDebet = JournalDetail::reportSlipInitialDebet($slip, $start, $default);
            $initialCredit = JournalDetail::reportSlipInitialCredit($slip, $start, $default);
            if ($initialDebet - $initialCredit < 0) {
                $initialCredit = ($initialDebet - $initialCredit) * -1;
                $initialDebet = 0;
            } else {
                $initialDebet = $initialDebet - $initialCredit;
                $initialCredit = 0;
            }
            $endDebet = $initialDebet + $totalDebet;
            $endCredit = $initialCredit + $totalCredit;
            if ($endDebet - $endCredit < 0) {
                $endCredit = $endDebet - $endCredit * -1;
                $endDebet = 0;
            } else {
                $endDebet = $endDebet - $endCredit;
                $endCredit = 0;
            }
            $html.= '<tr>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: right;" colspan=6><hr></td>
                    </tr>
                    <tr>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=3></td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;">Initial Balance</td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: right;">' . number_format($initialDebet, '2', '.', ',') . '</td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: right;">(' . number_format($initialCredit, '2', '.', ',') . ')</td>
                    </tr>
                    <tr>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=3></td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;">Mutation</td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: right;">' . number_format($totalDebet, '2', '.', ',') . '</td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: right;">(' . number_format($totalCredit, '2', '.', ',') . ')</td>
                    </tr>
                    <tr>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=3></td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;">End Balance</td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: right;">' . number_format($endDebet, '2', '.', ',') . '</td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: right;">(' . number_format($endCredit, '2', '.', ',') . ')</td>
                    </tr>
                </tbody>
            </table>';
        } else {
            $html.= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no slip activity in this period.</span><br><br>';
        }
        $html.='
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('slip_reportMU');
    }

    public function reportSlipMU() {
        Excel::create('Slip_Report_MU', function($excel) {
            $excel->sheet('Slip_Report_MU', function($sheet) {
                $startT = explode('-', Input::get('sDate'));
                $endT = explode('-', Input::get('eDate'));
                $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
                $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
                $slip = Slip::find(Input::get('slip'));
                if ($slip->Flag == 0) {
                    $sliptype = "Kas";
                } else {
                    $sliptype = "Bank";
                }

                $sheet->mergeCells('B1:N1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Laporan " . $sliptype);
                $sheet->mergeCells('C2:D2');
                $sheet->setCellValueByColumnAndRow(1, 2, "Periode :");
                $sheet->setCellValueByColumnAndRow(2, 2, $start . ' s/d ' . $end);
                $sheet->setCellValueByColumnAndRow(1, 3, "Slip :");
                $sheet->setCellValueByColumnAndRow(2, 3, $slip->SlipID . ' ' . $slip->SlipName);
                $row = 4;

                $sheet->cells('B' . $row . ':N' . $row, function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('B' . $row . ':N' . $row, 'thin');
                $sheet->setCellValueByColumnAndRow(1, $row, "Tanggal Doc");
                $sheet->setCellValueByColumnAndRow(2, $row, "Kode Akun");
                $sheet->setCellValueByColumnAndRow(3, $row, "Nama Akun");
                $sheet->setCellValueByColumnAndRow(4, $row, "Keterangan");
                $sheet->setCellValueByColumnAndRow(5, $row, "Remark");
                $sheet->setCellValueByColumnAndRow(6, $row, "Kepada");
                $sheet->setCellValueByColumnAndRow(7, $row, "Debet");
                $sheet->setCellValueByColumnAndRow(8, $row, "Kredit");
                $sheet->setCellValueByColumnAndRow(9, $row, "MU");
                $sheet->setCellValueByColumnAndRow(10, $row, "Kurs MU");
                $sheet->setCellValueByColumnAndRow(11, $row, "Debet IDR");
                $sheet->setCellValueByColumnAndRow(12, $row, "Kredit IDR");
                $sheet->setCellValueByColumnAndRow(13, $row, "Balance");
                $row++;

                $coa1 = $slip->ACC1InternalID;
                $coa2 = $slip->ACC2InternalID;
                $coa3 = $slip->ACC3InternalID;
                $coa4 = $slip->ACC4InternalID;
                $coa5 = $slip->ACC5InternalID;
                $coa6 = $slip->ACC6InternalID;
                //cari journal header yg detailnya ada slip
                $journalHeader = JournalHeader::whereBetween('JournalDate', Array($start, $end))
                                ->whereIn('InternalID', function($query) use ($coa1, $coa2, $coa3, $coa4, $coa5, $coa6) {
                                    $query->from('t_journal_detail')
                                    ->select('JournalInternalID')
                                    ->where('ACC1InternalID', $coa1)
                                    ->where('ACC2InternalID', $coa2)
                                    ->where('ACC3InternalID', $coa3)
                                    ->where('ACC4InternalID', $coa4)
                                    ->where('ACC5InternalID', $coa5)
                                    ->where('ACC6InternalID', $coa6);
                                })->get();
                $hitung = $totdebet = $totcredit = 0;
//                if ($startT[1] == "01") {
//                    $bulan = 12;
//                    $tahun = $startT[2] - 1;
//                } else {
//                    $bulan = $startT[1] - 1;
//                    $tahun = $startT[2];
//                }
                if (AccountValue::where("Month", $startT[1])->where("Year", $startT[2])->count() > 0) {
                    $inbalance = AccountValue::where("Month", $startT[1])->where("Year", $startT[2])->first()->InitialBalance;

                    $inbalance+= JournalHeader::whereBetween('JournalDate', Array($startT[2] . "-" . $startT[1] . "-01", $start))
                            ->join("t_journal_detail", "t_journal_detail.JournalInternalID", "=", "t_journal_header.InternalID")
                            ->where('t_journal_detail.ACC1InternalID', $coa1)
                            ->where('t_journal_detail.ACC2InternalID', $coa2)
                            ->where('t_journal_detail.ACC3InternalID', $coa3)
                            ->where('t_journal_detail.ACC4InternalID', $coa4)
                            ->where('t_journal_detail.ACC5InternalID', $coa5)
                            ->where('t_journal_detail.ACC6InternalID', $coa6)
                            ->sum('JournalDebet');
                    $inbalance-= JournalHeader::whereBetween('JournalDate', Array($startT[2] . "-" . $startT[1] . "-01", $start))
                            ->join("t_journal_detail", "t_journal_detail.JournalInternalID", "=", "t_journal_header.InternalID")
                            ->where('t_journal_detail.ACC1InternalID', $coa1)
                            ->where('t_journal_detail.ACC2InternalID', $coa2)
                            ->where('t_journal_detail.ACC3InternalID', $coa3)
                            ->where('t_journal_detail.ACC4InternalID', $coa4)
                            ->where('t_journal_detail.ACC5InternalID', $coa5)
                            ->where('t_journal_detail.ACC6InternalID', $coa6)
                            ->sum('JournalCredit');
                } else {
                    $inbalance = Coa::where('ACC1InternalID', $coa1)
                                    ->where('ACC2InternalID', $coa2)
                                    ->where('ACC3InternalID', $coa3)
                                    ->where('ACC4InternalID', $coa4)
                                    ->where('ACC5InternalID', $coa5)
                                    ->where('ACC6InternalID', $coa6)->first()->InitialValue;

                    $inbalance+= JournalHeader::where('JournalDate', "<", $start)
                            ->join("t_journal_detail", "t_journal_detail.JournalInternalID", "=", "t_journal_header.InternalID")
                            ->where('t_journal_detail.ACC1InternalID', $coa1)
                            ->where('t_journal_detail.ACC2InternalID', $coa2)
                            ->where('t_journal_detail.ACC3InternalID', $coa3)
                            ->where('t_journal_detail.ACC4InternalID', $coa4)
                            ->where('t_journal_detail.ACC5InternalID', $coa5)
                            ->where('t_journal_detail.ACC6InternalID', $coa6)
                            ->sum('JournalDebet');
                    $inbalance-= JournalHeader::where('JournalDate', "<", $start)
                            ->join("t_journal_detail", "t_journal_detail.JournalInternalID", "=", "t_journal_header.InternalID")
                            ->where('t_journal_detail.ACC1InternalID', $coa1)
                            ->where('t_journal_detail.ACC2InternalID', $coa2)
                            ->where('t_journal_detail.ACC3InternalID', $coa3)
                            ->where('t_journal_detail.ACC4InternalID', $coa4)
                            ->where('t_journal_detail.ACC5InternalID', $coa5)
                            ->where('t_journal_detail.ACC6InternalID', $coa6)
                            ->sum('JournalCredit');
                }
                $balance = $inbalance;
                if (count($journalHeader) > 0) {
                    foreach ($journalHeader as $jh) {
                        $journalDetail = JournalDetail::where("JournalInternalID", $jh->InternalID)->get();
                        foreach ($journalDetail as $jd) {
                            //ambil yang bukan slip 
                            if (!($jd->ACC1InternalID == $slip->ACC1InternalID && $jd->ACC2InternalID == $slip->ACC2InternalID && $jd->ACC3InternalID == $slip->ACC3InternalID && $jd->ACC4InternalID == $slip->ACC4InternalID && $jd->ACC5InternalID == $slip->ACC5InternalID && $jd->ACC6InternalID == $slip->ACC6InternalID)) {
                                $debetMU = $jd->JournalCreditMU;
                                $creditMU = $jd->JournalDebetMU;
                                $debet = $jd->JournalCredit;
                                $credit = $jd->JournalDebet;
                                $sheet->setBorder('B' . $row . ':N' . $row, 'thin');
                                $sheet->setCellValueByColumnAndRow(1, $row, date("d F y", strtotime($jh->JournalDate)));
                                $sheet->setCellValueByColumnAndRow(2, $row, Coa::formatCoa($jd->ACC1InternalID, $jd->ACC2InternalID, $jd->ACC3InternalID, $jd->ACC4InternalID, $jd->ACC5InternalID, $jd->ACC6InternalID, 1));
                                $sheet->setCellValueByColumnAndRow(3, $row, Coa::find(Coa::getInternalID($jd->ACC1InternalID, $jd->ACC2InternalID, $jd->ACC3InternalID, $jd->ACC4InternalID, $jd->ACC5InternalID, $jd->ACC6InternalID))->COAName);
                                $sheet->setCellValueByColumnAndRow(4, $row, $jh->Notes);
                                $sheet->setCellValueByColumnAndRow(5, $row, $jh->Remark);
                                $sheet->setCellValueByColumnAndRow(6, $row, $jh->From);
                                $sheet->setCellValueByColumnAndRow(7, $row, number_format($debetMU, '2', '.', ','));
                                $sheet->setCellValueByColumnAndRow(8, $row, number_format($creditMU, '2', '.', ','));
                                $sheet->setCellValueByColumnAndRow(9, $row, Currency::find($jd->CurrencyInternalID)->CurrencyName);
                                $sheet->setCellValueByColumnAndRow(10, $row, number_format($jd->CurrencyRate, '2', '.', ','));
                                $sheet->setCellValueByColumnAndRow(11, $row, number_format($debet, '2', '.', ','));
                                $sheet->setCellValueByColumnAndRow(12, $row, number_format($credit, '2', '.', ','));
                                $balance -=$credit;
                                $balance += $debet;
                                $totdebet += $debet;
                                $totcredit += $credit;
                                $sheet->setCellValueByColumnAndRow(13, $row, number_format($balance, '2', '.', ','));
                                $row++;
                                $hitung++;
                            }
                        }
                    }
                }

                if ($hitung == 0) {
                    $sheet->mergeCells('B' . $row . ':N' . $row);
                    $sheet->setCellValueByColumnAndRow(1, $row, "There is no report.");
                } else {
                    $row++;
                    $sheet->setCellValueByColumnAndRow(8, $row, "Saldo Awal (IDR)");
                    $sheet->setCellValueByColumnAndRow(9, $row, ":");
                    $sheet->setCellValueByColumnAndRow(10, $row, number_format($inbalance, '2', '.', ','));
                    $row++;
                    $sheet->setCellValueByColumnAndRow(10, $row, number_format($totdebet, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(11, $row, number_format($totcredit, '2', '.', ','));
                    $row++;
                    $sheet->setCellValueByColumnAndRow(8, $row, "Saldo Akhir (IDR)");
                    $sheet->setCellValueByColumnAndRow(9, $row, ":");
                    $sheet->setCellValueByColumnAndRow(11, $row, number_format($balance, '2', '.', ','));
                    $row++;
                    $sheet->setCellValueByColumnAndRow(8, $row, "Kontrol (IDR)");
                    $sheet->setCellValueByColumnAndRow(9, $row, ":");
                    $sheet->setCellValueByColumnAndRow(10, $row, number_format($inbalance + $totdebet, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(11, $row, number_format($balance + $totcredit, '2', '.', ','));
                }
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
            });
        })->export('xls');
    }

    public function reportSlip() {
        Excel::create('Slip_Report', function($excel) {
            $excel->sheet('Slip_Report', function($sheet) {
                $startT = explode('-', Input::get('sDate'));
                $endT = explode('-', Input::get('eDate'));
                $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
                $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
                $slip = Slip::find(Input::get('slip'));
                if ($slip->Flag == 0) {
                    $sliptype = "Kas";
                } else {
                    $sliptype = "Bank";
                }

                $sheet->mergeCells('B1:J1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Laporan " . $sliptype);
                $sheet->mergeCells('C2:D2');
                $sheet->setCellValueByColumnAndRow(1, 2, "Periode :");
                $sheet->setCellValueByColumnAndRow(2, 2, $start . ' s/d ' . $end);
                $sheet->setCellValueByColumnAndRow(1, 3, "Slip :");
                $sheet->setCellValueByColumnAndRow(2, 3, $slip->SlipID . ' ' . $slip->SlipName);
                $row = 4;

                $sheet->cells('B' . $row . ':J' . $row, function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setFontWeight('bold');
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('B' . $row . ':J' . $row, 'thin');
                $sheet->setCellValueByColumnAndRow(1, $row, "Tanggal Doc");
                $sheet->setCellValueByColumnAndRow(2, $row, "Kode Akun");
                $sheet->setCellValueByColumnAndRow(3, $row, "Nama Akun");
                $sheet->setCellValueByColumnAndRow(4, $row, "Keterangan");
                $sheet->setCellValueByColumnAndRow(5, $row, "Remark");
                $sheet->setCellValueByColumnAndRow(6, $row, "Kepada");
                $sheet->setCellValueByColumnAndRow(7, $row, "Debet IDR");
                $sheet->setCellValueByColumnAndRow(8, $row, "Kredit IDR");
                $sheet->setCellValueByColumnAndRow(9, $row, "Balance");
                $row++;

                $coa1 = $slip->ACC1InternalID;
                $coa2 = $slip->ACC2InternalID;
                $coa3 = $slip->ACC3InternalID;
                $coa4 = $slip->ACC4InternalID;
                $coa5 = $slip->ACC5InternalID;
                $coa6 = $slip->ACC6InternalID;
                //cari journal header yg detailnya ada slip
                $journalHeader = JournalHeader::whereIn('InternalID', function($query) use ($coa1, $coa2, $coa3, $coa4, $coa5, $coa6) {
                            $query->from('t_journal_detail')
                                    ->select('JournalInternalID')
                                    ->where('ACC1InternalID', $coa1)
                                    ->where('ACC2InternalID', $coa2)
                                    ->where('ACC3InternalID', $coa3)
                                    ->where('ACC4InternalID', $coa4)
                                    ->where('ACC5InternalID', $coa5)
                                    ->where('ACC6InternalID', $coa6);
                        })->get();
                $hitung = $totdebet = $totcredit = 0;
                if (AccountValue::where("Month", $startT[1])->where("Year", $startT[2])->count() > 0) {
                    $inbalance = AccountValue::where("Month", $startT[1])->where("Year", $startT[2])->first()->InitialBalance;

                    $inbalance+= JournalHeader::whereBetween('JournalDate', Array($startT[2] . "-" . $startT[1] . "-01", $start))
                            ->join("t_journal_detail", "t_journal_detail.JournalInternalID", "=", "t_journal_header.InternalID")
                            ->where('t_journal_detail.ACC1InternalID', $coa1)
                            ->where('t_journal_detail.ACC2InternalID', $coa2)
                            ->where('t_journal_detail.ACC3InternalID', $coa3)
                            ->where('t_journal_detail.ACC4InternalID', $coa4)
                            ->where('t_journal_detail.ACC5InternalID', $coa5)
                            ->where('t_journal_detail.ACC6InternalID', $coa6)
                            ->sum('JournalDebet');
                    $inbalance-= JournalHeader::whereBetween('JournalDate', Array($startT[2] . "-" . $startT[1] . "-01", $start))
                            ->join("t_journal_detail", "t_journal_detail.JournalInternalID", "=", "t_journal_header.InternalID")
                            ->where('t_journal_detail.ACC1InternalID', $coa1)
                            ->where('t_journal_detail.ACC2InternalID', $coa2)
                            ->where('t_journal_detail.ACC3InternalID', $coa3)
                            ->where('t_journal_detail.ACC4InternalID', $coa4)
                            ->where('t_journal_detail.ACC5InternalID', $coa5)
                            ->where('t_journal_detail.ACC6InternalID', $coa6)
                            ->sum('JournalCredit');
                } else {
                    $inbalance = Coa::where('ACC1InternalID', $coa1)
                                    ->where('ACC2InternalID', $coa2)
                                    ->where('ACC3InternalID', $coa3)
                                    ->where('ACC4InternalID', $coa4)
                                    ->where('ACC5InternalID', $coa5)
                                    ->where('ACC6InternalID', $coa6)->first()->InitialValue;

                    $inbalance+= JournalHeader::where('JournalDate', "<", $start)
                            ->join("t_journal_detail", "t_journal_detail.JournalInternalID", "=", "t_journal_header.InternalID")
                            ->where('t_journal_detail.ACC1InternalID', $coa1)
                            ->where('t_journal_detail.ACC2InternalID', $coa2)
                            ->where('t_journal_detail.ACC3InternalID', $coa3)
                            ->where('t_journal_detail.ACC4InternalID', $coa4)
                            ->where('t_journal_detail.ACC5InternalID', $coa5)
                            ->where('t_journal_detail.ACC6InternalID', $coa6)
                            ->sum('JournalDebet');
                    $inbalance-= JournalHeader::where('JournalDate', "<", $start)
                            ->join("t_journal_detail", "t_journal_detail.JournalInternalID", "=", "t_journal_header.InternalID")
                            ->where('t_journal_detail.ACC1InternalID', $coa1)
                            ->where('t_journal_detail.ACC2InternalID', $coa2)
                            ->where('t_journal_detail.ACC3InternalID', $coa3)
                            ->where('t_journal_detail.ACC4InternalID', $coa4)
                            ->where('t_journal_detail.ACC5InternalID', $coa5)
                            ->where('t_journal_detail.ACC6InternalID', $coa6)
                            ->sum('JournalCredit');
                }
                $balance = $inbalance;
                if (count($journalHeader) > 0) {
                    foreach ($journalHeader as $jh) {
                        $journalDetail = JournalDetail::where("JournalInternalID", $jh->InternalID)->get();
                        foreach ($journalDetail as $jd) {
                            //ambil yang bukan slip 
                            if (!($jd->ACC1InternalID == $slip->ACC1InternalID && $jd->ACC2InternalID == $slip->ACC2InternalID && $jd->ACC3InternalID == $slip->ACC3InternalID && $jd->ACC4InternalID == $slip->ACC4InternalID && $jd->ACC5InternalID == $slip->ACC5InternalID && $jd->ACC6InternalID == $slip->ACC6InternalID)) {
                                $debet = $jd->JournalCredit;
                                $credit = $jd->JournalDebet;
                                $sheet->setBorder('B' . $row . ':J' . $row, 'thin');
                                $sheet->setCellValueByColumnAndRow(1, $row, date("d F y", strtotime($jh->JournalDate)));
                                $sheet->setCellValueByColumnAndRow(2, $row, Coa::formatCoa($jd->ACC1InternalID, $jd->ACC2InternalID, $jd->ACC3InternalID, $jd->ACC4InternalID, $jd->ACC5InternalID, $jd->ACC6InternalID, 1));
                                $sheet->setCellValueByColumnAndRow(3, $row, Coa::find(Coa::getInternalID($jd->ACC1InternalID, $jd->ACC2InternalID, $jd->ACC3InternalID, $jd->ACC4InternalID, $jd->ACC5InternalID, $jd->ACC6InternalID))->COAName);
                                $sheet->setCellValueByColumnAndRow(4, $row, $jh->Notes);
                                $sheet->setCellValueByColumnAndRow(5, $row, $jh->Remark);
                                $sheet->setCellValueByColumnAndRow(6, $row, $jh->From);
                                $sheet->setCellValueByColumnAndRow(7, $row, number_format($debet, '2', '.', ','));
                                $sheet->setCellValueByColumnAndRow(8, $row, number_format($credit, '2', '.', ','));
                                $balance -=$credit;
                                $balance += $debet;
                                $totdebet += $debet;
                                $totcredit += $credit;
                                $sheet->setCellValueByColumnAndRow(9, $row, number_format($balance, '2', '.', ','));
                                $row++;
                                $hitung++;
                            }
                        }
                    }
                }

                if ($hitung == 0) {
                    $sheet->mergeCells('B' . $row . ':J' . $row);
                    $sheet->setCellValueByColumnAndRow(1, $row, "There is no report.");
                } else {
                    $row++;
                    $sheet->setCellValueByColumnAndRow(6, $row, "Saldo Awal (IDR)");
                    $sheet->setCellValueByColumnAndRow(7, $row, ":");
                    $sheet->setCellValueByColumnAndRow(8, $row, number_format($inbalance, '2', '.', ','));
                    $row++;
                    $sheet->setCellValueByColumnAndRow(8, $row, number_format($totdebet, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(9, $row, number_format($totcredit, '2', '.', ','));
                    $row++;
                    $sheet->setCellValueByColumnAndRow(6, $row, "Saldo Akhir (IDR)");
                    $sheet->setCellValueByColumnAndRow(7, $row, ":");
                    $sheet->setCellValueByColumnAndRow(9, $row, number_format($balance, '2', '.', ','));
                    $row++;
                    $sheet->setCellValueByColumnAndRow(6, $row, "Kontrol (IDR)");
                    $sheet->setCellValueByColumnAndRow(7, $row, ":");
                    $sheet->setCellValueByColumnAndRow(8, $row, number_format($inbalance + $totdebet, '2', '.', ','));
                    $sheet->setCellValueByColumnAndRow(9, $row, number_format($balance + $totcredit, '2', '.', ','));
                }
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
            });
        })->export('xls');
    }

}
