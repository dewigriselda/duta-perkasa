<?php

class TopUpController extends BaseController {

    public function showTopup() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteTopUp') {
                return $this->deleteTopUp();
            }
//            if (Input::get('jenis') == 'insertCustomertopup') {
//                return $this->insertCustomertopup();
//            } else if (Input::get('jenis') == 'updateCustomertopup') {
//                return $this->updateCustomertopup();
//            } else if (Input::get('jenis') == 'deleteCustomertopup') {
//                return $this->deleteCustomertopup();
//            } else if (Input::get('jenis') == 'ClosedCustomertopup') {
//                return $this->closedCustomertopup();
//            } else if (Input::get('jenis') == 'OpenCustomertopup') {
//                return $this->openCustomertopup();
//            } else if (Input::get('jenis') == 'topupSalesOrder') {
//                if (Input::get('type') == 'view') {
//                    return $this->topupSalesOrder();
//                } else {
//                    return $this->topupSalesOrderExcel();
//                }
//            }
        }
        return View::make('coa.topup')
                        ->withToogle('accounting')->withAktif('topup');
    }

    public function topUpNew() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteTopUp') {
                return $this->deleteTopUp();
            }
//            if (Input::get('jenis') == 'deleteSalesOrder') {
//                return $this->deleteSalesOrder();
//            } else if (Input::get('jenis') == 'summarySalesOrder') {
//                return $this->summarySalesOrder();
//            } else if (Input::get('jenis') == 'detailSalesOrder') {
//                return $this->detailSalesOrder();
//            } else if (Input::get('jenis') == 'printSalesOrder') {
//                if (Input::get('type') == 'exclude') {
//                    return Redirect::route('salesOrderPrint', Input::get('internalID'));
//                } else {
//                    return Redirect::route('salesOrderIncludePrint', Input::get('internalID'));
//                }
//            } else if (Input::get('jenis') == 'approveSalesOrder') {
//                return $this->approveSalesOrder();
//            } else if (Input::get('jenis') == 'insertCustomer') {
//                $this->insertCustomer();
//                return $this->insertQuotation2();
//            } else {
            return $this->insertTopup();
//            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
//            $data = TopUp::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
            return View::make('coa.topup')
                            ->withToogle('accounting')->withAktif('topup')
//                            ->withData($data)
            ;
        }
        $topup = $this->createID(0, "customer") . '.';
        return View::make('coa.topupNew')
                        ->withToogle('accounting')->withAktif('topup')
                        ->withTopup($topup);
    }

    public function topUpUpdate($id) {
        $internalid = TopUp::getIdtopUp($id);
        $topup = TopUp::find($internalid);
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteTopUp') {
                return $this->deleteTopUp();
            }
//            if (Input::get('jenis') == 'deleteSalesOrder') {
//                return $this->deleteSalesOrder();
//            } else if (Input::get('jenis') == 'summarySalesOrder') {
//                return $this->summarySalesOrder();
//            } else if (Input::get('jenis') == 'detailSalesOrder') {
//                return $this->detailSalesOrder();
//            } else if (Input::get('jenis') == 'printSalesOrder') {
//                if (Input::get('type') == 'exclude') {
//                    return Redirect::route('salesOrderPrint', Input::get('internalID'));
//                } else {
//                    return Redirect::route('salesOrderIncludePrint', Input::get('internalID'));
//                }
//            } else if (Input::get('jenis') == 'approveSalesOrder') {
//                return $this->approveSalesOrder();
//            } else if (Input::get('jenis') == 'insertCustomer') {
//                $this->insertCustomer();
//                return $this->insertQuotation2();
//            } else {
            return $this->updateTopup();
//            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = TopUp::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
            return View::make('penjualanAdd.salesOrderSearch')
                            ->withToogle('transaction')->withAktif('salesOrder')
                            ->withData($data);
        }

        return View::make('coa.topupUpdate')
                        ->withToogle('accounting')->withAktif('topup')
                        ->withId($id)->withTopup($topup)->withInternalid($internalid)
                        ->withTopup($topup);
    }

    public function insertTopup() {
//        dd(Input::get('paymentType'));
        //rule
        if (Input::get('paymentType') == '1') {
            $rule = array(
//                'TopUpID' => 'required',
                'downpayment' => 'required',
                'account' => 'required',
                'date' => 'required',
                'currency' => 'required',
                'paymentType' => 'required',
                'remark' => 'max:1000'
            );
        } else {
            if (Input::get("type") == "customer") {
                $rule = array(
//                    'TopUpID' => 'required',
                    'salesOrderID' => 'required',
                    'downpayment' => 'required',
                    'account' => 'required',
                    'date' => 'required',
                    'currency' => 'required',
                    'paymentType' => 'required',
                    'remark' => 'max:1000'
                );
            } else {
                $rule = array(
//                    'TopUpID' => 'required',
                    'purchaseOrderID' => 'required',
                    'downpayment' => 'required',
                    'account' => 'required',
                    'date' => 'required',
                    'currency' => 'required',
                    'paymentType' => 'required',
                    'remark' => 'max:1000'
                );
            }
        }
        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
//            dd($validator->messages());
            //tidak valid
            return View::make('coa.topup')
                            ->withMessages('gagalInsert')
                            ->withError($validator->messages())
                            ->withToogle('accounting')->withAktif('topup')
                            ->withErrors($validator);
        } else {
            $total1 = $total2 = $total3 = 0;
            //valid
            if (Input::get("type") == "customer")
                $id = TopUp::getNextIDCustomerTopUp($this->createID(0, Input::get("type")) . '.');
            else
                $id = TopUp::getNextIDSupplierTopUp($this->createID(0, Input::get("type")) . '.');
            //yg ada ppn 
            $downpayment = str_replace(",", "", Input::get('downpayment'));
            //yg ga ada ppn
            if (Input::get('vat') == '')
                $grandTotal = str_replace(",", "", Input::get('downpayment'));
//            $grandTotal = str_replace(",", "", Input::get('grandTotalValue'));
            else
                $grandTotal = 10 / 11 * $downpayment;
            $selba = str_replace(",", "", Input::get('selisihbayar'));
            $date = explode('-', Input::get('date'));
            $customertopup = new TopUp();
            if (Input::get("type") == "customer")
                $customertopup->ACC6InternalID = Input::get('coa6');
            else
                $customertopup->ACC6InternalID = Input::get('coa62');
            $customertopup->TopUpID = $id;
            $customertopup->TopUpDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $customertopup->SlipInternalID = Input::get('account');
            $customertopup->CurrencyInternalID = Input::get('currency');
            $customertopup->CurrencyRate = Input::get('rate');
            //non PPN
            if (Input::get('vat') == '') {
                $customertopup->VAT = '0';
                $total1 = $grandTotal;
                $total2 = $grandTotal + $selba;
                $total3 = 0;
            }
            //PPN
            else {
                $customertopup->VAT = 1;
                $total1 = $downpayment;
//                $total1 = $grandTotal;
                if ($downpayment > 0) {
//                if ($grandTotal > 0) {
                    $total2 = ceil(10 / 11 * $downpayment + $selba);
                    $total3 = floor(1 / 11 * round($downpayment));
//                    $total2 = ceil(10 / 11 * $grandTotal + $selba);
//                    $total3 = floor(1 / 11 * round($grandTotal));
                } else {
                    $total2 = ceil(10 / 11 * $selba);
                    $total3 = floor(1 / 11 * round($selba));
                }
            }
            $customertopup->PaymentType = Input::get('paymentType');
            if (Input::get('paymentType') != '1') {
                if (Input::get("type") == "customer")
                    $customertopup->SalesOrderInternalID = Input::get('salesOrderID');
                else
                    $customertopup->PurchaseOrderInternalID = Input::get('purchaseOrderID');
            }
            if (Input::get('TaxNumber') != '') {
                $customertopup->Tax = Input::get('TaxNumber');
            } else {
                $customertopup->Tax = ".-.";
            }
            $customertopup->Type = Input::get('type');
            $customertopup->SelisihBayar = Input::get('selisihbayar');
            //tanpa pajak
            $customertopup->TopUp = ceil($grandTotal);
            //dengan pajak
            $customertopup->GrandTotalTopUp = $downpayment;
            $customertopup->UserRecord = Auth::user()->UserID;
            $customertopup->UserModified = "0";
            $customertopup->CompanyInternalID = Auth::user()->Company->InternalID;
            $customertopup->Remark = Input::get('remark');
            $customertopup->save();

            if (Input::get('paymentType') == '0') {
                $amount = Topup::where('SalesOrderInternalID', Input::get('salesOrderID'))->sum('GrandTotalTopUp');
                $salesOrder = SalesOrderHeader::find(Input::get('salesOrderID'))->GrandTotal;
                $price = $salesOrder - $amount;
                $SO = SalesOrderHeader::find(Input::get('salesOrderID'));
                if ($price > 0) {
                    $SO->is_CBD_Finished = 0;
                } else {
                    $SO->is_CBD_Finished = 1;
                }
                $SO->save();
            }
// dd(Coa6::find($customertopup->ACC6InternalID)->COAInternalID);
//public function insertJournal($memoNumber, $date, $slip, $tax, $total1, $total2, $total3, $coa6) {
            if (Input::get("type") == 'customer')
//                $this->insertJournalCustomer($customertopup->TopUpID, $customertopup->dtRecord, $customertopup->SlipInternalID, $customertopup->VAT, $total1, $total2, $total3, Coa6::find($customertopup->ACC6InternalID));
                $this->insertJournalCustomer($customertopup->TopUpID, $customertopup->dtRecord, $customertopup->SlipInternalID, $customertopup->VAT, $total1, $total2, $total3, Coa6::find($customertopup->ACC6InternalID), $customertopup->SelisihBayar, $customertopup->PaymentType);
            else
                $this->insertJournalSupplier($customertopup->TopUpID, $customertopup->dtRecord, $customertopup->SlipInternalID, $customertopup->VAT, $total1, $total2, $total3, Coa6::find($customertopup->ACC6InternalID), $customertopup->SelisihBayar, $customertopup->PaymentType);
//                $this->insertJournalSupplier($customertopup->TopUpID, $customertopup->dtRecord, $customertopup->SlipInternalID, $customertopup->VAT, $total1, $total2, $total3, Coa6::find($customertopup->ACC6InternalID));
//            return View::make('coa.topup')
//                            ->withMessages('suksesInsert')
//                            ->withToogle('accounting')->withAktif('customertopup');
            return Redirect::Route('showTopUp');
        }
    }

    public function updateTopUp() {
        //rule
        $rule = array(
            'grandTotalValue' => 'required',
            'account' => 'required',
            'date' => 'required',
            'paymentType' => 'required',
            'currency' => 'required',
            'remark' => 'max:1000'
        );
        //validasi
        $validator = Validator ::make(Input::all(), $rule);
        if ($validator->fails()) {
            dd($validator->messages());
            //tidak valid
            return View::make('coa.topup')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('accounting')->withAktif('topup');
        } else {
            $downpayment = str_replace(",", "", Input::get('downpayment'));
            //yg ga ada ppn
            if (Input::get('vat') == '')
                $grandTotal = str_replace(",", "", Input::get('downpayment'));
//            $grandTotal = str_replace(",", "", Input::get('grandTotalValue'));
            else
                $grandTotal = 10 / 11 * $downpayment;
            $total1 = $total2 = $total3 = 0;
            //valid
            $customertopup = Topup::find(Input::get('InternalID'));
            $selba = str_replace(",", "", Input::get('selisihbayar'));
            $date = explode('-', Input::get('date'));
            if ($customertopup->CompanyInternalID == Auth::user()->Company->InternalID) {
                $customertopup->SlipInternalID = Input::get('account');
                $customertopup->CurrencyInternalID = Input::get('currency');
                $customertopup->CurrencyRate = Input::get('rate');
                if (Input::get('vat') == '') {
                    $customertopup->VAT = '0';
                    $total1 = $grandTotal;
                    $total2 = $grandTotal;
                    $total3 = 0;
                }//PPN
                else {
                    $customertopup->VAT = 1;
                    $total1 = $downpayment;
//                $total1 = $grandTotal;
                    if ($downpayment > 0) {
//                if ($grandTotal > 0) {
                        $total2 = ceil(10 / 11 * $downpayment + $selba);
                        $total3 = floor(1 / 11 * round($downpayment));
//                    $total2 = ceil(10 / 11 * $grandTotal + $selba);
//                    $total3 = floor(1 / 11 * round($grandTotal));
                    } else {
                        $total2 = ceil(10 / 11 * $selba);
                        $total3 = floor(1 / 11 * round($selba));
                    }
                }
                $customertopup->PaymentType = Input::get('paymentType');
                if (Input::get('paymentType') != '1') {
                    if (Input::get("type") == "customer")
                        $customertopup->SalesOrderInternalID = Input::get('salesOrderID');
                    else
                        $customertopup->PurchaseOrderInternalID = Input::get('purchaseOrderID');
                }
                if (Input::get('TaxNumber') != '') {
                    $customertopup->Tax = Input::get('TaxNumber');
                } else {
                    $customertopup->Tax = ".-.";
                }
                $customertopup->Type = Input::get('type');
                $customertopup->SelisihBayar = Input::get('selisihbayar');
                $customertopup->TopUp = ceil($grandTotal);
                $customertopup->TopUpDate = $date[2] . '-' . $date[1] . '-' . $date[0];
                $customertopup->GrandTotalTopUp = $downpayment;
                $customertopup->UserModified = Auth::user()->UserID;
                $customertopup->Remark = Input::get('remark');
                $customertopup->save();

                if (Input::get('paymentType') == '0') {
                    $amount = Topup::where('SalesOrderInternalID', Input::get('salesOrderID'))->sum('GrandTotalTopUp');
                    $salesOrder = SalesOrderHeader::find(Input::get('salesOrderID'))->GrandTotal;
                    $price = $salesOrder - $amount;
                    $SO = SalesOrderHeader::find(Input::get('salesOrderID'));
                    if ($price > 0) {
                        $SO->is_CBD_Finished = 0;
                    } else {
                        $SO->is_CBD_Finished = 1;
                    }
                    $SO->save();
                }
//hapus jurnal yang lama
                $journal = JournalHeader::where('TransactionID', '=', $customertopup->TopUpID)->get();
                foreach ($journal as $value) {
                    JournalDetail::where('JournalInternalID', '=', $value->InternalID)->delete();
                    JournalHeader::where('InternalID', '=', $value->InternalID)->delete();
                }
//insert jurnal yang baru
                if (Input::get("type") == 'customer')
                    $this->insertJournalCustomer($customertopup->TopUpID, $customertopup->dtRecord, $customertopup->SlipInternalID, $customertopup->VAT, $total1, $total2, $total3, Coa6::find($customertopup->ACC6InternalID), $customertopup->SelisihBayar, $customertopup->PaymentType);
                else
                    $this->insertJournalSupplier($customertopup->TopUpID, $customertopup->dtRecord, $customertopup->SlipInternalID, $customertopup->VAT, $total1, $total2, $total3, Coa6::find($customertopup->ACC6InternalID), $customertopup->SelisihBayar, $customertopup->PaymentType);

                return View::make('coa.topup')
                                ->withMessages('suksesUpdate')
                                ->withToogle('accounting')->withAktif('topup');
            } else {
                return View::make('coa.topup')
                                ->withMessages('accessDenied')
                                ->withToogle('accounting')->withAktif('topup');
            }
        }
    }

    public function deleteTopUp() {
        //tidak ada maka boleh dihapus
        $customertopup = TopUp::find(Input::get('InternalID'));
        if ($customertopup->CompanyInternalID == Auth::user()->CompanyInternalID) {
            $journal = JournalHeader::where('TransactionID', '=', $customertopup->TopUpID)->get();
            foreach ($journal as $value) {
                JournalDetail::where('JournalInternalID', '=', $value->InternalID)->delete();
                JournalHeader::where('InternalID', '=', $value->InternalID)->delete();
            }
            $customertopup->delete();
            if ($customertopup->PaymentType == '0') {
                $SO = SalesOrderHeader::find($customertopup->SalesOrderInternalID);
                $SO->is_CBD_Finished = 0;
                $SO->save();
            }
            return View::make('coa.topup')
                            ->withMessages('suksesDelete')
                            ->withToogle('accounting')->withAktif('topup');
        } else {
            return View::make('coa.topup')
                            ->withMessages('accessDenied')
                            ->withToogle('accounting')->withAktif('topup');
        }
    }

    public function closedCustomertopup() {
        $header = Customertopup::find(Input::get('InternalID'));
        $header->Closed = "Closed";
        $header->save();
        return View::make('coa.customertopup')
                        ->withMessages('suksesClosed')
                        ->withToogle('accounting')->withAktif('customertopup');
    }

    public function openCustomertopup() {
        $header = Customertopup::find(Input::get('InternalID'));
        $header->Closed = "Open";
        $header->save();
        return View::make('coa.customertopup')
                        ->withMessages('suksesOpen')
                        ->withToogle('accounting')->withAktif('customertopup');
    }

    public function insertJournalCustomer($memoNumber, $date, $slip, $tax, $total1, $total2, $total3, $coa6, $selisih, $typePayment) {
        $flag = Slip::find($slip)->Flag;
        $namePayment = "";
        if ($typePayment == 0) {
            $namePayment = "CBD";
        } else if ($typePayment == 1) {
            $namePayment = "Deposit";
        } else {
            $namePayment = "DP";
        }

        $header = new JournalHeader;
        $defaultPPN = Default_s::getInternalCoa('Sales Tax');
        $defaultPPN2 = Default_s::find(3);
        //coa : SELISIH BAYAR
//        $akunselisih = Coa::find(170);
        //akun asli
        $akunselisih = Coa::find(164);
        $datee = explode('-', $date);
        $cari = '';
        if ($flag == 0) {
            $cari = 'CI-' . $datee[0] . '-' . $datee[1];
            $header->JournalType = 'Cash In';
        } else {
            $cari = 'BI-' . $datee[0] . '-' . $datee[1];
            $header->JournalType = 'Bank In';
        }
        $header->JournalID = JournalHeader::getNextIDJournal($cari . '-');
        $header->SlipInternalID = $slip;
        $header->JournalDate = $date;
        $header->JournalFrom = Auth::user()->UserID;
        $header->Notes = $memoNumber . "-" . $coa6->ACC6Name;
        $department = Department::where('Default', '1')->first();
        $header->DepartmentInternalID = $department->InternalID;
        $header->TransactionID = $memoNumber;
        $header->ACC5InternalID = '0';
        $header->Lock = '0';
        $header->Check = '0';
        $header->Flag = '0';
        $header->UserRecord = Auth::user()->UserID;
        $header->CompanyInternalID = Auth::user()->Company->InternalID;
        $header->UserModified = '0';
        $header->Remark = "TopupCustomer-" . $namePayment;
        $header->save();

        $currency = Currency::where('Default', 1)->first()->InternalID;
        if ($total1 > 0) {
            //insert detail untuk slip
            $detail = new JournalDetail();
            $detail->JournalInternalID = $header->InternalID;
            $detail->JournalIndex = 1;
            $detail->JournalNotes = 'Slip';
            $detail->JournalDebetMU = round($total1);
            $detail->JournalCreditMU = 0;
            $detail->CurrencyInternalID = $currency;
            $detail->CurrencyRate = 1;
            $detail->JournalDebet = round($total1);
            $detail->JournalCredit = 0;
            $detail->JournalTransactionID = NULL;
            $default = Slip::find($slip);
            $detail->ACC1InternalID = $default->ACC1InternalID;
            $detail->ACC2InternalID = $default->ACC2InternalID;
            $detail->ACC3InternalID = $default->ACC3InternalID;
            $detail->ACC4InternalID = $default->ACC4InternalID;
            $detail->ACC5InternalID = $default->ACC5InternalID;
            $detail->ACC6InternalID = $default->ACC6InternalID;
            $coa = Coa::getInternalID($default->ACC1InternalID, $default->ACC2InternalID, $default->ACC3InternalID, $default->ACC4InternalID, $default->ACC5InternalID, $default->ACC6InternalID);
            $detail->COAName = Coa::find($coa)->COAName;
            $detail->UserRecord = Auth::user()->UserID;
            $detail->UserModified = '0';
            $detail->save();
        }
        //insert detail untuk customer dp account
        $detail = new JournalDetail();
        $detail->JournalInternalID = $header->InternalID;
        $detail->JournalIndex = 2;
        $detail->JournalNotes = 'Customer Down Payment';
        $detail->JournalDebetMU = 0;
        $detail->JournalCreditMU = ceil($total2);
        $detail->CurrencyInternalID = $currency;
        $detail->CurrencyRate = 1;
        $detail->JournalDebet = 0;
        $detail->JournalCredit = ceil($total2);
        $detail->JournalTransactionID = NULL;
        $default = Coa::find($coa6->COAInternalID);
        $detail->ACC1InternalID = $default->ACC1InternalID;
        $detail->ACC2InternalID = $default->ACC2InternalID;
        $detail->ACC3InternalID = $default->ACC3InternalID;
        $detail->ACC4InternalID = $default->ACC4InternalID;
        $detail->ACC5InternalID = $default->ACC5InternalID;
        $detail->ACC6InternalID = $default->ACC6InternalID;
        $coa = Coa::getInternalID($default->ACC1InternalID, $default->ACC2InternalID, $default->ACC3InternalID, $default->ACC4InternalID, $default->ACC5InternalID, $default->ACC6InternalID);
        $detail->COAName = Coa::find($coa)->COAName;
        $detail->UserRecord = Auth::user()->UserID;
        $detail->UserModified = '0';
        $detail->save();

        //kalau ppn
        if ($tax == 1) {
            $detail = new JournalDetail();
            $detail->JournalInternalID = $header->InternalID;
            $detail->JournalIndex = 3;
            $detail->JournalNotes = $defaultPPN2->DefaultID;
            $detail->JournalDebetMU = 0;
            $detail->JournalCreditMU = floor($total3);
            $detail->CurrencyInternalID = $currency;
            $detail->CurrencyRate = 1;
            $detail->JournalDebet = 0;
            $detail->JournalCredit = floor($total3);
            $detail->JournalTransactionID = NULL;
            $detail->ACC1InternalID = $defaultPPN->ACC1InternalID;
            $detail->ACC2InternalID = $defaultPPN->ACC2InternalID;
            $detail->ACC3InternalID = $defaultPPN->ACC3InternalID;
            $detail->ACC4InternalID = $defaultPPN->ACC4InternalID;
            $detail->ACC5InternalID = $defaultPPN->ACC5InternalID;
            $detail->ACC6InternalID = $defaultPPN->ACC6InternalID;
            $coa = Coa::getInternalID($defaultPPN->ACC1InternalID, $defaultPPN->ACC2InternalID, $defaultPPN->ACC3InternalID, $defaultPPN->ACC4InternalID, $defaultPPN->ACC5InternalID, $defaultPPN->ACC6InternalID);
            $detail->COAName = Coa::find($coa)->COAName;
            $detail->UserRecord = Auth::user()->UserID;
            $detail->UserModified = '0';
            $detail->save();
        }

        //kalau ada selisih
        //selisih positif masuk kredit, minus masuk debit
        if ($selisih != 0) {
            $detail = new JournalDetail();
            $detail->JournalInternalID = $header->InternalID;
            if ($tax == 1) {
                $detail->JournalIndex = 4;
            } else {
                $detail->JournalIndex = 3;
            }
            $detail->JournalNotes = "Selisih Bayar";
            if ($selisih < 0) {
                $detail->JournalDebetMU = 0;
                $detail->JournalCreditMU = -1 * $selisih;
                $detail->JournalDebet = 0;
                $detail->JournalCredit = -1 * $selisih;
            } else {
                $detail->JournalDebetMU = $selisih;
                $detail->JournalCreditMU = 0;
                $detail->JournalDebet = $selisih;
                $detail->JournalCredit = 0;
            }
            $detail->CurrencyInternalID = $currency;
            $detail->CurrencyRate = 1;
            $detail->JournalTransactionID = NULL;
            $detail->ACC1InternalID = $akunselisih->ACC1InternalID;
            $detail->ACC2InternalID = $akunselisih->ACC2InternalID;
            $detail->ACC3InternalID = $akunselisih->ACC3InternalID;
            $detail->ACC4InternalID = $akunselisih->ACC4InternalID;
            $detail->ACC5InternalID = $akunselisih->ACC5InternalID;
            $detail->ACC6InternalID = $akunselisih->ACC6InternalID;
            $coa = Coa::getInternalID($akunselisih->ACC1InternalID, $akunselisih->ACC2InternalID, $akunselisih->ACC3InternalID, $akunselisih->ACC4InternalID, $akunselisih->ACC5InternalID, $akunselisih->ACC6InternalID);
            $detail->COAName = Coa::find($coa)->COAName;
            $detail->UserRecord = Auth::user()->UserID;
            $detail->UserModified = '0';
            $detail->save();
        }
    }

    public function insertJournalSupplier($memoNumber, $date, $slip, $tax, $total1, $total2, $total3, $coa6, $selisih, $typePayment) {
        $flag = Slip::find($slip)->Flag;
        $namePayment = "";
        if ($typePayment == 0) {
            $namePayment = "CBD";
        } else if ($typePayment == 1) {
            $namePayment = "Deposit";
        } else {
            $namePayment = "DP";
        }

        $header = new JournalHeader;
        //Purchase Tax nih ,Sales Tax = 3
        $defaultPPN = Default_s::getInternalCoa('Purchase Tax');
        $defaultPPN2 = Default_s::find(6);
        //coa : SELISIH BAYAR
        //akun asli
        $akunselisih = Coa::find(164);
//        $akunselisih = Coa::find(170);
        $datee = explode('-', $date);
        $cari = '';
        if ($flag == 0) {
            $cari = 'CO-' . $datee[0] . '-' . $datee[1];
            $header->JournalType = 'Cash Out';
        } else {
            $cari = 'BO-' . $datee[0] . '-' . $datee[1];
            $header->JournalType = 'Bank Out';
        }
        $header->JournalID = JournalHeader::getNextIDJournal($cari . '-');
        $header->SlipInternalID = $slip;
        $header->JournalDate = $date;
        $header->JournalFrom = Auth::user()->UserID;
        $header->Notes = $memoNumber . "-" . $coa6->ACC6Name;
        $department = Department::where('Default', '1')->first();
        $header->DepartmentInternalID = $department->InternalID;
        $header->TransactionID = $memoNumber;
        $header->ACC5InternalID = '0';
        $header->Lock = '0';
        $header->Check = '0';
        $header->Flag = '0';
        $header->UserRecord = Auth::user()->UserID;
        $header->CompanyInternalID = Auth::user()->Company->InternalID;
        $header->UserModified = '0';
        $header->Remark = "TopupSupplier-" . $namePayment;
        $header->save();

        $currency = Currency::where('Default', 1)->first()->InternalID;
        if ($total1 > 0) {
            //insert detail untuk slip
            //udah ditukar untuk supplier
            $detail = new JournalDetail();
            $detail->JournalInternalID = $header->InternalID;
            $detail->JournalIndex = 1;
            $detail->JournalNotes = 'Slip';
            $detail->JournalDebetMU = 0;
            $detail->JournalCreditMU = round($total1);
            $detail->CurrencyInternalID = $currency;
            $detail->CurrencyRate = 1;
            $detail->JournalDebet = 0;
            $detail->JournalCredit = round($total1);
            $detail->JournalTransactionID = NULL;
            $default = Slip::find($slip);
            $detail->ACC1InternalID = $default->ACC1InternalID;
            $detail->ACC2InternalID = $default->ACC2InternalID;
            $detail->ACC3InternalID = $default->ACC3InternalID;
            $detail->ACC4InternalID = $default->ACC4InternalID;
            $detail->ACC5InternalID = $default->ACC5InternalID;
            $detail->ACC6InternalID = $default->ACC6InternalID;
            $coa = Coa::getInternalID($default->ACC1InternalID, $default->ACC2InternalID, $default->ACC3InternalID, $default->ACC4InternalID, $default->ACC5InternalID, $default->ACC6InternalID);
            $detail->COAName = Coa::find($coa)->COAName;
            $detail->UserRecord = Auth::user()->UserID;
            $detail->UserModified = '0';
            $detail->save();
        }
        //insert detail untuk customer dp account
        //udah ditukar untuk supplier
        $detail = new JournalDetail();
        $detail->JournalInternalID = $header->InternalID;
        $detail->JournalIndex = 2;
        $detail->JournalNotes = 'Supplier Down Payment';
        $detail->JournalDebetMU = ceil($total2);
        $detail->JournalCreditMU = 0;
        $detail->CurrencyInternalID = $currency;
        $detail->CurrencyRate = 1;
        $detail->JournalDebet = ceil($total2);
        $detail->JournalCredit = 0;
        $detail->JournalTransactionID = NULL;
        $default = Coa::find($coa6->COAInternalID);
        $detail->ACC1InternalID = $default->ACC1InternalID;
        $detail->ACC2InternalID = $default->ACC2InternalID;
        $detail->ACC3InternalID = $default->ACC3InternalID;
        $detail->ACC4InternalID = $default->ACC4InternalID;
        $detail->ACC5InternalID = $default->ACC5InternalID;
        $detail->ACC6InternalID = $default->ACC6InternalID;
        $coa = Coa::getInternalID($default->ACC1InternalID, $default->ACC2InternalID, $default->ACC3InternalID, $default->ACC4InternalID, $default->ACC5InternalID, $default->ACC6InternalID);
        $detail->COAName = Coa::find($coa)->COAName;
        $detail->UserRecord = Auth::user()->UserID;
        $detail->UserModified = '0';
        $detail->save();

        //kalau ppn
        //udah ditukar untuk supplier
        if ($tax == 1) {
            $detail = new JournalDetail();
            $detail->JournalInternalID = $header->InternalID;
            $detail->JournalIndex = 3;
            $detail->JournalNotes = $defaultPPN2->DefaultID;
            $detail->JournalDebetMU = floor($total3);
            $detail->JournalCreditMU = 0;
            $detail->CurrencyInternalID = $currency;
            $detail->CurrencyRate = 1;
            $detail->JournalDebet = floor($total3);
            $detail->JournalCredit = 0;
            $detail->JournalTransactionID = NULL;
            $detail->ACC1InternalID = $defaultPPN->ACC1InternalID;
            $detail->ACC2InternalID = $defaultPPN->ACC2InternalID;
            $detail->ACC3InternalID = $defaultPPN->ACC3InternalID;
            $detail->ACC4InternalID = $defaultPPN->ACC4InternalID;
            $detail->ACC5InternalID = $defaultPPN->ACC5InternalID;
            $detail->ACC6InternalID = $defaultPPN->ACC6InternalID;
            $coa = Coa::getInternalID($defaultPPN->ACC1InternalID, $defaultPPN->ACC2InternalID, $defaultPPN->ACC3InternalID, $defaultPPN->ACC4InternalID, $defaultPPN->ACC5InternalID, $defaultPPN->ACC6InternalID);
            $detail->COAName = Coa::find($coa)->COAName;
            $detail->UserRecord = Auth::user()->UserID;
            $detail->UserModified = '0';
            $detail->save();
        }

        //kalau ada selisih
        //udah ditukar untuk supplier
        if ($selisih != 0) {
            $detail = new JournalDetail();
            $detail->JournalInternalID = $header->InternalID;
            if ($tax == 1) {
                $detail->JournalIndex = 4;
            } else {
                $detail->JournalIndex = 3;
            }
            $detail->JournalNotes = "Selisih Bayar";
            if ($selisih < 0) {
                $detail->JournalDebetMU = -1 * $selisih;
                $detail->JournalCreditMU = 0;
                $detail->JournalDebet = -1 * $selisih;
                $detail->JournalCredit = 0;
            } else {
                $detail->JournalDebetMU = 0;
                $detail->JournalCreditMU = $selisih;
                $detail->JournalDebet = 0;
                $detail->JournalCredit = $selisih;
            }
            $detail->CurrencyInternalID = $currency;
            $detail->CurrencyRate = 1;
            $detail->JournalTransactionID = NULL;
            $detail->ACC1InternalID = $akunselisih->ACC1InternalID;
            $detail->ACC2InternalID = $akunselisih->ACC2InternalID;
            $detail->ACC3InternalID = $akunselisih->ACC3InternalID;
            $detail->ACC4InternalID = $akunselisih->ACC4InternalID;
            $detail->ACC5InternalID = $akunselisih->ACC5InternalID;
            $detail->ACC6InternalID = $akunselisih->ACC6InternalID;
            $coa = Coa::getInternalID($akunselisih->ACC1InternalID, $akunselisih->ACC2InternalID, $akunselisih->ACC3InternalID, $akunselisih->ACC4InternalID, $akunselisih->ACC5InternalID, $akunselisih->ACC6InternalID);
            $detail->COAName = Coa::find($coa)->COAName;
            $detail->UserRecord = Auth::user()->UserID;
            $detail->UserModified = '0';
            $detail->save();
        }
    }

    public function exportExcel() {
        Excel::create('Master_Customertopup', function($excel) {
            $excel->sheet('Master_Customertopup', function($sheet) {
                $sheet->mergeCells('B1:K1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Master Customer Top Up");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Customer Name");
                $sheet->setCellValueByColumnAndRow(3, 2, "Date");
                $sheet->setCellValueByColumnAndRow(4, 2, "Customertopup ID");
                $sheet->setCellValueByColumnAndRow(5, 2, "Top Up Amount");
                $sheet->setCellValueByColumnAndRow(6, 2, "Grand Total (Tax)");
                $sheet->setCellValueByColumnAndRow(7, 2, "Payment Type");
                $sheet->setCellValueByColumnAndRow(8, 2, "Record");
                $sheet->setCellValueByColumnAndRow(9, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(10, 2, "Remark");
                $row = 3;

                $startT = explode('-', Input::get('sDate'));
                $endT = explode('-', Input::get('eDate'));
                $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
                $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];

                foreach (Customertopup::where('CompanyInternalID', Auth::user()->Company->InternalID)->whereBetween('TopUpDate', Array($start, $end))->get() as $data) {
                    $payment = '';
                    if ($data->PaymentType == 0) {
                        $payment = 'CBD';
                    } else if ($data->PaymentType == 1) {
                        $payment = 'Deposit';
                    } else {
                        $payment = 'Down Payment';
                    }
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, $data->customer->ACC6Name);
                    $sheet->setCellValueByColumnAndRow(3, $row, date("d-m-Y", strtotime($data->TopUpDate)));
                    $sheet->setCellValueByColumnAndRow(4, $row, $data->TopUpID);
                    $sheet->setCellValueByColumnAndRow(5, $row, number_format($data->TopUp, 2, '.', ','));
                    $sheet->setCellValueByColumnAndRow(6, $row, number_format($data->GrandTotalTopUp, 2, '.', ','));
                    $sheet->setCellValueByColumnAndRow(7, $row, $payment);
                    $sheet->setCellValueByColumnAndRow(8, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(9, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(10, $row, $data->Remark);
                    $row++;
                }

                if (Customertopup::where('CompanyInternalID', Auth::user()->Company->InternalID)->whereBetween('TopUpDate', Array($start, $end))->count() <= 0) {
                    $sheet->mergeCells('B3:K3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "No data available in table");

                    $sheet->cells('B3:K3', function($cells) {
                        $cells->setAlignment('center');
                    });

                    $sheet->setBorder('B3:K' . $row, 'thin');
                }

                $row--;
                $sheet->setBorder('B2:K' . $row, 'thin');
                $sheet->cells('B2:K2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:K' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('E3:E' . $row, function($cells) {
                    $cells->setAlignment('right');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
            });
        })->export('xls');
    }

    public function exportTax() {
        Excel::create('Tax_Customertopup', function($excel) {
            $excel->sheet('Tax_Customertopup', function($sheet) {
                $sheet->mergeCells('B1:N1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Tax Customer Top Up");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Tax Number");
                $sheet->setCellValueByColumnAndRow(3, 2, "Customer ID");
                $sheet->setCellValueByColumnAndRow(4, 2, "Customer");
                $sheet->setCellValueByColumnAndRow(5, 2, "Date");
                $sheet->setCellValueByColumnAndRow(6, 2, "Top Up Amount");
                $sheet->setCellValueByColumnAndRow(7, 2, "Grand Total (Tax)");
                $sheet->setCellValueByColumnAndRow(8, 2, "Payment Type");
                $sheet->setCellValueByColumnAndRow(9, 2, "Account");
                $sheet->setCellValueByColumnAndRow(10, 2, "Sales Order ID");
                $sheet->setCellValueByColumnAndRow(11, 2, "Record");
                $sheet->setCellValueByColumnAndRow(12, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(13, 2, "Remark");
                $row = 3;

                $startT = explode('-', Input::get('sDate'));
                $endT = explode('-', Input::get('eDate'));
                $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
                $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];

//                $custopup = Customertopup::groupBy('Tax')->orderBy('T2opUpDate', 'DESC')->orderBy('InternalID', 'DESC')
//                        ->where('CompanyInternalID', Auth::user()->Company->InternalID)
//                        ->whereBetween('TopUpDate', Array($start, $end))
//                        ->where('Tax', '!=', 'null')
//                        ->get();

                $custopup = DB::select(DB::raw("SELECT table1.* FROM (SELECT distinct * from `m_customertopup` where `CompanyInternalID` = " . Auth::user()->Company->InternalID
                                        . " and `TopUpDate` between '" . $start . "' and '" . $end . "' and `Tax` != 'null' order by `TopUpDate` desc, `InternalID` desc) as table1 GROUP BY Tax"));

                foreach ($custopup as $data) {
                    $payment = '';
                    if ($data->PaymentType == 0) {
                        $payment = 'CBD';
                    } else if ($data->PaymentType == 1) {
                        $payment = 'Deposit';
                    } else {
                        $payment = 'Down Payment';
                    }
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, $data->Tax);
                    $sheet->setCellValueByColumnAndRow(3, $row, $data->TopUpID);
                    $sheet->setCellValueByColumnAndRow(4, $row, Coa6::find($data->ACC6InternalID)->ACC6Name);
                    $sheet->setCellValueByColumnAndRow(5, $row, date("d-m-Y", strtotime($data->TopUpDate)));
                    $sheet->setCellValueByColumnAndRow(6, $row, number_format($data->TopUp, 2, '.', ','));
                    $sheet->setCellValueByColumnAndRow(7, $row, number_format($data->GrandTotalTopUp, 2, '.', ','));
                    $sheet->setCellValueByColumnAndRow(8, $row, $payment);
                    $sheet->setCellValueByColumnAndRow(9, $row, Slip::find($data->SlipInternalID)->SlipName);
                    $sheet->setCellValueByColumnAndRow(10, $row, SalesOrderHeader::find($data->SalesOrderInternalID)->SalesOrderID);
                    $sheet->setCellValueByColumnAndRow(11, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(12, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(13, $row, $data->Remark);
                    $row++;
                }

                if (count($custopup) <= 0) {
                    $sheet->mergeCells('B3:N3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "No data available in table");

                    $sheet->cells('B3:N3', function($cells) {
                        $cells->setAlignment('center');
                    });

                    $sheet->setBorder('B3:N' . $row, 'thin');
                }

                $row--;
//                $sheet->setBorder('B2:K' . $row, 'thin');
//                $sheet->cells('B2:K2', function($cells) {
//                    $cells->setBackground('#eaf6f7');
//                    $cells->setValignment('middle');
//                    $cells->setAlignment('center');
//                });
//                $sheet->cells('B1', function($cells) {
//                    $cells->setValignment('middle');
//                    $cells->setAlignment('center');
//                    $cells->setFontWeight('bold');
//                    $cells->setFontSize('16');
//                });
//                $sheet->cells('B3:K' . $row, function($cells) {
//                    $cells->setAlignment('left');
//                    $cells->setValignment('middle');
//                });
//                $sheet->cells('E3:E' . $row, function($cells) {
//                    $cells->setAlignment('right');
//                });
//                $sheet->cells('B3:B' . $row, function($cells) {
//                    $cells->setAlignment('center');
//                });
            });
        })->export('xls');
    }

    public function outstandingDP() {
        Excel::create('Outstanding_DP', function($excel) {
            $excel->sheet('Outstanding_DP', function($sheet) {
                $sheet->mergeCells('B1:H1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Outstanding DP");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Sales Order ID");
                $sheet->setCellValueByColumnAndRow(3, 2, "Date");
                $sheet->setCellValueByColumnAndRow(4, 2, "Expired Date");
                $sheet->setCellValueByColumnAndRow(5, 2, "Down Payment");
                $sheet->setCellValueByColumnAndRow(6, 2, "Use");
                $sheet->setCellValueByColumnAndRow(7, 2, "Remain");
                $row = 3;
                $query = Customertopup::outstandingDP();
                $sumGrandTotal = 0;
                if (count($query) > 0) {
                    foreach ($query as $data) {
                        $DP = $data->GrandTotal * $data->DownPayment / 100;
                        $use = $DP - $data->total;
                        $grandTotal = $data->total;
                        $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                        $sheet->setCellValueByColumnAndRow(2, $row, $data->SalesOrderID);
                        $sheet->setCellValueByColumnAndRow(3, $row, date("d-M-Y", strtotime($data->SalesOrderDate)));
                        $sheet->setCellValueByColumnAndRow(4, $row, date("d-M-Y", strtotime($data->ExpiredDate)));
                        $sheet->setCellValueByColumnAndRow(5, $row, number_format($DP, '2', '.', ','));
                        $sheet->setCellValueByColumnAndRow(6, $row, number_format($use, '2', '.', ','));
                        $sheet->setCellValueByColumnAndRow(7, $row, number_format($data->total, '2', '.', ','));
                        $sumGrandTotal += $grandTotal;
                        $row++;
                    }
                    $row++;
                    $sheet->mergeCells('B' . $row . ':G' . $row);
                    $sheet->setCellValueByColumnAndRow(1, $row, "Total");
                    $sheet->setCellValueByColumnAndRow(7, $row, number_format($sumGrandTotal, '2', '.', ','));
                } else {
                    $sheet->mergeCells('B' . $row . ':G' . $row);
                    $sheet->setCellValueByColumnAndRow(1, $row, "There is no outstanding Down Payment.");
                }

                $sheet->setBorder('B2:H' . $row, 'thin');
                $sheet->cells('B2:H2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:H' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('F3:H' . $row, function($cells) {
                    $cells->setAlignment('right');
                });
            });
        })->export('xls');
    }

    public function outstandingDPPDF() {
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Outstanding Down Payment</h5>';
        $html .= '<table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales Order ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Expired Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Down Payment</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Use</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Remain</th>
                                        </tr>
                                    </thead>
                                    <tbody>';

        $query = Customertopup::outstandingDP();
        $sumGrandTotal = 0;
        if (count($query) > 0) {
            foreach ($query as $data) {
                $DP = $data->GrandTotal * $data->DownPayment / 100;
                $use = $DP - $data->total;
                $grandTotal = $data->total;
                $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->SalesOrderID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->SalesOrderDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->ExpiredDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($DP, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($use, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->total, '2', '.', ',') . '</td>
                            </tr>';
                $sumGrandTotal += $grandTotal;
            }
            $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif; margin: 5px !important;" colspan="6"><hr></td>
                            </tr>';
            $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="80%" colspan="5">Total : </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . number_format($sumGrandTotal, '2', '.', ',') . '</td>
                            </tr>';
        } else {
            $html .= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: center">There is no outstanding Down Payment.</td>
                        </tr>';
        }

        $html .= '</tbody>
            </table>';

        $html .= '</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('outstanding DP');
    }

    public function getTaxCustomer() {
        $date = explode("-", Input::get('date'));
        $dateNow = $date[2] . "-" . $date[1] . "-" . $date[0];
        $tax = Tax_s::resultTax($dateNow);
        if (count($tax) == 0) {
            $taxResult = "0" . "_habis_" . "0";
            return $taxResult;
        } else {

            $start = str_replace('-', '.', $tax[0]->StartRange);
            $end = str_replace('-', '.', $tax[0]->EndRange);
            $startExplode = explode('.', $start);
            $endExplode = explode('.', $end);

            $purchase = SalesAddHeader::where('TaxNumber', 'LIKE', '%.' . $startExplode[1] . '-' . $startExplode[2] . '.%')->orderBy(DB::raw('SUBSTR(TaxNumber,5,15)'), 'desc')->first();
            $customerTopUp = Customertopup::where('Tax', 'LIKE', '%.' . $startExplode[1] . '-' . $startExplode[2] . '.%')->orderBy(DB::raw('SUBSTR(Tax,5,15)'), 'desc')->first();
            $number = 0;
            if (count($purchase) > 0 || count($customerTopUp) > 0) {
                if (count($purchase) > 0) {
                    $tamp = str_replace('-', '.', $purchase->TaxNumber);
                    $tampExplode = explode('.', $tamp);
                    $number = $tampExplode[3] + 1;
                }
                if (count($customerTopUp) > 0) {
                    $tamp2 = str_replace('-', '.', $customerTopUp->Tax);
                    $tampExplode2 = explode('.', $tamp2);
                    if ($tampExplode2[3] + 1 > $number) {
                        $number = $tampExplode2[3] + 1;
                    }
                }
            } else {
                $number = $startExplode[3];
            }

            $taxResult = $startExplode[0] . '.' . $startExplode[1] . '-' . $startExplode[2] . '.' . str_pad($number, 8, '0', STR_PAD_LEFT);

            $tampCount = 0;
            $lasttax1 = $tax[0]->lasttax1 == NULL ? 0 : $tax[0]->lasttax1;
            $lasttax2 = $tax[0]->lasttax2 == NULL ? 0 : $tax[0]->lasttax2;
            if ($lasttax1 >= $lasttax2) {
                if ($lasttax1 == 0) {
                    $tampCount = $endExplode[3] - $startExplode[3] + 1;
                } else {
                    $tampCount = $endExplode[3] - explode('.', $lasttax1)[1];
                }
            } else {
                $tampCount = $endExplode[3] - explode('.', $lasttax2)[1];
            }
            $persen = ($tampCount / $tax[0]->total) * 100;

            if (($number) > (int) $endExplode[3]) {
                $taxResult = $tax[0]->EndRange . "_Tax Reached Limit_" . $persen;
            } else {
                $taxResult = $taxResult . "_sukses_" . $persen;
            }
            return $taxResult;
        }
    }

    public function getSOAmount() {
        $id = Input::get('id');
        $amount = TopUp::where('SalesOrderInternalID', $id)->sum('GrandTotalTopUp');
        $salesOrder = SalesOrderHeader::find($id)->GrandTotal;
        $price = $salesOrder - $amount;
        return number_format($price, 2, '.', ',');
    }

    public function getSOAmountDp() {
        $id = Input::get('id');
        //sudah dibayar
        $amount = TopUp::where('SalesOrderInternalID', $id)->sum('GrandTotalTopUp');
        $salesOrder = SalesOrderHeader::find($id);
        //total dp
        if ($salesOrder->DownPayment > 0) {
            $dp = $salesOrder->DownPayment / 100;
            $grandTotal = $dp * $salesOrder->GrandTotal;
        } else {
            $grandTotal = $salesOrder->DPValue;
        }
        //
        //sisa dp
        $price = $grandTotal - $amount;
        return number_format($price, 2, '.', ',');
    }

    public function getSOAmountExcept() {
        $id = Input::get('id');
        $topup = Input::get('topup');
        $amount = TopUp::where('SalesOrderInternalID', $id)->where('InternalID', '!=', $topup)->sum('GrandTotalTopUp');
        $salesOrder = SalesOrderHeader::find($id)->GrandTotal;
        $price = $salesOrder - $amount;
        return number_format($price, 2, '.', ',');
    }

    public function getSOAmountDpExcept() {
        $id = Input::get('id');
        $topup = Input::get('topup');
        $amount = TopUp::where('SalesOrderInternalID', $id)->where('InternalID', '!=', $topup)->sum('GrandTotalTopUp');
        $salesOrder = SalesOrderHeader::find($id);
        //total dp
        if ($salesOrder->DownPayment > 0) {
            $dp = $salesOrder->DownPayment / 100;
            $grandTotal = $dp * $salesOrder->GrandTotal;
        } else {
            $grandTotal = $salesOrder->DPValue;
        }
        //
        $price = $grandTotal - $amount;
        return number_format($price, 2, '.', ',');
    }

    public function getPOAmount() {
        $id = Input::get('id');
        $amount = TopUp::where('PurchaseOrderInternalID', $id)->sum('GrandTotalTopUp');
        $salesOrder = PurchaseOrderHeader::find($id)->GrandTotal;
        $price = $salesOrder - $amount;
        return number_format($price, 2, '.', ',');
    }

    public function getPOAmountDp() {
        $id = Input::get('id');
        //sudah dibayar
        $amount = TopUp::where('PurchaseOrderInternalID', $id)->sum('GrandTotalTopUp');
        $salesOrder = PurchaseOrderHeader::find($id);
        //total dp
        if ($salesOrder->DownPayment > 0) {
            $dp = $salesOrder->DownPayment / 100;
            $grandTotal = $dp * $salesOrder->GrandTotal;
        } else {
            $grandTotal = $salesOrder->DPValue;
        }
        //
        //sisa dp
        $price = $grandTotal - $amount;
        return number_format($price, 2, '.', ',');
    }

    public function getPOAmountExcept() {
        $id = Input::get('id');
        $topup = Input::get('topup');
        $amount = TopUp::where('PurchaseOrderInternalID', $id)->where('InternalID', '!=', $topup)->sum('GrandTotalTopUp');
        $salesOrder = PurchaseOrderHeader::find($id)->GrandTotal;
        $price = $salesOrder - $amount;
        return number_format($price, 2, '.', ',');
    }

    public function getPOAmountDpExcept() {
        $id = Input::get('id');
        $topup = Input::get('topup');
        $amount = TopUp::where('PurchaseInternalID', $id)->where('InternalID', '!=', $topup)->sum('GrandTotalTopUp');
        $salesOrder = PurchaseOrderHeader::find($id);
        //total dp
        if ($salesOrder->DownPayment > 0) {
            $dp = $salesOrder->DownPayment / 100;
            $grandTotal = $dp * $salesOrder->GrandTotal;
        } else {
            $grandTotal = $salesOrder->DPValue;
        }
        //
        $price = $grandTotal - $amount;
        return number_format($price, 2, '.', ',');
    }

    public function getChangeCbd() {
        $id = Input::get('so');
        $so = SalesOrderHeader::find($id);
        $so->is_CBD_Finished = 0;
        $so->save();
        return 1;
    }

    public function getIDTop() {
        $month = Date('m');
        $year = Date('y');
        $id = "TU." . $year . $month . ".";
        $top = Customertopup::where('CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('TopUpID', 'LIKE', $id . '%')
                ->orderBy('InternalID', '=', 'desc')
                ->first();
        if (count($top) > 0) {
            $top = $top->TopUpID;
        } else {
            $top = 0000;
        }
        $idSecond = substr($top, -4) + 1;
        $idSecond = str_pad($idSecond, 4, '0', STR_PAD_LEFT);
        $idTop = $id . $idSecond;

        return $idTop;
    }

    public function topupSalesOrder() {
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $totalSO = 0;
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after {
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div>
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Outstanding TopUp Sales Order</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span>';

        $html .= '<table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales Order ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Nama Customer</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Tanggal Sales Order</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Jenis Pembayaran</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Grand Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
        $sumGrandTotal = 0;
        foreach ((DB::select("SELECT * FROM t_salesorder_header WHERE NOT EXISTS (SELECT * FROM m_customertopup
          WHERE t_salesorder_header.InternalID = m_customertopup.SalesOrderInternalID) AND t_salesorder_header.isCash IN (2,4)
                AND t_salesorder_header.Void = 'Active'
                AND t_salesorder_header.SalesOrderDate BETWEEN '$start' AND '$end'
                AND t_salesorder_header.CompanyInternalID = '" . Auth::user()->Company->InternalID . " '
                "))
        as $data) {
            $namaCust = DB::table('m_coa6')->where('InternalID', $data->ACC6InternalID)->pluck('ACC6Name');
            if ($data->isCash == 2) {
                $namaPembayaran = 'CBD';
            } else {
                $namaPembayaran = 'Down Payment';
            }
            $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->SalesOrderID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $namaCust . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->SalesOrderDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $namaPembayaran . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->GrandTotal, '2', '.', ',') . '</td>
                            </tr>';
        }
        $html .= '</tbody>
            </table>';

        $html .= '</tbody>
            </table>';

        $html .= '</div>

                            <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span>
                    </div>
                </body>
            </html>';
//        return PDF ::load($html, 'A4', 'portrait')->download('sales_order_summary');
        return $html;
    }

    public function topupSalesOrderExcel() {
        Excel::create('TopUp_SalesOrder', function($excel) {
            $excel->sheet('TopUp_SalesOrder', function($sheet) {
                $startT = explode('-', Input::get('sDate'));
                $endT = explode('-', Input::get('eDate'));
                $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
                $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
                $totalSO = 0;
                $sheet->mergeCells('B1:F1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Sales Order Outstanding Top Up Report");
                $sheet->mergeCells('C2:F2');
                $sheet->setCellValueByColumnAndRow(1, 2, "Period :");
                $sheet->setCellValueByColumnAndRow(2, 2, date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))));
                $row = 3;
                $sheet->cells('B' . $row . ':F' . $row, function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('B' . $row . ':F' . $row, 'thin');
                $sheet->setCellValueByColumnAndRow(1, $row, "Sales Order ID");
                $sheet->setCellValueByColumnAndRow(2, $row, "Nama Customer");
                $sheet->setCellValueByColumnAndRow(3, $row, "Tanggal Sales Order");
                $sheet->setCellValueByColumnAndRow(4, $row, "Jenis Pembayaran");
                $sheet->setCellValueByColumnAndRow(5, $row, "Grand Total");
                $row++;
                $sumGrandTotal = 0;
                foreach ((DB::select("SELECT * FROM t_salesorder_header WHERE NOT EXISTS (SELECT * FROM m_customertopup
                  WHERE t_salesorder_header.InternalID = m_customertopup.SalesOrderInternalID) AND t_salesorder_header.isCash IN (2,4)
                        AND t_salesorder_header.Void = 'Active'
                        AND t_salesorder_header.SalesOrderDate BETWEEN '$start' AND '$end'
                        AND t_salesorder_header.CompanyInternalID = '" . Auth::user()->Company->InternalID . " '
                        "))
                as $data) {
                    $namaCust = DB::table('m_coa6')->where('InternalID', $data->ACC6InternalID)->pluck('ACC6Name');
                    if ($data->isCash == 2) {
                        $namaPembayaran = 'CBD';
                    } else {
                        $namaPembayaran = 'Down Payment';
                    }

                    $sheet->setCellValueByColumnAndRow(1, $row, $data->SalesOrderID);
                    $sheet->setCellValueByColumnAndRow(2, $row, $namaCust);
                    $sheet->setCellValueByColumnAndRow(3, $row, date("d-M-Y", strtotime($data->SalesOrderDate)));
                    $sheet->setCellValueByColumnAndRow(4, $row, $namaPembayaran);
                    $sheet->setCellValueByColumnAndRow(5, $row, number_format($data->GrandTotal, '2', '.', ','));
                    $sheet->setBorder('B' . $row . ':F' . $row, 'thin');
                    $row++;
                }

                $row++;

                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:F' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('left');
                });
            });
        })->export('xls');
    }

    public function topupCSV($id) {
        Session::flash('idCSV', $id);
        Excel::create('Tax_Report_TopUp', function($excel) {
            $excel->sheet('Tax_Report_TopUp', function($sheet) {
                //baris pertama
                $id = Session::get('idCSV');
                $sheet->setCellValueByColumnAndRow(0, 1, "FK");
                $sheet->setCellValueByColumnAndRow(1, 1, "KD_JENIS_TRANSAKSI");
                $sheet->setCellValueByColumnAndRow(2, 1, "FG_PENGGANTI");
                $sheet->setCellValueByColumnAndRow(3, 1, "NOMOR_FAKTUR");
                $sheet->setCellValueByColumnAndRow(4, 1, "MASA_PAJAK");
                $sheet->setCellValueByColumnAndRow(5, 1, "TAHUN_PAJAK");
                $sheet->setCellValueByColumnAndRow(6, 1, "TANGGAL_FAKTUR");
                $sheet->setCellValueByColumnAndRow(7, 1, "NPWP");
                $sheet->setCellValueByColumnAndRow(8, 1, "NAMA");
                $sheet->setCellValueByColumnAndRow(9, 1, "ALAMAT_LENGKAP");
                $sheet->setCellValueByColumnAndRow(10, 1, "JUMLAH_DPP");
                $sheet->setCellValueByColumnAndRow(11, 1, "JUMLAH_PPN");
                $sheet->setCellValueByColumnAndRow(12, 1, "JUMLAH_PPNBM");
                $sheet->setCellValueByColumnAndRow(13, 1, "ID_KETERANGAN_TAMBAHAN");
                $sheet->setCellValueByColumnAndRow(14, 1, "FG_UANG_MUKA");
                $sheet->setCellValueByColumnAndRow(15, 1, "UANG_MUKA_DPP");
                $sheet->setCellValueByColumnAndRow(16, 1, "UANG_MUKA_PPN");
                $sheet->setCellValueByColumnAndRow(17, 1, "UANG_MUKA_PPNBM");
                $sheet->setCellValueByColumnAndRow(18, 1, "REFERENSI");
                //baris kedua
                $sheet->setCellValueByColumnAndRow(0, 2, "LT");
                $sheet->setCellValueByColumnAndRow(1, 2, "NPWP");
                $sheet->setCellValueByColumnAndRow(2, 2, "NAMA");
                $sheet->setCellValueByColumnAndRow(3, 2, "JALAN");
                $sheet->setCellValueByColumnAndRow(4, 2, "BLOK");
                $sheet->setCellValueByColumnAndRow(5, 2, "NOMOR");
                $sheet->setCellValueByColumnAndRow(6, 2, "RT");
                $sheet->setCellValueByColumnAndRow(7, 2, "RW");
                $sheet->setCellValueByColumnAndRow(8, 2, "KECAMATAN");
                $sheet->setCellValueByColumnAndRow(9, 2, "KELURAHAN");
                $sheet->setCellValueByColumnAndRow(10, 2, "KABUPATEN");
                $sheet->setCellValueByColumnAndRow(11, 2, "PROPINSI");
                $sheet->setCellValueByColumnAndRow(12, 2, "KODE_POS");
                $sheet->setCellValueByColumnAndRow(13, 2, "NOMOR_TELEPON");
                //baris ketiga
                $sheet->setCellValueByColumnAndRow(0, 3, "OF");
                $sheet->setCellValueByColumnAndRow(1, 3, "KODE_OBJEK");
                $sheet->setCellValueByColumnAndRow(2, 3, "NAMA");
                $sheet->setCellValueByColumnAndRow(3, 3, "HARGA_SATUAN");
                $sheet->setCellValueByColumnAndRow(4, 3, "JUMLAH_BARANG");
                $sheet->setCellValueByColumnAndRow(5, 3, "HARGA_TOTAL");
                $sheet->setCellValueByColumnAndRow(6, 3, "DISKON");
                $sheet->setCellValueByColumnAndRow(7, 3, "DPP");
                $sheet->setCellValueByColumnAndRow(8, 3, "PPN");
                $sheet->setCellValueByColumnAndRow(9, 3, "TARIF_PPNBM");
                $sheet->setCellValueByColumnAndRow(10, 3, "PPNBM");
                //isi baris pertama
                $row = 4;
//                $start = date('Y-m-d', strtotime(Input::get('startDate')));
//                $end = date('Y-m-d', strtotime(Input::get('endDate')));
                $header = TopUp::where('TopUpID', $id)->first();
                $header->Flag = 1;
                $header->save();

                $sheet->setCellValueByColumnAndRow(0, $row, "FK");
                $sheet->setCellValueExplicit('B' . $row, '0' . 1, \PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValueByColumnAndRow(2, $row, 0);
                //nomorPajak
                $nomorPajak = str_replace('.', '', $header->Tax);
                $nomorPajak = str_replace('-', '', $nomorPajak);
                $nomorPajak = substr($nomorPajak, 4);
                if ($nomorPajak == '' || $nomorPajak == 0) {
                    $nomorPajak = "000000000000000";
                    $sheet->setCellValueExplicit('D' . $row, $nomorPajak, \PHPExcel_Cell_DataType::TYPE_STRING);
                } else {
                    $sheet->setCellValueExplicit('D' . $row, '0' . $nomorPajak, \PHPExcel_Cell_DataType::TYPE_STRING);
                }
                $topDate = explode('-', $header->TopUpDate);
                $topMonth = $topDate[1];
                $topYear = $topDate[0];

                $sheet->setCellValueByColumnAndRow(4, $row, $topMonth);
                $sheet->setCellValueByColumnAndRow(5, $row, $topYear);
                $sheet->setCellValueByColumnAndRow(6, $row, date('d/m/Y', strtotime($header->TopUpDate)));


                $pajakCustomer = str_replace('.', '', $header->customer->TaxID);
                $pajakCustomer = str_replace('-', '', $pajakCustomer);
                if ($pajakCustomer == '' || $pajakCustomer == 0) {
                    $pajakCustomer = "000000000000000";
                }
                $sheet->setCellValueExplicit('H' . $row, $pajakCustomer, \PHPExcel_Cell_DataType::TYPE_STRING);
                if ($pajakCustomer != "000000000000000") {
                    $sheet->setCellValueByColumnAndRow(8, $row, strtoupper($header->customer->ACC6Name));
                } else {
                    $accname = explode(",", $header->customer->ACC6Name);
                    $nameAcc = "";
                    if (isset($accname[1])) {
                        $nameAcc = strtoupper($accname[1]) . ". " . strtoupper($accname[0]);
                    } else {
                        $nameAcc = strtoupper($accname[0]);
                    }
//                        $sheet->setCellValueByColumnAndRow(8, $row, $header->Coa6->ACC6Name);
                    $sheet->setCellValueByColumnAndRow(8, $row, $nameAcc);
                }

                $sheet->setCellValueByColumnAndRow(9, $row, $header->customer->Address);
                //total
//                $total = $header->GrandTotalTopUp * 10 / 11;
                $total = $header->TopUp;
                $sheet->setCellValueByColumnAndRow(10, $row, round($total));
                $sheet->setCellValueByColumnAndRow(11, $row, floor((round($total) * 0.1)));
                $sheet->setCellValueByColumnAndRow(12, $row, 0);
                $sheet->setCellValueByColumnAndRow(13, $row, '');


                $downpayment = $header->TopUp;
//                $downpayment = $header->GrandTotalTopUp * 10 / 11;
                $sheet->setCellValueByColumnAndRow(14, $row, 1);
                $sheet->setCellValueByColumnAndRow(15, $row, round($downpayment));
                $sheet->setCellValueByColumnAndRow(16, $row, floor(round($downpayment) * 0.1));
                $sheet->setCellValueByColumnAndRow(17, $row, "0");
                $sheet->setCellValueByColumnAndRow(18, $row, $header->TopUpID);

                $row++;

                $sheet->setCellValueByColumnAndRow(0, $row, "OF");
                $sheet->setCellValueByColumnAndRow(1, $row, "01");
                $sheet->setCellValueByColumnAndRow(2, $row, "Uang Muka atas Penjualan");
                $sheet->setCellValueByColumnAndRow(3, $row, round($downpayment));
                $sheet->setCellValueByColumnAndRow(4, $row, 1);
                $sheet->setCellValueByColumnAndRow(5, $row, 1 * round($downpayment));
                $sheet->setCellValueByColumnAndRow(6, $row, 0);
                $sheet->setCellValueByColumnAndRow(7, $row, round($downpayment));
                $sheet->setCellValueByColumnAndRow(8, $row, floor(round($downpayment) * 0.1));
                $sheet->setCellValueByColumnAndRow(9, $row, 0);
                $sheet->setCellValueByColumnAndRow(10, $row, 0);
                $row++;

                $sheet->setColumnFormat(array(
                    'B' => \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
                    'D' => \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
                    'H' => \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
                ));
            });
        })->export('csv');
    }

    public function topupCsvBanyak() {
        Excel::create('Tax_Report_TopUp', function($excel) {
            $excel->sheet('Tax_Report_TopUp', function($sheet) {
                //baris pertama
                $sheet->setCellValueByColumnAndRow(0, 1, "FK");
                $sheet->setCellValueByColumnAndRow(1, 1, "KD_JENIS_TRANSAKSI");
                $sheet->setCellValueByColumnAndRow(2, 1, "FG_PENGGANTI");
                $sheet->setCellValueByColumnAndRow(3, 1, "NOMOR_FAKTUR");
                $sheet->setCellValueByColumnAndRow(4, 1, "MASA_PAJAK");
                $sheet->setCellValueByColumnAndRow(5, 1, "TAHUN_PAJAK");
                $sheet->setCellValueByColumnAndRow(6, 1, "TANGGAL_FAKTUR");
                $sheet->setCellValueByColumnAndRow(7, 1, "NPWP");
                $sheet->setCellValueByColumnAndRow(8, 1, "NAMA");
                $sheet->setCellValueByColumnAndRow(9, 1, "ALAMAT_LENGKAP");
                $sheet->setCellValueByColumnAndRow(10, 1, "JUMLAH_DPP");
                $sheet->setCellValueByColumnAndRow(11, 1, "JUMLAH_PPN");
                $sheet->setCellValueByColumnAndRow(12, 1, "JUMLAH_PPNBM");
                $sheet->setCellValueByColumnAndRow(13, 1, "ID_KETERANGAN_TAMBAHAN");
                $sheet->setCellValueByColumnAndRow(14, 1, "FG_UANG_MUKA");
                $sheet->setCellValueByColumnAndRow(15, 1, "UANG_MUKA_DPP");
                $sheet->setCellValueByColumnAndRow(16, 1, "UANG_MUKA_PPN");
                $sheet->setCellValueByColumnAndRow(17, 1, "UANG_MUKA_PPNBM");
                $sheet->setCellValueByColumnAndRow(18, 1, "REFERENSI");
                //baris kedua
                $sheet->setCellValueByColumnAndRow(0, 2, "LT");
                $sheet->setCellValueByColumnAndRow(1, 2, "NPWP");
                $sheet->setCellValueByColumnAndRow(2, 2, "NAMA");
                $sheet->setCellValueByColumnAndRow(3, 2, "JALAN");
                $sheet->setCellValueByColumnAndRow(4, 2, "BLOK");
                $sheet->setCellValueByColumnAndRow(5, 2, "NOMOR");
                $sheet->setCellValueByColumnAndRow(6, 2, "RT");
                $sheet->setCellValueByColumnAndRow(7, 2, "RW");
                $sheet->setCellValueByColumnAndRow(8, 2, "KECAMATAN");
                $sheet->setCellValueByColumnAndRow(9, 2, "KELURAHAN");
                $sheet->setCellValueByColumnAndRow(10, 2, "KABUPATEN");
                $sheet->setCellValueByColumnAndRow(11, 2, "PROPINSI");
                $sheet->setCellValueByColumnAndRow(12, 2, "KODE_POS");
                $sheet->setCellValueByColumnAndRow(13, 2, "NOMOR_TELEPON");
                //baris ketiga
                $sheet->setCellValueByColumnAndRow(0, 3, "OF");
                $sheet->setCellValueByColumnAndRow(1, 3, "KODE_OBJEK");
                $sheet->setCellValueByColumnAndRow(2, 3, "NAMA");
                $sheet->setCellValueByColumnAndRow(3, 3, "HARGA_SATUAN");
                $sheet->setCellValueByColumnAndRow(4, 3, "JUMLAH_BARANG");
                $sheet->setCellValueByColumnAndRow(5, 3, "HARGA_TOTAL");
                $sheet->setCellValueByColumnAndRow(6, 3, "DISKON");
                $sheet->setCellValueByColumnAndRow(7, 3, "DPP");
                $sheet->setCellValueByColumnAndRow(8, 3, "PPN");
                $sheet->setCellValueByColumnAndRow(9, 3, "TARIF_PPNBM");
                $sheet->setCellValueByColumnAndRow(10, 3, "PPNBM");
                //isi baris pertama
                $row = 4;
                $start = date('Y-m-d', strtotime(Input::get('startDate')));
                $end = date('Y-m-d', strtotime(Input::get('endDate')));
                $salesorder = TopUp::where('VAT', 1)
                        ->where('is_automatic', 0)
                        ->where('Void', "Active")
                        ->whereBetween('TopUpDate', array($start, $end))
                        ->get();
                foreach ($salesorder as $header) {
                    $update = DB::table('m_customertopup')->where('InternalID', $header->InternalID)->update(array('Flag' => 1));
                    $sheet->setCellValueByColumnAndRow(0, $row, "FK");
                    $sheet->setCellValueExplicit('B' . $row, '0' . 1, \PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValueByColumnAndRow(2, $row, 0);
                    //nomorPajak
                    $nomorPajak = str_replace('.', '', $header->Tax);
                    $nomorPajak = str_replace('-', '', $nomorPajak);
                    $nomorPajak = substr($nomorPajak, 4);
                    if ($nomorPajak == '' || $nomorPajak == 0) {
                        $nomorPajak = "000000000000000";
                        $sheet->setCellValueExplicit('D' . $row, $nomorPajak, \PHPExcel_Cell_DataType::TYPE_STRING);
                    } else {
                        $sheet->setCellValueExplicit('D' . $row, '0' . $nomorPajak, \PHPExcel_Cell_DataType::TYPE_STRING);
                    }
                    $topDate = explode('-', $header->TopUpDate);
                    $topMonth = $topDate[1];
                    $topYear = $topDate[0];

                    $sheet->setCellValueByColumnAndRow(4, $row, $topMonth);
                    $sheet->setCellValueByColumnAndRow(5, $row, $topYear);
                    $sheet->setCellValueByColumnAndRow(6, $row, date('d/m/Y', strtotime($header->TopUpDate)));

                    //nomorPajak
                    if ($header->CustomerDetailInternalID != NULL) {
                        $custDetail = CustomerDetail::find($header->CustomerDetailInternalID);
                        if ($custDetail->TaxCentralization == 0) {
                            $pajakCustomer = str_replace('.', '', $custDetail->TaxID);
                        } else {
                            $pajakCustomer = str_replace('.', '', $header->customer->TaxID);
                        }
                    } else {
                        $pajakCustomer = str_replace('.', '', $header->customer->TaxID);
                    }
                    $pajakCustomer = str_replace('-', '', $pajakCustomer);
                    if ($pajakCustomer == '' || $pajakCustomer == 0) {
                        $pajakCustomer = "000000000000000";
                    }
                    $sheet->setCellValueExplicit('H' . $row, $pajakCustomer, \PHPExcel_Cell_DataType::TYPE_STRING);
                    if ($pajakCustomer != "000000000000000") {
                        if ($header->customer->NPWPACC6Name != NULL || $header->customer->NPWPACC6Name != "NULL" || $header->customer->NPWPACC6Name != "") {
                            $sheet->setCellValueByColumnAndRow(8, $row, strtoupper($header->customer->NPWPACC6Name));
                        } else {
                            $sheet->setCellValueByColumnAndRow(8, $row, strtoupper($header->customer->ACC6Name));
                        }
                    } else {
                        $accname = explode(",", $header->customer->ACC6Name);
                        $nameAcc = "";
                        if (isset($accname[1])) {
                            $nameAcc = strtoupper($accname[1]) . ". " . strtoupper($accname[0]);
                        } else {
                            $nameAcc = strtoupper($accname[0]);
                        }
//                        $sheet->setCellValueByColumnAndRow(8, $row, $header->Coa6->ACC6Name);
                        $sheet->setCellValueByColumnAndRow(8, $row, $nameAcc);
                    }

                    if (is_null($header->CustomerDetailInternalID) == false) {
                        $custDetail = CustomerDetail::find($header->CustomerDetailInternalID);
                        if ($custDetail->TaxCentralization == 0) {
                            if (is_null($header->customer->NpwpAddress) == false) {
                                $sheet->setCellValueByColumnAndRow(9, $row, $custDetail->NpwpAddress);
                            } else {
                                $sheet->setCellValueByColumnAndRow(9, $row, $custDetail->Address);
                            }
                        } else {
                            if (is_null($header->customer->NpwpAddress) == false) {
                                $sheet->setCellValueByColumnAndRow(9, $row, $header->customer->NpwpAddress);
                            } else {
                                $sheet->setCellValueByColumnAndRow(9, $row, $custDetail->Address);
                            }
                        }
                    } else {
                        if (is_null($header->customer->NpwpAddress) == false) {
                            $sheet->setCellValueByColumnAndRow(9, $row, $header->customer->NpwpAddress);
                        } else {
                            $sheet->setCellValueByColumnAndRow(9, $row, $header->customer->Address);
                        }
                    }
                    //total
                    $total = $header->GrandTotalTopUp * 10 / 11;
                    $sheet->setCellValueByColumnAndRow(10, $row, round($total));
                    $sheet->setCellValueByColumnAndRow(11, $row, floor((round($total) * 0.1)));
                    $sheet->setCellValueByColumnAndRow(12, $row, 0);
                    $sheet->setCellValueByColumnAndRow(13, $row, '');


                    $downpayment = $header->GrandTotalTopUp * 10 / 11;
                    $sheet->setCellValueByColumnAndRow(14, $row, 1);
                    $sheet->setCellValueByColumnAndRow(15, $row, round($downpayment));
                    $sheet->setCellValueByColumnAndRow(16, $row, floor(round($downpayment) * 0.1));
                    $sheet->setCellValueByColumnAndRow(17, $row, "0");
                    $sheet->setCellValueByColumnAndRow(18, $row, $header->TopUpID);

                    $row++;

                    $sheet->setCellValueByColumnAndRow(0, $row, "OF");
                    $sheet->setCellValueByColumnAndRow(1, $row, "01");
                    $sheet->setCellValueByColumnAndRow(2, $row, "Uang Muka atas Penjualan");
                    $sheet->setCellValueByColumnAndRow(3, $row, round($downpayment));
                    $sheet->setCellValueByColumnAndRow(4, $row, 1);
                    $sheet->setCellValueByColumnAndRow(5, $row, 1 * round($downpayment));
                    $sheet->setCellValueByColumnAndRow(6, $row, 0);
                    $sheet->setCellValueByColumnAndRow(7, $row, round($downpayment));
                    $sheet->setCellValueByColumnAndRow(8, $row, floor(round($downpayment) * 0.1));
                    $sheet->setCellValueByColumnAndRow(9, $row, 0);
                    $sheet->setCellValueByColumnAndRow(10, $row, 0);
                    $row++;
                }
                $sheet->setColumnFormat(array(
                    'B' => \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
                    'D' => \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
                    'H' => \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
                ));
            });
        })->export('csv');
    }

    function createID($tipe, $jenis) {
        if ($jenis == "customer")
            $salesOrder = 'TUC';
        elseif ($jenis == "supplier")
            $salesOrder = 'TUS';
        else
            $salesOrder = 'TUC';

        if ($tipe == 0) {
            $salesOrder .= '.' . date('m') . date('y');
        }
        return $salesOrder;
    }

    public function checkSOCustomer() {
        $customer = Input::get('customer');
        $cash = Input::get('cash') + 2;
        $salesOrder = SalesOrderHeader::checkNotShipping($customer, $cash);
        return $salesOrder;
    }

    public function checkPOSupplier() {
        $customer = Input::get('supplier');
        $cash = Input::get('cash') + 2;
        $salesOrder = PurchaseOrderHeader::checkNotMrv($customer, $cash);
        return $salesOrder;
    }

    public function checkSOCustomerUpdate() {
        $customer = Input::get('customer');
        $cash = Input::get('cash') + 2;
        $salesOrder = SalesOrderHeader::bisaUpdateTopUp($customer, $cash);
        return $salesOrder;
    }

    public function checkPOSupplierUpdate() {
        $customer = Input::get('supplier');
        $cash = Input::get('cash') + 2;
        $salesOrder = PurchaseOrderHeader::bisaUpdateTopUp($customer, $cash);
        return $salesOrder;
    }

    public function getSO() {
        $id = Input::get('InternalID');
        $salesOrder = SalesOrderHeader::find($id);
        $grandTotal = $salesOrder->GrandTotal;
        $dp = TopUp::where('SalesOrderInternalID', $id)->get();
        if (count($dp) > 0) {
            foreach ($dp as $row) {
                $grandTotal -= $row->TopUp;
            }
        }

        return $grandTotal;
    }

    public function getPO() {
        $id = Input::get('InternalID');
        $salesOrder = PurchaseOrderHeader::find($id);
        $grandTotal = $salesOrder->GrandTotal;
        $dp = TopUp::where('PurchaseOrderInternalID', $id)->get();
        if (count($dp) > 0) {
            foreach ($dp as $row) {
                $grandTotal -= $row->TopUp;
            }
        }

        return $grandTotal;
    }

    public function checkImportPO() {
        $po = PurchaseOrderHeader::find(Input::get('po'));

        if ($po->Import == 0)
            return "lokal";
        else
            return "impor";
    }

    public function formatCariIDCustomerTopUp() {
        $date = explode('-', Input::get('date'));
        $id = $this->createID(1, "customer");
        $yearDigit = substr($date[2], 2);
        $id .= '.' . $date[1] . $yearDigit . '.';
        echo TopUp::getNextIDCustomerTopUp($id);
    }

    public function formatCariIDSupplierTopUp() {
        $date = explode('-', Input::get('date'));
        $id = $this->createID(1, "supplier");
        $yearDigit = substr($date[2], 2);
        $id .= '.' . $date[1] . $yearDigit . '.';
        echo TopUp::getNextIDSupplierTopUp($id);
    }

    public function topUpDataBackup($data) {
        $explode = explode('---;---', $data);
        $typePayment = $explode[0];
        $typeTax = $explode[1];
        $start = $explode[2];
        $end = $explode[3];
        $where = '';
        if ($typePayment != '-1' && $typePayment != '') {
            $where .= 'PaymentType = "' . $typePayment . '" ';
        }
        if ($typeTax != '-1' && $typeTax != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'm_topup.VAT = "' . $typeTax . '" ';
        }
        if ($start != '' && $end != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $start = explode('-', $start);
            $end = explode('-', $end);
            $startSearch = $start[2] . '-' . $start[1] . '-' . $start[0];
            $endSearch = $end[2] . '-' . $end[1] . '-' . $end[0];
            $where .= 'TopUpDate between "' . $startSearch . ' 00:00:00" AND "' . $endSearch . ' 23:59:59"';
        }
        $table = 'v_topup';
        $primaryKey = 'v_topup`.`InternalID';
        $columns = array(
            array('db' => 'TopUpID', 'dt' => 0),
            array('db' => 'ACC6Name', 'dt' => 1),
            array(
                'db' => 'TopUpDate',
                'dt' => 2,
                'formatter' => function( $d, $row ) {
                    return date("d-m-Y", strtotime($d));
                }
            ),
            array(
                'db' => 'TopUp',
                'dt' => 3,
                'formatter' => function( $d, $row ) {
                    return number_format($d, '2', '.', ',');
                }
            ),
            array(
                'db' => 'GrandTotalTopUp',
                'dt' => 4,
                'formatter' => function( $d, $row ) {
                    return number_format($d, '2', '.', ',');
                }
            ),
            array(
                'db' => 'PaymentType',
                'dt' => 5,
                'formatter' => function( $d, $row ) {
                    if ($d == 0) {
                        return 'CBD';
                    } else if ($d == 1) {
                        return 'Deposit';
                    } else {
                        return 'Down Payment';
                    }
                }
            ),
            array(
                'db' => 'SlipName',
                'dt' => 6,
                'formatter' => function( $d, $row ) {
                    if (isset($d)) {
                        return $d;
                    } else {
                        return '-';
                    }
                }
            ),
            array(
                'db' => 'Transaction',
                'dt' => 7,
                'formatter' => function( $d, $row ) {
                    if (isset($d)) {
                        return $d;
                    } else {
                        return '-';
                    }
                }
            ),
            array('db' => 'Tax', 'dt' => 8, 'formatter' => function($d, $row) {
                    if (isset($d)) {
                        return $d;
                    } else {
                        return '-';
                    }
                }),
//            array(
//                'db' => 'Void',
//                'dt' => 9,
//                'formatter' => function( $d, $row ) {
//                    return $d;
//                }
//            ),
            array('db' => 'InternalID', 'dt' => 9, 'formatter' => function($d, $row) {
                    $topup = TopUp::find($d);
//                    $editCustomer = Customertopup::leftjoin('t_salesorder_header', 'm_customertopup.SalesOrderInternalID', '=', 't_salesorder_header.InternalID')
//                                    ->where('m_customertopup.InternalID', $d)
//                                    ->select('m_customertopup.*', 't_salesorder_header.SalesOrderID')->first();
//                    $arrData = array($editCustomer);
//                    $f = 13;
//                    $tamp = $arrData;
//                    $tamp = myEscapeStringData($arrData);
//                    $tamp = myEncryptJavaScriptText($tamp, $f);
                    //ga bsa 2 2 nya
                    $action = ' <a href="' . Route('topUpUpdate', TopUp::find($d)->TopUpID) . '">'
                            . '<button id="btn-' . $d . '" data-all=""
                                            data-toggle="modal" role="dialog"
                                            class="btn btn-pure-xs btn-xs btn-edit">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </button></a>
                                    
                        <button data-target="#m_topupDelete" data-internal="' . $d . '"  data-toggle="modal" role="dialog"
                                           onclick="deleteAttach(this)" data-id="' . $d . '" data-name="' . TopUp::find($d)->TopUpID . '" class="btn btn-pure-xs btn-xs btn-delete">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>';
                    if (checkModul('O05') && strpos($topup->TopUpID, 'TUC') !== false) {
                        $action .= '<a href="' . Route('topupCSV', $topup->TopUpID) . '" target="_blank">
                                        <button id="btn-' . $topup->TopUpID . '-print"
                                                class="btn btn-pure-xs btn-xs">
                                            <span class="glyphicon glyphicon-download"></span> CSV
                                        </button>
                                    </a>';
                    }
//                        $action .= '<button disabled class="btn btn-pure-xs btn-xs btn-edit"><span class="glyphicon glyphicon-edit"></span></button>';
//                    if ($salesOrderInternalID->Void == "Active")
//                        $void = "Void";
//                    else
//                        $void = "Active";
//                    $action .= '<button disabled class="btn btn-pure-xs btn-xs btn-edit">' . $void . '</button>';


                    return $action;
                }),
        );
        $sql_details = getConnection();

        require('ssp.class.php');
        $ID_CLIENT_VALUE = Auth::user()->CompanyInternalID;
        if ($where != '') {
            $extraCondition = $where . ' AND ' . 'CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        } else {
            $extraCondition = 'CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        }

//        $join = ' INNER JOIN m_slip on m_slip.InternalID = m_topup.SlipInternalID '
//                . 'INNER JOIN m_coa6 on m_coa6.InternalID = m_topup.ACC6InternalID '
//                . 'LEFT JOIN t_salesorder_header on t_salesorder_header.InternalID = m_topup.SalesOrderInternalID ';

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $extraCondition, $join = null));
    }

}
