<?php

class EmailController extends BaseController {

    public function checkActivationAccount($enkripText) {
        $enkripText = myDecryptEmail($enkripText);
        $users = User::all();
        $text = "";
        foreach ($users as $user) {
            $text = $user->UserID . "---;---" . $user->InternalID;
            if (Hash::check($text, $enkripText)) {
                //cek userID dan internal id apakah sama dengan yang dipasingkan
                $usr = User::find($user->InternalID);
                $usr->Status = 1;
                $usr->save();
            }
        }
        if (Auth::check()) {
            Auth::logout();
        }
        //lempar ke view dimana berisi informasi akun telah aktif
        Session::flash('messages', 'suksesAktifasi');
        return Redirect::Route('login');
    }

    public function checkForgotPassword($enkripText) {
        $enkripText = myDecryptEmail($enkripText);
        $users = User::all();
        $text = "";
        foreach ($users as $user) {
            $text = $user->UserID . "---;---" . $user->InternalID;
            if (Hash::check($text, $enkripText)) {
                //cek userID dan internal id apakah sama dengan yang dipasingkan
                $newPass = str_random(8);
                $usr = User::find($user->InternalID);
                $usr->UserPwd = Hash::make($newPass);
                $usr->save();

                $data['name'] = $user->UserName;
                $data['userID'] = $user->UserID;
                $data['password'] = $newPass;
                Session::flash('userForgot',$user);
                
	        Mail::send('emails.sendPassword', $data, function($message) {
	        $user = Session::get('userForgot');
	             $message->to($user->Email, $user->Company->CompanyName)->subject('New Password');
	        });
            }
        }
        if (Auth::check()) {
            Auth::logout();
        }
        //lempar ke view dimana berisi informasi akun telah aktif
        Session::flash('messages', 'suksesResetPassword');
        return Redirect::Route('login');
    }

}