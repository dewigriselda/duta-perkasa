<?php

ini_set('MAX_EXECUTION_TIME', -1);

class HomeController extends BaseController {
    /*
      |--------------------------------------------------------------------------
      | Default Home Controller
      |--------------------------------------------------------------------------
      |
      | You may wish to use controllers instead of, or in addition to, Closure
      | based routes. That's great! Here is an example controller method to
      | get you started. To route to this controller, just add the route:
      |
      |	Route::get('/', 'HomeController@showWelcome');
      |
     */

    public function showDashboard() {
//        $this->importInventory();
//        $this->importParcel();
//        $this->importParcel2();
//        $this->importCoa6();
//        dd('sukses');
//        $this->updateID();
        return View::make('dashboard')
                        ->withAktif('dashboard')
                        ->withToogle('dashboard');
    }

    public function updateID() {
        foreach (Group::where('CompanyInternalID', Auth::user()->CompanyInternalID)->get() as $value) {
            $counter = 1;
            foreach (Inventory::where('GroupInternalID', $value->InternalID)->get() as $value) {
                $value->InventoryID = $value->group->GroupID . str_pad($counter, 4, '0', STR_PAD_LEFT);
                $value->save();
                $counter++;
            }
        }
    }

    static function wStock() {
        $table = 'v_wstock';
        $primaryKey = 'InventoryID';
        $columns = array(
            array('db' => 'InventoryID', 'dt' => 0),
            array('db' => 'InventoryName', 'dt' => 1),
            array(
                'db' => 'currentStock',
                'dt' => 2
            ),
        );

        $sql_details = getConnection();

        require('ssp.class.php');
        $extraCondition = " v_wstock.currentStock < v_wstock.MinStock";

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $extraCondition, $join = NULL));
    }

    public function showLogin() {
        if (Auth::check()) {
            return Redirect::Route('showDashboard');
        } else {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                if (Input::get('jenis') == 'userLogin') {
                    return $this->login();
                } else if (Input::get('jenis') == 'userForgot') {
                    return $this->forgot();
                }
            }
            return View::make('login');
        }
    }

    public function login() {
        //rule
        $rule = array(
            'email' => 'required|email',
            'password' => 'required|max:16'
        );

        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            return View::make('login')
                            ->withMessages('salahLogin')
                            ->withError($validator->messages());
        } else {
            $email = Input::get('email');
            $password = Input::get('password');
            if (Input::get('remember') == 'remember') {
                if (Auth::attempt(array('Email' => $email, 'password' => $password), true)) {
                    $dataUser = User::find(Auth::user()->InternalID);
                    $statusUser = $dataUser->Status;
                    $statusCompany = $dataUser->Company->Status;
                    if ($statusCompany == 0) {
                        Session::flash('status', 'company');
                        return $this->logout();
                    } else if ($statusUser == 0) {
                        Session::flash('status', 'user');
                        return $this->logout();
                    }
                    Session::put('LastLogin', $dataUser->LastLogin);
                    $dataUser->LastLogin = date('Y-m-d H:i:s');
                    $dataUser->save();
                    if ($dataUser->CompanyInternalID == -2) {
                        return Redirect::intended('showHomeAgent');
                    } else if ($dataUser->CompanyInternalID != -1) {
                        return Redirect::intended('showDashboard');
                    }
                    return Redirect::intended('showUser');
                }
//                else if ($email == "r@hasia.com" && $password == 1) {
//                    //login untuk yg non ppn
//                }
            } else {
                if (Auth::attempt(array('Email' => $email, 'password' => $password))) {
                    $dataUser = User::find(Auth::user()->InternalID);
                    $statusUser = $dataUser->Status;
                    $statusCompany = $dataUser->Company->Status;
                    if ($statusCompany == 0) {
                        Session::flash('status', 'company');
                        return $this->logout();
                    } else if ($statusUser == 0) {
                        Session::flash('status', 'user');
                        return $this->logout();
                    }
                    Session::put('LastLogin', $dataUser->LastLogin);
                    $dataUser->LastLogin = date('Y-m-d H:i:s');
                    $dataUser->save();
                    if ($dataUser->CompanyInternalID == -2) {
                        return Redirect::intended('showHomeAgent');
                    } else if ($dataUser->CompanyInternalID != -1) {
                        return Redirect::intended('showDashboard');
                    }
                    return Redirect::intended('showUser');
                }
            }
        }
        return View::make('login')
                        ->withMessages('gagalLogin');
    }

    public function forgot() {
        if (User::where('Email', Input::get('email'))->count() > 0) {
            $user = User::where('Email', Input::get('email'))->first();
            $plainText = $user->UserID . '---;---' . $user->InternalID;
            $enkrip = Hash::make($plainText);
            $enkrip = myEncryptEmail($enkrip);
            $data['name'] = $user->UserName;
            $data["link"] = "http://application.salmonacc.com/checkForgotPassword/" . $enkrip;
            Mail::send('emails.forgotPassword', $data, function($message) {
                $user = User::where('Email', Input::get('email'))->first();
                $message->to($user->Email, $user->Company->CompanyName)->subject('Forgot Password');
            });
            return View::make("login")
                            ->withMessages('suksesResetPassword');
        } else {
            return View::make("login")
                            ->withMessages('gagalResetPassword');
        }
    }

    public function logout() {
        Auth::logout();
        return Redirect::intended('login');
    }

    public function showContact() {
//        $this->importInventory();
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            return $this->contactUs();
        }
        return View::make('contact')->withAktif('none')->withToogle('none');
    }

    public function contactUs() {
        //rule
        $rule = array(
            'Subject' => 'required',
            'message' => 'required'
        );

        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            return View::make('contact')
                            ->withAktif('none')
                            ->withToogle('none')
                            ->withMessages('gagalContact')
                            ->withError($validator->messages());
        } else {
            $user = User::find(Auth::user()->InternalID);
            $data = [];
            $data["userName"] = $user->UserName;
            $data["email"] = $user->Email;
            $data["subject"] = Input::get("Subject");
            $data["pesan"] = Input::get("message");

            Mail::send('emails.feedbackUser', $data, function($message) {
                $message->to("salmon.support@salmonacc.com", "Salmon Support Team")->subject(Input::get("Subject"));
            });

            Mail::send('emails.feedbackUser', $data, function($message) {
                $message->to("dswitono@genesysindonesia.com", "Doni Wistono")->subject(Input::get("Subject"));
            });

            return View::make('contact')->withAktif('none')->withToogle('none')->withMessages('suksesContact');
        }
    }

    public function showHelp() {
//        $this->importInventory();
//        $this->gantiInventoryLama();
//        $this->importGantiHarga();
//        $this->importUploadPrice();
//        $this->importParcel();
//        $this->importParcel2();
//        $this->hapusKembar();
//        $this->hapusCustomerKembar();
//        $this->gantiNPWP();
//        $this->isiStock();
//        $this->buatInventoryTransferRSIN();
        return View::make('help')->withAktif('help')->withToogle('');
    }

    public function buatInventoryTransferSIN() {
        $log='';
        foreach (ShippingAddHeader::where("VAT", 0)->where("ShippingDate", "<", "2017-11-24")->get() as $sh) {
            $header = new TransferHeader;
            $transfer = $this->createIDTransfer(1) . '.';
            $date = explode('-', $sh->ShippingDate);
            $yearDigit = substr($date[0], 2);
            $transfer .= $date[1] . $yearDigit . '.';
            $transferNumber = TransferHeader::getNextIDTransfer($transfer);
            $header->TransferID = $transferNumber;
            $header->TransferDate = $sh->ShippingDate;
            $header->WarehouseInternalID = $sh->WarehouseInternalID;
            $header->WarehouseDestinyInternalID = Warehouse::where("Type", 1)->first()->InternalID;
            $header->TransferType = 1;
            $header->UserRecord = $sh->UserRecord;
            $header->CompanyInternalID = Auth::user()->Company->InternalID;
            $header->UserModified = '0';
            $header->Remark = $sh->ShippingID;
            $header->save();

            $log .= $header->TransferID.' '.$sh->ShippingID.'<br>';
            foreach (ShippingAddDetail::where("ShippingInternalID", $sh->InternalID)->get() as $sd) {
                $detail = new TransferDetail();
                $detail->TransferInternalID = $header->InternalID;
                $detail->InventoryInternalID = $sd->InventoryInternalID;
                $detail->UomInternalID = $sd->UomInternalID;
                $detail->Qty = $sd->Qty;
                $detail->UserRecord = $sd->UserRecord;
                $detail->UserModified = '0';
                $detail->save();
                setTampInventory($sd->InventoryInternalID);
            }
        }
        dd($log);
    }

    public function buatInventoryTransferRSIN() {
        $log='';
        foreach (SalesReturnHeader::where("VAT", 0)->where("SalesReturnDate", "<", "2017-11-24")->get() as $sh) {
            $header = new TransferHeader;
            $transfer = $this->createIDTransfer(1) . '.';
            $date = explode('-', $sh->SalesReturnDate);
            $yearDigit = substr($date[0], 2);
            $transfer .= $date[1] . $yearDigit . '.';
            $transferNumber = TransferHeader::getNextIDTransfer($transfer);
            $header->TransferID = $transferNumber;
            $header->TransferDate = $sh->SalesReturnDate;
            $header->WarehouseInternalID = Warehouse::where("Type", 1)->first()->InternalID;
            $header->WarehouseDestinyInternalID = $sh->WarehouseInternalID;
            $header->TransferType = 1;
            $header->UserRecord = $sh->UserRecord;
            $header->CompanyInternalID = Auth::user()->Company->InternalID;
            $header->UserModified = '0';
            $header->Remark = $sh->SalesReturnID;
            $header->save();

            $log .= $header->TransferID.' '.$sh->SalesReturnID.'<br>';
            foreach (SalesReturnDetail::where("SalesReturnInternalID", $sh->InternalID)->get() as $sd) {
                $detail = new TransferDetail();
                $detail->TransferInternalID = $header->InternalID;
                $detail->InventoryInternalID = $sd->InventoryInternalID;
                $detail->UomInternalID = $sd->UomInternalID;
                $detail->Qty = $sd->Qty;
                $detail->UserRecord = $sd->UserRecord;
                $detail->UserModified = '0';
                $detail->save();
                setTampInventory($sd->InventoryInternalID);
            }
        }
        dd($log);
    }

    function createIDTransfer($tipe) {
        $transfer = 'TF';
        if ($tipe == 0) {
            $transfer .= '.' . date('m') . date('y');
        }
        return $transfer;
    }

    public function isiStock() {
        $inv = Inventory::orderBy("InternalID", "asc")->take(500)->skip(300)->get();

        foreach ($inv as $i) {
            $mid = new MemoInDetail();
            $mid->MemoInInternalID = 1;
            $mid->InventoryInternalID = $i->InternalID;
            $mid->UomInternalID = InventoryUom::where("InventoryInternalID", $i->InternalID)->where("Default", 1)->first()->UomInternalID;
            $mid->Qty = 10000;
            $mid->Price = 0;
            $mid->SubTotal = 0;
            $mid->Remark = '3';
            $mid->UserRecord = Auth::user()->InternalID;
            $mid->save();
        }
        dd("3 done");
    }

    public function gantiInventoryLama() {
        $data = Excel::load('excel/catatancyc.xlsx', function($reader) {
                    $result = $reader->select(array('idlama', 'transaksi', 'iddetail'))->get();
                    $record = '';
                    foreach ($result[0] as $row) {
                        $lama = Inventory::find($row['idlama']);
                        $inv = Inventory::where("InventoryName", $lama->InventoryName)->where("InternalID", "!=", $row['idlama'])->first();
                        if (count($inv) == 0) {
//                            if ($row['idlama'] != 29344 && $row['idlama'] != 29347 & $row['idlama'] != 29355 && $row['idlama'] != 29356 && $row['idlama'] != 29630 && $row['idlama'] != 29631 && $row['idlama'] != 29632 && $row['idlama'] != 29633)
//                                dd($row['idlama']);
                            if ($row['idlama'] == 29344)
                                $inv = Inventory::where("InventoryName", "AM B2/X4 100(112)LB5 20mm")->where("InternalID", "!=", $row['idlama'])->first();
                            else if ($row['idlama'] == 29347)
                                $inv = Inventory::where("InventoryName", "AM B3/X5 80B5 25mm")->where("InternalID", "!=", $row['idlama'])->first();
                            else if ($row['idlama'] == 29355)
                                $inv = Inventory::where("InventoryName", "AM B5/X8 100(112)B5 45mm")->where("InternalID", "!=", $row['idlama'])->first();
                            else if ($row['idlama'] == 29356)
                                $inv = Inventory::where("InventoryName", "AM B0/X2 90SB5 12mm")->where("InternalID", "!=", $row['idlama'])->first();
                            else if ($row['idlama'] == 29630)
                                $inv = Inventory::where("InventoryName", "8090-X2-28mm W")->where("InternalID", "!=", $row['idlama'])->first();
                            else if ($row['idlama'] == 29631)
                                $inv = Inventory::where("InventoryName", "8115-X3-38mm W (6120)")->where("InternalID", "!=", $row['idlama'])->first();
                            else if ($row['idlama'] == 29632)
                                $inv = Inventory::where("InventoryName", "8130-X4-50mm W")->where("InternalID", "!=", $row['idlama'])->first();
                            else if ($row['idlama'] == 29633)
                                $inv = Inventory::where("InventoryName", "8160-X5-60mm W")->where("InternalID", "!=", $row['idlama'])->first();
                        }
//                        if ($row['idlama'] != 29344 && $row['idlama'] != 29347 && $row['idlama'] != 29355 && $row['idlama'] != 29356 && $row['idlama'] != 29630 && $row['idlama'] != 29631 && $row['idlama'] != 29632 && $row['idlama'] != 29633) {
                        if ($row['transaksi'] == "quot") {
                            $q = QuotationDetail::find($row['iddetail']);
                            $q->InventoryInternalID = $inv->InternalID;
                            $q->save();
                        } else if ($row['transaksi'] == "po") {
                            $q = PurchaseOrderDetail::find($row['iddetail']);
                            $q->InventoryInternalID = $inv->InternalID;
                            $q->save();

                            foreach (MrvDetail::where("PurchaseOrderDetailInternalID", $q->InternalID)->get() as $m) {
                                $m->InventoryInternalID = $inv->InternalID;
                                $m->save();

                                foreach (PurchaseDetail::where("MrvDetailInternalID", $m->InternalID)->get() as $p) {
                                    $p->InventoryInternalID = $inv->InternalID;
                                    $p->save();
                                }
                            }
                        } else if ($row['transaksi'] == "so") {
                            $q = SalesOrderDetail::find($row['iddetail']);
                            $q->InventoryInternalID = $inv->InternalID;
                            $q->save();

                            foreach (ShippingAddDetail::where("SalesOrderDetailInternalID", $q->InternalID)->get() as $m) {
                                $m->InventoryInternalID = $inv->InternalID;
                                $m->save();
                            }
                            foreach (SalesAddDetail::where("SalesOrderDetailInternalID", $q->InternalID)->get() as $m) {
                                $m->InventoryInternalID = $inv->InternalID;
                                $m->save();
                            }
                        } else if ($row['transaksi'] == "transfer") {
                            $q = TransferDetail::find($row['iddetail']);
                            $q->InventoryInternalID = $inv->InternalID;
                            $q->save();
                        } else {
                            dd($record);
                        }
                        $record .= $row['idlama'] . " ganti " . $inv->InternalID . " di " . $row['transaksi'] . ' ' . $row['iddetail'] . "<br>";
//                        }
                    }
                    dd($record);
                });
        dd('selesai ganti inventory lama');
    }

    public function hapusCustomerKembar() {
//select ACC6Name from `m_coa6` where Type = "c" group by `ACC6Name` having COUNT(ACC6Name) > 1
        $record = '';
        $cuskembar = DB::table('m_coa6')
                        ->select(DB::raw('ACC6Name, count(*) as jml'))
                        ->where("Type", "c")
                        ->groupBy('ACC6Name')
                        ->having('jml', '>', 1)->get();

        foreach ($cuskembar as $ck) {
            $cus = Coa6::where("ACC6Name", $ck->ACC6Name)->where("Type", "c")->orderBy("InternalID", "asc")->get();
            $hitungpakai = 0;

            foreach ($cus as $c) {
                $cSO = SalesOrderHeader::where('ACC6InternalID', $c->InternalID)->count();
                $cQU = QuotationHeader::where('ACC6InternalID', $c->InternalID)->count();
                if (($cSO + $cQU) > 0) {
                    //DIPAKAI
                    $hitungpakai++;
                }
            }

            if ($hitungpakai == 0) {
                //ga ada customer yg dipakai
                $hapus = Coa6::where("ACC6Name", $ck->ACC6Name)->where("Type", "c")->orderBy("InternalID", "asc")->first();
                foreach ($cus as $h) {
                    if ($h->InternalID != $hapus->InternalID) {
                        $record .= $ck->ACC6Name . " " . $h->InternalID . "<br>";
                        $d = Coa6::find($h->InternalID)->delete();
                    }
                }
            } else if ($hitungpakai == 1) {
                //dipakai cma 1
                foreach ($cus as $c) {
                    $cSO = SalesOrderHeader::where('ACC6InternalID', $c->InternalID)->count();
                    $cQU = QuotationHeader::where('ACC6InternalID', $c->InternalID)->count();
                    if ($cSO + $cQU <= 0) {
                        $record .= $ck->ACC6Name . " " . $c->InternalID . "<br>";
                        $d = Coa6::find($c->InternalID)->delete();
                    }
                }
            } else {
                
            }
        }
        dd($record);
    }

    public function importUploadPower() {
        $data = Excel::load('excel/uploadpoweralu.xlsx', function($reader) {
                    $result = $reader->select(array('id', 'power'))->get();
                    foreach ($result[0] as $row) {
                        $inventory = Inventory::where('InventoryID', $row['id'])->first();
                        $inventory->Power = $row['power'];
                        $inventory->save();
                    }
                });
        dd('selesai9');
    }

    public function gantiNPWP() {
        $data = Excel::load('excel/nomornpwp.xlsx', function($reader) {
                    $result = $reader->select(array('npwp', 'nama'))->get();
                    foreach ($result[0] as $row) {
//                        if($row['nama'] == "CV. ROM POER SEJAHTERA")
//                        {
//                            dd($row['npwp']);
//                            dd(Coa6::where("Type", "c")->where('ACC6Name', $row['nama'])->first());
//                        }
                        $npwp = '';
                        $customer = Coa6::where("Type", "c")->where('ACC6Name', $row['nama'])->first();
                        if (count($customer) > 0) {
                            if ($row['npwp'] != 0) {
                                for ($a = 0; $a < strlen($row['npwp']); $a++) {
                                    if ($a == 1 || $a == 4 || $a == 7 || $a == 11)
                                        $npwp .= $row['npwp'][$a] . ".";
                                    else if ($a == 8)
                                        $npwp .= $row['npwp'][$a] . "-";
                                    else
                                        $npwp .= $row['npwp'][$a];
                                }
//                                dd($npwp);
                                $customer->TaxID = $npwp;
                            }
                            $customer->save();
                        }
                    }
                });
        dd('selesai9');
    }

    public function pindahPower() {
        foreach (Inventory::where('Power', 0)->orWhere('Power', '')->get() as $i) {
//            if ($i->Power == 0 || $i->Power == '' || $i->Power == null) {
            //cek power d inv uom
            $uom = InventoryUom::where('InventoryInternalID', $i->InternalID)->first();
//            if ($i->InternalID == 18944)
//                dd($uom);
            if ($uom->Power != 0 && $uom->Power != '' && $uom->Power != null) {
                $i->Power = $uom->Power;
                $i->save();
            }
//            }
        }
        dd('done');
    }

    public function hapusKembar() {
        $data = Inventory::where('InventoryID', "like", "%WSP%")->get();
        $cnt = 1;
        foreach ($data as $d) {
            if ($cnt > 0) {
                $count = Inventory::where('InventoryID', $d->InventoryID)->count();
                if ($count > 1) {
                    //data lama disamakan dengan data baru
                    $kembar = Inventory::where('InventoryID', $d->InventoryID)->orderBy('dtRecord', 'asc')->get();
                    $kembar[0]->BrandInternalID = $kembar[1]->BrandInternalID;
                    $kembar[0]->GroupInternalID = $kembar[1]->GroupInternalID;
                    $kembar[0]->VarietyInternalID = $kembar[1]->VarietyInternalID;
                    $kembar[0]->Remark = $kembar[1]->Remark;
                    $kembar[0]->save();

                    //uom lama juga diupdate
                    $uomlama = InventoryUom::where('InventoryInternalID', $kembar[0]->InternalID)->first();
                    $uombaru = InventoryUom::where('InventoryInternalID', $kembar[1]->InternalID)->first();
                    $uomlama->PriceA = $uombaru->PriceA;
                    $uomlama->Power = $uombaru->Power;
                    $uomlama->save();

                    //uom baru dihapus, inv baru dihapus
                    $uombaru->delete();
                    $kembar[1]->delete();
                }
                $cnt++;
            }
        }
        dd('selesai');
    }

    public function showChangeLog() {
        return View::make('changeLog')->withAktif('changeLog')->withToogle('');
    }

    public function importCoaMaster() {
        $data = Excel::load('excel/coamaster.xlsx', function($reader) {
                    $result = $reader->select(array('nama', 'acc1', 'acc2', 'acc3', 'acc4', 'lv1', 'lv2', 'lv3', 'persediaan', 'flag'))->get();
                    foreach ($result[0] as $row) {
                        $coa = new Coa();
                        $coa->ACC1InternalID = $row['acc1'];
                        $coa->ACC2InternalID = $row['acc2'];
                        $coa->ACC3InternalID = $row['acc3'];
                        $coa->ACC4InternalID = $row['acc4'];
                        $coa->ACC5InternalID = 0;
                        $coa->ACC6InternalID = 0;
                        $coa->CoaName = $row['nama'];
                        $coa->Flag = $row['flag'];
                        $coa->Header1 = $row['lv1'];
                        $coa->Header2 = $row['lv2'];
                        $coa->Header3 = $row['lv3'];
                        $coa->HeaderCashFlow1 = '-';
                        $coa->HeaderCashFlow2 = '-';
                        $coa->HeaderCashFlow3 = '-';
                        $coa->Persediaan = $row['persediaan'];
                        $coa->InitialBalance = 0;
                        $coa->Remark = '-';
                        $coa->CompanyInternalID = Auth::user()->Company->InternalID;
                        $coa->UserRecord = Auth::user()->UserID;
                        $coa->UserModified = "0";
                        $coa->save();
                    }
                });
        dd('DONE IMPORT COA');
    }

    public function importGantiHarga() {
        $data = Excel::load('excel/uploadinventorybaru/price04hst.xlsx', function($reader) {
                    $result = $reader->select(array('inventoryid', 'inventoryname', 'power', 'varietyinternalid', 'price', 'remark', 'brand', 'group'))->get();
//                    $result = $reader->select(array('cat', 'name', 'list'))->get();
//                    Inventory::where('InternalID', '>', 0)->delete();
//                    InventoryUom::where('InternalID', '>', 0)->delete();

                    foreach ($result[0] as $row) {
//                    $in = Inventory::where('InventoryName', $row['name'])->first();
//                    $inUom = InventoryUom::where('InventoryInternalID',$in->InternalID)->update(array('PriceA' => $row['list']));
//                    $in->Power = $row['power'];
//                    $in->save();
                        $cek = Variety::where('VarietyID', $row['varietyinternalid'])->count();
                        if ($cek == 0) {
                            $variety = new Variety();
                            $variety->VarietyID = $row['varietyinternalid'];
                            $variety->VarietyName = $row['varietyinternalid'];
                            $variety->GroupInternalID = $row['group'];
                            $variety->Remark = '-';
                            $variety->CompanyInternalID = Auth::user()->Company->InternalID;
                            $variety->UserRecord = Auth::user()->UserID;
                            $variety->UserModified = "0";
                            $variety->save();
                        } else {
                            $variety = Variety::where('VarietyID', $row['varietyinternalid'])->first();
                        }
                        $inventory = Inventory::where('InventoryID', $row['inventoryid'])->first();
//                        $inventory = new Inventory;
//                        $inventory->InventoryID = $row['inventoryid'];
//                        $inventory->InventoryTypeInternalID = 5;
//                        $inventory->InventoryName = $row['inventoryname'];
//                        $inventory->UoM = 'PCS';
//                        $inventory->LeadTime = 0;
//                        $inventory->UsageInventory = 0;
//                        $inventory->MaxStock = 1000;
//                        $inventory->MinStock = 1;
////                        $inventory->BarcodeCode = Input::get('BarcodeCode');
//                        $inventory->GroupInternalID = $variety->GroupInternalID;
                        $inventory->VarietyInternalID = $variety->InternalID;
                        $inventory->BrandInternalID = $row['brand'];
//                        $inventory->UserRecord = Auth::user()->UserID;
//                        $inventory->CompanyInternalID = Auth::user()->Company->InternalID;
//                        $inventory->UserModified = "0";
                        $inventory->UserModified = Auth::user()->UserID;
//                        $inventory->Remark = $row['remark'];
                        $inventory->save();

//                        $inventory = Inventory::where('InventoryID', $row['inventoryid'])->first();
                        //create inventory uom langsung
//                        $inventoryUom = InventoryUom::where('InventoryInternalID', $inventory->InternalID)->first();
//                        $inventoryUom->InventoryInternalID = $inventory->InternalID;
//                        $inventoryUom->UomInternalID = 16;
//                        $inventoryUom->PriceA = $row['price'];
//                        $inventoryUom->PriceB = 0;
//                        $inventoryUom->PriceC = 0;
//                        $inventoryUom->PriceD = 0;
//                        $inventoryUom->PriceE = 0;
//                        $inventoryUom->Default = 1;
//                        $inventoryUom->Value = 1;
//                        $inventoryUom->Power = $row['power'];
//                        $inventoryUom->Status = 0;
//                        $inventoryUom->UserRecord = Auth::user()->UserID;
//                        $inventoryUom->UserModified= Auth::user()->UserID;
////                        $inventoryUom->UserModified = "0";
//                        $inventoryUom->CompanyInternalID = Auth::user()->Company->InternalID;
//                        $inventoryUom->Remark = '-';
//                        $inventoryUom->save();
                    }
                });

        dd('HST');
    }

    public function importInventory() {
        $data = Excel::load('excel/inv02cyc.xlsx', function($reader) {
                    $result = $reader->select(array('inventoryid', 'inventoryname', 'power', 'varietyinternalid', 'price', 'remark', 'brand', 'group'))->get();
//                    $result = $reader->select(array('cat', 'name', 'list'))->get();
//                    Inventory::where('InternalID', '>', 0)->delete();
//                    InventoryUom::where('InternalID', '>', 0)->delete();

                    foreach ($result[0] as $row) {
//                    $in = Inventory::where('InventoryName', $row['name'])->first();
//                    $inUom = InventoryUom::where('InventoryInternalID',$in->InternalID)->update(array('PriceA' => $row['list']));
//                    $in->Power = $row['power'];
//                    $in->save();
                        $cek = Variety::where('VarietyID', $row['varietyinternalid'])->count();
                        if ($cek == 0) {
                            $variety = new Variety();
                            $variety->VarietyID = $row['varietyinternalid'];
                            $variety->VarietyName = $row['varietyinternalid'];
                            $variety->GroupInternalID = $row['group'];
                            $variety->Remark = '-';
                            $variety->CompanyInternalID = Auth::user()->Company->InternalID;
                            $variety->UserRecord = Auth::user()->UserID;
                            $variety->UserModified = "0";
                            $variety->save();
                        } else {
                            $variety = Variety::where('VarietyID', $row['varietyinternalid'])->first();
                        }

                        $inventory = new Inventory;
                        $inventory->InventoryID = $row['inventoryid'];
                        $inventory->InventoryTypeInternalID = 5;
                        $inventory->InventoryName = $row['inventoryname'];
                        $inventory->UoM = 'PCS';
                        $inventory->LeadTime = 0;
                        $inventory->UsageInventory = 0;
                        $inventory->MaxStock = 1000;
                        $inventory->MinStock = 1;
//                        $inventory->BarcodeCode = Input::get('BarcodeCode');
                        $inventory->GroupInternalID = $variety->GroupInternalID;
                        $inventory->VarietyInternalID = $variety->InternalID;
                        $inventory->BrandInternalID = $row['brand'];
                        $inventory->UserRecord = Auth::user()->UserID;
                        $inventory->CompanyInternalID = Auth::user()->Company->InternalID;
                        $inventory->UserModified = "0";
                        $inventory->Remark = $row['remark'];
                        $inventory->save();

                        //create inventory uom langsung
                        $inventoryUom = new InventoryUom();
                        $inventoryUom->InventoryInternalID = $inventory->InternalID;
                        $inventoryUom->UomInternalID = 16;
                        $inventoryUom->PriceA = $row['price'];
                        $inventoryUom->PriceB = 0;
                        $inventoryUom->PriceC = 0;
                        $inventoryUom->PriceD = 0;
                        $inventoryUom->PriceE = 0;
                        $inventoryUom->Default = 1;
                        $inventoryUom->Value = 1;
                        $inventoryUom->Power = $row['power'];
                        $inventoryUom->Status = 0;
                        $inventoryUom->UserRecord = Auth::user()->UserID;
                        $inventoryUom->UserModified = "0";
                        $inventoryUom->CompanyInternalID = Auth::user()->Company->InternalID;
                        $inventoryUom->Remark = '-';
                        $inventoryUom->save();
                    }
                });

        dd('cyc8aug');
    }

    public function importUploadPrice() {
        $data = Excel::load('excel/uploadprice/uploadprice09.xlsx', function($reader) {
                    $result = $reader->select(array('inventoryid', 'price'))->get();
                    foreach ($result[0] as $row) {
                        $inventory = Inventory::where('InventoryID', $row['inventoryid'])->first();
                        $inventoryuom = InventoryUom::where('InventoryInternalID', $inventory->InternalID)->where('Default', 1)->first();
                        $inventoryuom->PriceA = $row['price'];
                        $inventoryuom->save();
                    }
                });
        dd('selesai9');
    }

    public function importCoa6() {
        $data = Excel::load('excel/coa6dutaperkasa.xlsx', function($reader) {
                    $result = $reader->select(array('acc6id', 'acc6name', 'address', 'city', 'block', 'addressnumber', 'rt', 'rw', 'district', 'subdistrict', 'province', 'postalcode', 'taxid', 'phone', 'creditlimit', 'type', 'typeuser'))->get();
                    foreach ($result[0] as $row) {
                        if ($row['taxid'] == 0) {
                            $row['taxid'] = '000000000000000';
                        }
                        $taxId = str_split($row['taxid']);
                        $taxText = $taxId[0] . $taxId[1] . '.' . $taxId[2] . $taxId[3] . $taxId[4] . '.' . $taxId[5] . $taxId[6] . $taxId[7] . '.' . $taxId[8] . '-' . $taxId[9] . $taxId[10] . $taxId[11] . '.' . $taxId[12];
                        if (count($taxId) > 13) {
                            $taxText .= $taxId[13];
                        }
                        if (count($taxId) > 14) {
                            $taxText .= $taxId[14];
                        }
                        if (Coa6::where('ACC6ID', $row['acc6id'])->count() <= 0) {
                            $coa6 = new Coa6;
                            $coa6->ACC6ID = $row['acc6id'];
                            $coa6->ACC6Name = $row['acc6name'];
                            $coa6->Address = $row['address'];
                            $coa6->City = $row['city'];
                            $coa6->Block = $row['block'];
                            $coa6->AddressNumber = $row['addressnumber'];
                            $coa6->RT = $row['rt'];
                            $coa6->RW = $row['rw'];
                            $coa6->District = $row['district'];
                            $coa6->Subdistrict = $row['subdistrict'];
                            $coa6->Province = $row['province'];
                            $coa6->PostalCode = $row['postalcode'];
                            $coa6->TaxID = $taxText;
                            $coa6->Phone = $row['phone'];
                            $coa6->CreditLimit = $row['creditlimit'];
                            $coa6->Type = $row['type'];
                            $coa6->typeUser = $row['typeuser'];
                            $coa6->UserRecord = Auth::user()->UserID;
                            $coa6->UserModified = "0";
                            $coa6->CompanyInternalID = Auth::user()->Company->InternalID;
                            $coa6->Remark = '-';
                            $coa6->save();
                        }
                    }
                });
    }

    public function importParcel() {
        $data = Excel::load('excel/uploadinventorybaru/parceldutaperkasa.xlsx', function($reader) {
                    $result = $reader->select(array('parcelid', 'parcelname', 'remark', 'inventory1', 'inventory2', 'inventory3', 'inventory4', 'inventory5', 'inventory6'))->get();
                    foreach ($result[0] as $row) {
                        if (Parcel::where('ParcelID', $row['parcelid'])->count() <= 0) {
                            $parcel = new Parcel;
                            $parcel->ParcelID = $row['parcelid'];
                            $parcel->ParcelName = $row['parcelname'];
                            $parcel->TextPrint = $row['parcelname'];
                            $parcel->BarcodeCode = 0;
                            $parcel->GrandTotal = 0;
                            $parcel->Commission = 0;
                            $parcel->UserRecord = Auth::user()->UserID;
                            $parcel->UserModified = "0";
                            $parcel->CompanyInternalID = Auth::user()->Company->InternalID;
                            $parcel->Remark = $row['remark'];
                            $parcel->save();

                            //create inventory parcel langsung
                            $array = array($row['inventory1'], $row['inventory2'], $row['inventory3'], $row['inventory4'], $row['inventory5'], $row['inventory6']);
                            foreach ($array as $data) {
                                if ($data != 'Item Code') {
                                    $inventoryParcel = new ParcelInventory();
                                    $inventoryParcel->ParcelInternalID = $parcel->InternalID;
                                    $inventoryParcel->InventoryInternalID = Inventory::where('InventoryName', $data)->first()->InternalID;
                                    $inventoryParcel->UomInternalID = 16;
                                    $inventoryParcel->Qty = 1;
                                    $inventoryParcel->Price = 0;
                                    $inventoryParcel->SubTotal = 0;
                                    $inventoryParcel->UserRecord = Auth::user()->UserID;
                                    $inventoryParcel->UserModified = "0";
                                    $inventoryParcel->CompanyInternalID = Auth::user()->Company->InternalID;
                                    $inventoryParcel->Remark = '-';
                                    $inventoryParcel->save();
                                }
                            }
                        }
                    }
                });
        dd('doneparcel');
    }

    public function importParcel2() {
        $data = Excel::load('excel/uploadinventorybaru/parceldutaperkasa2.xlsx', function($reader) {
                    $result = $reader->select(array('parcelid', 'parcelname', 'remark', 'inventoryu', 'inventoryp', 'inventory1', 'inventory2', 'inventory3', 'inventory4', 'inventory5'))->get();
                    foreach ($result[0] as $row) {
                        if (Parcel::where('ParcelID', $row['parcelid'] . 'U')->count() <= 0) {
                            $parcel = new Parcel;
                            $parcel->ParcelID = $row['parcelid'] . 'U';
                            $parcel->ParcelName = $row['parcelname'] . ' U(foot)';
                            $parcel->TextPrint = $row['parcelname'] . ' U(foot)';
                            $parcel->BarcodeCode = 0;
                            $parcel->GrandTotal = 0;
                            $parcel->Commission = 0;
                            $parcel->UserRecord = Auth::user()->UserID;
                            $parcel->UserModified = "0";
                            $parcel->CompanyInternalID = Auth::user()->Company->InternalID;
                            $parcel->Remark = $row['remark'];
                            $parcel->save();

                            //create inventory parcel langsung
                            $array = array($row['inventoryu'], $row['inventory1'], $row['inventory2'], $row['inventory3'], $row['inventory4'], $row['inventory5']);
                            foreach ($array as $data) {
                                if ($data != 'Item Code') {
                                    $inventoryParcel = new ParcelInventory();
                                    $inventoryParcel->ParcelInternalID = $parcel->InternalID;
                                    $inventoryParcel->InventoryInternalID = Inventory::where('InventoryName', $data)->first()->InternalID;
                                    $inventoryParcel->UomInternalID = 16;
                                    $inventoryParcel->Qty = 1;
                                    $inventoryParcel->Price = 0;
                                    $inventoryParcel->SubTotal = 0;
                                    $inventoryParcel->UserRecord = Auth::user()->UserID;
                                    $inventoryParcel->UserModified = "0";
                                    $inventoryParcel->CompanyInternalID = Auth::user()->Company->InternalID;
                                    $inventoryParcel->Remark = '-';
                                    $inventoryParcel->save();
                                }
                            }
                        }
                        if (Parcel::where('ParcelID', $row['parcelid'] . 'P')->count() <= 0) {
                            $parcel = new Parcel;
                            $parcel->ParcelID = $row['parcelid'] . 'P';
                            $parcel->ParcelName = $row['parcelname'] . ' P(No foot)';
                            $parcel->TextPrint = $row['parcelname'] . ' P(No foot)';
                            $parcel->BarcodeCode = 0;
                            $parcel->GrandTotal = 0;
                            $parcel->Commission = 0;
                            $parcel->UserRecord = Auth::user()->UserID;
                            $parcel->UserModified = "0";
                            $parcel->CompanyInternalID = Auth::user()->Company->InternalID;
                            $parcel->Remark = $row['remark'];
                            $parcel->save();

                            //create inventory parcel langsung
                            $array = array($row['inventoryp'], $row['inventory1'], $row['inventory2'], $row['inventory3'], $row['inventory4'], $row['inventory5']);
                            foreach ($array as $data) {
                                if ($data != 'Item Code') {
                                    $inventoryParcel = new ParcelInventory();
                                    $inventoryParcel->ParcelInternalID = $parcel->InternalID;
                                    $inventoryParcel->InventoryInternalID = Inventory::where('InventoryName', $data)->first()->InternalID;
                                    $inventoryParcel->UomInternalID = 16;
                                    $inventoryParcel->Qty = 1;
                                    $inventoryParcel->Price = 0;
                                    $inventoryParcel->SubTotal = 0;
                                    $inventoryParcel->UserRecord = Auth::user()->UserID;
                                    $inventoryParcel->UserModified = "0";
                                    $inventoryParcel->CompanyInternalID = Auth::user()->Company->InternalID;
                                    $inventoryParcel->Remark = '-';
                                    $inventoryParcel->save();
                                }
                            }
                        }
                    }
                });
        dd('doneparcel2');
    }

}
