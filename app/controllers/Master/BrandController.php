<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class BrandController extends BaseController {

    public function showBrand() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertBrand') {
                return $this->insertBrand();
            }
            if (Input::get('jenis') == 'updateBrand') {
                return $this->updateBrand();
            }
            if (Input::get('jenis') == 'deleteBrand') {
                return $this->deleteBrand();
            }
        }
        return View::make('master.brand')
                        ->withToogle('master')->withAktif('brand');
    }

    public function insertBrand() {
        $data = Input::all();
        $rule = array(
            "BrandID" => 'required|max:200|unique:m_brand,BrandID,NULL,BrandID,CompanyInternalID,' . Auth::user()->Company->InternalID . '',
            "BrandName" => 'required',
            "remark" => 'required'
        );
        $validator = Validator::make($data, $rule);
        if ($validator->Fails()) {
            return View::make('master.brand')
                            ->withMessages("gagalInsert")
                            ->withErrors($validator->messages())
                            ->withToogle('master')->withAktif('brand');
        } else {
            $brand = new Brand();
            $brand->BrandID = Input::get('BrandID');
            $brand->BrandName = Input::get('BrandName');
            $brand->Remark = Input::get('remark');
            $brand->CompanyInternalID = Auth::user()->Company->InternalID;
            $brand->UserModified = "0";
            $brand->UserRecord = Auth::user()->UserID;
            $brand->save();

            return View::make('master.brand')
                            ->withMessages("suksesInsert")
                            ->withToogle('master')->withAktif('brand');
        }
    }

    public function updateBrand() {
        $data = Input::all();
        $rule = array(
            "BrandName" => 'required',
            "remark" => 'required'
        );
        $validator = Validator::make($data, $rule);
        if ($validator->Fails()) {
            return View::make('master.brand')
                            ->withMessages("gagalInsert")
                            ->withErrors($validator->messages())
                            ->withToogle('master')->withAktif('brand');
        } else {
            $brand = Brand::find(Input::get('InternalID'));
            $brand->BrandName = Input::get('BrandName');
            $brand->Remark = Input::get('remark');
            $brand->CompanyInternalID = Auth::user()->Company->InternalID;
            $brand->UserModified = Auth::user()->UserID;
            $brand->save();

            return View::make('master.brand')
                            ->withMessages("suksesUpdate")
                            ->withToogle('master')->withAktif('brand');
        }
    }

    public function deleteBrand() {
        $Brand = DB::table('m_inventory')->where('BrandInternalID', Input::get('InternalID'))->first();
        if (is_null($Brand)) {
            //tidak ada, maka boleh hapus
            $brand = Brand::find(Input::get('InternalID'));
            if ($brand->CompanyInternalID == Auth::user()->Company->InternalID) {
                $brand->delete();
                return View::make('master.brand')
                                ->withMessages('suksesDelete')
                                ->withToogle('master')->withAktif('brand');
            } else {
                return View::make('master.brand')
                                ->withMessages('accessDenied')
                                ->withToogle('master')->withAktif('brand');
            }
        } else {
            //ada maka tidak dihapus
            return View::make('master.brand')
                            ->withMessages('gagalDelete')
                            ->withToogle('master')->withAktif('brand');
        }
    }

    public function exportBrand() {
        Excel::create('Master_Brand', function($excel) {
            $excel->sheet('Master_Brand', function($sheet) {
                $sheet->mergeCells('B1:G1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Master Brand");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Brand ID");
                $sheet->setCellValueByColumnAndRow(3, 2, "Brand Name");
                $sheet->setCellValueByColumnAndRow(4, 2, "Record");
                $sheet->setCellValueByColumnAndRow(5, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(6, 2, "Remark");
                $row = 3;
                foreach (Brand::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, "`" . $data->BrandID);
                    $sheet->setCellValueByColumnAndRow(3, $row, $data->BrandName);
                    $sheet->setCellValueByColumnAndRow(4, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(5, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(6, $row, $data->Remark);
                    $row++;
                }

                if (Brand::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() <= 0) {
                    $sheet->mergeCells('B3:G3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "No data available in table");

                    $sheet->cells('B3:G3', function($cells) {
                        $cells->setAlignment('center');
                    });
                    $sheet->setBorder('B3:G' . $row, 'thin');
                }

                $row--;
                $sheet->setBorder('B2:G' . $row, 'thin');
                $sheet->cells('B2:G2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:G' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
            });
        })->export('xls');
    }

}
