<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class VarietyController extends BaseController {

    public function showVariety() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertVariety') {
                return $this->insertVariety();
            }
            if (Input::get('jenis') == 'updateVariety') {
                return $this->updateVariety();
            }
            if (Input::get('jenis') == 'deleteVariety') {
                return $this->deleteVariety();
            }
        }
        return View::make('master.variety')
                        ->withToogle('master')->withAktif('variety');
    }

    public function insertVariety() {
        $data = Input::all();
        $rule = array(
            "VarietyID" => 'required|max:200|unique:m_variety,VarietyID,NULL,VarietyID,CompanyInternalID,' . Auth::user()->Company->InternalID . '',
            "VarietyName" => 'required',
            "GroupInternalID" => 'required',
            "remark" => 'required'
        );
        $validator = Validator::make($data, $rule);
        if ($validator->Fails()) {
            return View::make('master.variety')
                            ->withMessages("gagalInsert")
                            ->withErrors($validator->messages())
                            ->withToogle('master')->withAktif('variety');
        } else {
            $variety = new Variety();
            $variety->VarietyID = Input::get('VarietyID');
            $variety->VarietyName = Input::get('VarietyName');
            $variety->GroupInternalID = Input::get('GroupInternalID');
            $variety->Remark = Input::get('remark');
            $variety->CompanyInternalID = Auth::user()->Company->InternalID;
            $variety->UserModified = "0";
            $variety->UserRecord = Auth::user()->UserID;
            $variety->save();

            return View::make('master.variety')
                            ->withMessages("suksesInsert")
                            ->withToogle('master')->withAktif('variety');
        }
    }

    public function updateVariety() {
        $data = Input::all();
        $rule = array(
            "VarietyName" => 'required',
            "GroupInternalID" => 'required',
            "remark" => 'required'
        );
        $validator = Validator::make($data, $rule);
        if ($validator->Fails()) {
            return View::make('master.variety')
                            ->withMessages("gagalInsert")
                            ->withErrors($validator->messages())
                            ->withToogle('master')->withAktif('variety');
        } else {
            $variety = Variety::find(Input::get('InternalID'));
            $variety->VarietyName = Input::get('VarietyName');
            $variety->GroupInternalID = Input::get('GroupInternalID');
            $variety->Remark = Input::get('remark');
            $variety->CompanyInternalID = Auth::user()->Company->InternalID;
            $variety->UserModified = Auth::user()->UserID;
            $variety->save();

            return View::make('master.variety')
                            ->withMessages("suksesUpdate")
                            ->withToogle('master')->withAktif('variety');
        }
    }

    public function deleteVariety() {
        $Variety = DB::table('m_inventory')->where('VarietyInternalID', Input::get('InternalID'))->first();
        if (is_null($Variety)) {
            //tidak ada, maka boleh hapus
            $variety = Variety::find(Input::get('InternalID'));
            if ($variety->CompanyInternalID == Auth::user()->Company->InternalID) {
                $variety->delete();
                return View::make('master.variety')
                                ->withMessages('suksesDelete')
                                ->withToogle('master')->withAktif('variety');
            } else {
                return View::make('master.variety')
                                ->withMessages('accessDenied')
                                ->withToogle('master')->withAktif('variety');
            }
        } else {
            //ada maka tidak dihapus
            return View::make('master.variety')
                            ->withMessages('gagalDelete')
                            ->withToogle('master')->withAktif('variety');
        }
    }

    public function exportVariety() {
        Excel::create('Master_Variety', function($excel) {
            $excel->sheet('Master_Variety', function($sheet) {
                $sheet->mergeCells('B1:H1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Master Variety");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Variety ID");
                $sheet->setCellValueByColumnAndRow(3, 2, "Variety Name");
                $sheet->setCellValueByColumnAndRow(4, 2, "Group Name");
                $sheet->setCellValueByColumnAndRow(5, 2, "Record");
                $sheet->setCellValueByColumnAndRow(6, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(7, 2, "Remark");
                $row = 3;
                foreach (Variety::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, "`" . $data->VarietyID);
                    $sheet->setCellValueByColumnAndRow(3, $row, $data->VarietyName);
                    $sheet->setCellValueByColumnAndRow(4, $row, $data->Group->GroupName);
                    $sheet->setCellValueByColumnAndRow(5, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(6, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(7, $row, $data->Remark);
                    $row++;
                }

                if (Variety::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() <= 0) {
                    $sheet->mergeCells('B3:H3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "No data available in table");

                    $sheet->cells('B3:H3', function($cells) {
                        $cells->setAlignment('center');
                    });
                    $sheet->setBorder('B3:H' . $row, 'thin');
                }

                $row--;
                $sheet->setBorder('B2:H' . $row, 'thin');
                $sheet->cells('B2:H2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:H' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
            });
        })->export('xls');
    }

}
