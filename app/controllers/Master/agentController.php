<?php

class AgentController extends BaseController {

    public function showAgent() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertAgent') {
                return $this->insertAgent();
            }
            if (Input::get('jenis') == 'updateAgent') {
                return $this->updateAgent();
            }
            if (Input::get('jenis') == 'activeAgent') {
                return $this->activeAgent();
            }
            if (Input::get('jenis') == 'nonActiveAgent') {
                return $this->nonActiveAgent();
            }
        }
        return View::make('master.agent')
                        ->withToogle('master')->withAktif('agent');
    }

    public function showAgentWithdraw() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'completedWithdraw') {
                return $this->completedWithdraw();
            }
        }
        return View::make('agent.agentWithdraw')
                        ->withToogle('master')->withAktif('agentWithdraw');
    }

    static function completedWithdraw() {
        $withdraw = Withdraw::find(Input::get('InternalID'));
        if (Auth::user()->Company->InternalID == '-1') {
            $withdraw->Status = 1;
            $withdraw->save();

            $agent = User::find($withdraw->AgentInternalID);
            $data["UserID"] = $agent->UserID;
            $data["UserName"] = $agent->UserName;
            $data["Email"] = $agent->Email;
            $data["Phone"] = $agent->Phone;
            $data["AccountNumber"] = $agent->AccountNumber;
            $data["AccountName"] = $agent->AccountName;
            $data["BankAccount"] = $agent->BankAccount;

            $data["WithdrawDate"] = date('d M Y', strtotime($withdraw->WithdrawDate));
            $data["WithdrawID"] = $withdraw->WithdrawID;
            $data["WithdrawNominal"] = number_format($withdraw->WithdrawNominal, 2, '.', ',');

            Mail::send('emails.emailCompleteWithdraw', $data, function($message) {
                $withdraw = Withdraw::find(Input::get('InternalID'));
                $agent = User::find($withdraw->AgentInternalID);
                $message->to($agent->Email, $agent->UserName)->subject('Completed Withdraw - ' . $agent->UserID . ' ');
            });
            return View::make('agent.agentWithdraw')
                            ->withMessages('suksesUpdate')
                            ->withToogle('master')->withAktif('agentWithdraw');
        } else { //bukan se company area dan bukan super admin
            return View::make('agent.agentWithdraw')
                            ->withMessages('accessDenied')
                            ->withToogle('master')->withAktif('agentWithdraw');
        }
    }

    public static function insertAgent() {
        //rule
        $rule = array(
            'AgentID' => 'required|max:16|unique:m_user,UserID',
            'Name' => 'required|max:200',
            'Password' => 'required|max:16',
            'Phone' => 'required|max:200',
            'AccountName' => 'required|max:200',
            'AccountNumber' => 'required|max:200',
            'BankAccount' => 'required|max:200',
            'Email' => 'required|email|max:200|unique:m_user,Email',
            'remark' => 'required|max:1000'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('master.agent')
                            ->withMessages('gagalInsert')
                            ->withError($validator->messages())
                            ->withToogle('master')->withAktif('agent')
                            ->withErrors($validator);
        } else {
            //valid
            $user = new User;
            $user->UserID = Input::get('AgentID');
            $user->UserName = Input::get('Name');
            $user->Phone = Input::get('Phone');
            $user->AccountName = Input::get('AccountName');
            $user->AccountNumber = Input::get('AccountNumber');
            $user->BankAccount = Input::get('BankAccount');
            $user->Email = Input::get('Email');
            $user->UserPwd = Hash::make(Input::get('Password'));
            $user->CompanyInternalID = -2;
            $user->Picture = NULL;
            $user->Status = 1;
            $user->UserRecord = Auth::user()->UserID;
            $user->UserModified = "0";
            $user->Remark = Input::get('remark');
            $user->save();

            return View::make('master.agent')
                            ->withMessages('suksesInsert')
                            ->withToogle('master')->withAktif('agent');
        }
    }

    static function updateAgent() {
        //rule
        $rule = array(
            'Name' => 'required|max:200',
            'remark' => 'required|max:1000',
            'Phone' => 'required|max:200',
            'AccountName' => 'required|max:200',
            'AccountNumber' => 'required|max:200',
            'BankAccount' => 'required|max:200'
        );
        //cek Email sama enggak
        $emailLama = User::find(Input::get('InternalID'))->Email;
        $data = Input::all();
        if (strtoupper(Input::get('Email')) == strtoupper($emailLama)) {
            $rule2 = array(
                'Email' => 'required|max:200|email'
            );
            $rule = array_merge($rule, $rule2);
        } else {
            $rule2 = array(
                'Email' => 'required|max:200|email|unique:m_user,Email'
            );
            $rule = array_merge($rule, $rule2);
        }

        //validasi
        $validator = Validator::make(Input::all(), $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('master.agent')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('master')->withAktif('agent');
        } else {
            //valid
            $user = User::find(Input::get('InternalID'));
            if ($user->CompanyInternalID == Auth::user()->Company->InternalID || Auth::user()->Company->InternalID == '-1') {
                $user->UserName = Input::get('Name');
                $user->CompanyInternalID = -2;
                $user->UserModified = Auth::user()->UserID;
                $user->Phone = Input::get('Phone');
                $user->AccountName = Input::get('AccountName');
                $user->AccountNumber = Input::get('AccountNumber');
                $user->BankAccount = Input::get('BankAccount');
                $user->Email = Input::get('Email');
                $user->Remark = Input::get('remark');
                $user->save();
                return View::make('master.agent')
                                ->withMessages('suksesUpdate')
                                ->withToogle('master')->withAktif('agent');
            } else {
                return View::make('master.agent')
                                ->withMessages('accessDenied')
                                ->withToogle('master')->withAktif('agent');
            }
        }
    }

    static function nonActiveAgent() {
        $user = User::find(Input::get('InternalID'));
        if ($user->CompanyInternalID == Auth::user()->Company->InternalID || Auth::user()->Company->InternalID == '-1') {

            $user->Status = 0;
            $user->save();

            return View::make('master.agent')
                            ->withMessages('suksesUpdate')
                            ->withToogle('master')->withAktif('agent');
        } else { //bukan se company area dan bukan super admin
            return View::make('master.agent')
                            ->withMessages('accessDenied')
                            ->withToogle('master')->withAktif('agent');
        }
    }

    static function activeAgent() {
        $user = User::find(Input::get('InternalID'));
        if ($user->CompanyInternalID == Auth::user()->Company->InternalID || Auth::user()->Company->InternalID == '-1') {

            $user->Status = 1;
            $user->save();

            return View::make('master.agent')
                            ->withMessages('suksesUpdate')
                            ->withToogle('master')->withAktif('agent');
        } else { //bukan se company area dan bukan super admin
            return View::make('master.agent')
                            ->withMessages('accessDenied')
                            ->withToogle('master')->withAktif('agent');
        }
    }

    public function exportAgent() {
        Excel::create('Master_Agent', function($excel) {
            $excel->sheet('Master_Agent', function($sheet) {
                $sheet->mergeCells('B1:I1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Master Agent");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Agent Name");
                $sheet->setCellValueByColumnAndRow(3, 2, "User ID");
                $sheet->setCellValueByColumnAndRow(4, 2, "Email");
                $sheet->setCellValueByColumnAndRow(5, 2, "Phone");
                $sheet->setCellValueByColumnAndRow(6, 2, "Record");
                $sheet->setCellValueByColumnAndRow(7, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(8, 2, "Remark");
                $row = 3;
                $userData = User::where('CompanyInternalID', -2)->get();
                foreach ($userData as $data) {
                    $company = User::find($data->InternalID)->Company;
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, $data->UserName);
                    $sheet->setCellValueByColumnAndRow(3, $row, "`" . $data->UserID);
                    $sheet->setCellValueByColumnAndRow(4, $row, $data->Email);
                    $sheet->setCellValueByColumnAndRow(5, $row, '`' . $data->Phone);
                    $sheet->setCellValueByColumnAndRow(6, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(7, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(8, $row, $data->Remark);
                    $row++;
                }

                if (User::where('CompanyInternalID', -2)->count() <= 0) {
                    $sheet->mergeCells('B3:I3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "No data available in table");

                    $sheet->cells('B3:I3', function($cells) {
                        $cells->setAlignment('center');
                    });

                    $sheet->setBorder('B3:I' . $row, 'thin');
                }

                $row--;
                $sheet->setBorder('B2:I' . $row, 'thin');
                $sheet->cells('B2:I2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:I' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
            });
        })->export('xls');
    }

}
