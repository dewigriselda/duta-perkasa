<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class GroupController extends BaseController {

    public function showGroup() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertGroup') {
                return $this->insertGroup();
            }
            if (Input::get('jenis') == 'updateGroup') {
                return $this->updateGroup();
            }
            if (Input::get('jenis') == 'deleteGroup') {
                return $this->deleteGroup();
            }
        }
        return View::make('master.group')
                        ->withToogle('master')->withAktif('group');
    }

    public function insertGroup() {
        $data = Input::all();
        $rule = array(
            "GroupID" => 'required|max:200|unique:m_group,GroupID,NULL,GroupID,CompanyInternalID,' . Auth::user()->Company->InternalID . '',
            "GroupName" => 'required',
            "remark" => 'required'
        );
        $validator = Validator::make($data, $rule);
        if ($validator->Fails()) {
            return View::make('master.group')
                            ->withMessages("gagalInsert")
                            ->withErrors($validator->messages())
                            ->withToogle('master')->withAktif('group');
        } else {
            $group = new Group();
            $group->GroupID = Input::get('GroupID');
            $group->GroupName = Input::get('GroupName');
            $group->Remark = Input::get('remark');
            $group->CompanyInternalID = Auth::user()->Company->InternalID;
            $group->UserModified = "0";
            $group->UserRecord = Auth::user()->UserID;
            $group->save();

            return View::make('master.group')
                            ->withMessages("suksesInsert")
                            ->withToogle('master')->withAktif('group');
        }
    }

    public function updateGroup() {
        $data = Input::all();
        $rule = array(
            "GroupName" => 'required',
            "remark" => 'required'
        );
        $validator = Validator::make($data, $rule);
        if ($validator->Fails()) {
            return View::make('master.group')
                            ->withMessages("gagalInsert")
                            ->withErrors($validator->messages())
                            ->withToogle('master')->withAktif('group');
        } else {
            $group = Group::find(Input::get('InternalID'));
            $group->GroupName = Input::get('GroupName');
            $group->Remark = Input::get('remark');
            $group->CompanyInternalID = Auth::user()->Company->InternalID;
            $group->UserModified = Auth::user()->UserID;
            $group->save();

            return View::make('master.group')
                            ->withMessages("suksesUpdate")
                            ->withToogle('master')->withAktif('group');
        }
    }

    public function deleteGroup() {
        $variety = DB::table('m_variety')->where('GroupInternalID', Input::get('InternalID'))->first();
        if (is_null($variety)) {
            //tidak ada, maka boleh hapus
            $group = Group::find(Input::get('InternalID'));
            if ($group->CompanyInternalID == Auth::user()->Company->InternalID) {
                $group->delete();
                return View::make('master.group')
                                ->withMessages('suksesDelete')
                                ->withToogle('master')->withAktif('group');
            } else {
                return View::make('master.group')
                                ->withMessages('accessDenied')
                                ->withToogle('master')->withAktif('group');
            }
        } else {
            //ada maka tidak dihapus
            return View::make('master.group')
                            ->withMessages('gagalDelete')
                            ->withToogle('master')->withAktif('group');
        }
    }

    public function exportGroup() {
        Excel::create('Master_Group', function($excel) {
            $excel->sheet('Master_Group', function($sheet) {
                $sheet->mergeCells('B1:G1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Master Group");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Group ID");
                $sheet->setCellValueByColumnAndRow(3, 2, "Group Name");
                $sheet->setCellValueByColumnAndRow(4, 2, "Record");
                $sheet->setCellValueByColumnAndRow(5, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(6, 2, "Remark");
                $row = 3;
                foreach (Group::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, "`" . $data->GroupID);
                    $sheet->setCellValueByColumnAndRow(3, $row, $data->GroupName);
                    $sheet->setCellValueByColumnAndRow(4, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(5, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(6, $row, $data->Remark);
                    $row++;
                }

                if (Group::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() <= 0) {
                    $sheet->mergeCells('B3:G3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "No data available in table");

                    $sheet->cells('B3:G3', function($cells) {
                        $cells->setAlignment('center');
                    });
                    $sheet->setBorder('B3:G' . $row, 'thin');
                }

                $row--;
                $sheet->setBorder('B2:G' . $row, 'thin');
                $sheet->cells('B2:G2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:G' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
            });
        })->export('xls');
    }

}
