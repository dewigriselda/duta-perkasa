<?php

class InventoryController extends BaseController {

    public function showInventory() {
        $yearMin = JournalHeader::getYearMin();
        $yearMax = date("Y");
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertInventory') {
                return $this->insertInventory();
            }
            if (Input::get('jenis') == 'updateInventory') {
                return $this->updateInventory();
            }
            if (Input::get('jenis') == 'deleteInventory') {
                return $this->deleteInventory();
            }
            if (Input::get('jenis') == 'detailInventory') {
//                return $this->detailInventory();
//                return $this->detailInventoryHtml();
                return $this->detailInventoryExcel();
            }
            if (Input::get('jenis') == 'summaryInventory') {
                return $this->summaryView();
//                return $this->summaryInventory();
            }
            if (Input::get('jenis') == 'stockInventory') {
                return $this->stockInventory();
            }
            if (Input::get('jenis') == 'reportStock') {
                return $this->reportStock();
            }
            if (Input::get('jenis') == 'importInventory') {
                return $this->importInventory();
            }
        }
        return View::make('master.inventory')
                        ->withToogle('master')->withAktif('inventory')
                        ->withYearmin($yearMin)
                        ->withYearmax($yearMax);
    }

    public function importInventory() {
        if (Input::hasFile('file')) {
            Excel::load(Input::file('file'), function($reader) {

                $filename = $_FILES['file']['name'];
                $codeID = explode(' ', $filename)[0];

                $result = $reader->select(array('code', 'cat', 'model_no', 'desc', 'power', 'de',
                            'j5', 'ae37', 'af43', 'mt', 'dt', 'brand', 'list'))->get();

                foreach ($result as $row) {
                    if ($row['code'] != '' && $row['model_no'] != '0' && $row['model_no'] != '') {
                        //CEK INVENTORY UDAH ADA ATAU BELUM
                        $codehuruf = preg_replace('/[0-9]/', '', $codeID);
                        $codeangka = str_pad((int) $row['code'], 4, '0', STR_PAD_LEFT);
                        $code = $codehuruf . $codeangka;
                        $cekinv = Inventory::where('InventoryID', $code)->count();
                        //kalau ada (Update harga dan remark)
                        if ($cekinv > 0) {
                            $inv = Inventory::where('InventoryID', $code)->first();
                            $inv->Power = !empty($row['power']) ? $row['power'] : '0';
                            $inv->Remark = !empty($row['desc']) ? $row['desc'] : '-';
                            $inv->save();
                            $updateinv = InventoryUom::where('InventoryInternalID', $inv->InternalID)->where('Default', 1)->first();
                            $updateinv->PriceA = str_replace(",", "", $row['list']);
                            $updateinv->Power = !empty($row['power']) ? $row['power'] : '0';
                            $updateinv->save();
                        }
                        //kalau tidak ada (Buat baru)
                        else {
                            //cek Group
                            $cekgroup = Group::where('GroupID', $codehuruf)->count();
                            if ($cekgroup == 0) {
                                $group = new Group();
                                $group->GroupID = $codehuruf;
                                $group->GroupName = $codehuruf;
                                $group->Remark = '-';
                                $group->CompanyInternalID = Auth::user()->Company->InternalID;
                                $group->UserRecord = Auth::user()->UserID;
                                $group->UserModified = "0";
                                $group->save();
                            } else {
                                $group = Group::where('GroupID', $codehuruf)->first();
                            }

                            //cek Variety
                            $cekvar = Variety::where('VarietyID', $row['cat'])->count();
                            if ($cekvar == 0) {
                                $variety = new Variety();
                                $variety->VarietyID = $row['cat'];
                                $variety->VarietyName = $row['cat'];
                                $variety->GroupInternalID = $group->InternalID;
                                $variety->Remark = '-';
                                $variety->CompanyInternalID = Auth::user()->Company->InternalID;
                                $variety->UserRecord = Auth::user()->UserID;
                                $variety->UserModified = "0";
                                $variety->save();
                            } else {
                                $variety = Variety::where('VarietyID', $row['cat'])->first();
                            }

                            //cek Brand
                            $cekbrand = Brand::where('BrandID', $row['brand'])->count();
                            if ($cekbrand == 0) {
                                $brand = new Brand();
                                $brand->BrandID = $row['brand'];
                                $brand->BrandName = $row['brand'];
                                $brand->Remark = '-';
                                $brand->CompanyInternalID = Auth::user()->Company->InternalID;
                                $brand->UserRecord = Auth::user()->UserID;
                                $brand->UserModified = "0";
                                $brand->save();
                            } else {
                                $brand = Brand::where('BrandID', $row['brand'])->first();
                            }

                            //Insert Inventory
                            $inventory = new Inventory;
                            $inventory->InventoryID = $code;
                            //5 = Persediaan
                            $inventory->InventoryTypeInternalID = 5;
                            $inventory->InventoryName = $row['model_no'];
                            $inventory->UoM = 'PCS';
                            $inventory->LeadTime = 0;
                            $inventory->UsageInventory = 0;
                            $inventory->MaxStock = 1000;
                            $inventory->MinStock = 1;
                            $inventory->GroupInternalID = $group->InternalID;
                            $inventory->VarietyInternalID = $variety->InternalID;
                            $inventory->BrandInternalID = $brand->InternalID;
                            $inventory->UserRecord = Auth::user()->UserID;
                            $inventory->CompanyInternalID = Auth::user()->Company->InternalID;
                            $inventory->UserModified = "0";
                            $inventory->Power = !empty($row['power']) ? $row['power'] : '0';
                            $inventory->Remark = !empty($row['desc']) ? $row['desc'] : '-';
                            $inventory->save();

                            //create inventory uom langsung
                            $inventoryUom = new InventoryUom();
                            $inventoryUom->InventoryInternalID = $inventory->InternalID;
                            //16 = PCS
                            $inventoryUom->UomInternalID = 16;
                            $inventoryUom->PriceA = str_replace(",", "", $row['list']);
                            $inventoryUom->PriceB = 0;
                            $inventoryUom->PriceC = 0;
                            $inventoryUom->PriceD = 0;
                            $inventoryUom->PriceE = 0;
                            $inventoryUom->Default = 1;
                            $inventoryUom->Value = 1;
                            $inventoryUom->Power = !empty($row['power']) ? $row['power'] : '0';
                            $inventoryUom->Status = 0;
                            $inventoryUom->UserRecord = Auth::user()->UserID;
                            $inventoryUom->UserModified = "0";
                            $inventoryUom->CompanyInternalID = Auth::user()->Company->InternalID;
                            $inventoryUom->Remark = '-';
                            $inventoryUom->save();
                        }
                    }
                }
            });
        }
        return Redirect::route('showInventory');
    }

    public static function insertInventory() {
        $yearMin = JournalHeader::getYearMin();
        $yearMax = date("Y");
//rule
        $rule = array(
            'InventoryID' => 'required|max:200|unique:m_inventory,InventoryID,NULL,InventoryID,CompanyInternalID,' . Auth::user()->Company->InternalID . '',
            'Type' => 'required',
//            'InitialValue' => 'required',
//            'Palen' => 'required',
//            'Konsinyasi' => 'required',
            'MaxStock' => 'required',
            'MinStock' => 'required',
            'ValueDefault' => 'required',
            'UoMDefault' => 'required',
            'LeadTime' => 'required',
            'Usage' => 'required',
//            'InitialQuantity' => 'required',
//            'BarcodeCode' => 'required|max:13',
            'VarietyInternalID' => 'required',
            'BrandInternalID' => 'required',
            'InventoryName' => 'required|max:200'
        );

//validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
//tidak valid
            return View::make('master.inventory')
                            ->withMessages('gagalInsert')
                            ->withError($validator->messages())
                            ->withYearmin($yearMin)
                            ->withYearmax($yearMax)
                            ->withToogle('master')->withAktif('inventory')
                            ->withErrors($validator);
        } else {
//valid
            $inventory = new Inventory;
            $inventory->InventoryID = Input::get('InventoryID');
            $inventory->InventoryTypeInternalID = Input::get('Type');
            $inventory->InventoryName = Input::get('InventoryName');
            $inventory->UoM = Uom::find(Input::get('UoMDefault'))->UomID;
            $inventory->Palen = 0;
            $inventory->LeadTime = Input::get('LeadTime');
            $inventory->UsageInventory = Input::get('Usage');
//            $inventory->Konsinyasi = Input::get('Konsinyasi');
//            $inventory->InitialValue = str_replace(',', '', Input::get('InitialValue'));
//            $inventory->InitialQuantity = str_replace(',', '', Input::get('InitialQuantity'));
            $inventory->MaxStock = str_replace(',', '', Input::get('MaxStock'));
            $inventory->MinStock = str_replace(',', '', Input::get('MinStock'));
            $inventory->BarcodeCode = Input::get('BarcodeCode');
            $inventory->Power = Input::get('power');
            $inventory->GroupInternalID = Variety::find(Input::get('VarietyInternalID'))->GroupInternalID;
            $inventory->VarietyInternalID = Input::get('VarietyInternalID');
            $inventory->BrandInternalID = Input::get('BrandInternalID');
            $inventory->UserRecord = Auth::user()->UserID;
            $inventory->CompanyInternalID = Auth::user()->Company->InternalID;
            $inventory->UserModified = "0";
            $inventory->Description = Input::get('description');
            $inventory->Remark = Input::get('remark');
            $inventory->save();

//insert inventory child
            if (Input::get('InventoryInternalID') != NULL) {
                $i = 0;
                $uom = Input::get('inventoryUom');
                $qty = Input::get('qty');
                foreach (Input::get('InventoryInternalID') as $child) {
                    $childInv = new InventoryChild();
                    $childInv->InventoryInternalID = $inventory->InternalID;
                    $childInv->ChildInventoryInternalID = $child;
                    $childInv->UomInternalID = $uom[$i];
                    $childInv->Qty = $qty[$i];
                    $childInv->CompanyInternalID = Auth::user()->Company->InternalID;
                    $childInv->UserRecord = Auth::user()->UserID;
                    $childInv->save();
                    $i++;
                }
            }

//create inventory uom langsung
            $inventoryUom = new InventoryUom();
            $inventoryUom->InventoryInternalID = $inventory->InternalID;
            $inventoryUom->UomInternalID = Input::get('UoMDefault');
            $inventoryUom->PriceA = 0;
            $inventoryUom->PriceB = 0;
            $inventoryUom->PriceC = 0;
            $inventoryUom->PriceD = 0;
            $inventoryUom->PriceE = 0;
            $inventoryUom->Default = 1;
            $inventoryUom->Value = str_replace(',', '', Input::get('ValueDefault'));
            $inventoryUom->Status = 0;
            $inventoryUom->UserRecord = Auth::user()->UserID;
            $inventoryUom->UserModified = "0";
            $inventoryUom->CompanyInternalID = Auth::user()->Company->InternalID;
            $inventoryUom->Remark = '-';
            $inventoryUom->save();

            if (Input::get("ValueMedium") != "" && (Input::get('UoMMedium') != Input::get('UoMBig') ) && (Input::get('UoMMedium') != Input::get('UoMDefault') )) {
//create inventory uom langsung
                $inventoryUom = new InventoryUom();
                $inventoryUom->InventoryInternalID = $inventory->InternalID;
                $inventoryUom->UomInternalID = Input::get('UoMMedium');
                $inventoryUom->PriceA = 0;
                $inventoryUom->PriceB = 0;
                $inventoryUom->PriceC = 0;
                $inventoryUom->PriceD = 0;
                $inventoryUom->PriceE = 0;
                $inventoryUom->Default = 0;
                $inventoryUom->Value = str_replace(',', '', Input::get('ValueMedium'));
                $inventoryUom->Status = 0;
                $inventoryUom->UserRecord = Auth::user()->UserID;
                $inventoryUom->UserModified = "0";
                $inventoryUom->CompanyInternalID = Auth::user()->Company->InternalID;
                $inventoryUom->Remark = '-';
                $inventoryUom->save();
            }
            if (Input::get("ValueBig") != "" && (Input::get('UoMMedium') != Input::get('UoMBig') ) && (Input::get('UoMBig') != Input::get('UoMDefault') )) {
//create inventory uom langsung
                $inventoryUom = new InventoryUom();
                $inventoryUom->InventoryInternalID = $inventory->InternalID;
                $inventoryUom->UomInternalID = Input::get('UoMBig');
                $inventoryUom->PriceA = 0;
                $inventoryUom->PriceB = 0;
                $inventoryUom->PriceC = 0;
                $inventoryUom->PriceD = 0;
                $inventoryUom->PriceE = 0;
                $inventoryUom->Default = 0;
                $inventoryUom->Value = str_replace(',', '', Input::get('ValueBig'));
                $inventoryUom->Status = 0;
                $inventoryUom->UserRecord = Auth::user()->UserID;
                $inventoryUom->UserModified = "0";
                $inventoryUom->CompanyInternalID = Auth::user()->Company->InternalID;
                $inventoryUom->Remark = '-';
                $inventoryUom->save();
            }

            return View::make('master.inventory')
                            ->withMessages('suksesInsert')
                            ->withYearmin($yearMin)
                            ->withYearmax($yearMax)
                            ->withToogle('master')->withAktif('inventory');
        }
    }

    static function updateInventory() {
        $yearMin = JournalHeader::getYearMin();
        $yearMax = date("Y");
//rule
        $rule = array(
            'InventoryName' => 'required|max:200',
//            'BarcodeCode' => 'required|max:13',
            'Type' => 'required',
            'LeadTime' => 'required',
            'Usage' => 'required',
//            'Palen' => 'required',
//            'Konsinyasi' => 'required',
            'MaxStock' => 'required',
            'MinStock' => 'required',
            'VarietyInternalID' => 'required',
            'BrandInternalID' => 'required'
        );
//validasi
        $validator = Validator ::make(Input::all(), $rule);
        if ($validator->fails()) {
//tidak valid
            return View::make('master.inventory')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withYearmin($yearMin)
                            ->withYearmax($yearMax)
                            ->withToogle('master')->withAktif('inventory');
        } else {
//valid
            $inventory = Inventory::find(Input::get('InternalID'));
            if ($inventory->CompanyInternalID == Auth::user()->Company->InternalID) {
                $inventory->InventoryTypeInternalID = Input::get('Type');
                $inventory->InventoryName = Input::get('InventoryName');
                $inventory->BarcodeCode = Input::get('BarcodeCode');
//                $inventory->UoM = Input::get('UoM');
                $inventory->Palen = 0;
                $inventory->LeadTime = Input::get('LeadTime');
                $inventory->UsageInventory = Input::get('Usage');
//                $inventory->Konsinyasi = Input::get('Konsinyasi');
//delete anak lama
//                $delChild = Inventory::where('InventoryInternalID', Input::get('InternalID'))->get();
//                foreach ($delChild as $del) {
//                    $del->InventoryInternalID = NULL;
//                    $del->save();
//                }
                $delChild = InventoryChild::where('InventoryInternalID', Input::get('InternalID'))->delete();
//                //insert inventory child
//                if (Input::get('InventoryInternalID') != NULL) {
//                    foreach (Input::get('InventoryInternalID') as $child) {
//                        $childInv = Inventory::find($child);
//                        $childInv->InventoryInternalID = $inventory->InternalID;
//                        $childInv->save();
//                    }
//                }
//insert inventory child
                if (Input::get('InventoryInternalID') != NULL) {
                    $i = 0;
                    $uom = Input::get('inventoryUom');
                    $qty = Input::get('qty');
                    foreach (Input::get('InventoryInternalID') as $child) {
                        $childInv = new InventoryChild();
                        $childInv->InventoryInternalID = $inventory->InternalID;
                        $childInv->ChildInventoryInternalID = $child;
                        $childInv->UomInternalID = $uom[$i];
                        $childInv->Qty = $qty[$i];
                        $childInv->CompanyInternalID = Auth::user()->Company->InternalID;
                        $childInv->UserRecord = Auth::user()->UserID;
                        $childInv->save();
                        $i++;
                    }
                }
                $inventory->MaxStock = str_replace(',', '', Input::get('MaxStock'));
                $inventory->MinStock = str_replace(',', '', Input::get('MinStock'));
                $inventory->GroupInternalID = Variety::find(Input::get('VarietyInternalID'))->GroupInternalID;
                $inventory->VarietyInternalID = Input::get('VarietyInternalID');
                $inventory->BrandInternalID = Input::get('BrandInternalID');
                $inventory->Power = Input::get('power');
                $inventory->UserModified = Auth::user()->UserID;
                $inventory->Description = Input::get('description');
                $inventory->Remark = Input::get('remark');
                $inventory->save();
                return View::make('master.inventory')
                                ->withMessages('suksesUpdate')
                                ->withYearmin($yearMin)
                                ->withYearmax($yearMax)
                                ->withToogle('master')->withAktif('inventory');
            } else {
                return View::make('master.inventory')
                                ->withMessages('accessDenied')
                                ->withYearmin($yearMin)
                                ->withYearmax($yearMax)
                                ->withToogle('master')->withAktif('inventory');
            }
        }
    }

    static function deleteInventory() {
        $yearMin = JournalHeader::getYearMin();
        $yearMax = date("Y");
//cek apakah ID Inventory ada di tabel sales detail atau tidak
        $sales = DB::table('t_sales_detail')->where('InventoryInternalID', Input::get('InternalID'))->first();
//cek apakah ID Inventory ada di tabel sales return detail atau tidak
        $salesReturn = DB::table('t_salesreturn_detail')->where('InventoryInternalID', Input::get('InternalID'))->first();
//cek apakah ID Inventory ada di tabel sales order detail atau tidak
        $salesOrder = DB::table('t_salesorder_detail')->where('InventoryInternalID', Input::get('InternalID'))->first();
//cek apakah ID Inventory ada di tabel purchase detail atau tidak
        $purchase = DB::table('t_purchase_detail')->where('InventoryInternalID', Input::get('InternalID'))->first();
//cek apakah ID Inventory ada di tabel purchase return detail atau tidak
        $purchaseReturn = DB::table('t_purchasereturn_detail')->where('InventoryInternalID', Input::get('InternalID'))->first();
//cek apakah ID Inventory ada di tabel purchase order detail atau tidak
        $purchaseOrder = DB::table('t_purchaseorder_detail')->where('InventoryInternalID', Input::get('InternalID'))->first();
//cek apakah ID Inventory ada di tabel purchase memoin atau tidak
        $memoIn = DB::table('t_memoin_detail')->where('InventoryInternalID', Input::get('InternalID'))->first();
//cek apakah ID Inventory ada di tabel purchase memoout atau tidak
        $memoOut = DB::table('t_memoout_detail')->where('InventoryInternalID', Input::get('InternalID'))->first();
//cek apakah ID Inventory ada di tabel purchase transfer atau tidak
        $transfer = DB::table('t_transfer_detail')->where('InventoryInternalID', Input::get('InternalID'))->first();
//cek apakah ID Inventory ada di tabel parcel atau tidak
        $parcel = DB::table('m_parcel_inventory')->where('InventoryInternalID', Input::get('InternalID'))->first();
//cek apakah ID Inventory ada di tabel diskon atau tidak
        $diskon = DB::table('h_discount')->where('ChildInternalID', Input::get('InternalID'))->where('Parameter', 'inventory')->first();
        if (is_null($sales) && is_null($salesReturn) && is_null($salesOrder) && is_null($purchase) && is_null($purchaseReturn) && is_null($purchaseOrder) && is_null($memoIn) && is_null($memoOut) && is_null($transfer) && is_null($parcel) && is_null($diskon)) {
//tidak ada maka boleh dihapus
            $inventory = Inventory::find(Input::get('InternalID'));
            if ($inventory->CompanyInternalID == Auth::user()->Company->InternalID) {
                if ($inventory->Status == 0) {
//hapus inventory uom dulu
                    $inventoryUom = DB::table('m_inventory_uom')->where('InventoryInternalID', Input::get('InternalID'))->delete();
                    $child = InventoryChild::where('InventoryInternalID', Input::get('InternalID'))->delete();
                    $inventory->delete();
                    return View::make('master.inventory')
                                    ->withMessages('suksesDelete')
                                    ->withYearmin($yearMin)
                                    ->withYearmax($yearMax)
                                    ->withToogle('master')->withAktif('inventory');
                } else {
                    return View::make('master.inventory')
                                    ->withMessages('gagalDelete')
                                    ->withYearmin($yearMin)
                                    ->withYearmax($yearMax)
                                    ->withToogle('master')->withAktif('inventory');
                }
            } else {
                return View::make('master.inventory')
                                ->withMessages('accessDenied')
                                ->withYearmin($yearMin)
                                ->withYearmax($yearMax)
                                ->withToogle('master')->withAktif('inventory');
            }
        } else {
//ada maka tidak dihapus
            return View::make('master.inventory')
                            ->withMessages('gagalDelete')
                            ->withYearmin($yearMin)
                            ->withYearmax($yearMax)
                            ->withToogle('master')->withAktif('inventory');
        }
    }

    public function pushDataToCatalog() {
        try {
            if (is_connected_to_salmon()) {
                $this->pushData();
                Session::flash("messages", "suksesPushData");
                return Redirect::Route("showInventory");
            }
        } catch (Exception $e) {
            Session::flash("messages", "gagalPushData");
            return Redirect::Route("showInventory");
        }
    }

    public function pushData() {
        $this->pushPrice();
//inventory
        foreach (Inventory::where("Status", 0)->where("CompanyInternalID", Auth::user()->company->InternalID)->get() as $dataInventory) {
            $inventoryCheck = InventoryKatalog::where("InternalID", $dataInventory->InternalID)
                    ->where("InventoryID", $dataInventory->InventoryID)
                    ->where("CompanyInternalID", Auth::user()->company->InternalID)
                    ->count();
            if ($inventoryCheck == 0) {
//kalo tidak ada maka insert baru
                $inventoryKatalog = new InventoryKatalog();
                $inventoryKatalog->InternalID = $dataInventory->InternalID;
                $inventoryKatalog->InventoryID = $dataInventory->InventoryID;
                $inventoryKatalog->InventoryName = $dataInventory->InventoryName;
                $inventoryKatalog->UoM = $dataInventory->UoM;
                $inventoryKatalog->Palen = $dataInventory->Palen;
                $inventoryKatalog->Konsinyasi = $dataInventory->Konsinyasi;
                $inventoryKatalog->InitialValue = $dataInventory->InitialValue;
                $inventoryKatalog->InitialQuantity = $dataInventory->InitialQuantity;
                $inventoryKatalog->MinStock = $dataInventory->MinStock;
                $inventoryKatalog->MaxStock = $dataInventory->MaxStock;
                $inventoryKatalog->BarcodeCode = $dataInventory->BarcodeCode;
                $inventoryKatalog->GroupInternalID = $dataInventory->GroupInternalID;
                $inventoryKatalog->VarietyInternalID = $dataInventory->VarietyInternalID;
                $inventoryKatalog->BrandInternalID = $dataInventory->BrandInternalID;
                $inventoryKatalog->CompanyInternalID = $dataInventory->CompanyInternalID;
                $inventoryKatalog->InventoryTypeInternalID = $dataInventory->InventoryTypeInternalID;
                $inventoryKatalog->UserRecord = Auth::user()->UserID;
                $inventoryKatalog->UserModified = Auth::user()->UserID;
                $inventoryKatalog->Remark = $dataInventory->Remark;
                $inventoryKatalog->save();

//ganti status setelah upload ke online
                $inventoryUpdate = Inventory::find($dataInventory->InternalID);
                $inventoryUpdate->Status = 1;
                $inventoryUpdate->save();
            } else {
//kalo ada maka update datanya    
                $inventoryKatalog = InventoryKatalog::where("InternalID", $dataInventory->InternalID)->where("InventoryID", $dataInventory->InventoryID)
                                ->where("CompanyInternalID", Auth::user()->company->InternalID)->first();
                $inventoryKatalog->InventoryName = $dataInventory->InventoryName;
                $inventoryKatalog->UoM = $dataInventory->UoM;
                $inventoryKatalog->InitialValue = $dataInventory->InitialValue;
                $inventoryKatalog->InitialQuantity = $dataInventory->InitialQuantity;
                $inventoryKatalog->GroupInternalID = $dataInventory->GroupInternalID;
                $inventoryKatalog->VarietyInternalID = $dataInventory->VarietyInternalID;
                $inventoryKatalog->BrandInternalID = $dataInventory->BrandInternalID;
                $inventoryKatalog->CompanyInternalID = $dataInventory->CompanyInternalID;
                $inventoryKatalog->InventoryTypeInternalID = $dataInventory->InventoryTypeInternalID;
                $inventoryKatalog->UserRecord = Auth::user()->UserID;
                $inventoryKatalog->UserModified = Auth::user()->UserID;
                $inventoryKatalog->Remark = $dataInventory->Remark;
                $inventoryKatalog->save();

//ganti status setelah upload ke online
                $inventoryUpdate = Inventory::find($dataInventory->InternalID);
                $inventoryUpdate->Status = 1;
                $inventoryUpdate->save();
            }
        }

//uom
        foreach (Uom::where("Status", 0)->where("CompanyInternalID", Auth::user()->company->InternalID)->get() as $dataUom) {
            $uomCheck = UomKatalog1::where("InternalID", $dataUom->InternalID)
                    ->where("UomID", $dataUom->UomID)
                    ->where("CompanyInternalID", Auth::user()->company->InternalID)
                    ->count();

            if ($uomCheck == 0) {
//tidak ada maka insert baru
                $uomKatalog = new UomKatalog1();
                $uomKatalog->InternalID = $dataUom->InternalID;
                $uomKatalog->UomID = $dataUom->UomID;
                $uomKatalog->CompanyInternalID = $dataUom->CompanyInternalID;
                $uomKatalog->Remark = $dataUom->Remark;
                $uomKatalog->UserRecord = Auth::user()->UserID;
                $uomKatalog->UserModified = Auth::user()->UserID;
                $uomKatalog->save();

//update uom offlinenya jd 1
                $uom = Uom::find($dataUom->InternalID);
                $uom->Status = 1;
                $uom->save();
            } else {
//ada maka update baru datanya
                $uomKatalog = UomKatalog1::where("InternalID", $dataUom->InternalID)
                                ->where("UomID", $dataUom->UomID)
                                ->where("CompanyInternalID", Auth::user()->company->InternalID)->first();
                $uomKatalog->CompanyInternalID = $dataUom->CompanyInternalID;
                $uomKatalog->UserRecord = Auth::user()->UserID;
                $uomKatalog->UserModified = Auth::user()->UserID;
                $uomKatalog->Remark = $dataUom->Remark;
                $uomKatalog->save();

//update uom offlinenya jd 1
                $uom = Uom::find($dataUom->InternalID);
                $uom->Status = 1;
                $uom->save();
            }
        }

//inventoryUom
        foreach (InventoryUom::where("CompanyInternalID", Auth::user()->company->InternalID)->get() as $dataInventoryUom) {
            $inventoryUomCheck = InventoryUomKatalog::where("InternalID", $dataInventoryUom->InternalID)
                    ->where("CompanyInternalID", Auth::user()->company->InternalID)
                    ->count();

            if ($inventoryUomCheck == 0) {
//jika tidak ada maka insert baru
                $inventoryUomKatalog = new InventoryUomKatalog();
                $inventoryUomKatalog->InternalID = $dataInventoryUom->InternalID;
                $inventoryUomKatalog->InventoryInternalID = $dataInventoryUom->InventoryInternalID;
                $inventoryUomKatalog->UomInternalID = $dataInventoryUom->UomInternalID;
                $inventoryUomKatalog->PriceA = $dataInventoryUom->PriceA;
                $inventoryUomKatalog->PriceB = $dataInventoryUom->PriceB;
                $inventoryUomKatalog->PriceC = $dataInventoryUom->PriceC;
                $inventoryUomKatalog->PriceD = $dataInventoryUom->PriceD;
                $inventoryUomKatalog->PriceE = $dataInventoryUom->PriceE;
                $inventoryUomKatalog->Default = $dataInventoryUom->Default;
                $inventoryUomKatalog->Value = $dataInventoryUom->Value;
                $inventoryUomKatalog->CompanyInternalID = $dataInventoryUom->CompanyInternalID;
                $inventoryUomKatalog->UserRecord = Auth::user()->UserID;
                $inventoryUomKatalog->UserModified = Auth::user()->UserID;
                $inventoryUomKatalog->Remark = $dataInventoryUom->Remark;
                $inventoryUomKatalog->save();

//update status jd sudah di upload
                $inventoryUom = InventoryUom::find($dataInventoryUom->InternalID);
                $inventoryUom->Status = 1;
                $inventoryUom->save();
            } else {
//jika ada maka update baru
                $inventoryUomKatalog = InventoryUomKatalog::where("InternalID", $dataInventoryUom->InternalID)
                                ->where("CompanyInternalID", Auth::user()->company->InternalID)->first();

                $inventoryUomKatalog->InventoryInternalID = $dataInventoryUom->InventoryInternalID;
                $inventoryUomKatalog->UomInternalID = $dataInventoryUom->UomInternalID;
                $inventoryUomKatalog->PriceA = $dataInventoryUom->PriceA;
                $inventoryUomKatalog->PriceB = $dataInventoryUom->PriceB;
                $inventoryUomKatalog->PriceC = $dataInventoryUom->PriceC;
                $inventoryUomKatalog->PriceD = $dataInventoryUom->PriceD;
                $inventoryUomKatalog->PriceE = $dataInventoryUom->PriceE;
                $inventoryUomKatalog->Default = $dataInventoryUom->Default;
                $inventoryUomKatalog->Value = $dataInventoryUom->Value;
                $inventoryUomKatalog->UserRecord = Auth::user()->UserID;
                $inventoryUomKatalog->UserModified = Auth::user()->UserID;
                $inventoryUomKatalog->Remark = $dataInventoryUom->Remark;
                $inventoryUomKatalog->save();

//update status jd sudah di upload
                $inventoryUom = InventoryUom::find($dataInventoryUom->InternalID);
                $inventoryUom->Status = 1;
                $inventoryUom->save();
            }
        }

//product UOM
        foreach (ProductKatalog::where("CompanyInternalID", Auth::user()->company->InternalID)->get() as $dataProductKatalog) {
            $checkColorProduct = ProductColorKatalog::where("ProductInternalID", $dataProductKatalog->InternalID)
                            ->where("CompanyInternalID", Auth::user()->company->InternalID)->count();
            if ($checkColorProduct != 0) {
//jika ada maka ambil setiap inventory dari color

                foreach (ProductColorKatalog::where("ProductInternalID", $dataProductKatalog->InternalID)
                        ->where("CompanyInternalID", Auth::user()->company->InternalID)->get() as $data) {
//color ada maka ambil inventory Internal ID dari sini,
// baru select apakah inventory ini ada uom baru apa tidak
                    $this->syncProductUom($data->InventoryInternalID, $dataProductKatalog->InternalID);
                }
            } else {
                $this->syncProductUom($dataProductKatalog->InventoryInternalID, $dataProductKatalog->InternalID);
            }
        }
    }

    public function syncProductUom($inventoryInternal, $dataProductKatalog) {
        $dataProductKatalog = ProductKatalog::find($dataProductKatalog);
        if ($inventoryInternal != -1) {
//auto UoM
//select m_inventory sesuai Inventory yang di dapat dari product color
            $inventory = InventoryKatalog::find($inventoryInternal);
//inventory ini memiliki uom apa saja
//semua inventory uom dari inventory yang diinsert
            $inventoryUom = InventoryUomKatalog::where("InventoryInternalID", $inventory->InternalID)->get();
            foreach ($inventoryUom as $value) {
//cari jumlah uom yang ada di backend apakah sudah ada di catalog
                $uomID = UomKatalog1::find($value->UomInternalID);
                $countUom = UomKatalog::where('UomID', $uomID->UomID)->count();
                if ($countUom > 0) {
//uom sudah ada
                    $internalUom = UomKatalog::where('UomID', $uomID->UomID)->first()->InternalID;
//cek product uom sudah ada atau belum
                    $countProductUom = ProductUomKatalog::where("ProductInternalID", $dataProductKatalog->InternalID)->where('UomInternalID', $internalUom)->count();
                    if ($countProductUom == 0) {
//langsung membuat product uom
                        $productUom = new ProductUomKatalog();
                        $productUom->ProductInternalID = $dataProductKatalog->InternalID;
                        $productUom->UomInternalID = $internalUom;
                        $productUom->Default = $value->Default;
                        if ($value->Default == '1') {
                            ProductUomKatalog::where('Default', '=', 1)->where("ProductInternalID", $dataProductKatalog->InternalID)->where('CompanyInternalID', Auth::user()->Company->InternalID)->update(array('Default' => 0));
                        }
                        $productUom->Value = $value->Value;
                        $productUom->UserRecord = Auth::user()->InternalID;
                        $productUom->CompanyInternalID = Auth::user()->Company->InternalID;
                        $productUom->Remark = '-';
                        $productUom->save();
                    } else {
//update product uom
                        $internalProductUom = ProductUomKatalog::where("ProductInternalID", $dataProductKatalog->InternalID)->where('UomInternalID', $internalUom)->first()->InternalID;
                        $productUom = ProductUomKatalog::find($internalProductUom);
//                        if ($value->Default == '1') {
//                            ProductUomKatalog::where('Default', '=', 1)->where('InternalID', '!=', $internalProductUom)->where("ProductInternalID", $dataProductKatalog->InternalID)->where('CompanyInternalID', Auth::user()->Company->InternalID)->update(array('Default' => 0));
//                        }
                        if ($value->Default == '1' && $productUom->Default != 1) {
                            ProductUomKatalog::where('Default', '=', 1)
                                    ->where('InternalID', '!=', $internalProductUom)
                                    ->where("ProductInternalID", $dataProductKatalog->InternalID)
                                    ->where('CompanyInternalID', Auth::user()->Company->InternalID)
                                    ->update(array('Default' => 0));
                        }
                        $productUom->Default = $value->Default;
                        $productUom->Value = $value->Value;
                        $productUom->UserRecord = Auth::user()->InternalID;
                        $productUom->CompanyInternalID = Auth::user()->Company->InternalID;
                        $productUom->Remark = $productUom->Remark . $value->Default;
                        $productUom->save();
                    }
                } else {
//uom belum ada
//buat uom dulu
                    $uom = new UomKatalog();
                    $uom->UomID = $uomID->UomID;
                    $uom->UomInventoryInternalID = $uomID->InternalID;
                    $uom->Remark = $uomID->Remark;
                    $uom->CompanyInternalID = Auth::user()->CompanyInternalID;
                    $uom->UserRecord = Auth::user()->InternalID;
                    $uom->save();

//kemudian membuat product uom
                    $productUom = new ProductUomKatalog();
                    $productUom->ProductInternalID = $dataProductKatalog->InternalID;
                    $productUom->UomInternalID = $uom->InternalID;
                    $productUom->Default = $value->Default;
                    if ($value->Default == '1') {
                        ProductUomKatalog::where('Default', '=', 1)->where("ProductInternalID", $dataProductKatalog->InternalID)->where('CompanyInternalID', Auth::user()->Company->InternalID)->update(array('Default' => 0));
                    }
                    $productUom->Value = $value->Value;
                    $productUom->UserRecord = Auth::user()->InternalID;
                    $productUom->CompanyInternalID = Auth::user()->Company->InternalID;
                    $productUom->Remark = '-';
                    $productUom->save();
                }
            }
        }
    }

    public function pushPrice() {
        $defaultPrice = DefaultPrice::where("CompanyInternalID", Auth::user()->Company->InternalID)->get();
        foreach (DefaultPriceKatalog::all() as $data) {
            $DefaultPriceKatalog = DefaultPriceKatalog::find($data->InternalID)->delete();
        }
        Foreach ($defaultPrice as $data) {
            $defaultPriceKatalog = new DefaultPriceKatalog();
            $defaultPriceKatalog->InternalID = $data->InternalID;
            $defaultPriceKatalog->PriceID = $data->PriceID;
            $defaultPriceKatalog->PriceName = $data->PriceName;
            $defaultPriceKatalog->POS = $data->POS;
            $defaultPriceKatalog->Online = $data->Online;
            $defaultPriceKatalog->CompanyInternalID = $data->CompanyInternalID;
            $defaultPriceKatalog->save();
        }
    }

    public function exportExcel() {
        Excel::create('Master_Inventory', function($excel) {
            $excel->sheet('Master_Inventory', function($sheet) {
                $sheet->mergeCells('B1:M1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Master Inventory");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Type");
                $sheet->setCellValueByColumnAndRow(3, 2, "Inventory Name");
                $sheet->setCellValueByColumnAndRow(4, 2, "Inventory ID");
                $sheet->setCellValueByColumnAndRow(5, 2, "UoM (Default)");
                $sheet->setCellValueByColumnAndRow(6, 2, "Barcode");
                $sheet->setCellValueByColumnAndRow(7, 2, "Group");
                $sheet->setCellValueByColumnAndRow(8, 2, "Variety");
                $sheet->setCellValueByColumnAndRow(9, 2, "Brand");
                $sheet->setCellValueByColumnAndRow(10, 2, "Record");
                $sheet->setCellValueByColumnAndRow(11, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(12, 2, "Remark");
                $row = 3;
                foreach (Inventory::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, $data->InventoryType->InventoryTypeName);
                    $sheet->setCellValueByColumnAndRow(3, $row, $data->InventoryName);
                    $sheet->setCellValueByColumnAndRow(4, $row, "`" . $data->InventoryID);
                    $sheet->setCellValueByColumnAndRow(5, $row, $data->UoM);
                    $sheet->setCellValueByColumnAndRow(6, $row, $data->BarcodeCode);
                    $sheet->setCellValueByColumnAndRow(7, $row, $data->Group->GroupName);
                    $sheet->setCellValueByColumnAndRow(8, $row, $data->Variety->VarietyName);
                    $sheet->setCellValueByColumnAndRow(9, $row, $data->Brand->BrandName);
                    $sheet->setCellValueByColumnAndRow(10, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(11, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(12, $row, $data->Remark);
                    $row++;
                }

                if (Inventory::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() <= 0) {
                    $sheet->mergeCells('B3:M3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "No data available in table");

                    $sheet->cells('B3:M3', function($cells) {
                        $cells->setAlignment('center');
                    });

                    $sheet->setBorder('B3:M' . $row, 'thin');
                }

                $row--;
                $sheet->setBorder('B2:M' . $row, 'thin');
                $sheet->cells('B2:M2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:M' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
                $sheet->cells('F3:F' . $row, function($cells) {
                    $cells->setAlignment('right');
                });
            });
        })->export('xls');
    }

    public function detailInventory() {
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $warehouse = Input::get('warehouse');
        if ($warehouse == '-1') {
            $delimiterWarehouse = '!=';
            $warehouseName = 'All';
        } else {
            $delimiterWarehouse = '=';
            $warehouseName = Warehouse::find($warehouse)->WarehouseName;
        }
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img style="width: 1025px; height: 200px" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Inventory Detail Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Warehouse ' . $warehouseName . '</span><br>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period    : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span>
                            <table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Name</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Initial Stock</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Purchase</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Purchase Return</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales Return</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Memo In</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Memo Out</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Transfer In</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Transfer Out</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Transformation Out</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Transformation In</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">End Stock</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
        if (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() > 0) {
            foreach (Inventory::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $value) {
                if (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->where('InventoryInternalID', $value->InternalID)->count() > 0) {
                    $initialStockTamp = Inventory::getInitialStock($start, '<', $value->InternalID, $warehouse, $delimiterWarehouse);
                    $totalPieces = 0;
                    foreach (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->where('InventoryInternalID', $value->InternalID)->get() as $data) {
                        $pembelian = PurchaseDetail::
                                join('t_purchase_header', 't_purchase_header.InternalID', '=', 't_purchase_detail.PurchaseInternalID')
                                ->where('t_purchase_header.CompanyInternalID', Auth::user()->Company->InternalID)
                                ->where('t_purchase_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                                ->where('t_purchase_detail.InventoryInternalID', $data->InventoryInternalID)
                                ->where('t_purchase_detail.UomInternalID', $data->UomInternalID)
                                ->whereBetween('t_purchase_header.PurchaseDate', array($start, $end))
                                ->sum('Qty');
                        $penjualan = SalesDetail::
                                join('t_sales_header', 't_sales_header.InternalID', '=', 't_sales_detail.SalesInternalID')
                                ->where('t_sales_header.CompanyInternalID', Auth::user()->Company->InternalID)
                                ->where('t_sales_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                                ->where('t_sales_detail.InventoryInternalID', $data->InventoryInternalID)
                                ->where('t_sales_detail.UomInternalID', $data->UomInternalID)
                                ->whereBetween('t_sales_header.SalesDate', array($start, $end))
                                ->sum('Qty');
                        $Rpembelian = PurchaseReturnDetail::
                                join('t_purchasereturn_header', 't_purchasereturn_header.InternalID', '=', 't_purchasereturn_detail.PurchaseReturnInternalID')
                                ->where('t_purchasereturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                                ->where('t_purchasereturn_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                                ->where('t_purchasereturn_detail.InventoryInternalID', $data->InventoryInternalID)
                                ->where('t_purchasereturn_detail.UomInternalID', $data->UomInternalID)
                                ->whereBetween('t_purchasereturn_header.PurchaseReturnDate', array($start, $end))
                                ->sum('Qty');
                        $Rpenjualan = SalesReturnDetail::
                                join('t_salesreturn_header', 't_salesreturn_header.InternalID', '=', 't_salesreturn_detail.SalesReturnInternalID')
                                ->where('t_salesreturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                                ->where('t_salesreturn_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                                ->where('t_salesreturn_detail.InventoryInternalID', $data->InventoryInternalID)
                                ->where('t_salesreturn_detail.UomInternalID', $data->UomInternalID)
                                ->whereBetween('t_salesreturn_header.SalesReturnDate', array($start, $end))
                                ->sum('Qty');
                        $Min = MemoInDetail::
                                join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.MemoInInternalID')
                                ->where('t_memoin_header.CompanyInternalID', Auth::user()->Company->InternalID)
                                ->where('t_memoin_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                                ->where('t_memoin_detail.InventoryInternalID', $data->InventoryInternalID)
                                ->where('t_memoin_detail.UomInternalID', $data->UomInternalID)
                                ->whereBetween('t_memoin_header.MemoInDate', array($start, $end))
                                ->sum('Qty');
                        $Mout = MemoOutDetail::
                                join('t_memoout_header', 't_memoout_header.InternalID', '=', 't_memoout_detail.MemoOutInternalID')
                                ->where('t_memoout_header.CompanyInternalID', Auth::user()->Company->InternalID)
                                ->where('t_memoout_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                                ->where('t_memoout_detail.InventoryInternalID', $data->InventoryInternalID)
                                ->where('t_memoout_detail.UomInternalID', $data->UomInternalID)
                                ->whereBetween('t_memoout_header.MemoOutDate', array($start, $end))
                                ->sum('Qty');
                        $Tin = TransferDetail::
                                join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                                ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                                ->where('t_transfer_header.WarehouseDestinyInternalID', $delimiterWarehouse, $warehouse)
                                ->where('t_transfer_detail.InventoryInternalID', $data->InventoryInternalID)
                                ->where('t_transfer_detail.UomInternalID', $data->UomInternalID)
                                ->whereBetween('t_transfer_header.TransferDate', array($start, $end))
                                ->sum('Qty');
                        $Tout = TransferDetail::
                                join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                                ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                                ->where('t_transfer_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                                ->where('t_transfer_detail.InventoryInternalID', $data->InventoryInternalID)
                                ->where('t_transfer_detail.UomInternalID', $data->UomInternalID)
                                ->whereBetween('t_transfer_header.TransferDate', array($start, $end))
                                ->sum('Qty');
                        $Trin = TransformationDetail::
                                join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                                ->where('t_transformation_header.CompanyInternalID', Auth::user()->Company->InternalID)
                                ->where('t_transformation_detail.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                                ->where('t_transformation_detail.Type', "in")
                                ->where('t_transformation_detail.InventoryInternalID', $data->InventoryInternalID)
                                ->where('t_transformation_detail.UomInternalID', $data->UomInternalID)
                                ->whereBetween('t_transformation_header.TransformationDate', array($start, $end))
                                ->sum('Qty');
                        $Trout = TransformationDetail::
                                join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                                ->where('t_transformation_header.CompanyInternalID', Auth::user()->Company->InternalID)
                                ->where('t_transformation_detail.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                                ->where('t_transformation_detail.Type', "out")
                                ->where('t_transformation_detail.InventoryInternalID', $data->InventoryInternalID)
                                ->where('t_transformation_detail.UomInternalID', $data->UomInternalID)
                                ->whereBetween('t_transformation_header.TransformationDate', array($start, $end))
                                ->sum('Qty');
                        $initialStock = $initialStockTamp;
                        if ($data->Default == 0) {
                            $initialStock = 0;
                        }
                        $endStock = $initialStock + $pembelian + $Rpenjualan - $penjualan - $Rpembelian + $Min - $Mout + $Tin - $Tout + $Trin - $Trout;

                        $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->inventory->InventoryID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->inventory->InventoryName . ' | ' . $data->uom->UomID . ' (' . number_format($data->Value, 0, '.', ',') . ')</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($initialStock, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($pembelian, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($penjualan, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($Rpembelian, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($Rpenjualan, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($Min, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($Mout, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($Tin, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($Tout, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($Trin, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($Trout, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($endStock, '0', '.', ',')) . '</td>
                            </tr>';

                        $totalPieces += $endStock * $data->Value;
                    }

                    $html .= '<tr> 
                            <td colspan="14" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right">Summary : ';
                    foreach (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)
                            ->where('InventoryInternalID', $value->InternalID)
                            ->orderBy('Value', 'desc')->get() as $loop) {
                        $html .= number_format((int) ($totalPieces / $loop->Value), 0, '.', ',') . ' ' . $loop->uom->UomID . ' ';
                        $totalPieces = $totalPieces % $loop->Value;
                    }
                    $html .= '</td>
                        </tr>';
                }
            }
        } else {
            $html .= '<tr>
                            <td colspan="12" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: center">There is no inventory registered.</td>
                        </tr>';
        }

        $html .= '</tbody>
                            </div>
                    </div>
                </body>
            </html>';
        return PDF::load($html, 'A4', 'landscape')->download('Inventory_detail_report');
    }

    public function detailInventoryExcel() {
        Excel::create('inventory_detail', function($excel) {
            $excel->sheet('inventory_detail', function($sheet) {
                $zero = Input::get('nilai0');
                $bulan = Input::get('month');
                $tahun = Input::get('year');
                $warehouse = Input::get('warehouse');
                $group = Input::get('group');
                if ($warehouse == '-1') {
                    $huruf = 'N';
                    $delimiterWarehouse = '!=';
                    $warehouseName = 'All';
                } else {
                    $huruf = 'P';
                    $delimiterWarehouse = '=';
                    $warehouseName = Warehouse::find($warehouse)->WarehouseName;
                }
                if ($group == '-1') {
                    $delimiterGroup = '!=';
                    $groupName = 'All';
                } else {
                    $delimiterGroup = '=';
                    $groupName = Group::find($group)->GroupName;
                }
                $sheet->mergeCells('B1:T1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Inventory Detail Report");
                $sheet->mergeCells('B2:C2');
                $sheet->setCellValueByColumnAndRow(1, 2, "Period : " . date("M", strtotime($tahun . '-' . $bulan . '-01')) . ' ' . $tahun);
                $sheet->setCellValueByColumnAndRow(3, 2, "Warehouse : " . $warehouseName);
                $sheet->setCellValueByColumnAndRow(1, 3, "No.");
                $sheet->setCellValueByColumnAndRow(2, 3, "Inventory ID");
                $sheet->setCellValueByColumnAndRow(3, 3, "Inventory Name");
                $sheet->setCellValueByColumnAndRow(4, 3, "Initial Stock");
//                $sheet->setCellValueByColumnAndRow(5, 3, "Initial Value");
                $sheet->setCellValueByColumnAndRow(5, 3, "Purchase");
//                $sheet->setCellValueByColumnAndRow(7, 3, "Purchase Value");
                $sheet->setCellValueByColumnAndRow(6, 3, "Sales");
//                $sheet->setCellValueByColumnAndRow(9, 3, "Sales Value");
                $sheet->setCellValueByColumnAndRow(7, 3, "Purchase Return");
//                $sheet->setCellValueByColumnAndRow(11, 3, "Purchase Return Value");
                $sheet->setCellValueByColumnAndRow(8, 3, "Sales Return");
//                $sheet->setCellValueByColumnAndRow(13, 3, "Sales Return Value");
                $sheet->setCellValueByColumnAndRow(9, 3, "Memo In");
//                $sheet->setCellValueByColumnAndRow(15, 3, "Memo In Value");
                $sheet->setCellValueByColumnAndRow(10, 3, "Memo Out");
//                $sheet->setCellValueByColumnAndRow(17, 3, "Memo Out Value");
                $sheet->setCellValueByColumnAndRow(11, 3, "Transformation Out");
//                $sheet->setCellValueByColumnAndRow(19, 3, "Transformation Out Value");
                $sheet->setCellValueByColumnAndRow(12, 3, "Transformation In");
//                $sheet->setCellValueByColumnAndRow(21, 3, "Transformation In Value");
                if ($warehouse != '-1') {
                    $sheet->setCellValueByColumnAndRow(13, 3, "Transfer In");
//                    $sheet->setCellValueByColumnAndRow(23, 3, "Transfer In Value");
                    $sheet->setCellValueByColumnAndRow(14, 3, "Transfer Out");
//                    $sheet->setCellValueByColumnAndRow(25, 3, "Transfer Out Value");
                    $sheet->setCellValueByColumnAndRow(15, 3, "End Stock");
//                    $sheet->setCellValueByColumnAndRow(27, 3, "End Stock Value");
                } else {
                    $sheet->setCellValueByColumnAndRow(13, 3, "End Stock");
//                    $sheet->setCellValueByColumnAndRow(23, 3, "End Stock Value");
                }
                $row = 4;
//                $inventory = Inventory::join('m_inventorytype', 'm_inventorytype.InternalID', '=', 'm_inventory.InventoryTypeInternalID')
//                                ->select('m_inventory.*', DB::raw('m_inventory.InternalID as InventoryInternalID'))
//                                //->where('m_inventorytype.Flag', 1)
//                                ->where('m_inventorytype.CompanyInternalID', Auth::user()->CompanyInternalID)->get();

                $inventory = Inventory::where("GroupInternalID", $delimiterGroup, $group)->get();
                if (count($inventory) > 0) {
//                    foreach ($inventory as $data) {
//                        $InventoryData = detailReportInventoryWarehouse($data, $warehouse, $bulan, $tahun);
//                        $InventoryData = explode("^_^", $InventoryData);
//                        if (($zero != 'nilai0' && $InventoryData[15] != '' && $InventoryData[15] != 0) || $zero == 'nilai0') {
//                            $sheet->setCellValueByColumnAndRow(1, $row, $row - 3);
//                            $sheet->setCellValueByColumnAndRow(2, $row, $data->InventoryID);
//                            $sheet->setCellValueByColumnAndRow(3, $row, $data->InventoryName);
//                            $sheet->setCellValueByColumnAndRow(4, $row, number_format($InventoryData[0], '0', '.', ','));
////                        $sheet->setCellValueByColumnAndRow(5, $row, number_format($InventoryData[8], '0', '.', ','));
//                            $sheet->setCellValueByColumnAndRow(5, $row, number_format($InventoryData[1], '0', '.', ','));
////                        $sheet->setCellValueByColumnAndRow(7, $row, number_format($InventoryData[9], '0', '.', ','));
//                            $sheet->setCellValueByColumnAndRow(6, $row, number_format($InventoryData[3], '0', '.', ','));
////                        $sheet->setCellValueByColumnAndRow(9, $row, number_format($InventoryData[11], '0', '.', ','));
//                            $sheet->setCellValueByColumnAndRow(7, $row, number_format($InventoryData[4], '0', '.', ','));
////                        $sheet->setCellValueByColumnAndRow(11, $row, number_format($InventoryData[12], '0', '.', ','));
//                            $sheet->setCellValueByColumnAndRow(8, $row, number_format($InventoryData[2], '0', '.', ','));
////                        $sheet->setCellValueByColumnAndRow(13, $row, number_format($InventoryData[10], '0', '.', ','));
//                            $sheet->setCellValueByColumnAndRow(9, $row, number_format($InventoryData[5], '0', '.', ','));
////                        $sheet->setCellValueByColumnAndRow(15, $row, number_format($InventoryData[13], '0', '.', ','));
//                            $sheet->setCellValueByColumnAndRow(10, $row, number_format($InventoryData[6], '0', '.', ','));
////                        $sheet->setCellValueByColumnAndRow(17, $row, number_format($InventoryData[14], '0', '.', ','));
//                            $sheet->setCellValueByColumnAndRow(11, $row, number_format($InventoryData[16], '0', '.', ','));
////                        $sheet->setCellValueByColumnAndRow(19, $row, number_format($InventoryData[17] == '' ? 0 : $InventoryData[17], '0', '.', ','));
//                            $sheet->setCellValueByColumnAndRow(12, $row, number_format($InventoryData[18], '0', '.', ','));
////                        $sheet->setCellValueByColumnAndRow(21, $row, number_format($InventoryData[19] == '' ? 0 : $InventoryData[19], '0', '.', ','));
//                            if ($warehouse != '-1') {
//                                $sheet->setCellValueByColumnAndRow(13, $row, number_format($InventoryData[21] == '' ? 0 : $InventoryData[21], '0', '.', ','));
////                    $sheet->setCellValueByColumnAndRow(23, 3, "Transfer In Value");
//                                $sheet->setCellValueByColumnAndRow(14, $row, number_format($InventoryData[20] == '' ? 0 : $InventoryData[20], '0', '.', ','));
////                    $sheet->setCellValueByColumnAndRow(25, 3, "Transfer Out Value");
//                                $sheet->setCellValueByColumnAndRow(15, $row, number_format($InventoryData[7] == '' ? 0 : $InventoryData[7], '0', '.', ','));
////                    $sheet->setCellValueByColumnAndRow(27, 3, "End Stock Value");
//                            } else {
//                                $sheet->setCellValueByColumnAndRow(13, $row, number_format($InventoryData[7] == '' ? 0 : $InventoryData[7], '0', '.', ','));
////                        $sheet->setCellValueByColumnAndRow(23, $row, number_format($InventoryData[15] == '' ? 0 : $InventoryData[15], '0', '.', ','));
//                                $row++;
//                            }
//                        }
//                    } 
                    foreach ($inventory as $data) {
                        $endstock = Inventory::getEndStock($data->InternalID, $tahun . '-' . $bulan . '-01', $tahun . '-' . $bulan . '-31', $warehouse, $delimiterWarehouse);
                        if (($zero != 'nilai0' && $endstock != 0) || $zero == 'nilai0') {
                            $sheet->setCellValueByColumnAndRow(1, $row, $row - 3);
                            $sheet->setCellValueByColumnAndRow(2, $row, $data->InventoryID);
                            $sheet->setCellValueByColumnAndRow(3, $row, $data->InventoryName);
                            //initial
                            $sheet->setCellValueByColumnAndRow(4, $row, Inventory::getInitialStockBaru($data->InternalID, $tahun . '-' . $bulan . '-01', $warehouse, $delimiterWarehouse));
                            //purchase
                            $sheet->setCellValueByColumnAndRow(5, $row, Inventory::getMRV($data->InternalID, $bulan, $tahun, $warehouse, $delimiterWarehouse));
                            //sales
                            $sheet->setCellValueByColumnAndRow(6, $row, Inventory::getShipping($data->InternalID, $bulan, $tahun, $warehouse, $delimiterWarehouse));
                            //purchase return
                            $sheet->setCellValueByColumnAndRow(7, $row, Inventory::getPR($data->InternalID, $bulan, $tahun, $warehouse, $delimiterWarehouse));
                            //sales return
                            $sheet->setCellValueByColumnAndRow(8, $row, Inventory::getSR($data->InternalID, $bulan, $tahun, $warehouse, $delimiterWarehouse));
                            //memo in
                            $sheet->setCellValueByColumnAndRow(9, $row, Inventory::getMemoIn($data->InternalID, $bulan, $tahun, $warehouse, $delimiterWarehouse));
                            //memo out
                            $sheet->setCellValueByColumnAndRow(10, $row, Inventory::getMemoOut($data->InternalID, $bulan, $tahun, $warehouse, $delimiterWarehouse));
                            //transformation out
                            $sheet->setCellValueByColumnAndRow(11, $row, Inventory::getTransformationIn($data->InternalID, $bulan, $tahun, $warehouse, $delimiterWarehouse));
                            //transformation in
                            $sheet->setCellValueByColumnAndRow(12, $row, Inventory::getTransformationOut($data->InternalID, $bulan, $tahun, $warehouse, $delimiterWarehouse));
                            if ($warehouse != '-1') {
                                $sheet->setCellValueByColumnAndRow(13, $row, Inventory::getTransferIn($data->InternalID, $bulan, $tahun, $warehouse, $delimiterWarehouse));
                                $sheet->setCellValueByColumnAndRow(14, $row, Inventory::getTransferOut($data->InternalID, $bulan, $tahun, $warehouse, $delimiterWarehouse));
                                $sheet->setCellValueByColumnAndRow(15, $row, $endstock);
                            } else {
                                $sheet->setCellValueByColumnAndRow(13, $row, $endstock);
                            }
                            $row++;
                        }
                    }
                } else {
                    if ($warehouse != '-1') {
                        $sheet->mergeCells('B' . $row . ':P' . $row);
                    } else {
                        $sheet->mergeCells('B' . $row . ':N' . $row);
                    }
                    $sheet->setCellValueByColumnAndRow(1, $row, "There is no inventory registered.");
                }


                $row--;
//                $sheet->setBorder('B3:' . $huruf . $row, 'thin');
//                $sheet->cells('B3:' . $huruf . '3', function($cells) {
//                    $cells->setBackground('#eaf6f7');
//                    $cells->setValignment('middle');
//                    $cells->setAlignment('center');
//                });
//                $sheet->cells('B1', function($cells) {
//                    $cells->setValignment('middle');
//                    $cells->setAlignment('center');
//                    $cells->setFontWeight('bold');
//                    $cells->setFontSize('16');
//                });
//                $sheet->cells('B2:B3', function($cells) {
//                    $cells->setFontWeight('bold');
//                    $cells->setFontSize('14');
//                });
//                $sheet->cells('B3:' . $huruf . '3' . $row, function($cells) {
//                    $cells->setAlignment('center');
//                    $cells->setFontWeight('bold');
//                });
//                $sheet->cells('B4:' . $huruf . $row, function($cells) {
//                    $cells->setAlignment('left');
//                });
            });
        })->export('xls');
    }

    public function detailInventoryHtml() {
        $zero = Input::get('nilai0');
        $bulan = Input::get('month');
        $tahun = Input::get('year');
        $warehouse = Input::get('warehouse');
        $group = Input::get('group');
        if ($warehouse == '-1') {
            $delimiterWarehouse = '!=';
            $warehouseName = 'All';
        } else {
            $delimiterWarehouse = '=';
            $warehouseName = Warehouse::find($warehouse)->WarehouseName;
        }
        if ($group == '-1') {
            $delimiterGroup = '!=';
            $groupName = 'All';
        } else {
            $delimiterGroup = '=';
            $groupName = Group::find($group)->GroupName;
        }
        $count = 0;
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <!--<img style="width: 1025px; height: 200px" src = "' . substr(Auth::user()->Company->Logo, 1) . '">-->
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Inventory Detail Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Warehouse ' . $warehouseName . '</span><br>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Group ' . $groupName . '</span><br>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period    : ' . date("M", strtotime($tahun . '-' . $bulan . '-01')) . ' ' . $tahun . '</span>
                            <table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory Name</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Initial Stock</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Initial Stock Value</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Purchase</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Purchase Value</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales Value</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Purchase Return</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Purchase Return Value</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales Return</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales Return Value</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Memo In</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Memo In Value</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Memo Out</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Memo Out Value</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Transformation Out</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Transformation Out Value</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Transformation In</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Transformation In Value</th>';
        if ($warehouse != '-1' && false) {
            $html .= '                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Transfer In</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Transfer In Value</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Transfer Out</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Transfer Out Value</th>';
        }
        $html .= '<th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">End Stock</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">End Stock Value</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
        $inventory = Inventory::join('m_inventorytype', 'm_inventorytype.InternalID', '=', 'm_inventory.InventoryTypeInternalID')
                        ->select('m_inventory.*', DB::raw('m_inventory.InternalID as InventoryInternalID'))
                        //->where('m_inventorytype.Flag', 1)
                        ->where('m_inventory.GroupInternalID', $delimiterGroup, $group)
                        ->where('m_inventorytype.CompanyInternalID', Auth::user()->CompanyInternalID)->get();
//        dd($zero);
        if (count($inventory) > 0) {
            foreach ($inventory as $data) {
                $InventoryData = detailReportInventoryWarehouse($data, $warehouse, $bulan, $tahun);
                $InventoryData = explode("^_^", $InventoryData);
                if (($zero != 'nilai0' && $InventoryData[15] != '' && $InventoryData[15] != 0) || $zero == 'nilai0') {
//                if ($zero == 'nilai0' || ($InventoryData[7] != '' && $InventoryData[7] != 0 && $InventoryData[7] != "0")) {
//                    dd($InventoryData[19]);
                    $count++;
                    $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->InventoryID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->InventoryName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($InventoryData[0], '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($InventoryData[8], '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($InventoryData[1], '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($InventoryData[9], '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($InventoryData[3], '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($InventoryData[11], '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($InventoryData[4], '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($InventoryData[12], '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($InventoryData[2], '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($InventoryData[10], '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($InventoryData[5], '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($InventoryData[13], '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($InventoryData[6], '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($InventoryData[14], '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($InventoryData[16], '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($InventoryData[17] == '' ? 0 : $InventoryData[17], '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($InventoryData[18], '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($InventoryData[19] == '' ? 0 : $InventoryData[19], '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($InventoryData[7] == '' ? 0 : $InventoryData[7], '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($InventoryData[15] == '' ? 0 : $InventoryData[15], '0', '.', ',') . '</td>
                            </tr>';
                }
            }
        }
        if ($count == 0) {
            $html .= '<tr>
                            <td colspan="22" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: center">There is no inventory registered.</td>
                        </tr>';
        }

        $html .= '</tbody>
                            </div>
                    </div>
                </body>
            </html>';
//        return PDF::load($html, 'A4', 'landscape')->download('Inventory_detail_report');
        return $html;
    }

    //stok per barang yg lama
    public function stockInventory_lama() {
        $inventory = Inventory::find(Input::get('InternalID'));
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $warehouse = Input::get('warehouse');
        if ($warehouse == '-1') {
            $delimiterWarehouse = '!=';
            $warehouseName = 'All';
        } else {
            $delimiterWarehouse = '=';
            $warehouseName = Warehouse::find($warehouse)->WarehouseName;
        }
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img style="height: 298px" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Inventory ' . $inventory->InventoryName . ' Stock Card</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Warehouse ' . $warehouseName . '</span><br>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period    : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span>
                            <table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Transaction ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Uom</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Qty</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Price</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc (%)</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
        $count = 0;
        foreach (PurchaseDetail::join('t_purchase_header', 't_purchase_header.InternalID', '=', 't_purchase_detail.PurchaseInternalID')
                ->where('CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('InventoryInternalID', $inventory->InternalID)
                ->where('t_purchase_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                ->whereBetween('PurchaseDate', Array($start, $end))
                ->select('t_purchase_detail.Remark as Remarks', 't_purchase_detail.*', 't_purchase_header.*')
                ->orderBy('PurchaseDate')->get() as $data) {
            $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->PurchaseID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->PurchaseDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . Uom::find($data->UomInternalID)->UomID . ' (' . number_format(InventoryUom::where('InventoryInternalID', $data->InventoryInternalID)->where('UomInternalID', $data->UomInternalID)->first()->Value, 0, '.', ',') . ')</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>';
            if ($data->Remarks == '') {
                $html .= '<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>';
            } else {
                $html .= '<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . str_replace('+', ' + ', $data->Remarks) . '</td>';
            }
            $html .= '<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
            $count++;
        }
        foreach (PurchaseReturnDetail::join('t_purchasereturn_header', 't_purchasereturn_header.InternalID', '=', 't_purchasereturn_detail.PurchaseReturnInternalID')
                ->where('CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('InventoryInternalID', $inventory->InternalID)
                ->where('t_purchasereturn_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                ->whereBetween('PurchaseReturnDate', Array($start, $end))
                ->select('t_purchasereturn_detail.Remark as Remarks', 't_purchasereturn_detail.*', 't_purchasereturn_header.*')
                ->orderBy('PurchaseReturnDate')->get() as $data) {
            $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->PurchaseReturnID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->PurchaseReturnDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . Uom::find($data->UomInternalID)->UomID . ' (' . number_format(InventoryUom::where('InventoryInternalID', $data->InventoryInternalID)->where('UomInternalID', $data->UomInternalID)->first()->Value, 0, '.', ',') . ')</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>';
            if ($data->Remarks == '') {
                $html .= '<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>';
            } else {
                $html .= '<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . str_replace('+', ' + ', $data->Remarks) . '</td>';
            }
            $html .= '<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
            $count++;
        }
        foreach (ShippingAddDetail::join('t_shipping_header', 't_shipping_header.InternalID', '=', 't_shipping_detail.ShippingInternalID')
                ->where('CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('InventoryInternalID', $inventory->InternalID)
                ->where('t_shipping_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                ->whereBetween('ShippingDate', Array($start, $end))
                ->select('t_shipping_detail.Remark as Remarks', 't_shipping_detail.*', 't_shipping_header.*')
                ->orderBy('ShippingDate')->get() as $data) {
            $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->ShippingID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->ShippingDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . Uom::find($data->UomInternalID)->UomID . ' (' . number_format(InventoryUom::where('InventoryInternalID', $data->InventoryInternalID)->where('UomInternalID', $data->UomInternalID)->first()->Value, 0, '.', ',') . ')</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>';
            if ($data->Remarks == '') {
                $html .= '<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>';
            } else {
                $html .= '<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . str_replace('+', ' + ', $data->Remarks) . '</td>';
            }
            $html .= '<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
            $count++;
        }
        foreach (SalesReturnDetail::join('t_salesreturn_header', 't_salesreturn_header.InternalID', '=', 't_salesreturn_detail.SalesReturnInternalID')
                ->where('CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('InventoryInternalID', $inventory->InternalID)
                ->where('t_salesreturn_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                ->whereBetween('SalesReturnDate', Array($start, $end))
                ->select('t_salesreturn_detail.Remark as Remarks', 't_salesreturn_detail.*', 't_salesreturn_header.*')
                ->orderBy('SalesReturnDate')->get() as $data) {
            $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->SalesReturnID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->SalesReturnDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . Uom::find($data->UomInternalID)->UomID . ' (' . number_format(InventoryUom::where('InventoryInternalID', $data->InventoryInternalID)->where('UomInternalID', $data->UomInternalID)->first()->Value, 0, '.', ',') . ')</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>';
            if ($data->Remarks == '') {
                $html .= '<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>';
            } else {
                $html .= '<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . str_replace('+', ' + ', $data->Remarks) . '</td>';
            }
            $html .= '<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
            $count++;
        }
        foreach (MemoOutDetail::join('t_memoout_header', 't_memoout_header.InternalID', '=', 't_memoout_detail.MemoOutInternalID')
                ->where('CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('InventoryInternalID', $inventory->InternalID)
                ->where('t_memoout_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                ->whereBetween('MemoOutDate', Array($start, $end))
                ->select('t_memoout_detail.Remark as Remarks', 't_memoout_detail.*', 't_memoout_header.*')
                ->orderBy('MemoOutDate')->get() as $data) {
            $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->MemoOutID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->MemoOutDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . Uom::find($data->UomInternalID)->UomID . ' (' . number_format(InventoryUom::where('InventoryInternalID', $data->InventoryInternalID)->where('UomInternalID', $data->UomInternalID)->first()->Value, 0, '.', ',') . ')</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right"></td>';
            $html .= '<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right"></td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
            $count++;
        }
        foreach (MemoInDetail::join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.MemoInInternalID')
                ->where('CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('InventoryInternalID', $inventory->InternalID)
                ->where('t_memoin_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                ->whereBetween('MemoInDate', Array($start, $end))
                ->select('t_memoin_detail.Remark as Remarks', 't_memoin_detail.*', 't_memoin_header.*')
                ->orderBy('MemoInDate')->get() as $data) {
            $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->MemoInID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->MemoInDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . Uom::find($data->UomInternalID)->UomID . ' (' . number_format(InventoryUom::where('InventoryInternalID', $data->InventoryInternalID)->where('UomInternalID', $data->UomInternalID)->first()->Value, 0, '.', ',') . ')</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right"></td>';
            $html .= '<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right"></td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
            $count++;
        }
        foreach (TransferDetail::join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                ->where('CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('InventoryInternalID', $inventory->InternalID)
                ->where('t_transfer_header.WarehouseDestinyInternalID', $delimiterWarehouse, $warehouse)
                ->whereBetween('TransferDate', Array($start, $end))
                ->select('t_transfer_detail.Remark as Remarks', 't_transfer_detail.*', 't_transfer_header.*')
                ->orderBy('TransferDate')->get() as $data) {
            $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">In - ' . $data->TransferID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->TransferDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . Uom::find($data->UomInternalID)->UomID . ' (' . number_format(InventoryUom::where('InventoryInternalID', $data->InventoryInternalID)->where('UomInternalID', $data->UomInternalID)->first()->Value, 0, '.', ',') . ')</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right"></td>';
            $html .= '<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right"></td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
            $count++;
        }
        foreach (TransferDetail::join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                ->where('CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('InventoryInternalID', $inventory->InternalID)
                ->where('t_transfer_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                ->whereBetween('TransferDate', Array($start, $end))
                ->select('t_transfer_detail.Remark as Remarks', 't_transfer_detail.*', 't_transfer_header.*')
                ->orderBy('TransferDate')->get() as $data) {
            $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">Out - ' . $data->TransferID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->TransferDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . Uom::find($data->UomInternalID)->UomID . ' (' . number_format(InventoryUom::where('InventoryInternalID', $data->InventoryInternalID)->where('UomInternalID', $data->UomInternalID)->first()->Value, 0, '.', ',') . ')</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right"></td>';
            $html .= '<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right"></td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
            $count++;
        }

        if ($count == 0) {
            $html .= '<tr>
                            <td colspan="8" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: center">There is no transaction inventory registered.</td>
                        </tr></tbody></table>';
        } else {
            $html .= '</tbody></table>';
        }
        $html .= '<h5>Summary</h5>';

        $html .= '<table class="tableBorder" width="100%" style="">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Name</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Initial Stock</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Purchase</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Shipping</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Purchase Return</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales Return</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Memo In</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Memo Out</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Transfer In</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Transfer Out</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">End Stock</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
        $totalPieces = 0;
        $initialStockTamp = Inventory::getInitialStock($start, '<', $inventory->InternalID, $warehouse, $delimiterWarehouse);
        foreach (InventoryUom::where('InventoryInternalID', $inventory->InternalID)->get() as $data) {
            $pembelian = PurchaseDetail::
                    join('t_purchase_header', 't_purchase_header.InternalID', '=', 't_purchase_detail.PurchaseInternalID')
                    ->where('t_purchase_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_purchase_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_purchase_detail.InventoryInternalID', $data->InventoryInternalID)
                    ->where('t_purchase_detail.UomInternalID', $data->UomInternalID)
                    ->whereBetween('t_purchase_header.PurchaseDate', array($start, $end))
                    ->sum('Qty');
            $penjualan = ShippingAddDetail::
                    join('t_shipping_header', 't_shipping_header.InternalID', '=', 't_shipping_detail.ShippingInternalID')
                    ->where('t_shipping_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_shipping_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_shipping_detail.InventoryInternalID', $data->InventoryInternalID)
                    ->where('t_shipping_detail.UomInternalID', $data->UomInternalID)
                    ->whereBetween('t_shipping_header.ShippingDate', array($start, $end))
                    ->sum('Qty');
            $Rpembelian = PurchaseReturnDetail::
                    join('t_purchasereturn_header', 't_purchasereturn_header.InternalID', '=', 't_purchasereturn_detail.PurchaseReturnInternalID')
                    ->where('t_purchasereturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_purchasereturn_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_purchasereturn_detail.InventoryInternalID', $data->InventoryInternalID)
                    ->where('t_purchasereturn_detail.UomInternalID', $data->UomInternalID)
                    ->whereBetween('t_purchasereturn_header.PurchaseReturnDate', array($start, $end))
                    ->sum('Qty');
            $Rpenjualan = SalesReturnDetail::
                    join('t_salesreturn_header', 't_salesreturn_header.InternalID', '=', 't_salesreturn_detail.SalesReturnInternalID')
                    ->where('t_salesreturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_salesreturn_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_salesreturn_detail.InventoryInternalID', $data->InventoryInternalID)
                    ->where('t_salesreturn_detail.UomInternalID', $data->UomInternalID)
                    ->whereBetween('t_salesreturn_header.SalesReturnDate', array($start, $end))
                    ->sum('Qty');
            $Min = MemoInDetail::
                    join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.MemoInInternalID')
                    ->where('t_memoin_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_memoin_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_memoin_detail.InventoryInternalID', $data->InventoryInternalID)
                    ->where('t_memoin_detail.UomInternalID', $data->UomInternalID)
                    ->whereBetween('t_memoin_header.MemoInDate', array($start, $end))
                    ->sum('Qty');
            $Mout = MemoOutDetail::
                    join('t_memoout_header', 't_memoout_header.InternalID', '=', 't_memoout_detail.MemoOutInternalID')
                    ->where('t_memoout_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_memoout_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_memoout_detail.InventoryInternalID', $data->InventoryInternalID)
                    ->where('t_memoout_detail.UomInternalID', $data->UomInternalID)
                    ->whereBetween('t_memoout_header.MemoOutDate', array($start, $end))
                    ->sum('Qty');
            $Tin = TransferDetail::
                    join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                    ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_transfer_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_transfer_detail.InventoryInternalID', $data->InventoryInternalID)
                    ->where('t_transfer_detail.UomInternalID', $data->UomInternalID)
                    ->whereBetween('t_transfer_header.TransferDate', array($start, $end))
                    ->sum('Qty');
            $Tout = TransferDetail::
                    join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                    ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_transfer_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_transfer_detail.InventoryInternalID', $data->InventoryInternalID)
                    ->where('t_transfer_detail.UomInternalID', $data->UomInternalID)
                    ->whereBetween('t_transfer_header.TransferDate', array($start, $end))
                    ->sum('Qty');
            $initialStock = $initialStockTamp;
            if ($data->Default == 0) {
                $initialStock = 0;
            }
            $endStock = $initialStock + $pembelian + $Rpenjualan - $penjualan - $Rpembelian + $Min - $Mout + $Tin - $Tout;
            $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->inventory->InventoryID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->inventory->InventoryName . ' | ' . $data->uom->UomID . ' (' . number_format($data->Value, 0, '.', ',') . ')</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($initialStock, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($pembelian, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($penjualan, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($Rpembelian, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($Rpenjualan, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($Min, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($Mout, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($Tin, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($Tout, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($endStock, '0', '.', ',')) . '</td>
                            </tr>';

            $totalPieces += $endStock * $data->Value;
        }

        $html .= '<tr> 
                            <td colspan="12" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right">Summary : ';
        foreach (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('InventoryInternalID', $inventory->InternalID)
                ->orderBy('Value', 'desc')->get() as $loop) {
            $html .= number_format((int) ($totalPieces / $loop->Value), 0, '.', ',') . ' ' . $loop->uom->UomID . ' ';
            $totalPieces = $totalPieces % $loop->Value;
        }
        $html .= '</td>
                        </tr>';
        $html .= '</tbody></table>';


        $html .= '
                            </div>
                    </div>
                </body>
            </html>';
        return PDF::load($html, 'A4', 'landscape')->download('inventory_stock_report');
    }

    //kartu stok per barang yang baru 
    public function stockInventory() {
        Excel::create('Kartu_Stock', function($excel) {
            $excel->sheet('Kartu_Stock', function($sheet) {
                $inventory = Inventory::find(Input::get('InternalID'));
                $startT = explode('-', Input::get('sDate'));
                $endT = explode('-', Input::get('eDate'));
                $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
                $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
                $warehouse = Input::get('warehouse');
                $wherewarehouse = '';
                if ($warehouse == '-1') {
                    $delimiterWarehouse = '!=';
                    $warehouseName = 'All';
                    $wherewarehouse = "m_warehouse.Type = " . Auth::user()->WarehouseCheck;
                } else {
                    $delimiterWarehouse = '=';
                    $warehouseName = Warehouse::find($warehouse)->WarehouseName;
                    $wherewarehouse = "m_warehouse.Type > -1";
                }

                if ($warehouse != '-1') {
                    $huruf = 'H';
                } else {
                    if (count(Warehouse::all()) == 8) {
                        $huruf = 'P';
                    } else if (count(Warehouse::all()) == 9) {
                        $huruf = 'Q';
                    } else if (count(Warehouse::all()) == 10) {
                        $huruf = 'R';
                    } else if (count(Warehouse::all()) == 11) {
                        $huruf = 'S';
                    } else if (count(Warehouse::all()) == 12) {
                        $huruf = 'T';
                    }
                }
                $sheet->mergeCells('B1:' . $huruf . '1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Kartu Stock " . $inventory->InventoryName);
                $sheet->setCellValueByColumnAndRow(1, 2, "Warehouse: " . $warehouseName);
                $sheet->setCellValueByColumnAndRow(1, 3, "Tanggal");
                $sheet->setCellValueByColumnAndRow(2, 3, "ID Transaksi");
                $sheet->setCellValueByColumnAndRow(3, 3, "Description");
                $sheet->setCellValueByColumnAndRow(4, 3, "Masuk");
                $sheet->setCellValueByColumnAndRow(5, 3, "Keluar");
                $sheet->setCellValueByColumnAndRow(6, 3, "Balance");
                if ($warehouse != '-1') {
                    $sheet->setCellValueByColumnAndRow(7, 3, "Keterangan");
                } else {
                    $warecount = 7;
                    foreach (Warehouse::where("Type", Auth::user()->WarehouseCheck)->orderBy('InternalID', 'asc')->get() as $w) {
                        $sheet->setCellValueByColumnAndRow($warecount, 3, $w->WarehouseName);
                        //hitung initial stok perwarehouse disini
                        $arrstockware[$w->InternalID] = getInitialStockInventoryWithWarehouse($inventory->InternalID, $w->InternalID, $start);
                        $warecount++;
                    }
                    $sheet->setCellValueByColumnAndRow($warecount, 3, "Keterangan");
                }
                $row = 4;
                //hitung initial stok smua disini
                $balance = getInitialStockInventory($inventory->InternalID, $start);

                foreach (DB::select(DB::raw("SELECT * FROM v_wreportstock INNER JOIN m_warehouse ON m_warehouse.InternalID = v_wreportstock.Warehouse WHERE Warehouse $delimiterWarehouse $warehouse AND Inventory = '$inventory->InternalID' AND Date BETWEEN '$start' AND '$end' and $wherewarehouse")) as $data) {
//                    if (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->where('InventoryInternalID', $data->InternalID)->count() > 0) {
//                        $totalPieces = 0;
//                        $default = '';
//                        $color = "";
//                        $initialStockTamp = Inventory::getInitialStock($date, '<=', $data->InternalID, $warehouse, $delimiterWarehouse);
//                        $default = InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->where('InventoryInternalID', $data->InternalID)->where('Default', 1)->first()->uom->UomID;
//                        $totalPieces = $initialStockTamp;
//                        $incomingStock = Inventory::getIncomingStock($date, '<=', $data->InternalID, $warehouse, $delimiterWarehouse);
//                        if ((int) $data->MinStock > (int) $totalPieces) {
//                            $countMin++;
//                            $color = 'bgcolor="#f2dede"';
//                        } else if ((int) $data->MaxStock < (int) $totalPieces) {
//                            $countMax++;
//                            $color = 'bgcolor="#dff0d8"';
//                        }
//                    }
                    $sheet->setCellValueByColumnAndRow(1, $row, date("d-M-Y", strtotime($data->Date)));
//                    "`" .
                    $sheet->setCellValueByColumnAndRow(2, $row, $data->ID);
                    if ($data->Description != '-')
                        $tmpil = Coa6::find($data->Description)->ACC6Name;
                    else
                        $tmpil = '-';
                    if ($data->Type == "in") {
                        $masuk = $data->Qty;
                        $keluar = '';
                        $balance += $masuk;
                    } else {
                        $masuk = '';
                        $keluar = $data->Qty;
                        $balance -= $keluar;
                    }
                    $sheet->setCellValueByColumnAndRow(3, $row, $tmpil);
                    $sheet->setCellValueByColumnAndRow(4, $row, $masuk);
                    $sheet->setCellValueByColumnAndRow(5, $row, $keluar);
                    $sheet->setCellValueByColumnAndRow(6, $row, $balance);
                    if ($warehouse != '-1') {
                        $sheet->setCellValueByColumnAndRow(7, $row, "");
                    } else {
                        $warecount = 7;
                        foreach (Warehouse::where("Type", Auth::user()->WarehouseCheck)->orderBy('InternalID', 'asc')->get() as $w) {
                            if ($w->InternalID == $data->Warehouse) {
                                $arrstockware[$w->InternalID] += $masuk;
                                $arrstockware[$w->InternalID] -= $keluar;
                                $sheet->setCellValueByColumnAndRow($warecount, $row, $arrstockware[$w->InternalID]);
                            } else {
                                if ($arrstockware[$w->InternalID] != 0)
                                    $sheet->setCellValueByColumnAndRow($warecount, $row, $arrstockware[$w->InternalID]);
                                else
                                    $sheet->setCellValueByColumnAndRow($warecount, $row, "");
                            }
                            $warecount++;
                        }
                        $sheet->setCellValueByColumnAndRow($warecount, $row, "");
                    }
//                    $sheet->setCellValueByColumnAndRow(7, $row, "");
//                    $sheet->setCellValueByColumnAndRow(5, $row, number_format($data->MinStock, 0, '.', ','));
//                    $sheet->setCellValueByColumnAndRow(6, $row, number_format($data->MaxStock, 0, '.', ','));
//                    $sheet->setCellValueByColumnAndRow(7, $row, number_format($totalPieces, 0, '.', ',') . ' ' . $default);
//                    $sheet->setCellValueByColumnAndRow(8, $row, number_format($incomingStock, 0, '.', ',') . ' ' . $default);
                    $row++;
                }

                if ($row == 4) {
                    $sheet->mergeCells('B4:' . $huruf . '4');
                    $sheet->setCellValueByColumnAndRow(1, 4, "Tidak ada transaksi pada tanggal tersebut");

                    $sheet->cells('B4:' . $huruf . '4', function($cells) {
                        $cells->setAlignment('center');
                    });

                    $sheet->setBorder('B3:' . $huruf . $row, 'thin');
                }

                $row--;
                $sheet->setBorder('B3:' . $huruf . $row, 'thin');
                $sheet->cells('B3:' . $huruf . '3', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:' . $huruf . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:' . $huruf . $row, function($cells) {
                    $cells->setAlignment('center');
                });
            });
        })->export('xls');
    }

    public function summaryInventory_lama() {
//        dd(Inventory::all());
//        $aa ="SELECT * FROM m_inventory WHERE CompanyInternalID = ". Auth::user()->Company->InternalID." AND GroupInternalID != 'All'";
//        $query = DB::table('m_inventory')->where('CompanyInternalID', Auth::user()->Company->InternalID)->where('GroupInternalID', '!=', 'All');
//        $statement = $query->getConnection()->getPdo()->prepare($query->toSql());
//        $statement->execute($query->getBindings());
//        while ($record = $statement->fetch(PDO::FETCH_ASSOC)) {
//            dd($record);
//        }
//        $a = DB::connection()->getPdo();
//        $a = DB::select($aa);
//        $a = DB::listen(function ($query) {
//            $query->$aa;
//        });
//        $a = Inventory::all();
//        dd($a);
//        $queries = DB::getQueryLog();
//        echo "<pre>";
//        dd($queries);
        Excel::create('Inventory Summary Report', function($excel) {

            $excel->sheet('Inventory Summary Report', function($sheet) {
                $zero = Input::get('nilai0');
                $dateT = explode('-', Input::get('date'));
                $date = $dateT[2] . '-' . $dateT[1] . '-' . $dateT[0];
                $warehouse = Input::get('warehouse');
                $group = Input::get('group');
                if ($warehouse == '-1') {
                    $delimiterWarehouse = '!=';
                    $warehouseName = 'All';
                } else {
                    $delimiterWarehouse = '=';
                    $warehouseName = Warehouse::find($warehouse)->WarehouseName;
                }
                if ($group == '-1') {
                    $delimiterGroup = '!=';
                    $groupName = 'All';
                } else {
                    $delimiterGroup = '=';
                    $groupName = Group::find($group)->GroupName;
                }
                $sheet->mergeCells('B1:F1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Inventory Summary Report");
                $sheet->mergeCells('B2:C2');
                $sheet->setCellValueByColumnAndRow(1, 2, "Warehouse :");
                $sheet->mergeCells('D2:E2');
                $sheet->setCellValueByColumnAndRow(3, 2, $warehouseName);
                $sheet->mergeCells('B3:C3');
                $sheet->setCellValueByColumnAndRow(1, 3, "Group :");
                $sheet->mergeCells('D3:E3');
                $sheet->setCellValueByColumnAndRow(3, 3, $groupName);
                $sheet->mergeCells('B4:C4');
                $sheet->setCellValueByColumnAndRow(1, 4, "Period :");
                $sheet->mergeCells('D4:E4');
                $sheet->setCellValueByColumnAndRow(3, 4, date("d-M-Y", strtotime(Input::get('date'))));
                $sheet->setCellValueByColumnAndRow(1, 5, "No.");
                $sheet->setCellValueByColumnAndRow(2, 5, "Inventory ID");
                $sheet->setCellValueByColumnAndRow(3, 5, "Name");
                $sheet->setCellValueByColumnAndRow(4, 5, "UOM (Default)");
                $sheet->setCellValueByColumnAndRow(5, 5, "End Stock");
                $sheet->setCellValueByColumnAndRow(6, 5, "Stock from child");
                $sheet->setCellValueByColumnAndRow(7, 5, "In Order");

                $row = 3;
                $i = 6;
                $sheet->cells('B', function($cells) {
                    $cells->setAlignment('center');
                    $cells->setValignment('middle');
                });
                $sheet->cells('G', function($cells) {
                    $cells->setAlignment('center');
                    $cells->setValignment('middle');
                });
                if (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() > 0) {
//                    dd(Inventory::where('CompanyInternalID', Auth::user()->Company->InternalID)->where('GroupInternalID', $delimiterGroup, $group)->count());
//                    foreach (Inventory::where('CompanyInternalID', Auth::user()->Company->InternalID)->where('GroupInternalID', $delimiterGroup, $group)->get() as $value) {
                    $query = DB::table('m_inventory')->where('CompanyInternalID', Auth::user()->Company->InternalID)->where('GroupInternalID', $delimiterGroup, $group);
                    $statement = $query->getConnection()->getPdo()->prepare($query->toSql());
                    $statement->execute($query->getBindings());
                    while ($value = $statement->fetch(PDO::FETCH_ASSOC)) {
//                        echo "<pre>";
//                        dd($value["InternalID"]);
                        if (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->where('InventoryInternalID', $value['InternalID'])->count() > 0) {
                            $totalPieces = 0;
                            $default = '';
                            $initialStockTamp = Inventory::getInitialStock($date, '<=', $value['InternalID'], $warehouse, $delimiterWarehouse);
                            foreach (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->where('InventoryInternalID', $value['InternalID'])->get() as $data) {
                                $initialStock = $initialStockTamp;
                                if ($data->Default == 1) {
                                    $default = $data->uom->UomID;
                                }
                            }
                            $totalPieces = $initialStock;
                            if ($zero != 'nilai0' || ($totalPieces != '' && $totalPieces != 0)) {
                                $sheet->setCellValueByColumnAndRow(1, $i, $row - 2);
                                $sheet->setCellValueByColumnAndRow(2, $i, $data->inventory->InventoryID);
                                $sheet->setCellValueByColumnAndRow(3, $i, $data->inventory->InventoryName);
                                $sheet->setCellValueByColumnAndRow(4, $i, $default);

                                $html = number_format($totalPieces, 0, '.', ',') . ' ' . $default . ', equal with ';
                                foreach (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)
                                        ->where('InventoryInternalID', $value['InternalID'])
                                        ->orderBy('Value', 'desc')->get() as $loop) {
                                    $html .= number_format((int) ($totalPieces / $loop->Value), 0, '.', ',') . ' ' . $loop->uom->UomID . ' ';
                                    $totalPieces = $totalPieces % $loop->Value;
                                }

                                $sheet->setCellValueByColumnAndRow(5, $i, $html);

                                $jumlahMin = 0;
                                $uom = '';
                                foreach (InventoryChild::where('InventoryInternalID', $value['InternalID'])
                                        ->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $child) {
                                    $i++;
                                    $inv = Inventory::where('InternalID', $child->ChildInventoryInternalID)
                                                    ->where('CompanyInternalID', Auth::user()->Company->InternalID)->first();
                                    $totalPieces = Inventory::getInitialStock($date, '<=', $inv->InternalID, $warehouse, $delimiterWarehouse);
                                    foreach (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->where('InventoryInternalID', $inv->InternalID)->get() as $data) {
                                        if ($data->Default == 1) {
                                            $default = $data->uom->UomID;
                                            $uom = $default;
                                        }
                                    }
                                    $sheet->setCellValueByColumnAndRow(1, $i, '-');
                                    $sheet->cells('B' . $i, function($cells) {
                                        $cells->setAlignment('right');
                                    });
                                    $sheet->setCellValueByColumnAndRow(2, $i, $inv->InventoryID);
                                    $sheet->setCellValueByColumnAndRow(3, $i, $inv->InventoryName);
                                    $sheet->setCellValueByColumnAndRow(4, $i, $default);
                                    if ($jumlahMin == 0) {
                                        $jumlahMin = floor($totalPieces / $child->Qty);
                                    } else {
                                        if (floor($totalPieces / $child->Qty) < $jumlahMin) {
                                            $jumlahMin = floor($totalPieces / $child->Qty);
                                        }
                                    }
                                    $sheet->setCellValueByColumnAndRow(6, $i, '-');
                                    $html = number_format($totalPieces, 0, '.', ',') . ' ' . $default . ', equal with ';
                                    foreach (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)
                                            ->where('InventoryInternalID', $inv->InternalID)
                                            ->orderBy('Value', 'desc')->get() as $loop) {
                                        $html .= number_format((int) ($totalPieces / $loop->Value), 0, '.', ',') . ' ' . $loop->uom->UomID . ' ';
                                        $totalPieces = $totalPieces % $loop->Value;
                                    }
                                    $sheet->setCellValueByColumnAndRow(5, $i, $html);
                                }
                                $i = $i - InventoryChild::where('InventoryInternalID', $value['InternalID'])
                                                ->where('CompanyInternalID', Auth::user()->Company->InternalID)->count();
                                $sheet->setCellValueByColumnAndRow(6, $i, $jumlahMin . ' ' . $default); //blom
                                $i = $i + InventoryChild::where('InventoryInternalID', $value['InternalID'])
                                                ->where('CompanyInternalID', Auth::user()->Company->InternalID)->count();
                                $sum = MrvHeader::join('t_mrv_detail', 't_mrv_detail.MrvInternalID', '=', 't_mrv_header.InternalID')
                                        ->WhereNotIn('t_mrv_header.InternalID', PurchaseHeader::lists('MrvInternalID'))
                                        ->sum('Qty');
                                $sheet->setCellValueByColumnAndRow(7, $i, $sum);

                                $row++;
                                $i++;
                            }
                        }
//                        $row++;
//                        $i++;
                    }
                } else {
                    $sheet->mergeCells('D5:H5');
                    $sheet->setCellValueByColumnAndRow(1, $i, "There is no inventory registered.");
                }


                if (Inventory::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() <= 0) {
                    $sheet->mergeCells('B5:H5');
                    $sheet->setCellValueByColumnAndRow(1, 5, "No data available in table");

                    $sheet->cells('B5:H5', function($cells) {
                        $cells->setAlignment('center');
                    });

                    $sheet->setBorder('B5:H5' . $row, 'thin');
                }

//                $row--;
//                dd($row);
                $row++;
                $row++;
                $sheet->setBorder('B5:H' . $row, 'thin');
                $sheet->cells('B5:H5', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
//                $sheet->cells('B5:G' . $row, function($cells) {
//                    $cells->setAlignment('left');
//                    $cells->setValignment('middle');
//                });
                $sheet->cells('B5:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
            });
        })->export('xls');
        /* $dateT = explode('-', Input::get('date'));
          $date = $dateT[2] . '-' . $dateT[1] . '-' . $dateT[0];
          $warehouse = Input::get('warehouse');
          if ($warehouse == '-1') {
          $delimiterWarehouse = '!=';
          $warehouseName = 'All';
          } else {
          $delimiterWarehouse = '=';
          $warehouseName = Warehouse::find($warehouse)->WarehouseName;
          }
          $html = '
          <html>
          <head>
          <style>
          .tableBorder {
          border-spacing: 0;
          border: 0px;
          }
          .tableBorder th{
          padding: 3px;
          border-spacing: 0;
          border: 0.5px solid #ddd;
          text-align: center;
          }
          .tableBorder td{
          border-spacing: 0;
          padding: 8px;
          border: 0.5px solid #ddd;
          }
          .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:80%; bottom: -63px; right: 0px; height: 78px;}
          .footer .page:after {
          font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
          </style>
          </head>
          <body>
          <div class="footer">
          <span class="page"></span>
          </div>
          <div style="padding:0px; box-sizing: border-box;">
          <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
          <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
          <div style="width: 418px; float: left;">
          <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
          </div>
          <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
          <table>
          <tr style="background: none;">
          <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
          <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
          <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
          </tr>
          <tr style="background: none;">
          <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
          <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
          <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
          </tr>
          <tr style="background: none;">
          <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
          <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
          <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
          </tr>
          </table>
          </div>
          </div>
          <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Inventory Summary Report</h5>
          <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Warehouse ' . $warehouseName . '</span><br>
          <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('date'))) . '</span>
          <table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
          <thead >
          <tr style="border-collapse: separate; border: 0px solid #ddd;">
          <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory ID</th>
          <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Name</th>
          <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Uom (Default)</th>
          <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">End Stock</th>
          </tr>
          </thead>
          <tbody>';
          if (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() > 0) {
          foreach (Inventory::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $value) {
          if (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->where('InventoryInternalID', $value->InternalID)->count() > 0) {
          $totalPieces = 0;
          $default = '';
          $initialStockTamp = Inventory::getInitialStock($date, '<=', $value->InternalID, $warehouse, $delimiterWarehouse);
          foreach (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->where('InventoryInternalID', $value->InternalID)->get() as $data) {
          $initialStock = $initialStockTamp;
          if ($data->Default == 1) {
          $default = $data->uom->UomID;
          }
          }
          $totalPieces = $initialStock;
          $html.= '<tr>
          <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->inventory->InventoryID . '</td>
          <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->inventory->InventoryName . '</td>
          <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $default . '</td>
          <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($totalPieces, 0, '.', ',') . ' ' . $default . ', equal with ';
          foreach (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)
          ->where('InventoryInternalID', $value->InternalID)
          ->orderBy('Value', 'desc')->get() as $loop) {
          $html .= number_format((int) ($totalPieces / $loop->Value), 0, '.', ',') . ' ' . $loop->uom->UomID . ' ';
          $totalPieces = $totalPieces % $loop->Value;
          }
          $html.='</td>
          </tr>';
          }
          }
          } else {
          $html.= '<tr>
          <td colspan="4" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;  text-align: center">There is no inventory registered.</td>
          </tr>';
          }

          $html.= '</tbody>
          </div>
          </div>
          </body>
          </html>';
          return PDF ::load($html, 'A4', 'portrait')->download('inventory_summary_report'); */
    }

    public function summaryView() {
        $zero = Input::get('nilai0');
        $dateT = explode('-', Input::get('date'));
        $date = $dateT[2] . '-' . $dateT[1] . '-' . $dateT[0];
        $warehouse = Input::get('warehouse');
        $group = Input::get('group');
        if ($warehouse == '-1') {
            $delimiterWarehouse = '!=';
            $warehouseName = 'All';
        } else {
            $delimiterWarehouse = '=';
            $warehouseName = Warehouse::find($warehouse)->WarehouseName;
        }
        if ($warehouse != '-1') {
            $huruf = 'H';
        } else {
            if (count(Warehouse::all()) == 8) {
                $huruf = 'P';
            } else if (count(Warehouse::all()) == 9) {
                $huruf = 'Q';
            } else if (count(Warehouse::all()) == 10) {
                $huruf = 'R';
            } else if (count(Warehouse::all()) == 11) {
                $huruf = 'S';
            } else if (count(Warehouse::all()) == 12) {
                $huruf = 'T';
            }
        }
        if ($group == '-1') {
            $delimiterGroup = '!=';
            $groupName = 'All';
        } else {
            $delimiterGroup = '=';
            $groupName = Group::find($group)->GroupName;
        }
        $html = '
          <html>
          <head>
          <style>
          .tableBorder {
          border-spacing: 0;
          border: 0px;
          }
          .tableBorder th{
          padding: 3px;
          border-spacing: 0;
          border: 0.5px solid #ddd;
          text-align: center;
          }
          .tableBorder td{
          border-spacing: 0;
          padding: 8px;
          border: 0.5px solid #ddd;
          }
          .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:80%; bottom: -63px; right: 0px; height: 78px;}
          .footer .page:after {
          font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
          </style>
          </head>
          <body>
          <div class="footer">
          <span class="page"></span>
          </div>
          <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Inventory Summary Report</h5>
          <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Warehouse ' . $warehouseName . '</span><br>
          <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('date'))) . '</span>
          <table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
          <thead >
          <tr style="border-collapse: separate; border: 0px solid #ddd;">
          <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory ID</th>
          <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Name</th>
          <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">End Stock</th>';
        if ($warehouse == '-1') {
            foreach (Warehouse::orderBy('InternalID', 'asc')->get() as $w) {
                $html .= '<th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">' . $w->WarehouseName . '</th>';
            }
        }
        $html .= '</tr>
          </thead>
          <tbody>';
        if (Inventory::where('GroupInternalID', $delimiterGroup, $group)->count() > 0) {
            $query = DB::table('m_inventory')->where('GroupInternalID', $delimiterGroup, $group);
            $statement = $query->getConnection()->getPdo()->prepare($query->toSql());
            $statement->execute($query->getBindings());

            while ($data = $statement->fetch(PDO::FETCH_ASSOC)) {
                //Do whatever you want, one row at a time
//            foreach (Inventory::where('GroupInternalID', $delimiterGroup, $group)->get() as $data) {
                $endstock = Inventory::getEndStockNow($data["InternalID"], $date, $warehouse, $delimiterWarehouse);
                if (($zero != 'nilai0' && $endstock != 0) || $zero == 'nilai0') {
                    $html .= '<tr>
          <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data["InventoryID"] . '</td>
          <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data["InventoryName"] . '</td>
          <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($endstock, 0, '.', ',');
                    if ($warehouse == '-1') {
                        foreach (Warehouse::orderBy('InternalID', 'asc')->get() as $w) {
                            $stockwarehouse = Inventory::stockWarehouseNow($data["InternalID"], $date, $w->InternalID);
                            $html .= '<th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">' . $stockwarehouse . '</th>';
                        }
                    }
                    $html .= '</td>
          </tr>';
                }
            }
        } else {
            $html .= '<tr>
          <td colspan="3" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;  text-align: center">There is no inventory registered.</td>
          </tr>';
        }

        $html .= '</tbody>
          </div>
          </div>
          </body>
          </html>';
        return $html;
    }

    public function summaryInventory() {
//        dd(DB::table('v_wreportstock')->where("Inventory",">",0)->get());
        Excel::create('Inventory Summary Report', function($excel) {
            $excel->sheet('Inventory Summary Report', function($sheet) {
                $zero = Input::get('nilai0');
                $dateT = explode('-', Input::get('date'));
                $date = $dateT[2] . '-' . $dateT[1] . '-' . $dateT[0];
                $warehouse = Input::get('warehouse');
                $group = Input::get('group');
                if ($warehouse == '-1') {
                    $delimiterWarehouse = '!=';
                    $warehouseName = 'All';
                } else {
                    $delimiterWarehouse = '=';
                    $warehouseName = Warehouse::find($warehouse)->WarehouseName;
                }

                if ($warehouse != '-1') {
                    $huruf = 'H';
                } else {
                    if (count(Warehouse::all()) == 8) {
                        $huruf = 'P';
                    } else if (count(Warehouse::all()) == 9) {
                        $huruf = 'Q';
                    } else if (count(Warehouse::all()) == 10) {
                        $huruf = 'R';
                    } else if (count(Warehouse::all()) == 11) {
                        $huruf = 'S';
                    } else if (count(Warehouse::all()) == 12) {
                        $huruf = 'T';
                    }
                }
                if ($group == '-1') {
                    $delimiterGroup = '!=';
                    $groupName = 'All';
                } else {
                    $delimiterGroup = '=';
                    $groupName = Group::find($group)->GroupName;
                }
                $sheet->mergeCells('B1:F1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Inventory Summary Report");
                $sheet->mergeCells('B2:C2');
                $sheet->setCellValueByColumnAndRow(1, 2, "Warehouse :");
                $sheet->mergeCells('D2:E2');
                $sheet->setCellValueByColumnAndRow(3, 2, $warehouseName);
                $sheet->mergeCells('B3:C3');
                $sheet->setCellValueByColumnAndRow(1, 3, "Group :");
                $sheet->mergeCells('D3:E3');
                $sheet->setCellValueByColumnAndRow(3, 3, $groupName);
                $sheet->mergeCells('B4:C4');
                $sheet->setCellValueByColumnAndRow(1, 4, "Period :");
                $sheet->mergeCells('D4:E4');
                $sheet->setCellValueByColumnAndRow(3, 4, date("d-M-Y", strtotime(Input::get('date'))));
                $sheet->setCellValueByColumnAndRow(1, 5, "No.");
                $sheet->setCellValueByColumnAndRow(2, 5, "Inventory ID");
                $sheet->setCellValueByColumnAndRow(3, 5, "Name");
                $sheet->setCellValueByColumnAndRow(4, 5, "End Stock");
                if ($warehouse == '-1') {
                    $warecount = 5;
                    foreach (Warehouse::orderBy('InternalID', 'asc')->get() as $w) {
                        $sheet->setCellValueByColumnAndRow($warecount, 5, $w->WarehouseName);
                        $warecount++;
                    }
                }

                $row = 3;
                $i = 6;
                $sheet->cells('B', function($cells) {
                    $cells->setAlignment('center');
                    $cells->setValignment('middle');
                });
                $sheet->cells('G', function($cells) {
                    $cells->setAlignment('center');
                    $cells->setValignment('middle');
                });
                if (Inventory::where('GroupInternalID', $delimiterGroup, $group)->count() > 0) {
                    foreach (Inventory::where('GroupInternalID', $delimiterGroup, $group)->get() as $data) {
                        $endstock = Inventory::getEndStockNow($data->InternalID, $date, $warehouse, $delimiterWarehouse);
                        if (($zero != 'nilai0' && $endstock != 0) || $zero == 'nilai0') {
                            $sheet->setCellValueByColumnAndRow(1, $i, $i - 5);
                            $sheet->setCellValueByColumnAndRow(2, $i, $data->InventoryID);
                            $sheet->setCellValueByColumnAndRow(3, $i, $data->InventoryName);
                            $sheet->setCellValueByColumnAndRow(4, $i, $endstock);
                            if ($warehouse == '-1') {
                                $warecount = 5;
                                foreach (Warehouse::orderBy('InternalID', 'asc')->get() as $w) {
                                    //hitung stok perwarehouse
                                    $stockwarehouse = Inventory::stockWarehouseNow($data->InternalID, $date, $w->InternalID);
                                    $sheet->setCellValueByColumnAndRow($warecount, $i, $stockwarehouse);
                                    $warecount++;
                                }
                            }
                            $i++;
                        }
                    }
                } else {
                    $sheet->mergeCells('D5:H5');
                    $sheet->setCellValueByColumnAndRow(1, $i, "There is no inventory registered.");
                }


                if (Inventory::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() <= 0) {
                    $sheet->mergeCells('B5:H5');
                    $sheet->setCellValueByColumnAndRow(1, 5, "No data available in table");

                    $sheet->cells('B5:H5', function($cells) {
                        $cells->setAlignment('center');
                    });

                    $sheet->setBorder('B5:H5' . $row, 'thin');
                }
//                $row++;
//                $row++;
//                $sheet->setBorder('B5:H' . $row, 'thin');
//                $sheet->cells('B5:H5', function($cells) {
//                    $cells->setBackground('#eaf6f7');
//                    $cells->setValignment('middle');
//                    $cells->setAlignment('center');
//                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
//                $sheet->cells('B5:B' . $row, function($cells) {
//                    $cells->setAlignment('center');
//                });
            });
        })->export('xls');
    }

    public function reportStock() {

        Excel::create('Buffer_Report', function($excel) {
            $excel->sheet('Buffer_Report', function($sheet) {

                $dateT = explode('-', Input::get('date'));
                $date = $dateT[2] . '-' . $dateT[1] . '-' . $dateT[0];
                $warehouse = Input::get('warehouse');
                if ($warehouse == '-1') {
                    $delimiterWarehouse = '!=';
                    $warehouseName = 'All';
                } else {
                    $delimiterWarehouse = '=';
                    $warehouseName = Warehouse::find($warehouse)->WarehouseName;
                }
                $sheet->mergeCells('B1:H1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Buffer Stock Inventory");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Inventory ID");
                $sheet->setCellValueByColumnAndRow(3, 2, "Inventory Name");
                $sheet->setCellValueByColumnAndRow(4, 2, "UoM (Default)");
                $sheet->setCellValueByColumnAndRow(5, 2, "Min Stock");
                $sheet->setCellValueByColumnAndRow(6, 2, "Max Stock");
                $sheet->setCellValueByColumnAndRow(7, 2, "Stock");
                $sheet->setCellValueByColumnAndRow(8, 2, "Incoming Stock");
                $row = 3;
                $countMax = 0;
                $countMin = 0;
                $query = DB::table('m_inventory');
                $statement = $query->getConnection()->getPdo()->prepare($query->toSql());
                $statement->execute($query->getBindings());

                while ($data = $statement->fetch(PDO::FETCH_ASSOC)) {
//                foreach (Inventory::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                    if (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->where('InventoryInternalID', $data["InternalID"])->count() > 0) {
                        $totalPieces = 0;
                        $default = '';
                        $color = "";
                        $initialStockTamp = Inventory::getInitialStock($date, '<=', $data["InternalID"], $warehouse, $delimiterWarehouse);
                        $default = InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->where('InventoryInternalID', $data["InternalID"])->where('Default', 1)->first()->uom->UomID;
                        $totalPieces = $initialStockTamp;
                        $incomingStock = Inventory::getIncomingStock($date, '<=', $data["InternalID"], $warehouse, $delimiterWarehouse);
                        if ((int) $data["MinStock"] > (int) $totalPieces) {
                            $countMin++;
                            $color = 'bgcolor="#f2dede"';
                        } else if ((int) $data["MaxStock"] < (int) $totalPieces) {
                            $countMax++;
                            $color = 'bgcolor="#dff0d8"';
                        }
                    }
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, "`" . $data["InventoryID"]);
                    $sheet->setCellValueByColumnAndRow(3, $row, $data["InventoryName"]);
                    $sheet->setCellValueByColumnAndRow(4, $row, $default);
                    $sheet->setCellValueByColumnAndRow(5, $row, number_format($data["MinStock"], 0, '.', ','));
                    $sheet->setCellValueByColumnAndRow(6, $row, number_format($data["MaxStock"], 0, '.', ','));
                    $sheet->setCellValueByColumnAndRow(7, $row, number_format($totalPieces, 0, '.', ',') . ' ' . $default);
                    $sheet->setCellValueByColumnAndRow(8, $row, number_format($incomingStock, 0, '.', ',') . ' ' . $default);
                    $row++;
                }

                if (Inventory::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() <= 0) {
                    $sheet->mergeCells('B3:I3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "No data available in table");

                    $sheet->cells('B3:I3', function($cells) {
                        $cells->setAlignment('center');
                    });

                    $sheet->setBorder('B3:I' . $row, 'thin');
                }

                $row--;
                $sheet->setBorder('B2:I' . $row, 'thin');
                $sheet->cells('B2:I2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:I' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:I' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
            });
        })->export('xls');
        /* $html = '
          <html>
          <head>
          <style>
          .tableBorder {
          border-spacing: 0;
          border: 0px;
          }
          .tableBorder th{
          padding: 3px;
          border-spacing: 0;
          border: 0.5px solid #ddd;
          text-align: center;
          }
          .tableBorder td{
          border-spacing: 0;
          padding: 8px;
          border: 0.5px solid #ddd;
          }
          .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:80%; bottom: -63px; right: 0px; height: 78px;}
          .footer .page:after {
          font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
          </style>
          </head>
          <body>
          <div class="footer">
          <span class="page"></span>
          </div>
          <div style="padding:0px; box-sizing: border-box;">
          <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
          <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
          <div style="width: 418px; float: left;">
          <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
          </div>
          <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
          <table>
          <tr style="background: none;">
          <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
          <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
          <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
          </tr>
          <tr style="background: none;">
          <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
          <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
          <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
          </tr>
          <tr style="background: none;">
          <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
          <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
          <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
          </tr>
          </table>
          </div>
          </div>
          <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Buffer Stock Report</h5>
          <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Warehouse ' . $warehouseName . '</span><br>
          <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('date'))) . '</span>
          <table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
          <thead >
          <tr style="border-collapse: separate; border: 0px solid #ddd;">
          <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory ID</th>
          <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Name</th>
          <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Uom (Default)</th>
          <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Min Stock</th>
          <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Max Stock</th>
          <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Stock</th>
          </tr>
          </thead>
          <tbody>';
          if (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() > 0) {
          $countMax = 0;
          $countMin = 0;
          foreach (Inventory::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $value) {
          if (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->where('InventoryInternalID', $value->InternalID)->count() > 0) {
          $totalPieces = 0;
          $default = '';
          $color = "";
          $initialStockTamp = Inventory::getInitialStock($date, '<=', $value->InternalID, $warehouse, $delimiterWarehouse);
          $default = InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->where('InventoryInternalID', $value->InternalID)->where('Default', 1)->first()->uom->UomID;
          $totalPieces = $initialStockTamp;
          if ((int) $value->MinStock > (int) $totalPieces) {
          $countMin++;
          $color = 'bgcolor="#f2dede"';
          } else if ((int) $value->MaxStock < (int) $totalPieces) {
          $countMax++;
          $color = 'bgcolor="#dff0d8"';
          }

          $html.= '<tr>
          <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; ">' . $value->InventoryID . '</td>
          <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; ">' . $value->InventoryName . '</td>
          <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; ">' . $default . '</td>
          <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; ">' . number_format($value->MinStock, 0, '.', ',') . '</td>
          <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; ">' . number_format($value->MaxStock, 0, '.', ',') . '</td>
          <td ' . $color . ' style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right; ">' . number_format($totalPieces, 0, '.', ',') . ' ' . $default . '</td>
          </tr>';
          }
          }
          $html.= '<tr>
          <td colspan="5" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; color: red; text-align: right;"> * Warning Stock </td>
          <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;  text-align: right">' . $countMin . '</td>
          </tr>
          <tr>
          <td colspan="5" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; color: green; text-align: right;"> * Exceed Stock </td>
          <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;  text-align: right">' . $countMax . '</td>
          </tr>';
          } else {
          $html.= '<tr>
          <td colspan="6" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;  text-align: center">There is no inventory registered.</td>
          </tr>';
          }

          $html.= '</tbody>
          </div>
          </div>
          </body>
          </html>';
          return PDF ::load($html, 'A4', 'portrait')->download('buffer_stock_report'); */
    }

    public function historyPrice($id) {
        $inv = Inventory::where('InventoryID', $id)->where('CompanyInternalID', Auth::user()->Company->InternalID)->first();
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">History Price - ' . $inv->InventoryName . ' (' . $inv->InventoryID . ')</h5>';
        $html .= '<table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Customer</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Currency</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Rate</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Uom</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Qty</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Price</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc (%)</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
        if (SalesDetail::join('t_sales_header', 't_sales_header.InternalID', '=', 't_sales_detail.SalesInternalID')
                        ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_sales_header.WarehouseInternalID')
                        ->where("m_warehouse.Type", Auth::user()->WarehouseCheck)
                        ->where('InventoryInternalID', $inv->InternalID)->count() > 0) {
            foreach (SalesDetail::join('t_sales_header', 't_sales_header.InternalID', '=', 't_sales_detail.SalesInternalID')
                    ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_sales_header.WarehouseInternalID')
                    ->where("m_warehouse.Type", Auth::user()->WarehouseCheck)
                    ->where('InventoryInternalID', $inv->InternalID)->orderBy('SalesDate', 'desc')->get() as $data) {
                $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->SalesID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . Coa6::find($data->ACC6InternalID)->ACC6ID . ' ' . Coa6::find($data->ACC6InternalID)->ACC6Name . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . Currency::find($data->CurrencyInternalID)->CurrencyName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->CurrencyRate, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->uom->UomID . ' (' . number_format(InventoryUom::where('InventoryInternalID', $data->InventoryInternalID)->where('UomInternalID', $data->UomInternalID)->first()->Value, 0, '.', ',') . ')</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Discount . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->DiscountNominal . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
            }
        } else {
            $html .= '<tr>
                            <td colspan="10" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: center">There is no sales registered.</td>
                        </tr>';
        }

        $html .= '</tbody>
            </table>';
        $html .= '</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('history_price');
    }

//=======================================ajax==========================================
    public function generateInventoryID() {
        $variety = Variety::find(Input::get("idVar"));
        $brand = Brand::find(Input::get("idBrand"));
        $text = $variety->Group->GroupID . $variety->VarietyID . $brand->BrandID . ".";
        $generateID = $this->getNextIDInventory($text);
        return trim($generateID);
    }

    public static function getNextIDInventory($text) {
        $query = 'SELECT InventoryID From m_inventory Where InventoryID LIKE "' . $text . '%" AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" order by InventoryID desc';
        $InventoryID = DB::select(DB::raw($query));

        if (count($InventoryID) <= 0) {
            $InventoryID = '';
        } else {
            $InventoryID = $InventoryID[0]->InventoryID;
        }
        if ($InventoryID == '') {
            $InventoryID = $text . '0001';
        } else {
            $textTamp = $InventoryID;
            $InventoryID = substr($textTamp, (strlen($textTamp) - 4), 4) + 1;
            $InventoryID = str_pad($InventoryID, 4, '0', STR_PAD_LEFT);
            $InventoryID = $text . $InventoryID;
        }
        return $InventoryID;
    }

    public function checkInventoryID() {
        $InventoryName = "";
        $inventory = Inventory::where("InventoryID", Input::get("id"))->where("CompanyInternalID", Auth::user()->Company->InternalID)->count();
        if ($inventory > 0) {
            return "1";
        } else {
            return "0";
        }
    }

    public function checkInventoryHasBarcode() {
        $InventoryName = "";
        $inventory = Inventory::where("BarcodeCode", Input::get("id"))->where("CompanyInternalID", Auth::user()->Company->InternalID)->get();
        $parcel = Parcel::where("BarcodeCode", Input::get("id"))->where("CompanyInternalID", Auth::user()->Company->InternalID)->get();
        if (count($inventory) == 0 && count($parcel) == 0) {
            $InventoryName = "0";
        } else {
            if (count($inventory) > 0) {
                $InventoryName = $inventory[0]->InventoryID . " - " . $inventory[0]->InventoryName;
            } else if (count($parcel) > 0) {
                $InventoryName = $parcel[0]->ParcelID . " - " . $parcel[0]->ParcelName;
            }
        }
        return $InventoryName;
    }

    public function checkInventoryHasBarcodeUpdate() {
        $InventoryName = "";
        $inventory = Inventory::where("BarcodeCode", Input::get("id"))
                        ->where("InternalID", "!=", Input::get("inventory"))
                        ->where("CompanyInternalID", Auth::user()->Company->InternalID)->get();
        $parcel = Parcel::where("BarcodeCode", Input::get("id"))->where("CompanyInternalID", Auth::user()->Company->InternalID)->get();
        if (count($inventory) == 0 && count($parcel) == 0) {
            $InventoryName = "0";
        } else {
            if (count($inventory) > 0) {
                $InventoryName = $inventory[0]->InventoryID . " - " . $inventory[0]->InventoryName;
            } else if (count($parcel) > 0) {
                $InventoryName = $parcel[0]->ParcelID . " - " . $parcel[0]->ParcelName;
            }
        }
        return $InventoryName;
    }

    public function getInventoryQRCode() {
        $inventory = Inventory::find(Input::get("id"));
        return DNS1D::getBarcodeHTML($inventory->BarcodeCode, "EAN13", 1, 33);
    }

    static function inventoryDataBackup() {
        $table = 'm_inventory';

        $primaryKey = 'm_inventory`.`InternalID';

        $columns = array(
            array('db' => 'InventoryID', 'dt' => 0),
            array(
                'db' => 'InventoryTypeName',
                'dt' => 1,
                'formatter' => function( $d, $row ) {
                    return $d;
                }
            ),
            array(
                'db' => 'GroupName',
                'dt' => 2,
                'formatter' => function( $d, $row ) {
                    return $d;
                }
            ),
            array(
                'db' => 'VarietyName',
                'dt' => 3,
                'formatter' => function( $d, $row ) {
                    return $d;
                }
            ),
            array(
                'db' => 'BrandName',
                'dt' => 4,
                'formatter' => function( $d, $row ) {
                    return $d;
                }
            ),
            array('db' => 'InventoryName', 'dt' => 5),
            array('db' => 'm_inventory`.`InternalID', 'dt' => 6, 'formatter' => function( $d, $row ) {
                    $data = Inventory::find($d);
                    $data->dtRecordformat = date("d-m-Y H:i:s", strtotime($data->dtRecord));
                    $data->dtModifformat = date("d-m-Y H:i:s", strtotime($data->dtModified));
                    $data->Remark = str_replace("\r\n", " ", $data->Remark);
                    $arrData = array($data);
                    $tamp = myEscapeStringData($arrData);

                    $return = '<button id="btn-' . $data->InventoryID . '" data-target="#m_inventoryUpdate"';
                    $return .= "data-all='" . $tamp . "'";
                    $return .= 'data-toggle="modal" role="dialog" onclick="updateAttach(this)"
                                                                class="btn btn-pure-xs btn-xs btn-edit">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </button>
                                    <button data-target="#m_inventoryDelete" data-internal="' . $data->InternalID . '"  data-toggle="modal" role="dialog" onclick="deleteAttach(this)"
                                            data-id="' . $data->InventoryID . '" data-name="' . $data->InventoryName . '" class="btn btn-pure-xs btn-xs btn-delete">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>';
                    if (checkModul('O04')) {
                        $return .= '<a href="' . Route('historyPrice', $data->InventoryID) . '" target="_blank">
                                        <button id="btn-' . $data->InventoryID . '-print"
                                                class="btn btn-pure-xs btn-xs btn-price">
                                            History
                                        </button>
                                    </a>
                                    <button data-target="#r_stock" data-internal="' . $data->InternalID . '"  data-toggle="modal" role="dialog" onclick="stockAttach(this)"
                                            data-id="' . $data->InventoryID . '" class="btn btn-pure-xs btn-xs btn-stock">
                                        stock
                                    </button>';
//                                    <button id="btn-' . $data->InventoryID . '" title="Print Barcode Inventory" data-target="#print_barcode"';
//                        $return .= "data-all='" . $tamp . "'";
//                        $return .= 'data-toggle="modal" role="dialog" onclick="printAttach(this)"
//                                            class="btn btn-pure-xs btn-xs btn-print">
//                                        <span class="glyphicon glyphicon-print"></span>
//                                    </button>';
                    }
                    return $return;
                },
                'field' => 'm_inventory`.`InternalID')
        );

        $sql_details = getConnection();

        require('ssp.class.php');
        $ID_CLIENT_VALUE = Auth::user()->CompanyInternalID;
        $extraCondition = "m_inventory.`CompanyInternalID`=" . $ID_CLIENT_VALUE;
        $join = ' INNER JOIN m_inventorytype on m_inventorytype.InternalID = m_inventory.InventoryTypeInternalID';
        $join .= ' INNER JOIN m_variety on m_variety.InternalID = m_inventory.VarietyInternalID';
        $join .= ' INNER JOIN m_group on m_group.InternalID = m_variety.GroupInternalID';
        $join .= ' INNER JOIN m_brand on m_brand.InternalID = m_inventory.BrandInternalID ';

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $extraCondition, $join));
    }

    static function inventoryPriceDataBackup() {
        $table = 'm_inventory_uom';

        $primaryKey = 'm_inventory_uom`.`InternalID';

        $columns = array(
            array('db' => 'InventoryID', 'dt' => 0),
            array('db' => 'InventoryName', 'dt' => 1),
            array('db' => 'UomID', 'dt' => 2),
            array('db' => 'm_inventory_uom`.`InternalID', 'dt' => 3, 'formatter' => function( $d, $row ) {
                    $d = InventoryUom::find($d);
                    $return = '<input style="width: 90%" type="text" class="numajaDesimal price right" value="' . number_format($d->PurchasePrice, 2, '.', ',') . '" name="purchasePrice[]" id="purchasePrice' . $d->InternalID . '">';
                    return $return;
                },
                'field' => 'm_inventory_uom`.`InternalID'),
            array('db' => 'm_inventory_uom`.`InternalID', 'dt' => 4, 'formatter' => function( $d, $row ) {
                    $d = InventoryUom::find($d);
                    $return = '<input style="width: 90%" type="text" class="numajaDesimal price right" value="' . number_format($d->PriceA, 2, '.', ',') . '" name="PriceA[]" id="priceA' . $d->InternalID . '">';
                    return $return;
                },
                'field' => 'm_inventory_uom`.`InternalID'),
            array('db' => 'm_inventory_uom`.`InternalID', 'dt' => 5, 'formatter' => function( $d, $row ) {
                    $d = InventoryUom::find($d);
                    $return = '<input style="width: 90%" type="text" class="numajaDesimal price right" value="' . number_format($d->PriceB, 2, '.', ',') . '" name="PriceB[]" id="priceB' . $d->InternalID . '">';
                    return $return;
                },
                'field' => 'm_inventory_uom`.`InternalID'),
            array('db' => 'm_inventory_uom`.`InternalID', 'dt' => 6, 'formatter' => function( $d, $row ) {
                    $d = InventoryUom::find($d);
                    $return = '<input style="width: 90%" type="text" class="numajaDesimal price right" value="' . number_format($d->PriceC, 2, '.', ',') . '" name="PriceC[]" id="priceC' . $d->InternalID . '">';
                    return $return;
                },
                'field' => 'm_inventory_uom`.`InternalID'),
            array('db' => 'm_inventory_uom`.`InternalID', 'dt' => 7, 'formatter' => function( $d, $row ) {
                    $d = InventoryUom::find($d);
                    $return = '<input style="width: 90%" type="text" class="numajaDesimal price right" value="' . number_format($d->PriceD, 2, '.', ',') . '" name="PriceD[]" id="priceD' . $d->InternalID . '">';
                    return $return;
                },
                'field' => 'm_inventory_uom`.`InternalID'),
            array('db' => 'm_inventory_uom`.`InternalID', 'dt' => 8, 'formatter' => function( $d, $row ) {
                    $d = InventoryUom::find($d);
                    $return = '<input type="hidden" value="' . $d->InternalID . '" name="Inventory[]">'
                            . '<script type="text/javascript" src="' . Asset('js/formatUang.js') . '"></script>'
                            . '<script type="text/javascript" src="' . Asset('js/keyPressuang.js') . '"></script>'
                            . '<input style="width: 90%" type="text" class="numajaDesimal price right" value="' . number_format($d->PriceE, 2, '.', ',') . '" name="PriceE[]" id="priceE' . $d->InternalID . '">';
                    return $return;
                },
                'field' => 'm_inventory_uom`.`InternalID')
        );


        $sql_details = getConnection();

        require('ssp.class.php');
        $ID_CLIENT_VALUE = Auth::user()->CompanyInternalID;
        $extraCondition = "m_inventory_uom.CompanyInternalID=" . $ID_CLIENT_VALUE;
        $join = ' INNER JOIN m_uom on m_uom.InternalID = m_inventory_uom.UomInternalID '
                . ' INNER JOIN m_inventory on m_inventory.InternalID = m_inventory_uom.InventoryInternalID ';

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $extraCondition, $join));
    }

    static function inventoryMarketDataBackup() {
        $table = 'm_inventory_uom';

        $primaryKey = 'm_inventory_uom`.`InternalID';

        $columns = array(
            array('db' => 'InventoryID', 'dt' => 0),
            array('db' => 'InventoryName', 'dt' => 1),
            array('db' => 'm_inventory_uom`.`InternalID', 'dt' => 2, 'formatter' => function( $d, $row ) {
                    $hpp;
                    $default;
                    $d = InventoryUom::find($d);
                    $value = InventoryValue::where("InventoryInternalID", $d->InventoryInternalID)
                            ->where("UomInternalID", $d->UomInternalID)
                            ->orderBy('Year', 'desc')
                            ->orderBy('Month', 'desc')
                            ->get();
                    if (count($value) == 0) {
                        $hpp = Inventory::find($d->InventoryInternalID)->InitialValue;
                        $inventoryUom = InventoryUom::where("InventoryInternalID", $d->InventoryInternalID)->where("Default", 1)->first();
                        $default = " (" . $inventoryUom->Uom->UomID . " )";
                    } else {
                        $hpp = $value[0]->Value;
                        $inventoryUom = InventoryUom::where("InventoryInternalID", $d->InventoryInternalID)->where("Default", 1)->first();
                        $default = " (" . $inventoryUom->Uom->UomID . " )";
                    }
                    return number_format($hpp, 2, ".", ",") . ' - ' . $default;
                },
                'field' => 'm_inventory_uom`.`InternalID'),
            array('db' => 'm_inventory_uom`.`InternalID', 'dt' => 3, 'formatter' => function( $d, $row ) {
                    $d = InventoryUom::find($d);
                    $return = '<input type="hidden" value="' . $d->InternalID . '" name="Inventory[]">'
                            . '<script type="text/javascript" src="' . Asset('js/formatUang.js') . '"></script>'
                            . '<script type="text/javascript" src="' . Asset('js/keyPressuang.js') . '"></script>'
                            . '<input type="text" class="numajaDesimal price right" value="' . number_format($d->MarketPrice, 2, ".", ",") . '" id="market' . $d . '" name="MarketPrice[]" >';
                    return $return;
                },
                'field' => 'm_inventory_uom`.`InternalID')
        );


        $sql_details = getConnection();

        require('ssp.class.php');
        $ID_CLIENT_VALUE = Auth::user()->CompanyInternalID;
        $extraCondition = "m_inventory_uom.CompanyInternalID=" . $ID_CLIENT_VALUE;
        $join = ' INNER JOIN m_uom on m_uom.InternalID = m_inventory_uom.UomInternalID '
                . ' INNER JOIN m_inventory on m_inventory.InternalID = m_inventory_uom.InventoryInternalID ';

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $extraCondition, $join));
    }

    public function cleanInventoryUom() {
        foreach (Inventory::all() as $value) {
            foreach (InventoryUom::where('InventoryInternalID', $value->InternalID)->get() as $value2) {
                if (InventoryUom::where('InternalID', $value2->InternalID)->count() > 0) {
                    InventoryUom::where('InventoryInternalID', $value->InternalID)->where('UomInternalID', $value2->UomInternalID)->where('InternalID', '!=', $value2->InternalID)->delete();
//echo $value->InternalID . ' ' . $value2->UomInternalID . InventoryUom::where('InternalID', $value2->InternalID)->count() . '<br>';
                }
            }
            $count = InventoryUom::where('InventoryInternalID', $value->InternalID)->where('Default', 1)->count();
            if ($count == 0) {
//echo $value->InternalID . ' Tidak ada default.' . '<br>';
                $tamp = InventoryUom::where('InventoryInternalID', $value->InternalID)->first();
                $tamp->Default = 1;
                $tamp->save();
            }
        }
        echo 'selesai';
    }

    public function getAllInventory() {
        foreach (Inventory::select('m_inventory.*')->distinct()->join('m_inventory_uom', 'm_inventory_uom.InventoryInternalID', '=', 'm_inventory.InternalID')
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)->get() as $inventory) {
            ?>
            <option id="inventory<?php echo $inventory->InternalID ?>" value="<?php echo $inventory->InternalID . "---;---" . $inventory->Palen ?>">
                <?php echo $inventory->InventoryID . " " . $inventory->InventoryName ?></option><?php } ?>;

        <?php
        foreach (Parcel::where("CompanyInternalID", Auth::user()->Company->InternalID)->get() as $parcel) {
            ?>
            <option id="parcel<?php echo $parcel->InternalID ?>" value="<?php echo $parcel->InternalID ?>---;---parcel">
                <?php echo $parcel->ParcelID . " " . $parcel->ParcelName ?> </option><?php } ?>
        <?php
    }

    public function getAllInventoryNoParcel() {
        foreach (Inventory::select('m_inventory.*')->distinct()->join('m_inventory_uom', 'm_inventory_uom.InventoryInternalID', '=', 'm_inventory.InternalID')
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)->get() as $inventory) {
            ?>
            <option id="inventory<?php echo $inventory->InternalID ?>" value="<?php echo $inventory->InternalID . "---;---" . $inventory->Palen ?>">
                <?php echo $inventory->InventoryID . " " . $inventory->InventoryName ?></option><?php } ?>;

        <?php
    }

    public function getSearchResultInventory() {
        $input = splitSearchValue(Input::get("id"));
        $dataInventory = Inventory::select('m_inventory.*')->distinct()
                ->join('m_inventory_uom', 'm_inventory_uom.InventoryInternalID', '=', 'm_inventory.InternalID')
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where("m_inventory.InventoryName", "like", $input)
                ->orWhere("m_inventory.InventoryID", "like", $input)
                ->get();
        $dataParcel = Parcel::where("CompanyInternalID", Auth::user()->Company->InternalID)
                ->where("ParcelID", "like", $input)
                ->orWhere("ParcelName", "like", $input)
                ->get();
        $controller = Input::get("js");
        if (count($dataInventory) == 0 && count($dataParcel) == 0) {
            ?>
            <span>Inventory with keywords <i><u><?php echo Input::get("id"); ?></u></i> can't be found</span>
            <?php
        } else {
            ?>
            <select class="input-theme left inventory" id="inventory-0" data-toggle="popover" data-placement="bottom" data-html="true">
                <?php
                foreach ($dataInventory as $inventory) {
                    ?>
                    <option id="inventory<?php echo $inventory->InternalID ?>" value="<?php echo $inventory->InternalID ?>---;---inventory">
                        <?php echo $inventory->InventoryID . ' ' . $inventory->InventoryName; ?>
                    </option>
                    <?php
                }
                ?>
                <?php
                foreach ($dataParcel as $parcel) {
                    ?>
                    <!--foreach untuk paketnya-->
                    <option id="parcel<?php echo $parcel->InternalID ?>" value="<?php echo $parcel->InternalID ?>---;---parcel">
                        <?php echo $parcel->ParcelID . " " . $parcel->ParcelName ?>
                    </option>
                    <!--foreach untuk paketnya-->
                    <?php
                }
                ?>
            </select>
            <script type="text/javascript" src="<?php echo Asset('js/entry-js-controller/' . $controller . '.js') ?>"></script>
            <?php
        }
    }

    public function getInventorySearch() {
        $input = splitSearchValue(Input::get("id"));
        $tempRoot = InventoryChild::where("CompanyInternalID", Auth::user()->Company->InternalID)->get();
        $invRoot = array();
        $i = 0;
        foreach ($tempRoot as $a) {
            $invRoot[$i] = $a->InventoryInternalID;
            $i++;
            $invRoot[$i] = $a->ChildInventoryInternalID;
            $i++;
        }
        $inventory = Inventory::where("CompanyInternalID", Auth::user()->Company->InternalID)
                ->where(function($query) use($input) {
                    $query->orWhere("InventoryID", "like", $input)
                    ->orWhere("InventoryName", "like", $input);
                })
                ->whereNotIn('InternalID', $invRoot)
                ->take(100)
                ->get();
        if (count($inventory) == 0) {
            ?>
            <span>Inventory with keywords <i><u><?php echo Input::get("id"); ?></u></i> can't be found</span>
            <?php
        } else {
            ?>
            <select id="inventory-0" class="input-theme chosen-select choosen-modal" data-validation="required">
                <?php
                foreach ($inventory as $data) {
                    ?>
                    <option value="<?php echo $data->InternalID ?>"><?php echo $data->InventoryName ?></option>
                    <?php
                }
                ?>
            </select>
            <script >
                $("#btn-addRow").removeAttr("disabled");
                var baris = 1;
                $("#btn-addRow").click(function () {
                    $('#table-inventory tr:last').after('<tr id="row' + baris + '">' +
                            '<td class="chosen-uom">' +
                            '<input type="hidden" class="inventory" style="width: 100px" id="inventory-' + baris + '" style="" name="InventoryInternalID[]" value="' + $('#inventory-0').val() + '">' +
                            $("#inventory-0 option[value='" + $("#inventory-0").val() + "']").text() +
                            '</td>' +
                            '<td class="chosen-uom">' +
                            '<input type="hidden" class="inventory" style="width: 100px" style="" name="inventoryUom[]" value="' + $('#inventoryUom-0').val() + '">' +
                            $("#inventoryUom-0 option[value='" + $("#inventoryUom-0").val() + "']").text() +
                            '</td>' +
                            '<td class="chosen-uom">' +
                            '<input type="hidden" class="inventory" style="width: 100px" style="" name="qty[]" value="' + $('#qty-0').val() + '">' +
                            $('#qty-0').val() +
                            '</td>' +
                            '<td><button class="btn btn-pure-xs btn-xs btn-deleteRow" type="button" data="row' + baris + '"><span class="glyphicon glyphicon-trash"></span></button>' + '</td>' +
                            '</tr>');

                    $(".btn-deleteRow").click(function () {
                        if ($('#' + $(this).attr('data')).length > 0) {
                            document.getElementById($(this).attr('data')).remove()
                        }
                    });
                    baris++;
                });
                $("#inventory-0").change(function () {
                    //get uom
                    $.post(getUomThisInventory, {id: $('#inventory-0').val()}).done(function (data2) {
                        $("#inventoryUom-0").html(data2);
                        $("#inventoryUom-0").trigger("chosen:updated");
                    });
                });
            </script>
            <?php
        }
    }

    public function getInventoryChild() {
        $input = splitSearchValue(Input::get("query"));
        $tempRoot = InventoryChild::where("CompanyInternalID", Auth::user()->Company->InternalID)->get();
        $invRoot = array();
        $i = 0;
        foreach ($tempRoot as $a) {
            $invRoot[$i] = $a->InventoryInternalID;
            $i++;
            $invRoot[$i] = $a->ChildInventoryInternalID;
            $i++;
        }

        $tempRoot = InventoryChild::where('InventoryInternalID', Input::get("id"))->where("CompanyInternalID", Auth::user()->Company->InternalID)->get();
        $childRoot = array();
        $i = 0;
        foreach ($tempRoot as $a) {
            $childRoot[$i] = $a->ChildInventoryInternalID;
            $i++;
        }
        $inventory = Inventory::where("CompanyInternalID", Auth::user()->Company->InternalID)
                ->where(function($query) use($input) {
                    $query->orWhere("InventoryID", "like", $input)
                    ->orWhere("InventoryName", "like", $input);
                })
                ->whereNotIn('InternalID', $invRoot)
                ->where('InternalID', "!=", Input::get("id"))
                ->take(100)
                ->get();
        $childInventory = Inventory::where("CompanyInternalID", Auth::user()->Company->InternalID)
                ->where(function($query) use($input) {
                    $query->orWhere("InventoryID", "like", $input)
                    ->orWhere("InventoryName", "like", $input);
                })
                ->whereIn('InternalID', $childRoot)
                ->take(100)
                ->get();
        if (count($childInventory) == 0 && count($inventory) == 0) {
            ?>
            <span>Inventory with keywords <i><u><?php echo Input::get("query"); ?></u></i> can't be found</span>
            <?php
        } else {
            ?>
            <select id="inventory-0" class="input-theme chosen-select choosen-modal" data-validation="required">
                <?php
                foreach ($inventory as $data) {
                    ?>
                    <option value="<?php echo $data->InternalID ?>"><?php echo $data->InventoryName ?></option>
                    <?php
                }
                foreach ($childInventory as $childData) {
                    ?>
                    <option value="<?php echo $childData->InternalID ?>"><?php echo $childData->InventoryName ?></option>
                <?php } ?>
                }
                ?>
            </select>
            <script>
                $("#btn-addRow-update").removeAttr("disabled");
                var baris = <?php echo count($childInventory) + 1; ?>;
                $("#btn-addRow-update").click(function () {
                    $('#table-inventory-update tr:last').after('<tr id="row' + baris + '">' +
                            '<td class="chosen-uom">' +
                            '<input type="hidden" class="inventory" style="width: 100px" id="inventory-' + baris + '" style="" name="InventoryInternalID[]" value="' + $('#inventory-0').val() + '">' +
                            $("#inventory-0 option[value='" + $("#inventory-0").val() + "']").text() +
                            '</td>' +
                            '<td class="chosen-uom">' +
                            '<input type="hidden" class="inventory" style="width: 100px" style="" name="inventoryUom[]" value="' + $('#inventoryUom-update-0').val() + '">' +
                            $("#inventoryUom-update-0 option[value='" + $("#inventoryUom-update-0").val() + "']").text() +
                            '</td>' +
                            '<td class="chosen-uom">' +
                            '<input type="hidden" class="inventory" style="width: 100px" style="" name="qty[]" value="' + $('#qty-update-0').val() + '">' +
                            $('#qty-update-0').val() +
                            '</td>' +
                            '<td><button class="btn btn-pure-xs btn-xs btn-deleteRow-update" type="button" data="row' + baris + '"><span class="glyphicon glyphicon-trash"></span></button>' + '</td>' +
                            '</tr>');

                    $(".btn-deleteRow-update").click(function () {
                        if ($('#' + $(this).attr('data')).length > 0) {
                            document.getElementById($(this).attr('data')).remove()
                        }
                    });
                    baris++;
                });
                $("#inventory-0").change(function () {
                    //get uom
                    $.post(getUomThisInventory, {id: $('#inventory-0').val()}).done(function (data2) {
                        $("#inventoryUom-0").html(data2);
                        $("#inventoryUom-0").trigger("chosen:updated");
                    });
                });
            </script>
            <?php
        }
    }

    public function setInventoryChild() {
        $childInventory = InventoryChild::where("m_inventory.CompanyInternalID", Auth::user()->Company->InternalID)
                        ->join('m_uom', 'm_uom.InternalID', '=', 'm_inventory_child.UomInternalID')
                        ->join('m_inventory', 'm_inventory.InternalID', '=', 'm_inventory_child.ChildInventoryInternalID')
                        ->where('m_inventory_child.InventoryInternalID', Input::get('id'))
                        ->select('m_inventory.InternalID as InvInternalID', 'm_inventory.InventoryName'
                                , 'm_uom.InternalID as UOMInternalID', 'm_uom.UomID', 'm_inventory_child.Qty')->get();
        return $childInventory;
    }

    public function getInventorySearch2() {
        $input = splitSearchValue(Input::get("id"));
        $tempRoot = Inventory::whereNotNull('InventoryInternalID')->where("CompanyInternalID", Auth::user()->Company->InternalID)
                        ->select('InventoryInternalID')->distinct()->get();
        $invRoot = array();
        $i = 0;
        foreach ($tempRoot as $a) {
            $invRoot[$i] = $a->InventoryInternalID;
            $i++;
        }
        $inventory = Inventory::where("CompanyInternalID", Auth::user()->Company->InternalID)
                ->where(function($query) use($input) {
                    $query->orWhere("InventoryID", "like", $input)
                    ->orWhere("InventoryName", "like", $input);
                })
                ->whereNull('InventoryInternalID')
                ->whereNotIn('InternalID', $invRoot)
                ->take(100)
                ->get();
        if (count($inventory) == 0) {
            ?>
            <span>Inventory with keywords <i><u><?php echo Input::get("id"); ?></u></i> can't be found</span>
            <?php
        } else {
            ?>
            <select id="inventoryInternalID" multiple class="input-theme chosen-select choosen-modal" name="InventoryInternalID[]" data-validation="required">
                <?php
                foreach ($inventory as $data) {
                    ?>
                    <option value="<?php echo $data->InternalID ?>"><?php echo $data->InventoryName ?></option>
                    <?php
                }
                ?>
            </select>
            <script>
                $(document).ready(function () {
                    var config = {
                        '.chosen-select': {}
                    };
                    for (var selector in config) {
                        $(selector).chosen({
                            search_contains: true
                        });
                    }
                });
            </script>
            <?php
        }
    }

    public function getInventoryChild2() {
        $input = splitSearchValue(Input::get("query"));
        $tempRoot = Inventory::whereNotNull('InventoryInternalID')->where("CompanyInternalID", Auth::user()->Company->InternalID)
                        ->select('InventoryInternalID')->distinct()->get();
        $invRoot = array();
        $i = 0;
        foreach ($tempRoot as $a) {
            $invRoot[$i] = $a->InventoryInternalID;
            $i++;
        }
        $inventory = Inventory::where("CompanyInternalID", Auth::user()->Company->InternalID)
                ->where(function($query) use($input) {
                    $query->orWhere("InventoryID", "like", $input)
                    ->orWhere("InventoryName", "like", $input);
                })
                ->whereNull('InventoryInternalID')
                ->whereNotIn('InternalID', $invRoot)
                ->take(100)
                ->get();
        $childInventory = Inventory::where("CompanyInternalID", Auth::user()->Company->InternalID)
                ->where('InventoryInternalID', Input::get("id"))
                ->take(100)
                ->get();
        if (count($childInventory) == 0) {
            ?>
            <span>Inventory with keywords <i><u><?php echo Input::get("id"); ?></u></i> can't be found</span>
            <?php
        } else {
            ?>
            <select id="inventoryInternalID" multiple class="input-theme chosen-select choosen-modal" name="InventoryInternalID[]" data-validation="required">
                <?php
                foreach ($inventory as $data) {
                    ?>
                    <option value="<?php echo $data->InternalID ?>"><?php echo $data->InventoryName ?></option>
                    <?php
                }
                foreach ($childInventory as $childData) {
                    ?>
                    <option selected value="<?php echo $childData->InternalID ?>"><?php echo $childData->InventoryName ?></option>
                <?php } ?>
                }
                ?>
            </select>
            <script>
                $(document).ready(function () {
                    var config = {
                        '.chosen-select': {}
                    };
                    for (var selector in config) {
                        $(selector).chosen({
                            search_contains: true
                        });
                    }
                });
            </script>
            <?php
        }
    }

    public function getSearchResultInventoryNoParcel() {
        $input = splitSearchValue(Input::get("id"));
        $dataInventory = Inventory::select('m_inventory.*')->distinct()
                ->join('m_inventory_uom', 'm_inventory_uom.InventoryInternalID', '=', 'm_inventory.InternalID')
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where("m_inventory.InventoryName", "like", $input)
                ->orWhere("m_inventory.InventoryID", "like", $input)
                ->get();

        $controller = Input::get("js");
        if (count($dataInventory) == 0) {
            ?>
            <span>Inventory with keywords <i><u><?php echo Input::get("id"); ?></u></i> can't be found</span>
            <?php
        } else {
            ?>
            <select class="input-theme left inventory" id="inventory-0">
                <?php
                foreach ($dataInventory as $inventory) {
                    ?>
                    <option id="inventory<?php echo $inventory->InternalID ?>" value="<?php echo $inventory->InternalID ?>---;---inventory">
                        <?php echo $inventory->InventoryID . ' ' . $inventory->InventoryName; ?>
                    </option>
                    <?php
                }
                ?>
            </select>
            <script type="text/javascript" src="<?php echo Asset('js/entry-js-controller/' . $controller . '.js') ?>"></script>
            <?php
        }
    }

//=======================================ajax==========================================
}
