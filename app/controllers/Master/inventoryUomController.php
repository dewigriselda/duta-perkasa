<?php

class InventoryUomController extends BaseController {

    public function showInventoryUom() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertInventoryUom') {
                return $this->insertInventoryUom();
            }
            if (Input::get('jenis') == 'updateInventoryUom') {
                return $this->updateInventoryUom();
            }
            if (Input::get('jenis') == 'deleteInventoryUom') {
                return $this->deleteInventoryUom();
            }
            if (Input::get('jenis') == 'importUpdatePrice') {
                return $this->importUpdatePrice();
            }
        }
        return View::make('master.inventoryUom')
                        ->withToogle('master')->withAktif('inventoryUom');
    }

    public function showInventoryPrice() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'updateInventoryPrice') {
                return $this->updateInventoryPrice();
            }
        }
        return View::make('master.inventoryPrice')
                        ->withToogle('master')->withAktif('inventoryUom');
    }

    public function showInventoryMarketPrice() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'updateInventoryMarketPrice') {
                return $this->updateInventoryMarketPrice();
            }
        }
        return View::make('master.inventoryMarketPrice')
                        ->withToogle('master')->withAktif('inventoryUom');
    }

    public static function insertInventoryUom() {
        //rule
        $rule = array(
            'InventoryInternalID' => 'required',
            'UomInternalID' => 'required',
            'Default' => 'required|Integer|max:1',
            'Value' => 'numeric|required',
            'remark' => 'required|max:1000'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        $count = InventoryUom::where('InventoryInternalID', Input::get('InventoryInternalID'))->where('UomInternalID', Input::get('UomInternalID'))->count();
        if ($validator->fails()) {
            //tidak valid
            return View::make('master.inventoryUom')
                            ->withMessages('gagalInsert')
                            ->withErrmessages($count)
                            ->withErrors($validator->messages())
                            ->withToogle('master')->withAktif('inventoryUom');
        } else {
            $count = InventoryUom::where("InventoryInternalID", Input::get('InventoryInternalID'))->where("UomInternalID", Input::get('UomInternalID'))
                    ->count();
            if ($count != 1) {
                //valid
                $InventoryUom = new InventoryUom;
                $InventoryUom->InventoryInternalID = Input::get('InventoryInternalID');
                $InventoryUom->UomInternalID = Input::get('UomInternalID');
                if (Input::get('Default') == '1') {
                    InventoryUom::where('Default', '=', 1)->where("InventoryInternalID", Input::get('InventoryInternalID'))->where('CompanyInternalID', Auth::user()->Company->InternalID)->update(array('Default' => 0));
                }
                $InventoryUom->Default = Input::get('Default');
                $InventoryUom->PurchasePrice = str_replace(",", "", Input::get('purchasePrice'));
                $InventoryUom->PriceA = str_replace(",", "", Input::get('PriceA'));
                $InventoryUom->PriceB = str_replace(",", "", Input::get('PriceB'));
                $InventoryUom->PriceC = str_replace(",", "", Input::get('PriceC'));
                $InventoryUom->PriceD = str_replace(",", "", Input::get('PriceD'));
                $InventoryUom->PriceE = str_replace(",", "", Input::get('PriceE'));
                $InventoryUom->Value = Input::get('Value');
                $InventoryUom->UserRecord = Auth::user()->UserID;
                $InventoryUom->CompanyInternalID = Auth::user()->Company->InternalID;
                $InventoryUom->UserModified = "0";
                $InventoryUom->Remark = Input::get('remark');
                $InventoryUom->save();

                return View::make('master.inventoryUom')
                                ->withMessages('suksesInsert')
                                ->withToogle('master')->withAktif('inventoryUom');
            } else {
                return View::make('master.inventoryUom')
                                ->withMessages('sudahAda')
                                ->withToogle('master')->withAktif('inventoryUom');
            }
        }
    }

    static function updateInventoryUom() {
        //rule
        $rule = array(
            'Default' => 'required|Integer|max:1',
            'Value' => 'numeric|required',
            'remark' => 'required|max:1000'
        );
        //validasi
        $validator = Validator ::make(Input::all(), $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('master.inventoryUom')
                            ->withMessages('gagalInsert')
                            ->withErrors($validator->messages())
                            ->withToogle('master')->withAktif('inventoryUom');
        } else {
            //valid
            $InventoryUom = InventoryUom::find(Input::get('InternalID'));
            if ($InventoryUom->CompanyInternalID == Auth::user()->Company->InternalID) {
                if (Input::get('Default') == '1' && $InventoryUom->Default != 1) {
                    InventoryUom::where('Default', '=', 1)->where("InventoryInternalID", $InventoryUom->InventoryInternalID)->where('CompanyInternalID', Auth::user()->Company->InternalID)->update(array('Default' => 0));
                }
                $InventoryUom->Value = Input::get('Value');
                $InventoryUom->PurchasePrice = str_replace(",", "", Input::get('purchasePriceUpdate'));
                $InventoryUom->PriceA = str_replace(",", "", Input::get('PriceA'));
                $InventoryUom->PriceB = str_replace(",", "", Input::get('PriceB'));
                $InventoryUom->PriceC = str_replace(",", "", Input::get('PriceC'));
                $InventoryUom->PriceD = str_replace(",", "", Input::get('PriceD'));
                $InventoryUom->PriceE = str_replace(",", "", Input::get('PriceE'));
                $InventoryUom->UserModified = Auth::user()->UserID;
                $InventoryUom->Remark = Input::get('remark');
                $InventoryUom->save();
                if ($InventoryUom->Default == '1' && Input::get('Default') != 1) {
                    return View::make('master.inventoryUom')
                                    ->withMessages('gagalDefault')
                                    ->withToogle('master')->withAktif('inventoryUom');
                }
                $InventoryUom->Default = Input::get('Default');
                $InventoryUom->save();
                return View::make('master.inventoryUom')
                                ->withMessages('suksesUpdate')
                                ->withToogle('master')->withAktif('inventoryUom');
            } else {
                return View::make('master.inventoryUom')
                                ->withMessages('accessDenied')
                                ->withToogle('master')->withAktif('inventoryUom');
            }
        }
    }

    static function updateInventoryPrice() {
        //rule
        $rule = array(
            'Inventory' => 'required'
        );
        //validasi
        $validator = Validator ::make(Input::all(), $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('master.inventoryPrice')
                            ->withMessages('gagalUpdate')
                            ->withErrors($validator->messages())
                            ->withToogle('master')->withAktif('inventoryUom');
        } else {
            //valid
            for ($a = 0; $a < count(Input::get('Inventory')); $a++) {
                $InventoryUom = InventoryUom::find(Input::get('Inventory')[$a]);
                if ($InventoryUom->CompanyInternalID == Auth::user()->Company->InternalID) {
                    $InventoryUom->PurchasePrice = str_replace(",", "", Input::get('purchasePrice')[$a]);
                    $InventoryUom->PriceA = str_replace(",", "", Input::get('PriceA')[$a]);
                    $InventoryUom->PriceB = str_replace(",", "", Input::get('PriceB')[$a]);
                    $InventoryUom->PriceC = str_replace(",", "", Input::get('PriceC')[$a]);
                    $InventoryUom->PriceD = str_replace(",", "", Input::get('PriceD')[$a]);
                    $InventoryUom->PriceE = str_replace(",", "", Input::get('PriceE')[$a]);
                    $InventoryUom->UserModified = Auth::user()->UserID;
                    $InventoryUom->save();
                } else {
                    return View::make('master.inventoryPrice')
                                    ->withMessages('accessDenied')
                                    ->withToogle('master')->withAktif('inventoryUom');
                }
            }
            return View::make('master.inventoryPrice')
                            ->withMessages('suksesUpdate')
                            ->withToogle('master')->withAktif('inventoryUom');
        }
    }

    static function updateInventoryMarketPrice() {
        //rule
        $rule = array(
            'MarketPrice' => 'required'
        );
        //validasi
        $validator = Validator ::make(Input::all(), $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('master.inventoryMarketPrice')
                            ->withMessages('gagalUpdate')
                            ->withErrors($validator->messages())
                            ->withToogle('master')->withAktif('inventoryUom');
        } else {
            //valid
            for ($a = 0; $a < count(Input::get('Inventory')); $a++) {
                $InventoryUom = InventoryUom::find(Input::get('Inventory')[$a]);
                if ($InventoryUom->CompanyInternalID == Auth::user()->Company->InternalID) {
                    $InventoryUom->MarketPrice = str_replace(",", "", Input::get('MarketPrice')[$a]);
                    $InventoryUom->UserModified = Auth::user()->UserID;
                    $InventoryUom->save();
                } else {
                    return View::make('master.inventoryMarketPrice')
                                    ->withMessages('accessDenied')
                                    ->withToogle('master')->withAktif('inventoryUom');
                }
            }
            return View::make('master.inventoryMarketPrice')
                            ->withMessages('suksesUpdate')
                            ->withToogle('master')->withAktif('inventoryUom');
        }
    }

    static function deleteInventoryUom() {
        //tidak ada maka boleh dihapus
        $InventoryUom = InventoryUom::find(Input::get('InternalID'));
        $count = 0;
        $count += ParcelInventory::where("UomInternalID", $InventoryUom->UomInternalID)->where("InventoryInternalID", $InventoryUom->InventoryInternalID)->count();
        $count += MemoInDetail::where("UomInternalID", $InventoryUom->UomInternalID)->where("InventoryInternalID", $InventoryUom->InventoryInternalID)->count();
        $count += MemoOutDetail::where("UomInternalID", $InventoryUom->UomInternalID)->where("InventoryInternalID", $InventoryUom->InventoryInternalID)->count();
        $count += TransferDetail::where("UomInternalID", $InventoryUom->UomInternalID)->where("InventoryInternalID", $InventoryUom->InventoryInternalID)->count();
        $count += PurchaseOrderDetail::where("UomInternalID", $InventoryUom->UomInternalID)->where("InventoryInternalID", $InventoryUom->InventoryInternalID)->count();
        $count += SalesOrderDetail::where("UomInternalID", $InventoryUom->UomInternalID)->where("InventoryInternalID", $InventoryUom->InventoryInternalID)->count();

        if ($count == 0) {
            //gak di pakai maka boleh hapus
            if ($InventoryUom->CompanyInternalID == Auth::user()->Company->InternalID) {
                if ($InventoryUom->Default == 0) {
                    $InventoryUom->delete();
                    return View::make('master.inventoryUom')
                                    ->withMessages('suksesDelete')
                                    ->withToogle('master')->withAktif('inventoryUom');
                } else {
                    return View::make('master.inventoryUom')
                                    ->withMessages('gagalDefault')
                                    ->withToogle('master')->withAktif('inventoryUom');
                }
            } else {
                return View::make('master.inventoryUom')
                                ->withMessages('accessDenied')
                                ->withToogle('master')->withAktif('inventoryUom');
            }
        } else {
            //ada maka tidak dihapus
            return View::make('master.inventoryUom')
                            ->withMessages('gagalDelete')
                            ->withToogle('master')->withAktif('inventoryUom');
        }
    }

    public function exportInventoryUom() {
        Excel::create('Master_Inventory_Uom', function($excel) {
            $excel->sheet('Master_Inventory_Uom', function($sheet) {
                $sheet->mergeCells('B1:N1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Master Inventory Uom");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Inventory ID.");
                $sheet->setCellValueByColumnAndRow(3, 2, "Inventory");
                $sheet->setCellValueByColumnAndRow(4, 2, "Uom ID");
//                $sheet->setCellValueByColumnAndRow(5, 2, "Default");
                $sheet->setCellValueByColumnAndRow(5, 2, "Harga Beli");
                $count = 5;
                foreach (DefaultPrice::where("CompanyInternalID", Auth::user()->Company->InternalID)->orderBy("InternalID", "ASC")->get() as $data) {
                    $count++;
                    $sheet->setCellValueByColumnAndRow($count, 2, $data->PriceName);
                }
                $sheet->setCellValueByColumnAndRow($count += 1, 2, "Value");
                $sheet->setCellValueByColumnAndRow($count += 1, 2, "Record");
                $sheet->setCellValueByColumnAndRow($count += 1, 2, "Modified");
                $sheet->setCellValueByColumnAndRow($count += 1, 2, "Remark");
                $row = 3;
                foreach (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                    $default = '';
                    if ($data->Default == 1) {
                        $default = '(Default)';
                    }
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, $data->inventory->InventoryID);
                    $sheet->setCellValueByColumnAndRow(3, $row, $data->inventory->InventoryName . ' ' . $default);
                    $sheet->setCellValueByColumnAndRow(4, $row, $data->uom->UomID . ' ' . $default);
                    $sheet->setCellValueByColumnAndRow(5, $row, number_format($data->PurchasePrice, 2, '.', ','));
//                    $sheet->setCellValueByColumnAndRow(5, $row, $default);
                    $sheet->setCellValueByColumnAndRow(6, $row, number_format($data->PriceA, 2, '.', ','));
                    $sheet->setCellValueByColumnAndRow(7, $row, number_format($data->PriceB, 2, '.', ','));
                    $sheet->setCellValueByColumnAndRow(8, $row, number_format($data->PriceC, 2, '.', ','));
                    $sheet->setCellValueByColumnAndRow(9, $row, number_format($data->PriceD, 2, '.', ','));
                    $sheet->setCellValueByColumnAndRow(10, $row, number_format($data->PriceE, 2, '.', ','));
                    $sheet->setCellValueByColumnAndRow(11, $row, number_format($data->Value, 2, '.', ','));
                    $sheet->setCellValueByColumnAndRow(12, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(13, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(14, $row, $data->Remark);
                    $row++;
                }

                if (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() <= 0) {
                    $sheet->mergeCells('B3:O3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "No data available in table");

                    $sheet->cells('B3:O3', function($cells) {
                        $cells->setAlignment('center');
                    });

                    $sheet->setBorder('B3:O' . $row, 'thin');
                }

                $row--;
                $sheet->setBorder('B2:O' . $row, 'thin');
                $sheet->cells('B2:O2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:O' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('E3:E' . $row, function($cells) {
                    $cells->setAlignment('right');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
            });
        })->export('xls');
    }

    public function importUpdatePrice() {
        Excel::load(Input::file('file'), function($reader) {
            $result = $reader->select(array('inventory_id', 'inventory', 'uom_id', 'harga_beli', 'harga_toko', 'harga_grosir', 'harga_diskon', 'harga_diskon_2',
                        'harga_lain_lain', 'value', 'record', 'modified', 'remark'))->get();
            foreach ($result as $row) {
                $UomID = explode(" ", $row['uom_id']);
                $internalUom = Uom::where('UomID', $UomID[0])->first()->InternalID;
                $internalInventory = Inventory::where('InventoryID', $row['inventory_id'])->first()->InternalID;
                $InventoryUom = InventoryUom::where('InventoryInternalID', $internalInventory)->where('UomInternalID', $internalUom)->first();
                $InventoryUom->PurchasePrice = str_replace(",", "", $row['harga_beli']);
                $InventoryUom->PriceA = str_replace(",", "", $row['harga_toko']);
                $InventoryUom->PriceB = str_replace(",", "", $row['harga_grosir']);
                $InventoryUom->PriceC = str_replace(",", "", $row['harga_diskon']);
                $InventoryUom->PriceD = str_replace(",", "", $row['harga_diskon_2']);
                $InventoryUom->PriceE = str_replace(",", "", $row['harga_lain_lain']);
                $InventoryUom->Value = $row['value'];
                $InventoryUom->UserModified = Auth::user()->UserID;
                $InventoryUom->save();
            }
        });

        return View::make('master.inventoryUom')
                        ->withMessages('suksesUpdate')
                        ->withToogle('master')->withAktif('inventoryUom');
    }

    public function cekGV() {
        $hasil = cekGantiValue(Input::get('inventoryID'), Input::get('uomID'));
        return $hasil;
    }

    public function reportHPP() {
        Excel::create('Report_HPP', function($excel) {
            $excel->sheet('Report_HPP', function($sheet) {
                $sheet->mergeCells('B1:J1');
                $sheet->mergeCells('B2:J2');
                $sheet->setCellValueByColumnAndRow(1, 1, "Report HPP");
                $sheet->setCellValueByColumnAndRow(1, 2, "Period : " . date("d-M-Y", strtotime(date("Y-m-d"))));
                $sheet->setCellValueByColumnAndRow(1, 3, "No.");
                $sheet->setCellValueByColumnAndRow(2, 3, "Inventory");
                $sheet->setCellValueByColumnAndRow(3, 3, "Harga Beli");
                $count = 3;
                foreach (DefaultPrice::where("CompanyInternalID", Auth::user()->Company->InternalID)->orderBy("InternalID", "ASC")->get() as $data) {
                    $count++;
                    $sheet->setCellValueByColumnAndRow($count, 3, $data->PriceName);
                }
                $sheet->setCellValueByColumnAndRow($count += 1, 3, "HPP");
                $sheet->setCellValueByColumnAndRow($count += 1, 3, "Market Price");
                $row = 4;
                foreach (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('InventoryInternalID')->get() as $data) {
                    $inventory = Inventory::find($data->InventoryInternalID);
                    $month = date("m") - 1;
                    $year = date("Y");
                    if ($month == 0) {
                        $month = 12;
                        $year = date('Y') - 1;
                    }
                    $hpp;
                    $default;
                    $inventoryUomValue = InventoryUom::where("InventoryInternalID", $data->InventoryInternalID)
                                    ->where("UomInternalID", $data->UomInternalID)->pluck("Value");
                    $value = InventoryValue::where("InventoryInternalID", $data->InventoryInternalID)
                                    ->orderBy('Year','desc')
                                    ->orderBy('Month','desc')
                                    ->get();

                    if (count($value) == 0) {
                        $hpp = Inventory::find($data->InventoryInternalID)->InitialValue * $inventoryUomValue;
                        $inventoryUom = InventoryUom::where("InventoryInternalID", $data->InventoryInternalID)->where("Default", 1)->first();
                        $default = " (" . $inventoryUom->Uom->UomID . " )";
                    } else {
                        $hpp = $value[0]->Value * $inventoryUomValue;
                        $inventoryUom = InventoryUom::where("InventoryInternalID", $data->InventoryInternalID)->where("Default", 1)->first();
                        $default = " (" . $inventoryUom->Uom->UomID . " )";
                    }

                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 3);
                    $sheet->setCellValueByColumnAndRow(2, $row, $inventory->InventoryID . ' ' . $inventory->InventoryName . ' | ' . $data->uom->UomID);
                    $sheet->setCellValueByColumnAndRow(3, $row, number_format($data->PurchasePrice, 2, '.', ','));
                    $sheet->setCellValueByColumnAndRow(4, $row, number_format($data->PriceA, 2, '.', ','));
                    $sheet->setCellValueByColumnAndRow(5, $row, number_format($data->PriceB, 2, '.', ','));
                    $sheet->setCellValueByColumnAndRow(6, $row, number_format($data->PriceC, 2, '.', ','));
                    $sheet->setCellValueByColumnAndRow(7, $row, number_format($data->PriceD, 2, '.', ','));
                    $sheet->setCellValueByColumnAndRow(8, $row, number_format($data->PriceE, 2, '.', ','));
                    $sheet->setCellValueByColumnAndRow(9, $row, number_format($hpp, 2, ".", ","));
                    $sheet->setCellValueByColumnAndRow(10, $row, number_format($data->MarketPrice, 2, ".", ","));
                    $row++;
                }

                if (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() <= 0) {
                    $sheet->mergeCells('B4:O4');
                    $sheet->setCellValueByColumnAndRow(1, 4, "No data available in table");

                    $sheet->cells('B4:O4', function($cells) {
                        $cells->setAlignment('center');
                    });

                    $sheet->setBorder('B4:O' . $row, 'thin');
                }

                $row--;
                $sheet->setBorder('B3:K' . $row, 'thin');
                $sheet->cells('B3:K3', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B2', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('14');
                });
                $sheet->cells('B4:K' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('E4:E' . $row, function($cells) {
                    $cells->setAlignment('right');
                });
                $sheet->cells('B4:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
            });
        })->export('xls');
    }

    public function reportHPP2() {
        $dateT = explode('-', date("d-m-Y"));
        $date = $dateT[2] . '-' . $dateT[1] . '-' . $dateT[0];
        $strPrice = "";
        foreach (DefaultPrice::where("CompanyInternalID", Auth::user()->Company->InternalID)->orderBy("InternalID", "ASC")->get() as $data) {
            $strPrice .= '<th style = "font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">' . $data->PriceName . '</th>';
        }
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:80%; bottom: -63px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">HPP Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(date("Y-m-d"))) . '</span>
                            <table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory</th>
                                            ' . $strPrice . '
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">HPP</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Market Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
        if (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() > 0) {
            foreach (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('InventoryInternalID')->get() as $data) {
                $inventory = Inventory::find($data->InventoryInternalID);
                $month = date("m") - 1;
                $year = date("Y");
                if ($month == 0) {
                    $month = 12;
                    $year = date('Y') - 1;
                }
                $hpp;
                $default;
                $inventoryUomValue = InventoryUom::where("InventoryInternalID", $data->InventoryInternalID)
                                ->where("UomInternalID", $data->UomInternalID)->pluck("Value");
                $value = InventoryValue::where("InventoryInternalID", $data->InventoryInternalID)
                                ->orderBy('Year','desc')
                                ->orderBy('Month','desc')
                                ->get();

                if (count($value) == 0) {
                    $hpp = Inventory::find($data->InventoryInternalID)->InitialValue * $inventoryUomValue;
                    $inventoryUom = InventoryUom::where("InventoryInternalID", $data->InventoryInternalID)->where("Default", 1)->first();
                    $default = " (" . $inventoryUom->Uom->UomID . " )";
                } else {
                    $hpp = $value[0]->Value * $inventoryUomValue;
                    $inventoryUom = InventoryUom::where("InventoryInternalID", $data->InventoryInternalID)->where("Default", 1)->first();
                    $default = " (" . $inventoryUom->Uom->UomID . " )";
                }
                $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; ">' . $inventory->InventoryID . ' ' . $inventory->InventoryName . ' | ' . $data->uom->UomID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right; ">' . number_format($data->PriceA, 2, ".", ",") . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right; ">' . number_format($data->PriceB, 2, ".", ",") . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right; ">' . number_format($data->PriceC, 2, ".", ",") . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right; ">' . number_format($data->PriceD, 2, ".", ",") . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right; ">' . number_format($data->PriceE, 2, ".", ",") . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right; ">' . number_format($hpp, 2, ".", ",") . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right; ">' . number_format($data->MarketPrice, 2, ".", ",") . '</td>
                            </tr>';
            }
        } else {
            $html .= '<tr>
                            <td colspan="6" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;  text-align: center">There is no inventory registered.</td>
                        </tr>';
        }

        $html .= '</tbody>
                            </div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('Report_HPP');
    }

    static function inventoryUomDataBackup() {
        $table = 'm_inventory_uom';

        $primaryKey = 'm_inventory_uom`.`InternalID';

        $columns = array(
            array('db' => 'InventoryID', 'dt' => 0),
            array('db' => 'InventoryName', 'dt' => 1),
            array('db' => 'UomID', 'dt' => 2),
            array(
                'db' => 'Default',
                'dt' => 3,
                'formatter' => function( $d, $row ) {
                    if ($d == "0") {
                        return '-';
                    } else {
                        return 'Default';
                    }
                }
            ),
            array(
                'db' => 'Value',
                'dt' => 4,
                'formatter' => function( $d, $row ) {
                    return number_format($d, 0, '.', ',');
                }
            ),
            array(
                'db' => 'PurchasePrice',
                'dt' => 5,
                'formatter' => function( $d, $row ) {
                    return number_format($d, 2, '.', ',');
                }
            ),
            array(
                'db' => 'PriceA',
                'dt' => 6,
                'formatter' => function( $d, $row ) {
                    return number_format($d, 2, '.', ',');
                }
            ),
            array(
                'db' => 'PriceB',
                'dt' => 7,
                'formatter' => function( $d, $row ) {
                    return number_format($d, 2, '.', ',');
                }
            ),
            array(
                'db' => 'PriceC',
                'dt' => 8,
                'formatter' => function( $d, $row ) {
                    return number_format($d, 2, '.', ',');
                }
            ),
            array(
                'db' => 'PriceD',
                'dt' => 9,
                'formatter' => function( $d, $row ) {
                    return number_format($d, 2, '.', ',');
                }
            ),
            array(
                'db' => 'PriceE',
                'dt' => 10,
                'formatter' => function( $d, $row ) {
                    return number_format($d, 2, '.', ',');
                }
            ),
            array('db' => 'm_inventory_uom`.`InternalID', 'dt' => 11, 'formatter' => function( $d, $row ) {
                    $data = InventoryUom::find($d);
                    $data->dtRecordformat = date("d-m-Y H:i:s", strtotime($data->dtRecord));
                    $data->dtModifformat = date("d-m-Y H:i:s", strtotime($data->dtModified));
                    $data->Remark = str_replace("\r\n", " ", $data->Remark);
                    $arrData = array($data);
                    $tamp = myEscapeStringData($arrData);

                    $return = '<button id="btn-' . $data->InternalID . '" data-target="#m_inventoryUomUpdate"';
                    $return .= "data-all='" . $tamp . "'";
                    $return .= 'data-toggle="modal" role="dialog" onclick="updateAttach(this)" data-name="' . $data->inventory->InventoryName . " - " . $data->uom->UomID . '"
                                                                class="btn btn-pure-xs btn-xs btn-edit">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </button>
                                    <button data-target="#m_inventoryUomDelete" data-internal="' . $data->InternalID . '"  data-toggle="modal" role="dialog" onclick="deleteAttach(this)"
                                            data-id="' . $data->InventoryID . '" data-name="' . $data->inventory->InventoryName . " - " . $data->uom->UomID . '" class="btn btn-pure-xs btn-xs btn-delete">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>';
                    return $return;
                },
                'field' => 'm_inventory_uom`.`InternalID')
        );

        $sql_details = getConnection();

        require('ssp.class.php');
        $ID_CLIENT_VALUE = Auth::user()->CompanyInternalID;
        $extraCondition = "m_inventory_uom.CompanyInternalID=" . $ID_CLIENT_VALUE;
        $join = ' INNER JOIN m_uom on m_uom.InternalID = m_inventory_uom.UomInternalID '
                . ' INNER JOIN m_inventory on m_inventory.InternalID = m_inventory_uom.InventoryInternalID ';

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $extraCondition, $join));
    }

    //=================ajax=========================
    public function checkInvUom() {
        $inventoryUom = InventoryUom::where("InventoryInternalID", Input::get("idInv"))->where("UomInternalID", Input::get("idUom"))->count();
        if ($inventoryUom > 0) {
            //gak boleh
            return "1";
        } else {
            //boleh
            return "0";
        }
    }

    public function checkInvUomUpdate() {
        $inventoryUom = InventoryUom::where("InternalID", "!=", Input::get("idInvUom"))->where("InventoryInternalID", Input::get("idInv"))->where("UomInternalID", Input::get("idUom"))->count();
        if ($inventoryUom > 0) {
            //gak boleh
            return "1";
        } else {
            //boleh
            return "0";
        }
    }

    public function getResultSearchInventory() {
        $input = splitSearchValue(Input::get("id"));
        $inventory = Inventory::where("CompanyInternalID", Auth::user()->Company->InternalID)
                ->where("InventoryID", "like", $input)
                ->orWhere("InventoryName", "like", $input)
                ->take(100)
                ->get();
        if (count($inventory) == 0) {
            ?>
            <span>Inventory with keywords <i><u><?php echo Input::get("id"); ?></u></i> can't be found</span>
            <?php
        } else {
            ?>
            <select id="inventoryInternalID" class="input-theme" name="InventoryInternalID" data-validation="required">
                <?php
                foreach ($inventory as $data) {
                    ?>
                    <option value="<?php echo $data->InternalID ?>"><?php echo $data->InventoryName ?></option>
                    <?php
                }
                ?>
            </select>
            <?php
        }
    }

    //=================ajax=========================
}
