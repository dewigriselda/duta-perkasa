<?php

class NotesController extends BaseController {

    public function showNotes() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertNotes') {
                return $this->insertNotes();
            }
            if (Input::get('jenis') == 'updateNotes') {
                return $this->updateNotes();
            }
            if (Input::get('jenis') == 'doneNotes') {
                return $this->doneNotes();
            }
            if (Input::get('jenis') == 'deleteNotes') {
                return $this->deleteNotes();
            }
        }
        return View::make('master.notes')
                        ->withToogle('master')->withAktif('notes');
    }

    public static function insertNotes() {
        //rule
        $rule = array(
            'NotesID' => 'required',
//            'NotesID' => 'required|unique:m_notes,NotesID',
            'To' => 'required|max:200',
            'Description' => 'required',
            'Remark' => 'required|max:1000'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('master.notes')
                            ->withMessages('gagalInsert')
                            ->withError($validator->messages())
                            ->withToogle('master')->withAktif('notes')
                            ->withErrors($validator);
        } else {
            //valid
            $notes = new Notes;
            $notes->NotesID = Input::get('NotesID');
            $notes->To = Input::get('To');
            $notes->Description = Input::get('Description');
            $notes->CompanyInternalID = Auth::user()->Company->InternalID;
            $notes->Status = 0;
            $notes->UserRecord = Auth::user()->UserID;
            $notes->UserModified = "0";
            $notes->Remark = Input::get('Remark');
            $notes->save();

            return View::make('master.notes')
                            ->withMessages('suksesInsert')
                            ->withToogle('master')->withAktif('notes');
        }
    }

    static function updateNotes() {
        //rule
        $rule = array(
            'To' => 'required|max:200',
            'Description' => 'required',
            'Remark' => 'required|max:1000'
        );

        //validasi
        $validator = Validator::make(Input::all(), $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('master.notes')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('master')->withAktif('notes');
        } else {
            //valid
            $notes = Notes::find(Input::get('InternalID'));
            $notes->To = Input::get('To');
            $notes->Description = Input::get('Description');
            $notes->CompanyInternalID = Auth::user()->Company->InternalID;
            $notes->UserModified = Auth::user()->UserID;
            $notes->Remark = Input::get('Remark');
            $notes->save();
            return View::make('master.notes')
                            ->withMessages('suksesUpdate')
                            ->withToogle('master')->withAktif('notes');
        }
    }

    static function deleteNotes() {
        $notes = Notes::find(Input::get('InternalID'));
        if ($notes->CompanyInternalID == Auth::user()->Company->InternalID || Auth::user()->Company->InternalID == '-1') {
            $notes->delete();
            return View::make('master.notes')
                            ->withMessages('suksesDelete')
                            ->withToogle('master')->withAktif('notes');
        } else {
            return View::make('master.notes')
                            ->withMessages('accessDenied')
                            ->withToogle('master')->withAktif('notes');
        }
    }

    static function doneNotes() {
        $notes = Notes::find(Input::get('InternalID'));
        if ($notes->CompanyInternalID == Auth::user()->Company->InternalID || Auth::user()->Company->InternalID == '-1') {
            $notes->Status = 1;
            $notes->save();

            return View::make('master.notes')
                            ->withMessages('suksesUpdate')
                            ->withToogle('master')->withAktif('notes');
        } else { //bukan se company area dan bukan super admin
            return View::make('master.notes')
                            ->withMessages('accessDenied')
                            ->withToogle('master')->withAktif('notes');
        }
    }

    public function exportExcel() {
        Excel::create('Master_Notes', function($excel) {
            $excel->sheet('Master_Notes', function($sheet) {
                $sheet->mergeCells('B1:I1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Master Notes");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "ID");
                $sheet->setCellValueByColumnAndRow(3, 2, "To");
                $sheet->setCellValueByColumnAndRow(4, 2, "Description");
                $sheet->setCellValueByColumnAndRow(5, 2, "Status");
                $sheet->setCellValueByColumnAndRow(6, 2, "Record");
                $sheet->setCellValueByColumnAndRow(7, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(8, 2, "Remark");
                $row = 3;
                $notesData = Notes::where('CompanyInternalID', Auth::user()->Company->InternalID)->get();
                foreach ($notesData as $data) {
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, "`" . $data->NotesID);
                    $sheet->setCellValueByColumnAndRow(3, $row, $data->To);
                    $sheet->setCellValueByColumnAndRow(4, $row, $data->Description);
                    if ($data->Status == 0) {
                        $sheet->setCellValueByColumnAndRow(5, $row, 'Tidak Aktif');
                    } else {
                        $sheet->setCellValueByColumnAndRow(5, $row, 'Aktif');
                    }
                    $sheet->setCellValueByColumnAndRow(6, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(7, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(8, $row, $data->Remark);
                    $row++;
                }

                if (Notes::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() <= 0) {
                    $sheet->mergeCells('B3:I3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "No data available in table");

                    $sheet->cells('B3:I3', function($cells) {
                        $cells->setAlignment('center');
                    });

                    $sheet->setBorder('B3:I' . $row, 'thin');
                }

                $row--;
                $sheet->setBorder('B2:I' . $row, 'thin');
                $sheet->cells('B2:I2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:I' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
            });
        })->export('xls');
    }

    public function notesDataBackup() {
        $table = 'm_notes';
        $primaryKey = 'InternalID';
        $columns = array(
            array('db' => 'NotesID', 'dt' => 0),
            array('db' => 'To', 'dt' => 1),
            array('db' => 'Description', 'dt' => 2),
            array('db' => 'Status', 'dt' => 3, 'formatter' => function($d, $row) {
                    if ($d == 0) {
                        return 'Waiting';
                    } else {
                        return 'Done';
                    }
                }),
            array('db' => 'InternalID', 'dt' => 4, 'formatter' => function( $d, $row ) {
                    $data = Notes::find($d);
                    $data->dtRecordformat = date("d-m-Y H:i:s", strtotime($data->dtRecord));
                    $data->dtModifformat = date("d-m-Y H:i:s", strtotime($data->dtModified));
                    $data->Remark = str_replace("\r\n", " ", $data->Remark);
                    $arrData = array($data);
                    $tamp = myEscapeStringData($arrData);
//                    <tr>
//                                <td class="text-center">
//                                    <button id="btn-{{$data->NotesID}}" data-target="#m_notesUpdate" data-all='{{$tamp}}'
//                                            data-toggle="modal" role="dialog" class="btn btn-pure-xs btn-xs btn-edit">
//                                        <span class="glyphicon glyphicon-edit"></span>
//                                    </button>
//                                    <button data-target="#m_notesDelete" data-internal="{{$data->InternalID}}"  data-toggle="modal" role="dialog"
//                                            data-id="{{$data->NotesID}}" data-name='{{$data->NotesID}}'  class="btn btn-pure-xs btn-xs btn-delete">
//                                        <span class="glyphicon glyphicon-trash"></span>
//                                    </button>
//                                    <button data-target="#m_notesDone" data-internal="{{$data->InternalID}}"  data-toggle="modal" role="dialog"
//                                            data-id="{{$data->NotesID}}" class="btn btn-pure-xs btn-xs btn-active" @if($data->Status == 1) {{"disabled";}} @endif>
//                                           <span class="glyphicon glyphicon-ok"></span>
//                                    </button>
//                                </td>
//                            </tr>
                    $return = "<button id='btn-" . $data->NotesID . "' data-target='#m_notesUpdate' data-all='" . $tamp . "'
                                                        data-toggle='modal' role='dialog' onclick='updateNotes(this)'
                                                        class='btn btn-pure-xs btn-xs btn-edit'>
                                                    <span class='glyphicon glyphicon-edit'></span>
                               </button>";
                    $return .= " <button data-target='#m_notesDelete' data-internal='" . $data->InternalID . "'  data-toggle='modal' role='dialog' onclick='deleteNotes(this)'
                                                        data-id='" . $data->NotesID . "' data-name='" . $data->NotesID . "' class='btn btn-pure-xs btn-xs btn-delete'>
                                                    <span class='glyphicon glyphicon-trash'></span>
                                  </button>";
                    if ($data->Status == 1)
                        $disabled = 'disabled';
                    else
                        $disabled = '';
                    $return .= " <button data-target='#m_notesDone' data-internal='" . $data->InternalID . "'  data-toggle='modal' role='dialog' onclick='doneNotes(this)'
                                                        data-id='" . $data->NotesID . "' data-name='" . $data->NotesID . "' class='btn btn-pure-xs btn-xs btn-active' " . $disabled . ">
                                                    <span class='glyphicon glyphicon-ok'></span>
                                  </button>";
                    return $return;
                },
                        'field' => 'InternalID')
                );
                $sql_details = getConnection();
                require('ssp.class.php');
                $ID_CLIENT_VALUE = Auth::user()->CompanyInternalID;
                $extraCondition = 'CompanyInternalID=' . $ID_CLIENT_VALUE;

                echo json_encode(
                        SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $extraCondition, $join = NULL));
            }

        }
        