<?php

class ParcelController extends BaseController {

    public function showParcel() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteParcel') {
                return $this->deleteParcel();
            }
        }
        return View::make('master.parcel.parcel')
                        ->withToogle('master')->withAktif('parcel');
    }

    public function parcelNew() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            return $this->insertParcel();
        }
        return View::make('master.parcel.parcelNew')
                        ->withToogle('master')->withAktif('parcel');
    }

    public function parcelDetail($id) {
        $id = Parcel::getIdparcel($id);
        $header = Parcel::find($id);
        $detail = ParcelInventory::where("ParcelInternalID", $header->InternalID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            return View::make('master.parcel.parcelDetail')
                            ->withToogle('master')->withAktif('parcel')
                            ->withHeader($header)
                            ->withDetail($detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showParcel');
        }
    }

    public function parcelUpdate($id) {
        $id = Parcel::getIdparcel($id);
        $header = Parcel::find($id);
        $detail = ParcelInventory::where("ParcelInternalID", $header->InternalID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                return $this->updateParcel($id);
            }
            return View::make('master.parcel.parcelUpdate')
                            ->withToogle('master')->withAktif('parcel')
                            ->withHeader($header)
                            ->withDetail($detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showParcel');
        }
    }

    public function insertParcel() {
        //rule
        $rule = array(
            'ParcelID' => 'required',
            'ParcelName' => 'required',
            'Remark' => 'required|max:1000',
            'Price' => 'required',
            'TextPrint' => 'required',
            'Commission' => 'required',
//            'BarcodeCode' => 'required',
            'inventory' => 'required'
        );
        $parcelNumber = '';
        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalInsert';
            $error = $validator->messages();
        } else {
            //valid
            //insert header
            $header = new Parcel;
            $header->ParcelID = Input::get("ParcelID");
            $header->ParcelName = Input::get("ParcelName");
            $header->TextPrint = Input::get("TextPrint");
            $header->GrandTotal = str_replace(',', '', Input::get('Price'));
            $header->BarcodeCode = Input::get('BarcodeCode');
            $header->Commission = Input::get('Commission');
            $header->UserRecord = Auth::user()->UserID;
            $header->CompanyInternalID = Auth::user()->Company->InternalID;
            $header->UserModified = '0';
            $header->Remark = Input::get('Remark');
            $header->save();
            //insert detail
            $total = 0;
            for ($a = 0; $a < count(Input::get('inventory')); $a++) {
                $qtyValue = str_replace(',', '', Input::get('qty')[$a]);
                $priceValue = str_replace(',', '', Input::get('price')[$a]);
                $subTotal = ($priceValue * $qtyValue);
                if ($qtyValue > 0) {
                    $detail = new ParcelInventory();
                    $detail->ParcelInternalID = $header->InternalID;
                    $detail->InventoryInternalID = Input::get('inventory')[$a];
                    $detail->UomInternalID = Input::get('uom')[$a];
                    $detail->Qty = $qtyValue;
                    $detail->Price = $priceValue;
                    $detail->SubTotal = $subTotal;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = '0';
                    $detail->CompanyInternalID = Auth::user()->Company->InternalID;
                    $detail->save();
                }
                $total += $subTotal;
            }
            $messages = 'suksesInsert';
            $error = '';
        }
        return View::make('master.parcel.parcelNew')
                        ->withToogle('master')->withAktif('parcel')
                        ->withError($error)
                        ->withMessages($messages);
    }

    public function updateParcel($id) {
        //tipe
        $headerUpdate = Parcel::find($id);
        //rule
        $rule = array(
            'ParcelName' => 'required',
            'Remark' => 'required|max:1000',
            'Price' => 'required',
            'TextPrint' => 'required',
            'Commission' => 'required',
//            'BarcodeCode' => 'required',
            'inventory' => 'required'
        );
        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalUpdate';
            $error = $validator->messages();
        } else {
            $header = Parcel::find(Input::get('ParcelInternalID'));
            $currency = explode('---;---', Input::get('Currency'));
            $header->ParcelName = Input::get('ParcelName');
            $header->Commission = Input::get('Commission');
            $header->TextPrint = Input::get('TextPrint');
            $header->BarcodeCode = Input::get('BarcodeCode');
            $header->Commission = Input::get('Commission');
            $header->GrandTotal = str_replace(',', '', Input::get('Price'));
            $header->UserModified = Auth::user()->UserID;
            $header->Remark = Input::get('Remark');
            $header->save();

            //delete parcel detail -- nantinya insert ulang
            ParcelInventory::where("ParcelInternalID", $headerUpdate->InternalID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->delete();

            //insert detail
            $total = 0;
            for ($a = 0; $a < count(Input::get('inventory')); $a++) {
                $qtyValue = str_replace(',', '', Input::get('qty')[$a]);
                $priceValue = str_replace(',', '', Input::get('price')[$a]);
                $subTotal = ($priceValue * $qtyValue);
                $total += $subTotal;
                if ($qtyValue > 0) {
                    $detail = new ParcelInventory();
                    $detail->ParcelInternalID = Input::get('ParcelInternalID');
                    $detail->InventoryInternalID = Input::get('inventory')[$a];
                    $detail->UomInternalID = Input::get('uom')[$a];
                    $detail->Qty = $qtyValue;
                    $detail->Price = $priceValue;
                    $detail->SubTotal = $subTotal;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = Auth::user()->UserID;
                    $detail->CompanyInternalID = Auth::user()->Company->InternalID;
                    $detail->save();
                }
            }
            $messages = 'suksesUpdate';
            $error = '';
        }

        //tipe
        $header = Parcel::find($id);
        $detail = ParcelInventory::where("ParcelInternalID", $header->InternalID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->get();
        return View::make('master.parcel.parcelUpdate')
                        ->withToogle('master')->withAktif('parcel')
                        ->withHeader($header)
                        ->withDetail($detail)
                        ->withError($error)
                        ->withMessages($messages);
    }

    public function deleteParcel() {
        $parcel = Parcel::find(Input::get('InternalID'));

        if (count($parcel) > 0) {
            //tidak ada yang menggunakan data parcel maka data boleh dihapus
            //hapus journal
            if ($parcel->CompanyInternalID == Auth::user()->Company->InternalID) {
                //hapus detil
                $detilData = ParcelInventory::where("ParcelInternalID", $parcel->InternalID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->get();
                foreach ($detilData as $value) {
                    $detil = ParcelInventory::find($value->InternalID);
                    $detil->delete();
                }
                //hapus parcel
                $parcel = Parcel::find(Input::get('InternalID'));
                $parcel->delete();
                $messages = 'suksesDelete';
            } else {
                $messages = 'accessDenied';
            }
        } else {
            //ada maka data tidak boleh dihapus
            $messages = 'gagalDelete';
        }
        return View::make('master.parcel.parcel')
                        ->withToogle('master')->withAktif('parcel')
                        ->withMessages($messages);
    }

    public function getNextIDParcel($text) {
        $query = 'SELECT ParcelID From m_parcel Where ParcelID LIKE "' . $text . '%" AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" order by ParcelID desc';
        $parcelID = DB::select(DB::raw($query));

        if (count($parcelID) <= 0) {
            $parcelID = '';
        } else {
            $parcelID = $parcelID[0]->ParcelID;
        }

        if ($parcelID == '') {
            $parcelID = $text . '0001';
        } else {
            $textTamp = $parcelID;
            $parcelID = substr($textTamp, (strlen($textTamp) - 4), 4) + 1;
            $parcelID = str_pad($parcelID, 4, '0', STR_PAD_LEFT);
            $parcelID = $text . $parcelID;
        }
        return $parcelID;
    }

    function parcelPrint($id) {
        $id = Parcel::getIdparcel($id);
        $header = Parcel::find($id);
        $detail = ParcelInventory::where("ParcelInternalID", $header->InternalID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                    .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Parcel</h5>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 18px; width: 100%;">
                            <table>
                            <tr>
                                    <td width="423px">
                                        <table>
                                         <tr style="background: none;">
                                            <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Parcel ID</td>
                                            <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                            <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $header->ParcelID . '</td>
                                         </tr>
                                         <tr style="background: none;">
                                            <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Name</td>
                                            <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                            <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $header->ParcelName . '</td>
                                         </tr>
                                        </table>
                                    </td>
                                    <td width="423px">
                                        <table>
                                            <tr style="background: none;">
                                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Remark</td>
                                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $header->Remark . '</td>
                                             </tr>
                                         </table>
                                    </td>
                            </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Uom</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Qty</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Price</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            $total = 0;
            if (count($detail) > 0) {
                foreach ($detail as $data) {
                    $inventory = Inventory::find($data->InventoryInternalID);
                    $inv = $inventory->InventoryID . ' ' . $inventory->InventoryName;
                    $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $inv . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->uom->UomID . ' (' . number_format(InventoryUom::where('InventoryInternalID', $data->InventoryInternalID)->where('UomInternalID', $data->UomInternalID)->first()->Value, 0, '.', ',') . ')</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
                    $total += $data->SubTotal;
                }
            } else {
                $html.= '<tr>
                            <td colspan="5" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;  text-align: center">There is no inventory registered in this parcel.</td>
                        </tr>';
            }
            $html.= '</tbody>
                            </table>
                            <br>
                            <div style="box-sizing: border-box;min-width: 220px; margin-left: 548px; display: inline-block; clear: both;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Grand Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '</td>
                                 </tr>
                                </table>
                            </div>
                    </div>
                </body>
            </html>';
            return PDF ::load($html, 'A4', 'portrait')->show();
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showParcel');
        }
    }

    public function exportExcel() {
        Excel::create('Master_Parcel', function($excel) {
            $excel->sheet('Master_Parcel', function($sheet) {
                $sheet->mergeCells('B1:J1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Master Parcel");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Parcel ID");
                $sheet->setCellValueByColumnAndRow(3, 2, "Name");
                $sheet->setCellValueByColumnAndRow(4, 2, "Barcode");
                $sheet->setCellValueByColumnAndRow(5, 2, "GrandTotal");
                $sheet->setCellValueByColumnAndRow(6, 2, "Record");
                $sheet->setCellValueByColumnAndRow(7, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(8, 2, "Remark");
                $sheet->setCellValueByColumnAndRow(9, 2, "Detail");
                $row = 3;
                $nomor = 1;
                foreach (Parcel::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                    $sheet->setCellValueByColumnAndRow(1, $row, $nomor);
                    $sheet->setCellValueByColumnAndRow(2, $row, "`" . $data->ParcelID);
                    $sheet->setCellValueByColumnAndRow(3, $row, $data->ParcelName);
                    $sheet->setCellValueByColumnAndRow(4, $row, "`" . $data->BarcodeCode);

                    //loop detailnya

                    $sheet->setCellValueByColumnAndRow(5, $row, number_format($data->GrandTotal, 2, ".", ","));
                    $sheet->setCellValueByColumnAndRow(6, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(7, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(8, $row, $data->Remark);
                    $tampRow = $row;
                    $count = 0;
                    foreach (ParcelInventory::where("ParcelInternalID", $data->InternalID)->where('CompanyInternalID', $data->CompanyInternalID)->get() as $detail) {
                        $sheet->setCellValueByColumnAndRow(9, $tampRow, $detail->Inventory->InventoryName . " - " . $detail->Uom->UomID);
                        $tampRow++;
                        $count++;
                    }
                    $row += $count;
                    $nomor++;
                }

                if (Parcel::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() <= 0) {
                    $sheet->mergeCells('B3:J3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "No data available in table");
                    $sheet->cells('B3:J3', function($cells) {
                        $cells->setAlignment('center');
                    });

                    $sheet->setBorder('B3:J' . $row, 'thin');
                }

                $row--;
                $sheet->setBorder('B2:J' . $row, 'thin');
                $sheet->cells('B2:J2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:J' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
                $sheet->cells('F3:F' . $row, function($cells) {
                    $cells->setAlignment('right');
                });
            });
        })->export('xls');
    }

    //==================ajax==========================
    public function getUomThisInventoryParcel() {
        $inventoryUom = InventoryUom::where("InventoryInternalID", Input::get("id"))->get();
        foreach ($inventoryUom as $data) {
            ?>
            <option value="<?php echo $data->UomInternalID ?>" <?php echo ($data->Default == 1 ? "selected" : "") ?>><?php echo $data->Uom->UomID; ?></option>
            <?php
        }
    }

    public function getHPPValueInventoryParcel() {
        $date = Input::get("date");
        $arrDate = explode("-", $date);
        $inventoryInternalID = Input::get("inventory");
        $uomInternalID = Input::get("uom");

        $inventoryUomValue = InventoryUom::where("InventoryInternalID", $inventoryInternalID)->where("UomInternalID", $uomInternalID)->pluck("Value");
        $month = $arrDate[1] - 1;
        $year = $arrDate[2];
        if ($month == 0) {
            $month = 12;
            $year = $arrDate[2] - 1;
        }
        $hpp = 0;
        $value = InventoryValue::where("InventoryInternalID", $inventoryInternalID)
                        ->where("Month", $month)
                        ->where("Year", $year)
                        ->get();
        if (count($value) == 0) {
            $hpp = Inventory::find($inventoryInternalID)->InitialValue * $inventoryUomValue;
        } else {
            $hpp = $value[0]->Value * $inventoryUomValue;
        }
        return $hpp;
    }

    public function checkParcelID() {
        $parcelID = Input::get("ParcelID");
        $count = 0;
        $count = Parcel::where("ParcelID", $parcelID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->count();
        if ($count == 0) {
            return 0;
        } else {
            return 1;
        }
    }

    public function checkBarcodeCode() {
        $barcodeCode = Input::get("BarcodeCode");
        $InventoryName = "";
        $inventory = Inventory::where("BarcodeCode", $barcodeCode)->where("CompanyInternalID", Auth::user()->Company->InternalID)->get();
        $parcel = Parcel::where("BarcodeCode", $barcodeCode)->where("CompanyInternalID", Auth::user()->Company->InternalID)->get();
        if (count($inventory) == 0 && count($parcel) == 0) {
            return 0;
        } else {
            return 1;
        }
    }

    public function checkParcelIDUpdate() {
        $parcelID = Input::get("ParcelID");
        $count = 0;
        $count = Parcel::where("ParcelID", "!=", $parcelID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->count();
        if ($count == 0) {
            return 0;
        } else {
            return 1;
        }
    }

    public function getSearchResultInventoryParcel() {
        $input = splitSearchValue(Input::get("id"));
        $dataInventory = Inventory::select('m_inventory.*')->distinct()
                ->join('m_inventory_uom', 'm_inventory_uom.InventoryInternalID', '=', 'm_inventory.InternalID')
                ->where("m_inventory.Status", 0)
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where("m_inventory.InventoryName", "like", $input)
                ->orWhere("m_inventory.InventoryID", "like", $input)
                ->where("m_inventory.Status", 0)
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->get();

        if (count($dataInventory) == 0) {
            ?>
            <span>Inventory with keywords <i><u><?php echo Input::get("id"); ?></u></i> can't be found</span>
            <?php
        } else {
            ?>
            <select class="input-theme left inventory" id="inventory-0">
                <?php
                foreach ($dataInventory as $inventory) {
                    ?>
                    <option id="inventory<?php echo $inventory->InternalID ?>" value="<?php echo $inventory->InternalID ?>">
                        <?php echo $inventory->InventoryID . ' ' . $inventory->InventoryName; ?>
                    </option>
                    <?php
                }
                ?>
            </select>
            <script type="text/javascript" src="<?php echo Asset('js/entry-js-controller/controllerParcel.js') ?>"></script>
            <?php
        }
    }

    public function getNextParcelID() {
        $text = $this->getNextIDParcel("PKG." . date('m') . date('y'));
        return $text;
    }

    public function parcelDataBackup() {
        $table = 'm_parcel';
        $primaryKey = 'InternalID';
        $columns = array(
            array('db' => 'ParcelID', 'dt' => 0),
            array('db' => 'ParcelName', 'dt' => 1),
            array('db' => 'TextPrint', 'dt' => 2),
            array('db' => 'barcodeCode', 'dt' => 3),
            array('db' => 'GrandTotal', 'dt' => 4,'formatter'=>function($d,$row){
                return number_format($d, 2, '.', ',');
            }),
           
            array('db' => 'InternalID', 'dt' => 5, 'formatter' => function( $d, $row ) {
                    $data = Parcel::find($d);
                   
                    $return = '<a href="'.Route('parcelDetail',$data->ParcelID).'">
                                        <button id="btn-'.$data->InternalID.'-detail"
                                                class="btn btn-pure-xs btn-xs btn-detail">
                                            <span class="glyphicon glyphicon-zoom-in"></span>
                                        </button>
                                    </a>';
                    $return .= '  <a href="'.Route('parcelUpdate',$data->ParcelID).'">
                                        <button id="btn-'.$data->InternalID.'-update"
                                                class="btn btn-pure-xs btn-xs btn-edit">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </button>
                                    </a>';
                    $return .= ' <button data-target="#m_parcelDelete" data-internal="'.$data->InternalID.'"  data-toggle="modal" role="dialog"
                                            data-id="'.$data->InternalID.'" onclick="deleteAttach(this)" data-name="'.$data->ParcelName.'"  class="btn btn-pure-xs btn-xs btn-delete">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>';

                  
                    return $return;
                },
                        'field' => 'InternalID')
                );
                $sql_details = getConnection();
                require('ssp.class.php');
                $ID_CLIENT_VALUE = Auth::user()->CompanyInternalID;
                $extraCondition = '';


                echo json_encode(
                        SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $extraCondition, $join = NULL));


            }

            //==================ajax==========================
        }
        