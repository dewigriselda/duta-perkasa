<?php

class SalesManController extends BaseController {

    public function showSalesMan() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertSalesMan') {
                return $this->insertSalesMan();
            }
            if (Input::get('jenis') == 'updateSalesMan') {
                return $this->updateSalesMan();
            }
            if (Input::get('jenis') == 'activeSalesMan') {
                return $this->activeSalesMan();
            }
            if (Input::get('jenis') == 'nonActiveSalesMan') {
                return $this->nonActiveSalesMan();
            }
            if (Input::get('jenis') == 'reportSalesMan') {
                return $this->reportSalesMan();
            }
            if (Input::get('jenis') == 'deleteSalesMan') {
                return $this->deleteSalesMan();
            }
        }
        return View::make('master.salesMan')
                        ->withToogle('master')->withAktif('salesMan');
    }

    public static function insertSalesMan() {
        //rule
        $rule = array(
            'SalesManID' => 'required|max:16|unique:m_sales_man,SalesManID',
            'SalesManName' => 'required|max:200',
            'Address' => 'required|max:200',
            'Phone' => 'required|max:200',
            'Email' => 'required|email|max:200|unique:m_sales_man,Email',
            'Remark' => 'required|max:1000'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('master.salesMan')
                            ->withMessages('gagalInsert')
                            ->withError($validator->messages())
                            ->withToogle('master')->withAktif('salesMan')
                            ->withErrors($validator);
        } else {
            //valid
            $salesMan = new SalesMan;
            $salesMan->SalesManID = Input::get('SalesManID');
            $salesMan->SalesManName = Input::get('SalesManName');
            $salesMan->Address = Input::get('Address');
            $salesMan->Phone = Input::get('Phone');
            $salesMan->Email = Input::get('Email');
            $salesMan->CompanyInternalID = Auth::user()->Company->InternalID;
            $salesMan->Status = 1;
            $salesMan->UserRecord = Auth::user()->UserID;
            $salesMan->UserModified = "0";
            $salesMan->Remark = Input::get('Remark');
            $salesMan->save();

            return View::make('master.salesMan')
                            ->withMessages('suksesInsert')
                            ->withToogle('master')->withAktif('salesMan');
        }
    }

    static function updateSalesMan() {
        //rule
        $rule = array(
            'SalesManName' => 'required|max:200',
            'Address' => 'required|max:200',
            'Phone' => 'required|max:200',
            'Remark' => 'required|max:1000'
        );
        //cek Email sama enggak
        $emailLama = SalesMan::find(Input::get('InternalID'))->Email;
        $data = Input::all();
        if (strtoupper(Input::get('Email')) == strtoupper($emailLama)) {
            $rule2 = array(
                'Email' => 'required|max:200|email'
            );
            $rule = array_merge($rule, $rule2);
        } else {
            $rule2 = array(
                'Email' => 'required|max:200|email|unique:m_sales_man,Email'
            );
            $rule = array_merge($rule, $rule2);
        }

        //validasi
        $validator = Validator::make(Input::all(), $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('master.salesMan')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('master')->withAktif('salesMan');
        } else {
            //valid
            $salesMan = SalesMan::find(Input::get('InternalID'));
            $salesMan->SalesManName = Input::get('SalesManName');
            $salesMan->CompanyInternalID = Auth::user()->Company->InternalID;
            $salesMan->UserModified = Auth::user()->UserID;
            $salesMan->Address = Input::get('Address');
            $salesMan->Phone = Input::get('Phone');
            $salesMan->Email = Input::get('Email');
            $salesMan->Remark = Input::get('Remark');
            $salesMan->save();
            return View::make('master.salesMan')
                            ->withMessages('suksesUpdate')
                            ->withToogle('master')->withAktif('salesMan');
        }
    }

    static function deleteSalesMan() {
        $salesMan = SalesMan::find(Input::get('InternalID'));
        $sales = DB::table('t_sales_header')->where('SalesManInternalID', $salesMan->InternalID)->where('CompanyInternalID', Auth::user()->CompanyInternalID)->first();
        $salesOrder = DB::table('t_salesorder_header')->where('SalesManInternalID', $salesMan->InternalID)->where('CompanyInternalID', Auth::user()->CompanyInternalID)->first();
        //cek salesMan ada di pakai atau tidak
        if (is_null($sales) && is_null($salesOrder)) {
            //tidak ada maka boleh dihapus
            $salesMan = SalesMan::find(Input::get('InternalID'));
            if ($salesMan->CompanyInternalID == Auth::user()->Company->InternalID || Auth::user()->Company->InternalID == '-1') {
                $salesMan->delete();
                return View::make('master.salesMan')
                                ->withMessages('suksesDelete')
                                ->withToogle('master')->withAktif('salesMan');
            } else {
                return View::make('master.salesMan')
                                ->withMessages('accessDenied')
                                ->withToogle('master')->withAktif('salesMan');
            }
        } else {
            //ada maka tidak dihapus
            return View::make('master.salesMan')
                            ->withMessages('gagalDelete')
                            ->withToogle('master')->withAktif('salesMan');
        }
    }

    static function nonActiveSalesMan() {
        $salesMan = SalesMan::find(Input::get('InternalID'));
        if ($salesMan->CompanyInternalID == Auth::user()->Company->InternalID || Auth::user()->Company->InternalID == '-1') {

            $salesMan->Status = 0;
            $salesMan->save();

            return View::make('master.salesMan')
                            ->withMessages('suksesUpdate')
                            ->withToogle('master')->withAktif('salesMan');
        } else { //bukan se company area dan bukan super admin
            return View::make('master.salesMan')
                            ->withMessages('accessDenied')
                            ->withToogle('master')->withAktif('salesMan');
        }
    }

    static function activeSalesMan() {
        $salesMan = SalesMan::find(Input::get('InternalID'));
        if ($salesMan->CompanyInternalID == Auth::user()->Company->InternalID || Auth::user()->Company->InternalID == '-1') {

            $salesMan->Status = 1;
            $salesMan->save();

            return View::make('master.salesMan')
                            ->withMessages('suksesUpdate')
                            ->withToogle('master')->withAktif('salesMan');
        } else { //bukan se company area dan bukan super admin
            return View::make('master.salesMan')
                            ->withMessages('accessDenied')
                            ->withToogle('master')->withAktif('salesMan');
        }
    }

    public function exportExcel() {
        Excel::create('Master_Sales_Man', function($excel) {
            $excel->sheet('Master_Sales_Man', function($sheet) {
                $sheet->mergeCells('B1:K1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Master SalesMan");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Name");
                $sheet->setCellValueByColumnAndRow(3, 2, "ID");
                $sheet->setCellValueByColumnAndRow(4, 2, "Address");
                $sheet->setCellValueByColumnAndRow(5, 2, "Email");
                $sheet->setCellValueByColumnAndRow(6, 2, "Phone");
                $sheet->setCellValueByColumnAndRow(7, 2, "Status");
                $sheet->setCellValueByColumnAndRow(8, 2, "Record");
                $sheet->setCellValueByColumnAndRow(9, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(10, 2, "Remark");
                $row = 3;
                $salesManData = SalesMan::where('CompanyInternalID', Auth::user()->Company->InternalID)->get();
                foreach ($salesManData as $data) {
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, $data->SalesManName);
                    $sheet->setCellValueByColumnAndRow(3, $row, "`" . $data->SalesManID);
                    $sheet->setCellValueByColumnAndRow(4, $row, $data->Address);
                    $sheet->setCellValueByColumnAndRow(5, $row, $data->Email);
                    $sheet->setCellValueByColumnAndRow(6, $row, '`' . $data->Phone);
                    if ($data->Status == 0) {
                        $sheet->setCellValueByColumnAndRow(7, $row, 'Tidak Aktif');
                    } else {
                        $sheet->setCellValueByColumnAndRow(7, $row, 'Aktif');
                    }
                    $sheet->setCellValueByColumnAndRow(8, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(9, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(10, $row, $data->Remark);
                    $row++;
                }

                if (SalesMan::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() <= 0) {
                    $sheet->mergeCells('B3:K3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "No data available in table");

                    $sheet->cells('B3:K3', function($cells) {
                        $cells->setAlignment('center');
                    });

                    $sheet->setBorder('B3:J' . $row, 'thin');
                }

                $row--;
                $sheet->setBorder('B2:K' . $row, 'thin');
                $sheet->cells('B2:K2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:K' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
            });
        })->export('xls');
    }

    public function reportSalesMan() {
        $sumGrandTotal = 0;
        $sumPayment = 0;
        $sumCommission = 0;
        $salesManInternalID = Input::get("SalesManInternalID");
        $salesName = SalesMan::find($salesManInternalID)->SalesManName;
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Sales Man Summary Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span>';

        $dataSales = SalesMan::find($salesManInternalID);
        $html .= '<table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=9>' . $dataSales->SalesManName . '</th>
                                        </tr>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Currency</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Rate</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Vat</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Grand Total</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total Payment</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Commission</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
        if (SalesAddHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where('SalesManInternalID', $dataSales->InternalID)
                        ->whereBetween('SalesDate', Array($start, $end))->count() > 0) {
            foreach (SalesAddHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('SalesManInternalID', $dataSales->InternalID)
                    ->whereBetween('SalesDate', Array($start, $end))->get() as $data) {
                $grandTotal = $data->GrandTotal;
                $total = $grandTotal;
                $vat = 0;
                $payment = $grandTotal;
                $commission = $data->PurchasingCommissions;
                if ($data->isCash == 1) {
                    $payment = JournalDetail::where('JournalTransactionID', $data->SalesID)->sum('JournalCreditMU');
                }
                if ($data->VAT == 1) {
                    $total = $total * 10 / 11;
                    $vat = $total / 10;
                }
                $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->SalesID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->SalesDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->Currency->CurrencyName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->CurrencyRate, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($total, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($vat, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($grandTotal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($payment, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">(' . number_format($commission, '2', '.', ',') . ')</td>
                            </tr>';
                $sumGrandTotal += $grandTotal;
                $sumPayment += $payment;
                $sumCommission += $commission;
            }
            $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif; margin: 5px !important;" colspan="9"><hr></td>
                            </tr>';
            $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="80%" colspan="7">Total : </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . number_format($sumPayment, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">(' . number_format($sumCommission, '2', '.', ',') . ')</td>
                            </tr>';
        } else {
            $html .= '<tr>
                            <td colspan="9" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: center">There is no sales order registered.</td>
                        </tr>';
        }

        $html .= '</tbody>
            </table>';

        $html .= '</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('report_sales_man_' . $salesName);
    }

}
