<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class UomController extends BaseController {

    public function showUom() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertUom') {
                return $this->insertUom();
            }
            if (Input::get('jenis') == 'updateUom') {
                return $this->updateUom();
            }
            if (Input::get('jenis') == 'deleteUom') {
                return $this->deleteUom();
            }
        }
        return View::make('master.uom')
                        ->withToogle('master')->withAktif('uom');
    }

    public function insertUom() {
        $data = Input::all();
        $rule = array(
            "UomID" => 'required|max:200|unique:m_uom,UomID,NULL,UomID,CompanyInternalID,' . Auth::user()->Company->InternalID . '',
            "remark" => 'required'
        );
        $validator = Validator::make($data, $rule);
        if ($validator->Fails()) {
            return View::make('master.uom')
                            ->withMessages("gagalInsert")
                            ->withErrors($validator->messages())
                            ->withToogle('master')->withAktif('uom');
        } else {
            $uom = new Uom();
            $uom->UomID = Input::get('UomID');
            $uom->Remark = Input::get('remark');
            $uom->CompanyInternalID = Auth::user()->Company->InternalID;
            $uom->UserModified = "0";
            $uom->UserRecord = Auth::user()->UserID;
            $uom->save();

            return View::make('master.uom')
                            ->withMessages("suksesInsert")
                            ->withToogle('master')->withAktif('uom');
        }
    }

    public function updateUom() {
        $data = Input::all();
        $rule = array(
            "remark" => 'required'
        );
        $validator = Validator::make($data, $rule);
        if ($validator->Fails()) {
            return View::make('master.uom')
                            ->withMessages("gagalInsert")
                            ->withErrors($validator->messages())
                            ->withToogle('master')->withAktif('uom');
        } else {
            $uom = Uom::find(Input::get('InternalID'));
            $uom->Remark = Input::get('remark');
            $uom->CompanyInternalID = Auth::user()->Company->InternalID;
            $uom->UserModified = Auth::user()->UserID;
            $uom->save();

            return View::make('master.uom')
                            ->withMessages("suksesUpdate")
                            ->withToogle('master')->withAktif('uom');
        }
    }

    public function deleteUom() {
        $Uom = DB::table('m_inventory_uom')->where('UomInternalID', Input::get('InternalID'))->first();
        if (is_null($Uom)) {
            //tidak ada, maka boleh hapus
            $uom = Uom::find(Input::get('InternalID'));
            if ($uom->CompanyInternalID == Auth::user()->Company->InternalID) {
                $uom->delete();
                return View::make('master.uom')
                                ->withMessages('suksesDelete')
                                ->withToogle('master')->withAktif('uom');
            } else {
                return View::make('master.uom')
                                ->withMessages('accessDenied')
                                ->withToogle('master')->withAktif('uom');
            }
        } else {
            //ada maka tidak dihapus
            return View::make('master.uom')
                            ->withMessages('gagalDelete')
                            ->withToogle('master')->withAktif('uom');
        }
    }

    public function exportUom() {
        Excel::create('Master_Uom', function($excel) {
            $excel->sheet('Master_Uom', function($sheet) {
                $sheet->mergeCells('B1:F1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Master Uom");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Uom ID");
                $sheet->setCellValueByColumnAndRow(3, 2, "Record");
                $sheet->setCellValueByColumnAndRow(4, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(5, 2, "Remark");
                $row = 3;
                foreach (Uom::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, "`" . $data->UomID);
                    $sheet->setCellValueByColumnAndRow(3, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(4, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(5, $row, $data->Remark);
                    $row++;
                }

                if (Uom::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() <= 0) {
                    $sheet->mergeCells('B3:F3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "No data available in table");

                    $sheet->cells('B3:F3', function($cells) {
                        $cells->setAlignment('center');
                    });
                    $sheet->setBorder('B3:F' . $row, 'thin');
                }

                $row--;
                $sheet->setBorder('B2:F' . $row, 'thin');
                $sheet->cells('B2:F2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:F' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
            });
        })->export('xls');
    }

}
