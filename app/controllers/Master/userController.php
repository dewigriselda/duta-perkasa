<?php

class UserController extends BaseController {

    public function showUser() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertUser') {
                return $this->insertUser();
            }
            if (Input::get('jenis') == 'updateUser') {
                return $this->updateUser();
            }
            if (Input::get('jenis') == 'activeUser') {
                return $this->activeUser();
            }
            if (Input::get('jenis') == 'nonActiveUser') {
                return $this->nonActiveUser();
            }
            if (Input::get('jenis') == 'deleteUser') {
                return $this->deleteUser();
            }
            if (Input::get('jenis') == 'commissionReport') {
                return $this->commissionReport();
            }
        }
        return View::make('master.user')
                        ->withToogle('master')->withAktif('user');
    }

    public function showUserMatrix($userID) {
        $user = User::where("UserID", $userID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->first();
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertUserMatrix') {
                return $this->insertUserMatrix($user);
            }
        }
        return View::make('master.userMatrix')
                        ->withToogle('master')
                        ->withAktif('user')
                        ->withUser($user);
    }

    public function settProfile() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'updateProfile') {
                return $this->updateProfile();
            } else {
                return $this->updatePassword();
            }
        }
        return View::make('setting.profile')
                        ->withToogle('setting')->withAktif('profile');
    }

    public static function insertUser() {
        //rule
        $rule = array(
            'UserID' => 'required|max:16|unique:m_user,UserID',
            'Name' => 'required|max:200',
            'Password' => 'required|max:16',
            'Phone' => 'required|max:200',
            'Warehouse' => 'required',
            'Email' => 'required|email|max:200|unique:m_user,Email',
            'remark' => 'required|max:1000'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('master.user')
                            ->withMessages('gagalInsert')
                            ->withError($validator->messages())
                            ->withToogle('master')->withAktif('user')
                            ->withErrors($validator);
        } else {
            //valid
            $user = new User;
            $user->UserID = Input::get('UserID');
            $user->UserName = Input::get('Name');
            $user->Phone = Input::get('Phone');
            $user->Email = Input::get('Email');
            $user->UserPwd = Hash::make(Input::get('Password'));
            $user->WarehouseInternalID = Input::get('Warehouse');
            if (Auth::user()->Company->InternalID == '-1') {
                $user->CompanyInternalID = Input::get('company');
            } else {
                $user->CompanyInternalID = Auth::user()->Company->InternalID;
            }
            $user->Picture = NULL;
            $user->Status = 1;
            if (Input::get("see") == '')
                $user->SeeNPPN = 0;
            else
                $user->SeeNPPN = Input::get("see");
            $user->Approval = Input::get("approval");
            $user->UserRecord = Auth::user()->UserID;
            $user->UserModified = "0";
            $user->Remark = Input::get('remark');
            $user->save();

            if (Auth::user()->Company->PackageInternalID == 1) {
                $array = explode(',', Package::find(1)->MatrixNotIn);
                foreach (Matrix::whereNotIn('MatrixID', $array)->get() as $data) {
                    $userDetail = new UserDetail();
                    $userDetail->UserInternalID = $user->InternalID;
                    $userDetail->MatrixInternalID = $data->InternalID;
                    $userDetail->save();
                }
            }

            return View::make('master.user')
                            ->withMessages('suksesInsert')
                            ->withToogle('master')->withAktif('user');
        }
    }

    public static function insertUserMatrix($user) {
        $dataMatrix = Input::get("matrix");
        UserDetail::where("UserInternalID", $user->InternalID)->delete();

        if (!is_null($dataMatrix)) {
            foreach ($dataMatrix as $data) {
                $userDetail = new UserDetail();
                $userDetail->UserInternalID = $user->InternalID;
                $userDetail->MatrixInternalID = $data;
                $userDetail->save();
            }
        }
        return View::make('master.userMatrix')
                        ->withToogle('master')
                        ->withAktif('user')
                        ->withUser($user)
                        ->withMessages("suksesUpdate");
    }

    static function updateUser() {
        //rule
        $rule = array(
            'Name' => 'required|max:200',
            'remark' => 'required|max:1000',
            'Phone' => 'required|max:200'
        );
        //cek Email sama enggak
        $emailLama = User::find(Input::get('InternalID'))->Email;
        $data = Input::all();
        if (strtoupper(Input::get('Email')) == strtoupper($emailLama)) {
            $rule2 = array(
                'Email' => 'required|max:200|email'
            );
            $rule = array_merge($rule, $rule2);
        } else {
            $rule2 = array(
                'Email' => 'required|max:200|email|unique:m_user,Email'
            );
            $rule = array_merge($rule, $rule2);
        }

        //validasi
        $validator = Validator::make(Input::all(), $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('master.user')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('master')->withAktif('user');
        } else {
            //valid
            $user = User::find(Input::get('InternalID'));
            if ($user->CompanyInternalID == Auth::user()->Company->InternalID || Auth::user()->Company->InternalID == '-1') {
                $user->UserName = Input::get('Name');
                if (Auth::user()->Company->InternalID == '-1') {
                    $user->CompanyInternalID = Input::get('company');
                } else {
                    $user->CompanyInternalID = Auth::user()->Company->InternalID;
                }
                $user->UserModified = Auth::user()->UserID;
                $user->WarehouseInternalID = Input::get('Warehouse');
                $user->Phone = Input::get('Phone');
                $user->Email = Input::get('Email');
                $user->Remark = Input::get('remark');
                $user->SeeNPPN = Input::get("see");
                $user->Approval = Input::get("approval");
                $user->save();
                return View::make('master.user')
                                ->withMessages('suksesUpdate')
                                ->withToogle('master')->withAktif('user');
            } else {
                return View::make('master.user')
                                ->withMessages('accessDenied')
                                ->withToogle('master')->withAktif('user');
            }
        }
    }

    static function deleteUser() {
        $user = User::find(Input::get('InternalID'));
        $sales = DB::table('t_sales_header')->where('UserRecord', $user->UserID)->orWhere('UserModified', $user->UserID)->where('CompanyInternalID', Auth::user()->CompanyInternalID)->first();
        $purchase = DB::table('t_purchase_header')->where('UserRecord', $user->UserID)->orWhere('UserModified', $user->UserID)->where('CompanyInternalID', Auth::user()->CompanyInternalID)->first();
        $salesOrder = DB::table('t_salesorder_header')->where('UserRecord', $user->UserID)->orWhere('UserModified', $user->UserID)->where('CompanyInternalID', Auth::user()->CompanyInternalID)->first();
        $purchaseOrder = DB::table('t_purchaseorder_header')->where('UserRecord', $user->UserID)->orWhere('UserModified', $user->UserID)->where('CompanyInternalID', Auth::user()->CompanyInternalID)->first();
        $salesReturn = DB::table('t_salesreturn_header')->where('UserRecord', $user->UserID)->orWhere('UserModified', $user->UserID)->where('CompanyInternalID', Auth::user()->CompanyInternalID)->first();
        $purchaseReturn = DB::table('t_purchasereturn_header')->where('UserRecord', $user->UserID)->orWhere('UserModified', $user->UserID)->where('CompanyInternalID', Auth::user()->CompanyInternalID)->first();
        //cek user ada di pakai atau tidak
        if (is_null($sales) && is_null($purchase) && is_null($salesOrder) && is_null($purchaseOrder) && is_null($salesReturn) && is_null($purchaseReturn)) {
            //tidak ada maka boleh dihapus
            $user = User::find(Input::get('InternalID'));
            if ($user->CompanyInternalID == Auth::user()->Company->InternalID || Auth::user()->Company->InternalID == '-1') {
                $user->delete();
                return View::make('master.user')
                                ->withMessages('suksesDelete')
                                ->withToogle('master')->withAktif('user');
            } else {
                return View::make('master.user')
                                ->withMessages('accessDenied')
                                ->withToogle('master')->withAktif('user');
            }
        } else {
            //ada maka tidak dihapus
            return View::make('master.user')
                            ->withMessages('gagalDelete')
                            ->withToogle('master')->withAktif('user');
        }
    }

    static function nonActiveUser() {
        $user = User::find(Input::get('InternalID'));
        if ($user->CompanyInternalID == Auth::user()->Company->InternalID || Auth::user()->Company->InternalID == '-1') {

            $user->Status = 0;
            $user->save();

            return View::make('master.user')
                            ->withMessages('suksesUpdate')
                            ->withToogle('master')->withAktif('user');
        } else { //bukan se company area dan bukan super admin
            return View::make('master.user')
                            ->withMessages('accessDenied')
                            ->withToogle('master')->withAktif('user');
        }
    }

    static function activeUser() {
        $user = User::find(Input::get('InternalID'));
        if ($user->CompanyInternalID == Auth::user()->Company->InternalID || Auth::user()->Company->InternalID == '-1') {

            $user->Status = 1;
            $user->save();

            return View::make('master.user')
                            ->withMessages('suksesUpdate')
                            ->withToogle('master')->withAktif('user');
        } else { //bukan se company area dan bukan super admin
            return View::make('master.user')
                            ->withMessages('accessDenied')
                            ->withToogle('master')->withAktif('user');
        }
    }

    public function updateProfile() {
        //rule
        $rule = array(
            'Name' => 'required|max:200',
            'Phone' => 'required|max:200',
            'ProfilePicture' => 'image'
        );
        //cek Email sama enggak
        $emailLama = User::find(Auth::user()->InternalID)->Email;
        $data = Input::all();
        if (strtoupper(Input::get('Email')) == strtoupper($emailLama)) {
            $rule2 = array(
                'Email' => 'required|max:200|email'
            );
            $rule = array_merge($rule, $rule2);
        } else {
            $rule2 = array(
                'Email' => 'required|max:200|email|unique:m_user,Email'
            );
            $rule = array_merge($rule, $rule2);
        }

        //validasi
        $validator = Validator::make(Input::all(), $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('setting.profile')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('setting')->withAktif('profile');
        } else {
            $path = NULL;
            $input = Input::file('ProfilePicture');
            if ($input != '') {
                if (Input::hasFile('ProfilePicture') && Input::file('ProfilePicture')->getSize() <= 2000000) {
                    $destination = base_path() . '/uploaded_img/';
                    $filename = str_random(6) . '_' . date('d') . date('m') . date('y') . '_' . Input::file('ProfilePicture')->getClientOriginalName();
                    Input::file('ProfilePicture')->move($destination, $filename);
                    $height = Image::make(sprintf($destination . '%s', $filename))->height();
                    $width = Image::make(sprintf($destination . '%s', $filename))->width();
                    $heightResize = null;
                    $widthResize = null;
                    $countResize = 0;
                    if ($height > 180) {
                        $heightResize = 180;
                        $countResize = 1;
                    }
                    if ($width > 160) {
                        $widthResize = 160;
                        $countResize = 1;
                    }
                    if ($countResize == 1) {
                        $image = Image::make(sprintf($destination . '%s', $filename))->resize($widthResize, $heightResize)->save();
                    }
                    $path = '/uploaded_img/' . $filename;
                } else {
                    return View::make('setting.profile')
                                    ->withMessages('gagalUpload')
                                    ->withToogle('setting')->withAktif('profile');
                }
            }
            //valid
            $user = User::find(Auth::user()->InternalID);
            if ($user->CompanyInternalID == Auth::user()->Company->InternalID || Auth::user()->Company->InternalID == '-1') {
                $user->UserName = Input::get('Name');
                if (Auth::user()->Company->InternalID == '-1') {
                    $user->CompanyInternalID = Input::get('company');
                } else {
                    $user->CompanyInternalID = Auth::user()->Company->InternalID;
                }
                $user->Phone = Input::get('Phone');
                $user->Email = Input::get('Email');
                $user->UserModified = Auth::user()->UserID;
                if ($path != NULL) {
                    if ($user->Picture != NULL) {
                        $destination = public_path();
                        File::delete(sprintf($destination . '%s', $user->Picture));
                    }
                    $user->Picture = $path;
                }
                $user->Remark = Input::get('remark');
                $user->save();
                $messages = 'suksesUpdate';
                Session::flash('messages', $messages);
                return Redirect::Route('settProfile');
            } else {
                return View::make('setting.profile')
                                ->withMessages('accessDenied')
                                ->withToogle('setting')->withAktif('profile');
            }
        }
    }

    public function updatePassword() {
        //rule
        $rule = array(
            'Password' => 'required|max:16'
        );
        //validasi
        $validator = Validator::make(Input::all(), $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('setting.profile')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('setting')->withAktif('profile');
        } else {
            if (!Hash::check(Input::get('PasswordLama'), Auth::user()->UserPwd)) {
                return View::make('setting.profile')
                                ->withMessages('gagalPassword')
                                ->withError($validator->messages())
                                ->withToogle('setting')->withAktif('profile');
            }
            $path = NULL;
            //valid
            $user = User::find(Auth::user()->InternalID);
            if ($user->CompanyInternalID == Auth::user()->Company->InternalID || Auth::user()->Company->InternalID == '-1') {
                $user->UserPwd = Hash::make(Input::get('Password'));
                $user->save();
                $messages = 'suksesUpdatePassword';
                Session::flash('messages', $messages);
                return Redirect::Route('settProfile');
            } else {
                return View::make('setting.profile')
                                ->withMessages('accessDenied')
                                ->withToogle('setting')->withAktif('profile');
            }
        }
    }

    public function exportExcel() {
        Excel::create('Master_User', function($excel) {
            $excel->sheet('Master_User', function($sheet) {
                $sheet->mergeCells('B1:J1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Master User");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "User Name");
                $sheet->setCellValueByColumnAndRow(3, 2, "User ID");
                $sheet->setCellValueByColumnAndRow(4, 2, "Email");
                $sheet->setCellValueByColumnAndRow(5, 2, "Phone");
                $sheet->setCellValueByColumnAndRow(6, 2, "Company");
                $sheet->setCellValueByColumnAndRow(7, 2, "Record");
                $sheet->setCellValueByColumnAndRow(8, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(9, 2, "Remark");
                $row = 3;
                $userData = User::where('CompanyInternalID', Auth::user()->Company->InternalID)->get();
                if (Auth::user()->Company->InternalID == '-1') {
                    $userData = User::all();
                }
                foreach ($userData as $data) {
                    $company = User::find($data->InternalID)->Company;
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, $data->UserName);
                    $sheet->setCellValueByColumnAndRow(3, $row, "`" . $data->UserID);
                    $sheet->setCellValueByColumnAndRow(4, $row, $data->Email);
                    $sheet->setCellValueByColumnAndRow(5, $row, '`' . $data->Phone);
                    $sheet->setCellValueByColumnAndRow(6, $row, $company->CompanyName);
                    $sheet->setCellValueByColumnAndRow(7, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(8, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(9, $row, $data->Remark);
                    $row++;
                }

                if (User::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() <= 0) {
                    $sheet->mergeCells('B3:J3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "No data available in table");

                    $sheet->cells('B3:J3', function($cells) {
                        $cells->setAlignment('center');
                    });

                    $sheet->setBorder('B3:J' . $row, 'thin');
                }

                $row--;
                $sheet->setBorder('B2:J' . $row, 'thin');
                $sheet->cells('B2:J2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:J' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
            });
        })->export('xls');
    }

    public function commissionReport() {
        $user = User::find(Input::get('user'));
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $DetailData = SalesAddDetail::join('t_sales_header', 't_sales_header.InternalID', '=', 't_sales_detail.SalesInternalID')
                        ->where('t_sales_header.UserRecord', $user->UserID)
                        ->whereBetween("t_sales_header.SalesDate", Array($start, $end))
                        ->orderBy('SalesDate')->get();
        $DetailData2 = SalesAddParcel::join('t_sales_header', 't_sales_header.InternalID', '=', 't_sales_parcel.SalesInternalID')
                        ->where('t_sales_header.UserRecord', $user->UserID)
                        ->whereBetween("t_sales_header.SalesDate", Array($start, $end))
                        ->orderBy('SalesDate')->get();
        $total = 0;
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Commission Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">User  ' . $user->UserID . ' ' . $user->UserName . '</span><br>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span><br><br>';
        if (count($DetailData) <= 0 && count($DetailData2) <= 0) {
            $html .= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;"> There is no commission activity in this table</span>';
        } else {
            $html .= '<table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th width=15% style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Transaction</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Type</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">From</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Subtotal</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Commission (%)</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Commission Value</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            foreach ($DetailData as $data) {
                if ($data->Commission != 0) {
                    $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->SalesDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->SalesID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">Inventory</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . Inventory::find($data->InventoryInternalID)->InventoryName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format(($data->SubTotal), '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format(($data->Commission), '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format(($data->SubTotal * $data->Commission / 100), '2', '.', ',') . '</td>
                            </tr>';
                    $total += $data->SubTotal * $data->Commission / 100;
                }
            }
            foreach ($DetailData2 as $data) {
                if ($data->Commission != 0) {
                    $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->SalesDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->SalesID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">Parcel</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . Parcel::find($data->ParcelInternalID)->ParcelName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format(($data->SubTotal), '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format(($data->Commission), '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format(($data->SubTotal * $data->Commission / 100), '2', '.', ',') . '</td>
                            </tr>';
                    $total += $data->SubTotal * $data->Commission / 100;
                }
            }
            $html .= '
            <tr>
                <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: left"><hr/></td>
            </tr>
            <tr>
                <td colspan="4" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700;"></td>
                <td colspan = "2" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: left"> Total Commission Value</td>
                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right">' . number_format($total, '2', '.', '.') . '</td>
            </tr>
                            ';
            $html .= '</tbody>
            </table>';
        }
        $html .= '
                    </div>
                </body>
            </html>';
        return PDF::load($html, 'A4', 'potrait')->download('commission_report');
    }

}
