<?php

class WarehouseController extends BaseController {

    public function showWarehouse() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertWarehouse') {
                return $this->insertWarehouse();
            }
            if (Input::get('jenis') == 'updateWarehouse') {
                return $this->updateWarehouse();
            }
            if (Input::get('jenis') == 'deleteWarehouse') {
                return $this->deleteWarehouse();
            }
        }
        return View::make('master.warehouse')
                        ->withToogle('master')->withAktif('warehouse');
    }

    public static function insertWarehouse() {
        //rule
        $rule = array(
            'WarehouseID' => 'required|max:200|unique:m_warehouse,WarehouseID,NULL,WarehouseID,CompanyInternalID,' . Auth::user()->Company->InternalID . '',
            'WarehouseName' => 'required|max:200',
            'remark' => 'required|max:1000'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('master.warehouse')
                            ->withMessages('gagalInsert')
                            ->withError($validator->messages())
                            ->withToogle('master')->withAktif('warehouse')
                            ->withErrors($validator);
        } else {
            //valid
            $warehouse = new Warehouse;
            $warehouse->WarehouseID = Input::get('WarehouseID');
            $warehouse->WarehouseName = Input::get('WarehouseName');
            $warehouse->UserRecord = Auth::user()->UserID;
            $warehouse->CompanyInternalID = Auth::user()->Company->InternalID;
            $warehouse->UserModified = "0";
            $warehouse->Remark = Input::get('remark');
            $warehouse->save();

            return View::make('master.warehouse')
                            ->withMessages('suksesInsert')
                            ->withToogle('master')->withAktif('warehouse');
        }
    }

    static function updateWarehouse() {
        //rule
        $rule = array(
            'WarehouseName' => 'required|max:200',
            'remark' => 'required|max:1000'
        );
        //validasi
        $validator = Validator ::make(Input::all(), $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('master.warehouse')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('master')->withAktif('warehouse');
        } else {
            //valid
            $warehouse = Warehouse::find(Input::get('InternalID'));
            if ($warehouse->CompanyInternalID == Auth::user()->Company->InternalID) {
                $warehouse->WarehouseName = Input::get('WarehouseName');
                $warehouse->UserModified = Auth::user()->UserID;
                $warehouse->Remark = Input::get('remark');
                $warehouse->save();
                return View::make('master.warehouse')
                                ->withMessages('suksesUpdate')
                                ->withToogle('master')->withAktif('warehouse');
            } else {
                return View::make('master.warehouse')
                                ->withMessages('accessDenied')
                                ->withToogle('master')->withAktif('warehouse');
            }
        }
    }

    static function deleteWarehouse() {
        //cek apakah ID warehouse ada di tabel sales header atau tidak
        $sales = DB::table('t_sales_header')->where('WarehouseInternalID', Input::get('InternalID'))->first();
        //cek apakah ID warehouse ada di tabel sales return header atau tidak
        $salesReturn = DB::table('t_salesreturn_header')->where('WarehouseInternalID', Input::get('InternalID'))->first();
        //cek apakah ID warehouse ada di tabel sales order header atau tidak
        $salesOrder = DB::table('t_salesorder_header')->where('WarehouseInternalID', Input::get('InternalID'))->first();
        //cek apakah ID warehouse ada di tabel purchase header atau tidak
        $purchase = DB::table('t_purchase_header')->where('WarehouseInternalID', Input::get('InternalID'))->first();
        //cek apakah ID warehouse ada di tabel purchase return header atau tidak
        $purchaseReturn = DB::table('t_purchasereturn_header')->where('WarehouseInternalID', Input::get('InternalID'))->first();
        //cek apakah ID warehouse ada di tabel purchase order header atau tidak
        $purchaseOrder = DB::table('t_purchaseorder_header')->where('WarehouseInternalID', Input::get('InternalID'))->first();
        //cek apakah ID warehouse ada di tabel transfer header atau tidak
        $transfer = DB::table('t_transfer_header')->where('WarehouseInternalID', Input::get('InternalID'))->first();
        //cek apakah ID warehouse ada di tabel transfer header atau tidak
        $transfer2 = DB::table('t_transfer_header')->where('WarehouseDestinyInternalID', Input::get('InternalID'))->first();
        //cek apakah ID warehouse ada di tabel memoin atau tidak
        $memo = DB::table('t_memoin_header')->where('WarehouseInternalID', Input::get('InternalID'))->first();
        //cek apakah ID warehouse ada di tabel memoout atau tidak
        $memo2 = DB::table('t_memoout_header')->where('WarehouseInternalID', Input::get('InternalID'))->first();
        //cek apakah ID warehouse ada di tabel convertion atau tidak
        $convertion = DB::table('h_convertion')->where('WarehouseInternalID', Input::get('InternalID'))->first();
        //cek apakah ID warehouse ada di tabel user atau tidak
        $user = DB::table('m_user')->where('WarehouseInternalID', Input::get('InternalID'))->first();

        if (is_null($user) && is_null($sales) && is_null($salesReturn) && is_null($salesOrder) && is_null($purchase) && is_null($purchaseReturn) && is_null($purchaseOrder) && is_null($transfer) && is_null($transfer2) && is_null($memo) && is_null($memo2) && is_null($convertion)) {
            //tidak ada maka boleh dihapus
            $warehouse = Warehouse::find(Input::get('InternalID'));
            if ($warehouse->CompanyInternalID == Auth::user()->Company->InternalID) {
                $warehouse->delete();
                return View::make('master.warehouse')
                                ->withMessages('suksesDelete')
                                ->withToogle('master')->withAktif('warehouse');
            } else {
                return View::make('master.warehouse')
                                ->withMessages('accessDenied')
                                ->withToogle('master')->withAktif('warehouse');
            }
        } else {
            //ada maka tidak dihapus
            return View::make('master.warehouse')
                            ->withMessages('gagalDelete')
                            ->withToogle('master')->withAktif('warehouse');
        }
    }

    public function exportExcel() {
        Excel::create('Master_Warehouse', function($excel) {
            $excel->sheet('Master_Warehouse', function($sheet) {
                $sheet->mergeCells('B1:G1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Master Warehouse");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Warehouse Name");
                $sheet->setCellValueByColumnAndRow(3, 2, "Warehouse ID");
                $sheet->setCellValueByColumnAndRow(4, 2, "Record");
                $sheet->setCellValueByColumnAndRow(5, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(6, 2, "Remark");
                $row = 3;
                foreach (Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, $data->WarehouseName);
                    $sheet->setCellValueByColumnAndRow(3, $row, "`" . $data->WarehouseID);
                    $sheet->setCellValueByColumnAndRow(4, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(5, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(6, $row, $data->Remark);
                    $row++;
                }

                if (Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() <= 0) {
                    $sheet->mergeCells('B3:G3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "No data available in table");

                    $sheet->cells('B3:G3', function($cells) {
                        $cells->setAlignment('center');
                    });
                    $sheet->setBorder('B3:G' . $row, 'thin');
                }

                $row--;
                $sheet->setBorder('B2:G' . $row, 'thin');
                $sheet->cells('B2:G2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:G' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
            });
        })->export('xls');
    }

}
