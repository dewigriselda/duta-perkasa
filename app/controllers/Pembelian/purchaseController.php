<?php

class PurchaseController extends BaseController {

    public function showPurchase() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deletePurchase') {
                return $this->deletePurchase();
            } else if (Input::get('jenis') == 'summaryPurchase') {
                return $this->summaryPurchase();
            } else if (Input::get('jenis') == 'detailPurchase') {
                return $this->detailPurchase();
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = PurchaseHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
            return View::make('pembelian.purchaseSearch')
                            ->withToogle('transaction')->withAktif('purchase')
                            ->withData($data);
        }
        return View::make('pembelian.purchase')
                        ->withToogle('transaction')->withAktif('purchase');
    }

    public function purchaseNew() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deletePurchase') {
                return $this->deletePurchase();
            } else if (Input::get('jenis') == 'summaryPurchase') {
                return $this->summaryPurchase();
            } else if (Input::get('jenis') == 'detailPurchase') {
                return $this->detailPurchase();
            } else {
                return $this->insertPurchase();
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = PurchaseHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
            return View::make('pembelian.purchaseSearch')
                            ->withToogle('transaction')->withAktif('purchase')
                            ->withData($data);
        }
        $purchase = $this->createID(0) . '.';
        return View::make('pembelian.purchaseNew')
                        ->withToogle('transaction')->withAktif('purchase')
                        ->withPurchase($purchase);
    }

    public function purchaseDetail($id) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'summaryPurchase') {
                return $this->summaryPurchase();
            } else if (Input::get('jenis') == 'detailPurchase') {
                return $this->detailPurchase();
            }
        }
        $id = PurchaseHeader::getIdpurchase($id);
        $header = PurchaseHeader::find($id);
        $detail = PurchaseHeader::find($id)->purchaseDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            return View::make('pembelian.purchaseDetail')
                            ->withToogle('transaction')->withAktif('purchase')
                            ->withHeader($header)
                            ->withDetail($detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showPurchase');
        }
    }

    public function purchaseUpdate($id) {
        $id = PurchaseHeader::getIdpurchase($id);
        $header = PurchaseHeader::find($id);
        $detail = PurchaseHeader::find($id)->purchaseDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID && PurchaseHeader::isReturn($header->PurchaseID) == false) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                if (Input::get('jenis') == 'summaryPurchase') {
                    return $this->summaryPurchase();
                } else if (Input::get('jenis') == 'detailPurchase') {
                    return $this->detailPurchase();
                } else {
                    return $this->updatePurchase($id);
                }
            }
            $purchase = $this->createID(0) . '.';
            return View::make('pembelian.purchaseUpdate')
                            ->withToogle('transaction')->withAktif('purchase')
                            ->withHeader($header)
                            ->withDetail($detail)
                            ->withPurchase($purchase);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showPurchase');
        }
    }

    public function insertPurchase() {
        //rule
        $jenis = Input::get('isCash');
        if ($jenis == '0') {
            $rule = array(
                'date' => 'required',
                'coa6' => 'required',
                'remark' => 'required|max:1000',
                'warehouse' => 'required',
                'currency' => 'required',
                'slip' => 'required',
                'rate' => 'required',
                'inventory' => 'required'
            );
            $longTerm = 0;
        } else {
            $rule = array(
                'date' => 'required',
                'longTerm' => 'required|integer',
                'coa6' => 'required',
                'remark' => 'required|max:1000',
                'warehouse' => 'required',
                'currency' => 'required',
                'rate' => 'required',
                'inventory' => 'required'
            );
            $longTerm = Input::get('longTerm');
        }

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalInsert';
            $error = $validator->messages();
        } else {
            //valid
            //insert header
            $header = new PurchaseHeader;
            $purchase = $this->createID(1) . '.';
            $date = explode('-', Input::get('date'));
            $yearDigit = substr($date[2], 2);
            $purchase .= $date[1] . $yearDigit . '.';
            $purchaseNumber = PurchaseHeader::getNextIDPurchase($purchase);
            $header->PurchaseID = $purchaseNumber;
            $header->PurchaseDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $header->ACC6InternalID = Input::get('coa6');
            $header->LongTerm = $longTerm;
            $header->isCash = $jenis;
            $header->WarehouseInternalID = Input::get('warehouse');
            $currency = explode('---;---', Input::get('currency'));
            $header->CurrencyInternalID = $currency[0];
            $header->CurrencyRate = str_replace(',', '', Input::get('rate'));
            if (Input::get('vat') == '') {
                $header->VAT = '0';
            } else {
                $header->VAT = Input::get('vat');
            }
            $header->DiscountGlobal = str_replace(",", "", Input::get('DiscountGlobal'));
            $header->GrandTotal = Input::get('grandTotalValue');
            $header->DownPayment = str_replace(",", "", Input::get('DownPayment'));
            $header->TransactionType = Input::get('TransactionType');
            if (Input::get('Replacement') == '') {
                $header->Replacement = '0';
            } else {
                $header->Replacement = Input::get('Replacement');
            }
            if (Input::get('TaxNumber') == '') {
                $header->TaxNumber = '.-.';
            } else {
                $header->TaxNumber = Input::get('TaxNumber');
            }
            $header->TaxMonth = Input::get('TaxMonth');
            $header->TaxYear = Input::get('TaxYear');
            $header->TaxDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $header->UserRecord = Auth::user()->UserID;
            $header->CompanyInternalID = Auth::user()->Company->InternalID;
            $header->UserModified = '0';
            $header->Remark = Input::get('remark');
            $header->save();

            //insert detail
            $total = 0;
            for ($a = 0; $a < count(Input::get('inventory')); $a++) {
                $qtyValue = str_replace(',', '', Input::get('qty')[$a]);
                $priceValue = str_replace(',', '', Input::get('price')[$a]);
                $discValue = str_replace(',', '', Input::get('discountNominal')[$a]);
                $subTotal = ($priceValue * $qtyValue) - (($priceValue * $qtyValue) * Input::get('discount')[$a] / 100) - $discValue * $qtyValue;
                $total += $subTotal;
                if (Input::get('vat') == '1') {
                    $vatValue = $subTotal / 10;
                } else {
                    $vatValue = 0;
                }
                if ($qtyValue > 0) {
                    $detail = new PurchaseDetail();
                    $detail->PurchaseInternalID = $header->InternalID;
                    $detail->InventoryInternalID = Input::get('inventory')[$a];
                    $detail->Qty = $qtyValue;
                    $detail->Price = $priceValue;
                    $detail->Discount = Input::get('discount')[$a];
                    $detail->Discount1 = Input::get('discount1')[$a];
                    $detail->DiscountNominal = $discValue;
                    $detail->VAT = $vatValue;
                    $detail->SubTotal = $subTotal;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = '0';
                    $detail->save();
                }
            }

            $currency = $currency[0];
            $rate = str_replace(',', '', Input::get('rate'));
            $total -= str_replace(",", "", Input::get('DiscountGlobal'));
            $total -= str_replace(",", "", Input::get('DownPayment'));
            $dataType = PurchaseDetail::getTipeInventoryData($header->InternalID);
            if ($jenis == 0) {
                $slip = Input::get('slip');
                $this->insertJournal($purchaseNumber, $total, Input::get('vat'), $currency, $rate, $date, $slip, $dataType, str_replace(",", "", Input::get('DiscountGlobal')), str_replace(",", "", Input::get('DownPayment')));
            } else {
                $slip = '-1';
                $this->insertJournal($purchaseNumber, $total, Input::get('vat'), $currency, $rate, $date, $slip, $dataType, str_replace(",", "", Input::get('DiscountGlobal')), str_replace(",", "", Input::get('DownPayment')));
            }

            $messages = 'suksesInsert';
            $error = '';
        }
        $purchase = $this->createID(0) . '.';
        return View::make('pembelian.purchaseNew')
                        ->withToogle('transaction')->withAktif('purchase')
                        ->withPurchase($purchase)
                        ->withError($error)
                        ->withMessages($messages);
    }

    public function updatePurchase($id) {
        //tipe
        $headerUpdate = PurchaseHeader::find($id);
        $detailUpdate = PurchaseHeader::find($id)->purchaseDetail()->get();

        //rule
        if (Input::get('isCash') == '0') {
            $rule = array(
                'coa6' => 'required',
                'remark' => 'required|max:1000',
                'warehouse' => 'required',
                'currency' => 'required',
                'slip' => 'required',
                'rate' => 'required',
                'inventory' => 'required'
            );
            $longTerm = 0;
        } else {
            $rule = array(
                'longTerm' => 'required|integer',
                'coa6' => 'required',
                'remark' => 'required|max:1000',
                'warehouse' => 'required',
                'currency' => 'required',
                'rate' => 'required',
                'inventory' => 'required'
            );
            $longTerm = Input::get('longTerm');
        }

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalUpdate';
            $error = $validator->messages();
        } else {
            $header = PurchaseHeader::find(Input::get('PurchaseInternalID'));
            $header->ACC6InternalID = Input::get('coa6');
            $header->isCash = Input::get('isCash');
            $header->LongTerm = $longTerm;
            $header->WarehouseInternalID = Input::get('warehouse');
            $currency = explode('---;---', Input::get('currency'));
            $header->CurrencyInternalID = $currency[0];
            $header->CurrencyRate = str_replace(',', '', Input::get('rate'));
            if (Input::get('vat') == '') {
                $header->VAT = '0';
            } else {
                $header->VAT = Input::get('vat');
            }
            $header->DiscountGlobal = str_replace(",", "", Input::get('DiscountGlobal'));
            $header->GrandTotal = Input::get('grandTotalValue');
            $header->DownPayment = str_replace(",", "", Input::get('DownPayment'));
            $header->TransactionType = Input::get('TransactionType');
            if (Input::get('Replacement') == '') {
                $header->Replacement = '0';
            } else {
                $header->Replacement = Input::get('Replacement');
            }
            if (Input::get('TaxNumber') == '') {
                $header->TaxNumber = '.-.';
            } else {
                $header->TaxNumber = Input::get('TaxNumber');
            }
            $header->TaxMonth = Input::get('TaxMonth');
            $header->TaxYear = Input::get('TaxYear');
            $header->UserModified = Auth::user()->UserID;
            $header->Remark = Input::get('remark');
            $header->save();

            //delete purchase detail -- nantinya insert ulang
            PurchaseDetail::where('PurchaseInternalID', '=', Input::get('PurchaseInternalID'))->delete();

            //insert detail
            $total = 0;
            for ($a = 0; $a < count(Input::get('inventory')); $a++) {
                $qtyValue = str_replace(',', '', Input::get('qty')[$a]);
                $priceValue = str_replace(',', '', Input::get('price')[$a]);
                $discValue = str_replace(',', '', Input::get('discountNominal')[$a]);
                $subTotal = ($priceValue * $qtyValue) - (($priceValue * $qtyValue) * Input::get('discount')[$a] / 100) - $discValue * $qtyValue;
                $total += $subTotal;
                if (Input::get('vat') == '1') {
                    $vatValue = $subTotal / 10;
                } else {
                    $vatValue = 0;
                }
                if ($qtyValue > 0) {
                    $detail = new PurchaseDetail();
                    $detail->PurchaseInternalID = Input::get('PurchaseInternalID');
                    $detail->InventoryInternalID = Input::get('inventory')[$a];
                    $detail->Qty = $qtyValue;
                    $detail->Price = $priceValue;
                    $detail->Discount = Input::get('discount')[$a];
                    $detail->DiscountNominal = $discValue;
                    $detail->VAT = $vatValue;
                    $detail->SubTotal = $subTotal;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = Auth::user()->UserID;
                    $detail->save();
                }
            }

            $journal = JournalHeader::where('TransactionID', '=', $headerUpdate->PurchaseID)->get();
            foreach ($journal as $value) {
                JournalDetail::where('JournalInternalID', '=', $value->InternalID)->delete();
                JournalHeader::where('InternalID', '=', $value->InternalID)->delete();
            }

            $currency = $currency[0];
            $total -= str_replace(",", "", Input::get('DiscountGlobal'));
            $total -= str_replace(",", "", Input::get('DownPayment'));
            $rate = str_replace(',', '', Input::get('rate'));
            $date = date("d-m-Y", strtotime($headerUpdate->PurchaseDate));
            $date = explode('-', $date);
            $dataType = PurchaseDetail::getTipeInventoryData($header->InternalID);
            if (Input::get('isCash') == 0) {
                $slip = Input::get('slip');
                $this->insertJournal($headerUpdate->PurchaseID, $total, Input::get('vat'), $currency, $rate, $date, $slip, $dataType, str_replace(",", "", Input::get('DiscountGlobal')), str_replace(",", "", Input::get('DownPayment')));
            } else {
                $slip = '-1';
                $this->insertJournal($headerUpdate->PurchaseID, $total, Input::get('vat'), $currency, $rate, $date, $slip, $dataType, str_replace(",", "", Input::get('DiscountGlobal')), str_replace(",", "", Input::get('DownPayment')));
            }

            $messages = 'suksesUpdate';
            $error = '';
        }

        //tipe
        $header = PurchaseHeader::find($id);
        $detail = PurchaseHeader::find($id)->purchaseDetail()->get();
        $purchase = $this->createID(0) . '.';
        return View::make('pembelian.purchaseUpdate')
                        ->withToogle('transaction')->withAktif('purchase')
                        ->withHeader($header)
                        ->withDetail($detail)
                        ->withPurchase($purchase)
                        ->withError($error)
                        ->withMessages($messages);
    }

    public function deletePurchase() {
        $purchaseHeader = PurchaseAddHeader::find(Input::get('InternalID'));
        $purchase = DB::select(DB::raw('SELECT * FROM t_purchasereturn_header WHERE CompanyInternalID = "' . Auth::user()->CompanyInternalID . '" AND PurchaseReturnID LIKE "%' . $purchaseHeader->PurchaseID . '"'));
        if (is_null($purchase) == '') {
            //tidak ada yang menggunakan data purchase maka data boleh dihapus
            ////hapus journal
            $purchaseHeader = PurchaseHeader::find(Input::get('InternalID'));
            if ($purchaseHeader->CompanyInternalID == Auth::user()->Company->InternalID) {
                $journal = JournalHeader::where('TransactionID', '=', $purchaseHeader->PurchaseID)->get();
                foreach ($journal as $value) {
                    JournalDetail::where('JournalInternalID', '=', $value->InternalID)->delete();
                    JournalHeader::where('InternalID', '=', $value->InternalID)->delete();
                }
                //hapus detil
                $detilData = PurchaseHeader::find(Input::get('InternalID'))->purchaseDetail;
                foreach ($detilData as $value) {
                    $detil = purchaseDetail::find($value->InternalID);
                    $detil->delete();
                }
                //hapus purchase
                $purchase = PurchaseHeader::find(Input::get('InternalID'));
                $purchase->delete();
                $messages = 'suksesDelete';
            } else {
                $messages = 'accessDenied';
            }
        } else {
            //ada maka data tidak boleh dihapus
            $messages = 'gagalDelete';
        }
        $data = PurchaseHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
        return View::make('pembelian.purchaseSearch')
                        ->withToogle('transaction')->withAktif('purchase')
                        ->withMessages($messages)
                        ->withData($data);
    }

    function insertJournal($purchaseNumber, $total, $vat, $currency, $rate, $date, $slip, $dataType, $discountGlobal, $dp) {
        $header = new JournalHeader;
        $yearDigit = substr($date[2], 2);
        $dateText = $date[1] . $yearDigit;
//        $defaultPembelian = Default_s::find(4)->DefaultID;
        $defaultHutang = Default_s::find(5)->DefaultID;
        $defaultOutcome = Default_s::find(6)->DefaultID;
        $defaultDiscount = Default_s::find(10)->DefaultID;
        $defaultDP = Default_s::find(12)->DefaultID;
        if ($slip == '-1') {
            $cari = 'ME-' . $dateText;
            $header->JournalType = 'Memorial';
            $akun = array($defaultHutang, $defaultOutcome, $defaultDiscount, $defaultDP);
            $header->JournalID = JournalHeader::getNextIDJournal($cari . '-');
            $header->SlipInternalID = Null;
        } else {
            $akun = array("Slip", $defaultOutcome, $defaultDiscount, $defaultDP);
            $tampSlipID = Slip::find($slip);
            if ($tampSlipID->Flag == '0') {
                $cari = 'CO-' . $dateText;
                $header->JournalType = 'Cash Out';
            } else if ($tampSlipID->Flag == '1') {
                $cari = 'BO-' . $dateText;
                $header->JournalType = 'Bank Out';
            }
            $header->JournalID = JournalHeader::getNextIDJournal($cari . '-' . $tampSlipID->SlipID . '-');
            $header->SlipInternalID = $slip;
        }
        $header->JournalDate = $date[2] . '-' . $date[1] . '-' . $date[0];
        $header->JournalFrom = Auth::user()->UserID;
        $header->Notes = '';
        $department = Department::where('Default', '1')->first();
        $header->DepartmentInternalID = $department->InternalID;
        $header->TransactionID = $purchaseNumber;
        $header->ACC5InternalID = '0';
        $header->Lock = '0';
        $header->Check = '0';
        $header->Flag = '0';
        $header->UserRecord = Auth::user()->UserID;
        $header->CompanyInternalID = Auth::user()->Company->InternalID;
        $header->UserModified = '0';
        $header->Remark = '';
        $header->save();

        //insert detail
        if ($vat == '') {
            $vatValue = 0;
        } else {
            $vatValue = 10 * $total / 100;
        }
        $count = 1;
        foreach ($dataType as $data2) {
            $tipe = InventoryType::find($data2->InternalID);
            $detail = new JournalDetail();
            $detail->JournalInternalID = $header->InternalID;
            $detail->JournalIndex = $count;
            $detail->JournalNotes = $tipe->InventoryTypeName;
            $detail->JournalDebetMU = $data2->total;
            $detail->JournalCreditMU = 0;
            $detail->CurrencyInternalID = $currency;
            $detail->CurrencyRate = $rate;
            $detail->JournalDebet = $data2->total * $rate;
            $detail->JournalCredit = 0;
            $detail->JournalTransactionID = NULL;
            $detail->ACC1InternalID = $tipe->ACC1InternalID;
            $detail->ACC2InternalID = $tipe->ACC2InternalID;
            $detail->ACC3InternalID = $tipe->ACC3InternalID;
            $detail->ACC4InternalID = $tipe->ACC4InternalID;
            $detail->ACC5InternalID = $tipe->ACC5InternalID;
            $detail->ACC6InternalID = $tipe->ACC6InternalID;
            $coa = Coa::getInternalID($tipe->ACC1InternalID, $tipe->ACC2InternalID, $tipe->ACC3InternalID, $tipe->ACC4InternalID, $tipe->ACC5InternalID, $tipe->ACC6InternalID);
            $detail->COAName = Coa::find($coa)->COAName;
            $detail->UserRecord = Auth::user()->UserID;
            $detail->UserModified = '0';
            $detail->save();
            $count++;
        }

        foreach ($akun as $data) {
            $kreditValue = 0;
            $debetValue = 0;
            if (($data != $defaultOutcome || $vatValue != 0) && ($data != $defaultDiscount || $discountGlobal != 0) && ($data != $defaultDP || $dp != 0)) {
                $detail = new JournalDetail();
                $detail->JournalInternalID = $header->InternalID;
                $detail->JournalIndex = $count;
                $detail->JournalNotes = $data;
                if ($data == $defaultHutang || $data == 'Slip') {
                    $kreditValue = $total + $vatValue;
                    $detail->JournalDebetMU = 0;
                    $detail->JournalCreditMU = $kreditValue;
                } else if ($data == $defaultDiscount) {
                    $kreditValue = $discountGlobal;
                    $detail->JournalDebetMU = 0;
                    $detail->JournalCreditMU = $kreditValue;
                } else if ($data == $defaultDP) {
                    $kreditValue = $dp;
                    $detail->JournalDebetMU = 0;
                    $detail->JournalCreditMU = $kreditValue;
                } else {
                    $debetValue = $vatValue;
                    $detail->JournalDebetMU = $debetValue;
                    $detail->JournalCreditMU = 0;
                }
                $detail->CurrencyInternalID = $currency;
                $detail->CurrencyRate = $rate;
                $detail->JournalDebet = $debetValue * $rate;
                $detail->JournalCredit = $kreditValue * $rate;
                $detail->JournalTransactionID = NULL;
                if ($data != 'Slip') {
                    $default = Default_s::getInternalCoa($data);
                } else {
                    $default = Slip::getInternalCoa($slip);
                }
                $detail->ACC1InternalID = $default->ACC1InternalID;
                $detail->ACC2InternalID = $default->ACC2InternalID;
                $detail->ACC3InternalID = $default->ACC3InternalID;
                $detail->ACC4InternalID = $default->ACC4InternalID;
                $detail->ACC5InternalID = $default->ACC5InternalID;
                $detail->ACC6InternalID = $default->ACC6InternalID;
                $coa = Coa::getInternalID($default->ACC1InternalID, $default->ACC2InternalID, $default->ACC3InternalID, $default->ACC4InternalID, $default->ACC5InternalID, $default->ACC6InternalID);
                $detail->COAName = Coa::find($coa)->COAName;
                $detail->UserRecord = Auth::user()->UserID;
                $detail->UserModified = '0';
                $detail->save();
                $count++;
            }
        }
    }

    function purchasePrint($id) {
        $id = PurchaseHeader::getIdpurchase($id);
        $header = PurchaseHeader::find($id);
        $detail = PurchaseHeader::find($id)->purchaseDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $coa6 = PurchaseHeader::find($header->InternalID)->coa6;
            $supplier = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . '<br> ' . $coa6->Address . '<br>Phone: ' . $coa6->Phone . '<br> Fax: ' . $coa6->Fax;
            if ($header->isCash == 0) {
                $payment = 'Cash';
            } else {
                $payment = 'Credit';
            }
            $currency = Currency::find($header->CurrencyInternalID);
            $currencyName = $currency->CurrencyName;
            if ($header->VAT == 0) {
                $vat = 'Non Tax';
            } else {
                $vat = 'Tax';
            }
            $html = '
        <html>
            <head>
                <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 6px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box; position: relative;">
                    <div style="height: 195px;border-bottom:1px solid #ddd;margin-bottom:20px;">
                        <div style="height: 133px">
                        <img height="100%" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                        </div>
                        <div style="width: 420px; float: left;">
                            <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->Company->CompanyName . '</b>
                        </div> 
                        <div style=" box-sizing: border-box; margin-top: 0px;position: absolute; right: 2px; float: right;">
                             <table>
                             <tr style="background: none;">
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Address</td>
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                             </tr>
                            <tr style="background: none;">
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                             </tr>
                            <tr style="background: none;">
                                <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Phone Number</td>
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                             </tr>
                             </table>
                        </div>           
                    </div>
                    <h5 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Purchase</h5>
                    <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                        <table>
                        <tr>
                        <td width="275px">
                            <table>
                             <tr style="background: none;">
                                <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Purchase ID</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->PurchaseID . '</td>
                             </tr>
                            <tr style="background: none;">
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Date</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->PurchaseDate)) . '</td>
                             </tr>
                            <tr style="background: none;">
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Supplier</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $supplier . '</td>
                             </tr>
                            <tr style="background: none;">
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Payment</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $payment . '</td>
                             </tr>';
            if ($header->isCash != 0) {
                $html.='
                            <tr style="background: none;">
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Due Date</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime("+" . $header->LongTerm . " day", strtotime($header->PurchaseDate))) . '</td>
                             </tr>';
            }
            $html.='
                            </table>
                            </td>
                            <td>
                            <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Warehouse</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Warehouse->WarehouseName . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Currency</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $currencyName . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Rate</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . number_format($header->CurrencyRate, '2', '.', ',') . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Vat</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $vat . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Remark</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Remark . '</td>
                                 </tr>
                            </table></td>
                            </tr>
                        </table>
                    </div>    
                        <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
                                <thead >
                                    <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="25%">Inventory</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Qty</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Price</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc (%)</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>';

            $total = 0;
            $totalVAT = 0;
            if (count($detail) > 0) {
                foreach ($detail as $data) {
                    $inventory = Inventory::find($data->InventoryInternalID);
                    $inv = $inventory->InventoryID . ' ' . $inventory->InventoryName;
                    $html.= '<tr>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $inv . '</td>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                        </tr>';
                    $totalVAT += $data->VAT;
                    $total += $data->SubTotal;
                }
                if ($totalVAT != 0) {
                    $totalVAT = $totalVAT - ($header->DiscountGlobal * 0.1);
                }
            } else {
                $html.= '<tr>
                        <td colspan="6" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this purchase.</td>
                    </tr>';
            }
            $html.= '</tbody>
                        </table>
                        <div style="box-sizing: border-box;min-width: 200px; margin-left: 320px; display: inline-block; clear: both;">
                             <table>
                             <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Discount</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Grand Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total - $header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Tax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($totalVAT, '2', '.', ',') . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; ">Grand Total (Tax)</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">'
                    . number_format($header->GrandTotal, '2', '.', ',') . '</td>
                                 </tr>
                            </table>
                        </div>
                </div>
            </body>
        </html>';
            return PDF::load($html, 'A5', 'potrait')->show();
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showPurchase');
        }
    }

    function purchasePrintSJ($id) {
        $id = PurchaseHeader::getIdpurchase($id);
        $header = PurchaseHeader::find($id);
        $detail = PurchaseHeader::find($id)->purchaseDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $coa6 = PurchaseHeader::find($header->InternalID)->coa6;
            $supplier = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . ', ' . $coa6->Address . '<br>Phone: ' . $coa6->Phone . ', Fax: ' . $coa6->Fax;
            if ($header->isCash == 0) {
                $payment = 'Cash';
            } else {
                $payment = 'Credit';
            }
            $currency = Currency::find($header->CurrencyInternalID);
            $currencyName = $currency->CurrencyName;
            if ($header->VAT == 0) {
                $vat = 'Non Tax';
            } else {
                $vat = 'Tax';
            }
            $html = '
        <html>
            <head>
                <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 6px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box; position: relative;">
                    <div style="height: 195px;border-bottom:1px solid #ddd;margin-bottom:20px;">
                        <div style="height: 133px">
                        <img height="100%" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                        </div>
                        <div style="width: 420px; float: left;">
                            <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->Company->CompanyName . '</b>
                        </div> 
                        <div style=" box-sizing: border-box; margin-top: 0px;position: absolute; right: 2px; float: right;">
                             <table>
                             <tr style="background: none;">
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Address</td>
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                             </tr>
                            <tr style="background: none;">
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                             </tr>
                            <tr style="background: none;">
                                <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Phone Number</td>
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                             </tr>
                             </table>
                        </div>           
                    </div>
                    <h5 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Purchase</h5>
                    <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                        <table>
                        <tr>
                        <td width="275px">
                            <table>
                             <tr style="background: none;">
                                <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Purchase ID</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->PurchaseID . '</td>
                             </tr>
                            <tr style="background: none;">
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Date</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->PurchaseDate)) . '</td>
                             </tr>
                            <tr style="background: none;">
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Supplier</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $supplier . '</td>
                             </tr>
                            </table>
                            </td>
                            <td>
                            <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Warehouse</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Warehouse->WarehouseName . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Remark</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Remark . '</td>
                                 </tr>
                            </table></td>
                            </tr>
                        </table>
                    </div>    
                        <table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                <thead >
                                    <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="65%">Inventory</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="25%">Qty</th>
                                    </tr>
                                </thead>
                                <tbody>';

            $total = 0;
            $totalVAT = 0;
            if (count($detail) > 0) {
                foreach ($detail as $data) {
                    $inventory = Inventory::find($data->InventoryInternalID);
                    $inv = $inventory->InventoryID . ' ' . $inventory->InventoryName;
                    $html.= '<tr>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $inv . '</td>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                        </tr>';
                    $totalVAT += $data->VAT;
                    $total += $data->SubTotal;
                }
                if ($totalVAT != 0) {
                    $totalVAT = $totalVAT - ($header->DiscountGlobal * 0.1);
                }
            } else {
                $html.= '<tr>
                        <td colspan="2" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this purchase.</td>
                    </tr>';
            }
            $html.= '</tbody>
                        </table>
                </div>
            </body>
        </html>';
            return PDF::load($html, 'A5', 'potrait')->show();
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showPurchase');
        }
    }

    function createID($tipe) {
        $purchase = 'PI';
        if ($tipe == 0) {
            $purchase .= '.' . date('m') . date('y');
        }
        return $purchase;
    }

    public function formatCariIDPurchase() {
        $date = explode('-', Input::get('date'));
        $id = $this->createID(1);
        $yearDigit = substr($date[2], 2);
        $id .= '.' . $date[1] . $yearDigit . '.';
        echo PurchaseHeader::getNextIDPurchase($id);
    }

    function purchaseCSV($id) {
        $id = PurchaseHeader::getIdpurchase($id);
        $header = PurchaseHeader::find($id);
        Session::flash('idCSV', $id);
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            Excel::create('Tax_Report_purchase', function($excel) {
                $excel->sheet('Tax_Report_purchase', function($sheet) {
                    $id = Session::get('idCSV');
                    $header = PurchaseHeader::find($id);
                    $detail = PurchaseHeader::find($id)->purchaseDetail()->get();
                    //baris pertama
                    $sheet->setCellValueByColumnAndRow(0, 1, "FK");
                    $sheet->setCellValueByColumnAndRow(1, 1, "KD_JENIS_TRANSAKSI");
                    $sheet->setCellValueByColumnAndRow(2, 1, "FG_PENGGANTI");
                    $sheet->setCellValueByColumnAndRow(3, 1, "NOMOR_FAKTUR");
                    $sheet->setCellValueByColumnAndRow(4, 1, "MASA_PAJAK");
                    $sheet->setCellValueByColumnAndRow(5, 1, "TAHUN_PAJAK");
                    $sheet->setCellValueByColumnAndRow(6, 1, "TANGGAL_FAKTUR");
                    $sheet->setCellValueByColumnAndRow(7, 1, "NPWP");
                    $sheet->setCellValueByColumnAndRow(8, 1, "NAMA");
                    $sheet->setCellValueByColumnAndRow(9, 1, "ALAMAT_LENGKAP");
                    $sheet->setCellValueByColumnAndRow(10, 1, "JUMLAH_DPP");
                    $sheet->setCellValueByColumnAndRow(11, 1, "JUMLAH_PPN");
                    $sheet->setCellValueByColumnAndRow(12, 1, "JUMLAH_PPNBM");
                    $sheet->setCellValueByColumnAndRow(13, 1, "ID_KETERANGAN_TAMBAHAN_Number");
                    $sheet->setCellValueByColumnAndRow(14, 1, "FG_UANG_MUKA");
                    $sheet->setCellValueByColumnAndRow(15, 1, "UANG_MUKA_DPP");
                    $sheet->setCellValueByColumnAndRow(16, 1, "UANG_MUKA_PPN");
                    $sheet->setCellValueByColumnAndRow(17, 1, "UANG_MUKA_PPNBM");
                    $sheet->setCellValueByColumnAndRow(18, 1, "REFERENSI");
                    //baris kedua
                    $sheet->setCellValueByColumnAndRow(0, 2, "LT");
                    $sheet->setCellValueByColumnAndRow(1, 2, "NPWP");
                    $sheet->setCellValueByColumnAndRow(2, 2, "NAMA");
                    $sheet->setCellValueByColumnAndRow(3, 2, "JALAN");
                    $sheet->setCellValueByColumnAndRow(4, 2, "BLOK");
                    $sheet->setCellValueByColumnAndRow(5, 2, "NOMOR");
                    $sheet->setCellValueByColumnAndRow(6, 2, "RT");
                    $sheet->setCellValueByColumnAndRow(7, 2, "RW");
                    $sheet->setCellValueByColumnAndRow(8, 2, "KECAMATAN");
                    $sheet->setCellValueByColumnAndRow(9, 2, "KELURAHAN");
                    $sheet->setCellValueByColumnAndRow(10, 2, "KABUPATEN");
                    $sheet->setCellValueByColumnAndRow(11, 2, "PROPINSI");
                    $sheet->setCellValueByColumnAndRow(12, 2, "KODE_POS");
                    $sheet->setCellValueByColumnAndRow(13, 2, "NOMOR_TELEPON");
                    //baris ketiga
                    $sheet->setCellValueByColumnAndRow(0, 3, "OF");
                    $sheet->setCellValueByColumnAndRow(1, 3, "KODE_OBJEK");
                    $sheet->setCellValueByColumnAndRow(2, 3, "NAMA");
                    $sheet->setCellValueByColumnAndRow(3, 3, "HARGA_SATUAN");
                    $sheet->setCellValueByColumnAndRow(4, 3, "JUMLAH_BARANG");
                    $sheet->setCellValueByColumnAndRow(5, 3, "HARGA_TOTAL");
                    $sheet->setCellValueByColumnAndRow(6, 3, "DISKON");
                    $sheet->setCellValueByColumnAndRow(7, 3, "DPP");
                    $sheet->setCellValueByColumnAndRow(8, 3, "PPN");
                    $sheet->setCellValueByColumnAndRow(9, 3, "TARIF_PPNBM");
                    $sheet->setCellValueByColumnAndRow(10, 3, "PPNBM");
                    //isi baris pertama
                    $sheet->setCellValueByColumnAndRow(0, 4, "FK");
                    $sheet->setCellValueByColumnAndRow(1, 4, '0' . $header->TransactionType);
                    $sheet->setCellValueByColumnAndRow(2, 4, $header->Replacement);
                    //nomorPajak
                    $nomorPajak = str_replace('.', '', $header->TaxNumber);
                    $nomorPajak = str_replace('-', '', $nomorPajak);
                    $nomorPajak = substr($nomorPajak, 4);
                    $sheet->setCellValueByColumnAndRow(3, 4, $nomorPajak);
                    $sheet->setCellValueByColumnAndRow(4, 4, $header->TaxMonth);
                    $sheet->setCellValueByColumnAndRow(5, 4, substr($header->TaxYear, '2'));
                    $sheet->setCellValueByColumnAndRow(6, 4, date('d/m/Y', strtotime($header->TaxDate)));
                    //nomorPajak
                    $pajakSupplier = str_replace('.', '', $header->Coa6->TaxID);
                    $pajakSupplier = str_replace('-', '', $pajakSupplier);
                    if ($pajakSupplier == '') {
                        $pajakSupplier = '000000000000000';
                    }
                    $sheet->setCellValueByColumnAndRow(7, 4, $pajakSupplier);
                    $sheet->setCellValueByColumnAndRow(8, 4, $header->Coa6->ACC6Name);
                    $sheet->setCellValueByColumnAndRow(9, 4, $header->Coa6->Address);
                    //total
                    $total = $header->GrandTotal * 10 / 11;
                    $sheet->setCellValueByColumnAndRow(10, 4, floor($total));
                    $sheet->setCellValueByColumnAndRow(11, 4, floor(($total * 0.1)));
                    $sheet->setCellValueByColumnAndRow(12, 4, 0);
                    $sheet->setCellValueByColumnAndRow(13, 4, '');
                    if ($header->DownPayment == 0) {
                        $dp = 0;
                    } else {
                        $dp = 1;
                    }
                    $sheet->setCellValueByColumnAndRow(14, 4, $dp);
                    $sheet->setCellValueByColumnAndRow(15, 4, $header->DownPayment);
                    $sheet->setCellValueByColumnAndRow(16, 4, $header->DownPayment * 0.1);
                    $sheet->setCellValueByColumnAndRow(17, 4, "0");
                    $sheet->setCellValueByColumnAndRow(18, 4, "");
                    //isi baris kedua
                    $sheet->setCellValueByColumnAndRow(0, 5, "LT");
                    $sheet->setCellValueByColumnAndRow(1, 5, $pajakSupplier);
                    $sheet->setCellValueByColumnAndRow(2, 5, $header->Coa6->ACC6Name);
                    $sheet->setCellValueByColumnAndRow(3, 5, $header->Coa6->Address);
                    $sheet->setCellValueByColumnAndRow(4, 5, $header->Coa6->Block);
                    $sheet->setCellValueByColumnAndRow(5, 5, $header->Coa6->AddressNumber);
                    $sheet->setCellValueByColumnAndRow(6, 5, $header->Coa6->RT);
                    $sheet->setCellValueByColumnAndRow(7, 5, $header->Coa6->RW);
                    $sheet->setCellValueByColumnAndRow(8, 5, $header->Coa6->District);
                    $sheet->setCellValueByColumnAndRow(9, 5, $header->Coa6->Subdistrict);
                    $sheet->setCellValueByColumnAndRow(10, 5, $header->Coa6->City);
                    $sheet->setCellValueByColumnAndRow(11, 5, $header->Coa6->Province);
                    $sheet->setCellValueByColumnAndRow(12, 5, $header->Coa6->PostalCode);
                    $sheet->setCellValueByColumnAndRow(13, 5, $header->Coa6->Phone);

                    $row = 6;
                    foreach ($detail as $data) {
                        $sheet->setCellValueByColumnAndRow(0, $row, "OF");
                        $sheet->setCellValueByColumnAndRow(1, $row, $data->Inventory->InventoryID);
                        $sheet->setCellValueByColumnAndRow(2, $row, $data->Inventory->InventoryName);
                        $sheet->setCellValueByColumnAndRow(3, $row, $data->Price);
                        $sheet->setCellValueByColumnAndRow(4, $row, $data->Qty);
                        $sheet->setCellValueByColumnAndRow(5, $row, $data->Qty * $data->Price);
                        $sheet->setCellValueByColumnAndRow(6, $row, $data->Qty * $data->Price - $data->SubTotal);
                        $sheet->setCellValueByColumnAndRow(7, $row, $data->SubTotal);
                        $sheet->setCellValueByColumnAndRow(8, $row, $data->VAT);
                        $sheet->setCellValueByColumnAndRow(9, $row, 0);
                        $sheet->setCellValueByColumnAndRow(10, $row, 0);
                        $row++;
                    }

                    $row--;
                    $sheet->setBorder('B2:T' . $row, 'thin');
                    $sheet->cells('B2:T2', function($cells) {
                        $cells->setBackground('#eaf6f7');
                        $cells->setValignment('middle');
                    });
                    $sheet->cells('B1', function($cells) {
                        $cells->setValignment('middle');
                        $cells->setFontWeight('bold');
                        $cells->setFontSize('16');
                    });
                    $sheet->cells('B2:T' . $row, function($cells) {
                        $cells->setAlignment('left');
                        $cells->setValignment('middle');
                    });
                });
            })->export('csv');
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showPurchase');
        }
    }

    public function summaryPurchase() {
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Purchase Summary Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span>';
        $hitung = 0;
        foreach (Coa6::where("Type", "s")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $dataSupplier) {
            if (PurchaseHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                            ->where('ACC6InternalID', $dataSupplier->InternalID)
                            ->whereBetween('PurchaseDate', Array($start, $end))->count() > 0) {
            $html.= '<table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=7>' . $dataSupplier->ACC6Name . '</th>
                                        </tr>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Purchase ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Currency</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Rate</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total (After Discount)</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Vat</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Grand Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                foreach (PurchaseHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where('ACC6InternalID', $dataSupplier->InternalID)
                        ->whereBetween('PurchaseDate', Array($start, $end))->get() as $data) {
                    $grandTotal = $data->GrandTotal;
                    $total = $grandTotal;
                    $vat = 0;
                    if ($data->VAT == 1) {
                        $total = $total * 10 / 11;
                        $vat = $total / 10;
                    }
                    $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->PurchaseID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->PurchaseDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->Currency->CurrencyName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->CurrencyRate, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($total, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($vat, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($grandTotal, '2', '.', ',') . '</td>
                            </tr>';
                }
            
            $html.= '</tbody>
            </table>';
                $hitung++;
            }
        }
        
        if ($hitung == 0) {
            $html.='<br><span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no purchase.</span>';
        }
        $html.='</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('purchase_summary');
    }

    public function detailPurchase() {
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $dateTamp = '';
        $coa6Tamp = '';
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Purchase Detail Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span><br><br>';
        if (PurchaseHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                        ->whereBetween('PurchaseDate', Array($start, $end))
                        ->orderBy('PurchaseDate')->orderBy('ACC6InternalID')->count() > 0) {
            foreach (PurchaseHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                    ->whereBetween('PurchaseDate', Array($start, $end))
                    ->orderBy('PurchaseDate')->orderBy('ACC6InternalID')->get() as $dataPembelian) {
                if ($dateTamp != date("d-M-Y", strtotime($dataPembelian->PurchaseDate))) {
                    $html.= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Purchase Date : ' . date("d-M-Y", strtotime($dataPembelian->PurchaseDate)) . '</span><br><br>';
                    $dateTamp = date("d-M-Y", strtotime($dataPembelian->PurchaseDate));
                    $coa6Tamp = '';
                }
                if ($coa6Tamp != $dataPembelian->ACC6InternalID) {
                    $html.= '<span style="padding-left:23px;font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Supplier : ' . $dataPembelian->coa6->ACC6Name . '</span>';
                    $coa6Tamp = $dataPembelian->ACC6InternalID;
                }
                $html.= '<table class="tableBorder" style="width:95%; margin-top: 18px; clear: both; position: relative; left: 4%">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=6>' . $dataPembelian->PurchaseID . ' | ' . $dataPembelian->Currency->CurrencyName . ' | Rate : ' . number_format($dataPembelian->CurrencyRate, '2', '.', ',') . ' </th>
                                        </tr>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th width="20%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory ID</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Name</th>
                                            <th width="10%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Qty</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Price</th>
                                            <th width="10%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc (%)</th>
                                            <th width="10%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc 2 (%)</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                $total = 0;
                $vat = 0;
                foreach ($dataPembelian->purchaseDetail as $data) {
                    $total += $data->SubTotal;
                    $vat += $data->VAT;
                    $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->inventory->InventoryID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->inventory->InventoryName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Discount1 . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
                }
                if ($vat != 0) {
                    $vat = $vat - ($dataPembelian->DiscountGlobal * 0.1);
                }
                $html.= '<tr>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=4></td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=2>Total <br> Discount <br> Grand Total <br> VAT <br> Grand Total (Tax)</td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '<br> '
                        . '' . number_format($dataPembelian->DiscountGlobal, '2', '.', ',') . '<br>'
                        . '' . number_format($total - $dataPembelian->DiscountGlobal, '2', '.', ',') . '<br>'
                        . '' . number_format($vat, '2', '.', ',') . '<br>'
                        . '' . number_format($dataPembelian->GrandTotal, '2', '.', ',') . '</td>
                    </tr>
                </tbody>
            </table>';
            }
        } else {
            $html.= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no purchase.</span><br><br>';
        }
        $html.='</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('purchase_detail');
    }

    static function purchaseDataBackup($data) {
        $explode = explode('---;---', $data);
        $typePayment = $explode[0];
        $typeTax = $explode[1];
        $start = $explode[2];
        $end = $explode[3];
        $where = '';
        if ($typePayment != '-1' && $typePayment != '') {
            $where .= 'isCash = "' . $typePayment . '" ';
        }
        if ($typeTax != '-1' && $typeTax != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'VAT = "' . $typeTax . '" ';
        }
        if ($start != '' && $end != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $start = explode('-', $start);
            $end = explode('-', $end);
            $startSearch = $start[2] . '-' . $start[1] . '-' . $start[0];
            $endSearch = $end[2] . '-' . $end[1] . '-' . $end[0];
            $where .= 'PurchaseDate between "' . $startSearch . ' 00:00:00" AND "' . $endSearch . ' 23:59:59"';
        }
        $table = 't_purchase_header';
        $primaryKey = 't_purchase_header`.`InternalID';
        $columns = array(
            array('db' => 'PurchaseID', 'dt' => 0),
            array('db' => 't_purchase_header`.`InternalID', 'dt' => 1, 'formatter' => function( $d, $row ) {
                    $purchase = PurchaseHeader::find($d);
                    if ($purchase->isCash == 0) {
                        return 'Cash (-)';
                    } else {
                        return 'Credit ' . $purchase->LongTerm;
                    }
                },
                'field' => 't_purchase_header`.`InternalID'),
            array(
                'db' => 'PurchaseDate',
                'dt' => 2,
                'formatter' => function( $d, $row ) {
                    return date("d-m-Y", strtotime($d));
                }
            ),
            array('db' => 'CurrencyName', 'dt' => 3),
            array(
                'db' => 'CurrencyRate',
                'dt' => 4,
                'formatter' => function( $d, $row ) {
                    return number_format($d, '2', '.', ',');
                }
            ),
            array(
                'db' => 'ACC6Name',
                'dt' => 5,
                'formatter' => function( $d, $row ) {
                    return $d;
                }
            ),
            array(
                'db' => 'VAT',
                'dt' => 6,
                'formatter' => function( $d, $row ) {
                    if ($d == 0) {
                        return 'Non Tax';
                    } else {
                        return 'Tax';
                    }
                }
            ),
            array(
                'db' => 'GrandTotal',
                'dt' => 7,
                'formatter' => function( $d, $row ) {
                    return number_format($d, '2', '.', ',');
                }
            ),
            array(
                'db' => 'DownPayment',
                'dt' => 8,
                'formatter' => function( $d, $row ) {
                    return number_format($d, '2', '.', ',');
                }
            ),
            array(
                'db' => 'PurchaseID',
                'dt' => 9,
                'formatter' => function( $d, $row ) {
                    $tampReceiv = 'Completed';
                    foreach (PurchaseHeader::getPurchasePayable() as $receiv) {
                        if ($receiv->ID == $d) {
                            $tampReceiv = 'Uncompleted';
                        }
                    }
                    return $tampReceiv;
                }
            ),
            array('db' => 'TaxNumber', 'dt' => 10),
            array('db' => 't_purchase_header`.`InternalID', 'dt' => 11, 'formatter' => function( $d, $row ) {
                    $data = PurchaseHeader::find($d);
                    $action = '<td class="text-center">
                                    <a href="' . Route('purchaseDetail', $data->PurchaseID) . '">
                                        <button id="btn-' . $data->PurchaseID . '-detail"
                                                class="btn btn-pure-xs btn-xs btn-detail">
                                            <span class="glyphicon glyphicon-zoom-in"></span>
                                        </button>
                                    </a>';
                    if (!PurchaseHeader::isReturn($data->PurchaseID) && JournalDetail::where('JournalTransactionID', $data->PurchaseID)->sum('JournalCreditMU') == 0) {
                        $action.='<a href="' . Route('purchaseUpdate', $data->PurchaseID) . '">
                                        <button id="btn-' . $data->PurchaseID . '-update"
                                                class="btn btn-pure-xs btn-xs btn-edit">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </button>
                                    </a>
                                    <button data-target="#m_purchaseDelete" data-internal="' . $data->InternalID . '"  data-toggle="modal" role="dialog"
                                           onclick="deleteAttach(this)" data-id="' . $data->PurchaseID . '" data-name=' . $data->PurchaseID . ' class="btn btn-pure-xs btn-xs btn-delete">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>';
                    } else {
                        $action.='<button disabled class="btn btn-pure-xs btn-xs btn-edit"><span class="glyphicon glyphicon-edit"></span></button>
                                    <button disabled class="btn btn-pure-xs btn-xs btn-delete"><span class="glyphicon glyphicon-trash"></span></button>';
                    }
                    if (checkModul('O05')) {
                        $action.='<a href="' . Route('purchaseCSV', $data->PurchaseID) . '" target="_blank">
                                        <button id="btn-' . $data->PurchaseID . '-print"
                                                class="btn btn-pure-xs btn-xs">
                                            <span class="glyphicon glyphicon-download"></span> CSV
                                        </button>
                                    </a>';
                    }
                    return $action;
                },
                'field' => 't_purchase_header`.`InternalID')
        );

        $sql_details = getConnection();

        require('ssp.class.php');
        $ID_CLIENT_VALUE = Auth::user()->CompanyInternalID;
        if ($where != '') {
            $extraCondition = $where . ' AND ' . 't_purchase_header.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        } else {
            $extraCondition = 't_purchase_header.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        }
        $join = ' INNER JOIN m_currency on m_currency.InternalID = t_purchase_header.CurrencyInternalID '
                . 'INNER JOIN m_coa6 on m_coa6.InternalID = t_purchase_header.ACC6InternalID';

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $extraCondition, $join));
    }

}
