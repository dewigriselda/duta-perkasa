<?php

class PurchaseReturnController extends BaseController {

    public function showPurchaseReturn() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deletePurchaseReturn') {
                return $this->deletePurchaseReturn();
            } else if (Input::get('jenis') == 'summaryPurchase') {
                return $this->summaryPurchaseReturn();
            } else if (Input::get('jenis') == 'detailPurchase') {
                return $this->detailPurchaseReturn();
            } else if (Input::get('jenis') == 'insertPurchaseReturn') {
                return Redirect::Route('purchaseReturnNew', Input::get('purchase'));
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = PurchaseReturnHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
            return View::make('pembelian.purchaseReturnSearch')
                            ->withToogle('transaction')->withAktif('purchaseReturn')
                            ->withData($data);
        }
        return View::make('pembelian.purchaseReturnSearch')
                        ->withToogle('transaction')->withAktif('purchaseReturn');
    }

    public function purchaseReturnNew($id) {
        $id = PurchaseHeader::getIdpurchase($id);
        $header = PurchaseHeader::find($id);
        $detail = PurchaseHeader::find($id)->purchaseDetail()->get();
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertPurchaseReturn') {
                return Redirect::Route('purchaseReturnNew', Input::get('purchase'));
            } else if (Input::get('jenis') == 'summaryPurchase') {
                return $this->summaryPurchaseReturn();
            } else if (Input::get('jenis') == 'detailPurchase') {
                return $this->detailPurchaseReturn();
            } else if (Input::get('jenis') == 'deletePurchaseReturn') {
                return $this->deletePurchaseReturn();
            } else {
                return $this->insertPurchaseReturn();
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = PurchaseReturnHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
            return View::make('pembelian.purchaseReturnSearch')
                            ->withToogle('transaction')->withAktif('purchaseReturn')
                            ->withData($data);
        }
        return View::make('pembelian.purchaseReturnNew')
                        ->withToogle('transaction')->withAktif('purchaseReturn')
                        ->withHeader($header)
                        ->withDetail($detail);
    }

    public function deletePurchaseReturn() {
        $purchaseReturn = JournalDetail::where('JournalTransactionID', PurchaseReturnHeader::find(Input::get('InternalID'))->PurchaseReturnID)->count();
        if ($purchaseReturn > 0) {
            JournalDetail::where('JournalTransactionID', PurchaseReturnHeader::find(Input::get('InternalID'))->PurchaseReturnID)->delete();
        }
        $purchaseReturnHeader = PurchaseReturnHeader::find(Input::get('InternalID'));
        if ($purchaseReturnHeader->CompanyInternalID == Auth::user()->Company->InternalID) {
            $journal = JournalHeader::where('TransactionID', '=', $purchaseReturnHeader->PurchaseReturnID)->get();
            foreach ($journal as $value) {
                JournalDetail::where('JournalInternalID', '=', $value->InternalID)->delete();
                JournalHeader::where('InternalID', '=', $value->InternalID)->delete();
            }
            //hapus detil
            $detilData = PurchaseReturnHeader::find(Input::get('InternalID'))->purchaseReturnDetail;
            foreach ($detilData as $value) {
                $detil = purchaseReturnDetail::find($value->InternalID);
                $detil->delete();
                setTampInventory($detil->InventoryInternalID);
            }
            //hapus purchaseReturn
            $purchaseReturn = PurchaseReturnHeader::find(Input::get('InternalID'));
            $purchaseReturn->delete();
            $messages = 'suksesDelete';
        } else {
            $messages = 'accessDenied';
        }

        $data = PurchaseReturnHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
        return View::make('pembelian.purchaseReturnSearch')
                        ->withToogle('transaction')->withAktif('purchaseReturn')
                        ->withMessages($messages)
                        ->withData($data);
    }

    public function purchaseReturnDetail($id) {
        $id = PurchaseReturnHeader::getIdpurchaseReturn($id);
        $header = PurchaseReturnHeader::find($id);
        $detail = PurchaseReturnHeader::find($id)->purchaseReturnDetail()->get();
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertPurchaseReturn') {
                return Redirect::Route('purchaseReturnNew', Input::get('purchase'));
            } else if (Input::get('jenis') == 'detailPurchase') {
                return $this->detailPurchaseReturn();
            } else if (Input::get('jenis') == 'summaryPurchase') {
                return $this->summaryPurchaseReturn();
            } else if (Input::get('jenis') == 'deletePurchaseReturn') {
                return $this->deletePurchaseReturn();
            }
        }
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            return View::make('pembelian.purchaseReturnDetail')
                            ->withToogle('transaction')->withAktif('purchaseReturn')
                            ->withHeader($header)
                            ->withDetail($detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showPurchaseReturn');
        }
    }

    public function purchaseReturnUpdate($id) {
        $purchaseIDReal = substr($id, 7);
        $id = PurchaseReturnHeader::getIdpurchaseReturn($id);
        $header = PurchaseReturnHeader::find($id);
        $detail = PurchaseReturnHeader::find($id)->purchaseReturnDetail()->get();
        $idPurchase = PurchaseHeader::getIdpurchase($purchaseIDReal);
        $headerPurchase = PurchaseHeader::find($idPurchase);
        $detailPurchase = PurchaseHeader::find($idPurchase)->purchaseDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                if (Input::get('jenis') == 'insertPurchaseReturn') {
                    return Redirect::Route('purchaseReturnNew', Input::get('purchase'));
                } else if (Input::get('jenis') == 'summaryPurchase') {
                    return $this->summaryPurchaseReturn();
                } else if (Input::get('jenis') == 'detailPurchase') {
                    return $this->detailPurchaseReturn();
                } else if (Input::get('jenis') == 'deletePurchaseReturn') {
                    return $this->deletePurchaseReturn();
                } else {
                    return $this->updatePurchaseReturn($id);
                }
            }
            return View::make('pembelian.purchaseReturnUpdate')
                            ->withToogle('transaction')->withAktif('purchaseReturn')
                            ->withHeader($header)
                            ->withDetail($detail)
                            ->withHeaderpurchase($headerPurchase)
                            ->withDetailpurchase($detailPurchase);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showPurchaseReturn');
        }
    }

    public function insertPurchaseReturn() {
        //rule
        $rule = array(
            'date' => 'required',
            'remark' => 'required|max:1000',
            'warehouse' => 'required'
        );
        $purchaseNumber = '';
        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalInsert';
            $error = $validator->messages();
        } else {
            $id = Input::get('PurchaseInternalID');
            $header = PurchaseHeader::find($id);
            $date = explode('-', Input::get('date'));
            //insert header
            $headerI = new PurchaseReturnHeader;
            $purchaseReturnNumber = PurchaseReturnHeader::getNextIDPurchaseReturn($header->PurchaseID);
            $headerI->PurchaseReturnID = $purchaseReturnNumber;
            $headerI->PurchaseReturnDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $headerI->ACC6InternalID = $header->ACC6InternalID;
            $headerI->LongTerm = $header->LongTerm;
            $headerI->isCash = $header->isCash;
            $headerI->WarehouseInternalID = Input::get('warehouse'); //pilihan
            $headerI->CurrencyInternalID = $header->CurrencyInternalID;
            $headerI->CurrencyRate = $header->CurrencyRate;
            $headerI->VAT = $header->VAT;
            $headerI->DiscountGlobal = str_replace(",", "", Input::get("DiscountGlobal"));
            $headerI->GrandTotal = Input::get("grandTotalValue");
            $headerI->UserRecord = Auth::user()->UserID;
            $headerI->CompanyInternalID = Auth::user()->Company->InternalID;
            $headerI->UserModified = '0';
            $headerI->Remark = Input::get('remark');
            $headerI->save();
            //insert detail
            $total = 0;
            for ($a = 0; $a < count(Input::get('inventory')); $a++) {
                $qtyValue = str_replace(',', '', Input::get('qty')[$a]);
                $qtyReturnValue = str_replace(',', '', Input::get('returnPurchase')[$a]);
                if ($qtyValue < $qtyReturnValue) {
                    $qtyReturnValue = $qtyValue;
                }
                $priceValue = Input::get('price')[$a];
                $discValue = str_replace(',', '', Input::get('discountNominal')[$a]);
                $subTotal = ($priceValue * $qtyReturnValue) - (($priceValue * $qtyReturnValue) * Input::get('discount')[$a] / 100);
                $subTotal = $subTotal - ($subTotal * Input::get('discount1')[$a] / 100) - $discValue * $qtyReturnValue;
                if ($header->VAT == '1') {
                    $vatValue = $subTotal / 10;
                } else {
                    $vatValue = 0;
                }
                if ($qtyReturnValue > 0) {
                    $detail = new PurchaseReturnDetail();
                    $detail->PurchaseReturnInternalID = $headerI->InternalID;
                    $detail->InventoryInternalID = Input::get('inventory')[$a];
                    $detail->UomInternalID = Input::get('uom')[$a];
                    $detail->Qty = $qtyReturnValue;
                    $detail->Price = $priceValue;
                    $detail->Discount = Input::get('discount')[$a];
                    $detail->Discount1 = Input::get('discount1')[$a];
                    $detail->DiscountNominal = $discValue;
                    $detail->VAT = $vatValue;
                    $detail->SubTotal = $subTotal;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = '0';
                    $detail->PurchaseDetailInternalID = Input::get('InternalDetail')[$a];
                    $detail->save();
                    setTampInventory(Input::get('inventory')[$a]);
                }
                $total += $subTotal;
            }

            if ($headerI->VAT == '1') {
                $vatValueHeader = ($total - str_replace(",", "", Input::get("DiscountGlobal"))) / 10;
            } else {
                $vatValueHeader = 0;
            }
//            $headerI->GrandTotal = $total - str_replace(",", "", Input::get("DiscountGlobal")) + $vatValueHeader;
//            $headerI->save();

            $total -= str_replace(",", "", Input::get('DiscountGlobal'));
            $rate = $header->CurrencyRate;
            $dataType = PurchaseReturnDetail::getTipeInventoryData($headerI->InternalID);
            if ($header->isCash == 0) {
                if ($header->VAT == 1) {
                    $slip = PurchaseHeader::getSlipInternalID($header->PurchaseID);
                } else {
                    $slip = '-1';
                }
                $this->insertJournal($purchaseReturnNumber, $total, $header->VAT, $header->CurrencyInternalID, $rate, Input::get('date'), $slip, $dataType, str_replace(",", "", Input::get('DiscountGlobal')));
            } else {
                $slip = '-1';
                $this->insertJournal($purchaseReturnNumber, $total, $header->VAT, $header->CurrencyInternalID, $rate, Input::get('date'), $slip, $dataType, str_replace(",", "", Input::get('DiscountGlobal')));
            }
            $messages = 'suksesInsert';
            $error = '';
        }

        if (isset($headerI) && !is_null($headerI) && $messages == 'suksesInsert') {
            return Redirect::route('purchaseReturnDetail', $headerI->PurchaseReturnID)->with("msg", "print");
        }

        $header = PurchaseHeader::find($id);
        $detail = PurchaseHeader::find($id)->purchaseDetail()->get();
        return View::make('pembelian.purchaseReturnNew')
                        ->withToogle('transaction')->withAktif('purchaseReturn')
                        ->withError($error)
                        ->withMessages($messages)
                        ->withHeader($header)
                        ->withDetail($detail);
    }

    public function updatePurchaseReturn($id) {
        //tipe
        $headerUpdate = PurchaseReturnHeader::find($id);
        $detailUpdate = PurchaseReturnHeader::find($id)->purchaseReturnDetail()->get();

        //rule
        $rule = array(
            'remark' => 'required|max:1000',
            'inventory' => 'required',
            'uom' => 'required',
            'warehouse' => 'required'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalUpdate';
            $error = $validator->messages();
        } else {
            //delete purchaseReturn detail -- nantinya insert ulang
            PurchaseReturnDetail::where('PurchaseReturnInternalID', '=', Input::get('PurchaseReturnInternalID'))->update(array('is_deleted' => 1));
            //insert detail
            $total = 0;
            for ($a = 0; $a < count(Input::get('inventory')); $a++) {
                $qtyValue = str_replace(',', '', Input::get('qty')[$a]);
                $qtyReturnValue = str_replace(',', '', Input::get('returnPurchase')[$a]);
                if ($qtyValue < $qtyReturnValue) {
                    $qtyReturnValue = $qtyValue;
                }
                $priceValue = Input::get('price')[$a];
                $discValue = str_replace(',', '', Input::get('discountNominal')[$a]);
                $subTotal = ($priceValue * $qtyReturnValue) - (($priceValue * $qtyReturnValue) * Input::get('discount')[$a] / 100);
                $subTotal = $subTotal - ($subTotal * Input::get('discount1')[$a] / 100) - $discValue * $qtyReturnValue;
                if ($headerUpdate->VAT == '1') {
                    $vatValue = $subTotal / 10;
                } else {
                    $vatValue = 0;
                }
                if ($qtyReturnValue > 0) {
                    $detail = new PurchaseReturnDetail();
                    $detail->PurchaseReturnInternalID = Input::get('PurchaseReturnInternalID');
                    $detail->InventoryInternalID = Input::get('inventory')[$a];
                    $detail->UomInternalID = Input::get('uom')[$a];
                    $detail->Qty = $qtyReturnValue;
                    $detail->Price = $priceValue;
                    $detail->Discount = Input::get('discount')[$a];
                    $detail->Discount1 = Input::get('discount1')[$a];
                    $detail->DiscountNominal = $discValue;
                    $detail->VAT = $vatValue;
                    $detail->SubTotal = $subTotal;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = Auth::user()->UserID;
                    $detail->PurchaseDetailInternalID = Input::get('InternalDetail')[$a];
                    $detail->save();
                    setTampInventory(Input::get('inventory')[$a]);
                }
                $total += $subTotal;
            }

            //change grandtotal jd yg bener
            $headerI = PurchaseReturnHeader::find(Input::get('PurchaseReturnInternalID'));
            $headerI->Remark = Input::get('remark');
            $headerI->WarehouseInternalID = Input::get('warehouse'); //pilihan
            $headerI->DiscountGlobal = str_replace(",", "", Input::get("DiscountGlobal"));
            if ($headerI->VAT == '1') {
                $vatValueHeader = ($total - str_replace(",", "", Input::get("DiscountGlobal"))) / 10;
            } else {
                $vatValueHeader = 0;
            }
//            $headerI->GrandTotal = $total - str_replace(",", "", Input::get("DiscountGlobal")) + $vatValueHeader;
//            $headerI->save();

            $journal = JournalHeader::where('TransactionID', '=', $headerUpdate->PurchaseReturnID)->get();
            foreach ($journal as $value) {
                JournalDetail::where('JournalInternalID', '=', $value->InternalID)->delete();
                JournalHeader::where('InternalID', '=', $value->InternalID)->delete();
            }
            PurchaseReturnDetail::where('PurchaseReturnInternalID', '=', Input::get('PurchaseReturnInternalID'))->where('is_deleted', 1)->delete();

            $currency = $headerUpdate->CurrencyInternalID;
            $rate = $headerUpdate->CurrencyRate;
            $date = date("d-m-Y", strtotime($headerUpdate->PurchaseReturnDate));
            $dataType = PurchaseReturnDetail::getTipeInventoryData($headerI->InternalID);
            $total -= str_replace(",", "", Input::get('DiscountGlobal'));
            if ($headerUpdate->isCash == 0) {
                $purchaseIDReal = substr($headerUpdate->PurchaseReturnID, 7);
                if ($header->VAT == 1) {
                    $slip = PurchaseHeader::getSlipInternalID($purchaseIDReal);
                } else {
                    $slip = '-1';
                }
                $this->insertJournal($headerUpdate->PurchaseReturnID, $total, $headerUpdate->VAT, $currency, $rate, $date, $slip, $dataType, str_replace(",", "", Input::get('DiscountGlobal')));
            } else {
                $slip = '-1';
                $this->insertJournal($headerUpdate->PurchaseReturnID, $total, $headerUpdate->VAT, $currency, $rate, $date, $slip, $dataType, str_replace(",", "", Input::get('DiscountGlobal')));
            }
            $messages = 'suksesUpdate';
            $error = '';
        }

         if (isset($headerI) && !is_null($headerI) && $messages == 'suksesUpdate') {
            return Redirect::route('purchaseReturnDetail', $headerI->PurchaseReturnID)->with("msg", "print");
        }
        
        //tipe
        $header = PurchaseReturnHeader::find($id);
        $detail = PurchaseReturnHeader::find($id)->purchaseReturnDetail()->get();
        $purchaseReturnID = PurchaseReturnHeader::find($id)->PurchaseReturnID;
        $purchaseIDReal = substr($purchaseReturnID, 7);
        $idPurchase = PurchaseHeader::getIdpurchase($purchaseIDReal);
        $headerPurchase = PurchaseHeader::find($idPurchase);
        $detailPurchase = PurchaseHeader::find($idPurchase)->purchaseDetail()->get();
        return View::make('pembelian.purchaseReturnUpdate')
                        ->withToogle('transaction')->withAktif('purchaseReturn')
                        ->withHeader($header)
                        ->withDetail($detail)
                        ->withHeaderpurchase($headerPurchase)
                        ->withDetailpurchase($detailPurchase)
                        ->withError($error)
                        ->withMessages($messages);
    }

    function insertJournal($purchaseReturnNumber, $total, $vat, $currency, $rate, $date, $slip, $dataType, $discountGlobal) {
        $header = new JournalHeader;
        $date = explode('-', $date);
        $yearDigit = substr($date[2], 2);
        $dateText = $date[1] . $yearDigit;
//        $defaultPembelian = Default_s::find(4)->DefaultID;
        $defaultHutang = Default_s::find(5)->DefaultID;
        $defaultOutcome = Default_s::find(6)->DefaultID;
        $defaultDiscount = Default_s::find(10)->DefaultID;
        if ($slip == '-1') {
            $cari = 'ME-' . $dateText;
            $header->JournalType = 'Memorial';
            $akun = array($defaultHutang, $defaultOutcome, $defaultDiscount);
            $header->JournalID = JournalHeader::getNextIDJournal($cari . '-');
            $header->SlipInternalID = Null;
        } else {
            $akun = array("Slip", $defaultOutcome, $defaultDiscount);
            $tampSlipID = Slip::find($slip);
            if ($tampSlipID->Flag == '0') {
                $cari = 'CI-' . $dateText;
                $header->JournalType = 'Cash Out';
            } else if ($tampSlipID->Flag == '1') {
                $cari = 'BI-' . $dateText;
                $header->JournalType = 'Bank Out';
            }
            $header->JournalID = JournalHeader::getNextIDJournal($cari . '-' . $tampSlipID->SlipID . '-');
            $header->SlipInternalID = $slip;
        }
        $header->JournalDate = $date[2] . '-' . $date[1] . '-' . $date[0];
        $header->JournalFrom = Auth::user()->UserID;
        $header->Notes = '';
        $department = Department::where('Default', '1')->first();
        $header->DepartmentInternalID = $department->InternalID;
        $header->TransactionID = $purchaseReturnNumber;
        $header->ACC5InternalID = '0';
        $header->Lock = '0';
        $header->Check = '0';
        $header->Flag = '0';
        $header->UserRecord = Auth::user()->UserID;
        $header->CompanyInternalID = Auth::user()->Company->InternalID;
        $header->UserModified = '0';
        $header->Remark = '';
        $header->save();

        //insert detail
        if ($vat == 1) {
            $vatValue = floor(10 * $total / 100);
        } else {
            $vatValue = 0;
        }
        $count = 1;
        foreach ($dataType as $data2) {
            $tipe = InventoryType::find($data2->InternalID);
            $detail = new JournalDetail();
            $detail->JournalInternalID = $header->InternalID;
            $detail->JournalIndex = $count;
            $detail->JournalNotes = $tipe->InventoryTypeName;
            $detail->JournalDebetMU = 0;
            $detail->JournalCreditMU = $data2->total;
            $detail->CurrencyInternalID = $currency;
            $detail->CurrencyRate = $rate;
            $detail->JournalDebet = 0;
            $detail->JournalCredit = $data2->total * $rate;
            $detail->JournalTransactionID = NULL;
            $detail->ACC1InternalID = $tipe->ACC1InternalID;
            $detail->ACC2InternalID = $tipe->ACC2InternalID;
            $detail->ACC3InternalID = $tipe->ACC3InternalID;
            $detail->ACC4InternalID = $tipe->ACC4InternalID;
            $detail->ACC5InternalID = $tipe->ACC5InternalID;
            $detail->ACC6InternalID = $tipe->ACC6InternalID;
            $coa = Coa::getInternalID($tipe->ACC1InternalID, $tipe->ACC2InternalID, $tipe->ACC3InternalID, $tipe->ACC4InternalID, $tipe->ACC5InternalID, $tipe->ACC6InternalID);
            $detail->COAName = Coa::find($coa)->COAName;
            $detail->UserRecord = Auth::user()->UserID;
            $detail->UserModified = '0';
            $detail->save();
            $count++;
        }

        foreach ($akun as $data) {
            $kreditValue = 0;
            $debetValue = 0;
            if (($data != $defaultOutcome && $data != $defaultDiscount) || ($data == $defaultOutcome && $vatValue != 0) || ($data == $defaultDiscount && $discountGlobal != 0)) {
                $detail = new JournalDetail();
                $detail->JournalInternalID = $header->InternalID;
                $detail->JournalIndex = $count;
                $detail->JournalNotes = $data;
                if ($data == $defaultHutang || $data == 'Slip') {
                    $debetValue = $total + $vatValue;
                    $detail->JournalDebetMU = $debetValue;
                    $detail->JournalCreditMU = 0;
                } else if ($data == $defaultDiscount) {
                    $debetValue = $discountGlobal;
                    $detail->JournalDebetMU = $debetValue;
                    $detail->JournalCreditMU = 0;
                } else {
                    $kreditValue = $vatValue;
                    $detail->JournalDebetMU = 0;
                    $detail->JournalCreditMU = $kreditValue;
                }
                $detail->CurrencyInternalID = $currency;
                $detail->CurrencyRate = $rate;
                $detail->JournalDebet = $debetValue * $rate;
                $detail->JournalCredit = $kreditValue * $rate;
                $detail->JournalTransactionID = NULL;
                if ($data != 'Slip') {
                    $default = Default_s::getInternalCoa($data);
                } else {
                    $default = Slip::getInternalCoa($slip);
                }
                $detail->ACC1InternalID = $default->ACC1InternalID;
                $detail->ACC2InternalID = $default->ACC2InternalID;
                $detail->ACC3InternalID = $default->ACC3InternalID;
                $detail->ACC4InternalID = $default->ACC4InternalID;
                $detail->ACC5InternalID = $default->ACC5InternalID;
                $detail->ACC6InternalID = $default->ACC6InternalID;
                $coa = Coa::getInternalID($default->ACC1InternalID, $default->ACC2InternalID, $default->ACC3InternalID, $default->ACC4InternalID, $default->ACC5InternalID, $default->ACC6InternalID);
                $detail->COAName = Coa::find($coa)->COAName;
                $detail->UserRecord = Auth::user()->UserID;
                $detail->UserModified = '0';
                $detail->save();
                $count++;
            }
        }
    }

    function purchaseReturnPrint($id) {
        $id = PurchaseReturnHeader::getIdpurchaseReturn($id);
        $header = PurchaseReturnHeader::find($id);
        $detail = PurchaseReturnHeader::find($id)->purchaseReturnDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $coa6 = PurchaseReturnHeader::find($header->InternalID)->coa6;

            $supplier = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . '<br> ' . $coa6->Address . '<br>Phone: ' . $coa6->Phone . '<br> Fax: ' . $coa6->Fax;
            if ($header->isCash == 0) {
                $payment = 'Cash';
            } else if ($header->isCash == 1) {
                $payment = 'Credit';
            } else if ($header->isCash == 2) {
                $payment = 'CBD';
            } else if ($header->isCash == 3) {
                $payment = 'Deposit';
            } else {
                $payment = 'Down Payment';
            }
            $currency = Currency::find($header->CurrencyInternalID);
            $currencyName = $currency->CurrencyName;
            if ($header->VAT == 0) {
                $vat = 'Non Tax';
            } else {
                $vat = 'Tax';
            }
            $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 6px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box; position: relative;">
                    <div style="height: 195px;border-bottom:1px solid #ddd;margin-bottom:20px;">
                        <div style="height: 133px">
                        <img height="100%" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                        </div>
                            <div style="width: 420px; float: left;">
                                <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->Company->CompanyName . '</b>
                            </div> 
                            <div style=" box-sizing: border-box; margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Address</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Purchase Return</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;padding-left:6px;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span><br />
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                            <table>
                            <tr>
                            <td width="275px">
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Purchase Return ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->PurchaseReturnID . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->PurchaseReturnDate)) . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">Supplier</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $supplier . '</td>
                                 </tr>';
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-') {
                $html .= '     <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Contact Person</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->ContactPerson . '</td>
                                 </tr>';
            }
            $html .= '<tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Payment</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $payment . '</td>
                                 </tr>
                                </table>
                                </td>
                                <td style="vertical-align: text-top;">
                                <table>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Warehouse</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Warehouse->WarehouseName . '</td>
                                     </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Currency</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $currencyName . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Rate</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . number_format($header->CurrencyRate, '2', '.', ',') . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Vat</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $vat . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Remark</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Remark . '</td>
                                     </tr>
                                </table></td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="25%">Inventory</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Uom</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Qty</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Price</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc (%)</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc 2 (%)</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            $total = 0;
            $totalVAT = 0;
            if (count($detail) > 0) {
                foreach ($detail as $data) {
                    $inventory = Inventory::find($data->InventoryInternalID);
                    $inv = $inventory->InventoryID . ' ' . $inventory->TextPrint;
                    $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $inv . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $data->Uom->UomID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->Discount1 . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
                    $totalVAT += $data->VAT;
                    $total += $data->SubTotal;
                }
                if ($totalVAT != 0) {
                    $totalVAT = $totalVAT - ($header->DiscountGlobal * 0.1);
                }
            } else {
                $html .= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this purchaseReturn order.</td>
                        </tr>';
            }
            $html .= '</tbody>
                        </table>
                        
                    <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                        <table>
                        <tr>
                        <td width="275px" style="vertical-align: text-top;">
                            <table>
                            <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">Remark</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Remark . '</td>
                             </tr> 
                            </table>
                            </td>
                            <td width="200px" style="float:right;">
                             <table style="margin-left:30px;">
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Discount</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Grand Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total - $header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Tax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($totalVAT, '2', '.', ',') . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; ">Grand Total (Tax)</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">'
                    . number_format($header->GrandTotal, '2', '.', ',') . '</td>
                                 </tr>
                                </table>
                            </td>
                            </tr>
                        </table>
                    </div>
                    
                </div>
            </body>
        </html>';
//            return PDF ::load($html, 'A5', 'portrait')->show();

            return View::make('template.print.purchaseReturnPrint')
                            ->with('header', $header)
                            ->with('supplier', $supplier)
                            ->with('vat', $vat)
                            ->with('id', $id)
                            ->with('currencyName', $currencyName)
                            ->with('payment', $payment)
                            ->with('detail', $detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showPurchaseReturn');
        }
    }

    public function summaryPurchaseReturn() {
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $totalPR = 0;
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Purchase Return Summary Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span>
                            <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span>';
        $hitung = 0;
        foreach (Coa6::where("Type", "s")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $dataSupplier) {
            if (PurchaseReturnHeader::join("m_warehouse", "m_warehouse.InternalID", "=", "t_purchasereturn_header.WarehouseInternalID")
                            ->where('t_purchasereturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                            ->where('ACC6InternalID', $dataSupplier->InternalID)
                            ->where("Type", Auth::user()->WarehouseCheck)
                            ->whereBetween('PurchaseReturnDate', Array($start, $end))->count() > 0) {
                $html .= '<table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=7>' . $dataSupplier->ACC6Name . '</th>
                                        </tr>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Purchase Return ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Currency</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Rate</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total(After Discount)</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Vat</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Grand Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                $sumGrandTotal = 0;
                foreach (PurchaseReturnHeader::join("m_warehouse", "m_warehouse.InternalID", "=", "t_purchasereturn_header.WarehouseInternalID")
                        ->where('t_purchasereturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where('ACC6InternalID', $dataSupplier->InternalID)
                        ->where("Type", Auth::user()->WarehouseCheck)
                        ->whereBetween('PurchaseReturnDate', Array($start, $end))->get() as $data) {
                    $grandTotal = $data->GrandTotal;
                    $sumGrandTotal += $grandTotal;
                    $totalPR += $grandTotal;
                    $total = $grandTotal;
                    $vat = 0;
                    if ($data->VAT == 1) {
                        $total = $total * 10 / 11;
                        $vat = $total / 10;
                    }
                    $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->PurchaseReturnID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->PurchaseReturnDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->Currency->CurrencyName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->CurrencyRate, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($total, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($vat, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($grandTotal, '2', '.', ',') . '</td>
                            </tr>';
                }
                $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="80%" colspan="6">Total : </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . number_format($sumGrandTotal, '2', '.', ',') . '</td>
                        </tr>';

                $html .= '</tbody>
            </table>';
                $hitung++;
            }
        }

        if ($hitung == 0) {
            $html .= '<br><span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no purchase.</span>';
        }
        $html .= '<table class="tableBorder" width="100%"  style="margin-top: 10px; clear: both;  top: 50px;">
                    <thead>
                        <tr>
                            <th style="font-family: helvetica,sans-serif;font-size: 14px; margin: 5px !important; font-weight: 700; text-align: right" width="80%" colspan="6">Total Purchase Return : </th>
                            <th style="font-family: helvetica,sans-serif;font-size: 14px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . number_format($totalPR, '2', '.', ',') . '</th>
                        </tr>
                    <thead>
                </table>';
        $html .= '</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('purchase_return_summary');
    }

    public function detailPurchaseReturn() {
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $dateTamp = '';
        $coa6Tamp = '';
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Purchase Return Detail Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span>'
                . '<span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span><br><br>';
        if (PurchaseReturnHeader::join("m_warehouse", "m_warehouse.InternalID", "=", "t_purchasereturn_header.WarehouseInternalID")
                        ->where('t_purchasereturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where("Type", Auth::user()->WarehouseCheck)
                        ->whereBetween('PurchaseReturnDate', Array($start, $end))
                        ->orderBy('PurchaseReturnDate')->orderBy('ACC6InternalID')->count() > 0) {
            foreach (PurchaseReturnHeader::join("m_warehouse", "m_warehouse.InternalID", "=", "t_purchasereturn_header.WarehouseInternalID")
                    ->where('t_purchasereturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where("Type", Auth::user()->WarehouseCheck)
                    ->whereBetween('PurchaseReturnDate', Array($start, $end))
                    ->orderBy('PurchaseReturnDate')->orderBy('ACC6InternalID')->get() as $dataPembelian) {
                if ($dateTamp != date("d-M-Y", strtotime($dataPembelian->PurchaseReturnDate))) {
                    $html .= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Purchase Return Date : ' . date("d-M-Y", strtotime($dataPembelian->PurchaseReturnDate)) . '</span><br><br>';
                    $dateTamp = date("d-M-Y", strtotime($dataPembelian->PurchaseReturnDate));
                    $coa6Tamp = '';
                }
                if ($coa6Tamp != $dataPembelian->ACC6InternalID) {
                    $html .= '<span style="padding-left:23px;font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Supplier : ' . $dataPembelian->coa6->ACC6Name . '</span>';
                    $coa6Tamp = $dataPembelian->ACC6InternalID;
                }
                if ($dataPembelian->coa6->ContactPerson != '' && $dataPembelian->coa6->ContactPerson != '-' && $dataPembelian->coa6->ContactPerson != null) {
                    $html .= '<span style="padding-left:23px;font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Contact Person : ' . $dataPembelian->coa6->ContactPerson . '</span>';
                }
                $html .= '<table class="tableBorder" style="width:95%; margin-top: 18px; clear: both; position: relative; left: 4%">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=9>' . $dataPembelian->PurchaseReturnID . ' | ' . $dataPembelian->Currency->CurrencyName . ' | Rate : ' . number_format($dataPembelian->CurrencyRate, '2', '.', ',') . ' </th>
                                        </tr>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory ID</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Name</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Uom</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Qty</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Price</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc (%)</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc 2 (%)</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                $total = 0;
                $vat = 0;
                foreach ($dataPembelian->purchaseReturnDetail as $data) {
                    $total += $data->SubTotal;
                    $vat += $data->VAT;
                    $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->inventory->InventoryID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->inventory->InventoryName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->uom->UomID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Discount1 . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
                }
                $html .= '<tr>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=5></td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=3>Total <br> Discount <br> Grand Total <br> VAT <br> Grand Total </td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '<br> '
                        . '' . number_format($dataPembelian->DiscountGlobal, '2', '.', ',') . '<br>'
                        . '' . number_format($total - $dataPembelian->DiscountGlobal, '2', '.', ',') . '<br>'
                        . '' . number_format($vat, '2', '.', ',') . '<br>'
                        . '' . number_format($dataPembelian->GrandTotal, '2', '.', ',') . '</td>
                    </tr>
                </tbody>
            </table>';
            }
        } else {
            $html .= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no purchase return.</span><br><br>';
        }
        $html .= '</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('purchase_return_detail');
    }

    //====================ajax====================================
    public function getResultSearchPR() {
        $hitung = 0;
        $input = splitSearchValue(Input::get("id"));
        $purchaseHeader = PurchaseHeader::where('t_purchase_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->join("m_coa6", 'm_coa6.InternalID', '=', 't_purchase_header.ACC6InternalID')
                ->join("m_warehouse", 'm_warehouse.InternalID', '=', 't_purchase_header.WarehouseInternalID')
                ->where('m_warehouse.Type', Auth::user()->WarehouseCheck)
                ->where('Import', 0)
                ->Where(function($query) use($input) {
                    $query->orWhere('PurchaseID', "like", '%' . $input . "%")
                    ->orWhere("PurchaseDate", "like", '%' . date("Y-m-d", strtotime($input)) . "%")
                    ->orWhere("ACC6Name", "like", '%' . $input . "%");
                })
                ->OrderBy('PurchaseDate', 'desc')
                ->select('t_purchase_header.*', 'ACC6Name')
                ->get();
//                ->where("PurchaseID", "like", $input)
//                ->orWhere("PurchaseDate", "like", '%' . date("Y-m-d", strtotime(Input::get("id"))) . '%')
//                ->orWhere("UserRecord", "like", $input)
        if (count($purchaseHeader) == 0) {
            ?>
            <span>Purchase Return with keywords <i><u><?php echo Input::get("id"); ?></u></i> can't be found</span>
            <?php
        } else {
            ?>
            <select class="chosen-select choosen-modal" id="purchase" style="" name="purchase">
                <?php
                foreach ($purchaseHeader as $data) {
                    if (checkPurchaseReturn($data->InternalID) && $hitung < 100) {
                        ?>
                        <option value="<?php echo $data->PurchaseID ?>"><?php echo $data->PurchaseID . ' | ' . date("d-m-Y", strtotime($data->PurchaseDate)) . ' | ' . $data->coa6->ACC6Name ?></option>
                        <?php
                        $hitung++;
                    }
                }
                ?>
            </select>
            <script>
                $(document).ready(function () {
                    var hitung = '<?php echo $hitung; ?>';
                    if (hitung == 0) {
                        $('#purchase').after('<span>There is no result.</span>');
                        $('#purchase').remove();
                    } else {
                        $("#btn-add-pr").removeAttr("disabled");
                    }
                });
            </script>
            <?php
        }
    }

    //====================ajax====================================


    static function purchaseReturnDataBackup($data) {
        $explode = explode('---;---', $data);
        $typePayment = $explode[0];
        $typeTax = $explode[1];
        $start = $explode[2];
        $end = $explode[3];
        $where = '';
        if ($typePayment != '-1' && $typePayment != '') {
            $where .= 'isCash = "' . $typePayment . '" ';
        }
        if ($typeTax != '-1' && $typeTax != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'VAT = "' . $typeTax . '" ';
        }

        if ($where != '') {
            $where .= ' AND ';
        }
        $where .= 'm_warehouse.Type = ' . Auth::user()->WarehouseCheck . ' ';

        if ($start != '' && $end != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $start = explode('-', $start);
            $end = explode('-', $end);
            $startSearch = $start[2] . '-' . $start[1] . '-' . $start[0];
            $endSearch = $end[2] . '-' . $end[1] . '-' . $end[0];
            $where .= 'PurchaseReturnDate between "' . $startSearch . ' 00:00:00" AND "' . $endSearch . ' 23:59:59"';
        }
        $table = 't_purchasereturn_header';
        $primaryKey = 't_purchasereturn_header`.`InternalID';
        $columns = array(
            array('db' => 't_purchasereturn_header`.`InternalID', 'dt' => 0, 'formatter' => function($d, $row) {
                    return $d;
                }),
            array('db' => 'PurchaseReturnID', 'dt' => 1),
            array('db' => 'isCash', 'dt' => 2, 'formatter' => function( $d, $row ) {
                    if ($d == 0) {
                        return 'Cash';
                    } else if ($d == 1) {
                        return 'Credit';
                    } else if ($d == 2) {
                        return 'CBD';
                    } else if ($d == 3) {
                        return 'Deposit';
                    } else {
                        return 'Down Payment';
                    }
                },
                'field' => 't_purchasereturn_header`.`InternalID'),
            array(
                'db' => 'PurchaseReturnDate',
                'dt' => 3,
                'formatter' => function( $d, $row ) {
                    return date("d-m-Y", strtotime($d));
                }
            ),
            array('db' => 'CurrencyName', 'dt' => 4),
            array(
                'db' => 'CurrencyRate',
                'dt' => 5,
                'formatter' => function( $d, $row ) {
                    return number_format($d, '2', '.', ',');
                }
            ),
            array(
                'db' => 'ACC6Name',
                'dt' => 6,
                'formatter' => function( $d, $row ) {
                    return $d;
                }
            ),
            array(
                'db' => 'VAT',
                'dt' => 7,
                'formatter' => function( $d, $row ) {
                    if ($d == 0) {
                        return 'Non Tax';
                    } else {
                        return 'Tax';
                    }
                }
            ),
            array(
                'db' => 'GrandTotal',
                'dt' => 8,
                'formatter' => function( $d, $row ) {
                    return number_format($d, '2', '.', ',');
                }
            ),
            array('db' => 't_purchasereturn_header`.`InternalID', 'dt' => 9, 'formatter' => function( $d, $row ) {
                    $data = PurchaseReturnHeader::find($d);
                    $action = '<td class="text-center">
                                    <a href="' . Route('purchaseReturnDetail', $data->PurchaseReturnID) . '">
                                        <button id="btn-' . $data->PurchaseReturnID . '-detail"
                                                class="btn btn-pure-xs btn-xs btn-detail" title="detail">
                                            <span class="glyphicon glyphicon-zoom-in"></span>
                                        </button>
                                    </a>';
                    $action .= '<a href="' . Route('purchaseReturnUpdate', $data->PurchaseReturnID) . '">
                                        <button id="btn-' . $data->PurchaseReturnID . '-update"
                                                class="btn btn-pure-xs btn-xs btn-edit" title="update">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </button>
                                    </a>
                                    <button data-target="#m_purchaseReturnDelete" data-internal="' . $data->InternalID . '"  data-toggle="modal" role="dialog"
                                           onclick="deleteAttach(this)" data-id="' . $data->PurchaseReturnID . '" data-name=' . $data->PurchaseReturnID . ' class="btn btn-pure-xs btn-xs btn-delete" title="delete">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>';
                    return $action;
                },
                'field' => 't_purchasereturn_header`.`InternalID')
        );

        $sql_details = getConnection();

        require('ssp.class.php');
        $ID_CLIENT_VALUE = Auth::user()->CompanyInternalID;
        if ($where != '') {
            $extraCondition = $where . ' AND ' . 't_purchasereturn_header.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        } else {
            $extraCondition = 't_purchasereturn_header.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        }
        $join = ' INNER JOIN m_currency on m_currency.InternalID = t_purchasereturn_header.CurrencyInternalID '
                . 'INNER JOIN m_coa6 on m_coa6.InternalID = t_purchasereturn_header.ACC6InternalID'
                . ' INNER JOIN m_warehouse on m_warehouse.InternalID = t_purchasereturn_header.WarehouseInternalID'
        ;

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $extraCondition, $join));
    }

}
