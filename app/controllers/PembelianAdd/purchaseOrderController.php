<?php

class PurchaseOrderController extends BaseController {

    public function showPurchaseOrder() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deletePurchaseOrder') {
                return $this->deletePurchaseOrder();
            } else if (Input::get('jenis') == 'summaryPurchaseOrder') {
                return $this->summaryPurchaseOrder();
            } else if (Input::get('jenis') == 'detailPurchaseOrder') {
                return $this->detailPurchaseOrder();
            } else if (Input::get('jenis') == 'closePurchaseOrder') {
                return $this->closePurchaseOrder();
            } else if (Input::get('jenis') == 'outstandingPurchaseOrder') {
                return $this->outstandingPurchaseOrder();
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = PurchaseOrderHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
            return View::make('pembelianAdd.purchaseOrderSearch')
                            ->withToogle('transaction')->withAktif('purchaseOrder')
                            ->withData($data);
        }
        return View::make('pembelianAdd.purchaseOrder')
                        ->withToogle('transaction')->withAktif('purchaseOrder');
    }

    public function purchaseOrderNew() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deletePurchaseOrder') {
                return $this->deletePurchaseOrder();
            } else if (Input::get('jenis') == 'summaryPurchaseOrder') {
                return $this->summaryPurchaseOrder();
            } else if (Input::get('jenis') == 'detailPurchaseOrder') {
                return $this->detailPurchaseOrder();
            } else if (Input::get('jenis') == 'outstandingPurchaseOrder') {
                return $this->outstandingPurchaseOrder();
            } else {
                return $this->insertPurchaseOrder();
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = PurchaseOrderHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
            return View::make('pembelianAdd.purchaseOrderSearch')
                            ->withToogle('transaction')->withAktif('purchaseOrder')
                            ->withData($data);
        }
        $purchaseOrder = $this->createID(0) . '.';
        return View::make('pembelianAdd.purchaseOrderNew')
                        ->withToogle('transaction')->withAktif('purchaseOrder')
                        ->withPurchaseorder($purchaseOrder);
    }

    public function purchaseOrderRepeat() {
        return Redirect::route('repeatOrder', Coa6::find(Input::get('supplier'))->ACC6ID);
    }

    public function repeatOrder($id) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'order') {
                return $this->insertRepeatOrder($id);
            } else if (Input::get('jenis') == 'deletePurchaseOrder') {
                return $this->deletePurchaseOrder();
            }
        }
        $id = Coa6::where('ACC6ID', $id)->where('Type', 's')->first()->InternalID;
        $purchaseOrder = $this->createID(0) . '.';
        $detail = PurchaseOrderDetail::join('t_purchaseorder_header', 't_purchaseorder_header.InternalID', '=', 't_purchaseorder_detail.PurchaseOrderInternalID')
                        ->where('ACC6InternalID', $id)
                        ->select('InventoryInternalID')->distinct()->get();
        return View::make('pembelianAdd.purchaseOrderRepeatOrder')
                        ->withDetail($detail)
                        ->withToogle('transaction')->withAktif('purchaseOrder')
                        ->withPurchaseorder($purchaseOrder);
    }

    public function purchaseOrderDetail($id) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'summaryPurchaseOrder') {
                return $this->summaryPurchaseOrder();
            } else if (Input::get('jenis') == 'detailPurchaseOrder') {
                return $this->detailPurchaseOrder();
            } else if (Input::get('jenis') == 'deletePurchaseOrder') {
                return $this->deletePurchaseOrder();
            } else if (Input::get('jenis') == 'outstandingPurchaseOrder') {
                return $this->outstandingPurchaseOrder();
            }
        }
        $id = PurchaseOrderHeader::getIdpurchaseOrder($id);
        $header = PurchaseOrderHeader::find($id);
        $detail = PurchaseOrderHeader::find($id)->purchaseOrderDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            return View::make('pembelianAdd.purchaseOrderDetail')
                            ->withToogle('transaction')->withAktif('purchaseOrder')
                            ->withHeader($header)
                            ->withDetail($detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showPurchaseOrder');
        }
    }

    public function purchaseOrderUpdate($id) {
        $id = PurchaseOrderHeader::getIdpurchaseOrder($id);
        $header = PurchaseOrderHeader::find($id);
        $detail = PurchaseOrderHeader::find($id)->purchaseOrderDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID && PurchaseOrderHeader::isMrv($header->InternalID) == false) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                if (Input::get('jenis') == 'summaryPurchaseOrder') {
                    return $this->summaryPurchaseOrder();
                } else if (Input::get('jenis') == 'detailPurchaseOrder') {
                    return $this->detailPurchaseOrder();
                } else if (Input::get('jenis') == 'deletePurchaseOrder') {
                    return $this->deletePurchaseOrder();
                } else if (Input::get('jenis') == 'outstandingPurchaseOrder') {
                    return $this->outstandingPurchaseOrder();
                } else {
                    return $this->updatePurchaseOrder($id);
                }
            }
            $purchaseOrder = $this->createID(0) . '.';
            return View::make('pembelianAdd.purchaseOrderUpdate')
                            ->withToogle('transaction')->withAktif('purchaseOrder')
                            ->withHeader($header)
                            ->withDetail($detail)
                            ->withPurchaseorder($purchaseOrder);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showPurchaseOrder');
        }
    }

    public function insertPurchaseOrder() {
        //rule
        $jenis = Input::get('isCash');
        if ($jenis == '0' || $jenis == '2' || $jenis == '3') {
            $rule = array(
                'date' => 'required',
                'coa6' => 'required',
//                'remark' => 'required|max:1000',
//                'warehouse' => 'required',
                'currency' => 'required',
                'rate' => 'required'
            );
            $longTerm = 0;
        } else {
            if ($jenis == '1') {
                $rule = array(
                    'date' => 'required',
                    'longTerm' => 'required|integer',
                    'coa6' => 'required',
//                'remark' => 'required|max:1000',
//                'warehouse' => 'required',
                    'currency' => 'required',
                    'rate' => 'required'
                );
            } else {
                $rule = array(
                    'date' => 'required',
                    'longTerm' => 'required|integer',
                    'coa6' => 'required',
                    'downPayment' => 'required',
//                'remark' => 'required|max:1000',
//                'warehouse' => 'required',
                    'currency' => 'required',
                    'rate' => 'required'
                );
            }
            $longTerm = Input::get('longTerm');
        }
        if (!Input::hasFile('importorder')) {
            $rule += ['inventory' => 'required', 'uom' => 'required'];
        } else {
            $rule += ['importorder' => 'mimes:xls,xlsx,csv'];
        }
        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalInsert';
            $error = $validator->messages();
        } else {
            //valid
            //insert header
            $header = new PurchaseOrderHeader;
            $purchaseOrder = $this->createID(1) . '.';
            $date = explode('-', Input::get('date'));
            $yearDigit = substr($date[2], 2);
            $purchaseOrder .= $date[1] . $yearDigit . '.';
            $purchaseOrderNumber = PurchaseOrderHeader::getNextIDPurchaseOrder($purchaseOrder);
            $header->PurchaseOrderID = $purchaseOrderNumber;
            $header->PurchaseOrderDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $header->ACC6InternalID = Input::get('coa6');
            $header->LongTerm = $longTerm;
            $header->isCash = $jenis;
            $header->WarehouseInternalID = Input::get('warehouse');
            if (Input::get('jenisDP') == 0){
                $header->DownPayment = str_replace(",", "", Input::get('downPayment'));
                $header->DPValue = 0;
            }
            else{
                $header->DPValue = str_replace(",", "", Input::get('downPaymentValue'));
                $header->DownPayment = 0;
            }
            $currency = explode('---;---', Input::get('currency'));
            $header->CurrencyInternalID = $currency[0];
            $header->CurrencyRate = str_replace(',', '', Input::get('rate'));
            if (Input::get('vat') == '') {
                $header->VAT = '0';
            } else {
                $header->VAT = Input::get('vat');
            }
            if (Input::get('jenispo') == 'import') {
                $header->Import = 1;
            } else {
                $header->Import = 0;
            }
            $header->DiscountGlobal = str_replace(",", "", Input::get('DiscountGlobal'));
            $header->GrandTotal = Input::get('grandTotalValue');
            $header->Attn = Input::get('attn');
            $header->KirimVia = Input::get('kirimvia');
            $header->RequestBy = Input::get('requestby');
            $header->UserRecord = Auth::user()->UserID;
            $header->CompanyInternalID = Auth::user()->Company->InternalID;
            $header->UserModified = '0';
            $header->Remark = Input::get('remark');
            $header->save();
            //cek ada import order nda
            //kalau ada
            //IMPORT ORDER LAMA
//            if (Input::hasFile('importorder')) {
//                Excel::load(Input::file('importorder'), function($reader) {
//                    $result = $reader->select(array('inventory_id', 'inventory_name', 'uom_id', 'qty', 'price',
//                                'discount', 'discount_2', 'discount_nominal', 'subtotal'))->get();
//                    $header = PurchaseOrderHeader::orderBy('InternalID', 'desc')->first();
////                    echo "<pre>";
////                    print_r($result);
////                    echo "</pre>";
////                    exit();
//                    $total = 0;
//                    foreach ($result[0] as $row) {
//                        if (Input::get('vat') == '1') {
//                            $vatValue = $row['subtotal'] / 10;
//                        } else {
//                            $vatValue = 0;
//                        }
//                        $inventory = Inventory::where('InventoryID', $row['inventory_id'])->first()->InternalID;
//                        $uom = Uom::where('UomID', $row['uom_id'])->first()->InternalID;
//
//                        $detail = new PurchaseOrderDetail();
//                        $detail->PurchaseOrderInternalID = $header->InternalID;
//                        $detail->InventoryInternalID = $inventory;
//                        $detail->UomInternalID = $uom;
//                        $detail->Qty = $row['qty'];
//                        $detail->Price = $row['price'];
//                        $detail->Discount = $row['discount'];
//                        $detail->Discount1 = $row['discount_2'];
//                        $detail->DiscountNominal = $row['discount_nominal'];
//                        $detail->VAT = $vatValue;
//                        $detail->SubTotal = $row['subtotal'];
//                        $detail->UserRecord = Auth::user()->UserID;
//                        $detail->UserModified = '0';
//                        $detail->save();
//
//                        $total += $row['subtotal'];
//                    }
//                    $header->GrandTotal = $total;
//                    $header->save();
//                });
//            } 
            //IMPORT ORDER BARU
            if (Input::hasFile('importorder')) {
                Excel::load(Input::file('importorder'), function($reader) {
                    $filename = $_FILES['importorder']['name'];
                    $codeID = explode(' ', $filename)[0];

//                    $result = $reader->select(array('code', 'cat', 'model_no', 'desc', 'de',
//                                'j5', 'ae37', 'af43', 'mt', 'dt', 'brand', 'list'))->get();
                    $result = $reader->select(array('code', 'item_name', 'description', 'price', 'disc', 'qty', 'total'))->get();
                    $header = PurchaseOrderHeader::orderBy('InternalID', 'desc')->first();
//                    echo "<pre>";
//                    print_r($result);
//                    echo "</pre>";
//                    exit();
                    $total = 0;
                    foreach ($result as $row) {
                        if ($row['qty'] != '-' && $row['qty'] > 0) {
//                            $codehuruf = preg_replace('/[0-9]/', '', $codeID);
//                            $codeangka = str_pad((int) $row['code'], 4, '0', STR_PAD_LEFT);
//                            $code = $codehuruf . $codeangka;
//                            dd($row['pricelist']);
                            if (Inventory::where('InventoryID', $row['code'])->count() < 1)
                                break;
                            $inventory = Inventory::where('InventoryID', $row['code'])->first()->InternalID;
                            $uom = InventoryUom::where('InventoryInternalID', $inventory)->where('Default', 1)->first()->UomInternalID;
//                            $price = ;
//                            $warehouse = Warehouse::find($header->WarehouseInternalID);
//                            if ($warehouse->WarehouseID == 'DE')
//                                $qty = $row['de'];
//                            else if ($warehouse->WarehouseID == 'J5')
//                                $qty = $row['j5'];
//                            else if ($warehouse->WarehouseID == 'AE37')
//                                $qty = $row['ae37'];
//                            else if ($warehouse->WarehouseID == 'AF43')
//                                $qty = $row['af43'];
//                            else if ($warehouse->WarehouseID == 'MT')
//                                $qty = $row['mt'];
//                            else if ($warehouse->WarehouseID == 'DT')
//                                $qty = $row['dt'];
//                            else
//                                $qty = 0;
//                            if ($qty > 0) {
                            $detail = new PurchaseOrderDetail();
                            $detail->PurchaseOrderInternalID = $header->InternalID;
                            $detail->InventoryInternalID = $inventory;
                            $detail->UomInternalID = $uom;
                            $detail->Qty = $row['qty'];
                            $detail->Price = str_replace(",", "", $row['price']);
                            $detail->Discount = str_replace("%", "", $row['disc']) * 100;
                            $detail->Discount1 = 0;
                            $detail->DiscountNominal = 0;
                            $subtotal = $row['qty'] * str_replace(",", "", $row['price']) * (1 - str_replace(",", "", $row['disc']));
                            if (Input::get('vat') == '1') {
                                $vatValue = $subtotal / 10;
                            } else {
                                $vatValue = 0;
                            }
                            $detail->VAT = $vatValue;
                            $detail->SubTotal = $subtotal;
                            $detail->UserRecord = Auth::user()->UserID;
                            $detail->UserModified = '0';
                            $detail->save();
//                            }
                            $total += $subtotal;
                        }
                    }
                    $header->GrandTotal = $total;
                    $header->save();
                });
            } else {
                //kalau ga ada import order
                //insert detail
                $total = 0;
                for ($a = 0; $a < count(Input::get('inventory')); $a++) {
                    $qtyValue = str_replace(',', '', Input::get('qty')[$a]);
                    $priceValue = str_replace(',', '', Input::get('price')[$a]);
                    $discValue = str_replace(',', '', Input::get('discountNominal')[$a]);
                    $subTotal = ($priceValue * $qtyValue) - (($priceValue * $qtyValue) * Input::get('discount')[$a] / 100);
                    $subTotal = $subTotal - ($subTotal * Input::get('discount1')[$a] / 100) - $discValue * $qtyValue;
                    $subTotal = number_format($subTotal, 2, '.', '');
                    $total += $subTotal;
                    if (Input::get('vat') == '1') {
                        $vatValue = $subTotal / 10;
                    } else {
                        $vatValue = 0;
                    }
                    if ($qtyValue > 0) {
                        $detail = new PurchaseOrderDetail();
                        $detail->PurchaseOrderInternalID = $header->InternalID;
                        $detail->InventoryInternalID = Input::get('inventory')[$a];
                        $detail->UomInternalID = Input::get('uom')[$a];
                        $detail->Qty = $qtyValue;
                        $detail->Price = $priceValue;
                        $detail->Discount = Input::get('discount')[$a];
                        $detail->Discount1 = Input::get('discount1')[$a];
                        $detail->Remark = Input::get('spesifikasi')[$a];
                        $detail->DiscountNominal = $discValue;
                        $detail->VAT = $vatValue;
                        $detail->SubTotal = $subTotal;
                        $detail->UserRecord = Auth::user()->UserID;
                        $detail->UserModified = '0';
                        $detail->save();
                    }
                }
            }

//            //insert biaya kalau import
//            //perhitungan biaya ga ada masihan
            if (Input::get('jenispo') == 'import') {
                for ($z = 0; $z < count(Input::get('cost')); $z++) {
                    $totalCostValue = str_replace(',', '', Input::get('totalcost')[$z]);

                    $ordercost = new PurchaseOrderCostDetail();
                    $ordercost->PurchaseOrderInternalID = $header->InternalID;
                    $ordercost->CostInternalID = Input::get('cost')[$z];
                    $ordercost->TotalCost = $totalCostValue;
                    $ordercost->UserRecord = Auth::user()->UserID;
                    $ordercost->UserModified = '0';
                    $ordercost->save();
                }
            }

            $messages = 'suksesInsert';
            $error = '';
        }
        if ($messages == 'suksesInsert') {
            return Redirect::route('purchaseOrderDetail', $header->PurchaseOrderID);
        }
        $purchaseOrder = $this->createID(0) . '.';
        return View::make('pembelianAdd.purchaseOrderNew')
                        ->withToogle('transaction')->withAktif('purchaseOrder')
                        ->withPurchaseorder($purchaseOrder)
                        ->withError($error)
                        ->withMessages($messages);
    }

    public function insertRepeatOrder($id) {
        //rule
        $jenis = Input::get('isCash');
        if ($jenis == '0') {
            $rule = array(
                'date' => 'required',
                'coa6' => 'required',
//                'remark' => 'required|max:1000',
//                'warehouse' => 'required',
                'currency' => 'required',
                'rate' => 'required',
                'inventory' => 'required',
                'uom' => 'required'
            );
            $longTerm = 0;
        } else {
            $rule = array(
                'date' => 'required',
                'longTerm' => 'required|integer',
                'coa6' => 'required',
//                'remark' => 'required|max:1000',
//                'warehouse' => 'required',
                'currency' => 'required',
                'rate' => 'required',
                'inventory' => 'required',
                'uom' => 'required'
            );
            $longTerm = Input::get('longTerm');
        }

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalInsert';
            $error = $validator->messages();
        } else {
            //valid
            //insert header
            $header = new PurchaseOrderHeader;
            $purchaseOrder = $this->createID(1) . '.';
            $date = explode('-', Input::get('date'));
            $yearDigit = substr($date[2], 2);
            $purchaseOrder .= $date[1] . $yearDigit . '.';
            $purchaseOrderNumber = PurchaseOrderHeader::getNextIDPurchaseOrder($purchaseOrder);
            $header->PurchaseOrderID = $purchaseOrderNumber;
            $header->PurchaseOrderDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $header->ACC6InternalID = Input::get('coa6');
            $header->LongTerm = $longTerm;
            $header->isCash = $jenis;
            $header->WarehouseInternalID = Input::get('warehouse');
            $currency = explode('---;---', Input::get('currency'));
            $header->CurrencyInternalID = $currency[0];
            $header->CurrencyRate = str_replace(',', '', Input::get('rate'));
            if (Input::get('vat') == '') {
                $header->VAT = '0';
            } else {
                $header->VAT = Input::get('vat');
            }
            $header->DiscountGlobal = str_replace(",", "", Input::get('DiscountGlobal'));
            $header->GrandTotal = Input::get('grandTotalValue');
            $header->UserRecord = Auth::user()->UserID;
            $header->CompanyInternalID = Auth::user()->Company->InternalID;
            $header->UserModified = '0';
            $header->Remark = Input::get('remark');
            $header->save();

            //insert detail
            $total = 0;
            for ($a = 0; $a < count(Input::get('inventory')); $a++) {
                $qtyValue = str_replace(',', '', Input::get('qty')[$a]);
                $priceValue = str_replace(',', '', Input::get('price')[$a]);
                $discValue = str_replace(',', '', Input::get('discountNominal')[$a]);
                $subTotal = ($priceValue * $qtyValue) - (($priceValue * $qtyValue) * Input::get('discount')[$a] / 100);
                $subTotal = $subTotal - ($subTotal * Input::get('discount1')[$a] / 100) - $discValue * $qtyValue;
                $total += $subTotal;
                if (Input::get('vat') == '1') {
                    $vatValue = $subTotal / 10;
                } else {
                    $vatValue = 0;
                }
                if ($qtyValue > 0) {
                    $detail = new PurchaseOrderDetail();
                    $detail->PurchaseOrderInternalID = $header->InternalID;
                    $detail->InventoryInternalID = Input::get('inventory')[$a];
                    $detail->UomInternalID = Input::get('uom')[$a];
                    $detail->Qty = $qtyValue;
                    $detail->Price = $priceValue;
                    $detail->Discount = Input::get('discount')[$a];
                    $detail->Discount1 = Input::get('discount1')[$a];
                    $detail->DiscountNominal = $discValue;
                    $detail->VAT = $vatValue;
                    $detail->SubTotal = $subTotal;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = '0';
                    $detail->save();
                }
            }

            $messages = 'suksesInsert';
            $error = '';
        }
        $id = Coa6::where('ACC6ID', $id)->where('Type', 's')->first()->InternalID;
        $detail = PurchaseOrderDetail::join('t_purchaseorder_header', 't_purchaseorder_header.InternalID', '=', 't_purchaseorder_detail.PurchaseOrderInternalID')
                        ->where('ACC6InternalID', $id)
                        ->select('InventoryInternalID')->distinct()->get();
        $purchaseOrder = $this->createID(0) . '.';
        return View::make('pembelianAdd.purchaseOrderRepeatOrder')
                        ->withDetail($detail)
                        ->withToogle('transaction')->withAktif('purchaseOrder')
                        ->withPurchaseorder($purchaseOrder)
                        ->withError($error)
                        ->withMessages($messages);
    }

    public function updatePurchaseOrder($id) {
        //tipe
        $headerUpdate = PurchaseOrderHeader::find($id);
        $detailUpdate = PurchaseOrderHeader::find($id)->purchaseOrderDetail()->get();
        $error = $messages = '';
        //rule
        if (Input::get('isCash') == '0') {
            $rule = array(
                'coa6' => 'required',
//                'remark' => 'required|max:1000',
//                'warehouse' => 'required',
                'currency' => 'required',
                'rate' => 'required'
            );
            $longTerm = 0;
        } else {
            $rule = array(
                'longTerm' => 'required|integer',
                'coa6' => 'required',
//                'remark' => 'required|max:1000',
//                'warehouse' => 'required',
                'currency' => 'required',
                'rate' => 'required'
            );
            $longTerm = Input::get('longTerm');
        }

        if (!Input::hasFile('importorder')) {
            $rule += ['inventory' => 'required', 'uom' => 'required'];
        } else {
            //$rule += ['importorder' => 'mimes:xls,xlsx,csv'];
        }

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalUpdate';
            $error = $validator->messages();
            dd($error);
        } else {
            $header = PurchaseOrderHeader::find(Input::get('PurchaseOrderID'));
            $header->ACC6InternalID = Input::get('coa6');
            $header->isCash = Input::get('isCash');
            $header->LongTerm = $longTerm;
            $header->WarehouseInternalID = Input::get('warehouse');
            $currency = explode('---;---', Input::get('currency'));
            $header->CurrencyInternalID = $currency[0];
            $header->CurrencyRate = str_replace(',', '', Input::get('rate'));
            if (Input::get('vat') == '') {
                $header->VAT = '0';
            } else {
                $header->VAT = Input::get('vat');
            }
            if (Input::get('jenispo') == 'import') {
                $header->Import = 1;
            } else {
                $header->Import = 0;
            }
            if (Input::get('jenisDP') == 0){
                $header->DownPayment = str_replace(",", "", Input::get('downPayment'));
                $header->DPValue = 0;
            }
            else{
                $header->DPValue = str_replace(",", "", Input::get('downPaymentValue'));
                $header->DownPayment = 0;
            }
            $header->DiscountGlobal = str_replace(",", "", Input::get('DiscountGlobal'));
            $header->GrandTotal = Input::get('grandTotalValue');
            $header->Attn = Input::get('attn');
            $header->KirimVia = Input::get('kirimvia');
            $header->RequestBy = Input::get('requestby');
            $header->UserModified = Auth::user()->UserID;
            $header->Remark = Input::get('remark');
            $header->save();

            //delete purchaseOrder detail -- nantinya insert ulang
            PurchaseOrderDetail::where('PurchaseOrderInternalID', '=', Input::get('PurchaseOrderID'))->update(array('is_deleted' => 1));
            PurchaseOrderCostDetail::where('PurchaseOrderInternalID', '=', Input::get('PurchaseOrderID'))->update(array('is_deleted' => 1));
            $total = 0;
            if (Input::hasFile('importorder')) {
                Excel::load(Input::file('importorder'), function($reader) {
                    //$filename = $_FILES['importorder']['name'];
                    //$codeID = explode(' ', $filename)[0];
//                    $result = $reader->select(array('code', 'cat', 'model_no', 'desc', 'de',
//                                'j5', 'ae37', 'af43', 'mt', 'dt', 'brand', 'list'))->get();
                    $result = $reader->select(array('code', 'item_name', 'description', 'price', 'disc', 'qty', 'total'))->get();
                    //$header = PurchaseOrderHeader::orderBy('InternalID', 'desc')->first();

                    $header = PurchaseOrderHeader::find(Input::get('PurchaseOrderID'));
                    //dd($header);
//                    echo "<pre>";
//                    print_r($result);
//                    echo "</pre>";
//                    exit();
                    $total = 0;
                    foreach ($result as $row) {
                        if ($row['qty'] != '-' && $row['qty'] > 0) {
//                            $codehuruf = preg_replace('/[0-9]/', '', $codeID);
//                            $codeangka = str_pad((int) $row['code'], 4, '0', STR_PAD_LEFT);
//                            $code = $codehuruf . $codeangka;
//                            dd($row['pricelist']);
                            if (Inventory::where('InventoryID', $row['code'])->count() < 1)
                                break;
                            $inventory = Inventory::where('InventoryID', $row['code'])->first()->InternalID;
                            $uom = InventoryUom::where('InventoryInternalID', $inventory)->where('Default', 1)->first()->UomInternalID;
//                            $price = ;
//                            $warehouse = Warehouse::find($header->WarehouseInternalID);
//                            if ($warehouse->WarehouseID == 'DE')
//                                $qty = $row['de'];
//                            else if ($warehouse->WarehouseID == 'J5')
//                                $qty = $row['j5'];
//                            else if ($warehouse->WarehouseID == 'AE37')
//                                $qty = $row['ae37'];
//                            else if ($warehouse->WarehouseID == 'AF43')
//                                $qty = $row['af43'];
//                            else if ($warehouse->WarehouseID == 'MT')
//                                $qty = $row['mt'];
//                            else if ($warehouse->WarehouseID == 'DT')
//                                $qty = $row['dt'];
//                            else
//                                $qty = 0;
//                            if ($qty > 0) {
                            $detail = new PurchaseOrderDetail();
                            $detail->PurchaseOrderInternalID = $header->InternalID;
                            $detail->InventoryInternalID = $inventory;
                            $detail->UomInternalID = $uom;
                            $detail->Qty = $row['qty'];
                            $detail->Price = str_replace(",", "", $row['price']);
                            $detail->Discount = str_replace("%", "", $row['disc']) * 100;
                            $detail->Discount1 = 0;
                            $detail->DiscountNominal = 0;
                            $subtotal = $row['qty'] * str_replace(",", "", $row['price']) * (1 - str_replace(",", "", $row['disc']));
                            if (Input::get('vat') == '1') {
                                $vatValue = $subtotal / 10;
                            } else {
                                $vatValue = 0;
                            }
                            $detail->VAT = $vatValue;
                            $detail->SubTotal = $subtotal;
                            $detail->UserRecord = Auth::user()->UserID;
                            $detail->UserModified = '0';
                            $detail->save();
//                            }
                            $total += $subtotal;
                        }
                    }
                    $header->GrandTotal = $total;
                    $header->save();
                });
            } else {

                //insert detail

                for ($a = 0; $a < count(Input::get('inventory')); $a++) {
                    $qtyValue = str_replace(',', '', Input::get('qty')[$a]);
                    $priceValue = str_replace(',', '', Input::get('price')[$a]);
                    $discValue = str_replace(',', '', Input::get('discountNominal')[$a]);
                    $subTotal = ($priceValue * $qtyValue) - (($priceValue * $qtyValue) * Input::get('discount')[$a] / 100);
                    $subTotal = $subTotal - ($subTotal * Input::get('discount1')[$a] / 100) - $discValue * $qtyValue;
                    $subTotal = number_format($subTotal, 2, '.', '');
                    $total += $subTotal;
                    if (Input::get('vat') == '1') {
                        $vatValue = $subTotal / 10;
                    } else {
                        $vatValue = 0;
                    }
                    if ($qtyValue > 0) {
                        $detail = new PurchaseOrderDetail();
                        $detail->PurchaseOrderInternalID = Input::get('PurchaseOrderID');
                        $detail->InventoryInternalID = Input::get('inventory')[$a];
                        $detail->UomInternalID = Input::get('uom')[$a];
                        $detail->Qty = $qtyValue;
                        $detail->Price = $priceValue;
                        $detail->Discount = Input::get('discount')[$a];
                        $detail->Discount1 = Input::get('discount1')[$a];
                        $detail->Remark = Input::get('spesifikasi')[$a];
                        $detail->DiscountNominal = $discValue;
                        $detail->VAT = $vatValue;
                        $detail->SubTotal = $subTotal;
                        $detail->UserRecord = Auth::user()->UserID;
                        $detail->UserModified = Auth::user()->UserID;
                        $detail->save();
                    }
                }

                if ($header->Import == 1) {
                    for ($z = 0; $z < count(Input::get('cost')); $z++) {
                        $totalCostValue = str_replace(',', '', Input::get('totalcost')[$z]);

                        $ordercost = new PurchaseOrderCostDetail();
                        $ordercost->PurchaseOrderInternalID = $header->InternalID;
                        $ordercost->CostInternalID = Input::get('cost')[$z];
                        $ordercost->TotalCost = $totalCostValue;
                        $ordercost->UserRecord = Auth::user()->UserID;
                        $ordercost->UserModified = '0';
                        $ordercost->save();
                    }
                }
            }
            PurchaseOrderDetail::where('PurchaseOrderInternalID', '=', Input::get('PurchaseOrderID'))->where('is_deleted', 1)->delete();
            PurchaseOrderCostDetail::where('PurchaseOrderInternalID', '=', Input::get('PurchaseOrderID'))->where('is_deleted', 1)->delete();
            $messages = 'suksesUpdate';
            $error = '';
        }
        
        if ($messages == 'suksesUpdate') {
            return Redirect::route('purchaseOrderDetail', $header->PurchaseOrderID);
        }

        //tipe
        $header = PurchaseOrderHeader::find($id);
        $detail = PurchaseOrderHeader::find($id)->purchaseOrderDetail()->get();
        $purchaseOrder = $this->createID(0) . '.';
        return View::make('pembelianAdd.purchaseOrderUpdate')
                        ->withToogle('transaction')->withAktif('purchaseOrder')
                        ->withHeader($header)
                        ->withDetail($detail)
                        ->withPurchaseorder($purchaseOrder)
                        ->withError($error)
                        ->withMessages($messages);
    }

    public function deletePurchaseOrder() {
        $purchaseOrder = DB::select(DB::raw('SELECT * FROM t_mrv_header WHERE CompanyInternalID = "' . Auth::user()->CompanyInternalID . '" AND PurchaseOrderInternalID = "' . Input::get('InternalID') . '"'));
        $topup = DB::select(DB::raw('SELECT * FROM m_topup WHERE CompanyInternalID = "' . Auth::user()->CompanyInternalID . '" AND PurchaseOrderInternalID = "' . Input::get('InternalID') . '"'));
        if (is_null($purchaseOrder) == '' && is_null($topup) == '') {
            //tidak ada yang menggunakan data purchaseOrder maka data boleh dihapus
            $purchaseOrderHeader = PurchaseOrderHeader::find(Input::get('InternalID'));
            if ($purchaseOrderHeader->CompanyInternalID == Auth::user()->Company->InternalID) {
                //hapus detil
                $detilData = PurchaseOrderHeader::find(Input::get('InternalID'))->purchaseOrderDetail;
                foreach ($detilData as $value) {
                    $detil = purchaseOrderDetail::find($value->InternalID);
                    $detil->delete();
                }
                PurchaseOrderCostDetail::where('PurchaseOrderInternalID', $purchaseOrderHeader->InternalID)->delete();
                //hapus purchaseOrder
                $purchaseOrder = PurchaseOrderHeader::find(Input::get('InternalID'));
                $purchaseOrder->delete();
                $messages = 'suksesDelete';
            } else {
                $messages = 'accessDenied';
            }
        } else {
            //ada maka data tidak boleh dihapus
            $messages = 'gagalDelete';
        }
        $data = PurchaseOrderHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
        return View::make('pembelianAdd.purchaseOrderSearch')
                        ->withToogle('transaction')->withAktif('purchaseOrder')
                        ->withMessages($messages)
                        ->withData($data);
    }

    public function closePurchaseOrder() {
        $purchaseOrderHeader = PurchaseOrderHeader::find(Input::get('InternalID'));
        if ($purchaseOrderHeader->CompanyInternalID == Auth::user()->Company->InternalID) {
            $purchaseOrderHeader->Closed = 1;
            $purchaseOrderHeader->save();
            $messages = 'suksesUpdate';
        } else {
            $messages = 'accessDenied';
        }
        $data = PurchaseOrderHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
        return View::make('pembelianAdd.purchaseOrderSearch')
                        ->withToogle('transaction')->withAktif('purchaseOrder')
                        ->withMessages($messages)
                        ->withData($data);
    }

    function purchaseOrderPrint($id) {
        $id = PurchaseOrderHeader::getIdpurchaseOrder($id);
        $header = PurchaseOrderHeader::find($id);
        $detail = PurchaseOrderHeader::find($id)->purchaseOrderDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $coa6 = PurchaseOrderHeader::find($header->InternalID)->coa6;
            //$supplier = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . '<br> ' . $coa6->Address . '<br>Phone: ' . $coa6->Phone . '<br> Fax: ' . $coa6->Fax;
            $supplier = $coa6->ACC6Name;
            if ($header->isCash == 0) {
                $payment = 'Cash';
            } else if ($header->isCash == 1) {
                $payment = 'Credit';
            } else if ($header->isCash == 2) {
                $payment = 'CBD';
            } else if ($header->isCash == 3) {
                $payment = 'Deposit';
            } else {
                $payment = 'Down Payment';
            }
            $currency = Currency::find($header->CurrencyInternalID);
            $currencyName = $currency->CurrencyName;
            if ($header->VAT == 0) {
                $vat = 'Non Tax';
            } else {
                $vat = 'Tax';
            }
            $html = '
        <html>
            <head>
                <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 6px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box; position: relative;">
                    <div style="height: 195px;border-bottom:1px solid #ddd;margin-bottom:20px;">
                        <div style="height: 133px">
                        <img height="100%" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                        </div>
                        <div style="width: 420px; float: left;">
                            <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->Company->CompanyName . '</b>
                        </div> 
                        <div style=" box-sizing: border-box; margin-top: 0px;position: absolute; right: 2px; float: right;">
                             <table>
                             <tr style="background: none;">
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Address</td>
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                             </tr>
                            <tr style="background: none;">
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                             </tr>
                            <tr style="background: none;">
                                <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Phone Number</td>
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                             </tr>
                             </table>
                        </div>           
                    </div>
                    <h5 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Purchase Order</h5>
                    <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;padding-left:6px;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span><br>
                    <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                        <table>
                        <tr>
                        <td width="275px">
                            <table>
                             <tr style="background: none;">
                                <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Order ID</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->PurchaseOrderID . '</td>
                             </tr>
                            <tr style="background: none;">
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Date</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->PurchaseOrderDate)) . '</td>
                             </tr>
                            <tr style="background: none;">
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">Supplier</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">:</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $supplier . '</td>
                             </tr>';
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-') {
                $html .= '     <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Contact Person</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->ContactPerson . '</td>
                                 </tr>';
            }
            $html .= '<tr style="background: none;">
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Payment</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $payment . '</td>
                             </tr>
                             ';
            if ($header->isCash != 0) {
                $html .= '<tr style="background: none;">
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Due Date</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime("+" . $header->LongTerm . " day", strtotime($header->PurchaseOrderDate))) . '</td>
                    </tr>';
            }
            $html .= '
                            
                            </table>
                            </td>
                            <td>
                            <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Currency</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $currencyName . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Rate</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . number_format($header->CurrencyRate, '2', '.', ',') . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Vat</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $vat . '</td>
                                 </tr>
                            </table></td>
                            </tr>
                        </table>
                    </div>    
                        <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
                                <thead >
                                    <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="25%">Inventory</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="10%">Uom</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="10%">Qty</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="20%">Price</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="10%">Disc (%)</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="20%">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>';

            $total = 0;
            $totalVAT = 0;
            if (count($detail) > 0) {
                foreach ($detail as $data) {
                    $inventory = Inventory::find($data->InventoryInternalID);
                    $inv = $inventory->InventoryID . ' ' . $inventory->TextPrint;
                    $html .= '<tr>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $inv . '</td>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $data->Uom->UomID . '</td>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                        </tr>';
                    $totalVAT += $data->VAT;
                    $total += $data->SubTotal;
                }
                if ($totalVAT != 0) {
                    $totalVAT = $totalVAT - ($header->DiscountGlobal * 0.1);
                }
            } else {
                $html .= '<tr>
                        <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this purchaseOrder.</td>
                    </tr>';
            }
            $html .= '</tbody>
                        </table>
                    <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                        <table>
                        <tr>
                        <td width="275px" style="vertical-align: text-top;">
                            <table>
                            <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">Remark</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Remark . '</td>
                             </tr> 
                            </table>
                            </td>
                            <td width="200px" style="float:right;right:0px;">
                             <table style="margin-left:30px;">
                             <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Discount</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Grand Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total - $header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Tax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($totalVAT, '2', '.', ',') . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; ">Grand Total (Tax)</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">'
                    . number_format($header->GrandTotal, '2', '.', ',') . '</td>
                                 </tr>
                            </table></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </body>
        </html>';
//            return PDF::load($html, 'A5', 'potrait')->show();

            return View::make('template.print.purchaseOrderPrint')
                            ->with('header', $header)
                            ->with('supplier', $supplier)
                            ->with('vat', $vat)
                            ->with('id', $id)
                            ->with('currencyName', $currencyName)
                            ->with('payment', $payment)
                            ->with('detail', $detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showPurchaseOrder');
        }
    }

    function createID($tipe) {
        $purchaseOrder = 'PO';
        if ($tipe == 0) {
            $purchaseOrder .= '.' . date('m') . date('y');
        }
        return $purchaseOrder;
    }

    public function formatCariIDPurchaseOrder() {
        $date = explode('-', Input::get('date'));
        $id = $this->createID(1);
        $yearDigit = substr($date[2], 2);
        $id .= '.' . $date[1] . $yearDigit . '.';
        echo PurchaseOrderHeader::getNextIDPurchaseOrder($id);
    }

    public function summaryPurchaseOrder() {
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $supplier = Input::get('supplier');
        if ($supplier == '-1') {
            $tanda = '!=';
        } else {
            $tanda = '=';
        }
        $totalPO = 0;
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 100px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                               <!-- <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">-->
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Purchase Order Summary Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span>
                            <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span>';
        $hitung = 0;
        if ($supplier == '-1') {
            foreach (Coa6::where("Type", "s")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $dataSupplier) {
                if (PurchaseOrderHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                                ->where('ACC6InternalID', $dataSupplier->InternalID)
                                ->whereBetween('PurchaseOrderDate', Array($start, $end))->count() > 0) {
                    $html .= '<table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=7>' . $dataSupplier->ACC6Name . '</th>
                                        </tr>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Purchase Order ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Currency</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Rate</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total (After Discount)</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Vat</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Grand Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                    $sumGrandTotal = 0;
                    foreach (PurchaseOrderHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                            ->where('ACC6InternalID', $dataSupplier->InternalID)
                            ->whereBetween('PurchaseOrderDate', Array($start, $end))->get() as $data) {
                        $grandTotal = $data->GrandTotal;
                        $sumGrandTotal += $grandTotal;
                        $totalPO += $grandTotal;
                        $total = ceil($grandTotal);
                        $vat = 0;
                        if ($data->VAT == 1) {
                            $total = ceil($total * 10 / 11);
                            $vat = floor($total / 10);
                        }
                        $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->PurchaseOrderID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->PurchaseOrderDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->Currency->CurrencyName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->CurrencyRate, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($total, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($vat, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($grandTotal, '2', '.', ',') . '</td>
                            </tr>';
                    }
                    $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="80%" colspan="6">Total : </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . number_format($sumGrandTotal, '2', '.', ',') . '</td>
                        </tr>';

                    $html .= '</tbody>
            </table>';
                    $hitung++;
                }
            }
        } else {
            if (PurchaseOrderHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                            ->where('ACC6InternalID', $supplier)
                            ->whereBetween('PurchaseOrderDate', Array($start, $end))->count() > 0) {
                $html .= '<table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=7>' . Coa6::find($supplier)->ACC6Name . '</th>
                                        </tr>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Purchase Order ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Currency</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Rate</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total (After Discount)</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Vat</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Grand Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                $sumGrandTotal = 0;
                foreach (PurchaseOrderHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where('ACC6InternalID', $supplier)
                        ->whereBetween('PurchaseOrderDate', Array($start, $end))->get() as $data) {
                    $grandTotal = $data->GrandTotal;
                    $sumGrandTotal += $grandTotal;
                    $totalPO += $grandTotal;
                    $total = $grandTotal;
                    $vat = 0;
                    if ($data->VAT == 1) {
                        $total = $total * 10 / 11;
                        $vat = $total / 10;
                    }
                    $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->PurchaseOrderID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->PurchaseOrderDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->Currency->CurrencyName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->CurrencyRate, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($total, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($vat, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($grandTotal, '2', '.', ',') . '</td>
                            </tr>';
                }
                $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="80%" colspan="6">Total : </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . number_format($sumGrandTotal, '2', '.', ',') . '</td>
                        </tr>';

                $html .= '</tbody>
            </table>';
                $hitung++;
            }
        }

        if ($hitung == 0) {
            $html .= '<br><span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no purchase order.</span>';
        }
        $html .= '<table class="tableBorder" width="100%"  style="margin-top: 10px; clear: both;  top: 50px;">
                    <thead>
                        <tr>
                            <th style="font-family: helvetica,sans-serif;font-size: 14px; margin: 5px !important; font-weight: 700; text-align: right" width="80%" colspan="6">Total Purchase Order : </th>
                            <th style="font-family: helvetica,sans-serif;font-size: 14px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . number_format($totalPO, '2', '.', ',') . '</th>
                        </tr>
                    <thead>
                </table>';
        $html .= '</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('purchase_order_summary');
    }

    public function detailPurchaseOrder() {
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $supplier = Input::get('supplier');
        if ($supplier == '-1') {
            $tanda = '!=';
        } else {
            $tanda = '=';
        }
        $dateTamp = '';
        $coa6Tamp = '';
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 100px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <!--<img src = "' . substr(Auth::user()->Company->Logo, 1) . '">-->
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Purchase Order Detail Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span>'
                . '<span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span><br>';
        if ($supplier == '-1') {
            // $html .= '<span style="padding-left:23px;font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Supplier : All</span>';
        } else {
            $html .= '<span style="padding-left:23px;font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Supplier : ' . Coa6::find($supplier)->ACC6Name . '</span>';
        }
        $html .= '<br>';
        if (PurchaseOrderHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                        ->whereBetween('PurchaseOrderDate', Array($start, $end))
                        ->where('ACC6InternalID', $tanda, $supplier)
                        ->orderBy('PurchaseOrderDate')->orderBy('ACC6InternalID')->count() > 0) {
            foreach (PurchaseOrderHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                    ->whereBetween('PurchaseOrderDate', Array($start, $end))
                    ->where('ACC6InternalID', $tanda, $supplier)
                    ->orderBy('PurchaseOrderDate')->orderBy('ACC6InternalID')->get() as $dataPembelian) {
                if ($dateTamp != date("d-M-Y", strtotime($dataPembelian->PurchaseOrderDate))) {
                    $html .= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Purchase Order Date : ' . date("d-M-Y", strtotime($dataPembelian->PurchaseOrderDate)) . '</span><br>';
                    $dateTamp = date("d-M-Y", strtotime($dataPembelian->PurchaseOrderDate));
                    $coa6Tamp = '';
                }
                if ($supplier == '-1') {
                    $html .= '<span style="padding-left:23px;font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Supplier : ' . $dataPembelian->coa6->ACC6Name . '</span>';
                }
//                if ($dataPembelian->coa6->ContactPerson != '' && $dataPembelian->coa6->ContactPerson != '-' && $dataPembelian->coa6->ContactPerson != null) {
//                    $html .= '<span style="padding-left:23px;font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Contact Person : ' . $dataPembelian->coa6->ContactPerson . '</span>';
//                }
                $html .= '<table class="tableBorder" style="width:95%; margin-top: 18px; clear: both; position: relative; left: 4%">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=9>' . $dataPembelian->PurchaseOrderID . ' | ' . $dataPembelian->Currency->CurrencyName . ' | Rate : ' . number_format($dataPembelian->CurrencyRate, '2', '.', ',') . ' </th>
                                        </tr>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th width="20%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory ID</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Name</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Uom</th>
                                            <th width="10%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Qty</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Price</th>
                                            <th width="10%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc (%)</th>
                                            <th width="10%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc 2 (%)</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                $total = 0;
                $vat = 0;
                foreach ($dataPembelian->purchaseOrderDetail as $data) {
                    $total += ceil($data->SubTotal);
                        $vat += floor(ceil($data->SubTotal)/10);
//                    $vat += $data->VAT;
                    $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->inventory->InventoryID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->inventory->InventoryName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->Uom->UomID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Discount1 . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format(ceil($data->SubTotal), '2', '.', ',') . '</td>
                            </tr>';
                }
                if ($vat != 0) {
                    $vat = $vat - ($dataPembelian->DiscountGlobal * 0.1);
                }
                $html .= '<tr>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=5></td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=3>Total <br> Discount <br> Grand Total <br> VAT <br> Grand Total (Tax)</td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '<br>'
                        . '' . number_format($dataPembelian->DiscountGlobal, '2', '.', ',') . '<br>'
                        . '' . number_format($total - $dataPembelian->DiscountGlobal, '2', '.', ',') . '<br>'
                        . '' . number_format($vat, '2', '.', ',') . '<br>'
                        . '' . number_format($dataPembelian->GrandTotal, '2', '.', ',') . '</td>
                    </tr>
                </tbody>
            </table>';
            }
        } else {
            $html .= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no purchaseOrder.</span><br><br>';
        }
        $html .= '</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('purchase_order_detail');
    }

    public function outstandingPurchaseOrder() {
        Excel::create('Outstanding_Purchase_Order', function($excel) {
            $excel->sheet('Outstanding_Purchase_Order', function($sheet) {
                $sheet->mergeCells('B1:K1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Outstanding Purchase Order");
                $row = 3;

                $sheet->setCellValueByColumnAndRow(1, $row, 'Purchase Order ID');
                $sheet->setCellValueByColumnAndRow(2, $row, 'Purchase Order Date');
                $sheet->setCellValueByColumnAndRow(3, $row, 'Supplier');
                $sheet->setCellValueByColumnAndRow(4, $row, 'Product');
                $sheet->setCellValueByColumnAndRow(5, $row, 'UOM');
                $sheet->setCellValueByColumnAndRow(6, $row, 'Qty PO');
                $sheet->setCellValueByColumnAndRow(7, $row, 'MRV ID');
                $sheet->setCellValueByColumnAndRow(8, $row, 'MRV Date');
                $sheet->setCellValueByColumnAndRow(9, $row, 'Qty MRV');
                $sheet->setCellValueByColumnAndRow(10, $row, 'Outstanding Qty');
                $sheet->setBorder('B' . $row . ':K' . $row, 'thin');
                $sheet->cells('B' . $row . ':K' . $row, function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $row++;
                //ambil yg tidak di closed manual (PO nya jadi)
//                 $po = DB::select("SELECT t_mrv_header.*,`t_mrv_detail`.`Qty` as `Qty`, m_inventory.InternalID as InventoryID,m_inventory.`InventoryName` as `InventoryName`, 
//                    `m_uom`.`UomID` as `UomID`,`m_coa6`.`ACC6Name` as `SupplierName` FROM
//                                t_purchaseorder_header  inner join `t_purchaseorder_detail` on `t_purchaseorder_detail`.`PurchaseOrderInternalID` = `t_purchaseorder_header`.`InternalID` 
//                                inner join `m_coa6` on `m_coa6`.`InternalID` = `t_purchaseorder_header`.`ACC6InternalID`                              
//                                inner join `m_inventory` on `m_inventory`.`InternalID` = `t_purchaseorder_detail`.`InventoryInternalID` 
//                                inner join `m_uom` on `m_uom`.`InternalID` = `t_purchaseorder_detail`.`UomInternalID` 
//                                inner join t_mrv_header on t_mrv_header.PurchaseOrderInternalID = t_purchaseorder_header.InternalID 
//                                inner join t_mrv_detail on t_mrv_detail.MrvInternalID = t_mrv_header.InternalID and t_mrv_detail.InventoryInternalID = m_inventory.InternalID 
//where t_purchaseorde_header.Closed = 0 and sum(t_purchaseorder_detail.Qty)                               
//ORDER BY t_purchase_header.PurchaseID ASC");

                foreach (PurchaseOrderHeader::where('Closed', 0)->where('CompanyInternalID', Auth::user()->CompanyInternalID)->orderBy("PurchaseOrderID","asc")->get() as $po) {
                    //cek po nya yang belum slesai
                    if (checkMrv($po->InternalID)) {
                        $sheet->setCellValueByColumnAndRow(1, $row, $po->PurchaseOrderID);
                        $sheet->setCellValueByColumnAndRow(2, $row, date("d/m/Y", strtotime($po->PurchaseOrderDate)));
                        $sheet->setCellValueByColumnAndRow(3, $row, Coa6::find($po->ACC6InternalID)->ACC6Name);
                        $increment = 0;
                        foreach (PurchaseOrderDetail::where("PurchaseOrderInternalID", $po->InternalID)->get() as $pod) {
                            $rowbarang = $increment;
                            $totalbarang = 0;
                            $sheet->setCellValueByColumnAndRow(4, $row + $increment, "[" . Variety::find(Inventory::find($pod->InventoryInternalID)->VarietyInternalID)->VarietyName . '] ' . $pod->inventory->InventoryName . " (" . $pod->inventory->Power . ")");
                            $sheet->setCellValueByColumnAndRow(5, $row + $increment, Uom::find($pod->UomInternalID)->UomID);
                            $sheet->setCellValueByColumnAndRow(6, $row + $increment, $pod->Qty);
                            foreach (MrvDetail::where("PurchaseOrderDetailInternalID", $pod->InternalID)
                                    ->join("t_mrv_header","t_mrv_header.InternalID","=","t_mrv_detail.MrvInternalID")
                                    ->orderBy("MrvDate","asc")->get() as $md) {
                                $increment++;
                                $sheet->setCellValueByColumnAndRow(7, $row + $increment, MrvHeader::find($md->MrvInternalID)->MrvID);
                                $sheet->setCellValueByColumnAndRow(8, $row + $increment, date("d/m/Y", strtotime(MrvHeader::find($md->MrvInternalID)->MrvDate)));
                                $sheet->setCellValueByColumnAndRow(9, $row + $increment, $md->Qty);
                                $totalbarang += $md->Qty;
                            }
                            $sheet->setCellValueByColumnAndRow(9, $row + $rowbarang, $totalbarang);
                            $sheet->setCellValueByColumnAndRow(10, $row + $rowbarang, $pod->Qty - $totalbarang);
                            $increment++;
                        }
                        $row = $row + $increment + 1;
                    }
                }
//                    }
//                }
//                $row--;
//                $sheet->setBorder('B2:C' . $row, 'thin');
                $sheet->setBorder('B4' . ':K' . $row, 'thin');
                if ($row == 4) {
                    $sheet->mergeCells('B1:K1');
                    $sheet->setCellValueByColumnAndRow(1, 4, "Tidak ada barang yang belum terkirim");
                }
                $sheet->cells('B2:K2', function($cells) {
                    $cells->setFontWeight('bold');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
//                $sheet->cells('B3:G' . $row, function($cells) {
//                    $cells->setAlignment('left');
//                    $cells->setValignment('middle');
//                });
//                $sheet->cells('B3:B' . $row, function($cells) {
//                    $cells->setAlignment('center');
//                });
            });
        })->export('xls');
    }

    //===============================ajax====================================
    public function getUomThisInventoryPurchaseOrder() {
        $inventoryUom = InventoryUom::where("InventoryInternalID", Input::get("id"))->get();
        foreach ($inventoryUom as $data) {
            ?>
            <option value="<?php echo $data->UomInternalID ?>" <?php echo ($data->Default == 1 ? "selected" : "") ?>><?php echo $data->Uom->UomID; ?></option>
            <?php
        }
    }

    public function getPriceUomThisInventoryPO() {
        $inventoryInternalID = Input::get("InventoryID");
        $uomInternalID = Input::get("UomID");
        $InventoryPrice = InventoryUom::where("InventoryInternalID", $inventoryInternalID)
                        ->where("UomInternalID", $uomInternalID)->first();
        return $InventoryPrice->Price . '---;---' . Input::get("urutan");
    }

    public function checkRecieveablePO() {
        $total = Input::get("total");
        $coa6 = Input::get("coa6");
        $sisaHutang = 0;
        $balance = 0;
        //pikirkan currency
        //hasil dari ini adalah headernya saja 
        $detail = PurchaseOrderHeader::getPurchaseReceivable($coa6);
        foreach ($detail as $data) {
            $balance += ($data->GrandTotal - JournalDetail::where('JournalTransactionID', $data->ID)->sum('JournalCreditMU')) * $data->CurrencyRate;
        }

        $creditLimitCustomer = Coa6::find($coa6)->CreditLimit;
        $sisaHutang = $creditLimitCustomer - $balance;

        if ($sisaHutang < $total) {
            return '1' . '---;---' . $creditLimitCustomer . '---;---' . abs($sisaHutang);
        } else {
            return '0' . '---;---0---;---0';
        }
    }

    public function getSearchResultInventoryForPO() {
//        $input = splitSearchValue(Input::get("id"));
//        $dataInventory = Inventory::select('m_inventory.*')->distinct()
//                ->join('m_inventory_uom', 'm_inventory_uom.InventoryInternalID', '=', 'm_inventory.InternalID')
//                ->where("m_inventory.Status", 0)
//                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->where("m_inventory.InventoryName", "like", $input)
//                ->orWhere("m_inventory.InventoryID", "like", $input)
//                ->where("m_inventory.Status", 0)
//                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->get();
        $idnih = "%" . explode(" ", Input::get("id"))[0] . "%";
//        dd($idnih);
        $pass = str_replace(explode(" ", Input::get("id"))[0], '', Input::get('id'));
//        dd($pass);
        $input = splitSearchValue($pass);
//        $input = splitSearchValue(Input::get("id"));
        $dataInventory = Inventory::select('m_inventory.*')->distinct()
                ->join('m_inventory_uom', 'm_inventory_uom.InventoryInternalID', '=', 'm_inventory.InternalID')
                ->where("m_inventory.Status", 0)
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where(function($query) use ($idnih, $input) {
                    $query->where("m_inventory.InventoryID", "like", $idnih)
                    ->where("m_inventory.InventoryName", "like", $input)
                    ->orWhere("m_inventory.InventoryName", "like", $idnih);
                })
                ->where("m_inventory.Status", 0)
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->get();

        if (count($dataInventory) == 0) {
            ?>
            <span>Inventory with keywords <i><u><?php echo Input::get("id"); ?></u></i> can't be found</span>
            <?php
        } else {
            ?>
            <select class="input-theme left inventory" id="inventory-0">
                <?php
                foreach ($dataInventory as $inventory) {
                    ?>
                    <option id="inventory<?php echo $inventory->InternalID ?>" value="<?php echo $inventory->InternalID ?>---;---inventory">
                        <?php echo $inventory->InventoryID . ' ' . $inventory->InventoryName . ' (power: ' . $inventory->Power . ')'; ?>
                    </option>
                    <?php
                }
                ?>
            </select>
            <script type="text/javascript" src="<?php echo Asset('js/entry-js-controller/controllerPO.js') ?>"></script>
            <?php
        }
    }

    public function getPriceRangeThisInventoryPO() {
        $hasil = getPriceRangeInventory(Input::get("InventoryID"), Input::get("UomID"), Input::get("Qty"));
        return $hasil . '---;---' . Input::get("urutan");
    }

    //===============================ajax====================================


    static function purchaseOrderDataBackup($data) {
        $explode = explode('---;---', $data);
        $typePayment = $explode[0];
        $typeTax = $explode[1];
        $start = $explode[2];
        $end = $explode[3];
        $where = '';
        if ($typePayment != '-1' && $typePayment != '') {
            $where .= 'isCash = "' . $typePayment . '" ';
        }
        if ($typeTax != '-1' && $typeTax != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'VAT = "' . $typeTax . '" ';
        }
        if ($start != '' && $end != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $start = explode('-', $start);
            $end = explode('-', $end);
            $startSearch = $start[2] . '-' . $start[1] . '-' . $start[0];
            $endSearch = $end[2] . '-' . $end[1] . '-' . $end[0];
            $where .= 'PurchaseOrderDate between "' . $startSearch . ' 00:00:00" AND "' . $endSearch . ' 23:59:59"';
        }
        $table = 't_purchaseorder_header';
        $primaryKey = 't_purchaseorder_header`.`InternalID';
        $columns = array(
            array('db' => 't_purchaseorder_header`.`InternalID', 'dt' => 0, 'formatter' => function($d, $row) {
                    return $d;
                }),
            array('db' => 'PurchaseOrderID', 'dt' => 1),
            array('db' => 'isCash', 'dt' => 2, 'formatter' => function( $d, $row ) {
                    if ($d == 0) {
                        return 'Cash';
                    } else if ($d == 1) {
                        return 'Credit';
                    } else if ($d == 2) {
                        return 'CBD';
                    } else if ($d == 3) {
                        return 'Deposit';
                    } else {
                        return 'Down Payment';
                    }
                },
                'field' => 't_purchaseorder_header`.`InternalID'),
            array(
                'db' => 'PurchaseOrderDate',
                'dt' => 3,
                'formatter' => function( $d, $row ) {
                    return date("d-m-Y", strtotime($d));
                }
            ),
            array('db' => 'CurrencyName', 'dt' => 4),
            array(
                'db' => 'CurrencyRate',
                'dt' => 5,
                'formatter' => function( $d, $row ) {
                    return number_format($d, '2', '.', ',');
                }
            ),
            array(
                'db' => 'ACC6Name',
                'dt' => 6,
                'formatter' => function( $d, $row ) {
                    return $d;
                }
            ),
            array(
                'db' => 'VAT',
                'dt' => 7,
                'formatter' => function( $d, $row ) {
                    if ($d == 0) {
                        return 'Non Tax';
                    } else {
                        return 'Tax';
                    }
                }
            ),
            array(
                'db' => 'GrandTotal',
                'dt' => 8,
                'formatter' => function( $d, $row ) {
                    return number_format($d, '2', '.', ',');
                }
            ),
            array(
                'db' => 'Import',
                'dt' => 9,
                'formatter' => function( $d, $row ) {
                    if ($d == 0) {
                        return 'Local';
                    } else {
                        return 'Import';
                    }
                }
            ),
            array(
                'db' => 't_purchaseorder_header`.`InternalID',
                'dt' => 10,
                'formatter' => function( $d, $row ) {
                    return (checkMrv($d) == true && PurchaseOrderHeader::find($d)->Closed == 0 ? 'Undone' : 'Done');
                }
            ),
            array('db' => 't_purchaseorder_header`.`InternalID', 'dt' => 11, 'formatter' => function( $d, $row ) {
                    $data = PurchaseOrderHeader::find($d);
                    $action = '<td class="text-center">
                                    <a href="' . Route('purchaseOrderDetail', $data->PurchaseOrderID) . '">
                                        <button id="btn-' . $data->PurchaseOrderID . '-detail"
                                                class="btn btn-pure-xs btn-xs btn-detail" title="detail">
                                            <span class="glyphicon glyphicon-zoom-in"></span>
                                        </button>
                                    </a>';
                    if (!PurchaseOrderHeader::isMrv($data->InternalID)) {
                        $action .= '<a href="' . Route('purchaseOrderUpdate', $data->PurchaseOrderID) . '">
                                        <button id="btn-' . $data->PurchaseOrderID . '-update"
                                                class="btn btn-pure-xs btn-xs btn-edit" title="update">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </button>
                                    </a>
                                    <button data-target="#m_purchaseOrderDelete" data-internal="' . $data->InternalID . '"  data-toggle="modal" role="dialog"
                                           onclick="deleteAttach(this)" data-id="' . $data->PurchaseOrderID . '" data-name=' . $data->PurchaseOrderID . ' class="btn btn-pure-xs btn-xs btn-delete" title="delete">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>';
                    } else {
                        $action .= '<button disabled class="btn btn-pure-xs btn-xs btn-edit"><span class="glyphicon glyphicon-edit"></span></button>
                                    <button disabled class="btn btn-pure-xs btn-xs btn-delete"><span class="glyphicon glyphicon-trash"></span></button>';
                    }
                    if (checkMrv($d) == true && $data->Closed == 0) {
                        if ((checkAmountPurchase($data->InternalID) <= 0 && $data->isCash == 2) || ($data->isCash == 4 && checkDpMrv($data->InternalID)) || ($data->isCash != 2 && $data->isCash != 4)) {
                            $action .= '<a href="' . Route('mrvNew', $data->PurchaseOrderID) . '" target="_blank">
                                        <button id="btn-' . $data->PurchaseOrderID . '-mrv"
                                                class="btn btn-pure-xs btn-xs" title="mrv">
                                            <b>MRV</b>
                                        </button>
                                    </a>';
                        } else {
                            $action .= '<button disabled class="btn btn-pure-xs btn-xs btn-closed" title="mrv">MRV</button>';
                        }
                    } else {
                        $action .= '<button disabled class="btn btn-pure-xs btn-xs btn-closed" title="mrv">MRV</button>';
                    }
                    if (PurchaseOrderHeader::find($d)->Closed == 0) {
                        $action .= '<button data-target="#m_purchaseOrderClose" data-internal="' . $data->InternalID . '"  data-toggle="modal" role="dialog" title="close"
                                           onclick="closeAttach(this)" data-id="' . $data->PurchaseOrderID . '" data-name=' . $data->PurchaseOrderID . ' class="btn btn-pure-xs btn-xs btn-closed">
                                        <span class="glyphicon glyphicon-lock"></span>
                                    </button>';
                    } else {
                        $action .= '<button disabled class="btn btn-pure-xs btn-xs btn-closed" title="close"><span class="glyphicon glyphicon-lock"></span></button>';
                    }
                    return $action;
                },
                'field' => 't_purchaseorder_header`.`InternalID')
        );

        $sql_details = getConnection();

        require('ssp.class.php');
        $ID_CLIENT_VALUE = Auth::user()->CompanyInternalID;
        if ($where != '') {
            $extraCondition = $where . ' AND ' . 't_purchaseorder_header.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        } else {
            $extraCondition = 't_purchaseorder_header.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        }
        $join = ' INNER JOIN m_currency on m_currency.InternalID = t_purchaseorder_header.CurrencyInternalID '
                . 'INNER JOIN m_coa6 on m_coa6.InternalID = t_purchaseorder_header.ACC6InternalID';

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $extraCondition, $join));
    }

}
