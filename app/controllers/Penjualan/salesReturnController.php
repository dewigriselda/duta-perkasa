<?php

class SalesReturnController extends BaseController {

    public function showSalesReturn() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteSalesReturn') {
                return $this->deleteSalesReturn();
            } else if (Input::get('jenis') == 'summarySales') {
                return $this->summarySalesReturn();
            } else if (Input::get('jenis') == 'detailSales') {
                return $this->detailSalesReturn();
            } else if (Input::get('jenis') == 'insertSalesReturn') {
                return Redirect::Route('salesReturnNew', Input::get('sales'));
            } else if (Input::get('jenis') == 'printSalesReturn') {
                if (Input::get('type') == 'exclude') {
                    return Redirect::route('salesReturnPrint', Input::get('internalID'));
                } else {
                    return Redirect::route('salesReturnIncludePrint', Input::get('internalID'));
                }
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = SalesReturnHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
            return View::make('penjualan.salesReturnSearch')
                            ->withToogle('transaction')->withAktif('salesReturn')
                            ->withData($data);
        }
        return View::make('penjualan.salesReturn')
                        ->withToogle('transaction')->withAktif('salesReturn');
    }

    public function salesReturnNew($id) {
        $id = SalesHeader::getIdsales($id);
        $header = SalesHeader::find($id);
        $detail = SalesHeader::find($id)->salesDetail()->get();
        $description = SalesHeader::find($id)->salesDescription()->get();
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertSalesReturn') {
                return Redirect::Route('salesReturnNew', Input::get('sales'));
            } else if (Input::get('jenis') == 'summarySales') {
                return $this->summarySalesReturn();
            } else if (Input::get('jenis') == 'detailSales') {
                return $this->detailSalesReturn();
            } else if (Input::get('jenis') == 'deleteSalesReturn') {
                return $this->deleteSalesReturn();
            } else if (Input::get('jenis') == 'printSalesReturn') {
                if (Input::get('type') == 'exclude') {
                    return Redirect::route('salesReturnPrint', Input::get('internalID'));
                } else {
                    return Redirect::route('salesReturnIncludePrint', Input::get('internalID'));
                }
            } else {
                return $this->insertSalesReturn();
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = SalesReturnHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
            return View::make('penjualan.salesReturnSearch')
                            ->withToogle('transaction')->withAktif('salesReturn')
                            ->withData($data);
        }
        return View::make('penjualan.salesReturnNew')
                        ->withToogle('transaction')->withAktif('salesReturn')
                        ->withHeader($header)
                        ->withDescription($description)
                        ->withDetail($detail);
    }

    public function salesReturnDetail($id) {
        $id = SalesReturnHeader::getIdsalesReturn($id);
        $header = SalesReturnHeader::find($id);
        $detail = SalesReturnHeader::find($id)->salesReturnDetail()->get();
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertSalesReturn') {
                return Redirect::Route('salesReturnNew', Input::get('sales'));
            } else if (Input::get('jenis') == 'summarySales') {
                return $this->summarySalesReturn();
            } else if (Input::get('jenis') == 'detailSales') {
                return $this->detailSalesReturn();
            } else if (Input::get('jenis') == 'deleteSalesReturn') {
                return $this->deleteSalesReturn();
            } else if (Input::get('jenis') == 'printSalesReturn') {
                if (Input::get('type') == 'exclude') {
                    return Redirect::route('salesReturnPrint', Input::get('internalID'));
                } else {
                    return Redirect::route('salesReturnIncludePrint', Input::get('internalID'));
                }
            }
        }
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            return View::make('penjualan.salesReturnDetail')
                            ->withToogle('transaction')->withAktif('salesReturn')
                            ->withHeader($header)
                            ->withDetail($detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showSalesReturn');
        }
    }

    public function salesReturnUpdate($id) {
        $salesIDReal = substr($id, 7);
        $id = SalesReturnHeader::getIdsalesReturn($id);
        $header = SalesReturnHeader::find($id);
        $detail = SalesReturnHeader::find($id)->salesReturnDetail()->get();
        $description = SalesReturnHeader::find($id)->salesReturnDescription()->get();
        $idSales = SalesHeader::getIdsales($salesIDReal);
        $headerSales = SalesHeader::find($idSales);
        $detailSales = SalesHeader::find($idSales)->salesDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                if (Input::get('jenis') == 'insertSalesReturn') {
                    return Redirect::Route('salesReturnNew', Input::get('sales'));
                } else if (Input::get('jenis') == 'summarySales') {
                    return $this->summarySalesReturn();
                } else if (Input::get('jenis') == 'detailSales') {
                    return $this->detailSalesReturn();
                } else if (Input::get('jenis') == 'deleteSalesReturn') {
                    return $this->deleteSalesReturn();
                } else if (Input::get('jenis') == 'printSalesReturn') {
                    if (Input::get('type') == 'exclude') {
                        return Redirect::route('salesReturnPrint', Input::get('internalID'));
                    } else {
                        return Redirect::route('salesReturnIncludePrint', Input::get('internalID'));
                    }
                } else {
                    return $this->updateSalesReturn($id);
                }
            }
            return View::make('penjualan.salesReturnUpdate')
                            ->withToogle('transaction')->withAktif('salesReturn')
                            ->withHeader($header)
                            ->withDetail($detail)
                            ->withDescription($description)
                            ->withHeadersales($headerSales)
                            ->withDetailsales($detailSales);
        } else {
            $messages = 'accessDenied';

            Session::flash('messages', $messages);
            return Redirect::Route('showSalesReturn');
        }
    }

    public function insertSalesReturn() {
        //rule
        $rule = array(
            'date' => 'required',
            'warehouse' => 'required',
            'remark' => 'required|max:1000',
            'InternalDescription' => 'required'
        );
        $salesNumber = '';
        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalInsert';
            $error = $validator->messages();
        } else {
            $id = Input::get('SalesInternalID');
            $header = SalesHeader::find($id);
            $date = explode('-', Input::get('date'));
            //insert header
            $headerI = new SalesReturnHeader;
            $salesReturnNumber = SalesReturnHeader::getNextIDSalesReturn($header->SalesID);
            $headerI->SalesReturnID = $salesReturnNumber;
            $headerI->SalesReturnDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $headerI->ACC6InternalID = $header->ACC6InternalID;
            $headerI->LongTerm = $header->LongTerm;
            $headerI->isCash = $header->isCash;
            $headerI->CurrencyInternalID = $header->CurrencyInternalID;
            $headerI->WarehouseInternalID = Input::get('warehouse');
            $headerI->CurrencyRate = $header->CurrencyRate;
            $headerI->VAT = $header->VAT;
            $headerI->DiscountGlobal = str_replace(",", "", Input::get('DiscountGlobal'));
            $headerI->GrandTotal = Input::get('grandTotalValue');
            $headerI->Print = 1;
            $headerI->UserRecord = Auth::user()->UserID;
            $headerI->CompanyInternalID = Auth::user()->Company->InternalID;
            $headerI->UserModified = '0';
            $headerI->Remark = Input::get('remark');
            $headerI->save();
            //insert detail
            $total = 0;

            for ($a = 0; $a < count(Input::get('InternalDescription')); $a++) {
                if (Input::get('qtyDescription')[$a] > 0) {
                    $data = SalesAddDescription::find(Input::get('InternalDescription')[$a]);
                    $description = new SalesReturnDescription();
                    $description->SalesReturnInternalID = $headerI->InternalID;
                    $description->InventoryText = $data->InventoryText;
                    $description->UomText = $data->UomText;
                    $description->Price = $data->Price;
                    $description->DiscountNominal = $data->DiscountNominal;
                    $description->Discount = $data->Discount;
                    $qty = 0;
                    if (Input::get('max')[$a] < Input::get('qtyDescription')[$a])
                        $qty = Input::get('max')[$a];
                    else
                        $qty = Input::get('qtyDescription')[$a];
                    $priceValue = $data->Price;
                    $discValue = $data->DiscountNominal;
                    $subTotal = ($priceValue * $qty ) - (($priceValue * $qty) * $data->Discount / 100) - $discValue * $qty;
                    if ($header->VAT == '1') {
                        $vatValue = $subTotal / 10;
                    } else {
                        $vatValue = 0;
                    }
                    $description->VAT = $vatValue;
                    $description->Qty = $qty;
                    $description->SubTotal = $qty / $data->Qty * $data->SubTotal;
                    $description->Spesifikasi = $data->Spesifikasi;
                    $description->SalesDescriptionInternalID = Input::get('InternalDescription')[$a];
                    $description->UserRecord = Auth::user()->UserID;
                    $description->UserModified = '0';
                    $description->save();

                    for ($c = 0; $c < count(Input::get('InternalDetail' . ($a + 1))); $c++) {
                        if (Input::get('tipe' . ($a + 1))[$c] == "inventory") {
                            $detail = SalesAddDetail::find(Input::get('InternalDetail' . ($a + 1))[$c]);
                            //jika non paket
                            $qtyValue = $detail->Qty;
                            $qtyReturnValue = str_replace(',', '', Input::get('qty' . ($a + 1))[$c]);
                            $sumReturn = SalesReturnHeader::getSumReturn($detail->InventoryInternalID, $header->SalesID, $detail->InternalID);
                            $qtyValue = $qtyValue - $sumReturn;
                            if ($qtyValue < $qtyReturnValue) {
                                $qtyReturnValue = $qtyValue;
                            }
                            $priceValue = $detail->Price;
                            $discValue = $detail->DiscountNominal;
                            $subTotal = ($priceValue * $qtyReturnValue ) - (($priceValue * $qtyReturnValue) * $detail->Discount / 100) - $discValue * $qtyReturnValue;
                            if ($header->VAT == '1') {
                                $vatValue = $subTotal / 10;
                            } else {
                                $vatValue = 0;
                            }
                            if ($qtyReturnValue > 0) {
                                $detail2 = new SalesReturnDetail();
                                $detail2->SalesReturnInternalID = $headerI->InternalID;
                                $detail2->InventoryInternalID = $detail->InventoryInternalID;
                                $detail2->DescriptionInternalID = $description->InternalID;
                                $detail2->UomInternalID = $detail->UomInternalID;
                                $detail2->Qty = $qtyReturnValue;
                                $detail2->Price = $priceValue;
                                $detail2->Discount = $detail->Discount;
                                $detail2->DiscountNominal = $discValue;
                                $detail2->VAT = $vatValue;
                                $detail2->SubTotal = $subTotal;
                                $detail2->UserRecord = Auth::user()->UserID;
                                $detail2->UserModified = '0';
                                $detail2->SalesDetailInternalID = Input::get('InternalDetail' . ($a + 1))[$c];
                                $detail2->save();
                                setTampInventory($detail->InventoryInternalID);
                            }
                            $total += $subTotal;
                        } else {
                            $detail = SalesAddParcel::find(Input::get("InternalDetail" . ($a + 1))[$c]);
                            //jika paket

                            $parcelDetail = ParcelInventory::where("ParcelInternalID", $detail->ParcelInternalID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->get();
                            $qtyValue = $detail->Qty;
                            $qtyReturnValue = str_replace(',', '', Input::get('qty' . ($a + 1))[$c]);
                            $sumReturn = SalesReturnHeader::getSumReturnParcel($detail->ParcelInternalID, $header->SalesID, $detail->InternalID);
                            $qtyValue = $qtyValue - $sumReturn;
                            if ($qtyValue < $qtyReturnValue) {
                                $qtyReturnValue = $qtyValue;
                            }
                            $priceValue = $detail->Price;
                            $discValue = $detail->DiscountNominal;
                            $subTotal = ($priceValue * $qtyReturnValue ) - (($priceValue * $qtyReturnValue) * $detail->Discount / 100) - $discValue * $qtyReturnValue;

                            if ($header->VAT == '1') {
                                $vatValue = $subTotal / 10;
                            } else {
                                $vatValue = 0;
                            }
                            if ($qtyReturnValue > 0) {
                                $SAParcel = new SalesReturnParcel();
                                $SAParcel->SalesReturnInternalID = $headerI->InternalID;
                                $SAParcel->ParcelInternalID = $detail->ParcelInternalID;
                                $SAParcel->DescriptionInternalID = $description->InternalID;
                                $SAParcel->SalesParcelDetailInternalID = Input::get("InternalDetail" . ($a + 1))[$c];
                                $SAParcel->Qty = $qtyReturnValue;
                                $SAParcel->Price = $priceValue;
                                $SAParcel->Discount = $detail->Discount;
                                $SAParcel->VAT = $vatValue;
                                $SAParcel->DiscountNominal = $discValue;
                                $SAParcel->SubTotal = $subTotal;
                                $SAParcel->CompanyInternalID = Auth::user()->Company->InternalID;
                                $SAParcel->UserRecord = Auth::user()->UserID;
                                $SAParcel->UserModified = "0";
                                $SAParcel->Remark = "-";
                                $SAParcel->save();

                                foreach ($parcelDetail as $dataParcel) {
                                    $uom1 = $dataParcel->UomInternalID;
                                    $qtyValue1 = $dataParcel->Qty * $qtyReturnValue;
                                    $priceValue1 = $dataParcel->Price;
                                    $subTotal1 = ($priceValue1 * $qtyValue1);
                                    if (Input::get('vat') == '1') {
                                        $vatValue1 = $subTotal1 / 10;
                                    } else {
                                        $vatValue1 = 0;
                                    }
                                    $detail2 = new SalesReturnDetail();
                                    $detail2->SalesReturnInternalID = $headerI->InternalID;
                                    $detail2->InventoryInternalID = $dataParcel->InventoryInternalID;
                                    $detail2->UomInternalID = $uom1;
                                    $detail2->SalesReturnParcelInternalID = $SAParcel->InternalID;
                                    $detail2->DescriptionInternalID = $description->InternalID;
                                    $detail2->Qty = $qtyValue1;
                                    $detail2->Price = $priceValue1;
                                    $detail2->Discount = 0;
                                    $detail2->DiscountNominal = 0;
                                    $detail2->VAT = $vatValue1;
                                    $detail2->SubTotal = $subTotal1;
                                    $detail2->SalesDetailInternalID = $header->InternalID;
                                    $detail2->UserRecord = Auth::user()->UserID;
                                    $detail2->UserModified = '0';
                                    $detail2->save();
                                    setTampInventory($dataParcel->InventoryInternalID);
                                }
                            }
                            $total += $subTotal;
                        }
                    }
                }
            }
            if ($headerI->VAT == '1') {
                $vatValueHeader = ($total - str_replace(",", "", Input::get("DiscountGlobal"))) / 10;
            } else {
                $vatValueHeader = 0;
            }
//            $headerI->GrandTotal = $total - str_replace(",", "", Input::get("DiscountGlobal")) + $vatValueHeader;
//            $headerI->save();

            $rate = $header->CurrencyRate;
            $total -= str_replace(",", "", Input::get('DiscountGlobal'));
            if ($header->VAT == '1') {
                if ($header->isCash == 0) {
                    if ($header->VAT == 1) {
                        $slip = SalesHeader::getSlipInternalID($header->SalesID);
                    } else {
                        $slip = '-1';
                    }
                    $this->insertJournal($salesReturnNumber, ceil($total), $headerI->VAT, $header->CurrencyInternalID, $rate, Input::get('date'), $slip);
                } else {
                    $slip = '-1';
                    $this->insertJournal($salesReturnNumber, ceil($total), $headerI->VAT, $header->CurrencyInternalID, $rate, Input::get('date'), $slip);
                }
            }

            $messages = 'suksesInsert';
            $error = '';

            //kalau non ppn insert ke inventory transfer!!
            if ($headerI->VAT == 0) {
                $headerT = new TransferHeader;
                $transfer = $this->createIDTransfer(1) . '.';
                $date = explode('-', Input::get('date'));
                $yearDigit = substr($date[2], 2);
                $transfer .= $date[1] . $yearDigit . '.';
                $transferNumber = TransferHeader::getNextIDTransfer($transfer);
                $headerT->TransferID = $transferNumber;
                $headerT->TransferDate = $date[2] . '-' . $date[1] . '-' . $date[0];
                $headerT->WarehouseInternalID = Warehouse::where("Type", 1)->first()->InternalID;
                $headerT->WarehouseDestinyInternalID = $headerI->WarehouseInternalID;
                $headerT->TransferType = 0;
                $headerT->UserRecord = Auth::user()->UserID;
                $headerT->CompanyInternalID = Auth::user()->Company->InternalID;
                $headerT->UserModified = '0';
                $headerT->Remark = $headerI->SalesReturnID;
                $headerT->save();
                //insert detail
                $total = 0;
                foreach (SalesReturnDetail::where("SalesReturnInternalID", $headerI->InternalID)->get() as $s) {
                    $detail = new TransferDetail();
                    $detail->TransferInternalID = $headerT->InternalID;
                    $detail->InventoryInternalID = $s->InventoryInternalID;
                    $detail->UomInternalID = $s->UomInternalID;
                    $detail->Qty = $s->Qty;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = '0';
                    $detail->save();
                    setTampInventory($s->InventoryInternalID);
                }
            }
        }
        if (isset($headerI) && !is_null($headerI) && $messages == 'suksesInsert') {
            return Redirect::route('salesReturnDetail', $headerI->SalesReturnID)->with("msg", "print");
        }

        $header = SalesHeader::find($id);
        $detail = SalesHeader::find($id)->salesDetail()->get();
        return View::make('penjualan.salesReturnNew')
                        ->withToogle('transaction')->withAktif('salesReturn')
                        ->withError($error)
                        ->withMessages($messages)
                        ->withHeader($header)
                        ->withDetail($detail);
    }

    function createIDTransfer($tipe) {
        $transfer = 'TF';
        if ($tipe == 0) {
            $transfer .= '.' . date('m') . date('y');
        }
        return $transfer;
    }

    public function updateSalesReturn($id) {
        //tipe
        $headerUpdate = SalesReturnHeader::find($id);
        $detailUpdate = SalesReturnHeader::find($id)->salesReturnDetail()->get();

        //rule
        $rule = array(
            'warehouse' => 'required',
            'remark' => 'required|max:1000',
            'InternalDescription' => 'required'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalUpdate';
            $error = $validator->messages();
        } else {
            SalesReturnDetail::where('SalesReturnInternalID', '=', Input::get('SalesReturnInternalID'))->update(array('is_deleted' => 1));
            SalesReturnParcel::where('SalesReturnInternalID', '=', Input::get('SalesReturnInternalID'))->update(array('is_deleted' => 1));
            SalesReturnDescription::where('SalesReturnInternalID', '=', Input::get('SalesReturnInternalID'))->update(array('is_deleted' => 1));
            $headerI = SalesReturnHeader::find(Input::get('SalesReturnInternalID'));
            $salesID = explode("-", $headerI->SalesReturnID)[1];
            $header = SalesAddHeader::where('SalesID', $salesID)->first();
            //insert detail
            $total = 0;
            for ($a = 0; $a < count(Input::get('InternalDescription')); $a++) {
                if (Input::get('qtyDescription')[$a] > 0) {
                    $data = SalesAddDescription::find(Input::get('InternalDescription')[$a]);
                    $description = new SalesReturnDescription();
                    $description->SalesReturnInternalID = $headerI->InternalID;
                    $description->InventoryText = $data->InventoryText;
                    $description->UomText = $data->UomText;
                    $description->Price = $data->Price;
                    $description->DiscountNominal = $data->DiscountNominal;
                    $description->Discount = $data->Discount;
                    $qty = 0;
                    if (Input::get('max')[$a] < Input::get('qtyDescription')[$a])
                        $qty = Input::get('max')[$a];
                    else
                        $qty = Input::get('qtyDescription')[$a];
                    $description->Qty = $qty;
                    $description->SubTotal = $qty / $data->Qty * $data->SubTotal;
                    $description->Spesifikasi = $data->Spesifikasi;
                    $description->SalesDescriptionInternalID = Input::get('InternalDescription')[$a];
                    $description->UserRecord = Auth::user()->UserID;
                    $description->UserModified = '0';
                    $description->save();

                    for ($c = 0; $c < count(Input::get('InternalDetail' . ($a + 1))); $c++) {
                        if (Input::get('tipe' . ($a + 1))[$c] == "inventory") {
                            $detail = SalesAddDetail::find(Input::get('InternalDetail' . ($a + 1))[$c]);
                            //jika non paket
                            $qtyValue = $detail->Qty;
                            $qtyReturnValue = str_replace(',', '', Input::get('qty' . ($a + 1))[$c]);
                            $sumReturn = SalesReturnHeader::getSumReturnExcept($detail->InventoryInternalID, $headerI->SalesReturnID, $detail->InternalID);
                            $qtyValue = $qtyValue - $sumReturn;
                            if ($qtyValue < $qtyReturnValue) {
                                $qtyReturnValue = $qtyValue;
                            }
                            $priceValue = $detail->Price;
                            $discValue = $detail->DiscountNominal;
                            $subTotal = ($priceValue * $qtyReturnValue ) - (($priceValue * $qtyReturnValue) * $detail->Discount / 100) - $discValue * $qtyReturnValue;
                            if ($headerI->VAT == '1') {
                                $vatValue = $subTotal / 10;
                            } else {
                                $vatValue = 0;
                            }
                            if ($qtyReturnValue > 0) {
                                $detail2 = new SalesReturnDetail();
                                $detail2->SalesReturnInternalID = $headerI->InternalID;
                                $detail2->InventoryInternalID = $detail->InventoryInternalID;
                                $detail2->DescriptionInternalID = $description->InternalID;
                                $detail2->UomInternalID = $detail->UomInternalID;
                                $detail2->Qty = $qtyReturnValue;
                                $detail2->Price = $priceValue;
                                $detail2->Discount = $detail->Discount;
                                $detail2->DiscountNominal = $discValue;
                                $detail2->VAT = $vatValue;
                                $detail2->SubTotal = $subTotal;
                                $detail2->UserRecord = Auth::user()->UserID;
                                $detail2->UserModified = '0';
                                $detail2->SalesDetailInternalID = Input::get('InternalDetail' . ($a + 1))[$c];
                                $detail2->save();
                                setTampInventory($detail->InventoryInternalID);
                            }
                            $total += $subTotal;
                        } else {
                            $detail = SalesAddParcel::find(Input::get("InternalDetail" . ($a + 1))[$c]);
                            //jika paket
                            $parcelDetail = ParcelInventory::where("ParcelInternalID", $detail->ParcelInternalID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->get();
                            $qtyValue = $detail->Qty;
                            $qtyReturnValue = str_replace(',', '', Input::get('qty' . ($a + 1))[$c]);
                            $sumReturn = SalesReturnHeader::getSumReturnExceptParcel($detail->ParcelInternalID, $headerI->SalesReturnID, $detail->InternalID);
                            $qtyValue = $qtyValue - $sumReturn;
                            if ($qtyValue < $qtyReturnValue) {
                                $qtyReturnValue = $qtyValue;
                            }
                            $priceValue = $detail->Price;
                            $discValue = $detail->DiscountNominal;
                            $subTotal = ($priceValue * $qtyReturnValue ) - (($priceValue * $qtyReturnValue) * $detail->Discount / 100) - $discValue * $qtyReturnValue;

                            if ($headerI->VAT == '1') {
                                $vatValue = $subTotal / 10;
                            } else {
                                $vatValue = 0;
                            }
                            if ($qtyReturnValue > 0) {
                                $SAParcel = new SalesReturnParcel();
                                $SAParcel->SalesReturnInternalID = $headerI->InternalID;
                                $SAParcel->ParcelInternalID = $detail->ParcelInternalID;
                                $SAParcel->DescriptionInternalID = $description->InternalID;
                                $SAParcel->SalesParcelDetailInternalID = Input::get("InternalDetail" . ($a + 1))[$c];
                                $SAParcel->Qty = $qtyReturnValue;
                                $SAParcel->Price = $priceValue;
                                $SAParcel->Discount = $detail->Discount;
                                $SAParcel->VAT = $vatValue;
                                $SAParcel->DiscountNominal = $discValue;
                                $SAParcel->SubTotal = $subTotal;
                                $SAParcel->CompanyInternalID = Auth::user()->Company->InternalID;
                                $SAParcel->UserRecord = Auth::user()->UserID;
                                $SAParcel->UserModified = "0";
                                $SAParcel->Remark = "-";
                                $SAParcel->save();

                                foreach ($parcelDetail as $dataParcel) {
                                    $uom1 = $dataParcel->UomInternalID;
                                    $qtyValue1 = $dataParcel->Qty * $qtyReturnValue;
                                    $priceValue1 = $dataParcel->Price;
                                    $subTotal1 = ($priceValue1 * $qtyValue1);
                                    if (Input::get('vat') == '1') {
                                        $vatValue1 = $subTotal1 / 10;
                                    } else {
                                        $vatValue1 = 0;
                                    }
                                    $detail2 = new SalesReturnDetail();
                                    $detail2->SalesReturnInternalID = $headerI->InternalID;
                                    $detail2->InventoryInternalID = $dataParcel->InventoryInternalID;
                                    $detail2->UomInternalID = $uom1;
                                    $detail2->SalesReturnParcelInternalID = $SAParcel->InternalID;
                                    $detail2->DescriptionInternalID = $description->InternalID;
                                    $detail2->Qty = $qtyValue1;
                                    $detail2->Price = $priceValue1;
                                    $detail2->Discount = 0;
                                    $detail2->DiscountNominal = 0;
                                    $detail2->VAT = $vatValue1;
                                    $detail2->SubTotal = $subTotal1;
                                    $detail2->SalesDetailInternalID = $header->InternalID;
                                    $detail2->UserRecord = Auth::user()->UserID;
                                    $detail2->UserModified = '0';
                                    $detail2->save();
                                    setTampInventory($dataParcel->InventoryInternalID);
                                }
                            }
                            $total += $subTotal;
                        }
                    }
                }
            }
            //change grandtotal jd yg bener

            $headerI->Remark = Input::get('remark');
            $headerI->WarehouseInternalID = Input::get('warehouse');
            $headerI->DiscountGlobal = str_replace(",", "", Input::get('DiscountGlobal'));
            if ($headerI->VAT == '1') {
                $vatValueHeader = ($total - str_replace(",", "", Input::get("DiscountGlobal"))) / 10;
            } else {
                $vatValueHeader = 0;
            }
//            $headerI->GrandTotal = $total - str_replace(",", "", Input::get("DiscountGlobal")) + $vatValueHeader;
//            $headerI->save();

            $journal = JournalHeader::where('TransactionID', '=', $headerUpdate->SalesReturnID)->get();
            foreach ($journal as $value) {
                JournalDetail:: where('JournalInternalID', '=', $value->InternalID)->delete();
                JournalHeader::where('InternalID', '=', $value->InternalID)->delete();
            }
            SalesReturnDetail::where('SalesReturnInternalID', '=', Input::get('SalesReturnInternalID'))->where('is_deleted', 1)->delete();
            SalesReturnParcel::where('SalesReturnInternalID', '=', Input::get('SalesReturnInternalID'))->where('is_deleted', 1)->delete();
            SalesReturnDescription::where('SalesReturnInternalID', '=', Input::get('SalesReturnInternalID'))->where('is_deleted', 1)->delete();

            $currency = $headerUpdate->CurrencyInternalID;
            $rate = $headerUpdate->CurrencyRate;
            $date = date("d-m-Y", strtotime($headerUpdate->SalesReturnDate));
            $total -= str_replace(",", "", Input::get('DiscountGlobal'));
            if ($headerUpdate->isCash == 0) {
                $salesIDReal = substr($headerUpdate->SalesReturnID, 7);
                if ($header->VAT == 1) {
                    $slip = SalesHeader:: getSlipInternalID($salesIDReal);
                } else {
                    $slip = '-1';
                }
                $this->insertJournal($headerUpdate->SalesReturnID, ceil($total), $headerUpdate->VAT, $currency, $rate, $date, $slip);
            } else {
                $slip = '-1';
                $this->insertJournal($headerUpdate->SalesReturnID, ceil($total), $headerUpdate->VAT, $currency, $rate, $date, $slip);
            }
            $messages = 'suksesUpdate';
            $error = '';

            //kalau non ppn insert ke inventory transfer!!
            if ($header->VAT == 0) {
                $headerT = TransferHeader::where("Remark", $headerI->SalesReturnID)->first();
                $headerT->WarehouseInternalID = $headerI->WarehouseInternalID;
                $headerT->WarehouseDestinyInternalID = Warehouse::where("Type", 1)->first()->InternalID;
                $headerT->UserModified = Auth::user()->UserID;
                $headerT->save();
                //hapus detail yg lama
                TransferDetail::where("TransferInternalID", $headerT->InternalID)->delete();
                //insert detail baru
                foreach (SalesReturnDetail::where("SalesReturnInternalID", $headerI->InternalID)->get() as $s) {
                    $detail = new TransferDetail();
                    $detail->TransferInternalID = $headerT->InternalID;
                    $detail->InventoryInternalID = $s->InventoryInternalID;
                    $detail->UomInternalID = $s->UomInternalID;
                    $detail->Qty = $s->Qty;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = '0';
                    $detail->save();
                    setTampInventory($s->InventoryInternalID);
                }
            }

            if (isset($headerI) && !is_null($headerI) && $messages == 'suksesUpdate') {
                return Redirect::route('salesReturnDetail', $headerI->SalesReturnID)->with("msg", "print");
            }
        }

        //tipe
        $header = SalesReturnHeader::find($id);
        $detail = SalesReturnHeader::find($id)->salesReturnDetail()->get();
        $salesReturnID = SalesReturnHeader::find($id)->SalesReturnID;
        $salesIDReal = substr($salesReturnID, 7);
        $idSales = SalesHeader::getIdsales($salesIDReal);
        $headerSales = SalesHeader::find($idSales);
        $detailSales = SalesHeader::find($idSales)->salesDetail()->get();
        return View::make('penjualan.salesReturnUpdate')
                        ->withToogle('transaction')->withAktif('salesReturn')
                        ->withHeader($header)
                        ->withDetail($detail)
                        ->withHeadersales($headerSales)
                        ->withDetailsales($detailSales)
                        ->withError($error)
                        ->withMessages($messages);
    }

    public function deleteSalesReturn() {
        $salesReturn = JournalDetail::where('JournalTransactionID', SalesReturnHeader::find(Input::get('InternalID'))->SalesReturnID)->count();
        if ($salesReturn > 0) {
            JournalDetail::where('JournalTransactionID', SalesReturnHeader::find(Input::get('InternalID'))->SalesReturnID)->delete();
        }
        $salesReturnHeader = SalesReturnHeader::find(Input::get('InternalID'));
        if ($salesReturnHeader->CompanyInternalID == Auth::user()->Company->InternalID) {
            $journal = JournalHeader::where('TransactionID', '=', $salesReturnHeader->SalesReturnID)->get();
            foreach ($journal as $value) {
                JournalDetail:: where('JournalInternalID', '=', $value->InternalID)->delete();
                JournalHeader::where('InternalID', '=', $value->InternalID)->delete();
            }
            //hapus detil
            $detilData = SalesReturnHeader::find(Input::get('InternalID'))->salesReturnDetail;
            foreach ($detilData as $value) {
                $detil = salesReturnDetail::find($value->InternalID);
                setTampInventory($detil->InventoryInternalID);
                $detil->delete();
            }
            $descData = SalesReturnHeader::find(Input::get('InternalID'))->salesReturnDescription;
            foreach ($descData as $value) {
                $detil = salesReturnDescription::find($value->InternalID);
                $detil->delete();
            }
            //delete sales return parcel
            SalesReturnParcel::where('SalesReturnInternalID', '=', Input::get('InternalID'))->delete();
            //hapus salesReturn
            $salesReturn = SalesReturnHeader::find(Input::get('InternalID'));
            $salesReturn->delete();
            $messages = 'suksesDelete';
        } else {
            $messages = 'accessDenied';
        }
        $data = SalesReturnHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
        return View::make('penjualan.salesReturnSearch')
                        ->withToogle('transaction')->withAktif(
                                'salesReturn')
                        ->withMessages($messages)->withData($data);
    }

    function insertJournal($salesReturnNumber, $total, $vat, $currency, $rate, $date, $slip) {
        $header = new JournalHeader;
        $date = explode('-', $date);
        $yearDigit = substr($date[2], 2);
        $dateText = $date[1] . $yearDigit;
        $defaultPenjualan = Default_s::find(1)->DefaultID;
        $defaultPiutang = Default_s::find(2)->DefaultID;
        $defaultIncome = Default_s::find(3)->DefaultID;
        if ($slip == '-1') {
            $cari = 'ME-' . $dateText;
            $header->JournalType = 'Memorial';
            $akun = array($defaultPenjualan, $defaultPiutang, $defaultIncome);
            $header->JournalID = JournalHeader::getNextIDJournal($cari . '-');
            $header->SlipInternalID = Null;
        } else {
            $akun = array($defaultPenjualan, 'Slip', $defaultIncome);
            $tampSlipID = Slip::find($slip);
            if ($tampSlipID->Flag == '0') {
                $cari = 'CO-' . $dateText;
                $header->JournalType = 'Cash In';
            } else if ($tampSlipID->Flag == '1') {
                $cari = 'BO-' . $dateText;
                $header->JournalType = 'Bank In';
            }
            $header->JournalID = JournalHeader::getNextIDJournal($cari . '-' . $tampSlipID->SlipID . '-');
            $header->SlipInternalID = $slip;
        }
        $header->JournalDate = $date[2] . '-' . $date[1] . '-' . $date[0];
        $header->JournalFrom = Auth::user()->UserID;
        $header->Notes = '';
        $department = Department::where('Default', '1')->first();
        $header->DepartmentInternalID = $department->InternalID;
        $header->TransactionID = $salesReturnNumber;
        $header->ACC5InternalID = '0';
        $header->Lock = '0';
        $header->Check = '0';
        $header->Flag = '0';
        $header->UserRecord = Auth::user()->UserID;
        $header->CompanyInternalID = Auth::user()->Company->InternalID;
        $header->UserModified = '0';
        $header->Remark = '';
        $header->save();

        //insert detail
        if ($vat == 1) {
            $vatValue = floor(10 * $total / 100);
        } else {
            $vatValue = 0;
        }
        $count = 1;
        foreach ($akun as $data) {
            $kreditValue = 0;
            $debetValue = 0;
            if ($data != $defaultIncome || ($data == $defaultIncome && $vatValue != 0)) {
                $detail = new JournalDetail();
                $detail->JournalInternalID = $header->InternalID;
                $detail->JournalIndex = $count;
                $detail->JournalNotes = $data;
                if ($data == $defaultPenjualan) {
                    $debetValue = $total;
                    $detail->JournalDebetMU = $debetValue;
                    $detail->JournalCreditMU = 0;
                } else if ($data == $defaultPiutang || $data == 'Slip') {
                    $kreditValue = $total + $vatValue;
                    $detail->JournalDebetMU = 0;
                    $detail->JournalCreditMU = $kreditValue;
                } else {
                    $debetValue = $vatValue;
                    $detail->JournalDebetMU = $debetValue;
                    $detail->JournalCreditMU = 0;
                }
                $detail->CurrencyInternalID = $currency;
                $detail->CurrencyRate = $rate;
                $detail->JournalDebet = $debetValue * $rate;
                $detail->JournalCredit = $kreditValue * $rate;
                $detail->JournalTransactionID = NULL;
                if ($data != 'Slip') {
                    $default = Default_s::getInternalCoa($data);
                } else {
                    $default = Slip::getInternalCoa($slip);
                }
                $detail->ACC1InternalID = $default->ACC1InternalID;
                $detail->ACC2InternalID = $default->ACC2InternalID;
                $detail->ACC3InternalID = $default->ACC3InternalID;
                $detail->ACC4InternalID = $default->ACC4InternalID;
                $detail->ACC5InternalID = $default->ACC5InternalID;
                $detail->ACC6InternalID = $default->ACC6InternalID;
                $coa = Coa::getInternalID($default->ACC1InternalID, $default->ACC2InternalID, $default->ACC3InternalID, $default->ACC4InternalID, $default->ACC5InternalID, $default->ACC6InternalID);
                $detail->COAName = Coa::find($coa)->COAName;
                $detail->UserRecord = Auth::user()->UserID;
                $detail->UserModified = '0';
                $detail->save();
                $count++;
            }
        }
    }

    function salesReturnPrintInclude($id) {
        $id = SalesReturnHeader::getIdsalesReturn($id);
        $header = SalesReturnHeader::find($id);
        $header->Print = $header->Print + 1;
        $header->save();
        $detail = SalesReturnHeader::find($id)->salesReturnDetail()->get();
        $description = SalesReturnDescription::where('SalesReturnInternalID', $id)->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $coa6 = SalesReturnHeader::find($header->InternalID)->coa6;
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-')
                $cp = 'CP: ' . $coa6->ContactPerson . '<br>';
            else
                $cp = '';
            $customer = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . '<br>' . $cp . $coa6->Phone . '<br>' . $coa6->Fax . '<br>' . $coa6->Email;
            if ($header->isCash == 0) {
                $payment = 'Cash';
            } else if ($header->isCash == 1) {
                $payment = 'Credit';
            } else if ($header->isCash == 2) {
                $payment = 'CBD';
            } else if ($header->isCash == 3) {
                $payment = 'Deposit';
            } else {
                $payment = 'Down Payment';
            }
            $currency = Currency::find($header->CurrencyInternalID);
            $currencyName = $currency->CurrencyName;
            if ($header->VAT == 0) {
                $vat = 'Non Tax';
            } else {
                $vat = 'Tax';
            }
            $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 6px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box; position: relative;">
                            <div style="height: 195px;border-bottom:1px solid #ddd;margin-bottom:20px;">
                        <div style="height: 133px">
                        <img height="100%" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                        </div>
                        <div style="width: 420px; float: left;">
                            <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->Company->CompanyName . '</b>
                        </div> 
                            <div style=" box-sizing: border-box; margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Address</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Sales Return</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;padding-left:6px;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span><br>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                            <table>
                            <tr>
                            <td width="275px">
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Sales Return ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->SalesReturnID . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->SalesReturnDate)) . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">Customer</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $customer . '</td>
                                 </tr>';
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-') {
                $html .= '     <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Contact Person</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->ContactPerson . '</td>
                                 </tr>';
            }
            $html .= '<tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Payment</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $payment . '</td>
                                 </tr>
                                </table>
                                </td>
                                <td style="vertical-align: text-top;">
                                <table>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Warehouse</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Warehouse->WarehouseName . '</td>
                                     </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Currency</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $currencyName . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Rate</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . number_format($header->CurrencyRate, '2', '.', ',') . '</td>
                                     </tr>
                                </table></td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" style="width:100%; margin-top: 5px; ">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">No.</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="25%">Inventory</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Uom</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Qty</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Price</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc (%)</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            $total = 0;
            $totalVAT = 0;
            $counter = 1;
            $price = 0;
            $discount = 0;
            $subtotal = 0;
            if (count($description) > 0) {
                foreach ($description as $data) {
                    if ($header->VAT == 1) {
                        $price = $data->Price + ($data->Price * 0.1);
                        $discount = $data->DiscountNominal + ($data->DiscountNominal * 0.1);
                    } else {
                        $price = $data->Price;
                        $discount = $data->DiscountNominal;
                    }
                    $subtotal = ($price - $discount) * $data->Qty;
                    $subtotal = $subtotal - (($data->Discount / 100) * $subtotal);
                    $html .= '<tr>
                                <td rowspan="' . (1 + ($data->Spesifikasi != '' ? 1 : 0)) . '" style="vertical-align: top;font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $counter . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $data->InventoryText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->UomText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($discount, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($subtotal, '2', '.', ',') . '</td>
                            </tr>';
                    if ($data->Spesifikasi != '') {
                        $html .= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . nl2br($data->Spesifikasi) . '</td>
                            </tr>';
                    }

                    $counter++;
                    $total += $subtotal;
                }
            } else {
                $html .= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this salesReturn order.</td>
                        </tr>';
            }
            $html .= '</tbody>
                            </table>
 
                    <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                        <table>
                        <tr>
                        <td width="275px" style="vertical-align: text-top;">
                            <table>
                            <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">Remark</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Remark . '</td>
                             </tr> 
                            </table>
                            </td>
                            <td width="200px" style="float:right;">
                             <table style="margin-left:30px;">
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Discount</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Grand Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total - $header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                
                                </table>
                            </td>
                            </tr>
                        </table>
                    </div>
                    

                    </div>
                </body>
            </html>';
//            return PDF ::load($html, 'A5', 'portrait')->show();

            return View::make('template.print.salesReturnIncludePrint')
                            ->with('header', $header)
                            ->with('customer', $customer)
                            ->with('vat', $vat)
                            ->with('id', $id)
                            ->with('description', $description)
                            ->with('currencyName', $currencyName)
                            ->with('payment', $payment)
                            ->with('detail', $detail);
        } else {
            $messages = 'accessDenied';

            Session::flash('messages', $messages);
            return Redirect ::Route('showSalesReturn');
        }
    }

    function salesReturnPrint($id) {
        $id = SalesReturnHeader::getIdsalesReturn($id);
        $header = SalesReturnHeader::find($id);
        $header->Print = $header->Print + 1;
        $header->save();
        $detail = SalesReturnHeader::find($id)->salesReturnDetail()->get();
        $description = SalesReturnDescription::where('SalesReturnInternalID', $id)->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $coa6 = SalesReturnHeader::find($header->InternalID)->coa6;
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-')
                $cp = 'CP: ' . $coa6->ContactPerson . '<br>';
            else
                $cp = '';
            $customer = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . '<br>' . $cp . $coa6->Phone . '<br>' . $coa6->Fax . '<br>' . $coa6->Email;
            if ($header->isCash == 0) {
                $payment = 'Cash';
            } else if ($header->isCash == 1) {
                $payment = 'Credit';
            } else if ($header->isCash == 2) {
                $payment = 'CBD';
            } else if ($header->isCash == 3) {
                $payment = 'Deposit';
            } else {
                $payment = 'Down Payment';
            }
            $currency = Currency::find($header->CurrencyInternalID);
            $currencyName = $currency->CurrencyName;
            if ($header->VAT == 0) {
                $vat = 'Non Tax';
            } else {
                $vat = 'Tax';
            }
            $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 6px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box; position: relative;">
                            <div style="height: 195px;border-bottom:1px solid #ddd;margin-bottom:20px;">
                        <div style="height: 133px">
                        <img height="100%" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                        </div>
                        <div style="width: 420px; float: left;">
                            <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->Company->CompanyName . '</b>
                        </div> 
                            <div style=" box-sizing: border-box; margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Address</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Sales Return</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;padding-left:6px;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span><br>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                            <table>
                            <tr>
                            <td width="275px">
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Sales Return ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->SalesReturnID . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->SalesReturnDate)) . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">Customer</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $customer . '</td>
                                 </tr>';
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-') {
                $html .= '     <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Contact Person</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->ContactPerson . '</td>
                                 </tr>';
            }
            $html .= '<tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Payment</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $payment . '</td>
                                 </tr>
                                </table>
                                </td>
                                <td style="vertical-align: text-top;">
                                <table>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Warehouse</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Warehouse->WarehouseName . '</td>
                                     </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Currency</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $currencyName . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Rate</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . number_format($header->CurrencyRate, '2', '.', ',') . '</td>
                                     </tr>
                                </table></td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" style="width:100%; margin-top: 5px; ">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">No.</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="25%">Inventory</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Uom</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Qty</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Price</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc (%)</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            $total = 0;
            $totalVAT = 0;
            $counter = 1;
            if (count($description) > 0) {
                foreach ($description as $data) {
                    $html .= '<tr>
                                <td rowspan="' . (1 + ($data->Spesifikasi != '' ? 1 : 0)) . '" style="vertical-align: top;font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $counter . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $data->InventoryText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->UomText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
                    if ($data->Spesifikasi != '') {
                        $html .= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . nl2br($data->Spesifikasi) . '</td>
                            </tr>';
                    }
                    $totalVAT += $data->VAT;
                    $counter++;
                    $total += $data->SubTotal;
                }
                if ($totalVAT != 0) {
                    $totalVAT = $totalVAT - ($header->DiscountGlobal * 0.1);
                }
            } else {
                $html .= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this salesReturn order.</td>
                        </tr>';
            }
            $html .= '</tbody>
                            </table>
 
                    <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                        <table>
                        <tr>
                        <td width="275px" style="vertical-align: text-top;">
                            <table>
                            <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">Remark</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Remark . '</td>
                             </tr> 
                            </table>
                            </td>
                            <td width="200px" style="float:right;">
                             <table style="margin-left:30px;">
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Discount</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Grand Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total - $header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Tax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($totalVAT, '2', '.', ',') . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; ">Grand Total (Tax)</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">'
                    . number_format($total - $header->DiscountGlobal + $totalVAT, '2', '.', ',') . '</td>
                                 </tr>
                                </table>
                            </td>
                            </tr>
                        </table>
                    </div>
                    

                    </div>
                </body>
            </html>';
//            return PDF ::load($html, 'A5', 'portrait')->show();

            return View::make('template.print.salesReturnPrint')
                            ->with('header', $header)
                            ->with('customer', $customer)
                            ->with('vat', $vat)
                            ->with('id', $id)
                            ->with('description', $description)
                            ->with('currencyName', $currencyName)
                            ->with('payment', $payment)
                            ->with('detail', $detail);
        } else {
            $messages = 'accessDenied';

            Session::flash('messages', $messages);
            return Redirect ::Route('showSalesReturn');
        }
    }

    function salesReturnInternalPrint($id) {
        $id = SalesReturnHeader::getIdsalesReturn($id);
        $header = SalesReturnHeader::find($id);
        $detail = SalesReturnHeader::find($id)->salesReturnDetail()->get();
        $description = SalesReturnDescription::where('SalesReturnInternalID', $id)->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $coa6 = SalesReturnHeader::find($header->InternalID)->coa6;
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-')
                $cp = 'CP: ' . $coa6->ContactPerson . '<br>';
            else
                $cp = '';
            $customer = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . '<br>' . $cp . $coa6->Phone . '<br>' . $coa6->Fax . '<br>' . $coa6->Email;
            if ($header->isCash == 0) {
                $payment = 'Cash';
            } else if ($header->isCash == 1) {
                $payment = 'Credit';
            } else if ($header->isCash == 2) {
                $payment = 'CBD';
            } else if ($header->isCash == 3) {
                $payment = 'Deposit';
            } else {
                $payment = 'Down Payment';
            }
            $currency = Currency::find($header->CurrencyInternalID);
            $currencyName = $currency->CurrencyName;
            if ($header->VAT == 0) {
                $vat = 'Non Tax';
            } else {
                $vat = 'Tax';
            }
            $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 6px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box; position: relative;">
                        <div style="height: 70px;border-bottom:1px solid #ddd;margin-bottom:20px;">
                        <div style="width: 420px; float: left;">
                            <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->Company->CompanyName . '</b>
                        </div> 
                            <div style=" box-sizing: border-box; margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Address</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Sales Return</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;padding-left:6px;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span><br>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                            <table>
                            <tr>
                            <td width="275px">
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Sales Return ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->SalesReturnID . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->SalesReturnDate)) . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">Customer</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $customer . '</td>
                                 </tr>';
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-') {
                $html .= '     <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Contact Person</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->ContactPerson . '</td>
                                 </tr>';
            }
            $html .= '<tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Payment</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $payment . '</td>
                                 </tr>
                                </table>
                                </td>
                                <td style="vertical-align: text-top;">
                                <table>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Warehouse</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Warehouse->WarehouseName . '</td>
                                     </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Currency</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $currencyName . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Rate</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . number_format($header->CurrencyRate, '2', '.', ',') . '</td>
                                     </tr>
                                </table></td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" style="width:100%; margin-top: 5px; ">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">No.</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="25%">Inventory</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Uom</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Qty</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Price</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc (%)</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            $total = 0;
            $totalVAT = 0;
            $counter = 1;
            if (count($description) > 0) {
                foreach ($description as $data) {
                    $detail = SalesReturnDetail::where('DescriptionInternalID', $data->InternalID)->where('SalesReturnParcelInternalID', 0)->get();
                    $parcel = SalesReturnParcel::where('DescriptionInternalID', $data->InternalID)->get();
                    $html .= '<tr>
                                <td rowspan="' . (1 + ($data->Spesifikasi != '' ? 1 : 0) + count($detail) + count($parcel)) . '" style="vertical-align: top;font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $counter . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $data->InventoryText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->UomText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
                    if ($data->Spesifikasi != '') {
                        $html .= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . nl2br($data->Spesifikasi) . '</td>
                            </tr>';
                    }
                    foreach ($detail as $data2) {
                        $html .= '<tr>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;"><i>' . $data2->inventory->InventoryName . '</i></td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data2->uom->UomID . '</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data2->Qty, 0, '.', ',') . '</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data2->Price, '2', '.', ',') . '</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data2->Discount . '' . '</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data2->DiscountNominal, '2', '.', ',') . '</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data2->SubTotal, '2', '.', ',') . '</td>
                                </tr>';
                    }
                    foreach ($parcel as $data2) {
                        $html .= '<tr>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;"><i>' . $data2->parcel->ParcelName . '</i></td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">-</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data2->Qty, 0, '.', ',') . '</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data2->Price, '2', '.', ',') . '</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data2->Discount . '' . '</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data2->DiscountNominal, '2', '.', ',') . '</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data2->SubTotal, '2', '.', ',') . '</td>
                                </tr>';
                    }
                    $totalVAT += $data->VAT;
                    $total += $data->SubTotal;
                    $counter++;
                }
                if ($totalVAT != 0) {
                    $totalVAT = $totalVAT - ($header->DiscountGlobal * 0.1);
                }
            } else {
                $html .= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this salesReturn order.</td>
                        </tr>';
            }
            $html .= '</tbody>
                            </table>
 
                    <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                        <table>
                        <tr>
                        <td width="275px" style="vertical-align: text-top;">
                            <table>
                            <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">Remark</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Remark . '</td>
                             </tr> 
                            </table>
                            </td>
                            <td width="200px" style="float:right;">
                             <table style="margin-left:30px;">
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Discount</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Grand Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total - $header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Tax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($totalVAT, '2', '.', ',') . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; ">Grand Total (Tax)</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">'
                    . number_format($header->GrandTotal, '2', '.', ',') . '</td>
                                 </tr>
                                </table>
                            </td>
                            </tr>
                        </table>
                    </div>
                    

                    </div>
                </body>
            </html>';
//            return PDF ::load($html, 'A5', 'portrait')->show();

            return View::make('template.print.salesReturnInternalPrint')
                            ->with('header', $header)
                            ->with('customer', $customer)
                            ->with('vat', $vat)
                            ->with('id', $id)
                            ->with('description', $description)
                            ->with('currencyName', $currencyName)
                            ->with('payment', $payment)
                            ->with('detail', $detail);
        } else {
            $messages = 'accessDenied';

            Session::flash('messages', $messages);
            return Redirect ::Route('showSalesReturn');
        }
    }

    public function summarySalesReturn() {
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $totalSR = 0;
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <!--<div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>-->
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Sales Return Summary Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span>
                            <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span>';
        $hitung = 0;
        foreach (Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $dataCustomer) {
            if (SalesReturnHeader::join("m_warehouse", "m_warehouse.InternalID", "=", "t_salesreturn_header.WarehouseInternalID")
                            ->where('t_salesreturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                            ->where('ACC6InternalID', $dataCustomer->InternalID)
                            ->where("Type", Auth::user()->WarehouseCheck)
                            ->where("VAT", "!=", Auth::user()->SeeNPPN)
//                            ->where("TaxNumber", "!=", ".-.")
                            ->whereBetween('SalesReturnDate', Array($start, $end))->count() > 0) {
                $html .= '<table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=7>' . $dataCustomer->ACC6Name . '</th>
                                        </tr>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales Return ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Currency</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Rate</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total(After Discount)</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Vat</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Grand Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                $sumGrandTotal = 0;
                foreach (SalesReturnHeader::join("m_warehouse", "m_warehouse.InternalID", "=", "t_salesreturn_header.WarehouseInternalID")
                        ->where('t_salesreturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where('ACC6InternalID', $dataCustomer->InternalID)
                        ->where("Type", Auth::user()->WarehouseCheck)
                        ->where("VAT", "!=", Auth::user()->SeeNPPN)
//                        ->where("TaxNumber", "!=", ".-.")
                        ->whereBetween('SalesReturnDate', Array($start, $end))->get() as $data) {
                    $grandTotal = $data->GrandTotal;
                    $sumGrandTotal += $grandTotal;
                    $totalSR += $grandTotal;
                    $total = ceil($grandTotal);
                    $vat = 0;
                    if ($data->VAT == 1) {
                        $total = ceil($total * 10 / 11);
                        $vat = floor($total / 10);
                    }
                    $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->SalesReturnID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->SalesReturnDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->Currency->CurrencyName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->CurrencyRate, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($total, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($vat, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($grandTotal, '2', '.', ',') . '</td>
                            </tr>';
                }
                $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="80%" colspan="6">Total : </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . number_format($sumGrandTotal, '2', '.', ',') . '</td>
                        </tr>';

                $html .= '</tbody>
            </table>';
                $hitung++;
            }
        }

        if ($hitung == 0) {
            $html .= '<br><span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no sales return.</span>';
        }
        $html .= '<table class="tableBorder" width="100%"  style="margin-top: 10px; clear: both;  top: 50px;">
                    <thead>
                        <tr>
                            <th style="font-family: helvetica,sans-serif;font-size: 14px; margin: 5px !important; font-weight: 700; text-align: right" width="80%" colspan="6">Total Sales Return : </th>
                            <th style="font-family: helvetica,sans-serif;font-size: 14px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . number_format($totalSR, '2', '.', ',') . '</th>
                        </tr>
                    <thead>
                </table>';
        $html .= '</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('sales_return_summary');
    }

    public function detailSalesReturn() {
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $dateTamp = '';
        $coa6Tamp = '';
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <!--<div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>-->
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Sales Return Detail Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span>'
                . '<span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span><br><br>';
        if (SalesReturnHeader::join("m_warehouse", "m_warehouse.InternalID", "=", "t_salesreturn_header.WarehouseInternalID")
                        ->where('t_salesreturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                        ->whereBetween('SalesReturnDate', Array($start, $end))
//                        ->where("TaxNumber", "!=", ".-.")
                        ->where("Type", Auth::user()->WarehouseCheck)
                        ->where("VAT", "!=", Auth::user()->SeeNPPN)
                        ->orderBy('SalesReturnDate')->orderBy('ACC6InternalID')->count() > 0) {
            foreach (SalesReturnHeader::join("m_warehouse", "m_warehouse.InternalID", "=", "t_salesreturn_header.WarehouseInternalID")
                    ->where('t_salesreturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->whereBetween('SalesReturnDate', Array($start, $end))
//                    ->where("TaxNumber", "!=", ".-.")
                    ->where("Type", Auth::user()->WarehouseCheck)
                    ->where("VAT", "!=", Auth::user()->SeeNPPN)
                    ->orderBy('SalesReturnDate')->orderBy('ACC6InternalID')->get() as $dataPenjualan) {
                if ($dateTamp != date("d-M-Y", strtotime($dataPenjualan->SalesReturnDate))) {
                    $html .= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales Return Date : ' . date("d-M-Y", strtotime($dataPenjualan->SalesReturnDate)) . '</span><br><br>';
                    $dateTamp = date("d-M-Y", strtotime($dataPenjualan->SalesReturnDate));
                    $coa6Tamp = '';
                }
                if ($coa6Tamp != $dataPenjualan->ACC6InternalID) {
                    $html .= '<span style="padding-left:23px;font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Customer : ' . $dataPenjualan->coa6->ACC6Name . '</span>';
                    $coa6Tamp = $dataPenjualan->ACC6InternalID;
                }
                if ($dataPenjualan->coa6->ContactPerson != '' && $dataPenjualan->coa6->ContactPerson != '-' && $dataPenjualan->coa6->ContactPerson != null) {
                    $html .= '<span style="padding-left:23px;font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Contact Person : ' . $dataPenjualan->coa6->ContactPerson . '</span>';
                }
                $html .= '<table class="tableBorder" style="width:95%; margin-top: 18px; clear: both; position: relative; left: 4%">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=8>' . $dataPenjualan->SalesReturnID . ' | ' . $dataPenjualan->Currency->CurrencyName . ' | Rate : ' . number_format($dataPenjualan->CurrencyRate, '2', '.', ',') . ' </th>
                                        </tr>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory ID</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Name</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Uom</th>
                                            <th width="10%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Qty</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Price</th>
                                            <th width="10%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc (%)</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                $total = 0;
                $vat = 0;
                foreach ($dataPenjualan->salesReturnDetail as $data) {
                    if ($data->SalesReturnParcelInternalID == 0) {
                        $total += ceil($data->SubTotal);
                        $vat += floor(ceil($data->SubTotal) / 10);
//                        $vat += $data->VAT;
                        $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->inventory->InventoryID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->inventory->InventoryName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->Uom->UomID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format(ceil($data->SubTotal), '2', '.', ',') . '</td>
                            </tr>';
                    }
                }
                foreach (SalesReturnParcel::where("SalesReturnInternalID", $dataPenjualan->InternalID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->get() as $data) {
                    $total += ceil($data->SubTotal);
                    $vat += floor(ceil($data->SubTotal) / 10);
//                    $vat += $data->VAT;
                    $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->Parcel->ParcelID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->Parcel->ParcelName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">-</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format(ceil($data->SubTotal), '2', '.', ',') . '</td>
                            </tr>';
                }
                if ($vat != 0) {
                    $vat = $vat - ($dataPenjualan->DiscountGlobal * 0.1);
                }
                $html .= '<tr>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=5></td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=2>Total <br> Discount <br> Grand Total <br> VAT <br> Grand Total </td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '<br> '
                        . '' . number_format($dataPenjualan->DiscountGlobal, '2', '.', ',') . '<br>'
                        . '' . number_format($total - $dataPenjualan->DiscountGlobal, '2', '.', ',') . '<br>'
                        . '' . number_format($vat, '2', '.', ',') . '<br>'
                        . '' . number_format($dataPenjualan->GrandTotal, '2', '.', ',') . '</td>
                    </tr>
                </tbody>
            </table>';
            }
        } else {
            $html .= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no sales return.</span><br><br>';
        }
        $html .= '</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('sales_return_detail');
    }

    //=====================================ajax=======================================
    public function getResultSearchSR() {
        $hitung = 0;
        $input = splitSearchValue(Input::get("id"));
        $date = date("Y-m-d", strtotime(Input::get("id")));
//        $salesReturnHeader = SalesHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
//                ->where("SalesID", "like", $input)
//                ->orWhere("SalesDate", "like", '%' . date("Y-m-d", strtotime(Input::get("id"))) . "%")
//                ->orWhere("UserRecord", "like", $input)
//                ->OrderBy('SalesDate', 'desc')
//                ->get();
        $salesReturnHeader = SalesAddHeader::where('t_sales_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->join('m_coa6', 'm_coa6.InternalID', '=', 't_sales_header.ACC6InternalID')
                ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_sales_header.WarehouseInternalID')
                ->where("Type", Auth::user()->WarehouseCheck)
                ->where('VAT', '!=', Auth::user()->SeeNPPN)
                ->where(function($query) use ($input, $date) {
                    $query->where("SalesID", "like", $input)
                    ->orWhere("SalesDate", "like", '%' . $date . '%')
                    ->orWhere("ACC6Name", "like", '%' . $input . '%');
                })
                ->OrderBy('SalesDate', 'desc')
                ->select('t_sales_header.*', 'm_coa6.ACC6Name')
                ->get();
        if (count($salesReturnHeader) == 0) {
            ?>
            <span>Sales Order with keywords <i><u><?php echo Input::get("id"); ?></u></i> can't be found</span>
            <?php
        } else {
            ?>
            <select class="chosen-select choosen-modal" id="sales" style="" name="sales">
                <?php
                foreach ($salesReturnHeader as $sales) {
                    if (checkSalesReturn($sales->InternalID) && $hitung < 100) {
                        ?>
                        <option value="<?php echo $sales->SalesID ?>"><?php echo $sales->SalesID . ' | ' . date("d-m-Y", strtotime($sales->SalesDate)) . ' | ' . $sales->coa6->ACC6Name ?></option>
                        <?php
                        $hitung++;
                    }
                }
                ?>
            </select>
            <script>
                $(document).ready(function () {
                    var hitung = '<?php echo $hitung; ?>';
                    if (hitung == 0) {
                        $('#sales').after('<span>There is no result.</span>');
                        $('#sales').remove();
                    } else {
                        $("#btn-add-sr").removeAttr("disabled");
                    }
                });
            </script>
            <?php
        }
    }

    //===================================//ajax=======================================
    public function salesReturnPrintStruk($id) {
        $id = SalesReturnHeader::getIdsalesReturn($id);
        $header = SalesReturnHeader::find($id);
        $detail = SalesReturnHeader::find($id)->salesReturnDetail()->get();
        $html = '<!DOCTYPE html>
        <html id="printSales" style="width: 118mm; height: 150mm;">
            <head>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Receipt</title>
                <style>
                    @media print{@page { size: 89mm auto;}}
                    @page {
                        margin-top: 0;
                        margin-bottom: 0;
                        size: 89mm auto;
                    }

                    body {
                        font-family: calibri; 
                        width: 100%;
                        margin: 0 auto;
                    }
                    
                    body * {
                        letter-spacing: 0.6mm !important;
                    }
                    
                    h4 {
                        font-size: 18px;
                        letter-spacing: 0.4px;
                        margin-bottom: 2px;
                    }

                    h3 {
                        font-size: 16px;
                        letter-spacing: 0.4px;
                        margin-top: 0px;
                        margin-bottom: 2px;
                    }

                    p {
                        font-size: 14px;
                        letter-spacing: 0.2px;
                    }

                    table {
                        border-collapse: separate;
                        font-size: 16px;
                        letter-spacing: 0.2px;
                        width: 100%;
                        margin: 0 auto;
                    }

                    table td {
                        vertical-align: middle;
                        padding: -10px;
                    }

                </style>
            </head>';
        $html .= '<body>
        <h4 style="text-align: center;">' . Auth::user()->company->CompanyName . '</h4>
        <p style="text-align: center;">' . Auth::user()->company->Address . ', ' . Auth::user()->company->City . '</p>
        <p style="text-align: center;">Telp : ' . Auth::user()->company->Phone . ', Fax : ' . Auth::user()->company->Fax . '</p>    
        <h3 style="text-align: center;">Sales Return</h3>';

        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $html .= '<table style="border-top: 0.4px solid #000000; border-bottom: 0.4px solid #000000;">
                <tr>
                    <td>' . $header->SalesReturnID . '</td>
                    <td colspan="2" style="text-align:right;">' . Auth::user()->UserID . '</td>
                </tr>
                <tr>
                    <td colspan="3">
                       ' . date("d-m-Y", strtotime($header->SalesDate)) . '
                    </td>
                </tr>
                </table>';

            $html .= '<table style="border-bottom: 0.4px solid #000000;font-size: 18px;">';
//loop data inventory
            $count = 0;
            $total = 0;
            $totalVAT = 0;
            $i = 1;
            foreach ($detail as $data) {
                if ($data->SalesReturnParcelInternalID == 0) {
                    $inventory = Inventory::find($data->InventoryInternalID);
                    if ($inventory->TextPrint != '' || $inventory->TextPrint != Null) {
                        $inv = $inventory->InventoryID . '- ' . $inventory->TextPrint;
                    } else {
                        $inv = $inventory->InventoryID . '- ' . $inventory->InventoryName;
                    }

                    $html .= '<tr>
                                <td colspan="4">
                                    ' . $i . '. ' . $inv . '
                                </td>
                            </tr>';
                    $html .= '<tr>
                                    <td style="text-align: right; width: 20%">' . number_format($data->Qty, 0, '.', ',') . ' ' . $data->Uom->UomID . '</td>
                                    <td style="text-align: right; width: 10%">x</td>
                                    <td style="text-align: right; width: 20%">' . number_format($data->Price, 2, ".", ",") . '</td>
                                    <td style="text-align: right; width: 30%">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                              </tr>';
                    $html .= '<tr>  
                                    <td colspan="4" style="font-size:12px;" ><div style="margin-left:64px">( Disc : ' . $data->Discount . '% , Disc : ' . number_format($data->DiscountNominal, '2', '.', ',') . ')</div></td>
                              </tr>';
                    $count++;
                    $i++;
                    $totalVAT += $data->VAT;
                    $total += $data->SubTotal;
                    if ($totalVAT != 0) {
                        $totalVAT = $totalVAT - ($header->DiscountGlobal * 0.1);
                    }
                }
            }
            foreach (SalesReturnParcel::where("SalesReturnInternalID", $header->InternalID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->get() as $data) {

                $html .= '<tr>
                                <td colspan="4">
                                    ' . $i . '. ' . $data->Parcel->ParcelID . ' - ' . $data->Parcel->ParcelName . '
                                </td>
                            </tr>';
                $html .= '<tr>
                                    <td style="text-align: right; width: 20%">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                    <td style="text-align: right; width: 10%">x</td>
                                    <td style="text-align: right; width: 20%">' . number_format($data->Price, 2, ".", ",") . '</td>
                                    <td style="text-align: right; width: 30%">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                              </tr>';
                $html .= '<tr>
                                    <td colspan="4" style="font-size:10px; ">( Disc : ' . $data->Discount . '% , Disc : ' . number_format($data->DiscountNominal, '2', '.', ',') . ')</td>
                              </tr>';
                $count++;
                $i++;
                if ($data->VAT != 0) {
                    $totalVAT += ($data->Price * $data->Qty) * 0.1;
                }
                $total += $data->Price * $data->Qty;
            }

            $html .= '</table>';


            $html .= '<table style="margin-bottom: 7px;font-size: 18px;">
                        <tr>
                            <td>Sum : ' . $count . ' item</td>
                            <td style="text-align: right;">Total</td>
                            <td style="text-align: right;">' . number_format($total, '2', '.', ',') . '</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: right;">Discount</td>
                            <td style="text-align: right;">' . number_format($header->DiscountGlobal, '2', '.', ',') . '</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: right;">Grand Total</td>
                            <td style="text-align: right;">' . number_format($total - $header->DiscountGlobal, '2', '.', ',') . '</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: right;">Tax</td>
                            <td style="text-align: right;">(' . $totalVAT . ')</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: right;"><strong>Grand Total</strong></td>
                            <td style="border-bottom: 1px solid #000000; text-align: right;">' . number_format($header->GrandTotal, '2', '.', ',') . '</td>
                        </tr>';
            $html .= '</table>

                    <ul style="page-break-after:always; font-size: 12px; list-style: none; display: block; padding: 0; margin: 0 auto;">
                        <li style="margin-bottom: 2px; line-height: 14px; text-align: center;">Thank you for your visit.</li>
                        <li style="line-height: 14px; text-align: center;">Please come again later.</li>
                    </ul>
                    <script src="' . Asset("") . 'lib/bootstrap/js/jquery-1.11.1.min.js"></script>
                    <script>
                    
                        $(document).ready(function(e){
                        window.print();
                        });
                        $(document).click(function(e){
                        window.location.assign("' . URL("/") . '");
                        });
                    </script>
                </body>
            </html>';

            echo $html;
        }
    }

    static function salesReturnDataBackup($data) {
        $explode = explode('---;---', $data);
        $coa6 = $explode[0];
        $typePayment = $explode[1];
        $typeTax = $explode[2];
        $start = $explode[3];
        $end = $explode[4];
        $where = '';
        if ($typePayment != '-1' && $typePayment != '') {
            $where .= 'isCash = "' . $typePayment . '" ';
        }
        if ($coa6 != '-1' && $coa6 != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'ACC6InternalID = ' . $coa6 . ' ';
        }
        if (Auth::user()->SeeNPPN == 1) {
//            if ($typeTax != '-1' && $typeTax != '') {
//                if ($where != '') {
//                    $where .= ' AND ';
//                }
//                $where .= 'VAT = "' . $typeTax . '" ';
//            }
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'VAT = "0" ';
        } else {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'VAT = "1" ';
        }

        if ($where != '') {
            $where .= ' AND ';
        }
        $where .= 'm_warehouse.Type = ' . Auth::user()->WarehouseCheck . ' ';

        if ($start != '' && $end != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $start = explode('-', $start);
            $end = explode('-', $end);
            $startSearch = $start[2] . '-' . $start[1] . '-' . $start[0];
            $endSearch = $end[2] . '-' . $end[1] . '-' . $end[0];
            $where .= 'SalesReturnDate between "' . $startSearch . ' 00:00:00" AND "' . $endSearch . ' 23:59:59"';
        }
        $table = 't_salesreturn_header';
        $primaryKey = 't_salesreturn_header`.`InternalID';
        $columns = array(
            array('db' => 't_salesreturn_header`.`InternalID', 'dt' => 0, 'formatter' => function($d, $row) {
                    return $d;
                }),
            array('db' => 'SalesReturnID', 'dt' => 1),
            array('db' => 'isCash', 'dt' => 2, 'formatter' => function( $d, $row ) {
                    if ($d == 0) {
                        return 'Cash';
                    } else if ($d == 1) {
                        return 'Credit';
                    } else if ($d == 2) {
                        return 'CBD';
                    } else if ($d == 3) {
                        return 'Deposit';
                    } else {
                        return 'Down Payment';
                    }
                },
                'field' => 't_salesreturn_header`.`InternalID'),
            array(
                'db' => 'SalesReturnDate',
                'dt' => 3,
                'formatter' => function( $d, $row ) {
                    return date("d-m-Y", strtotime($d));
                }
            ),
            array('db' => 'CurrencyName', 'dt' => 4),
            array(
                'db' => 'CurrencyRate',
                'dt' => 5,
                'formatter' => function( $d, $row ) {
                    return number_format($d, '2', '.', ',');
                }
            ),
            array(
                'db' => 'ACC6Name',
                'dt' => 6,
                'formatter' => function( $d, $row ) {
                    return $d;
                }
            ),
//            array(
//                'db' => 'VAT',
//                'dt' => 6,
//                'formatter' => function( $d, $row ) {
//                    if ($d == 0) {
//                        return 'Non Tax';
//                    } else {
//                        return 'Tax';
//                    }
//                }
//            ),
            array(
                'db' => 'GrandTotal',
                'dt' => 7,
                'formatter' => function( $d, $row ) {
                    return number_format($d, '2', '.', ',');
                }
            ),
            array(
                'db' => 'Print',
                'dt' => 8,
                'formatter' => function($d, $row) {
                    return $d . " times";
                }
            ),
            array('db' => 't_salesreturn_header`.`InternalID', 'dt' => 9, 'formatter' => function( $d, $row ) {
                    $data = SalesReturnHeader::find($d);
                    $action = '<td class="text-center">
                                    <a href="' . Route('salesReturnDetail', $data->SalesReturnID) . '">
                                        <button id="btn-' . $data->SalesReturnID . '-detail"
                                                class="btn btn-pure-xs btn-xs btn-detail" title="detail">
                                            <span class="glyphicon glyphicon-zoom-in"></span>
                                        </button>
                                    </a>';
                    $action .= '<a href="' . Route('salesReturnUpdate', $data->SalesReturnID) . '">
                                        <button id="btn-' . $data->SalesReturnID . '-update"
                                                class="btn btn-pure-xs btn-xs btn-edit" title="update">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </button>
                                    </a>
                                    <button data-target="#m_salesReturnDelete" data-internal="' . $data->InternalID . '"  data-toggle="modal" role="dialog" title="delete"
                                           onclick="deleteAttach(this)" data-id="' . $data->SalesReturnID . '" data-name=' . $data->SalesReturnID . ' class="btn btn-pure-xs btn-xs btn-delete">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>';
//                    $action .='<a title="print" href="'.Route('salesReturnPrint',$data->SalesReturnID).'">'
//                            . '<button id="btn-'.$data->SalesReturnID.'-print"'
//                            . ' class="btn btn-pure-xs btn-xs btn-print">'
//                            . '<span class="glyphicon glyphicon-print"></span>'
//                            . '</button></a>';
//                    $action .= '<button data-target="#r_print" data-internal="' . $data->SalesReturnID . '"  data-toggle="modal" role="dialog" title="print"
//                                           onclick="printAttach(this)" data-id="' . $data->SalesReturnID . '" data-name=' . $data->SalesReturnID . ' class="btn btn-pure-xs btn-xs btn-closed">
//                                        <span class="glyphicon glyphicon-print"></span>
//                                    </button>';
                    $action .= '<a target="_blank" title="print" href="' . route('salesReturnPrint', $data->SalesReturnID) . '">'
                            . '<button id="btn-' . $data->SalesReturnID . '-print"'
                            . 'class="btn btn-pure-xs btn-xs btn-print" title="print">'
                            . '<span class="glyphicon glyphicon-print"></span>'
                            . '</button></a>';
//                      <a href="' . Route('salesReturnPrintStruk', $data->SalesReturnID) . '" target="_blank">
//                                        <button id="btn-' . $data->SalesReturnID . '-update"
//                                                class="btn btn-pure-xs btn-xs btn-edit">
//                                            <span class="glyphicon glyphicon-print"></span> Struk
//                                        </button>
//                                    </a>    
                    return $action;
                },
                'field' => 't_salesreturn_header`.`InternalID')
        );

        $sql_details = getConnection();

        require('ssp.class.php');
        $ID_CLIENT_VALUE = Auth::user()->CompanyInternalID;
        if ($where != '') {
            $extraCondition = $where . ' AND ' . 't_salesreturn_header.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        } else {
            $extraCondition = 't_salesreturn_header.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        }
        $join = ' INNER JOIN m_currency on m_currency.InternalID = t_salesreturn_header.CurrencyInternalID '
                . 'INNER JOIN m_coa6 on m_coa6.InternalID = t_salesreturn_header.ACC6InternalID '
                . 'INNER JOIN m_warehouse on m_warehouse.InternalID = t_salesreturn_header.WarehouseInternalID';

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $extraCondition, $join));
    }

}
