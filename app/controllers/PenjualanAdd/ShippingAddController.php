<?php

class ShippingAddController extends BaseController {

    public function showShipping() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteShipping') {
                return $this->deleteShipping();
            } else if (Input::get('jenis') == 'summaryShipping') {
                return $this->summaryShipping();
            } else if (Input::get('jenis') == 'detailShipping') {
                return $this->detailShipping();
            } else if (Input::get('jenis') == 'insertShipping') {
                return Redirect::Route('shippingNew', Input::get('shipping'));
            } else if (Input::get('jenis') == 'insertPacking') {
                return Redirect::Route('packingNew', Input::get('shipping'));
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = ShippingAddHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
            return View::make('penjualanAdd.shippingAddSearch')
                            ->withToogle('transaction')->withAktif('shipping')
                            ->withData($data);
        }
        return View::make('penjualanAdd.shippingAdd')
                        ->withToogle('transaction')->withAktif('shipping');
    }

    public function showPackingList() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deletePacking') {
                return $this->deletePacking();
            } else if (Input::get('jenis') == 'detailPacking') {
                return $this->detailPacking();
            } else if (Input::get('jenis') == 'insertPacking') {
//                $shipping = '';
//                foreach (Input::get('shippinglist') as $shipping2) {
//                    $shipping .= $shipping2 . ',';
//                }
//                $shipping = substr($shipping, 0, -1);
//                return Redirect::Route('packingNew', $shipping);
                return Redirect::Route('packingNew', Input::get('shipping'));
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = Packing::advancedSearch(Input::get('startDate'), Input::get('endDate'));
            return View::make('penjualanAdd.packingSearch')
                            ->withToogle('transaction')->withAktif('shipping')
                            ->withData($data);
        }
        return View::make('penjualanAdd.packing')
                        ->withToogle('transaction')->withAktif('shipping');
    }

    public function shippingNew($id) {
        $id = SalesOrderHeader::getIdsalesOrder($id);
        $header = SalesOrderHeader::find($id);
        $detail = SalesOrderHeader::find($id)->salesOrderDetail()->get();
        $description = SalesOrderHeader::find($id)->SalesOrderDescription()->get();
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteShipping') {
                return $this->deleteShipping();
            } else if (Input::get('jenis') == 'summaryShipping') {
                return $this->summaryShipping();
            } else if (Input::get('jenis') == 'detailShipping') {
                return $this->detailShipping();
            } else if (Input::get('jenis') == 'insertShipping') {
                return Redirect::Route('shippingNew', Input::get('shipping'));
            } else if (Input::get('jenis') == 'insertPacking') {
                return Redirect::Route('packingNew', Input::get('shipping'));
            } else {
                return $this->insertShipping($id);
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = ShippingAddHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
            return View::make('penjualanAdd.shippingAddSearch')
                            ->withToogle('transaction')->withAktif('shipping')
                            ->withData($data);
        }
        if ($header->VAT == 0)
            $tax = "NonTax";
        else
            $tax = "Tax";
        $shipping = $this->createID(0, $tax) . '.';
        return View::make('penjualanAdd.shippingAddNew')
                        ->withToogle('transaction')->withAktif('shipping')
                        ->withHeader($header)
                        ->withDetail($detail)
                        ->withDescription($description)
                        ->withShipping($shipping);
    }

    public function packingNew($id) {
//        $dataSales = explode(',', $id);
//        $header = array();
//        $detail = array();
//        $description = array();
        //yang lama
//        foreach ($dataSales as $value) {
//            $int = ShippingAddHeader::getIdShipping($value);
//            array_push($header, ShippingAddHeader::find($int));
//            array_push($detail, ShippingAddHeader::find($int)->shippingDetail()->get());
//            array_push($description, ShippingAddHeader::find($int)->shippingDescription()->get());
//        }
        //yang baru
//        
        $id = SalesOrderHeader::getIdsalesOrder($id);
        $header = SalesOrderHeader::find($id);
//        $detail = SalesOrderHeader::find($id)->salesOrderDetail()->get();
        $description = SalesOrderHeader::find($id)->SalesOrderDescription()->get();

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deletePacking') {
                return $this->deletePacking();
            } else if (Input::get('jenis') == 'summaryPacking') {
                return $this->summaryPacking();
            } else if (Input::get('jenis') == 'detailPacking') {
                return $this->detailPacking();
            } else if (Input::get('jenis') == 'insertPacking') {
//                $shipping = '';
//                foreach (Input::get('shippinglist') as $shipping2) {
//                    $shipping .= $shipping2 . ',';
//                }
//                $shipping = substr($shipping, 0, -1);
//                return Redirect::Route('packingNew', $shipping);
                return Redirect::Route('packingNew', Input::get('shipping'));
            } else {
                return $this->insertPacking($id);
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = Packing::advancedSearch(Input::get('startDate'), Input::get('endDate'));
            return View::make('penjualanAdd.packingSearch')
                            ->withToogle('transaction')->withAktif('shipping')
                            ->withData($data);
        }
        $packing = $this->createIDPacking(0) . '.';
        return View::make('penjualanAdd.packingNew')
                        ->withToogle('transaction')->withAktif('shipping')
                        ->withId($id)
                        ->withHeader($header)
//                        ->withDetail($detail)
                        ->withDescriptionn($description)
                        ->withPacking($packing);
    }

    public function packingUpdate($id) {
//        $packing = Packing::where('PackingID', $id)->first();
//        $header = array();
//        $detail = array();
//        $description = array();
//        $dataSales = PackingShipping::where('PackingInternalID', Packing::getIdPacking($id))->get();
//        foreach ($dataSales as $value) {
//            $int = $value->ShippingInternalID;
//            array_push($header, ShippingAddHeader::find($int));
//            array_push($detail, ShippingAddHeader::find($int)->shippingDetail()->get());
//            array_push($description, ShippingAddHeader::find($int)->shippingDescription()->get());
//        }
//        $idSalesOrder = $header->SalesOrderInternalID;
//        $headerSalesOrder = SalesOrderHeader::find($idSalesOrder);
//        $detailSalesOrder = SalesOrderHeader::find($idSalesOrder)->salesOrderDetail()->get();
        $header = ShippingAddHeader::where('PackingID', $id)->first();
        $packing = ShippingAddHeader::where('PackingID', $id)->orderBy('InternalID', 'asc')->get();
        $detailship = ShippingAddDetail::join('t_shipping_header', 't_shipping_header.InternalID', '=', 't_shipping_detail.ShippingInternalID')
                        ->where('PackingID', $id)->orderBy('t_shipping_detail.InternalID', 'asc')->get();
        $sodet = SalesOrderHeader::find($header->SalesOrderInternalID)->salesOrderDetail;
        $description = SalesOrderHeader::find($header->SalesOrderInternalID)->SalesOrderDescription()->get();
//        $detailship = ShippingAddDetail::find($header->SalesOrderInternalID)->SalesOrderDescription()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                if (Input::get('jenis') == 'summaryPacking') {
                    return $this->summaryPacking();
                } else if (Input::get('jenis') == 'detailPacking') {
                    return $this->detailPacking();
                } else if (Input::get('jenis') == 'insertPacking') {
                    return Redirect::Route('packingNew', Input::get('packing'));
                } else if (Input::get('jenis') == 'deletePacking') {
                    return $this->deletePacking();
                } else {
                    return $this->updatePacking($id);
                }
            } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
                $data = Packing::advancedSearch(Input::get('startDate'), Input::get('endDate'));
                return View::make('penjualanAdd.packingSearch')
                                ->withToogle('transaction')->withAktif('shipping')
                                ->withData($data);
            }
            return View::make('penjualanAdd.packingUpdate')
                            ->withToogle('transaction')->withAktif('shipping')
                            ->withHeader($header)
                            ->withPacking($packing)
                            ->withDetaill($detailship)
                            ->withSodet($sodet)
                            ->withDescriptionn($description);
//                            ->withHeaderorder($headerSalesOrder)
//                            ->withDetailorder($detailSalesOrder);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showPackingList');
        }
    }

    public function shippingDetail($id) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'summaryShipping') {
                return $this->summaryShipping();
            } else if (Input::get('jenis') == 'detailShipping') {
                return $this->detailShipping();
            } else if (Input::get('jenis') == 'insertShipping') {
                return Redirect::Route('shippingNew', Input::get('shipping'));
            } else if (Input::get('jenis') == 'deleteShipping') {
                return $this->deleteShipping();
            } else if (Input::get('jenis') == 'insertPacking') {
                return Redirect::Route('packingNew', Input::get('shipping'));
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = ShippingAddHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
            return View::make('penjualanAdd.shippingAddSearch')
                            ->withToogle('transaction')->withAktif('shipping')
                            ->withData($data);
        }
        $id = ShippingAddHeader::getIdshipping($id);
        $header = ShippingAddHeader::find($id);
        $detail = ShippingAddHeader::find($id)->shippingDetail()->get();

        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            return View::make('penjualanAdd.shippingAddDetail')
                            ->withToogle('transaction')->withAktif('shipping')
                            ->withHeader($header)
                            ->withDetail($detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showShipping');
        }
    }

    public function packingDetail($id) {
//        $dataSales = explode(',', $id);
        $header = array();
        $detail = array();
        $description = array();
        $dataSales = PackingShipping::where('PackingInternalID', Packing::getIdPacking($id))->get();
        foreach ($dataSales as $value) {
            $int = $value->ShippingInternalID;
            array_push($header, ShippingAddHeader::find($int));
            array_push($detail, ShippingAddHeader::find($int)->shippingDetail()->get());
            array_push($description, ShippingAddHeader::find($int)->shippingDescription()->get());
        }
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'summaryPacking') {
                return $this->summaryPacking();
            } else if (Input::get('jenis') == 'insertPacking') {
                $shipping = '';
                foreach (Input::get('shippinglist') as $shipping2) {
                    $shipping .= $shipping2 . ',';
                }
                $shipping = substr($shipping, 0, -1);
                return Redirect::Route('packingNew', $shipping);
            } else if (Input::get('jenis') == 'deletePacking') {
                return $this->deletePacking();
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = Packing::advancedSearch(Input::get('startDate'), Input::get('endDate'));
            return View::make('penjualanAdd.packingSearch')
                            ->withToogle('transaction')->withAktif('shipping')
                            ->withData($data);
        }
        $id = Packing::getIdPacking($id);
        $header = Packing::find($id);
        if ($header->CompanyInternalID == Auth::user()->CompanyInternalID) {
            $a = PackingShipping::where('PackingInternalID', $id)->get();
            $string = '';
            foreach ($a as $b) {
                $string .= $b->shipping->ShippingID . ',';
            }
            return View::make('penjualanAdd.packingDetail')
                            ->withToogle('transaction')->withAktif('shipping')
                            ->withHeader($header)
                            ->withId($string)
                            ->withDetail($detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showPackingList');
        }
    }

    public function shippingUpdate($id) {
        $id = ShippingAddHeader::getIdshipping($id);
        $header = ShippingAddHeader::find($id);
        $detail = ShippingAddHeader::find($id)->shippingDetail()->get();
        $description = ShippingAddHeader::find($id)->shippingDescription()->get();
        $idSalesOrder = $header->SalesOrderInternalID;
        $headerSalesOrder = SalesOrderHeader::find($idSalesOrder);
        $detailSalesOrder = SalesOrderHeader::find($idSalesOrder)->salesOrderDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                if (Input::get('jenis') == 'summaryShipping') {
                    return $this->summaryShipping();
                } else if (Input::get('jenis') == 'detailShipping') {
                    return $this->detailShipping();
                } else if (Input::get('jenis') == 'insertShipping') {
                    return Redirect::Route('shippingNew', Input::get('shipping'));
                } else if (Input::get('jenis') == 'insertPacking') {
                    return Redirect::Route('packingNew', Input::get('shipping'));
                } else if (Input::get('jenis') == 'deleteShipping') {
                    return $this->deleteShipping();
                } else {
//                    if ($header->PackingID == null || $header->PackingID == '') {
                    return $this->updateShipping($id);
//                    } else {
//                        return $this->updatePacking($id);
//                    }
                }
            } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
                $data = ShippingAddHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
                return View::make('penjualanAdd.shippingAddSearch')
                                ->withToogle('transaction')->withAktif('shipping')
                                ->withData($data);
            }
//            if ($header->PackingID == null || $header->PackingID == '') {
            return View::make('penjualanAdd.shippingAddUpdate')
                            ->withToogle('transaction')->withAktif('shipping')
                            ->withHeader($header)
                            ->withDet($detail)
                            ->withDesc($description)
                            ->withHeaderorder($headerSalesOrder)
                            ->withDetailorder($detailSalesOrder);
//            } else {
//                return View::make('penjualanAdd.packingUpdate')
//                                ->withToogle('transaction')->withAktif('shipping')
//                                ->withHeader($header)
//                                ->withDet($detail)
//                                ->withDesc($description)
//                                ->withHeaderorder($headerSalesOrder)
//                                ->withDetailorder($detailSalesOrder);
//            }
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showShipping');
        }
    }

    public function insertShipping($id) {
//rule
        $salesOrderInternalID = Input::get('SalesOrderInternalID');
        $salesOrder = SalesOrderHeader::find($salesOrderInternalID);
        if ($salesOrder->isCash == 0) {
            $rule = array(
                'date' => 'required',
//                'remark' => 'required|max:1000',
                'warehouse' => 'required',
//                'slip' => 'required',
                'InternalDescription' => 'required'
            );
        } else {
            $rule = array(
                'date' => 'required',
//                'remark' => 'required|max:1000',
                'warehouse' => 'required',
                'InternalDescription' => 'required'
            );
            $longTerm = 0;
        }
//validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
//tidak valid
            $messages = 'gagalInsert';
            $error = $validator->messages();
        } else {
//valid
//insert header
            $tax = '';
            if ($salesOrder->VAT == 0) {
                $tax = "NonTax";
            } else {
                $tax = "Tax";
            }
            $header = new ShippingAddHeader;
            $shipping = $this->createID(1, $tax) . '.';
            $date = explode('-', Input::get('date'));
            $yearDigit = substr($date[2], 2);
            $shipping .= $date[1] . $yearDigit . '.';
            $shippingNumber = ShippingAddHeader::getNextIDShipping($shipping);
            $header->ShippingID = $shippingNumber;
            $header->ShippingDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $header->ACC6InternalID = $salesOrder->ACC6InternalID;
            $header->LongTerm = $salesOrder->LongTerm;
            $header->isCash = $salesOrder->isCash;
            $header->WarehouseInternalID = Input::get('warehouse');
            $header->VAT = $salesOrder->VAT;
            $header->TransactionType = Input::get('TransactionType');
            if (Input::get('Replacement') == '') {
                $header->Replacement = '0';
            } else {
                $header->Replacement = Input::get('Replacement');
            }
            if (Input::get('TaxNumber') == '') {
                $header->TaxNumber = '.-.';
            } else {
                $header->TaxNumber = Input::get('TaxNumber');
            }
            $header->TaxMonth = Input::get('TaxMonth');
            $header->TaxYear = Input::get('TaxYear');
            $header->TaxDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $header->NumberVehicle = Input::get('vehicle');
            $header->DriverName = Input::get('driver');
            $header->Print = 1;
            $header->UserRecord = Auth::user()->UserID;
            $header->CompanyInternalID = Auth::user()->Company->InternalID;
            $header->SalesOrderInternalID = $salesOrderInternalID;
            $header->UserModified = '0';
            $header->Remark = Input::get('remark');
            $header->save();
//insert detail
            $total = 0;
//            for ($a = 0; $a < count(Input::get('InternalDescription')); $a++) {
//                echo "<pre>";
//                print_r(Input::get('InternalDetail' . ($a + 1)));
//                echo "</pre>";
//                echo "<pre>";
//                print_r(Input::get('tipe' . ($a + 1)));
//                echo "</pre>";
//            }exit();
            for ($a = 0; $a < count(Input::get('InternalDescription')); $a++) {
                if (Input::get('qtyDescription')[$a] > 0) {
                    $data = SalesOrderDescription::find(Input::get('InternalDescription')[$a]);
                    $description = new ShippingAddDescription();
                    $description->ShippingInternalID = $header->InternalID;
                    $description->InventoryText = $data->InventoryText;
                    $description->UomText = $data->UomText;
                    $qty = 0;
                    if (Input::get('max')[$a] < Input::get('qtyDescription')[$a])
                        $qty = Input::get('max')[$a];
                    else
                        $qty = Input::get('qtyDescription')[$a];
                    $description->Qty = $qty;
                    $description->Spesifikasi = $data->Spesifikasi;
                    $description->SalesOrderDescriptionInternalID = Input::get('InternalDescription')[$a];
                    $description->UserRecord = Auth::user()->UserID;
                    $description->UserModified = '0';
                    $description->save();

                    for ($c = 0; $c < count(Input::get('InternalDetail' . ($a + 1))); $c++) {
                        if (Input::get('tipe' . ($a + 1))[$c] == "inventory") {
                            $detail = SalesOrderDetail::find(Input::get('InternalDetail' . ($a + 1))[$c]);
//jika non paket
                            $qtyValue = $detail->Qty;
                            $qtyShippingValue = str_replace(',', '', Input::get('qty' . ($a + 1))[$c]);
                            $sumShipping = ShippingAddHeader::getSumShipping($detail->InventoryInternalID, $salesOrderInternalID, Input::get('InternalDetail' . ($a + 1))[$c]);
                            if ($sumShipping == '') {
                                $sumShipping = '0';
                            }
                            $qtyValue = $qtyValue - $sumShipping;
                            if ($qtyValue < $qtyShippingValue) {
                                $qtyShippingValue = $qtyValue;
                            }
                            if ($qtyShippingValue > 0) {
                                $detail2 = new ShippingAddDetail();
                                $detail2->ShippingInternalID = $header->InternalID;
                                $detail2->InventoryInternalID = $detail->InventoryInternalID;
                                $detail2->DescriptionInternalID = $description->InternalID;
                                $detail2->UomInternalID = $detail->UomInternalID;
                                $detail2->Qty = $qtyShippingValue;
                                $detail2->UserRecord = Auth::user()->UserID;
                                $detail2->UserModified = '0';
                                $detail2->SalesOrderDetailInternalID = Input::get('InternalDetail' . ($a + 1))[$c];
                                $detail2->save();
                                setTampInventory($detail->InventoryInternalID);
                            }
                        } else {
                            $detail = SalesOrderParcel::find(Input::get("InternalDetail" . ($a + 1))[$c]);
//jika paket
                            $parcelDetail = ParcelInventory::where("ParcelInternalID", $detail->ParcelInternalID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->get();
                            $qtyValue = $detail->Qty;
                            $qtyShippingValue = str_replace(',', '', Input::get('qty' . ($a + 1))[$c]);
                            $sumShipping = ShippingAddHeader::getSumShippingParcel($detail->ParcelInternalID, "", Input::get('InternalDetail' . ($a + 1))[$c]);
                            if ($sumShipping == '') {
                                $sumShipping = '0';
                            }
                            $qtyValue = $qtyValue - $sumShipping;
                            if ($qtyValue < $qtyShippingValue) {
                                $qtyShippingValue = $qtyValue;
                            }
                            if ($qtyShippingValue > 0) {
                                $SAParcel = new ShippingAddParcel();
                                $SAParcel->ShippingInternalID = $header->InternalID;
                                $SAParcel->ParcelInternalID = $detail->ParcelInternalID;
                                $SAParcel->DescriptionInternalID = $description->InternalID;
                                $SAParcel->SalesOrderParcelDetailInternalID = Input::get("InternalDetail" . ($a + 1))[$c];
                                $SAParcel->Qty = $qtyShippingValue;
                                $SAParcel->CompanyInternalID = Auth::user()->Company->InternalID;
                                $SAParcel->UserRecord = Auth::user()->UserID;
                                $SAParcel->UserModified = "0";
                                $SAParcel->Remark = "-";
                                $SAParcel->save();

                                foreach ($parcelDetail as $dataParcel) {
                                    $uom1 = $dataParcel->UomInternalID;
                                    $qtyValue1 = $dataParcel->Qty * $qtyShippingValue;
                                    $detail2 = new ShippingAddDetail();
                                    $detail2->ShippingInternalID = $header->InternalID;
                                    $detail2->InventoryInternalID = $dataParcel->InventoryInternalID;
                                    $detail2->UomInternalID = $uom1;
                                    $detail2->ShippingParcelInternalID = $SAParcel->InternalID;
                                    $detail2->Qty = $qtyValue1;
                                    $detail2->SalesOrderDetailInternalID = $salesOrderInternalID;
                                    $detail2->UserRecord = Auth::user()->UserID;
                                    $detail2->UserModified = '0';
                                    $detail2->save();
                                    setTampInventory($dataParcel->InventoryInternalID);
                                }
                            }
                        }
                    }
//                $SalesOrderdescription = SalesOrderHeader::find($salesOrderInternalID)->salesOrderDescription;
                }
            }
            $messages = 'suksesInsert';
            $error = '';

            //kalau non ppn insert ke inventory transfer!!
            if ($header->VAT == 0) {
                $headerT = new TransferHeader;
                $transfer = $this->createIDTransfer(1) . '.';
                $date = explode('-', Input::get('date'));
                $yearDigit = substr($date[2], 2);
                $transfer .= $date[1] . $yearDigit . '.';
                $transferNumber = TransferHeader::getNextIDTransfer($transfer);
                $headerT->TransferID = $transferNumber;
                $headerT->TransferDate = $date[2] . '-' . $date[1] . '-' . $date[0];
                $headerT->WarehouseInternalID = $header->WarehouseInternalID;
                $headerT->WarehouseDestinyInternalID = Warehouse::where("Type", 1)->first()->InternalID;
                $headerT->TransferType = 0;
                $headerT->UserRecord = Auth::user()->UserID;
                $headerT->CompanyInternalID = Auth::user()->Company->InternalID;
                $headerT->UserModified = '0';
                $headerT->Remark = $header->ShippingID;
                $headerT->save();
                //insert detail
                $total = 0;
                foreach (ShippingAddDetail::where("ShippingInternalID", $header->InternalID)->get() as $s) {
                    $detail = new TransferDetail();
                    $detail->TransferInternalID = $headerT->InternalID;
                    $detail->InventoryInternalID = $s->InventoryInternalID;
                    $detail->UomInternalID = $s->UomInternalID;
                    $detail->Qty = $s->Qty;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = '0';
                    $detail->save();
                    setTampInventory($s->InventoryInternalID);
                }
            }
        }
        $shipping = $this->createID(0) . '.';

        if ($messages == 'suksesInsert') {
            return Redirect::route('shippingDetail', $header->ShippingID)->with("msg", "print");
        }
        
        if ($messages == 'suksesInsert') {
            $print = Route('shippingPrint', $shippingNumber);
        } else {
            $print = '';
        }
        return Redirect::route('showShipping', $header->ShippingID)->with("msg", "print");
        $header = SalesOrderHeader::find($id);
        $detail = SalesOrderHeader::find($id)->salesOrderDetail()->get();
        return View::make('penjualanAdd.shippingAddNew')
                        ->withToogle('transaction')->withAktif('shipping')
                        ->withShipping($shipping)
                        ->withError($error)
                        ->withHeader($header)
                        ->withDetail($detail)
                        ->withPrint($print)
                        ->withMessages($messages);
    }

    public function insertPacking($id) {
        //rule
        $salesOrderInternalID = Input::get('SalesOrderInternalID');
        $salesOrder = SalesOrderHeader::find($salesOrderInternalID);

        $rule = array(
            'date' => 'required'
        );
        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalInsert';
            $error = $validator->messages();
        } else {
            //valid
            //insert header
            //input setiap inputan detail jadi shipping header
            $tax = '';
            if ($salesOrder->VAT == 0) {
                $tax = "NonTax";
            } else {
                $tax = "Tax";
            }
            $date = explode('-', Input::get('date'));
            $yearDigit = substr($date[2], 2);
            $packing = $this->createIDPacking(1, $tax) . '.';
            $packing .= $date[1] . $yearDigit . '.';
            $packing = Packing::getNextIDPacking($packing);
            for ($a = 0; $a < Input::get('jumlahbarisdetail'); $a++) {
                if (Input::get('warehouse' . ($a + 1)) != null && Input::get('warehouse' . ($a + 1)) != '' && Input::get('qty' . ($a + 1)) > 0) {
                    $header = new ShippingAddHeader;
                    $shipping = $this->createID(1, $tax) . '.';
                    $shipping .= $date[1] . $yearDigit . '.';
                    $shippingNumber = ShippingAddHeader::getNextIDShipping($shipping);
                    $header->ShippingID = $shippingNumber;
                    $header->PackingID = $packing;
                    $header->ShippingDate = $date[2] . '-' . $date[1] . '-' . $date[0];
                    $header->ACC6InternalID = $salesOrder->ACC6InternalID;
                    $header->LongTerm = $salesOrder->LongTerm;
                    $header->isCash = $salesOrder->isCash;
                    $header->WarehouseInternalID = Input::get('warehouse' . ($a + 1));
                    $header->VAT = $salesOrder->VAT;
                    $header->NumberVehicle = Input::get('vehicle');
                    $header->DriverName = Input::get('driver');
                    $header->Print = 1;
                    $header->UserRecord = Auth::user()->UserID;
                    $header->CompanyInternalID = Auth::user()->Company->InternalID;
                    $header->SalesOrderInternalID = $salesOrderInternalID;
                    $header->UserModified = '0';
                    $header->Remark = Input::get('remark');
                    $header->save();
//                    $total = 0;
                    //insert description
                    if (explode('---;---', Input::get('inventory' . ($a + 1)))[1] == "inventory") {
                        $detail = SalesOrderDetail::find(explode('---;---', Input::get('inventory' . ($a + 1)))[2]);
                    } else {
                        $detail = SalesOrderParcel::find(explode('---;---', Input::get('inventory' . ($a + 1)))[2]);
                    }
                    $data = SalesOrderDescription::find($detail->DescriptionInternalID);
                    $description = new ShippingAddDescription();
                    $description->ShippingInternalID = $header->InternalID;
                    $description->InventoryText = $data->InventoryText;
                    $description->UomText = $data->UomText;
                    $description->Qty = $data->Qty;
                    $description->Spesifikasi = $data->Spesifikasi;
                    $description->SalesOrderDescriptionInternalID = $detail->DescriptionInternalID;
                    $description->UserRecord = Auth::user()->UserID;
                    $description->UserModified = '0';
                    $description->save();

                    //kalau inventory
                    if (explode('---;---', Input::get('inventory' . ($a + 1)))[1] == "inventory") {
                        $qtyValue = $detail->Qty;
                        $qtyShippingValue = str_replace(',', '', Input::get('qty' . ($a + 1)));
                        $sumShipping = ShippingAddHeader::getSumShipping($detail->InventoryInternalID, $salesOrderInternalID, $detail->InternalID);
                        if ($sumShipping == '') {
                            $sumShipping = '0';
                        }
                        $qtyValue = $qtyValue - $sumShipping;
                        if ($qtyValue < $qtyShippingValue) {
                            $qtyShippingValue = $qtyValue;
                        }
                        if ($qtyShippingValue > 0) {
                            $detail2 = new ShippingAddDetail();
                            $detail2->ShippingInternalID = $header->InternalID;
                            $detail2->InventoryInternalID = $detail->InventoryInternalID;
                            $detail2->DescriptionInternalID = $description->InternalID;
                            $detail2->UomInternalID = $detail->UomInternalID;
                            $detail2->Qty = $qtyShippingValue;
                            $detail2->UserRecord = Auth::user()->UserID;
                            $detail2->UserModified = '0';
                            $detail2->SalesOrderDetailInternalID = $detail->InternalID;
                            $detail2->save();
                            setTampInventory($detail->InventoryInternalID);
                        }
                    } else {
                        //kalau parcel
                        $parcelDetail = ParcelInventory::where("ParcelInternalID", $detail->ParcelInternalID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->get();
                        $qtyValue = $detail->Qty;
                        $qtyShippingValue = str_replace(',', '', Input::get('qty' . ($a + 1)));
                        $sumShipping = ShippingAddHeader::getSumShippingParcel($detail->ParcelInternalID, "", $detail->InternalID);
                        if ($sumShipping == '') {
                            $sumShipping = '0';
                        }
                        $qtyValue = $qtyValue - $sumShipping;
                        if ($qtyValue < $qtyShippingValue) {
                            $qtyShippingValue = $qtyValue;
                        }
                        if ($qtyShippingValue > 0) {
                            $SAParcel = new ShippingAddParcel();
                            $SAParcel->ShippingInternalID = $header->InternalID;
                            $SAParcel->ParcelInternalID = $detail->ParcelInternalID;
                            $SAParcel->DescriptionInternalID = $description->InternalID;
                            $SAParcel->SalesOrderParcelDetailInternalID = $detail->InternalID;
                            $SAParcel->Qty = $qtyShippingValue;
                            $SAParcel->CompanyInternalID = Auth::user()->Company->InternalID;
                            $SAParcel->UserRecord = Auth::user()->UserID;
                            $SAParcel->UserModified = "0";
                            $SAParcel->Remark = "-";
                            $SAParcel->save();

                            //masih salah?
                            foreach ($parcelDetail as $dataParcel) {
                                $uom1 = $dataParcel->UomInternalID;
                                $qtyValue1 = $dataParcel->Qty * $qtyShippingValue;
                                $detail2 = new ShippingAddDetail();
                                $detail2->ShippingInternalID = $header->InternalID;
                                $detail2->InventoryInternalID = $dataParcel->InventoryInternalID;
                                $detail2->UomInternalID = $uom1;
                                $detail2->ShippingParcelInternalID = $SAParcel->InternalID;
                                $detail2->Qty = $qtyValue1;
                                $detail2->SalesOrderDetailInternalID = $salesOrderInternalID;
                                $detail2->UserRecord = Auth::user()->UserID;
                                $detail2->UserModified = '0';
                                $detail2->save();
                                setTampInventory($dataParcel->InventoryInternalID);
                            }
                        }
                    }

                    //insert transfer disini
                    if ($header->VAT == 0) {
                        $headerT = new TransferHeader;
                        $transfer = $this->createIDTransfer(1) . '.';
                        $date = explode('-', Input::get('date'));
                        $yearDigit = substr($date[2], 2);
                        $transfer .= $date[1] . $yearDigit . '.';
                        $transferNumber = TransferHeader::getNextIDTransfer($transfer);
                        $headerT->TransferID = $transferNumber;
                        $headerT->TransferDate = $date[2] . '-' . $date[1] . '-' . $date[0];
                        $headerT->WarehouseInternalID = $header->WarehouseInternalID;
                        $headerT->WarehouseDestinyInternalID = Warehouse::where("Type", 1)->first()->InternalID;
                        $headerT->TransferType = 0;
                        $headerT->UserRecord = Auth::user()->UserID;
                        $headerT->CompanyInternalID = Auth::user()->Company->InternalID;
                        $headerT->UserModified = '0';
                        $headerT->Remark = $header->ShippingID;
                        $headerT->save();
                        //insert detail

                        foreach (ShippingAddDetail::where("ShippingInternalID", $header->InternalID)->get() as $s) {
                            $detail = new TransferDetail();
                            $detail->TransferInternalID = $headerT->InternalID;
                            $detail->InventoryInternalID = $s->InventoryInternalID;
                            $detail->UomInternalID = $s->UomInternalID;
                            $detail->Qty = $s->Qty;
                            $detail->UserRecord = Auth::user()->UserID;
                            $detail->UserModified = '0';
                            $detail->save();
                            setTampInventory($s->InventoryInternalID);
                        }
                    }
//                $SalesOrderdescription = SalesOrderHeader::find($salesOrderInternalID)->salesOrderDescription;
                }
            }
            $messages = 'suksesInsert';
            $error = '';
        }
        
        if ($messages == 'suksesInsert') {
            return Redirect::route('packingDetail', $header->PackingID)->with("msg", "print");
        }
        
        $shipping = $this->createID(0) . '.';

        if ($messages == 'suksesInsert') {
            $print = Route('packingPrint', $packing);
        } else {
            $print = '';
        }
        return Redirect::route('showShipping')->with("msg", "print");
        $header = SalesOrderHeader::find($id);
        $detail = SalesOrderHeader::find($id)->salesOrderDetail()->get();
//        return View::make('penjualanAdd.packingNew')
//                        ->withToogle('transaction')->withAktif('shipping')
//                        ->withShipping($shipping)
//                        ->withError($error)
//                        ->withHeader($header)
//                        ->withDetail($detail)
//                        ->withPrint($print)
//                        ->withMessages($messages);
        return Redirect::Route('packingNew', $header->SalesOrderID);
    }

    public function updatePacking($id) {
        //rule
        $shipping = ShippingAddHeader::where('PackingID', $id)->get();
        $salesOrderInternalID = ShippingAddHeader::where('PackingID', $id)->first()->SalesOrderInternalID;
        $salesOrder = SalesOrderHeader::find($salesOrderInternalID);

        //validasi
//        $data = Input::all();
//        $validator = Validator::make($data, $rule);
//        if ($validator->fails()) {
//            //tidak valid
//            $messages = 'gagalInsert';
//            $error = $validator->messages();
//        } else {
        //hapus shipping header yang tak ada di daftar update
        foreach ($shipping as $sh) {
            //jika tidak ada
            if (count(Input::get('daftarshipping')) > 0 && !in_array($sh->InternalID, Input::get('daftarshipping'))) {
                ShippingAddDetail::where('ShippingInternalID', '=', $sh->InternalID)->delete();
                ShippingAddParcel::where('ShippingInternalID', '=', $sh->InternalID)->delete();
                ShippingAddDescription::where('ShippingInternalID', '=', $sh->InternalID)->delete();
                ShippingAddHeader::where('InternalID', '=', $sh->InternalID)->delete();
            }
            //jika ada
            else {
                $sh->NumberVehicle = Input::get('vehicle');
                $sh->DriverName = Input::get('driver');
                $sh->Remark = Input::get('remark');
                $sh->save();
            }
        }

        //valid
        //insert header
        //input setiap inputan detail jadi shipping header
        $tax = '';
        if ($salesOrder->VAT == 0) {
            $tax = "NonTax";
        } else {
            $tax = "Tax";
        }
        $date = explode('-', $shipping[0]->ShippingDate);
//        $date = explode('-', Input::get('date'));
        $yearDigit = substr($date[0], 2);
//        $yearDigit = substr($date[2], 2);
        $packing = $this->createIDPacking(1, $tax) . '.';
        $packing .= $date[1] . $yearDigit . '.';
        $packing = Packing::getNextIDPacking($packing);
        for ($a = 0; $a < Input::get('jumlahbarisdetail'); $a++) {
            if (Input::get('shipping' . ($a + 1)) == null || Input::get('shipping' . ($a + 1)) == '') {
                if (Input::get('warehouse' . ($a + 1)) != null && Input::get('warehouse' . ($a + 1)) != '' && Input::get('qty' . ($a + 1)) > 0) {
                    $header = new ShippingAddHeader;
                    $shipping = $this->createID(1, $tax) . '.';
                    $shipping .= $date[1] . $yearDigit . '.';
                    $shippingNumber = ShippingAddHeader::getNextIDShipping($shipping);
                    $header->ShippingID = $shippingNumber;
                    $header->PackingID = $id;
                    $header->ShippingDate = $date[0] . '-' . $date[1] . '-' . $date[2];
//                $header->ShippingDate = $date[2] . '-' . $date[1] . '-' . $date[0];
                    $header->ACC6InternalID = $salesOrder->ACC6InternalID;
                    $header->LongTerm = $salesOrder->LongTerm;
                    $header->isCash = $salesOrder->isCash;
                    $header->WarehouseInternalID = Input::get('warehouse' . ($a + 1));
                    $header->VAT = $salesOrder->VAT;
                    $header->NumberVehicle = Input::get('vehicle');
                    $header->DriverName = Input::get('driver');
                    $header->Print = 1;
                    $header->UserRecord = Auth::user()->UserID;
                    $header->CompanyInternalID = Auth::user()->Company->InternalID;
                    $header->SalesOrderInternalID = $salesOrderInternalID;
                    $header->UserModified = '0';
                    $header->Remark = Input::get('remark');
                    $header->save();
//                    $total = 0;
                    //insert description
                    if (explode('---;---', Input::get('inventory' . ($a + 1)))[1] == "inventory") {
                        $detail = SalesOrderDetail::find(explode('---;---', Input::get('inventory' . ($a + 1)))[2]);
                    } else {
                        $detail = SalesOrderParcel::find(explode('---;---', Input::get('inventory' . ($a + 1)))[2]);
                    }
                    $data = SalesOrderDescription::find($detail->DescriptionInternalID);
                    $description = new ShippingAddDescription();
                    $description->ShippingInternalID = $header->InternalID;
                    $description->InventoryText = $data->InventoryText;
                    $description->UomText = $data->UomText;
                    $description->Qty = $data->Qty;
                    $description->Spesifikasi = $data->Spesifikasi;
                    $description->SalesOrderDescriptionInternalID = $detail->DescriptionInternalID;
                    $description->UserRecord = Auth::user()->UserID;
                    $description->UserModified = '0';
                    $description->save();

                    //kalau inventory
                    if (explode('---;---', Input::get('inventory' . ($a + 1)))[1] == "inventory") {
                        $qtyValue = $detail->Qty;
                        $qtyShippingValue = str_replace(',', '', Input::get('qty' . ($a + 1)));
                        $sumShipping = ShippingAddHeader::getSumShipping($detail->InventoryInternalID, $salesOrderInternalID, $detail->InternalID);
                        if ($sumShipping == '') {
                            $sumShipping = '0';
                        }
                        $qtyValue = $qtyValue - $sumShipping;
                        if ($qtyValue < $qtyShippingValue) {
                            $qtyShippingValue = $qtyValue;
                        }
                        if ($qtyShippingValue > 0) {
                            $detail2 = new ShippingAddDetail();
                            $detail2->ShippingInternalID = $header->InternalID;
                            $detail2->InventoryInternalID = $detail->InventoryInternalID;
                            $detail2->DescriptionInternalID = $description->InternalID;
                            $detail2->UomInternalID = $detail->UomInternalID;
                            $detail2->Qty = $qtyShippingValue;
                            $detail2->UserRecord = Auth::user()->UserID;
                            $detail2->UserModified = '0';
                            $detail2->SalesOrderDetailInternalID = $detail->InternalID;
                            $detail2->save();
                            setTampInventory($detail->InventoryInternalID);
                        }
                    } else {
                        //kalau parcel
                        $parcelDetail = ParcelInventory::where("ParcelInternalID", $detail->ParcelInternalID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->get();
                        $qtyValue = $detail->Qty;
                        $qtyShippingValue = str_replace(',', '', Input::get('qty' . ($a + 1)));
                        $sumShipping = ShippingAddHeader::getSumShippingParcel($detail->ParcelInternalID, "", $detail->InternalID);
                        if ($sumShipping == '') {
                            $sumShipping = '0';
                        }
                        $qtyValue = $qtyValue - $sumShipping;
                        if ($qtyValue < $qtyShippingValue) {
                            $qtyShippingValue = $qtyValue;
                        }
                        if ($qtyShippingValue > 0) {
                            $SAParcel = new ShippingAddParcel();
                            $SAParcel->ShippingInternalID = $header->InternalID;
                            $SAParcel->ParcelInternalID = $detail->ParcelInternalID;
                            $SAParcel->DescriptionInternalID = $description->InternalID;
                            $SAParcel->SalesOrderParcelDetailInternalID = $detail->InternalID;
                            $SAParcel->Qty = $qtyShippingValue;
                            $SAParcel->CompanyInternalID = Auth::user()->Company->InternalID;
                            $SAParcel->UserRecord = Auth::user()->UserID;
                            $SAParcel->UserModified = "0";
                            $SAParcel->Remark = "-";
                            $SAParcel->save();

                            //masih salah?
                            foreach ($parcelDetail as $dataParcel) {
                                $uom1 = $dataParcel->UomInternalID;
                                $qtyValue1 = $dataParcel->Qty * $qtyShippingValue;
                                $detail2 = new ShippingAddDetail();
                                $detail2->ShippingInternalID = $header->InternalID;
                                $detail2->InventoryInternalID = $dataParcel->InventoryInternalID;
                                $detail2->UomInternalID = $uom1;
                                $detail2->ShippingParcelInternalID = $SAParcel->InternalID;
                                $detail2->Qty = $qtyValue1;
                                $detail2->SalesOrderDetailInternalID = $salesOrderInternalID;
                                $detail2->UserRecord = Auth::user()->UserID;
                                $detail2->UserModified = '0';
                                $detail2->save();
                                setTampInventory($dataParcel->InventoryInternalID);
                            }
                        }
                    }

                    //kalau non ppn insert ke inventory transfer!!
                    if ($header->VAT == 0) {
                        $headerT = TransferHeader::where("Remark", $header->ShippingID)->first();
                        $headerT->WarehouseInternalID = $header->WarehouseInternalID;
                        $headerT->WarehouseDestinyInternalID = Warehouse::where("Type", 1)->first()->InternalID;
                        $headerT->UserModified = Auth::user()->UserID;
                        $headerT->save();
                        //hapus detail yg lama
                        TransferDetail::where("TransferInternalID", $headerT->InternalID)->delete();
                        //insert detail baru
                        foreach (ShippingAddDetail::where("ShippingInternalID", $header->InternalID)->get() as $s) {
                            $detail = new TransferDetail();
                            $detail->TransferInternalID = $headerT->InternalID;
                            $detail->InventoryInternalID = $s->InventoryInternalID;
                            $detail->UomInternalID = $s->UomInternalID;
                            $detail->Qty = $s->Qty;
                            $detail->UserRecord = Auth::user()->UserID;
                            $detail->UserModified = '0';
                            $detail->save();
                            setTampInventory($s->InventoryInternalID);
                        }
                    }
//                $SalesOrderdescription = SalesOrderHeader::find($salesOrderInternalID)->salesOrderDescription;
                }
            } else {
                if (explode('---;---', Input::get('inventory' . ($a + 1)))[1] == "inventory") {
                    $esde = ShippingAddDetail::where('ShippingInternalID', Input::get('shipping' . ($a + 1)))->first();
                    $tamp = $esde;
                    $detail = SalesOrderDetail::find($esde->SalesOrderDetailInternalID);
                    $qtyShippingValue = str_replace(',', '', Input::get('qty' . ($a + 1)));
                    if ($esde->Qty != $qtyShippingValue) {
                        $esde->delete();

                        $qtyValue = $detail->Qty;
                        $sumShipping = ShippingAddHeader::getSumShipping($detail->InventoryInternalID, $salesOrderInternalID, $detail->InternalID);
                        if ($sumShipping == '') {
                            $sumShipping = '0';
                        }
                        $qtyValue = $qtyValue - $sumShipping;
                        if ($qtyValue < $qtyShippingValue) {
                            $qtyShippingValue = $qtyValue;
                        }
                        if ($qtyShippingValue > 0) {
                            $detail2 = new ShippingAddDetail();
                            $detail2->ShippingInternalID = Input::get('shipping' . ($a + 1));
                            $detail2->InventoryInternalID = $tamp->InventoryInternalID;
                            $detail2->DescriptionInternalID = $tamp->DescriptionInternalID;
                            $detail2->UomInternalID = $tamp->UomInternalID;
                            $detail2->Qty = $qtyShippingValue;
                            $detail2->UserRecord = Auth::user()->UserID;
                            $detail2->UserModified = '0';
                            $detail2->SalesOrderDetailInternalID = $detail->InternalID;
                            $detail2->save();
                            setTampInventory($tamp->InventoryInternalID);
                        }
                    }
                }
            }
        }
        $messages = 'suksesUpdate';
        
        if ($messages == 'suksesUpdate') {
            return Redirect::route('packingDetail', $header->PackingID)->with("msg", "print");
        }
        
//        $error = '';
//        }
//        $shipping = $this->createID(0) . '.';
//        $header = SalesOrderHeader::find($id);
//        $detail = SalesOrderHeader::find($id)->salesOrderDetail()->get();
//        if ($messages == 'suksesInsert') {
//            $print = Route('packingPrint', $packing);
//        } else {
//            $print = '';
//        }
//        return View::make('penjualanAdd.packingNew')
//                        ->withToogle('transaction')->withAktif('shipping')
//                        ->withShipping($shipping)
//                        ->withError($error)
//                        ->withHeader($header)
//                        ->withDetail($detail)
//                        ->withPrint($print)
//                        ->withMessages($messages);
//        return Redirect::Route('packingNew', $header->SalesOrderID);
        return Redirect::Route('packingUpdate', $id);
    }

    public function insertPacking_lama($id) {
//rule
        $rule = array(
            'date' => 'required',
//            'remark' => 'required|max:1000'
        );
//validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
//tidak valid
            $messages = 'gagalInsert';
            $error = $validator->messages();
        } else {
//valid
//            $tax = '';
//            if ($salesOrder->VAT == 0) {
//                $tax = "NonTax";
//            } else {
//                $tax = "Tax";
//            }
            $dataSales = explode(',', $id);
//            $header = array();
//            $detail = array();
//            $description = array();
//insert packing
            $date = explode('-', Input::get('date'));
            $yearDigit = substr($date[2], 2);
            $shipping = $this->createIDPacking(0) . '.';
//            $shipping .= $date[1] . $yearDigit . '.';
            $shippingNumber = Packing::getNextIDPacking($shipping);
//            dd($shippingNumber);
            $packing = new Packing();
            $packing->PackingID = $shippingNumber;
            $packing->PackingDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $packing->ACC6InternalID = ShippingAddHeader::find(ShippingAddHeader::getIdShipping($dataSales[0]))->ACC6InternalID;
            $packing->Print = 1;
            $packing->CompanyInternalID = Auth::user()->CompanyInternalID;
            $packing->UserRecord = Auth::user()->UserID;
            $packing->UserModified = '0';
            $packing->Remark = Input::get('remark');
            $packing->save();

//insertpackingshipping
            foreach ($dataSales as $value) {
                $int = ShippingAddHeader::getIdShipping($value);

                $packship = new PackingShipping();
                $packship->PackingInternalID = $packing->InternalID;
                $packship->ShippingInternalID = $int;
                $packship->CompanyInternalID = Auth::user()->Company->InternalID;
                $packship->UserRecord = Auth::user()->UserID;
                $packship->UserModified = '0';
                $packship->Remark = '-';
                $packship->save();

//                array_push($header, ShippingAddHeader::find($int));
//                array_push($detail, ShippingAddHeader::find($int)->shippingDetail()->get());
//                array_push($description, ShippingAddHeader::find($int)->shippingDescription()->get());
            }
//            $messages = 'suksesInsert';
//            $error = '';
        }
        return Redirect::Route('showPackingList');
    }

    public function updateShipping($id) {
//tipe
        $headerUpdate = ShippingAddHeader::find($id);
        $detailUpdate = ShippingAddHeader::find($id)->shippingDetail()->get();

//rule
        if ($headerUpdate->isCash == 0) {
            $rule = array(
//                'remark' => 'required|max:1000',
                'warehouse' => 'required',
//                'slip' => 'required',
                'InternalDescription' => 'required'
            );
        } else {
            $rule = array(
//                'remark' => 'required|max:1000',
                'warehouse' => 'required',
                'InternalDescription' => 'required'
            );
            $longTerm = 0;
        }

//validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
//tidak valid
            $messages = 'gagalUpdate';
            $error = $validator->messages();
        } else {
            $header = ShippingAddHeader::find(Input::get('ShippingInternalID'));
            $header->WarehouseInternalID = Input::get('warehouse');
            $header->TransactionType = Input::get('TransactionType');
            if (Input::get('Replacement') == '') {
                $header->Replacement = '0';
            } else {
                $header->Replacement = Input::get('Replacement');
            }
            if (Input::get('TaxNumber') == '') {
                $header->TaxNumber = '.-.';
            } else {
                $header->TaxNumber = Input::get('TaxNumber');
            }
            $header->TaxMonth = Input::get('TaxMonth');
            $header->TaxYear = Input::get('TaxYear');
            $header->NumberVehicle = Input::get('vehicle');
            $header->DriverName = Input::get('driver');
            $header->UserModified = Auth::user()->UserID;
            $header->Remark = Input::get('remark');
            $header->save();

            ShippingAddDetail::where('ShippingInternalID', '=', Input::get('ShippingInternalID'))->update(array('is_deleted' => 1));
            ShippingAddParcel::where('ShippingInternalID', '=', Input::get('ShippingInternalID'))->update(array('is_deleted' => 1));
            ShippingAddDescription::where('ShippingInternalID', '=', Input::get('ShippingInternalID'))->update(array('is_deleted' => 1));

//insert detail
//$total = 0;
            for ($a = 0; $a < count(Input::get('InternalDescription')); $a++) {
                if (Input::get('qtyDescription')[$a] > 0) {
                    $data = SalesOrderDescription::find(Input::get('InternalDescription')[$a]);
                    $description = new ShippingAddDescription();
                    $description->ShippingInternalID = $header->InternalID;
                    $description->InventoryText = $data->InventoryText;
                    $description->UomText = $data->UomText;
                    $qty = 0;
                    if (Input::get('max')[$a] < Input::get('qtyDescription')[$a])
                        $qty = Input::get('max')[$a];
                    else
                        $qty = Input::get('qtyDescription')[$a];
                    $description->Qty = $qty;
                    $description->Spesifikasi = $data->Spesifikasi;
                    $description->SalesOrderDescriptionInternalID = Input::get('InternalDescription')[$a];
                    $description->UserRecord = Auth::user()->UserID;
                    $description->UserModified = '0';
                    $description->save();

                    for ($c = 0; $c < count(Input::get('InternalDetail' . ($a + 1))); $c++) {
                        if (Input::get('tipe' . ($a + 1))[$c] == "inventory") {
                            $detail = SalesOrderDetail::find(Input::get('InternalDetail' . ($a + 1))[$c]);
//jika non paket
                            $qtyValue = $detail->Qty;
                            $qtyShippingValue = str_replace(',', '', Input::get('qty' . ($a + 1))[$c]);
                            $sumShipping = ShippingAddHeader::getSumShippingExcept($detail->InventoryInternalID, $header->InternalID, Input::get('InternalDetail' . ($a + 1))[$c]);
                            if ($sumShipping == '') {
                                $sumShipping = '0';
                            }
                            $qtyValue = $qtyValue - $sumShipping;
                            if ($qtyValue < $qtyShippingValue) {
                                $qtyShippingValue = $qtyValue;
                            }
                            if ($qtyShippingValue > 0) {
                                $detail2 = new ShippingAddDetail();
                                $detail2->ShippingInternalID = $header->InternalID;
                                $detail2->InventoryInternalID = $detail->InventoryInternalID;
                                $detail2->UomInternalID = $detail->UomInternalID;
                                $detail2->DescriptionInternalID = $description->InternalID;
                                $detail2->Qty = $qtyShippingValue;
                                $detail2->UserRecord = Auth::user()->UserID;
                                $detail2->UserModified = '0';
                                $detail2->SalesOrderDetailInternalID = Input::get('InternalDetail' . ($a + 1))[$c];
                                $detail2->save();
                                setTampInventory($detail->InventoryInternalID);
                            }
                        } else {
                            $detail = SalesOrderParcel::find(Input::get("InternalDetail" . ($a + 1))[$c]);
//jika paket
                            $parcelDetail = ParcelInventory::where("ParcelInternalID", $detail->ParcelInternalID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->get();
                            $qtyValue = $detail->Qty;
                            $qtyShippingValue = str_replace(',', '', Input::get('qty' . ($a + 1))[$c]);
                            $sumShipping = ShippingAddHeader::getSumShippingExceptParcel($detail->ParcelInternalID, $header->InternalID, Input::get('InternalDetail' . ($a + 1))[$c]);
                            if ($sumShipping == '') {
                                $sumShipping = '0';
                            }

                            $qtyValue = $qtyValue - $sumShipping;
                            if ($qtyValue < $qtyShippingValue) {
                                $qtyShippingValue = $qtyValue;
                            }
                            if ($qtyShippingValue > 0) {
                                $SAParcel = new ShippingAddParcel();
                                $SAParcel->ShippingInternalID = $header->InternalID;
                                $SAParcel->ParcelInternalID = $detail->ParcelInternalID;
                                $SAParcel->DescriptionInternalID = $description->InternalID;
                                $SAParcel->SalesOrderParcelDetailInternalID = Input::get("InternalDetail" . ($a + 1))[$c];
                                $SAParcel->Qty = $qtyShippingValue;
                                $SAParcel->CompanyInternalID = Auth::user()->Company->InternalID;
                                $SAParcel->UserRecord = Auth::user()->UserID;
                                $SAParcel->UserModified = "0";
                                $SAParcel->Remark = "-";
                                $SAParcel->save();

                                foreach ($parcelDetail as $dataParcel) {
                                    $uom1 = $dataParcel->UomInternalID;
                                    $qtyValue1 = $dataParcel->Qty * $qtyShippingValue;
                                    $detail2 = new ShippingAddDetail();
                                    $detail2->ShippingInternalID = $header->InternalID;
                                    $detail2->InventoryInternalID = $dataParcel->InventoryInternalID;
                                    $detail2->UomInternalID = $uom1;
                                    $detail2->ShippingParcelInternalID = $SAParcel->InternalID;
                                    $detail2->Qty = $qtyValue1;
                                    $detail2->SalesOrderDetailInternalID = $headerUpdate->SalesOrderInternalID;
                                    $detail2->UserRecord = Auth::user()->UserID;
                                    $detail2->UserModified = '0';
                                    $detail2->save();
                                    setTampInventory($dataParcel->InventoryInternalID);
                                }
                            }
                        }
                    }
//                $SalesOrderdescription = SalesOrderHeader::find($salesOrderInternalID)->salesOrderDescription;
                }
            }
            ShippingAddDetail::where('ShippingInternalID', '=', Input::get('ShippingInternalID'))->where('is_deleted', 1)->delete();
            ShippingAddParcel::where('ShippingInternalID', '=', Input::get('ShippingInternalID'))->where('is_deleted', 1)->delete();
            ShippingAddDescription::where('ShippingInternalID', '=', Input::get('ShippingInternalID'))->where('is_deleted', 1)->delete();
            $messages = 'suksesUpdate';
            $error = '';

            //kalau non ppn insert ke inventory transfer!!
            if ($header->VAT == 0) {
                $headerT = TransferHeader::where("Remark", $header->ShippingID)->first();
                $headerT->WarehouseInternalID = $header->WarehouseInternalID;
                $headerT->WarehouseDestinyInternalID = Warehouse::where("Type", 1)->first()->InternalID;
                $headerT->UserModified = Auth::user()->UserID;
                $headerT->save();
                //hapus detail yg lama
                TransferDetail::where("TransferInternalID", $headerT->InternalID)->delete();
                //insert detail baru
                foreach (ShippingAddDetail::where("ShippingInternalID", $header->InternalID)->get() as $s) {
                    $detail = new TransferDetail();
                    $detail->TransferInternalID = $headerT->InternalID;
                    $detail->InventoryInternalID = $s->InventoryInternalID;
                    $detail->UomInternalID = $s->UomInternalID;
                    $detail->Qty = $s->Qty;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = '0';
                    $detail->save();
                    setTampInventory($s->InventoryInternalID);
                }
            }
        }

        if ($messages == 'suksesUpdate') {
            return Redirect::route('shippingDetail', $header->ShippingID)->with("msg", "print");
        }
        
//tipe
        $header = ShippingAddHeader::find($id);
        $detail = ShippingAddHeader::find($id)->shippingDetail()->get();
        $idSalesOrder = $header->SalesOrderInternalID;
        $headerSalesOrder = SalesOrderHeader::find($idSalesOrder);
        $detailSalesOrder = SalesOrderHeader::find($idSalesOrder)->salesOrderDetail()->get();
        return View::make('penjualanAdd.shippingAddUpdate')
                        ->withToogle('transaction')->withAktif('shipping')
                        ->withHeader($header)
                        ->withDetail($detail)
                        ->withHeaderorder($headerSalesOrder)
                        ->withDetailorder($detailSalesOrder)
                        ->withError($error)
                        ->withMessages($messages);
    }

    public function updatePacking_lama($id) {
//rule
        $rule = array(
//            'remark' => 'required|max:1000'
        );
//validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
//tidak valid
            $messages = 'gagalUpdate';
            $error = $validator->messages();
        } else {
//valid
            $packing = Packing::where('PackingID', $id)->first();
            $packing->UserModified = Auth::user()->UserID;
            $packing->Remark = Input::get('remark');
            $packing->save();

            $messages = 'suksesUpdate';
            $error = '';
        }
        return Redirect::Route('packingUpdate', $id);
    }

    public function deleteShipping() {
        $shippingHeader = ShippingAddHeader::find(Input::get('InternalID'));
        if (PackingShipping::where('ShippingInternalID', Input::get('InternalID'))->count() < 1) {
//tidak ada yang menggunakan data shipping maka data boleh dihapus
//hapus journal
            if ($shippingHeader->CompanyInternalID == Auth::user()->Company->InternalID) {
//                $journal = JournalHeader::where('TransactionID', '=', $shippingHeader->ShippingID)->get();
//                foreach ($journal as $value) {
//                    JournalDetail::where('JournalInternalID', '=', $value->InternalID)->delete();
//                    JournalHeader::where('InternalID', '=', $value->InternalID)->delete();
//                }
//hapus detil
                $th = TransferHeader::where("Remark", $shippingHeader->ShippingID)->first();
                if (count($th) > 0) {
                    TransferDetail::where("TransferInternalID", $th->InternalID)->delete();
                    $th->delete();
                }

                $detilData = ShippingAddHeader::find(Input::get('InternalID'))->shippingDetail;
                $description = ShippingAddHeader::find(Input::get('InternalID'))->shippingDescription;
                foreach ($detilData as $value) {
                    $detil = ShippingAddDetail::find($value->InternalID);
                    $detil->delete();
                    setTampInventory($detil->InventoryInternalID);
                }

                foreach ($description as $value) {
                    $detil = ShippingAddDescription::find($value->InternalID);
                    $detil->delete();
                }

//hapus shipping parcel
                ShippingAddParcel::where("ShippingInternalID", Input::get('InternalID'))->delete();
//hapus shipping
                $shipping = ShippingAddHeader::find(Input::get('InternalID'));
                $shipping->delete();
                $messages = 'suksesDelete';
            } else {
                $messages = 'accessDenied';
            }
        } else {
//ada maka data tidak boleh dihapus
            $messages = 'gagalDelete';
        }
        $data = ShippingAddHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
        return View::make('penjualanAdd.shippingAddSearch')
                        ->withToogle('transaction')->withAktif('shipping')
                        ->withMessages($messages)
                        ->withData($data);
    }

    public function deletePacking() {
        $packing = Packing::find(Input::get('InternalID'));
        if ($packing->CompanyInternalID == Auth::user()->Company->InternalID) {
//hapus detil
            $packingship = PackingShipping::where('PackingInternalID', Input::get('InternalID'));
            foreach ($packingship as $value) {
                $detil = PackingShipping::find($value->InternalID);
                $detil->delete();
            }

            $packing->delete();
            $messages = 'suksesDelete';
        } else {
            $messages = 'gagalDelete';
        }

        return Redirect::Route('showPackingList');
    }

    function shippingPrint($id) {
        $id = ShippingAddHeader::getIdshipping($id);
        $header = ShippingAddHeader::find($id);
        $header->Print = $header->Print + 1;
        $header->save();
        $detail = ShippingAddHeader::find($id)->shippingDetail()->get();
        $description = ShippingAddDescription::where('ShippingInternalID', $id)->get();
        $headerOrder = SalesOrderHeader::find($header->SalesOrderInternalID);
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $coa6 = ShippingAddHeader::find($header->InternalID)->coa6;
            $namacustomer = $coa6->ACC6Name;
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-')
                $cp = 'CP: ' . $coa6->ContactPerson . '<br>';
            else
                $cp = '';
            $customer = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . '<br>' . $cp . $coa6->City . '<br>' . $cp . $coa6->Phone . '<br>' . $coa6->Fax . '<br>' . $coa6->Email;
            if ($header->isCash == 0) {
                $payment = 'Cash';
            } else {
                $payment = 'Credit';
            }
            if ($header->VAT == 0) {
                $vat = 'Non Tax';
            } else {
                $vat = 'Tax';
            }
            $html = '
            <html>
                <head>
                    <style>
                    
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 6px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 195px;border-bottom:1px solid #ddd;margin-bottom:20px;">
                        <div style="height: 133px">
                        <img height="100%" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                        </div>
                        <div style="width: 420px; float: left;">
                            <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->Company->CompanyName . '</b>
                        </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Address</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Shipping</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span><br>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                            <table>
                            <tr>
                            <td width="275px">
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Shipping ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->ShippingID . '</td>
                                 </tr>
                               
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Customer</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $customer . '</td>
                                 </tr>';
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-') {
                $html .= '     <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Contact Person</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->ContactPerson . '</td>
                                 </tr>';
            }
            $html .= '   <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Payment</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $payment . '</td>
                                 </tr>';
            if ($header->isCash != 0) {
                $html .= '<tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Due Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime("+" . $header->LongTerm . " day", strtotime($header->ShippingDate))) . '</td>
                                 </tr>';
            }
            $html .= '
                                </table>
                                </td>
                                <td>
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->ShippingDate)) . '</td>
                                 </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Warehouse</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Warehouse->WarehouseName . '</td>
                                     </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Number of Vehicle</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->NumberVehicle . '</td>
                                     </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Driver Name</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->DriverName . '</td>
                                     </tr>
                                </table></td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">No.</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="65%">Inventory</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Uom</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Qty</th>
                                        </tr>
                                    </thead>
                                    <tbody>';

            $counter = 1;
            if (count($description) > 0) {
                foreach ($description as $data) {
                    $html .= '<tr>
                                <td rowspan="' . (1 + ($data->Spesifikasi != '' ? 1 : 0)) . '" style="vertical-align: top;font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $counter . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $data->InventoryText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->UomText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                            </tr>';
                    if ($data->Spesifikasi != '') {
                        $html .= '<tr>
                            <td colspan="3" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . nl2br($data->Spesifikasi) . '</td>
                            </tr>';
                    }
                    $counter++;
                }
            } else {
                $html .= '<tr>
                            <td colspan="3" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this shipping.</td>
                        </tr>';
            }
            $html .= '</tbody>
                            </table>
                                 <table  style="">
                            <tr><td style="vertical-align: top">
                            
                                 <table style="">
                                 <tr>
                                <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;vertical-align:top">Tanda Terima, </td>
                                
                            </tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr>
                            
                            </tr>
                                </table>
                       
                            </td>
                                <td style="vertical-align: top">
                           
                            <table style="width:250px;text-align:top;">
                             <tr>
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 500;vertical-align: top">Remark : </td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 500;vertical-align: top;text-align: left;">' . $header->Remark . '</td>
                                 </tr>
                                <tr><td colspan="2" style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 500;vertical-align: top">Perhatian Barang2 yang sudah diterima tidak dapat ditukar / dikembalikan</td></tr>
                                </table>
                            </td>
<td>
<table style="">
<tr>
                                <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Hormat Kami, </td>
                                
                            </tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr>
                            <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">(  ' . Auth::user()->UserName . '  )</td>
                           
                            </tr>
</table></td>
</tr>
                            </table>
                    </div>
                </body>
            </html>';
//            return PDF ::load($html, 'A5', 'portrait')->show();

            return View::make('template.print.shippingPrint')
                            ->with('header', $header)
                            ->with('customer', $customer)
                            ->with('namacustomer', $namacustomer)
                            ->with('vat', $vat)
                            ->with('id', $id)
                            ->with('description', $description)
                            ->with('payment', $payment)
                            ->with('detail', $detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showShipping');
        }
    }

    function packingPrint_lama($id) {
        $id = Packing::getIdPacking($id);
        $header = Packing::find($id);
        $header->Print = $header->Print + 1;
        $header->save();
//        $detail = ShippingAddHeader::find($id)->shippingDetail()->get();
        $description = ShippingAddDescription::whereIn('ShippingInternalID', function($query) use ($id) {
                    $query->select('ShippingInternalID')->from('t_packing_shipping')->where('PackingInternalID', $id);
                })->get();
//        $headerOrder = SalesOrderHeader::find($header->SalesOrderInternalID);
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $coa6 = ShippingAddHeader::find(PackingShipping::where('PackingInternalID', $id)->first()->ShippingInternalID)->coa6;
            $namacustomer = $coa6->ACC6Name;
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-')
                $cp = 'CP: ' . $coa6->ContactPerson . '<br>';
            else
                $cp = '';
            $customer = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . '<br>' . $cp . $coa6->Phone . '<br>' . $coa6->Fax . '<br>' . $coa6->Email;
//            if ($header->isCash == 0) {
//                $payment = 'Cash';
//            } else {
//                $payment = 'Credit';
//            }
//            if ($header->VAT == 0) {
//                $vat = 'Non Tax';
//            } else {
//                $vat = 'Tax';
//            }
            $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 6px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 195px;border-bottom:1px solid #ddd;margin-bottom:20px;">
                        <div style="height: 133px">
                        <img height="100%" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                        </div>
                        <div style="width: 420px; float: left;">
                            <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->Company->CompanyName . '</b>
                        </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Address</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Packing List</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span><br>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                            <table>
                            <tr>
                            <td width="275px">
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Packing ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->ShippingID . '</td>
                                 </tr>
                               
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Customer</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $customer . '</td>
                                 </tr>';
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-') {
                $html .= '     <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Contact Person</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->ContactPerson . '</td>
                                 </tr>';
            }
            $html .= '
                                </table>
                                </td>
                                <td>
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->ShippingDate)) . '</td>
                                 </tr>
                                     <!--<tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Number of Vehicle</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->NumberVehicle . '</td>
                                     </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Driver Name</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->DriverName . '</td>
                                     </tr>-->
                                </table></td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">No.</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="65%">Inventory</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Uom</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Qty</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Warehouse</th>
                                        </tr>
                                    </thead>
                                    <tbody>';

            $counter = 1;
            if (count($description) > 0) {
                foreach ($description as $data) {
                    $html .= '<tr>
                                <td rowspan="' . (1 + ($data->Spesifikasi != '' ? 1 : 0)) . '" style="vertical-align: top;font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $counter . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $data->InventoryText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->UomText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . ShippingAddHeader::find($data->ShippingInternalID)->warehouse->WarehouseName . '</td>
                            </tr>';
                    if ($data->Spesifikasi != '') {
                        $html .= '<tr>
                            <td colspan="3" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . nl2br($data->Spesifikasi) . '</td>
                            </tr>';
                    }
                    $counter++;
                }
            } else {
                $html .= '<tr>
                            <td colspan="3" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this shipping.</td>
                        </tr>';
            }
            $html .= '</tbody>
                            </table>
                                 <table  style="">
                            <tr><td style="vertical-align: top">
                            
                                 <table style="">
                                 <tr>
                                <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;vertical-align:top">Tanda Terima, </td>
                                
                            </tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr>
                            
                            </tr>
                                </table>
                       
                            </td>
                                <td style="vertical-align: top">
                           
                            <table style="width:250px;text-align:top;">
                             <tr>
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 500;vertical-align: top">Remark : </td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 500;vertical-align: top;text-align: left;">' . $header->Remark . '</td>
                                 </tr>
                                <tr><td colspan="2" style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 500;vertical-align: top">Perhatian Barang2 yang sudah diterima tidak dapat ditukar / dikembalikan</td></tr>
                                </table>
                            </td>
<td>
<table style="">
<tr>
                                <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Hormat Kami, </td>
                                
                            </tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr>
                            <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">(  ' . Auth::user()->UserName . '  )</td>
                           
                            </tr>
</table></td>
</tr>
                            </table>
                    </div>
                </body>
            </html>';
//            return PDF ::load($html, 'A5', 'portrait')->show();

            return View::make('template.print.packingPrint')
                            ->with('header', $header)
                            ->with('customer', $customer)
//                            ->with('vat', $vat)
//                            ->with('id', $id)
                            ->with('description', $description);
//                            ->with('payment', $payment)
//                            ->with('detail', $detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showShipping');
        }
    }

    function packingPrint($id) {
        $shipping = ShippingAddHeader::where('PackingID', $id)->get();
        $templateshipping = $shipping[0];
        foreach ($shipping as $header) {
            $header->Print = $header->Print + 1;
            $header->save();
        }

        if ($templateshipping->CompanyInternalID == Auth::user()->Company->InternalID) {
            $coa6 = $templateshipping->coa6;
            $namacustomer = $coa6->ACC6Name;
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-')
                $cp = 'CP: ' . $coa6->ContactPerson . '<br>';
            else
                $cp = '';
            $customer = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . '<br>' . $cp . $coa6->Phone . '<br>' . $coa6->Fax . '<br>' . $coa6->Email;

            $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 6px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 195px;border-bottom:1px solid #ddd;margin-bottom:20px;">
                        <div style="height: 133px">
                        <img height="100%" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                        </div>
                        <div style="width: 420px; float: left;">
                            <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->Company->CompanyName . '</b>
                        </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Address</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h4 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Packing List</h4>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span><br>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                            <table>
                            <tr>
                            <td width="275px">
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 11px;  font-weight: 700;">Packing ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 11px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 11px;  font-weight: 700;">' . $templateshipping->PackingID . '</td>
                                 </tr>
                               
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 11px;  font-weight: 700;">Customer</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 11px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 11px;  font-weight: 700;">' . $customer . '</td>
                                 </tr>';
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-') {
                $html .= '     <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 11px;  font-weight: 700;">Contact Person</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 11px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 11px;  font-weight: 700;">' . $coa6->ContactPerson . '</td>
                                 </tr>';
            }
            $html .= '
                                </table>
                                </td>
                                <td>
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 11px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 11px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 11px;  font-weight: 700;">' . date("d-M-Y", strtotime($templateshipping->ShippingDate)) . '</td>
                                 </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 11px;  font-weight: 700;">Number of Vehicle</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 11px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 11px;  font-weight: 700;">' . $templateshipping->NumberVehicle . '</td>
                                     </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 11px;  font-weight: 700;">Driver Name</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 11px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 11px;  font-weight: 700;">' . $templateshipping->DriverName . '</td>
                                     </tr>
                                </table></td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">No.</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="65%">Inventory</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Uom</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Qty</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Warehouse</th>
                                        </tr>
                                    </thead>
                                    <tbody>';

            $counter = 1;
//            if (count($description) > 0) {
            foreach ($shipping as $head) {
                $data = ShippingAddDetail::where('ShippingInternalID', $head->InternalID)->first();
                $html .= '<tr>
                                <td rowspan="' . (1 + ($data->Spesifikasi != '' ? 1 : 0)) . '" style="vertical-align: top;font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $counter . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . Inventory::find($data->InventoryInternalID)->InventoryName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . Uom::find($data->UomInternalID)->UomName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $head->warehouse->WarehouseName . '</td>
                            </tr>';
//                if ($data->Spesifikasi != '') {
//                    $html .= '<tr>
//                            <td colspan="3" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . nl2br($data->Spesifikasi) . '</td>
//                            </tr>';
//                }
                $counter++;
            }
//            } 
//            else {
//                $html .= '<tr>
//                            <td colspan="3" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this shipping.</td>
//                        </tr>';
//            }
            $html .= '</tbody>
                            </table>
                                 <table  style="">
                            <tr><td style="vertical-align: top">
                            
                                 <table style="">
                                 <tr>
                                <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;vertical-align:top">Tanda Terima, </td>
                                
                            </tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr>
                            
                            </tr>
                                </table>
                       
                            </td>
                                <td style="vertical-align: top">
                           
                            <table style="width:250px;text-align:top;">
                             <tr>
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 500;vertical-align: top">Remark : </td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 500;vertical-align: top;text-align: left;">' . $header->Remark . '</td>
                                 </tr>
                                <tr><td colspan="2" style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 500;vertical-align: top">Perhatian Barang2 yang sudah diterima tidak dapat ditukar / dikembalikan</td></tr>
                                </table>
                            </td>
<td>
<table style="">
<tr>
                                <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Hormat Kami, </td>
                                
                            </tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr>
                            <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">(  ' . Auth::user()->UserName . '  )</td>
                           
                            </tr>
</table></td>
</tr>
                            </table>
                    </div>
                </body>
            </html>';
//            return PDF ::load($html, 'A5', 'portrait')->show();

            return View::make('template.print.packingPrint')
                            ->with('header', $templateshipping)
                            ->with('customer', $customer)
                            ->with('namacustomer', $namacustomer)
//                            ->with('vat', $vat)
//                            ->with('id', $id)
                            ->with('shipping', $shipping);
//                            ->with('payment', $payment)
//                            ->with('detail', $detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showShipping');
        }
    }

    function shippingInternalPrint($id) {
        $id = ShippingAddHeader::getIdshipping($id);
        $header = ShippingAddHeader::find($id);
        $detail = ShippingAddHeader::find($id)->shippingDetail()->get();
        $description = ShippingAddDescription::where('ShippingInternalID', $id)->get();
        $headerOrder = SalesOrderHeader::find($header->SalesOrderInternalID);
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $coa6 = ShippingAddHeader::find($header->InternalID)->coa6;
            $namacustomer = $coa6->ACC6Name;
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-')
                $cp = 'CP: ' . $coa6->ContactPerson . '<br>';
            else
                $cp = '';
            $customer = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . '<br>' . $cp . $coa6->City . '<br>' . $cp . $coa6->Phone . '<br>' . $coa6->Fax . '<br>' . $coa6->Email;
            if ($header->isCash == 0) {
                $payment = 'Cash';
            } else {
                $payment = 'Credit';
            }
            if ($header->VAT == 0) {
                $vat = 'Non Tax';
            } else {
                $vat = 'Tax';
            }
            $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 6px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 70px;border-bottom:1px solid #ddd;margin-bottom:20px;">
                        <div style="width: 420px; float: left;">
                            <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->Company->CompanyName . '</b>
                        </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Address</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Shipping</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span><br>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                            <table>
                            <tr>
                            <td width="275px">
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Shipping ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->ShippingID . '</td>
                                 </tr>
                               
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Customer</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $customer . '</td>
                                 </tr>';
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-') {
                $html .= '     <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Contact Person</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->ContactPerson . '</td>
                                 </tr>';
            }
            $html .= '   <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Payment</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $payment . '</td>
                                 </tr>';
            if ($header->isCash != 0) {
                $html .= '<tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Due Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime("+" . $header->LongTerm . " day", strtotime($header->ShippingDate))) . '</td>
                                 </tr>';
            }
            $html .= '
                                </table>
                                </td>
                                <td>
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->ShippingDate)) . '</td>
                                 </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Warehouse</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Warehouse->WarehouseName . '</td>
                                     </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Number of Vehicle</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->NumberVehicle . '</td>
                                     </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Driver Name</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->DriverName . '</td>
                                     </tr>
                                </table></td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">No.</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="65%">Inventory</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Uom</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Qty</th>
                                        </tr>
                                    </thead>
                                    <tbody>';

            $counter = 1;
            if (count($description) > 0) {
                foreach ($description as $data) {
                    $detail = ShippingAddDetail::where('DescriptionInternalID', $data->InternalID)->where('ShippingParcelInternalID', 0)->get();
                    $parcel = ShippingAddParcel::where('DescriptionInternalID', $data->InternalID)->get();
                    $html .= '<tr>
                                <td rowspan="' . (1 + ($data->Spesifikasi != '' ? 1 : 0) + count($detail) + count($parcel)) . '" style="vertical-align: top;font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $counter . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $data->InventoryText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->UomText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                            </tr>';
                    if ($data->Spesifikasi != '') {
                        $html .= '<tr>
                            <td colspan="3" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . nl2br($data->Spesifikasi) . '</td>
                            </tr>';
                    }
                    foreach ($detail as $data2) {
                        $html .= '<tr>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;"><i>' . $data2->inventory->InventoryName . '</i></td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data2->uom->UomID . '</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data2->Qty, 0, '.', ',') . '</td>
                                </tr>';
                    }
                    foreach ($parcel as $data2) {
                        $html .= '<tr>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;"><i>' . $data2->parcel->ParcelName . '</i></td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">-</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data2->Qty, 0, '.', ',') . '</td>
                                </tr>';
                    }
                    $counter++;
                }
            } else {
                $html .= '<tr>
                            <td colspan="3" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this shipping.</td>
                        </tr>';
            }
            $html .= '</tbody>
                            </table>
                                 <table  style="">
                            <tr><td style="vertical-align: top">
                            
                                 <table style="">
                                 <tr>
                                <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;vertical-align:top">Tanda Terima, </td>
                                
                            </tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr>
                            
                            </tr>
                                </table>
                       
                            </td>
                                <td style="vertical-align: top">
                           
                            <table style="width:250px;text-align:top;">
                             <tr>
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 500;vertical-align: top">Remark : </td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 500;vertical-align: top;text-align: left;">' . $header->Remark . '</td>
                                 </tr>
                                <tr><td colspan="2" style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 500;vertical-align: top">Perhatian Barang2 yang sudah diterima tidak dapat ditukar / dikembalikan</td></tr>
                                </table>
                            </td>
<td>
<table style="">
<tr>
                                <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Hormat Kami, </td>
                                
                            </tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr>
                            <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">(  ' . Auth::user()->UserName . '  )</td>
                           
                            </tr>
</table></td>
</tr>
                            </table>
                    </div>
                </body>
            </html>';
//return PDF ::load($html, 'A5', 'portrait')->show();

            return View::make('template.print.shippingInternalPrint')
                            ->with('header', $header)
                            ->with('customer', $customer)
                            ->with('namacustomer', $namacustomer)
                            ->with('vat', $vat)
                            ->with('id', $id)
                            ->with('description', $description)
                            ->with('payment', $payment)
                            ->with('detail', $detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showShipping');
        }
    }

    function shippingPrintSJ($id) {
        $id = ShippingAddHeader::getIdshipping($id);
        $header = ShippingAddHeader::find($id);
        $detail = ShippingAddHeader::find($id)->shippingDetail()->get();
        $headerOrder = SalesOrderHeader::find($header->SalesOrderInternalID);
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $coa6 = ShippingAddHeader::find($header->InternalID)->coa6;
            $customer = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . ', ' . $coa6->Address . '<br>Phone: ' . $coa6->Phone . ', Fax: ' . $coa6->Fax;
            if ($header->isCash == 0) {
                $payment = 'Cash';
            } else {
                $payment = 'Credit';
            }
            if ($header->VAT == 0) {
                $vat = 'Non Tax';
            } else {
                $vat = 'Tax';
            }
            $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 6px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 195px;border-bottom:1px solid #ddd;margin-bottom:20px;">
                        <div style="height: 133px">
                        <img height="100%" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                        </div>
                        <div style="width: 420px; float: left;">
                            <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->Company->CompanyName . '</b>
                        </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Address</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Shipping</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span><br><br>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                            <table>
                            <tr>
                            <td width="275px">
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Shipping ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->ShippingID . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->ShippingDate)) . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Customer</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $customer . '</td>
                                 </tr>';
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-') {
                $html .= '     <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Contact Person</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->ContactPerson . '</td>
                                 </tr>';
            }
            $html .= '   </table>
                                </td>
                                <td>
                                <table>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Warehouse</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Warehouse->WarehouseName . '</td>
                                     </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Shipping Order</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $headerOrder->SalesOrderID . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Remark</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Remark . '</td>
                                     </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Number of Vehicle</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->NumberVehicle . '</td>
                                     </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Driver Name</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->DriverName . '</td>
                                     </tr>
                                </table></td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="65%">Inventory</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Uom</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="25%">Qty</th>
                                        </tr>
                                    </thead>
                                    <tbody>';

            if (count($detail) > 0) {
                foreach ($detail as $data) {
                    if ($data->ShippingParcelInternalID == 0) {
                        $inventory = Inventory::find($data->InventoryInternalID);
                        $inv = $inventory->InventoryID . ' ' . $inventory->TextPrint;
                        $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $inv . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $data->Uom->UomID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                            </tr>';
                    }
                }
                $Parcel = ShippingAddHeader::searchParcel($id);
                foreach ($Parcel as $data) {
                    $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $data->ParcelID . '-' . $data->ParcelName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">-</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, '0', '.', ',') . '</td>
                            </tr>';
                }
            } else {
                $html .= '<tr>
                            <td colspan="3" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this shipping.</td>
                        </tr>';
            }
            $html .= '</tbody>
                            </table>
                            <table width="100%" style="margin-top: 10px;">
                            <tr>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; padding-left: 50px; font-weight: 500;" width="60%">
                            Tanda Terima.
                            </td>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; padding-left: 50px; font-weight: 500;" width="40%">
                            Hormat Kami.
                            </td>
                            </tr>
                            </table>
                    </div>
                </body>
            </html>';
            return PDF ::load($html, 'A5', 'portrait')->show();
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showShipping');
        }
    }

    function createID($tipe, $tax = null) {
        if ($tax == "Tax")
            $shipping = 'SH';
        else if ($tax == "NonTax")
            $shipping = 'SHN';
        else
            $shipping = 'SH';
        if ($tipe == 0) {
            $shipping .= '.' . date('m') . date('y');
        }
        return $shipping;
    }

    function createIDTransfer($tipe) {
        $transfer = 'TF';
        if ($tipe == 0) {
            $transfer .= '.' . date('m') . date('y');
        }
        return $transfer;
    }

    function createIDPacking($tipe, $tax = null) {
        if ($tax == "Tax")
            $shipping = 'PK';
        else if ($tax == "NonTax")
            $shipping = 'PKN';
        else
            $shipping = 'PK';
        if ($tipe == 0) {
            $shipping .= '.' . date('m') . date('y');
        }
        return $shipping;
    }

    public function formatCariIDShipping() {
        $date = explode('-', Input::get('date'));
        $id = $this->createID(1);
        $yearDigit = substr($date[2], 2);
        $id .= '.' . $date[1] . $yearDigit . '.';
        echo ShippingAddHeader::getNextIDShipping($id);
    }

//    function shippingCSV($id) {
//        $id = ShippingAddHeader::getIdshipping($id);
//        $header = ShippingAddHeader::find($id);
//        Session::flash('idCSV', $id);
//        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
//            Excel::create('Tax_Report_shipping', function($excel) {
//                $excel->sheet('Tax_Report_shipping', function($sheet) {
//                    $id = Session::get('idCSV');
//                    $header = ShippingHeader::find($id);
//                    $detail = ShippingHeader::find($id)->shippingDetail()->get();
//                    //baris pertama
//                    $sheet->setCellValueByColumnAndRow(0, 1, "FK");
//                    $sheet->setCellValueByColumnAndRow(1, 1, "KD_JENIS_TRANSAKSI");
//                    $sheet->setCellValueByColumnAndRow(2, 1, "FG_PENGGANTI");
//                    $sheet->setCellValueByColumnAndRow(3, 1, "NOMOR_FAKTUR");
//                    $sheet->setCellValueByColumnAndRow(4, 1, "MASA_PAJAK");
//                    $sheet->setCellValueByColumnAndRow(5, 1, "TAHUN_PAJAK");
//                    $sheet->setCellValueByColumnAndRow(6, 1, "TANGGAL_FAKTUR");
//                    $sheet->setCellValueByColumnAndRow(7, 1, "NPWP");
//                    $sheet->setCellValueByColumnAndRow(8, 1, "NAMA");
//                    $sheet->setCellValueByColumnAndRow(9, 1, "ALAMAT_LENGKAP");
//                    $sheet->setCellValueByColumnAndRow(10, 1, "JUMLAH_DPP");
//                    $sheet->setCellValueByColumnAndRow(11, 1, "JUMLAH_PPN");
//                    $sheet->setCellValueByColumnAndRow(12, 1, "JUMLAH_PPNBM");
//                    $sheet->setCellValueByColumnAndRow(13, 1, "ID_KETERANGAN_TAMBAHAN_Number");
//                    $sheet->setCellValueByColumnAndRow(14, 1, "FG_UANG_MUKA");
//                    $sheet->setCellValueByColumnAndRow(15, 1, "UANG_MUKA_DPP");
//                    $sheet->setCellValueByColumnAndRow(16, 1, "UANG_MUKA_PPN");
//                    $sheet->setCellValueByColumnAndRow(17, 1, "UANG_MUKA_PPNBM");
//                    $sheet->setCellValueByColumnAndRow(18, 1, "REFERENSI");
//                    //baris kedua
//                    $sheet->setCellValueByColumnAndRow(0, 2, "LT");
//                    $sheet->setCellValueByColumnAndRow(1, 2, "NPWP");
//                    $sheet->setCellValueByColumnAndRow(2, 2, "NAMA");
//                    $sheet->setCellValueByColumnAndRow(3, 2, "JALAN");
//                    $sheet->setCellValueByColumnAndRow(4, 2, "BLOK");
//                    $sheet->setCellValueByColumnAndRow(5, 2, "NOMOR");
//                    $sheet->setCellValueByColumnAndRow(6, 2, "RT");
//                    $sheet->setCellValueByColumnAndRow(7, 2, "RW");
//                    $sheet->setCellValueByColumnAndRow(8, 2, "KECAMATAN");
//                    $sheet->setCellValueByColumnAndRow(9, 2, "KELURAHAN");
//                    $sheet->setCellValueByColumnAndRow(10, 2, "KABUPATEN");
//                    $sheet->setCellValueByColumnAndRow(11, 2, "PROPINSI");
//                    $sheet->setCellValueByColumnAndRow(12, 2, "KODE_POS");
//                    $sheet->setCellValueByColumnAndRow(13, 2, "NOMOR_TELEPON");
//                    //baris ketiga
//                    $sheet->setCellValueByColumnAndRow(0, 3, "OF");
//                    $sheet->setCellValueByColumnAndRow(1, 3, "KODE_OBJEK");
//                    $sheet->setCellValueByColumnAndRow(2, 3, "NAMA");
//                    $sheet->setCellValueByColumnAndRow(3, 3, "HARGA_SATUAN");
//                    $sheet->setCellValueByColumnAndRow(4, 3, "JUMLAH_BARANG");
//                    $sheet->setCellValueByColumnAndRow(5, 3, "HARGA_TOTAL");
//                    $sheet->setCellValueByColumnAndRow(6, 3, "DISKON");
//                    $sheet->setCellValueByColumnAndRow(7, 3, "DPP");
//                    $sheet->setCellValueByColumnAndRow(8, 3, "PPN");
//                    $sheet->setCellValueByColumnAndRow(9, 3, "TARIF_PPNBM");
//                    $sheet->setCellValueByColumnAndRow(10, 3, "PPNBM");
//                    //isi baris pertama
//                    $sheet->setCellValueByColumnAndRow(0, 4, "FK");
//                    $sheet->setCellValueByColumnAndRow(1, 4, '0' . $header->TransactionType);
//                    $sheet->setCellValueByColumnAndRow(2, 4, $header->Replacement);
//                    //nomorPajak
//                    $nomorPajak = str_replace('.', '', $header->TaxNumber);
//                    $nomorPajak = str_replace('-', '', $nomorPajak);
//                    $nomorPajak = substr($nomorPajak, 4);
//                    $sheet->setCellValueByColumnAndRow(3, 4, $nomorPajak);
//                    $sheet->setCellValueByColumnAndRow(4, 4, $header->TaxMonth);
//                    $sheet->setCellValueByColumnAndRow(5, 4, substr($header->TaxYear, '2'));
//                    $sheet->setCellValueByColumnAndRow(6, 4, date('d/m/Y', strtotime($header->TaxDate)));
//                    //nomorPajak
//                    $pajakCustomer = str_replace('.', '', $header->Coa6->TaxID);
//                    $pajakCustomer = str_replace('-', '', $pajakCustomer);
//                    if ($pajakCustomer == '') {
//                        $pajakCustomer = '000000000000000';
//                    }
//                    $sheet->setCellValueByColumnAndRow(7, 4, $pajakCustomer);
//                    $sheet->setCellValueByColumnAndRow(8, 4, $header->Coa6->ACC6Name);
//                    $sheet->setCellValueByColumnAndRow(9, 4, $header->Coa6->Address);
//                    //total
//                    $total = $header->GrandTotal * 10 / 11;
//                    $sheet->setCellValueByColumnAndRow(10, 4, floor($total));
//                    $sheet->setCellValueByColumnAndRow(11, 4, floor(($total * 0.1)));
//                    $sheet->setCellValueByColumnAndRow(12, 4, 0);
//                    $sheet->setCellValueByColumnAndRow(13, 4, '');
//                    if ($header->DownPayment == 0) {
//                        $dp = 0;
//                    } else {
//                        $dp = 1;
//                    }
//                    $sheet->setCellValueByColumnAndRow(14, 4, $dp);
//                    $sheet->setCellValueByColumnAndRow(15, 4, $header->DownPayment);
//                    $sheet->setCellValueByColumnAndRow(16, 4, $header->DownPayment * 0.1);
//                    $sheet->setCellValueByColumnAndRow(17, 4, "0");
//                    $sheet->setCellValueByColumnAndRow(18, 4, "");
//                    //isi baris kedua
//                    $sheet->setCellValueByColumnAndRow(0, 5, "LT");
//                    $sheet->setCellValueByColumnAndRow(1, 5, $pajakCustomer);
//                    $sheet->setCellValueByColumnAndRow(2, 5, $header->Coa6->ACC6Name);
//                    $sheet->setCellValueByColumnAndRow(3, 5, $header->Coa6->Address);
//                    $sheet->setCellValueByColumnAndRow(4, 5, $header->Coa6->Block);
//                    $sheet->setCellValueByColumnAndRow(5, 5, $header->Coa6->AddressNumber);
//                    $sheet->setCellValueByColumnAndRow(6, 5, $header->Coa6->RT);
//                    $sheet->setCellValueByColumnAndRow(7, 5, $header->Coa6->RW);
//                    $sheet->setCellValueByColumnAndRow(8, 5, $header->Coa6->District);
//                    $sheet->setCellValueByColumnAndRow(9, 5, $header->Coa6->Subdistrict);
//                    $sheet->setCellValueByColumnAndRow(10, 5, $header->Coa6->City);
//                    $sheet->setCellValueByColumnAndRow(11, 5, $header->Coa6->Province);
//                    $sheet->setCellValueByColumnAndRow(12, 5, $header->Coa6->PostalCode);
//                    $sheet->setCellValueByColumnAndRow(13, 5, $header->Coa6->Phone);
//
//                    $row = 6;
//                    foreach ($detail as $data) {
//                        $sheet->setCellValueByColumnAndRow(0, $row, "OF");
//                        $sheet->setCellValueByColumnAndRow(1, $row, $data->Inventory->InventoryID);
//                        $sheet->setCellValueByColumnAndRow(2, $row, $data->Inventory->InventoryName);
//                        $sheet->setCellValueByColumnAndRow(3, $row, $data->Price);
//                        $sheet->setCellValueByColumnAndRow(4, $row, $data->Qty);
//                        $sheet->setCellValueByColumnAndRow(5, $row, $data->Qty * $data->Price);
//                        $sheet->setCellValueByColumnAndRow(6, $row, $data->Qty * $data->Price - $data->SubTotal);
//                        $sheet->setCellValueByColumnAndRow(7, $row, $data->SubTotal);
//                        $sheet->setCellValueByColumnAndRow(8, $row, $data->VAT);
//                        $sheet->setCellValueByColumnAndRow(9, $row, 0);
//                        $sheet->setCellValueByColumnAndRow(10, $row, 0);
//                        $row++;
//                    }
//
//                    $row--;
//                    $sheet->setBorder('B2:T' . $row, 'thin');
//                    $sheet->cells('B2:T2', function($cells) {
//                        $cells->setBackground('#eaf6f7');
//                        $cells->setValignment('middle');
//                    });
//                    $sheet->cells('B1', function($cells) {
//                        $cells->setValignment('middle');
//                        $cells->setFontWeight('bold');
//                        $cells->setFontSize('16');
//                    });
//                    $sheet->cells('B2:T' . $row, function($cells) {
//                        $cells->setAlignment('left');
//                        $cells->setValignment('middle');
//                    });
//                });
//            })->export('csv');
//        } else {
//            $messages = 'accessDenied';
//            Session::flash('messages', $messages);
//            return Redirect::Route('showShipping');
//        }
//    }

    public function summaryShipping() {
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $totalSA = 0;
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <!--<div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>-->
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Shipping Summary Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span>
                            <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span>';
        $hitung = 0;
        foreach (Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $dataCustomer) {
            if (ShippingAddHeader::join("m_warehouse", "m_warehouse.InternalID", "=", "t_shipping_header.WarehouseInternalID")
                            ->where('t_shipping_header.CompanyInternalID', Auth::user()->Company->InternalID)
                            ->where('ACC6InternalID', $dataCustomer->InternalID)
                            ->where("VAT", "!=", Auth::user()->SeeNPPN)
                            ->where("Type", Auth::user()->WarehouseCheck)
                            ->whereBetween('ShippingDate', Array($start, $end))->count() > 0) {
                $html .= '<table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=2>' . $dataCustomer->ACC6Name . '</th>
                                        </tr>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Shipping ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                foreach (ShippingAddHeader::join("m_warehouse", "m_warehouse.InternalID", "=", "t_shipping_header.WarehouseInternalID")
                        ->where('t_shipping_header.CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where('ACC6InternalID', $dataCustomer->InternalID)
                        ->where("VAT", "!=", Auth::user()->SeeNPPN)
                        ->where("Type", Auth::user()->WarehouseCheck)
                        ->whereBetween('ShippingDate', Array($start, $end))->get() as $data) {
                    $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->ShippingID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->ShippingDate)) . '</td>
                            </tr>';
                }
                $html .= '</tbody>
            </table>';
                $hitung++;
            }
        }

        if ($hitung == 0) {
            $html .= '<br><span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no shipping.</span>';
        }

        $html .= '</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('shipping_summary');
    }

    public function detailShipping() {
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $dateTamp = '';
        $coa6Tamp = '';
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <!--<div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>-->
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Shipping Detail Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span>
                            <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span><br><br>';
        if (ShippingAddHeader::join("m_warehouse", "m_warehouse.InternalID", "=", "t_shipping_header.WarehouseInternalID")
                        ->where('t_shipping_header.CompanyInternalID', Auth::user()->Company->InternalID)
                        ->whereBetween('ShippingDate', Array($start, $end))
                        ->where("VAT", "!=", Auth::user()->SeeNPPN)
                        ->where("Type", Auth::user()->WarehouseCheck)
                        ->orderBy('ShippingDate')->orderBy('ACC6InternalID')->count() > 0) {
            foreach (ShippingAddHeader::join("m_warehouse", "m_warehouse.InternalID", "=", "t_shipping_header.WarehouseInternalID")
                    ->where('t_shipping_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->whereBetween('ShippingDate', Array($start, $end))
                    ->where("VAT", "!=", Auth::user()->SeeNPPN)
                    ->where("Type", Auth::user()->WarehouseCheck)
                    ->orderBy('ShippingDate')->orderBy('ACC6InternalID')->get() as $dataPenjualan) {
                if ($dateTamp != date("d-M-Y", strtotime($dataPenjualan->ShippingDate))) {
                    $html .= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Shipping Date : ' . date("d-M-Y", strtotime($dataPenjualan->ShippingDate)) . '</span><br><br>';
                    $dateTamp = date("d-M-Y", strtotime($dataPenjualan->ShippingDate));
                    $coa6Tamp = '';
                }
                if ($coa6Tamp != $dataPenjualan->ACC6InternalID) {
                    $html .= '<span style="padding-left:23px;font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Customer : ' . $dataPenjualan->coa6->ACC6Name . '</span>';
                    $coa6Tamp = $dataPenjualan->ACC6InternalID;
                }
                if ($dataPenjualan->coa6->ContactPerson != '' && $dataPenjualan->coa6->ContactPerson != '-' && $dataPenjualan->coa6->ContactPerson != null) {
                    $html .= '<span style="padding-left:23px;font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Contact Person : ' . $dataPenjualan->coa6->ContactPerson . '</span>';
                }
                $html .= '<table class="tableBorder" style="width:95%; margin-top: 18px; clear: both; position: relative; left: 4%">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=4>' . $dataPenjualan->ShippingID . ' </th>
                                        </tr>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory ID</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Name</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Uom</th>
                                            <th width="10%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Qty</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                foreach ($dataPenjualan->shippingDetail as $data) {
                    if ($data->ShippingParcelInternalID == 0) {
                        $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->inventory->InventoryID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->inventory->InventoryName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->Uom->UomID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                            </tr>';
                    }
                }
                foreach (ShippingAddParcel::where("ShippingInternalID", $dataPenjualan->InternalID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->get() as $data) {
                    $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->Parcel->ParcelID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->Parcel->ParcelName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;"> - </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                            </tr>';
                }
                $html .= '
                </tbody>
            </table>';
            }
        } else {
            $html .= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no shipping.</span><br><br>';
        }
        $html .= '</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('shipping_detail');
    }

//=====================================ajax=======================================
    public function getShippingList() {
        $shiplist = ShippingAddHeader::where('ACC6InternalID', Input::get('customer'))->whereNotIn('InternalID', function($query) {
                    $query->select('ShippingInternalID')->from('t_packing_shipping');
                })->get();
//dd(Input::get('customer'));
        if (count($shiplist) == 0) {
            ?>
            <span>Shipping with this customer can't be found</span>
        <?php } else {
            ?>
            <select class="input-theme chosen-select choosen-modal input-stretch" multiple id="shippinglist" style="" name="shippinglist[]">    
                <?php
                foreach ($shiplist as $sl) {
                    ?>
                    <option value="<?php echo $sl->ShippingID ?>"><?php echo $sl->ShippingID . ' ' . date("d-m-Y", strtotime($sl->ShippingDate)) ?></option>
                    <?php
                }
                ?>
            </select>
            <script>
                var config = {'.chosen-select': {}};
                for (var selector in config) {
                    $(selector).chosen({
                        search_contains: true
                    });
                }
            </script>

            <?php
        }
    }

    public function getResultSearchShippingSO() {
        $hitung = 0;
        $input = splitSearchValue(Input::get("id"));
        $date = date("Y-m-d", strtotime(Input::get("id")));
        $salesOrderHeader = SalesOrderHeader::where('t_salesorder_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->join('m_coa6', 'm_coa6.InternalID', '=', 't_salesorder_header.ACC6InternalID')
                ->where('Closed', 0)
                ->where('Status', 1)
                ->where('VAT', "!=", Auth::user()->SeeNPPN)
                ->where(function($query) use ($input, $date) {
                    $query->where("SalesOrderID", "like", $input)
                    ->orWhere("SalesOrderDate", "like", '%' . $date . '%')
                    ->orWhere("ACC6Name", "like", '%' . $input . '%');
                })
                ->OrderBy('SalesOrderDate', 'desc')
                ->select('t_salesorder_header.*', 'm_coa6.ACC6Name')
                ->get();
        if (count($salesOrderHeader) == 0) {
            ?>
            <span>Sales Order with keywords <i><u><?php echo Input::get("id"); ?></u></i> can't be found</span>
        <?php } else {
            ?>
            <select class="chosen-select choosen-modal" id="shipping" style="" name="shipping">
                <?php
                foreach ($salesOrderHeader as $shipping) {
                    //tambah pengecekan disini
                    if (checkShippingAdd($shipping->InternalID) && $hitung < 100) {
                        if (checkAmountSales($shipping->InternalID) <= 0 && $shipping->isCash == 2) {
                            ?>
                            <option value="<?php echo $shipping->SalesOrderID ?>"><?php echo $shipping->SalesOrderID . ' | ' . date("d-m-Y", strtotime($shipping->SalesOrderDate)) . ' | ' . $shipping->ACC6Name ?></option>
                        <?php } else if ($shipping->isCash == 4 && checkDpShipping($shipping->InternalID)) {
                            ?>
                            <option value="<?php echo $shipping->SalesOrderID ?>"><?php echo $shipping->SalesOrderID . ' | ' . date("d-m-Y", strtotime($shipping->SalesOrderDate)) . ' | ' . $shipping->ACC6Name ?></option>
                            <?php
                        } else if ($shipping->isCash != 2 && $shipping->isCash != 4) {
                            ?>
                            <option value="<?php echo $shipping->SalesOrderID ?>"><?php echo $shipping->SalesOrderID . ' | ' . date("d-m-Y", strtotime($shipping->SalesOrderDate)) . ' | ' . $shipping->ACC6Name ?></option>
                            <?php
                        }
                        $hitung++;
                    }
                }
                ?>
            </select>
            <script>
                $(document).ready(function () {
                    var hitung = '<?php echo $hitung; ?>';
                    if (hitung == 0) {
                        $('#selectSalesOrder').after('<span>There is no result.</span>');
                        $('#selectSalesOrder').remove();
                    } else {
                        $("#btn-add-so").removeAttr("disabled");
                    }
                });
            </script>
            <?php
        }
    }

    public function getResultSearchShippingSO2() {
        $hitung = 0;
        $input = splitSearchValue(Input::get("id"));
        $date = date("Y-m-d", strtotime(Input::get("id")));
        $salesOrderHeader = SalesOrderHeader::where('t_salesorder_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->join('m_coa6', 'm_coa6.InternalID', '=', 't_salesorder_header.ACC6InternalID')
                ->where('Closed', 0)
                ->where('Status', 1)
                ->where('VAT', "!=", Auth::user()->SeeNPPN)
                ->where(function($query) use ($input, $date) {
                    $query->where("SalesOrderID", "like", $input)
                    ->orWhere("SalesOrderDate", "like", '%' . $date . '%')
                    ->orWhere("ACC6Name", "like", '%' . $input . '%');
                })
                ->OrderBy('SalesOrderDate', 'desc')
                ->select('t_salesorder_header.*', 'm_coa6.ACC6Name')
                ->get();
        if (count($salesOrderHeader) == 0) {
            ?>
            <span>Sales Order with keywords <i><u><?php echo Input::get("id"); ?></u></i> can't be found</span>
        <?php } else {
            ?>
            <select class="chosen-select choosen-modal" id="shipping" style="" name="shipping">
                <?php
                foreach ($salesOrderHeader as $shipping) {
                    //tambah pengecekan disini
                    if (checkShippingAdd($shipping->InternalID) && $hitung < 100) {
                        if (checkAmountSales($shipping->InternalID) <= 0 && $shipping->isCash == 2) {
                            ?>
                            <option value="<?php echo $shipping->SalesOrderID ?>"><?php echo $shipping->SalesOrderID . ' | ' . date("d-m-Y", strtotime($shipping->SalesOrderDate)) . ' | ' . $shipping->ACC6Name ?></option>
                        <?php } else if ($shipping->isCash == 4 && checkDpShipping($shipping->InternalID)) {
                            ?>
                            <option value="<?php echo $shipping->SalesOrderID ?>"><?php echo $shipping->SalesOrderID . ' | ' . date("d-m-Y", strtotime($shipping->SalesOrderDate)) . ' | ' . $shipping->ACC6Name ?></option>
                            <?php
                        } else if ($shipping->isCash != 2 && $shipping->isCash != 4) {
                            ?>
                            <option value="<?php echo $shipping->SalesOrderID ?>"><?php echo $shipping->SalesOrderID . ' | ' . date("d-m-Y", strtotime($shipping->SalesOrderDate)) . ' | ' . $shipping->ACC6Name ?></option>
                            <?php
                        }
                        $hitung++;
                    }
                }
                ?>
            </select>
            <script>
                $(document).ready(function () {
                    var hitung = '<?php echo $hitung; ?>';
                    if (hitung == 0) {
                        $('#selectSalesOrder2').after('<span>There is no result.</span>');
                        $('#selectSalesOrder2').remove();
                    } else {
                        $("#btn-add-so2").removeAttr("disabled");
                    }
                });
            </script>
            <?php
        }
    }

    public function getSearchResultInventoryForPacking() {
        $idnih = "%" . explode(" ", Input::get("id"))[0] . "%";
        $pass = str_replace(explode(" ", Input::get("id"))[0], '', Input::get('id'));
        $input = splitSearchValue($pass);
//        $input = splitSearchValue(Input::get("id"));
        $dataInventory = Inventory::select('m_inventory.*')->distinct()
                ->join('m_inventory_uom', 'm_inventory_uom.InventoryInternalID', '=', 'm_inventory.InternalID')
                ->where("m_inventory.Status", 0)
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where("m_inventory.InventoryID", "like", $idnih)
                ->where("m_inventory.InventoryName", "like", $input)
                ->where("m_inventory.Status", 0)
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->get();

        $dataParcel = Parcel::where("CompanyInternalID", Auth::user()->Company->InternalID)
                ->where("ParcelID", "like", $input)
                ->orWhere("ParcelName", "like", $input)
                ->get();

        if (count($dataInventory) == 0 && count($dataParcel) == 0) {
            ?>
            <span>Inventory with keywords <i><u><?php echo Input::get("id"); ?></u></i> can't be found</span>
            <?php
        } else {
            ?>
            <select class="input-theme left inventory" id="inventory-0" data-toggle="popover" data-placement="bottom" data-html="true">
                <?php
                foreach ($dataInventory as $inventory) {
                    ?>
                    <option id="inventory<?php echo $inventory->InternalID ?>" value="<?php echo $inventory->InternalID ?>---;---inventory">
                        <?php echo $inventory->InventoryID . ' ' . $inventory->InventoryName . ' (power: ' . $inventory->Power . ')'; ?>
                    </option>
                    <?php
                }
                ?>
                <?php
                foreach ($dataParcel as $parcel) {
                    ?>
                    <!--foreach untuk paketnya-->
                    <option id="parcel<?php echo $parcel->InternalID ?>" value="<?php echo $parcel->InternalID ?>---;---parcel">
                        <?php echo $parcel->ParcelID . " " . $parcel->ParcelName ?>
                    </option>
                    <!--foreach untuk paketnya-->
                    <?php
                }
                ?>
            </select>
            <?php
        }
    }

    public function hitungStockInventoryWarehouse() {
        $inv = Input::Get('inventory');
        $war = Input::Get('warehouse');

        return getEndStockInventoryWithWarehouse($inv, $war);
    }

    public function hitungStockInventoryWarehouseTanpaPacking() {
        $inv = Input::Get('inventory');
        $war = Input::Get('warehouse');
        $pack = Input::Get('packing');

        return getEndStockInventoryWithWarehouseWithoutPacking($inv, $war, $pack);
    }

    public function currentStockShipping() {
        $internalID = Input::get('internalID');
        $warehouse = Input::get('warehouse');
        $header = SalesOrderHeader::find($internalID);
        $detail = SalesOrderHeader::find($internalID)->salesOrderDetail()->get();
        $initialStockTamp = array();
        $inventoryneh = array();
        $i = 0;
        foreach ($detail as $data) {
            if ($data->SalesOrderParcelInternalID == 0) {
                $initialStockTamp[$i] = Inventory::getInitialStock(date("Y-m-d"), '<=', $data->InventoryInternalID, $warehouse, "=");
                $inventoryneh[$i] = $data->InventoryInternalID;
                $i++;
            }
        }
        return $initialStockTamp;
    }

    public function currentStockShippingUpdate() {
        $internalID = Input::get('internalID');
        $warehouse = Input::get('warehouse');
        $shipping = Input::get('shipping');
        $header = SalesOrderHeader::find($internalID);
        $detail = SalesOrderHeader::find($internalID)->salesOrderDetail()->get();
        $initialStockTamp = array();
        $inventoryneh = array();
        $i = 0;
        foreach ($detail as $data) {
            if ($data->SalesOrderParcelInternalID == 0) {
                $initialStockTamp[$i] = Inventory::getInitialStockWithoutShipping(date("Y-m-d"), '<=', $data->InventoryInternalID, $warehouse, "=", $shipping);
                $inventoryneh[$i] = $data->InventoryInternalID;
                $i++;
            }
        }
        return $initialStockTamp;
    }

    public function shippingPrintStruk($id) {
        $html = '<!DOCTYPE html>
        <html id="printShipping" style="width: 118mm; height: 150mm;">
            <head>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Receipt</title>
                <style>
                    @media print{@page { size: 89mm auto;}}
                    @page {
                        margin-top: 0;
                        margin-bottom: 0;
                        size: 89mm auto;
                    }

                    body {
                        font-family: calibri; 
                        width: 100%;
                        margin: 0 auto;
                    }
                    
                    body * {
                        letter-spacing: 0.6mm !important;
                    }
                    
                    h4 {
                        font-size: 18px;
                        letter-spacing: 0.4px;
                        margin-bottom: 2px;
                    }

                    h3 {
                        font-size: 16px;
                        letter-spacing: 0.4px;
                        margin-top: 0px;
                        margin-bottom: 2px;
                    }

                    p {
                        font-size: 14px;
                        letter-spacing: 0.2px;
                    }

                    table {
                        border-collapse: separate;
                        font-size: 16px;
                        letter-spacing: 0.2px;
                        width: 100%;
                        margin: 0 auto;
                    }

                    table td {
                        vertical-align: middle;
                        padding: -10px;
                    }

                </style>
            </head>';
        $html .= '<body>
        <h4 style="text-align: center;">' . Auth::user()->company->CompanyName . '</h4>
        <p style="text-align: center;">' . Auth::user()->company->Address . ', ' . Auth::user()->company->City . '</p>
        <p style="text-align: center;">Telp : ' . Auth::user()->company->Phone . ', Fax : ' . Auth::user()->company->Fax . '</p>    
        <h3 style="text-align: center;">Shipping</h3>';
        $id = ShippingAddHeader::getIdshipping($id);
        $header = ShippingAddHeader::find($id);
        $detail = ShippingAddHeader::find($id)->shippingDetail()->get();
        $headerOrder = SalesOrderHeader::find($header->SalesOrderInternalID);
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $html .= '<table style="border-top: 0.4px solid #000000; border-bottom: 0.4px solid #000000;">
                <tr>
                    <td>' . $header->ShippingID . '</td>
                    <td colspan="2" style="text-align:right;">' . Auth::user()->UserID . '</td>
                </tr>
                <tr>
                    <td colspan="3">
                       ' . date("d-m-Y", strtotime($header->ShippingDate)) . '
                    </td>
                </tr>
                </table>';

            $html .= '<table style="border-bottom: 0.4px solid #000000;font-size: 18px;">';
//loop data inventory
            $count = 0;

            $i = 1;
            foreach ($detail as $data) {
                if ($data->ShippingParcelInternalID == 0) {
                    $inventory = Inventory::find($data->InventoryInternalID);
                    if ($inventory->TextPrint != '' || $inventory->TextPrint != Null) {
                        $inv = $inventory->InventoryID . '- ' . $inventory->TextPrint;
                    } else {
                        $inv = $inventory->InventoryID . '- ' . $inventory->InventoryName;
                    }

                    $html .= '<tr>
                                <td>
                                    ' . $i . '. ' . $inv . '
                                </td>
                            </tr>';
                    $html .= '<tr>
                                    <td style="text-align: right; width: 20%">' . number_format($data->Qty, 0, '.', ',') . ' ' . $data->Uom->UomID . '</td>

                              </tr>';

                    $count++;
                    $i++;
                }
            }
            $Parcel = ShippingAddHeader::searchParcel($id);
            foreach ($Parcel as $data) {

                $html .= '<tr>
                                <td>
                                    ' . $i . '. ' . $data->Parcel->ParcelID . ' - ' . $data->Parcel->ParcelName . '
                                </td>
                            </tr>';
                $html .= '<tr>
                                    <td style="text-align: right; width: 20%">' . number_format($data->Qty, 0, '.', ',') . ' ' . $data->Uom->UomID . '</td>

                              </tr>';

                $count++;
                $i++;
            }

            $html .= '</table>';
            $html .= '

                    <ul style="page-break-after:always; font-size: 12px; list-style: none; display: block; padding: 0; margin: 0 auto;">
                        <li style="margin-bottom: 2px; line-height: 14px; text-align: center;">Thank you for your visit.</li>
                        <li style="line-height: 14px; text-align: center;">Please come again later.</li>
                    </ul>
                    <script src="' . Asset("") . 'lib/bootstrap/js/jquery-1.11.1.min.js"></script>
                    <script>
                    
                        $(document).ready(function(e){
                        window.print();
                        });
                        $(document).click(function(e){
                        window.location.assign("' . URL("/") . '");
                        });
                    </script>
                </body>
            </html>';

            echo $html;
        }
    }

    static function shippingDataBackup($data) {
        $explode = explode('---;---', $data);
        $coa6 = $explode[0];
        $typePayment = $explode[1];
        $typeTax = $explode[2];
        $start = $explode[3];
        $end = $explode[4];
        $where = '';
        if ($typePayment != '-1' && $typePayment != '') {
            $where .= 'isCash = "' . $typePayment . '" ';
        }
        if ($coa6 != '-1' && $coa6 != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'ACC6InternalID = ' . $coa6 . ' ';
        }
        if (Auth::user()->SeeNPPN == 1) {
//            if ($typeTax != '-1' && $typeTax != '') {
//                if ($where != '') {
//                    $where .= ' AND ';
//                }
//                $where .= 'VAT = "' . $typeTax . '" ';
//            }
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'VAT = "0" ';
        } else {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'VAT = "1" ';
        }
        if ($start != '' && $end != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $start = explode('-', $start);
            $end = explode('-', $end);
            $startSearch = $start[2] . '-' . $start[1] . '-' . $start[0];
            $endSearch = $end[2] . '-' . $end[1] . '-' . $end[0];
            $where .= 'ShippingDate between "' . $startSearch . ' 00:00:00" AND "' . $endSearch . ' 23:59:59"';
        }
        if ($where != '') {
            $where .= ' AND ';
        }
        $where .= 'Type = ' . Auth::user()->WarehouseCheck . ' ';
//        if (empty($start)) {
//            if ($where != '') {
//                $where .= ' AND ';
//            }
//            $where .= 'ShippingDate = "' . date("Y-m-d") . '"';
//        }
        $table = 'v_wshipping';
        $primaryKey = 'InternalID';
//        $table = 't_shipping_header';
//        $primaryKey = 't_shipping_header`.`InternalID';
        $columns = array(
            array('db' => 'InternalID', 'dt' => 0, 'formatter' => function($d, $row) {
                    return $d;
                }),
            array('db' => 'ShippingID', 'dt' => 1),
            array('db' => 'SalesOrderID', 'dt' => 2),
            array('db' => 'isCash', 'dt' => 3, 'formatter' => function( $d, $row ) {
                    if ($d == 0) {
                        return 'Cash';
                    } else if ($d == 1) {
                        return 'Credit';
                    } else if ($d == 2) {
                        return 'CBD';
                    } else if ($d == 3) {
                        return 'Deposit';
                    } else {
                        return 'Down Payment';
                    }
                },
                'field' => 't_shipping_header`.`InternalID'),
            array(
                'db' => 'ShippingDate',
                'dt' => 4,
                'formatter' => function( $d, $row ) {
                    return date("d-m-Y", strtotime($d));
                }
            ),
            array(
                'db' => 'ACC6Name',
                'dt' => 5,
                'formatter' => function( $d, $row ) {
                    return $d;
                }
            ),
            array(
                'db' => 'NumberVehicle',
                'dt' => 6,
                'formatter' => function( $d, $row ) {
                    return $d;
                }
            ),
            array('db' => 'DriverName', 'dt' => 7),
            array(
                'db' => 'PackingID',
                'dt' => 8,
                "formatter" => function($d, $row) {
                    if ($d == null || $d == '') {
                        return '-';
                    } else
                        return $d;
                }
            ),
            array(
                'db' => 'Print',
                'dt' => 9,
                "formatter" => function($d, $row) {
                    return $d . " times";
                }
            ),
            array('db' => 'InternalID', 'dt' => 10, 'formatter' => function( $d, $row ) {
                    $data = ShippingAddHeader::find($d);
                    $so = SalesOrderHeader::find($data->SalesOrderInternalID);
                    $action = '<td class="text-center">
                                    <a href="' . Route('shippingDetail', $data->ShippingID) . '">
                                        <button id="btn-' . $data->ShippingID . '-detail"
                                                class="btn btn-pure-xs btn-xs btn-detail" title="detail">
                                            <span class="glyphicon glyphicon-zoom-in"></span>
                                        </button>
                                    </a>';
                    if ($data->PackingID == null || $data->PackingID == '') {
                        $action .= '<a href="' . Route('shippingUpdate', $data->ShippingID) . '">
                                        <button id="btn-' . $data->ShippingID . '-update"
                                                class="btn btn-pure-xs btn-xs btn-edit" title="update">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </button>
                    </a>';
                    } else {
                        $action .= '<a href="' . Route('packingUpdate', $data->PackingID) . '">
                                        <button id="btn-' . $data->PackingID . '-update"
                                                class="btn btn-pure-xs btn-xs btn-edit" title="update">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </button>
                                    </a>';
                    }
                    $action .= ' <button data-target="#m_shippingDelete" data-internal="' . $data->InternalID . '"  data-toggle="modal" role="dialog" title="delete"
                                           onclick="deleteAttach(this)" data-id="' . $data->ShippingID . '" data-name=' . $data->ShippingID . ' class="btn btn-pure-xs btn-xs btn-delete">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>';
                    if ($data->PackingID == null || $data->PackingID == '') {
                        $action .= '<a target="_blank" title="print" href="' . route('shippingPrint', $data->ShippingID) . '">'
                                . '<button id="btn-' . $data->ShippingID . '-print"'
                                . 'class="btn btn-pure-xs btn-xs btn-print" title="print">'
                                . '<span class="glyphicon glyphicon-print"></span>'
                                . '</button></a>';
                    } else {
                        $action .= '<a target="_blank" title="print" href="' . route('packingPrint', $data->PackingID) . '">'
                                . '<button id="btn-' . $data->PackingID . '-print"'
                                . 'class="btn btn-pure-xs btn-xs btn-print" title="print">'
                                . '<span class="glyphicon glyphicon-print"></span>'
                                . '</button></a>';
                    }
//                    $action .= '<a target="_blank" title="prin internalt" href="' . route('shippingInternalPrint', $data->ShippingID) . '">'
//                            . '<button id="btn-' . $data->ShippingID . '-print"'
//                            . 'class="btn btn-pure-xs btn-xs btn-print" title="internal print">'
//                            . 'Internal'
//                            . '</button></a>';
//                    if (checkModul('O05')) {
//                        $action.='<a href="' . Route('shippingCSV', $data->ShippingID) . '" target="_blank">
//                                        <button id="btn-' . $data->ShippingID . '-print"
//                                                class="btn btn-pure-xs btn-xs">
//                                            <span class="glyphicon glyphicon-download"></span> CSV
//                                        </button>
//                                    </a>';
//                    }
//                    $action.='<a href="' . Route('shippingPrintStruk', $data->ShippingID) . '" target="_blank" >
//                                        <button id="btn-' . $data->ShippingID . '-print"
//                                                class="btn btn-pure-xs btn-xs btn-edit">
//                                            <span class="glyphicon glyphicon-print"></span> Struk   
//                                        </button>
//                                    </a>';
                    if (checkSalesAdd($so->InternalID) && $so->Closed == 0) {
                        $action .= '<a href="' . Route('salesNew', $data->SalesOrder->SalesOrderID) . '" target="_blank">
                                        <button id="btn-' . $data->SalesOrder->SalesOrderID . '-sales"
                                                class="btn btn-pure-xs btn-xs" title="sales">
                                            <b>Sa</b>
                                        </button>
                                    </a>';
                    } else {
                        $action .= '<button disabled class="btn btn-pure-xs btn-xs btn-edit"><b>Sa</b></button>';
                    }
                    return $action;
                },
                'field' => 't_shipping_header`.`InternalID')
        );
        $sql_details = getConnection();

        require('ssp.class.php');
        $ID_CLIENT_VALUE = Auth::user()->CompanyInternalID;
        if ($where != '') {
            $extraCondition = $where . ' AND ' . 'CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        } else {
            $extraCondition = 'CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        }
//        $join = ' INNER JOIN m_warehouse on m_warehouse.InternalID = t_shipping_header.WarehouseInternalID';

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $extraCondition, $join = null));
    }

    static function packingDataBackup($data) {
        $explode = explode('---;---', $data);
        $coa6 = $explode[0];
        $typePayment = $explode[1];
        $typeTax = $explode[2];
        $start = $explode[3];
        $end = $explode[4];
        $where = '';
//        if ($typePayment != '-1' && $typePayment != '') {
//            $where .= 'isCash = "' . $typePayment . '" ';
//        }
        if ($coa6 != '-1' && $coa6 != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'ACC6InternalID = ' . $coa6 . ' ';
        }
//        if (Auth::user()->SeeNPPN == 1) {
//            if ($typeTax != '-1' && $typeTax != '') {
//                if ($where != '') {
//                    $where .= ' AND ';
//                }
//                $where .= 'VAT = "' . $typeTax . '" ';
//            }
//        } else {
//            if ($where != '') {
//                $where .= ' AND ';
//            }
//            $where .= 'VAT = "1" ';
//        }
        if ($start != '' && $end != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $start = explode('-', $start);
            $end = explode('-', $end);
            $startSearch = $start[2] . '-' . $start[1] . '-' . $start[0];
            $endSearch = $end[2] . '-' . $end[1] . '-' . $end[0];
            $where .= 'PackingDate between "' . $startSearch . ' 00:00:00" AND "' . $endSearch . ' 23:59:59"';
        }
//        if (empty($start)) {
//            if ($where != '') {
//                $where .= ' AND ';
//            }
//            $where .= 'ShippingDate = "' . date("Y-m-d") . '"';
//        }
        $table = 't_packing';
        $primaryKey = 't_packing`.`InternalID';
        $columns = array(
            array('db' => 'PackingID', 'dt' => 0),
//            array('db' => 'isCash', 'dt' => 1, 'formatter' => function( $d, $row ) {
//                    if ($d == 0) {
//                        return 'Cash';
//                    } else {
//                        return 'Credit';
//                    }
//                },
//                'field' => 't_shipping_header`.`InternalID'),
            array(
                'db' => 'PackingDate',
                'dt' => 1,
                'formatter' => function( $d, $row ) {
                    return date("d-m-Y", strtotime($d));
                }
            ),
            array(
                'db' => 'ACC6Name',
                'dt' => 2,
                'formatter' => function( $d, $row ) {
                    return $d;
                }
            ),
            array(
                'db' => 't_packing`.`InternalID',
                'dt' => 3,
                'formatter' => function( $d, $row ) {
                    return PackingShipping::where('PackingInternalID', $d)->count();
                }
            ),
//            array(
//                'db' => 'NumberVehicle',
//                'dt' => 4,
//                'formatter' => function( $d, $row ) {
//                    return $d;
//                }
//            ),
//            array('db' => 'DriverName', 'dt' => 5),
//            array(
//                'db' => 'Print',
//                'dt' => 6,
//                "formatter" => function($d, $row) {
//                    return $d . " times";
//                }
//            ),
            array('db' => 't_packing`.`InternalID', 'dt' => 4, 'formatter' => function( $d, $row ) {
                    $data = Packing::find($d);
                    $action = '<td class="text-center">
                                    <a href="' . Route('packingDetail', $data->PackingID) . '">
                                        <button id="btn-' . $data->PackingID . '-detail"
                                                class="btn btn-pure-xs btn-xs btn-detail" title="detail">
                                            <span class="glyphicon glyphicon-zoom-in"></span>
                                        </button>
                                    </a>';
                    $action .= '<a href="' . Route('packingUpdate', $data->PackingID) . '">
                                        <button id="btn-' . $data->PackingID . '-update"
                                                class="btn btn-pure-xs btn-xs btn-edit" title="update">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </button>
                                    </a>
                                    <button data-target="#m_packingDelete" data-internal="' . $data->InternalID . '"  data-toggle="modal" role="dialog" title="delete"
                                           onclick="deleteAttach(this)" data-id="' . $data->PackingID . '" data-name=' . $data->PackingID . ' class="btn btn-pure-xs btn-xs btn-delete">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>';
                    $action .= '<a target="_blank" title="print" href="' . route('packingPrint', $data->PackingID) . '">'
                            . '<button id="btn-' . $data->PackingID . '-print"'
                            . 'class="btn btn-pure-xs btn-xs btn-print" title="print">'
                            . '<span class="glyphicon glyphicon-print"></span>'
                            . '</button></a>';
                    $shipack = PackingShipping::where('PackingInternalID', $d)->first();
                    $ship = ShippingAddHeader::find($shipack->ShippingInternalID);
                    $so = SalesOrderHeader::find($ship->SalesOrderInternalID);
                    if (checkSalesAdd($so->InternalID) && $data->Closed == 0) {
                        $action .= '<a href="' . Route('salesNew', $so->SalesOrderID) . '" target="_blank">
                                        <button id="btn-' . $so->SalesOrderID . '-sales"
                                                class="btn btn-pure-xs btn-xs" title="sales">
                                            <b>Sa</b>
                                        </button>
                                    </a>';
                    } else {
                        $action .= '<button disabled class="btn btn-pure-xs btn-xs btn-edit"><b>Sa</b></button>';
                    }

//                    if (checkModul('O05')) {
//                        $action.='<a href="' . Route('shippingCSV', $data->ShippingID) . '" target="_blank">
//                                        <button id="btn-' . $data->ShippingID . '-print"
//                                                class="btn btn-pure-xs btn-xs">
//                                            <span class="glyphicon glyphicon-download"></span> CSV
//                                        </button>
//                                    </a>';
//                    }
//                    $action.='<a href="' . Route('shippingPrintStruk', $data->ShippingID) . '" target="_blank" >
//                                        <button id="btn-' . $data->ShippingID . '-print"
//                                                class="btn btn-pure-xs btn-xs btn-edit">
//                                            <span class="glyphicon glyphicon-print"></span> Struk   
//                                        </button>
//                                    </a>';
                    return $action;
                },
                'field' => 't_packing`.`InternalID')
        );
        $sql_details = getConnection();

        require('ssp.class.php');
        $ID_CLIENT_VALUE = Auth::user()->CompanyInternalID;
        if ($where != '') {
            $extraCondition = $where . ' AND ' . 't_packing.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        } else {
            $extraCondition = 't_packing.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        }
        $join = 'INNER JOIN m_coa6 on m_coa6.InternalID = t_packing.ACC6InternalID';

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $extraCondition, $join));
    }

//===================================//ajax=======================================
}
