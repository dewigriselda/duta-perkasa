<?php

class SalesAddController extends BaseController {

    public function showSales() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteSales') {
                return $this->deleteSales();
            } else if (Input::get('jenis') == 'summarySales') {
//                return $this->summarySales();
                return $this->summarySalesExcel();
            } else if (Input::get('jenis') == 'detailSales') {
//                return $this->summarySalesExcel();
                return $this->detailSales();
            } else if (Input::get('jenis') == 'insertSales') {
                return Redirect::Route('salesNew', Input::get('sales'));
            } else if (Input::get('jenis') == 'taxSales') {
                return $this->taxSales();
            } else if (Input::get('jenis') == 'salesReport') {
                return $this->salesReport();
            } else if (Input::get('jenis') == 'printSales') {
                if (Input::get('type') == 'exclude') {
                    return Redirect::route('salesPrint', Input::get('internalID'));
                } else {
                    return Redirect::route('salesIncludePrint', Input::get('internalID'));
                }
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = SalesAddHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
            return View::make('penjualanAdd.salesAddSearch')
                            ->withToogle('transaction')->withAktif('sales')
                            ->withData($data);
        }
        return View::make('penjualanAdd.salesAdd')
                        ->withToogle('transaction')->withAktif('sales');
    }

    public function salesNew($id) {
        $id = SalesOrderHeader::getIdsalesOrder($id);
        $header = SalesOrderHeader::find($id);
        $detail = SalesOrderHeader::find($id)->salesOrderDetail()->get();
        $description = SalesOrderHeader::find($id)->salesOrderDescription()->get();
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteSales') {
                return $this->deleteSales();
            } else if (Input::get('jenis') == 'summarySales') {
                return $this->summarySales();
            } else if (Input::get('jenis') == 'detailSales') {
                return $this->detailSales();
            } else if (Input::get('jenis') == 'insertSales') {
                return Redirect::Route('salesNew', Input::get('sales'));
            } else if (Input::get('jenis') == 'taxSales') {
                return $this->taxSales();
            } else if (Input::get('jenis') == 'printSales') {
                if (Input::get('type') == 'exclude') {
                    return Redirect::route('salesPrint', Input::get('internalID'));
                } else {
                    return Redirect::route('salesIncludePrint', Input::get('internalID'));
                }
            } else if (Input::get('jenis') == 'salesReport') {
                return $this->salesReport();
            } else {
                return $this->insertSales($id);
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = SalesAddHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
            return View::make('penjualanAdd.salesAddSearch')
                            ->withToogle('transaction')->withAktif('sales')
                            ->withData($data);
        }
        if ($header->VAT == 0)
            $tax = "NonTax";
        else
            $tax = "Tax";

        if ($header->isCash == 4) {
            $downpayment2 = TopUp::where('PaymentType', 2)
                    ->where('SalesOrderInternalID', $header->InternalID)
                    ->where('Type', "customer")
                    ->where("Remark", "!=", "Automatic dari Invoice")
                    ->sum('TopUp');
            $topup = SalesAddHeader::where('isCash', 4)
                    ->where('SalesOrderInternalID', $header->InternalID)
                    ->sum('DownPayment');
            $downpayment = $downpayment2 - $topup;
        } else {
            $downpayment = 0;
        }

        $sales = $this->createID(0, $tax) . '.';
        return View::make('penjualanAdd.salesAddNew')
                        ->withToogle('transaction')->withAktif('sales')
                        ->withHeader($header)
                        ->withDetail($detail)
                        ->withDescription($description)
                        ->withDownpayment($downpayment)
                        ->withSales($sales);
    }

    public function salesCashNew() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteSales') {
                return $this->deleteSales();
            } else if (Input::get('jenis') == 'summarySales') {
                return $this->summarySales();
            } else if (Input::get('jenis') == 'detailSales') {
                return $this->detailSales();
            } else if (Input::get('jenis') == 'insertSales') {
                return Redirect::Route('salesNew', Input::get('sales'));
            } else if (Input::get('jenis') == 'taxSales') {
                return $this->taxSales();
            } else if (Input::get('jenis') == 'printSales') {
                if (Input::get('type') == 'exclude') {
                    return Redirect::route('salesPrint', Input::get('internalID'));
                } else {
                    return Redirect::route('salesIncludePrint', Input::get('internalID'));
                }
            } else if (Input::get('jenis') == 'salesReport') {
                return $this->salesReport();
            } else {
                return $this->insertSalesCash();
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = SalesAddHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
            return View::make('penjualanAdd.salesAddSearch')
                            ->withToogle('transaction')->withAktif('sales')
                            ->withData($data);
        }
        $sales = $this->createID(0) . '.';
        return View::make('penjualanAdd.salesCashNew')
                        ->withToogle('transaction')->withAktif('sales')
                        ->withSales($sales);
    }

    public function salesDetail($id) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'summarySales') {
                return $this->summarySales();
            } else if (Input::get('jenis') == 'detailSales') {
                return $this->detailSales();
            } else if (Input::get('jenis') == 'insertSales') {
                return Redirect::Route('salesNew', Input::get('sales'));
            } else if (Input::get('jenis') == 'deleteSales') {
                return $this->deleteSales();
            } else if (Input::get('jenis') == 'taxSales') {
                return $this->taxSales();
            } else if (Input::get('jenis') == 'printSales') {
                if (Input::get('type') == 'exclude') {
                    return Redirect::route('salesPrint', Input::get('internalID'));
                } else {
                    return Redirect::route('salesIncludePrint', Input::get('internalID'));
                }
            } else if (Input::get('jenis') == 'salesReport') {
                return $this->salesReport();
            }
        }
        $id = SalesAddHeader::getIdsales($id);
        $header = SalesAddHeader::find($id);
        $detail = SalesAddHeader::find($id)->salesDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            return View::make('penjualanAdd.salesAddDetail')
                            ->withToogle('transaction')->withAktif('sales')
                            ->withHeader($header)
                            ->withDetail($detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showSales');
        }
    }

    public function salesUpdate($id) {
        $id = SalesAddHeader::getIdsales($id);
        $header = SalesAddHeader::find($id);
        $detail = SalesAddHeader::find($id)->salesDetail()->get();
        $description = SalesAddHeader::find($id)->salesDescription()->get();
        $idSalesOrder = $header->SalesOrderInternalID;
        $headerSalesOrder = SalesOrderHeader::find($idSalesOrder);
        $detailSalesOrder = SalesOrderHeader::find($idSalesOrder)->salesOrderDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID && SalesAddHeader::isReturn($header->SalesID) == false) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                if (Input::get('jenis') == 'summarySales') {
                    return $this->summarySales();
                } else if (Input::get('jenis') == 'detailSales') {
                    return $this->detailSales();
                } else if (Input::get('jenis') == 'insertSales') {
                    return Redirect::Route('salesNew', Input::get('sales'));
                } else if (Input::get('jenis') == 'deleteSales') {
                    return $this->deleteSales();
                } else if (Input::get('jenis') == 'taxSales') {
                    return $this->taxSales();
                } else if (Input::get('jenis') == 'printSales') {
                    if (Input::get('type') == 'exclude') {
                        return Redirect::route('salesPrint', Input::get('internalID'));
                    } else {
                        return Redirect::route('salesIncludePrint', Input::get('internalID'));
                    }
                } else if (Input::get('jenis') == 'salesReport') {
                    return $this->salesReport();
                } else {
                    return $this->updateSales($id);
                }
            }
            if ($header->isCash == 4) {
                $downpayment = TopUp::where('PaymentType', 2)
                        ->where('SalesOrderInternalID', $header->SalesOrderInternalID)
                        ->where('Type', "customer")
                        ->where("Remark", "!=", "Automatic dari Invoice")
                        ->sum('TopUp');
                $topup = SalesAddHeader::where('isCash', 4)
                        ->where('SalesOrderInternalID', $header->SalesOrderInternalID)
                        ->where('InternalID', "!=", $header->InternalID)
                        ->sum('DownPayment');
                $totalDownpayment = $downpayment - $topup;
            } else {
                $totalDownpayment = 0;
            }


            return View::make('penjualanAdd.salesAddUpdate')
                            ->withToogle('transaction')->withAktif('sales')
                            ->withHeader($header)
                            ->withDetail($detail)
                            ->withDescription($description)
                            ->withDownpayment($totalDownpayment)
                            ->withHeaderorder($headerSalesOrder)
                            ->withDetailorder($detailSalesOrder);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showSales');
        }
    }

    public function insertSalesCash() {
        //rule
        $jenis = Input::get('isCash');
        if ($jenis == '0') {
            $rule = array(
                'date' => 'required',
                'coa6' => 'required',
                'slip' => 'required',
//                'remark' => 'required|max:1000',
                'currency' => 'required',
                'warehouse' => 'required',
                'rate' => 'required',
                'purchasingCommission' => 'required',
                'salesman' => 'required'
            );
            $longTerm = 0;
        } else {
            $rule = array(
                'date' => 'required',
                'longTerm' => 'required|integer',
                'coa6' => 'required',
//                'remark' => 'required|max:1000',
                'currency' => 'required',
                'warehouse' => 'required',
                'rate' => 'required',
                'purchasingCommission' => 'required',
                'salesman' => 'required'
            );
            $longTerm = Input::get('longTerm');
        }
        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalInsert';
            $error = $validator->messages();
        } else {
            //valid
            //insert sales order dulu
            $tax = '';
            if (Input::get('vat') == '') {
                $tax = "NonTax";
            } else {
                $tax = "Tax";
            }
            $header = new SalesOrderHeader;
            $salesOrder = $this->createIDSO(1, $tax) . '.';
            $date = explode('-', Input::get('date'));
            $yearDigit = substr($date[2], 2);
            $salesOrder .= $date[1] . $yearDigit . '.';
            $salesOrderNumber = SalesOrderHeader::getNextIDSalesOrder($salesOrder);
            $header->SalesOrderID = $salesOrderNumber;
            $header->SalesOrderDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $header->ACC6InternalID = Input::get('coa6');
            $header->PurchasingCommission = str_replace(',', '', Input::get('purchasingCommission'));
            $header->SalesManInternalID = Input::get('salesman');
            $header->LongTerm = $longTerm;
            $header->isCash = $jenis;
            $currency = explode('---;---', Input::get('currency'));
            $header->WarehouseInternalID = Input::get('warehouse');
            $header->CurrencyInternalID = $currency[0];
            $header->CurrencyRate = str_replace(',', '', Input::get('rate'));
            if (Input::get('vat') == '') {
                $header->VAT = '0';
            } else {
                $header->VAT = Input::get('vat');
            }
            $header->DiscountGlobal = str_replace(",", "", Input::get('DiscountGlobal'));
            $header->GrandTotal = Input::get('grandTotalValue');
            $header->POCustomer = Input::get('POCustomer');
            $header->DeliveryTerms = Input::get('DeliveryTerms');
            $header->DeliveryTime = Input::get('DeliveryTime');
            $header->Print = 1;
            $header->Status = 1;
            $header->UserRecord = Auth::user()->UserID;
            $header->CompanyInternalID = Auth::user()->Company->InternalID;
            $header->UserModified = '0';
            $header->Remark = Input::get('remark');
            $header->save();
            $total = 0;
            for ($a = 0; $a < count(Input::get('inventory')); $a++) {
                $tamp = explode("---;---", Input::get('inventory')[$a]);
                $qtyValue = str_replace(',', '', Input::get('qty')[$a]);
                $priceValue = str_replace(',', '', Input::get('price')[$a]);
                $discValue = str_replace(',', '', Input::get('discountNominal')[$a]);
                $description = new SalesOrderDescription();
                $description->SalesOrderInternalID = $header->InternalID;
                if ($tamp[1] != "parcel") {
                    $description->InventoryText = Inventory::find(Input::get('inventory')[$a])->InventoryName;
                    $description->UomText = Uom::find(Input::get('uom')[$a])->UomID;
                } else {
                    $description->InventoryText = Parcel::find($tamp[0])->ParcelName;
                    $description->UomText = '-';
                }
                $description->Price = $priceValue;
                $description->DiscountNominal = $discValue;
                $description->Discount = Input::get('discount')[$a];
                $subTotal = ($priceValue * $qtyValue ) - (($priceValue * $qtyValue) * Input::get('discount')[$a] / 100) - $discValue * $qtyValue;
                if ($header->VAT == '1') {
                    $vatValue = $subTotal / 10;
                } else {
                    $vatValue = 0;
                }
                $description->VAT = $vatValue;
                $description->Qty = $qtyValue;
                $description->SubTotal = $qtyValue * $priceValue;
                $description->Spesifikasi = '-';
                $description->UserRecord = Auth::user()->UserID;
                $description->UserModified = '0';
                $description->save();

                if ($tamp[1] != "parcel") {
                    //jika bukan paket
                    $uom = Input::get('uom')[$a];
                    $subTotal = ($priceValue * $qtyValue) - (($priceValue * $qtyValue) * Input::get('discount')[$a] / 100) - $discValue * $qtyValue;
                    if (Input::get('vat') == '1') {
                        $vatValue = $subTotal / 10;
                    } else {
                        $vatValue = 0;
                    }
                    if ($qtyValue > 0) {
                        $detail = new SalesOrderDetail();
                        $detail->SalesOrderInternalID = $header->InternalID;
                        $detail->InventoryInternalID = Input::get('inventory')[$a];
                        $detail->DescriptionInternalID = $description->InternalID;
                        $detail->UomInternalID = $uom;
                        $detail->SalesOrderParcelInternalID = 0;
                        $detail->Qty = $qtyValue;
                        $detail->Price = $priceValue;
                        $detail->Discount = Input::get('discount')[$a];
                        $detail->DiscountNominal = $discValue;
                        $detail->VAT = $vatValue;
                        $detail->SubTotal = $subTotal;
                        $detail->UserRecord = Auth::user()->UserID;
                        $detail->UserModified = '0';
                        $detail->save();
                    }
                    $total += $subTotal;
                } else {
                    //jika paket
                    $parcelDetail = ParcelInventory::where("ParcelInternalID", $tamp[0])->where("CompanyInternalID", Auth::user()->Company->InternalID)->get();
                    $subTotal = ($priceValue * $qtyValue) - (($priceValue * $qtyValue) * Input::get('discount')[$a] / 100) - $discValue * $qtyValue;
                    if (Input::get('vat') == '1') {
                        $vatValue = $subTotal / 10;
                    } else {
                        $vatValue = 0;
                    }
                    if ($qtyValue > 0) {
                        $SOParcel = new SalesOrderParcel();
                        $SOParcel->SalesOrderInternalID = $header->InternalID;
                        $SOParcel->ParcelInternalID = $tamp[0];
                        $SOParcel->DescriptionInternalID = $description->InternalID;
                        $SOParcel->Qty = $qtyValue;
                        $SOParcel->Price = $priceValue;
                        $SOParcel->Discount = Input::get('discount')[$a];
                        $SOParcel->VAT = $vatValue;
                        $SOParcel->DiscountNominal = $discValue;
                        $SOParcel->SubTotal = $subTotal;
                        $SOParcel->CompanyInternalID = Auth::user()->Company->InternalID;
                        $SOParcel->UserRecord = Auth::user()->UserID;
                        $SOParcel->UserModified = "0";
                        $SOParcel->Remark = "-";
                        $SOParcel->save();

                        foreach ($parcelDetail as $dataParcel) {
                            $uom1 = $dataParcel->UomInternalID;
                            $qtyValue1 = $dataParcel->Qty * $qtyValue;
                            $priceValue1 = $dataParcel->Price;
                            $subTotal1 = ($priceValue1 * $qtyValue1);
                            if (Input::get('vat') == '1') {
                                $vatValue1 = $subTotal1 / 10;
                            } else {
                                $vatValue1 = 0;
                            }
                            $detail = new SalesOrderDetail();
                            $detail->SalesOrderInternalID = $header->InternalID;
                            $detail->InventoryInternalID = $dataParcel->InventoryInternalID;
                            $detail->UomInternalID = $uom1;
                            $detail->SalesOrderParcelInternalID = $SOParcel->InternalID;
                            $detail->Qty = $qtyValue1;
                            $detail->Price = $priceValue1;
                            $detail->Discount = 0;
                            $detail->DiscountNominal = 0;
                            $detail->VAT = $vatValue1;
                            $detail->SubTotal = $subTotal1;
                            $detail->UserRecord = Auth::user()->UserID;
                            $detail->UserModified = '0';
                            $detail->save();
                        }
                    }
                    $total += $subTotal;
                }
            }

            $salesOrder = $header;
            //insert header
            $header2 = new SalesHeader;
            $sales = $this->createID(1, $tax) . '.';
            $sales .= $date[1] . $yearDigit . '.';
            $salesNumber = SalesAddHeader::getNextIDSales($sales);
            $header2->SalesID = $salesNumber;
            $header2->SalesDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $header2->ACC6InternalID = $salesOrder->ACC6InternalID;
            $header2->LongTerm = $salesOrder->LongTerm;
            $header2->isCash = $salesOrder->isCash;
            $header2->WarehouseInternalID = Input::get('warehouse');
            $header2->PurchasingCommission = str_replace(',', '', Input::get('purchasingCommission'));
            $header2->CurrencyInternalID = $salesOrder->CurrencyInternalID;
            $header2->SalesManInternalID = $salesOrder->SalesManInternalID;
            $header2->CurrencyRate = $salesOrder->CurrencyRate;
            $header2->VAT = $salesOrder->VAT;
            $header2->DiscountGlobal = str_replace(",", "", Input::get("DiscountGlobal"));
            $header2->GrandTotal = Input::get("grandTotalValue");
            $header2->DownPayment = str_replace(",", "", Input::get('DownPayment'));
            $header2->TransactionType = Input::get('TransactionType');
            if (Input::get('Replacement') == '') {
                $header2->Replacement = '0';
            } else {
                $header2->Replacement = Input::get('Replacement');
            }
            if (Input::get('TaxNumber') == '') {
                $header2->TaxNumber = '.-.';
            } else {
                $header2->TaxNumber = Input::get('TaxNumber');
            }
            $header2->TaxMonth = Input::get('TaxMonth');
            $header2->TaxYear = Input::get('TaxYear');
            $header2->TaxDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $header2->UserRecord = Auth::user()->UserID;
            $header2->CompanyInternalID = Auth::user()->Company->InternalID;
            $header2->SalesOrderInternalID = $header->InternalID;
            $header2->Print = 1;
            $header2->UserModified = '0';
            $header2->Remark = Input::get('remark');
            $header2->save();
            //insert detail
            $total = 0;

            foreach (SalesOrderDescription::where('SalesOrderInternalID', $header->InternalID)->get() as $sodesc) {
                if ($sodesc->Qty > 0) {
                    $data = $sodesc;
                    $description = new SalesAddDescription();
                    $description->SalesInternalID = $header2->InternalID;
                    $description->InventoryText = $data->InventoryText;
                    $description->UomText = $data->UomText;
                    $description->Price = $data->Price;
                    $description->DiscountNominal = $data->DiscountNominal;
                    $description->Discount = $data->Discount;
                    $qty = $sodesc->Qty;
                    $priceValue = $data->Price;
                    $discValue = $data->DiscountNominal;
                    $subTotal = ($priceValue * $qty ) - (($priceValue * $qty) * $data->Discount / 100) - $discValue * $qty;
                    if ($header2->VAT == '1') {
                        $vatValue = $subTotal / 10;
                    } else {
                        $vatValue = 0;
                    }
                    $description->VAT = $vatValue;
                    $description->Qty = $qty;
                    $description->SubTotal = $qty / $data->Qty * $data->SubTotal;
                    $description->Spesifikasi = $data->Spesifikasi;
                    $description->SalesOrderDescriptionInternalID = $sodesc->InternalID;
                    $description->UserRecord = Auth::user()->UserID;
                    $description->UserModified = '0';
                    $description->save();

                    foreach (SalesOrderDetail::where('DescriptionInternalID', $sodesc->InternalID)->get() as $sodet) {
                        if ($sodet->SalesOrderParcelInternalID == 0) {
                            $detail = $sodet;
                            //jika non paket
                            $qtyValue = $detail->Qty;
                            $qtySalesValue = str_replace(',', '', $sodet->Qty);
                            $sumSales = SalesAddHeader::getSumSales($detail->InventoryInternalID, $header->InternalID, $sodet->InternalID);
                            if ($sumSales == '') {
                                $sumSales = '0';
                            }
                            $sumSalesReturn = SalesReturnHeader::getSumReturnOrder($detail->InventoryInternalID, $header->InternalID, $sodet->InternalID);
                            if ($sumSalesReturn == '') {
                                $sumSalesReturn = '0';
                            }
                            $qtyValue = $qtyValue - $sumSales + $sumSalesReturn;
                            if ($qtyValue < $qtySalesValue) {
                                $qtySalesValue = $qtyValue;
                            }
                            $priceValue = $detail->Price;
                            $discValue = $detail->DiscountNominal;
                            $subTotal = ($priceValue * $qtySalesValue ) - (($priceValue * $qtySalesValue) * $detail->Discount / 100) - $discValue * $qtySalesValue;
                            if ($header2->VAT == '1') {
                                $vatValue = $subTotal / 10;
                            } else {
                                $vatValue = 0;
                            }
                            if ($qtySalesValue > 0) {
                                $detail2 = new SalesAddDetail();
                                $detail2->SalesInternalID = $header2->InternalID;
                                $detail2->InventoryInternalID = $detail->InventoryInternalID;
                                $detail2->DescriptionInternalID = $description->InternalID;
                                $detail2->UomInternalID = $detail->UomInternalID;
                                $detail2->Qty = $qtySalesValue;
                                $detail2->Price = $priceValue;
                                $detail2->Discount = $detail->Discount;
                                $detail2->DiscountNominal = $discValue;
                                $detail2->VAT = $vatValue;
                                $detail2->SubTotal = $subTotal;
                                $detail2->UserRecord = Auth::user()->UserID;
                                $detail2->UserModified = '0';
                                $detail2->SalesOrderDetailInternalID = $sodet->InternalID;
                                $detail2->save();
                                setTampInventory($detail->InventoryInternalID);
                            }
                            $total += $subTotal;
                        } else {
                            $detail = SalesOrderParcel::find($sodet->SalesOrderParcelInternalID);
                            //jika paket
                            $parcelDetail = ParcelInventory::where("ParcelInternalID", $detail->ParcelInternalID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->get();
                            $qtyValue = $detail->Qty;
                            $qtySalesValue = str_replace(',', '', $detail->Qty);
                            $sumSales = SalesAddHeader::getSumSalesParcel($detail->ParcelInternalID, "", $sodet->InternalID);
                            $priceValue = $detail->Price;
                            $discValue = $detail->DiscountNominal;
                            $subTotal = ($priceValue * $qtySalesValue ) - (($priceValue * $qtySalesValue) * $detail->Discount / 100) - $discValue * $qtySalesValue;
                            if ($sumSales == '') {
                                $sumSales = '0';
                            }
                            $sumSalesReturnParcel = SalesReturnHeader::getSumReturnOrderParcel($detail->ParcelInternalID, $header2->SalesOrderInternalID, $sodet->SalesOrderParcelInternalID);
                            if ($sumSalesReturnParcel == '') {
                                $sumSalesReturnParcel = '0';
                            }
                            $qtyValue = $qtyValue - $sumSales + $sumSalesReturnParcel;
                            if ($qtyValue < $qtySalesValue) {
                                $qtySalesValue = $qtyValue;
                            }
                            if ($header2->VAT == '1') {
                                $vatValue = $subTotal / 10;
                            } else {
                                $vatValue = 0;
                            }
                            if ($qtySalesValue > 0) {
                                $SAParcel = new SalesAddParcel();
                                $SAParcel->SalesInternalID = $header2->InternalID;
                                $SAParcel->ParcelInternalID = $detail->ParcelInternalID;
                                $SAParcel->DescriptionInternalID = $description->InternalID;
                                $SAParcel->SalesOrderParcelDetailInternalID = $sodet->InternalID;
                                $SAParcel->Qty = $qtySalesValue;
                                $SAParcel->Price = $priceValue;
                                $SAParcel->Discount = $detail->Discount;
                                $SAParcel->VAT = $vatValue;
                                $SAParcel->DiscountNominal = $discValue;
                                $SAParcel->SubTotal = $subTotal;
                                $SAParcel->CompanyInternalID = Auth::user()->Company->InternalID;
                                $SAParcel->UserRecord = Auth::user()->UserID;
                                $SAParcel->UserModified = "0";
                                $SAParcel->Remark = "-";
                                $SAParcel->save();

                                foreach ($parcelDetail as $dataParcel) {
                                    $uom1 = $dataParcel->UomInternalID;
                                    $qtyValue1 = $dataParcel->Qty * $qtySalesValue;
                                    $priceValue1 = $dataParcel->Price;
                                    $subTotal1 = ($priceValue1 * $qtyValue1);
                                    if (Input::get('vat') == '1') {
                                        $vatValue1 = $subTotal1 / 10;
                                    } else {
                                        $vatValue1 = 0;
                                    }
                                    $detail2 = new SalesAddDetail();
                                    $detail2->SalesInternalID = $header2->InternalID;
                                    $detail2->InventoryInternalID = $dataParcel->InventoryInternalID;
                                    $detail2->UomInternalID = $uom1;
                                    $detail2->SalesParcelInternalID = $SAParcel->InternalID;
                                    $detail2->DescriptionInternalID = $description->InternalID;
                                    $detail2->Qty = $qtyValue1;
                                    $detail2->Price = $priceValue1;
                                    $detail2->Discount = 0;
                                    $detail2->DiscountNominal = 0;
                                    $detail2->VAT = $vatValue1;
                                    $detail2->SubTotal = $subTotal1;
                                    $detail2->SalesOrderDetailInternalID = $sodet->InternalID;
                                    $detail2->UserRecord = Auth::user()->UserID;
                                    $detail2->UserModified = '0';
                                    $detail2->save();
                                    setTampInventory($dataParcel->InventoryInternalID);
                                }
                            }
                            $total += $subTotal;
                        }
                    }
                }
            }

            if ($header2->VAT == '1') {
                $vatValueHeader = ($total - str_replace(",", "", Input::get("DiscountGlobal")) - str_replace(",", "", Input::get('DownPayment'))) / 10;
            } else {
                $vatValueHeader = 0;
            }
            $header2->GrandTotal = $total - str_replace(",", "", Input::get("DiscountGlobal")) - str_replace(",", "", Input::get('DownPayment')) + $vatValueHeader;
            $header2->save();

            $currency = $salesOrder->CurrencyInternalID;
            $rate = $salesOrder->CurrencyRate;
            $total -= str_replace(",", "", Input::get('DiscountGlobal'));
            $total -= str_replace(",", "", Input::get('DownPayment'));
            if ($header2->isCash == 0) {
                $slip = Input::get('slip');
                $this->insertJournal($salesNumber, $total, $salesOrder->VAT, $currency, $rate, $date, $slip, str_replace(",", "", Input::get('DownPayment')));
            } else {
                $slip = '-1';
                $this->insertJournal($salesNumber, $total, $salesOrder->VAT, $currency, $rate, $date, $slip, str_replace(",", "", Input::get('DownPayment')));
            }

            //insert shipping!!!
            $header3 = new ShippingAddHeader;
            $shipping = $this->createIDShip(1, $tax) . '.';
            $shipping .= $date[1] . $yearDigit . '.';
            $shippingNumber = ShippingAddHeader::getNextIDShipping($shipping);
            $header3->ShippingID = $shippingNumber;
            $header3->ShippingDate = $header->SalesOrderDate;
            $header3->ACC6InternalID = $salesOrder->ACC6InternalID;
            $header3->LongTerm = $salesOrder->LongTerm;
            $header3->isCash = $salesOrder->isCash;
            $header3->WarehouseInternalID = Input::get('warehouse');
            $header3->VAT = $salesOrder->VAT;
            $header3->TransactionType = Input::get('TransactionType');
            if (Input::get('Replacement') == '') {
                $header3->Replacement = '0';
            } else {
                $header3->Replacement = Input::get('Replacement');
            }
            if (Input::get('TaxNumber') == '') {
                $header3->TaxNumber = '.-.';
            } else {
                $header3->TaxNumber = Input::get('TaxNumber');
            }
            $header3->TaxMonth = Input::get('TaxMonth');
            $header3->TaxYear = Input::get('TaxYear');
            $header3->TaxDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $header3->NumberVehicle = Input::get('vehicle');
            $header3->DriverName = Input::get('driver');
            $header3->Print = 1;
            $header3->UserRecord = Auth::user()->UserID;
            $header3->CompanyInternalID = Auth::user()->Company->InternalID;
            $header3->SalesOrderInternalID = $header->InternalID;
            $header3->UserModified = '0';
            $header3->Remark = Input::get('remark');
            $header3->save();
            //insert detail
            $total = 0;
//            for ($a = 0; $a < count(Input::get('InternalDescription')); $a++) {
//                echo "<pre>";
//                print_r(Input::get('InternalDetail' . ($a + 1)));
//                echo "</pre>";
//                echo "<pre>";
//                print_r(Input::get('tipe' . ($a + 1)));
//                echo "</pre>";
//            }exit();
            foreach (SalesOrderDescription::where('SalesOrderInternalID', $header->InternalID)->get() as $sodesc) {
                if ($sodesc->Qty > 0) {
                    $data = $sodesc;
                    $description = new ShippingAddDescription();
                    $description->ShippingInternalID = $header3->InternalID;
                    $description->InventoryText = $data->InventoryText;
                    $description->UomText = $data->UomText;
                    $qty = $sodesc->Qty;
                    $description->Qty = $qty;
                    $description->Spesifikasi = $data->Spesifikasi;
                    $description->SalesOrderDescriptionInternalID = $sodesc->InternalID;
                    $description->UserRecord = Auth::user()->UserID;
                    $description->UserModified = '0';
                    $description->save();

                    foreach (SalesOrderDetail::where('DescriptionInternalID', $sodesc->InternalID)->get() as $sodet) {
                        if ($sodet->SalesOrderParcelInternalID == 0) {
                            $detail = $sodet;
                            //jika non paket
                            $qtyValue = $detail->Qty;
                            $qtyShippingValue = str_replace(',', '', $sodet->Qty);
                            $sumShipping = ShippingAddHeader::getSumShipping($detail->InventoryInternalID, $header->InternalID, $sodet->InternalID);
                            if ($sumShipping == '') {
                                $sumShipping = '0';
                            }
                            $qtyValue = $qtyValue - $sumShipping;
                            if ($qtyValue < $qtyShippingValue) {
                                $qtyShippingValue = $qtyValue;
                            }
                            if ($qtyShippingValue > 0) {
                                $detail2 = new ShippingAddDetail();
                                $detail2->ShippingInternalID = $header3->InternalID;
                                $detail2->InventoryInternalID = $detail->InventoryInternalID;
                                $detail2->DescriptionInternalID = $description->InternalID;
                                $detail2->UomInternalID = $detail->UomInternalID;
                                $detail2->Qty = $qtyShippingValue;
                                $detail2->UserRecord = Auth::user()->UserID;
                                $detail2->UserModified = '0';
                                $detail2->SalesOrderDetailInternalID = $sodet->InternalID;
                                $detail2->save();
                                setTampInventory($detail->InventoryInternalID);
                            }
                        } else {
                            $detail = SalesOrderParcel::find($sodet->SalesOrderParcelInternalID);
                            //jika paket
                            $parcelDetail = ParcelInventory::where("ParcelInternalID", $detail->ParcelInternalID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->get();
                            $qtyValue = $detail->Qty;
                            $qtyShippingValue = str_replace(',', '', $detail->Qty);
                            $sumShipping = ShippingAddHeader::getSumShippingParcel($detail->ParcelInternalID, "", $sodet->SalesOrderParcelInternalID);
                            if ($sumShipping == '') {
                                $sumShipping = '0';
                            }
                            $qtyValue = $qtyValue - $sumShipping;
                            if ($qtyValue < $qtyShippingValue) {
                                $qtyShippingValue = $qtyValue;
                            }
                            if ($qtyShippingValue > 0) {
                                $SAParcel = new ShippingAddParcel();
                                $SAParcel->ShippingInternalID = $header3->InternalID;
                                $SAParcel->ParcelInternalID = $detail->ParcelInternalID;
                                $SAParcel->DescriptionInternalID = $description->InternalID;
                                $SAParcel->SalesOrderParcelDetailInternalID = $sodet->SalesOrderParcelInternalID;
                                $SAParcel->Qty = $qtyShippingValue;
                                $SAParcel->CompanyInternalID = Auth::user()->Company->InternalID;
                                $SAParcel->UserRecord = Auth::user()->UserID;
                                $SAParcel->UserModified = "0";
                                $SAParcel->Remark = "-";
                                $SAParcel->save();

                                foreach ($parcelDetail as $dataParcel) {
                                    $uom1 = $dataParcel->UomInternalID;
                                    $qtyValue1 = $dataParcel->Qty * $qtyShippingValue;
                                    $detail2 = new ShippingAddDetail();
                                    $detail2->ShippingInternalID = $header3->InternalID;
                                    $detail2->InventoryInternalID = $dataParcel->InventoryInternalID;
                                    $detail2->UomInternalID = $uom1;
                                    $detail2->ShippingParcelInternalID = $SAParcel->InternalID;
                                    $detail2->Qty = $qtyValue1;
                                    $detail2->SalesOrderDetailInternalID = $sodet->InternalID;
                                    $detail2->UserRecord = Auth::user()->UserID;
                                    $detail2->UserModified = '0';
                                    $detail2->save();
                                    setTampInventory($dataParcel->InventoryInternalID);
                                }
                            }
                        }
                    }
                }
            }
            $messages = 'suksesInsert';
            $error = '';
        }
        $sales = $this->createID(0) . '.';
//        $header2 = SalesOrderHeader::find($header->InternalID);
//        $detail = SalesOrderHeader::find($header->InternalID)->salesOrderDetail()->get();
//        $print = Route('salesPrint', $salesNumber);

        return View::make('penjualanAdd.salesCashNew')
                        ->withToogle('transaction')->withAktif('sales')->withMessages($messages)
                        ->withSales($sales)->withError($error)
//                ->withPrint($print)
        ;
    }

    public function insertSales($id) {
        //rule
        $salesOrderInternalID = Input::get('SalesOrderInternalID');
        $salesOrder = SalesOrderHeader::find($salesOrderInternalID);
        if ($salesOrder->isCash == 0) {
            $rule = array(
                'date' => 'required',
//                'remark' => 'required|max:1000',
                'warehouse' => 'required',
                'purchasingCommission' => 'required',
                'slip' => 'required',
                'InternalDescription' => 'required'
            );
        } else {
            $rule = array(
                'date' => 'required',
//                'remark' => 'required|max:1000',
                'purchasingCommission' => 'required',
                'warehouse' => 'required',
                'InternalDescription' => 'required'
            );
            $longTerm = 0;
        }
        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalInsert';
            $error = $validator->messages();
        } else {
            //valid
            //insert header
            $tax = '';
            if ($salesOrder->VAT == 0) {
                $tax = "NonTax";
            } else {
                $tax = "Tax";
            }
            $header = new SalesHeader;
            $sales = $this->createID(1, $tax) . '.';
            $date = explode('-', Input::get('date'));
            $yearDigit = substr($date[2], 2);
            $sales .= $date[1] . $yearDigit . '.';
            $salesNumber = SalesAddHeader::getNextIDSales($sales);
            $header->SalesID = $salesNumber;
            $header->SalesDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $header->ACC6InternalID = $salesOrder->ACC6InternalID;
            $header->LongTerm = $salesOrder->LongTerm;
            $header->isCash = $salesOrder->isCash;
            $header->WarehouseInternalID = Input::get('warehouse');
            $header->PurchasingCommission = str_replace(',', '', Input::get('purchasingCommission'));
            $header->CurrencyInternalID = $salesOrder->CurrencyInternalID;
            $header->SalesManInternalID = $salesOrder->SalesManInternalID;
            $header->CurrencyRate = $salesOrder->CurrencyRate;
            $header->VAT = $salesOrder->VAT;
            $header->DiscountGlobal = str_replace(",", "", Input::get("DiscountGlobal"));
            $header->GrandTotal = Input::get("grandTotalValue");
            $header->DownPayment = str_replace(",", "", Input::get('DownPayment'));
            $header->TransactionType = Input::get('TransactionType');
            if (Input::get('Replacement') == '') {
                $header->Replacement = '0';
            } else {
                $header->Replacement = Input::get('Replacement');
            }
            if (Input::get('TaxNumber') == '') {
                $header->TaxNumber = '.-.';
            } else {
                $header->TaxNumber = Input::get('TaxNumber');
            }
            $header->TaxMonth = Input::get('TaxMonth');
            $header->TaxYear = Input::get('TaxYear');
            $header->TaxDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $header->UserRecord = Auth::user()->UserID;
            $header->CompanyInternalID = Auth::user()->Company->InternalID;
            $header->SalesOrderInternalID = $salesOrderInternalID;
            $header->Print = 1;
            $header->UserModified = '0';
            $header->Remark = Input::get('remark');
            $header->save();
            //insert detail
            $total = 0;

            for ($a = 0; $a < count(Input::get('InternalDescription')); $a++) {
                if (Input::get('qtyDescription')[$a] > 0) {

                    $data = SalesOrderDescription::find(Input::get('InternalDescription')[$a]);
                    $description = new SalesAddDescription();
                    $description->SalesInternalID = $header->InternalID;
                    $description->InventoryText = $data->InventoryText;
                    $description->UomText = $data->UomText;
                    $description->Price = $data->Price;
                    $description->DiscountNominal = $data->DiscountNominal;
                    $description->Discount = $data->Discount;
                    $qty = 0;
                    if (Input::get('max')[$a] < Input::get('qtyDescription')[$a])
                        $qty = Input::get('max')[$a];
                    else
                        $qty = Input::get('qtyDescription')[$a];
                    $priceValue = $data->Price;
                    $discValue = $data->DiscountNominal;
                    $subTotal = ($priceValue * $qty ) - (($priceValue * $qty) * $data->Discount / 100) - $discValue * $qty;
                    if ($header->VAT == '1') {
                        $vatValue = $subTotal / 10;
                    } else {
                        $vatValue = 0;
                    }
                    $description->VAT = $vatValue;
                    $description->Qty = $qty;
                    $description->SubTotal = $qty / $data->Qty * $data->SubTotal;
                    $description->Spesifikasi = $data->Spesifikasi;
                    $description->SalesOrderDescriptionInternalID = Input::get('InternalDescription')[$a];
                    $description->UserRecord = Auth::user()->UserID;
                    $description->UserModified = '0';
                    $description->save();
                    $total += $description->SubTotal;
                    for ($c = 0; $c < count(Input::get('InternalDetail' . ($a + 1))); $c++) {
                        if (Input::get('tipe' . ($a + 1))[$c] == "inventory") {
                            $detail = SalesOrderDetail::find(Input::get('InternalDetail' . ($a + 1))[$c]);
                            //jika non paket
                            $qtyValue = $detail->Qty;
                            $qtySalesValue = str_replace(',', '', Input::get('qty' . ($a + 1))[$c]);
                            $sumSales = SalesAddHeader::getSumSales($detail->InventoryInternalID, $salesOrderInternalID, Input::get('InternalDetail' . ($a + 1))[$c]);
                            if ($sumSales == '') {
                                $sumSales = '0';
                            }
                            $sumSalesReturn = SalesReturnHeader::getSumReturnOrder($detail->InventoryInternalID, $salesOrderInternalID, Input::get('InternalDetail' . ($a + 1))[$c]);
                            if ($sumSalesReturn == '') {
                                $sumSalesReturn = '0';
                            }
                            $qtyValue = $qtyValue - $sumSales + $sumSalesReturn;
                            if ($qtyValue < $qtySalesValue) {
                                $qtySalesValue = $qtyValue;
                            }
                            $priceValue = $detail->Price;
                            $discValue = $detail->DiscountNominal;
                            $subTotal = ($priceValue * $qtySalesValue ) - (($priceValue * $qtySalesValue) * $detail->Discount / 100) - $discValue * $qtySalesValue;
                            if ($header->VAT == '1') {
                                $vatValue = $subTotal / 10;
                            } else {
                                $vatValue = 0;
                            }
                            if ($qtySalesValue > 0) {
                                $detail2 = new SalesAddDetail();
                                $detail2->SalesInternalID = $header->InternalID;
                                $detail2->InventoryInternalID = $detail->InventoryInternalID;
                                $detail2->DescriptionInternalID = $description->InternalID;
                                $detail2->UomInternalID = $detail->UomInternalID;
                                $detail2->Qty = $qtySalesValue;
                                $detail2->Price = $priceValue;
                                $detail2->Discount = $detail->Discount;
                                $detail2->DiscountNominal = $discValue;
                                $detail2->VAT = $vatValue;
                                $detail2->SubTotal = $subTotal;
                                $detail2->UserRecord = Auth::user()->UserID;
                                $detail2->UserModified = '0';
                                $detail2->SalesOrderDetailInternalID = Input::get('InternalDetail' . ($a + 1))[$c];
                                $detail2->save();
                                setTampInventory($detail->InventoryInternalID);
                            }
//                            $total += $subTotal;
                        } else {
                            $detail = SalesOrderParcel::find(Input::get("InternalDetail" . ($a + 1))[$c]);
                            //jika paket
                            $parcelDetail = ParcelInventory::where("ParcelInternalID", $detail->ParcelInternalID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->get();
                            $qtyValue = $detail->Qty;
                            $qtySalesValue = str_replace(',', '', Input::get('qty' . ($a + 1))[$c]);
                            $sumSales = SalesAddHeader::getSumSalesParcel($detail->ParcelInternalID, "", Input::get('InternalDetail' . ($a + 1))[$c]);
                            $priceValue = $detail->Price;
                            $discValue = $detail->DiscountNominal;
                            $subTotal = ($priceValue * $qtySalesValue ) - (($priceValue * $qtySalesValue) * $detail->Discount / 100) - $discValue * $qtySalesValue;
                            if ($sumSales == '') {
                                $sumSales = '0';
                            }
                            $sumSalesReturnParcel = SalesReturnHeader::getSumReturnOrderParcel($detail->ParcelInternalID, $header->SalesOrderInternalID, Input::get('InternalDetail' . ($a + 1))[$a]);
                            if ($sumSalesReturnParcel == '') {
                                $sumSalesReturnParcel = '0';
                            }
                            $qtyValue = $qtyValue - $sumSales + $sumSalesReturnParcel;
                            if ($qtyValue < $qtySalesValue) {
                                $qtySalesValue = $qtyValue;
                            }
                            if ($header->VAT == '1') {
                                $vatValue = $subTotal / 10;
                            } else {
                                $vatValue = 0;
                            }
                            if ($qtySalesValue > 0) {
                                $SAParcel = new SalesAddParcel();
                                $SAParcel->SalesInternalID = $header->InternalID;
                                $SAParcel->ParcelInternalID = $detail->ParcelInternalID;
                                $SAParcel->DescriptionInternalID = $description->InternalID;
                                $SAParcel->SalesOrderParcelDetailInternalID = Input::get("InternalDetail" . ($a + 1))[$c];
                                $SAParcel->Qty = $qtySalesValue;
                                $SAParcel->Price = $priceValue;
                                $SAParcel->Discount = $detail->Discount;
                                $SAParcel->VAT = $vatValue;
                                $SAParcel->DiscountNominal = $discValue;
                                $SAParcel->SubTotal = $subTotal;
                                $SAParcel->CompanyInternalID = Auth::user()->Company->InternalID;
                                $SAParcel->UserRecord = Auth::user()->UserID;
                                $SAParcel->UserModified = "0";
                                $SAParcel->Remark = "-";
                                $SAParcel->save();

                                foreach ($parcelDetail as $dataParcel) {
                                    $uom1 = $dataParcel->UomInternalID;
                                    $qtyValue1 = $dataParcel->Qty * $qtySalesValue;
                                    $priceValue1 = $dataParcel->Price;
                                    $subTotal1 = ($priceValue1 * $qtyValue1);
                                    if (Input::get('vat') == '1') {
                                        $vatValue1 = $subTotal1 / 10;
                                    } else {
                                        $vatValue1 = 0;
                                    }
                                    $detail2 = new SalesAddDetail();
                                    $detail2->SalesInternalID = $header->InternalID;
                                    $detail2->InventoryInternalID = $dataParcel->InventoryInternalID;
                                    $detail2->UomInternalID = $uom1;
                                    $detail2->SalesParcelInternalID = $SAParcel->InternalID;
                                    $detail2->DescriptionInternalID = $description->InternalID;
                                    $detail2->Qty = $qtyValue1;
                                    $detail2->Price = $priceValue1;
                                    $detail2->Discount = 0;
                                    $detail2->DiscountNominal = 0;
                                    $detail2->VAT = $vatValue1;
                                    $detail2->SubTotal = $subTotal1;
                                    $detail2->SalesOrderDetailInternalID = $salesOrderInternalID;
                                    $detail2->UserRecord = Auth::user()->UserID;
                                    $detail2->UserModified = '0';
                                    $detail2->save();
                                    setTampInventory($dataParcel->InventoryInternalID);
                                }
                            }
//                            $total += $subTotal;
                        }
                    }
                }
            }

            //insert top up ketika down payment ada
            if ($header->DownPayment > 0) {
                $total1 = $total2 = $total3 = 0;
                //valid
                $id = $header->SalesID;
                //yg ada ppn 
                if ($header->VAT == 1) {
                    $downpayment = $header->DownPayment * 1.1;
                } else {
                    $downpayment = $header->DownPayment;
                }
                //yg ga ada ppn
                $grandTotal = $header->DownPayment;
//            dd($grandTotal);
                $selba = 0;
                $date = explode('-', Input::get('date'));
                $customertopup = new TopUp();
                $customertopup->ACC6InternalID = $header->ACC6InternalID;
                $customertopup->TopUpID = $id;
                $customertopup->TopUpDate = $header->SalesDate;
                $customertopup->SlipInternalID = NULL;
                $customertopup->CurrencyInternalID = $header->CurrencyInternalID;
                $customertopup->CurrencyRate = $header->CurrencyRate;
                //non PPN
                if ($header->VAT == 0) {
                    $customertopup->VAT = '0';
                    $total1 = $grandTotal;
                    $total2 = $grandTotal + $selba;
                    $total3 = 0;
                }
                //PPN
                else {
                    $customertopup->VAT = 1;
                    $total1 = $grandTotal;
                    if ($grandTotal > 0) {
                        $total2 = ceil(10 / 11 * $grandTotal + $selba);
                        $total3 = floor(1 / 11 * round($grandTotal));
                    } else {
                        $total2 = ceil(10 / 11 * $selba);
                        $total3 = floor(1 / 11 * round($selba));
                    }
                }
                $customertopup->PaymentType = 2;
                $customertopup->SalesOrderInternalID = $header->SalesOrderInternalID;
                $customertopup->Tax = $header->TaxNumber;
                $customertopup->Type = "customer";
                $customertopup->SelisihBayar = 0;
                //tanpa pajak
                $customertopup->TopUp = $grandTotal;
                //dengan pajak
                $customertopup->GrandTotalTopUp = $downpayment;
                $customertopup->UserRecord = Auth::user()->UserID;
                $customertopup->UserModified = "0";
                $customertopup->CompanyInternalID = Auth::user()->Company->InternalID;
                $customertopup->Remark = "Automatic dari Invoice";
                $customertopup->save();
            }

            if ($header->VAT == '1') {
                $vatValueHeader = ($total - str_replace(",", "", Input::get("DiscountGlobal")) - str_replace(",", "", Input::get('DownPayment'))) / 10;
            } else {
                $vatValueHeader = 0;
            }
//            $header->GrandTotal = $total - str_replace(",", "", Input::get("DiscountGlobal")) - str_replace(",", "", Input::get('DownPayment')) + $vatValueHeader;
//            $header->save();

            $currency = $salesOrder->CurrencyInternalID;
            $rate = $salesOrder->CurrencyRate;
            $total -= str_replace(",", "", Input::get('DiscountGlobal'));
            $total -= str_replace(",", "", Input::get('DownPayment'));
            if ($header->VAT == '1') {
                if ($header->isCash == 0) {
                    $slip = Input::get('slip');
                    $this->insertJournal($salesNumber, ceil($total), $salesOrder->VAT, $currency, $rate, $date, $slip, str_replace(",", "", Input::get('DiscountGlobal')), str_replace(",", "", Input::get('DownPayment')));
                } else {
                    $slip = '-1';
                    $this->insertJournal($salesNumber, ceil($total), $salesOrder->VAT, $currency, $rate, $date, $slip, str_replace(",", "", Input::get('DiscountGlobal')), str_replace(",", "", Input::get('DownPayment')));
                }
            }

            $messages = 'suksesInsert';
            $error = '';
        }
        $sales = $this->createID(0) . '.';
        if (isset($header) && !is_null($header) && $messages == 'suksesInsert') {
            return Redirect::route('salesDetail', $header->SalesID)->with("msg", "print");
        }
        $header = SalesOrderHeader::find($id);
        $detail = SalesOrderHeader::find($id)->salesOrderDetail()->get();
        $print = Route('salesPrint', $salesNumber);
        return View::make('penjualanAdd.salesAddNew')
                        ->withToogle('transaction')->withAktif('sales')
                        ->withSales($sales)
                        ->withError($error)
                        ->withHeader($header)
                        ->withDetail($detail)
                        ->withPrint($print)
                        ->withMessages($messages);
    }

    public function updateSales($id) {
        //tipe
        $headerUpdate = SalesAddHeader::find($id);
        $detailUpdate = SalesAddHeader::find($id)->salesDetail()->get();

        //rule
        if ($headerUpdate->isCash == 0) {
            $rule = array(
//                'remark' => 'required|max:1000',
                'warehouse' => 'required',
                'slip' => 'required',
                'purchasingCommission' => 'required',
                'InternalDescription' => 'required'
            );
        } else {
            $rule = array(
//                'remark' => 'required|max:1000',
                'warehouse' => 'required',
                'purchasingCommission' => 'required',
                'InternalDescription' => 'required'
            );
            $longTerm = 0;
        }

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalUpdate';
            $error = $validator->messages();
        } else {
            $header = SalesAddHeader::find(Input::get('SalesInternalID'));
            $header->WarehouseInternalID = Input::get('warehouse');
            $header->PurchasingCommission = str_replace(',', '', Input::get('purchasingCommission'));
            $header->DiscountGlobal = str_replace(",", "", Input::get("DiscountGlobal"));
            $header->GrandTotal = Input::get("grandTotalValue");
            $header->DownPayment = str_replace(",", "", Input::get('DownPayment'));
            $header->TransactionType = Input::get('TransactionType');
            if (Input::get('Replacement') == '') {
                $header->Replacement = '0';
            } else {
                $header->Replacement = Input::get('Replacement');
            }
            if (Input::get('TaxNumber') == '') {
                $header->TaxNumber = '.-.';
            } else {
                $header->TaxNumber = Input::get('TaxNumber');
            }
            $header->TaxMonth = Input::get('TaxMonth');
            $header->TaxYear = Input::get('TaxYear');
            $header->UserModified = Auth::user()->UserID;
            $header->Remark = Input::get('remark');
            $header->save();

            if (TopUp::where("TopUpID", $header->SalesID)->count() > 0) {
                //update yg d top up
                $tu = TopUp::where("TopUpID", $header->SalesID)->first();
                $tu->TopUp = $header->DownPayment;
                if ($header->VAT == 1)
                    $tu->GrandTotalTopUp = $header->DownPayment * 1.1;
                $tu->save();
            }
            SalesAddDetail::where('SalesInternalID', '=', Input::get('SalesInternalID'))->update(array('is_deleted' => 1));
            SalesAddParcel::where('SalesInternalID', '=', Input::get('SalesInternalID'))->update(array('is_deleted' => 1));
            SalesAddDescription::where('SalesInternalID', '=', Input::get('SalesInternalID'))->update(array('is_deleted' => 1));

            //insert detail
            $total = 0;

            for ($a = 0; $a < count(Input::get('InternalDescription')); $a++) {
                if (Input::get('qtyDescription')[$a] > 0) {
                    $data = SalesOrderDescription::find(Input::get('InternalDescription')[$a]);
                    $description = new SalesAddDescription();
                    $description->SalesInternalID = $header->InternalID;
                    $description->InventoryText = $data->InventoryText;
                    $description->UomText = $data->UomText;
                    $description->Price = $data->Price;
                    $description->DiscountNominal = $data->DiscountNominal;
                    $description->Discount = $data->Discount;
                    $qty = 0;
                    if (Input::get('max')[$a] < Input::get('qtyDescription')[$a])
                        $qty = Input::get('max')[$a];
                    else
                        $qty = Input::get('qtyDescription')[$a];
                    $description->Qty = $qty;
                    $description->SubTotal = $qty / $data->Qty * $data->SubTotal;
                    if ($header->VAT == '1') {
                        $vatValue = ($qty / $data->Qty * $data->SubTotal) / 10;
                    } else {
                        $vatValue = 0;
                    }
                    $description->VAT = $vatValue;
                    $description->Spesifikasi = $data->Spesifikasi;
                    $description->SalesOrderDescriptionInternalID = Input::get('InternalDescription')[$a];
                    $description->UserRecord = Auth::user()->UserID;
                    $description->UserModified = '0';
                    $description->save();
                    $total += $description->SubTotal;
                    for ($c = 0; $c < count(Input::get('InternalDetail' . ($a + 1))); $c++) {
                        if (Input::get('tipe' . ($a + 1))[$c] == "inventory") {
                            $detail = SalesOrderDetail::find(Input::get('InternalDetail' . ($a + 1))[$c]);
                            //jika non paket
                            $qtyValue = $detail->Qty;
                            $qtySalesValue = str_replace(',', '', Input::get('qty' . ($a + 1))[$c]);
                            $sumSales = SalesAddHeader::getSumSalesExcept($detail->InventoryInternalID, $header->InternalID, Input::get('InternalDetail' . ($a + 1))[$c]);
                            if ($sumSales == '') {
                                $sumSales = '0';
                            }
                            $sumSalesReturn = SalesReturnHeader::getSumReturnOrder($detail->InventoryInternalID, $header->SalesOrderInternalID, Input::get('InternalDetail' . ($a + 1))[$c]);
                            if ($sumSalesReturn == '') {
                                $sumSalesReturn = '0';
                            }
                            $qtyValue = $qtyValue - $sumSales + $sumSalesReturn;
                            if ($qtyValue < $qtySalesValue) {
                                $qtySalesValue = $qtyValue;
                            }
                            $priceValue = $detail->Price;
                            $discValue = $detail->DiscountNominal;
                            $subTotal = ($priceValue * $qtySalesValue ) - (($priceValue * $qtySalesValue) * $detail->Discount / 100) - $discValue * $qtySalesValue;
                            if ($header->VAT == '1') {
                                $vatValue = $subTotal / 10;
                            } else {
                                $vatValue = 0;
                            }
                            if ($qtySalesValue > 0) {
                                $detail2 = new SalesAddDetail();
                                $detail2->SalesInternalID = $header->InternalID;
                                $detail2->InventoryInternalID = $detail->InventoryInternalID;
                                $detail2->DescriptionInternalID = $description->InternalID;
                                $detail2->UomInternalID = $detail->UomInternalID;
                                $detail2->Qty = $qtySalesValue;
                                $detail2->Price = $priceValue;
                                $detail2->Discount = $detail->Discount;
                                $detail2->DiscountNominal = $discValue;
                                $detail2->VAT = $vatValue;
                                $detail2->SubTotal = $subTotal;
                                $detail2->UserRecord = Auth::user()->UserID;
                                $detail2->UserModified = '0';
                                $detail2->SalesOrderDetailInternalID = Input::get('InternalDetail' . ($a + 1))[$c];
                                $detail2->save();
                                setTampInventory($detail->InventoryInternalID);
                            }
//                            $total += $subTotal;
                        } else {
                            $detail = SalesOrderParcel::find(Input::get("InternalDetail" . ($a + 1))[$c]);
                            //jika paket
                            $parcelDetail = ParcelInventory::where("ParcelInternalID", $detail->ParcelInternalID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->get();
                            $qtyValue = $detail->Qty;
                            $qtySalesValue = str_replace(',', '', Input::get('qty' . ($a + 1))[$c]);
                            $sumSales = SalesAddHeader::getSumSalesExceptParcel($detail->ParcelInternalID, $header->InternalID, Input::get('InternalDetail' . ($a + 1))[$c]);
                            $priceValue = $detail->Price;
                            $discValue = $detail->DiscountNominal;
                            $subTotal = ($priceValue * $qtySalesValue ) - (($priceValue * $qtySalesValue) * $detail->Discount / 100) - $discValue * $qtySalesValue;
                            if ($sumSales == '') {
                                $sumSales = '0';
                            }
                            $sumSalesReturnParcel = SalesReturnHeader::getSumReturnOrderParcel($detail->ParcelInternalID, $header->SalesOrderInternalID, Input::get('InternalDetail' . ($a + 1))[$a]);
                            if ($sumSalesReturnParcel == '') {
                                $sumSalesReturnParcel = '0';
                            }
                            $qtyValue = $qtyValue - $sumSales + $sumSalesReturnParcel;
                            if ($qtyValue < $qtySalesValue) {
                                $qtySalesValue = $qtyValue;
                            }
                            if ($header->VAT == '1') {
                                $vatValue = $subTotal / 10;
                            } else {
                                $vatValue = 0;
                            }
                            if ($qtySalesValue > 0) {
                                $SAParcel = new SalesAddParcel();
                                $SAParcel->SalesInternalID = $header->InternalID;
                                $SAParcel->ParcelInternalID = $detail->ParcelInternalID;
                                $SAParcel->DescriptionInternalID = $description->InternalID;
                                $SAParcel->SalesOrderParcelDetailInternalID = Input::get("InternalDetail" . ($a + 1))[$c];
                                $SAParcel->Qty = $qtySalesValue;
                                $SAParcel->Price = $priceValue;
                                $SAParcel->Discount = $detail->Discount;
                                $SAParcel->VAT = $vatValue;
                                $SAParcel->DiscountNominal = $discValue;
                                $SAParcel->SubTotal = $subTotal;
                                $SAParcel->CompanyInternalID = Auth::user()->Company->InternalID;
                                $SAParcel->UserRecord = Auth::user()->UserID;
                                $SAParcel->UserModified = "0";
                                $SAParcel->Remark = "-";
                                $SAParcel->save();

                                foreach ($parcelDetail as $dataParcel) {
                                    $uom1 = $dataParcel->UomInternalID;
                                    $qtyValue1 = $dataParcel->Qty * $qtySalesValue;
                                    $priceValue1 = $dataParcel->Price;
                                    $subTotal1 = ($priceValue1 * $qtyValue1);
                                    if (Input::get('vat') == '1') {
                                        $vatValue1 = $subTotal1 / 10;
                                    } else {
                                        $vatValue1 = 0;
                                    }
                                    $detail2 = new SalesAddDetail();
                                    $detail2->SalesInternalID = $header->InternalID;
                                    $detail2->InventoryInternalID = $dataParcel->InventoryInternalID;
                                    $detail2->UomInternalID = $uom1;
                                    $detail2->DescriptionInternalID = $description->InternalID;
                                    $detail2->SalesParcelInternalID = $SAParcel->InternalID;
                                    $detail2->Qty = $qtyValue1;
                                    $detail2->Price = $priceValue1;
                                    $detail2->Discount = 0;
                                    $detail2->DiscountNominal = 0;
                                    $detail2->VAT = $vatValue1;
                                    $detail2->SubTotal = $subTotal1;
                                    $detail2->SalesOrderDetailInternalID = $header->SalesOrderInternalID;
                                    $detail2->UserRecord = Auth::user()->UserID;
                                    $detail2->UserModified = '0';
                                    $detail2->save();
                                    setTampInventory($dataParcel->InventoryInternalID);
                                }
                            }
//                            $total += $subTotal;
                        }
                    }
                }
            }
            if ($header->VAT == '1') {
                $vatValueHeader = ($total - str_replace(",", "", Input::get("DiscountGlobal")) - str_replace(",", "", Input::get('DownPayment'))) / 10;
            } else {
                $vatValueHeader = 0;
            }
//            $header->GrandTotal = $total - str_replace(",", "", Input::get("DiscountGlobal")) - str_replace(",", "", Input::get('DownPayment')) + $vatValueHeader;
//            $header->save();

            $journal = JournalHeader::where('TransactionID', '=', $headerUpdate->SalesID)->get();
            foreach ($journal as $value) {
                JournalDetail::where('JournalInternalID', '=', $value->InternalID)->delete();
                JournalHeader::where('InternalID', '=', $value->InternalID)->delete();
            }
            SalesAddDetail::where('SalesInternalID', '=', Input::get('SalesInternalID'))->where('is_deleted', 1)->delete();
            SalesAddParcel::where('SalesInternalID', '=', Input::get('SalesInternalID'))->where('is_deleted', 1)->delete();
            SalesAddDescription::where('SalesInternalID', '=', Input::get('SalesInternalID'))->where('is_deleted', 1)->delete();

            $currency = $headerUpdate->CurrencyInternalID;
            $rate = $headerUpdate->CurrencyRate;
            $date = date("d-m-Y", strtotime($headerUpdate->SalesDate));
            $date = explode('-', $date);
            $total -= str_replace(",", "", Input::get('DiscountGlobal'));
            $total -= str_replace(",", "", Input::get('DownPayment'));
            if ($header->VAT == '1') {
                if ($headerUpdate->isCash == 0) {
                    $slip = Input::get('slip');
                    $this->insertJournal($headerUpdate->SalesID, ceil($total), $headerUpdate->VAT, $currency, $rate, $date, $slip, str_replace(",", "", Input::get('DiscountGlobal')), str_replace(",", "", Input::get('DownPayment')));
                } else {
                    $slip = '-1';
                    $this->insertJournal($headerUpdate->SalesID, ceil($total), $headerUpdate->VAT, $currency, $rate, $date, $slip, str_replace(",", "", Input::get('DiscountGlobal')), str_replace(",", "", Input::get('DownPayment')));
                }
            }
            $messages = 'suksesUpdate';
            $error = '';
        }

        if (isset($header) && !is_null($header) && $messages == 'suksesUpdate') {
            return Redirect::route('salesDetail', $header->SalesID)->with("msg", "print");
        }
        
        //tipe
        $header = SalesAddHeader::find($id);
        $detail = SalesAddHeader::find($id)->salesDetail()->get();
        $idSalesOrder = $header->SalesOrderInternalID;
        $headerSalesOrder = SalesOrderHeader::find($idSalesOrder);
        $downpayment = $detailSalesOrder = SalesOrderHeader::find($idSalesOrder)->salesOrderDetail()->get();

        if ($header->isCash == 4) {
            $downpayment = TopUp::where('PaymentType', 2)
                    ->where('SalesOrderInternalID', $header->SalesOrderInternalID)
                    ->where('Type', "customer")
                    ->where("Remark", "!=", "Automatic dari Invoice")
                    ->sum('TopUp');
            $topup = SalesAddHeader::where('isCash', 4)
                    ->where('SalesOrderInternalID', $header->SalesOrderInternalID)
                    ->where('InternalID', "!=", $header->InternalID)
                    ->sum('DownPayment');
            $totalDownpayment = $downpayment - $topup;
        } else {
            $totalDownpayment = 0;
        }

        return View::make('penjualanAdd.salesAddUpdate')
                        ->withToogle('transaction')->withAktif('sales')
                        ->withHeader($header)
                        ->withDetail($detail)
                        ->withHeaderorder($headerSalesOrder)
                        ->withDetailorder($detailSalesOrder)
                        ->withDownpayment($totalDownpayment)
                        ->withError($error)
                        ->withMessages($messages);
    }

    public function deleteSales() {
        $salesHeader = SalesAddHeader::find(Input::get('InternalID'));
        $sales = DB::select(DB::raw('SELECT * FROM t_salesreturn_header WHERE CompanyInternalID = "' . Auth::user()->CompanyInternalID . '" AND SalesReturnID LIKE "%' . $salesHeader->SalesID . '"'));
        if (is_null($sales) == '') {
            //tidak ada yang menggunakan data sales maka data boleh dihapus
            //hapus journal
            if ($salesHeader->CompanyInternalID == Auth::user()->Company->InternalID) {
                $journal = JournalHeader::where('TransactionID', '=', $salesHeader->SalesID)->get();
                foreach ($journal as $value) {
                    JournalDetail::where('JournalInternalID', '=', $value->InternalID)->delete();
                    JournalHeader::where('InternalID', '=', $value->InternalID)->delete();
                }
                //hapus detil
                $detilData = SalesAddHeader::find(Input::get('InternalID'))->salesDetail;
                foreach ($detilData as $value) {
                    $detil = SalesAddDetail::find($value->InternalID);
                    $detil->delete();
                    setTampInventory($detil->InventoryInternalID);
                }
                $descriptionData = SalesAddHeader::find(Input::get('InternalID'))->salesDescription;
                foreach ($descriptionData as $value) {
                    $description = salesAddDescription::find($value->InternalID);
                    $description->delete();
                }
                //hapus sales parcel
                SalesAddParcel::where("SalesInternalID", Input::get('InternalID'))->delete();
                //hapus sales
                $sales = SalesAddHeader::find(Input::get('InternalID'));
                $sales->delete();
                $messages = 'suksesDelete';
            } else {
                $messages = 'accessDenied';
            }
        } else {
            //ada maka data tidak boleh dihapus
            $messages = 'gagalDelete';
        }
        $data = SalesAddHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
        return View::make('penjualanAdd.salesAddSearch')
                        ->withToogle('transaction')->withAktif('sales')
                        ->withMessages($messages)
                        ->withData($data);
    }

    function insertJournal($salesNumber, $total, $vat, $currency, $rate, $date, $slip, $discountGlobal, $dp) {
        $header = new JournalHeader;
        $yearDigit = substr($date[2], 2);
        $dateText = $date[1] . $yearDigit;
        if ($vat == 1) {
            $defaultPenjualan = Default_s::find(1)->DefaultID;
            $defaultPiutang = Default_s::find(2)->DefaultID;
            $defaultIncome = Default_s::find(3)->DefaultID;
            $defaultDP = Default_s::find(11)->DefaultID;
            $defaultDiscount = Default_s::find(134)->DefaultID;
        } else {
            $defaultPenjualan = Default_s::find(79)->DefaultID;
            $defaultPiutang = Default_s::find(80)->DefaultID;
            $defaultIncome = Default_s::find(81)->DefaultID;
            $defaultDP = Default_s::find(88)->DefaultID;
            $defaultDiscount = Default_s::find(134)->DefaultID;
        }
        if ($slip == '-1') {
            $cari = 'ME-' . $dateText;
            $header->JournalType = 'Memorial';
            $akun = array($defaultPenjualan, $defaultPiutang, $defaultIncome, $defaultDiscount, $defaultDP);
            $header->JournalID = JournalHeader::getNextIDJournal($cari . '-');
            $header->SlipInternalID = Null;
        } else {
            $akun = array($defaultPenjualan, "Slip", $defaultIncome, $defaultDiscount, $defaultDP);
            $tampSlipID = Slip::find($slip);
            if ($tampSlipID->Flag == '0') {
                $cari = 'CI-' . $dateText;
                $header->JournalType = 'Cash In';
            } else if ($tampSlipID->Flag == '1') {
                $cari = 'BI-' . $dateText;
                $header->JournalType = 'Bank In';
            }
            $header->JournalID = JournalHeader::getNextIDJournal($cari . '-' . $tampSlipID->SlipID . '-');
            $header->SlipInternalID = $slip;
        }
        $header->JournalDate = $date[2] . '-' . $date[1] . '-' . $date[0];
        $header->JournalFrom = Auth::user()->UserID;
        $header->Notes = '';
        $department = Department::where('Default', '1')->first();
        $header->DepartmentInternalID = $department->InternalID;
        $header->TransactionID = $salesNumber;
        $header->ACC5InternalID = '0';
        $header->Lock = '0';
        $header->Check = '0';
        $header->Flag = '0';
        $header->UserRecord = Auth::user()->UserID;
        $header->CompanyInternalID = Auth::user()->Company->InternalID;
        $header->UserModified = '0';
        $header->Remark = '';
        $header->save();

        //insert detail
        if ($vat == 1) {
            $vatValue = floor(10 * $total / 100);
        } else {
            $vatValue = 0;
        }
        $count = 1;
        foreach ($akun as $data) {
            $kreditValue = 0;
            $debetValue = 0;
            if (($data != $defaultIncome || $vatValue != 0) && ($data != $defaultDiscount || $discountGlobal != 0) && ($data != $defaultDP || $dp != 0)) {
                $detail = new JournalDetail();
                $detail->JournalInternalID = $header->InternalID;
                $detail->JournalIndex = $count;
                $detail->JournalNotes = $data;
                if ($data == $defaultPenjualan) {
                    $kreditValue = $total + $dp + $discountGlobal;
                    $detail->JournalDebetMU = 0;
                    $detail->JournalCreditMU = $kreditValue;
                } else if ($data == $defaultPiutang || $data == 'Slip') {
                    $debetValue = $total + $vatValue;
                    $detail->JournalDebetMU = $debetValue;
                    $detail->JournalCreditMU = 0;
                } else if ($data == $defaultDP) {
                    $debetValue = $dp;
                    $detail->JournalDebetMU = $debetValue;
                    $detail->JournalCreditMU = 0;
                } else if ($data == $defaultDiscount) {
                    $debetValue = $discountGlobal;
                    $detail->JournalDebetMU = $debetValue;
                    $detail->JournalCreditMU = 0;
                } else {
                    $kreditValue = $vatValue;
                    $detail->JournalDebetMU = 0;
                    $detail->JournalCreditMU = $kreditValue;
                }
                $detail->CurrencyInternalID = $currency;
                $detail->CurrencyRate = $rate;
                $detail->JournalDebet = $debetValue * $rate;
                $detail->JournalCredit = $kreditValue * $rate;
//                if ($data == "Receivable" || $data == "Slip")
//                    $detail->JournalTransactionID = $salesNumber;
                if ($data != 'Slip') {
                    $default = Default_s::getInternalCoa($data);
                } else {
                    $default = Slip::getInternalCoa($slip);
                }
                $detail->ACC1InternalID = $default->ACC1InternalID;
                $detail->ACC2InternalID = $default->ACC2InternalID;
                $detail->ACC3InternalID = $default->ACC3InternalID;
                $detail->ACC4InternalID = $default->ACC4InternalID;
                $detail->ACC5InternalID = $default->ACC5InternalID;
                $detail->ACC6InternalID = $default->ACC6InternalID;
                $coa = Coa::getInternalID($default->ACC1InternalID, $default->ACC2InternalID, $default->ACC3InternalID, $default->ACC4InternalID, $default->ACC5InternalID, $default->ACC6InternalID);
                $detail->COAName = Coa::find($coa)->COAName;
                $detail->UserRecord = Auth::user()->UserID;
                $detail->UserModified = '0';
                $detail->save();
                $count++;
            }
        }
    }

    function salesPrintInclude($id) {
        $id = SalesAddHeader::getIdsales($id);
        $header = SalesAddHeader::find($id);
        $header->Print = $header->Print + 1;
        $header->save();
        $detail = SalesAddHeader::find($id)->salesDetail()->get();
        $headerOrder = SalesOrderHeader::find($header->SalesOrderInternalID);
        $description = SalesAddDescription::where('SalesInternalID', $id)->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $coa6 = SalesAddHeader::find($header->InternalID)->coa6;
            $namacustomer = $coa6->ACC6Name;
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-')
                $cp = 'CP: ' . $coa6->ContactPerson . '<br>';
            else
                $cp = '';
            $customer = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . '<br>' . $cp . $coa6->City . '<br>' . $cp . $coa6->Phone . '<br>' . $coa6->Fax . '<br>' . $coa6->Email;
            if ($header->isCash == 0) {
                $payment = 'Cash';
            } else if ($header->isCash == 1) {
                $payment = 'Credit';
            } else if ($header->isCash == 2) {
                $payment = 'CBD';
            } else if ($header->isCash == 3) {
                $payment = 'Deposit';
            } else {
                $payment = 'Down Payment';
            }
            $currency = Currency::find($header->CurrencyInternalID);
            $currencyName = $currency->CurrencyName;
            if ($header->VAT == 0) {
                $vat = 'Non Tax';
            } else {
                $vat = 'Tax';
            }
            $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 6px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 195px;border-bottom:1px solid #ddd;margin-bottom:20px;">
                        <div style="height: 133px">
                        <img height="100%" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                        </div>
                        <div style="width: 320px; float: left;">
                            <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->Company->CompanyName . '</b>
                        </div> 
                            <div style=" box-sizing: border-box;margin-top:0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Address</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Sales</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;padding-left:6px;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span><br>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                            <table>
                            <tr>
                            <td width="275px">
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Sales ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->SalesID . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->SalesDate)) . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; vertical-align: text-top;">Customer</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; vertical-align: text-top;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $customer . '</td>
                                 </tr>';
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-') {
                $html .= '     <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Contact Person</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->ContactPerson . '</td>
                                 </tr>';
            }

            $html .= '
                                </table>
                                </td>
                                <td style="vertical-align: text-top;">
                                <table>';
            $html .= ' <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Payment</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $payment . '</td>
                                 </tr>';
            if ($header->isCash != 0) {
                $html .= '<tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Due Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime("+" . $header->LongTerm . " day", strtotime($header->SalesDate))) . '</td>
                                 </tr>';
            }
            $html .= '<tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Warehouse</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Warehouse->WarehouseName . '</td>
                                     </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Currency</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $currencyName . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Rate</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . number_format($header->CurrencyRate, '2', '.', ',') . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Vat</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $vat . '</td>
                                     </tr>
                                </table></td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">No.</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="25%">Inventory</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Uom</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Qty</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Price</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc (%)</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            $total = 0;
            $counter = 1;
            $price = 0;
            $discount = 0;
            $subtotal = 0;
            if (count($description) > 0) {
                foreach ($description as $data) {
                    if ($header->VAT == 1) {
                        $price = $data->Price + ($data->Price * 0.1);
                        $discount = $data->DiscountNominal + ($data->DiscountNominal * 0.1);
                    } else {
                        $price = $data->Price;
                        $discount = $data->DiscountNominal;
                    }
                    $subtotal = ($price - $discount) * $data->Qty;
                    $subtotal = $subtotal - (($data->Discount / 100) * $subtotal);
                    $html .= '<tr>
                                <td rowspan="' . (1 + ($data->Spesifikasi != '' ? 1 : 0)) . '" style="vertical-align: top;font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $counter . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $data->InventoryText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->UomText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($discount, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($subtotal, '2', '.', ',') . '</td>
                            </tr>';
                    if ($data->Spesifikasi != '') {
                        $html .= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . nl2br($data->Spesifikasi) . '</td>
                            </tr>';
                    }
                    $counter++;
                    $total += $subtotal;
                }
            } else {
                $html .= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this sales.</td>
                        </tr>';
            }
            $html .= '</tbody>
                            </table>
                              <table>
                            <tr><td>
                            <div style="box-sizing: border-box;min-width: 270px; display: inline-block; clear: both;">   
                            <table>
                             <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 500;vertical-align: top">Remark</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 500;vertical-align: top">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 500;vertical-align: top;text-align: left;">' . $header->Remark . '</td>
                                 </tr>
                                <tr><td colspan="3" style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 500;vertical-align: top">Pembayaran Via Transfer</td></tr>
                                 <tr><td colspan="3" style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 500;vertical-align: top">Bank BCA  a/n. CV. DUTA PERKASA</td></tr>
                                 <tr><td colspan="3" style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 500;vertical-align: top">A/C. 468-3824138 (IDR)</td></tr>
                                </table></div></td><td>
                            <div style="box-sizing: border-box;min-width: 200px; margin-left: 150px; display: inline-block; clear: both;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Discount</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Grand Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total - $header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>

                                </table>
                            </div></td></tr>
                            <tr><td><br></td></tr>                            
<tr>
                                <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Hormat Kami, </td>
                                
                            </tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr>
                            <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">(  ' . Auth::user()->UserName . '  )</td>
                           
                            </tr>
                            </table>
                    </div>
                </body>
            </html>';
            //return PDF ::load($html, 'A5', 'portrait')->show();

            return View::make('template.print.salesIncludePrint')
                            ->with('header', $header)
                            ->with('customer', $customer)
                            ->with('namacustomer', $namacustomer)
                            ->with('vat', $vat)
                            ->with('id', $id)
                            ->with('currencyName', $currencyName)
                            ->with('description', $description)
                            ->with('payment', $payment)
                            ->with('detail', $detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showSales');
        }
    }

    function salesPrint($id) {
        $id = SalesAddHeader::getIdsales($id);
        $header = SalesAddHeader::find($id);
        $header->Print = $header->Print + 1;
        $header->save();
        $detail = SalesAddHeader::find($id)->salesDetail()->get();
        $headerOrder = SalesOrderHeader::find($header->SalesOrderInternalID);
        $description = SalesAddDescription::where('SalesInternalID', $id)->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $coa6 = SalesAddHeader::find($header->InternalID)->coa6;
            $namacustomer = $coa6->ACC6Name;
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-')
                $cp = 'CP: ' . $coa6->ContactPerson . '<br>';
            else
                $cp = '';
            $customer = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . '<br>' . $cp . $coa6->City . '<br>' . $cp . $coa6->Phone . '<br>' . $coa6->Fax . '<br>' . $coa6->Email;
            if ($header->isCash == 0) {
                $payment = 'Cash';
            } else if ($header->isCash == 1) {
                $payment = 'Credit';
            } else if ($header->isCash == 2) {
                $payment = 'CBD';
            } else if ($header->isCash == 3) {
                $payment = 'Deposit';
            } else {
                $payment = 'Down Payment';
            }
            $currency = Currency::find($header->CurrencyInternalID);
            $currencyName = $currency->CurrencyName;
            if ($header->VAT == 0) {
                $vat = 'Non Tax';
            } else {
                $vat = 'Tax';
            }
            $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 6px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 195px;border-bottom:1px solid #ddd;margin-bottom:20px;">
                        <div style="height: 133px">
                        <img height="100%" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                        </div>
                        <div style="width: 320px; float: left;">
                            <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->Company->CompanyName . '</b>
                        </div> 
                            <div style=" box-sizing: border-box;margin-top:0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Address</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Sales</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;padding-left:6px;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span><br>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                            <table>
                            <tr>
                            <td width="275px">
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Sales ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->SalesID . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->SalesDate)) . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; vertical-align: text-top;">Customer</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; vertical-align: text-top;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $customer . '</td>
                                 </tr>';
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-') {
                $html .= '     <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Contact Person</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->ContactPerson . '</td>
                                 </tr>';
            }

            $html .= '
                                </table>
                                </td>
                                <td style="vertical-align: text-top;">
                                <table>';
            $html .= ' <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Payment</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $payment . '</td>
                                 </tr>';
            if ($header->isCash != 0) {
                $html .= '<tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Due Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime("+" . $header->LongTerm . " day", strtotime($header->SalesDate))) . '</td>
                                 </tr>';
            }
            $html .= '<tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Warehouse</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Warehouse->WarehouseName . '</td>
                                     </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Currency</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $currencyName . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Rate</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . number_format($header->CurrencyRate, '2', '.', ',') . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Vat</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $vat . '</td>
                                     </tr>
                                </table></td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">No.</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="25%">Inventory</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Uom</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Qty</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Price</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc (%)</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            $total = 0;
            $totalVAT = 0;
            $counter = 1;
            if (count($description) > 0) {
                foreach ($description as $data) {
                    $html .= '<tr>
                                <td rowspan="' . (1 + ($data->Spesifikasi != '' ? 1 : 0)) . '" style="vertical-align: top;font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $counter . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $data->InventoryText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->UomText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
                    if ($data->Spesifikasi != '') {
                        $html .= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . nl2br($data->Spesifikasi) . '</td>
                            </tr>';
                    }
                    $counter++;
                    $totalVAT += $data->VAT;
                    $total += $data->SubTotal;
                }

                if ($totalVAT != 0) {
                    $totalVAT = $totalVAT - ($header->DiscountGlobal * 0.1);
                }
            } else {
                $html .= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this sales.</td>
                        </tr>';
            }
            $html .= '</tbody>
                            </table>
                              <table>
                            <tr><td>
                            <div style="box-sizing: border-box;min-width: 270px; display: inline-block; clear: both;">   
                            <table>
                             <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 500;vertical-align: top">Remark</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 500;vertical-align: top">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 500;vertical-align: top;text-align: left;">' . $header->Remark . '</td>
                                 </tr>
                                <tr><td colspan="3" style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 500;vertical-align: top">Pembayaran Via Transfer</td></tr>
                                 <tr><td colspan="3" style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 500;vertical-align: top">Bank BCA  a/n. CV. DUTA PERKASA</td></tr>
                                 <tr><td colspan="3" style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 500;vertical-align: top">A/C. 468-3824138 (IDR)</td></tr>
                                </table></div></td><td>
                            <div style="box-sizing: border-box;min-width: 200px; margin-left: 150px; display: inline-block; clear: both;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Discount</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Grand Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total - $header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Tax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($totalVAT, '2', '.', ',') . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; ">Grand Total (Tax)</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">'
                    . number_format($total - $header->DiscountGlobal + $totalVAT, '2', '.', ',') . '</td>
                                 </tr>
                                </table>
                            </div></td></tr>
                            <tr><td><br></td></tr>                            
<tr>
                                <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Hormat Kami, </td>
                                
                            </tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr>
                            <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">(  ' . Auth::user()->UserName . '  )</td>
                           
                            </tr>
                            </table>
                    </div>
                </body>
            </html>';
//            return PDF ::load($html, 'A5', 'portrait')->show();

            return View::make('template.print.salesPrint')
                            ->with('header', $header)
                            ->with('customer', $customer)
                            ->with('namacustomer', $namacustomer)
                            ->with('vat', $vat)
                            ->with('id', $id)
                            ->with('description', $description)
                            ->with('currencyName', $currencyName)
                            ->with('payment', $payment)
                            ->with('detail', $detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showSales');
        }
    }

    function salesInternalPrint($id) {
        $id = SalesAddHeader::getIdsales($id);
        $header = SalesAddHeader::find($id);
        $detail = SalesAddHeader::find($id)->salesDetail()->get();
        $headerOrder = SalesOrderHeader::find($header->SalesOrderInternalID);
        $description = SalesAddDescription::where('SalesInternalID', $id)->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $coa6 = SalesAddHeader::find($header->InternalID)->coa6;
            $namacustomer = $coa6->ACC6Name;
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-')
                $cp = 'CP: ' . $coa6->ContactPerson . '<br>';
            else
                $cp = '';
            $customer = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . '<br>' . $cp . $coa6->City . '<br>' . $cp . $coa6->Phone . '<br>' . $coa6->Fax . '<br>' . $coa6->Email;
            if ($header->isCash == 0) {
                $payment = 'Cash';
            } else if ($header->isCash == 1) {
                $payment = 'Credit';
            } else if ($header->isCash == 2) {
                $payment = 'CBD';
            } else if ($header->isCash == 3) {
                $payment = 'Deposit';
            } else {
                $payment = 'Down Payment';
            }
            $currency = Currency::find($header->CurrencyInternalID);
            $currencyName = $currency->CurrencyName;
            if ($header->VAT == 0) {
                $vat = 'Non Tax';
            } else {
                $vat = 'Tax';
            }
            $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 6px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 70px;border-bottom:1px solid #ddd;margin-bottom:20px;">
                        <div style="width: 320px; float: left;">
                            <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->Company->CompanyName . '</b>
                        </div> 
                            <div style=" box-sizing: border-box;margin-top:0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Address</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Sales</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;padding-left:6px;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span><br>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                            <table>
                            <tr>
                            <td width="275px">
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Sales ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->SalesID . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->SalesDate)) . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; vertical-align: text-top;">Customer</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; vertical-align: text-top;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $customer . '</td>
                                 </tr>';
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-') {
                $html .= '     <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Contact Person</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->ContactPerson . '</td>
                                 </tr>';
            }

            $html .= '
                                </table>
                                </td>
                                <td style="vertical-align: text-top;">
                                <table>';
            $html .= ' <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Payment</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $payment . '</td>
                                 </tr>';
            if ($header->isCash != 0) {
                $html .= '<tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Due Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime("+" . $header->LongTerm . " day", strtotime($header->SalesDate))) . '</td>
                                 </tr>';
            }
            $html .= '<tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Warehouse</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Warehouse->WarehouseName . '</td>
                                     </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Currency</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $currencyName . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Rate</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . number_format($header->CurrencyRate, '2', '.', ',') . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Vat</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $vat . '</td>
                                     </tr>
                                </table></td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">No.</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="25%">Inventory</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Uom</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Qty</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Price</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc (%)</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            $total = 0;
            $totalVAT = 0;
            $counter = 1;
            if (count($description) > 0) {
                foreach ($description as $data) {
                    $detail = SalesAddDetail::where('DescriptionInternalID', $data->InternalID)->where('SalesParcelInternalID', 0)->get();
                    $parcel = SalesAddParcel::where('DescriptionInternalID', $data->InternalID)->get();
                    $html .= '<tr>
                                <td rowspan="' . (1 + ($data->Spesifikasi != '' ? 1 : 0) + count($detail) + count($parcel)) . '" style="vertical-align: top;font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $counter . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $data->InventoryText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->UomText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
                    if ($data->Spesifikasi != '') {
                        $html .= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . nl2br($data->Spesifikasi) . '</td>
                            </tr>';
                    }
                    foreach ($detail as $data2) {
                        $html .= '<tr>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;"><i>' . $data2->inventory->InventoryName . '</i></td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data2->uom->UomID . '</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data2->Qty, 0, '.', ',') . '</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data2->Price, '2', '.', ',') . '</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data2->Discount . '' . '</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data2->DiscountNominal, '2', '.', ',') . '</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data2->SubTotal, '2', '.', ',') . '</td>
                                </tr>';
                    }
                    foreach ($parcel as $data2) {
                        $html .= '<tr>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;"><i>' . $data2->parcel->ParcelName . '</i></td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">-</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data2->Qty, 0, '.', ',') . '</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data2->Price, '2', '.', ',') . '</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data2->Discount . '' . '</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data2->DiscountNominal, '2', '.', ',') . '</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data2->SubTotal, '2', '.', ',') . '</td>
                                </tr>';
                    }
                    $totalVAT += $data->VAT;
                    $total += $data->SubTotal;
                    $counter++;
                }
                if ($totalVAT != 0) {
                    $totalVAT = $totalVAT - ($header->DiscountGlobal * 0.1);
                }
            } else {
                $html .= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this sales.</td>
                        </tr>';
            }
            $html .= '</tbody>
                            </table>
                              <table>
                            <tr><td>
                            <div style="box-sizing: border-box;min-width: 270px; display: inline-block; clear: both;">   
                            <table>
                             <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 500;vertical-align: top">Remark</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 500;vertical-align: top">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 500;vertical-align: top;text-align: left;">' . $header->Remark . '</td>
                                 </tr>
                                <tr><td colspan="3" style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 500;vertical-align: top">Pembayaran Via Transfer</td></tr>
                                 <tr><td colspan="3" style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 500;vertical-align: top">Bank BCA  a/n. CV. DUTA PERKASA</td></tr>
                                 <tr><td colspan="3" style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 500;vertical-align: top">A/C. 468-3824138 (IDR)</td></tr>
                                </table></div></td><td>
                            <div style="box-sizing: border-box;min-width: 200px; margin-left: 140px; display: inline-block; clear: both;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Discount</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Grand Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total - $header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Tax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($totalVAT, '2', '.', ',') . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; ">Grand Total (Tax)</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">'
                    . number_format($header->GrandTotal, '2', '.', ',') . '</td>
                                 </tr>
                                </table>
                            </div></td></tr>
                            <tr><td><br></td></tr>                            
<tr>
                                <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Hormat Kami, </td>
                                
                            </tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr>
                            <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">(  ' . Auth::user()->UserName . '  )</td>
                           
                            </tr>
                            </table>
                    </div>
                </body>
            </html>';
//            return PDF ::load($html, 'A5', 'portrait')->show();
            return View::make('template.print.salesInternalPrint')
                            ->with('header', $header)
                            ->with('customer', $customer)
                            ->with('namacustomer', $namacustomer)
                            ->with('vat', $vat)
                            ->with('id', $id)
                            ->with('description', $description)
                            ->with('currencyName', $currencyName)
                            ->with('payment', $payment)
                            ->with('detail', $detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showSales');
        }
    }

    function salesPrintSJ($id) {
        $id = SalesAddHeader::getIdsales($id);
        $header = SalesAddHeader::find($id);
        $detail = SalesAddHeader::find($id)->salesDetail()->get();
        $headerOrder = SalesOrderHeader::find($header->SalesOrderInternalID);
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $coa6 = SalesAddHeader::find($header->InternalID)->coa6;
            $customer = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . '<br>' . $coa6->Address . '<br>Phone: ' . $coa6->Phone . '<br> Fax: ' . $coa6->Fax;
            if ($header->isCash == 0) {
                $payment = 'Cash';
            } else if ($header->isCash == 1) {
                $payment = 'Credit';
            } else if ($header->isCash == 2) {
                $payment = 'CBD';
            } else if ($header->isCash == 3) {
                $payment = 'Deposit';
            } else {
                $payment = 'Down Payment';
            }
            $currency = Currency::find($header->CurrencyInternalID);
            $currencyName = $currency->CurrencyName;
            if ($header->VAT == 0) {
                $vat = 'Non Tax';
            } else {
                $vat = 'Tax';
            }
            $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 6px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 195px;border-bottom:1px solid #ddd;margin-bottom:20px;">
                        <div style="height: 133px">
                        <img height="100%" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                        </div>
                        <div style="width: 420px; float: left;">
                            <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->Company->CompanyName . '</b>
                        </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Address</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Sales</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;padding-left:6px;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span><br>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                            <table>
                            <tr>
                            <td width="275px">
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Sales ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->SalesID . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->SalesDate)) . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; vertical-align: text-top;">Customer</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; vertical-align: text-top;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $customer . '</td>
                                 </tr>';
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-') {
                $html .= '     <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Contact Person</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->ContactPerson . '</td>
                                 </tr>';
            }
            $html .= ' </table>
                                </td>
                                <td style="vertical-align: text-top;">
                                <table>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Warehouse</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Warehouse->WarehouseName . '</td>
                                     </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Sales Order</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $headerOrder->SalesOrderID . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Remark</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Remark . '</td>
                                     </tr>
                                </table>
                                </td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="65%">Inventory</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Uom</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="25%">Qty</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            $total = 0;
            $totalVAT = 0;
            if (count($detail) > 0) {
                foreach ($detail as $data) {
                    if ($data->SalesParcelInternalID == 0) {
                        $inventory = Inventory::find($data->InventoryInternalID);
                        $inv = $inventory->InventoryID . ' ' . $inventory->TextPrint;
                        $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $inv . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $data->Uom->UomID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                            </tr>';
                        $totalVAT += $data->VAT;
                        $total += $data->SubTotal;

                        if ($totalVAT != 0) {
                            $totalVAT = $totalVAT - ($header->DiscountGlobal * 0.1);
                        }
                    }
                }
                $Parcel = SalesAddHeader::searchParcel($id);
                foreach ($Parcel as $data) {
                    $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $data->ParcelID . '-' . $data->ParcelName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">-</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, '0', '.', ',') . '</td>
                            </tr>';
                    $totalVAT += $data->VAT;
                    $total += $data->SubTotal;
                }
                if ($totalVAT != 0) {
                    $totalVAT = $totalVAT - ($header->DiscountGlobal * 0.1);
                }
            } else {
                $html .= '<tr>
                            <td colspan="3" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this sales.</td>
                        </tr>';
            }
            $html .= '</tbody>
                            </table>
                            <table width="100%" style="margin-top: 10px;">
                            <tr>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; padding-left: 50px; font-weight: 500;" width="60%">
                            Tanda Terima.
                            </td>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; padding-left: 50px; font-weight: 500;" width="40%">
                            Hormat Kami.
                            </td>
                            </tr>
                            </table>
                    </div>
                </body>
            </html>';
            return PDF ::load($html, 'A5', 'portrait')->show();
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showSales');
        }
    }

    function createID($tipe, $tax = null) {
        if ($tax == "Tax")
            $sales = 'SI';
        else if ($tax == "NonTax")
            $sales = 'SIN';
        else
            $sales = 'SI';
        if ($tipe == 0) {
            $sales .= '.' . date('m') . date('y');
        }
        return $sales;
    }

    function createIDSO($tipe, $tax = null) {
        if ($tax == "Tax")
            $sales = 'SO';
        else if ($tax == "NonTax")
            $sales = 'SON';
        else
            $sales = 'SO';
        if ($tipe == 0) {
            $sales .= '.' . date('m') . date('y');
        }
        return $sales;
    }

    function createIDShip($tipe, $tax = null) {
        if ($tax == "Tax")
            $shipping = 'SH';
        else if ($tax == "NonTax")
            $shipping = 'SHN';
        else
            $shipping = 'SH';
        if ($tipe == 0) {
            $shipping .= '.' . date('m') . date('y');
        }
        return $shipping;
    }

    public function formatCariIDSales() {
        $date = explode('-', Input::get('date'));
        $id = $this->createID(1);
        $yearDigit = substr($date[2], 2);
        $id .= '.' . $date[1] . $yearDigit . '.';
        echo SalesAddHeader::getNextIDSales($id);
    }

    function salesCSV($id) {
        $id = SalesHeader::getIdsales($id);
        $header = SalesHeader::find($id);
        Session::flash('idCSV', $id);
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            Excel::create('Tax_Report_sales', function($excel) {
                $excel->sheet('Tax_Report_sales', function($sheet) {
                    //baris pertama
                    $id = Session::get('idCSV');
                    $sheet->setCellValueByColumnAndRow(0, 1, "FK");
                    $sheet->setCellValueByColumnAndRow(1, 1, "KD_JENIS_TRANSAKSI");
                    $sheet->setCellValueByColumnAndRow(2, 1, "FG_PENGGANTI");
                    $sheet->setCellValueByColumnAndRow(3, 1, "NOMOR_FAKTUR");
                    $sheet->setCellValueByColumnAndRow(4, 1, "MASA_PAJAK");
                    $sheet->setCellValueByColumnAndRow(5, 1, "TAHUN_PAJAK");
                    $sheet->setCellValueByColumnAndRow(6, 1, "TANGGAL_FAKTUR");
                    $sheet->setCellValueByColumnAndRow(7, 1, "NPWP");
                    $sheet->setCellValueByColumnAndRow(8, 1, "NAMA");
                    $sheet->setCellValueByColumnAndRow(9, 1, "ALAMAT_LENGKAP");
                    $sheet->setCellValueByColumnAndRow(10, 1, "JUMLAH_DPP");
                    $sheet->setCellValueByColumnAndRow(11, 1, "JUMLAH_PPN");
                    $sheet->setCellValueByColumnAndRow(12, 1, "JUMLAH_PPNBM");
                    $sheet->setCellValueByColumnAndRow(13, 1, "ID_KETERANGAN_TAMBAHAN");
                    $sheet->setCellValueByColumnAndRow(14, 1, "FG_UANG_MUKA");
                    $sheet->setCellValueByColumnAndRow(15, 1, "UANG_MUKA_DPP");
                    $sheet->setCellValueByColumnAndRow(16, 1, "UANG_MUKA_PPN");
                    $sheet->setCellValueByColumnAndRow(17, 1, "UANG_MUKA_PPNBM");
                    $sheet->setCellValueByColumnAndRow(18, 1, "REFERENSI");
                    //baris kedua
                    $sheet->setCellValueByColumnAndRow(0, 2, "LT");
                    $sheet->setCellValueByColumnAndRow(1, 2, "NPWP");
                    $sheet->setCellValueByColumnAndRow(2, 2, "NAMA");
                    $sheet->setCellValueByColumnAndRow(3, 2, "JALAN");
                    $sheet->setCellValueByColumnAndRow(4, 2, "BLOK");
                    $sheet->setCellValueByColumnAndRow(5, 2, "NOMOR");
                    $sheet->setCellValueByColumnAndRow(6, 2, "RT");
                    $sheet->setCellValueByColumnAndRow(7, 2, "RW");
                    $sheet->setCellValueByColumnAndRow(8, 2, "KECAMATAN");
                    $sheet->setCellValueByColumnAndRow(9, 2, "KELURAHAN");
                    $sheet->setCellValueByColumnAndRow(10, 2, "KABUPATEN");
                    $sheet->setCellValueByColumnAndRow(11, 2, "PROPINSI");
                    $sheet->setCellValueByColumnAndRow(12, 2, "KODE_POS");
                    $sheet->setCellValueByColumnAndRow(13, 2, "NOMOR_TELEPON");
                    //baris ketiga
                    $sheet->setCellValueByColumnAndRow(0, 3, "OF");
                    $sheet->setCellValueByColumnAndRow(1, 3, "KODE_OBJEK");
                    $sheet->setCellValueByColumnAndRow(2, 3, "NAMA");
                    $sheet->setCellValueByColumnAndRow(3, 3, "HARGA_SATUAN");
                    $sheet->setCellValueByColumnAndRow(4, 3, "JUMLAH_BARANG");
                    $sheet->setCellValueByColumnAndRow(5, 3, "HARGA_TOTAL");
                    $sheet->setCellValueByColumnAndRow(6, 3, "DISKON");
                    $sheet->setCellValueByColumnAndRow(7, 3, "DPP");
                    $sheet->setCellValueByColumnAndRow(8, 3, "PPN");
                    $sheet->setCellValueByColumnAndRow(9, 3, "TARIF_PPNBM");
                    $sheet->setCellValueByColumnAndRow(10, 3, "PPNBM");
                    //isi baris pertama
                    $row = 4;
                    $header = SalesHeader::find(Session::get('idCSV'));
                    $sheet->setCellValueByColumnAndRow(0, $row, "FK");
                    $sheet->setCellValueExplicit('B' . $row, '0' . $header->TransactionType, \PHPExcel_Cell_DataType::TYPE_STRING);
//                    $sheet->setCellValueByColumnAndRow(1, $row, '0' . $header->TransactionType);
                    $sheet->setCellValueByColumnAndRow(2, $row, $header->Replacement);
                    //nomorPajak
                    $nomorPajak = str_replace('.', '', $header->TaxNumber);
                    $nomorPajak = str_replace('-', '', $nomorPajak);
                    $nomorPajak = substr($nomorPajak, 4);
                    if ($nomorPajak == '' || $nomorPajak == 0) {
                        $nomorPajak = "000000000000000";
                        $sheet->setCellValueExplicit('D' . $row, $nomorPajak, \PHPExcel_Cell_DataType::TYPE_STRING);
                    } else {
                        $sheet->setCellValueExplicit('D' . $row, '0' . $nomorPajak, \PHPExcel_Cell_DataType::TYPE_STRING);
                    }
//                    $sheet->setCellValueByColumnAndRow(3, $row, $nomorPajak);
                    $sheet->setCellValueByColumnAndRow(4, $row, $header->TaxMonth);
                    $sheet->setCellValueByColumnAndRow(5, $row, $header->TaxYear);
                    $sheet->setCellValueByColumnAndRow(6, $row, date('d/m/Y', strtotime($header->TaxDate)));

                    //nomorPajak
                    $pajakCustomer = str_replace('.', '', $header->Coa6->TaxID);
                    $pajakCustomer = str_replace('-', '', $pajakCustomer);
                    if ($pajakCustomer == '' || $pajakCustomer == 0) {
                        $pajakCustomer = "'000000000000000";
                    }

                    $sheet->setCellValueExplicit('H' . $row, $pajakCustomer, \PHPExcel_Cell_DataType::TYPE_STRING);
//                    $sheet->setCellValueByColumnAndRow(7, $row, $pajakCustomer);
                    $sheet->setCellValueByColumnAndRow(8, $row, $header->Coa6->ACC6Name);
                    $sheet->setCellValueByColumnAndRow(9, $row, $header->Coa6->Address);
                    //total
                    $salesDetail = SalesDetail::where('SalesInternalID', $header->InternalID)->sum('SubTotal');
                    $total = round($salesDetail - $header->DiscountGlobal);
                    $sheet->setCellValueByColumnAndRow(10, $row, ceil($total));
                    $sheet->setCellValueByColumnAndRow(11, $row, floor(($total * 0.1)));
                    $sheet->setCellValueByColumnAndRow(12, $row, 0);
                    $sheet->setCellValueByColumnAndRow(13, $row, '');
                    if ($header->DownPayment == 0) {
                        $dp = 0;
                    } else {
                        //pelunasan
                        $dp = 2;
                    }
                    if ($header->isCash == 2 || $header->isCash == 3) {
                        $sisa = 0;
                        $pajaksisa = 0;
                    } else {
                        if ($header->isCash == 4) {
                            $sisa = $total - $header->DownPayment;
                            $pajaksisa = floor($sisa * 0.1);
                        } else {
                            $sisa = 0;
                            $pajaksisa = 0;
                        }
                    }
                    $sheet->setCellValueByColumnAndRow(14, $row, $dp);
                    $sheet->setCellValueByColumnAndRow(15, $row, $sisa);
                    $sheet->setCellValueByColumnAndRow(16, $row, $pajaksisa);
//                    $sheet->setCellValueByColumnAndRow(15, $row, $header->DownPayment);
//                    $sheet->setCellValueByColumnAndRow(16, $row, floor($header->DownPayment * 0.1));
                    $sheet->setCellValueByColumnAndRow(17, $row, "0");
                    $sheet->setCellValueByColumnAndRow(18, $row, $header->SalesID);

                    $row++;
//                    $detail = $header->salesDetail()->get();
                    $detail = SalesAddHeader::find($id)->salesDescription()->get();
                    foreach ($detail as $data) {
                        $sheet->setCellValueByColumnAndRow(0, $row, "OF");
                        $sheet->setCellValueByColumnAndRow(1, $row, "");
//                        $sheet->setCellValueByColumnAndRow(1, $row, $data->Inventory->InventoryID);
                        $namabarang = $data->InventoryText . "\n";
                        $count = SalesAddDetail::where("DescriptionInternalID", $data->InternalID)
                                        ->join("m_inventory", "m_inventory.InternalID", "=", "t_sales_detail.InventoryInternalID")->count();
                        $cnt = 0;
                        foreach (SalesAddDetail::where("DescriptionInternalID", $data->InternalID)
                                ->join("m_inventory", "m_inventory.InternalID", "=", "t_sales_detail.InventoryInternalID")->get() as $detail) {
                            $cnt++;
                            if ($cnt == $count)
                                $namabarang .= $detail->InventoryName;
                            else
                                $namabarang .= $detail->InventoryName . "\n";
                        }
                        $sheet->setCellValueByColumnAndRow(2, $row, $namabarang);
//                        $sheet->setCellValueByColumnAndRow(2, $row, $data->Inventory->InventoryName);
                        $sheet->setCellValueByColumnAndRow(3, $row, $data->Price);
                        $sheet->setCellValueByColumnAndRow(4, $row, $data->Qty);
                        $sheet->setCellValueByColumnAndRow(5, $row, $data->Qty * $data->Price);
                        $sheet->setCellValueByColumnAndRow(6, $row, $data->Qty * $data->Price - $data->SubTotal);
                        $sheet->setCellValueByColumnAndRow(7, $row, $data->SubTotal);
                        $sheet->setCellValueByColumnAndRow(8, $row, $data->VAT);
                        $sheet->setCellValueByColumnAndRow(9, $row, 0);
                        $sheet->setCellValueByColumnAndRow(10, $row, 0);
                        $row++;
                    }
                    $sheet->setColumnFormat(array(
                        'B' => \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
                        'D' => \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
                        'H' => \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
                    ));
                });
            })->export('csv');
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showSales');
        }
    }

    public function taxSales() {
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $totalSA = 0;
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <!--<div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>-->
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Tax Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span>
                            <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span>';
        $hitung = 0;
        foreach (Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $dataCustomer) {
            if (SalesAddHeader::join("m_warehouse", "m_warehouse.InternalID", "=", "t_sales_header.WarehouseInternalID")
                            ->where('t_sales_header.CompanyInternalID', Auth::user()->Company->InternalID)
                            ->where('ACC6InternalID', $dataCustomer->InternalID)
                            ->where("VAT", "!=", Auth::user()->SeeNPPN)
                            ->where("m_warehouse.Type", Auth::user()->WarehouseCheck)
                            ->where("TaxNumber", "!=", ".-.")
                            ->whereBetween('SalesDate', Array($start, $end))->count() > 0) {
                $html .= '<table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=5>' . $dataCustomer->ACC6Name . '</th>
                                        </tr>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Tax Number</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Tax Month</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Tax Year</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                foreach (SalesAddHeader::join("m_warehouse", "m_warehouse.InternalID", "=", "t_sales_header.WarehouseInternalID")
                        ->where('t_sales_header.CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where('ACC6InternalID', $dataCustomer->InternalID)
                        ->where("VAT", "!=", Auth::user()->SeeNPPN)
                        ->where("TaxNumber", "!=", ".-.")
                        ->where("m_warehouse.Type", Auth::user()->WarehouseCheck)
                        ->whereBetween('SalesDate', Array($start, $end))->get() as $data) {
                    if ($data->TaxNumber == ".-.") {
                        $taxNumber = "-";
                    } else {
                        $taxNumber = $data->TaxNumber;
                    }
                    $monthName = date('F', mktime(0, 0, 0, $data->TaxMonth, 10));
                    $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->SalesID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->SalesDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $taxNumber . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $monthName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->TaxYear . '</td>
                            </tr>';
                }

                $html .= '</tbody>
            </table>';
                $hitung++;
            }
        }

        if ($hitung == 0) {
            $html .= '<br><span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no tax number.</span>';
        }
        $html .= '</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('tax_report');
    }

    public function summarySales() {
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $totalSA = 0;
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <!--<div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>-->
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Sales Summary Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span>
                            <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span>';
        $hitung = 0;
        foreach (Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $dataCustomer) {
            if (SalesAddHeader::join("m_warehouse", "m_warehouse.InternalID", "=", "t_sales_header.WarehouseInternalID")
                            ->where('t_sales_header.CompanyInternalID', Auth::user()->Company->InternalID)
                            ->where('ACC6InternalID', $dataCustomer->InternalID)
                            ->where("VAT", "!=", Auth::user()->SeeNPPN)
                            ->where("Type", Auth::user()->WarehouseCheck)
                            ->whereBetween('SalesDate', Array($start, $end))->count() > 0) {
                $html .= '<table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=7>' . $dataCustomer->ACC6Name . '</th>
                                        </tr>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Currency</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Rate</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total(After Discount)</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Vat</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Grand Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                $sumGrandTotal = 0;
                foreach (SalesAddHeader::join("m_warehouse", "m_warehouse.InternalID", "=", "t_sales_header.WarehouseInternalID")
                        ->where('t_sales_header.CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where('ACC6InternalID', $dataCustomer->InternalID)
                        ->where("VAT", "!=", Auth::user()->SeeNPPN)
                        ->where("Type", Auth::user()->WarehouseCheck)
                        ->whereBetween('SalesDate', Array($start, $end))->get() as $data) {
                    $grandTotal = $data->GrandTotal;
                    $sumGrandTotal += $grandTotal;
                    $totalSA += $grandTotal;
                    $total = $grandTotal;
                    $vat = 0;
                    if ($data->VAT == 1) {
                        $total = $total * 10 / 11;
                        $vat = $total / 10;
                    }
                    $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->SalesID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->SalesDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->Currency->CurrencyName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->CurrencyRate, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($total, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($vat, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($grandTotal, '2', '.', ',') . '</td>
                            </tr>';
                }
                $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="80%" colspan="6">Total : </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . number_format($sumGrandTotal, '2', '.', ',') . '</td>
                        </tr>';

                $html .= '</tbody>
            </table>';
                $hitung++;
            }
        }

        if ($hitung == 0) {
            $html .= '<br><span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no sales.</span>';
        }
        $html .= '<table class="tableBorder" width="100%"  style="margin-top: 10px; clear: both;  top: 50px;">
                    <thead>
                        <tr>
                            <th style="font-family: helvetica,sans-serif;font-size: 14px; margin: 5px !important; font-weight: 700; text-align: right" width="80%" colspan="6">Total Sales : </th>
                            <th style="font-family: helvetica,sans-serif;font-size: 14px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . number_format($totalSA, '2', '.', ',') . '</th>
                        </tr>
                    <thead>
                </table>';
        $html .= '</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('sales_summary');
    }

    public function detailSales() {
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $dateTamp = '';
        $coa6Tamp = '';
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <!--<div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>-->
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Sales Detail Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span>
                            <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span><br><br>';
        if (SalesAddHeader::join("m_warehouse", "m_warehouse.InternalID", "=", "t_sales_header.WarehouseInternalID")
                        ->where('t_sales_header.CompanyInternalID', Auth::user()->Company->InternalID)
                        ->whereBetween('SalesDate', Array($start, $end))
                        ->where("VAT", "!=", Auth::user()->SeeNPPN)
                        ->where("Type", Auth::user()->WarehouseCheck)
                        ->orderBy('SalesDate')->orderBy('ACC6InternalID')->count() > 0) {
            foreach (SalesAddHeader::join("m_warehouse", "m_warehouse.InternalID", "=", "t_sales_header.WarehouseInternalID")
                    ->where('t_sales_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->whereBetween('SalesDate', Array($start, $end))
                    ->where("VAT", "!=", Auth::user()->SeeNPPN)
                    ->where("Type", Auth::user()->WarehouseCheck)
                    ->orderBy('SalesDate')->orderBy('ACC6InternalID')->get() as $dataPenjualan) {
                if ($dateTamp != date("d-M-Y", strtotime($dataPenjualan->SalesDate))) {
                    $html .= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales Date : ' . date("d-M-Y", strtotime($dataPenjualan->SalesDate)) . '</span><br><br>';
                    $dateTamp = date("d-M-Y", strtotime($dataPenjualan->SalesDate));
                    $coa6Tamp = '';
                }
                if ($coa6Tamp != $dataPenjualan->ACC6InternalID) {
                    $html .= '<span style="padding-left:23px;font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Customer : ' . $dataPenjualan->coa6->ACC6Name . '</span>';
                    $coa6Tamp = $dataPenjualan->ACC6InternalID;
                }
                if ($dataPenjualan->coa6->ContactPerson != '' && $dataPenjualan->coa6->ContactPerson != '-' && $dataPenjualan->coa6->ContactPerson != null) {
                    $html .= '<span style="padding-left:23px;font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Contact Person : ' . $dataPenjualan->coa6->ContactPerson . '</span>';
                }
                $html .= '<table class="tableBorder" style="width:95%; margin-top: 18px; clear: both; position: relative; left: 4%">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=8>' . $dataPenjualan->SalesID . ' | ' . $dataPenjualan->Currency->CurrencyName . ' | Rate : ' . number_format($dataPenjualan->CurrencyRate, '2', '.', ',') . ' </th>
                                        </tr>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory ID</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Name</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Uom</th>
                                            <th width="10%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Qty</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Price</th>
                                            <th width="10%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc (%)</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                $total = 0;
                $vat = 0;
                foreach ($dataPenjualan->salesDetail as $data) {
                    if ($data->SalesParcelInternalID == 0) {
                        $total += ceil($data->SubTotal);
                        $vat += floor(ceil($data->SubTotal) / 10);
//                        $vat += $data->VAT;
                        $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->inventory->InventoryID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->inventory->InventoryName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->Uom->UomID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format(ceil($data->SubTotal), '2', '.', ',') . '</td>
                            </tr>';
                    }
                }
                foreach (SalesAddParcel::where("SalesInternalID", $dataPenjualan->InternalID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->get() as $data) {
                    $total += ceil($data->SubTotal);
                    $vat += floor(ceil($data->SubTotal) / 10);
//                    $vat += $data->VAT;
                    $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->Parcel->ParcelID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->Parcel->ParcelName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;"> - </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format(ceil($data->SubTotal), '2', '.', ',') . '</td>
                            </tr>';
                }
                if ($vat != 0) {
                    $vat = $vat - ($dataPenjualan->DiscountGlobal * 0.1);
                }
                $html .= '<tr>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=5></td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=2>Total <br> Discount <br> Grand Total <br> VAT <br> Grand Total </td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '<br>'
                        . '' . number_format($dataPenjualan->DiscountGlobal, '2', '.', ',') . '<br>'
                        . '' . number_format($total - $dataPenjualan->DiscountGlobal, '2', '.', ',') . '<br>'
                        . '' . number_format($vat, '2', '.', ',') . '<br>'
                        . '' . number_format($dataPenjualan->GrandTotal, '2', '.', ',') . '</td>
                    </tr>
                </tbody>
            </table>';
            }
        } else {
            $html .= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no sales.</span><br><br>';
        }
        $html .= '</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('sales_detail');
    }

    public function summarySalesExcel() {
        Excel::create('Summary_Sales', function($excel) {
            $excel->sheet('Summary_Sales', function($sheet) {
                $startT = explode('-', Input::get('sDate'));
                $endT = explode('-', Input::get('eDate'));
                $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
                $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
                $totalSA = 0;
                $sheet->mergeCells('B1:I1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Invoice Summary Report");
                $sheet->mergeCells('C2:D2');
                $sheet->setCellValueByColumnAndRow(1, 2, "Period :");
                $sheet->setCellValueByColumnAndRow(2, 2, date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))));
                $hitung = 0;
                $row = 3;
                foreach (Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $dataCustomer) {
                    if (SalesAddHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                                    ->where('ACC6InternalID', $dataCustomer->InternalID)
                                    ->whereBetween('SalesDate', Array($start, $end))->count() > 0) {
                        $row++;
                        $sheet->setBorder('B' . $row . ':I' . $row, 'thin');
                        $sheet->mergeCells('B' . $row . ':I' . $row);
                        $sheet->setCellValueByColumnAndRow(1, $row, $dataCustomer->ACC6Name);
                        $row++;
                        $sheet->cells('B' . $row . ':I' . $row, function($cells) {
                            $cells->setBackground('#eaf6f7');
                            $cells->setValignment('middle');
                            $cells->setAlignment('center');
                        });
                        $sheet->setBorder('B' . $row . ':I' . $row, 'thin');
                        $sheet->setCellValueByColumnAndRow(1, $row, "Sales ID");
                        $sheet->setCellValueByColumnAndRow(2, $row, "Date");
                        $sheet->setCellValueByColumnAndRow(3, $row, "Currency");
                        $sheet->setCellValueByColumnAndRow(4, $row, "Rate");
                        $sheet->setCellValueByColumnAndRow(5, $row, "Tax Number");
                        $sheet->setCellValueByColumnAndRow(6, $row, "DPP");
                        $sheet->setCellValueByColumnAndRow(7, $row, "VAT");
                        $sheet->setCellValueByColumnAndRow(8, $row, "Grand Total");
                        $row++;
                        $sumGrandTotal = 0;
                        foreach (SalesAddHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                                ->where('ACC6InternalID', $dataCustomer->InternalID)
                                ->whereBetween('SalesDate', Array($start, $end))->get() as $data) {
                            $grandTotal = $data->GrandTotal;
                            $sumGrandTotal += $grandTotal;
                            $totalSA += $grandTotal;
                            $total = $grandTotal;
                            $vat = 0;

                            $sheet->setCellValueByColumnAndRow(1, $row, $data->SalesID);
                            $sheet->setCellValueByColumnAndRow(2, $row, date("d/m/Y", strtotime($data->SalesDate)));
                            $sheet->setCellValueByColumnAndRow(3, $row, $data->Currency->CurrencyName);
                            $sheet->setCellValueByColumnAndRow(4, $row, number_format($data->CurrencyRate, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(5, $row, $data->TaxNumber);
                            $sheet->setCellValueByColumnAndRow(6, $row, number_format($total, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(7, $row, number_format($vat, '2', '.', ','));
                            $sheet->setCellValueByColumnAndRow(8, $row, number_format($grandTotal, '2', '.', ','));
                            $sheet->setBorder('B' . $row . ':I' . $row, 'thin');
                            $row++;
                        }
                        $sheet->setBorder('B' . $row . ':I' . $row, 'thin');
                        $sheet->mergeCells('B' . $row . ':H' . $row);
                        $sheet->setCellValueByColumnAndRow(1, $row, "Total :");
                        $sheet->setCellValueByColumnAndRow(8, $row, number_format($sumGrandTotal, '2', '.', ','));
                        $hitung++;
                        $row++;
                    }
                }
                if ($hitung == 0) {
                    $sheet->mergeCells('B' . $row . ':I' . $row);
                    $sheet->setCellValueByColumnAndRow(1, $row, "There is no Sales.");
                }
                $sheet->setBorder('B' . $row . ':I' . $row, 'thin');
                $sheet->mergeCells('B' . $row . ':H' . $row);
                $sheet->setCellValueByColumnAndRow(1, $row, "Total Sales :");
                $sheet->setCellValueByColumnAndRow(8, $row, number_format($totalSA, '2', '.', ','));

                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:F' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('left');
                });
            });
        })->export('xls');
    }

    //=====================================ajax=======================================
    public function getResultSearchSO() {
        $hitung = 0;
        $input = splitSearchValue(Input::get("id"));
        $date = date("Y-m-d", strtotime(Input::get("id")));
        $salesOrderHeader = SalesOrderHeader::where('t_salesorder_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->join('m_coa6', 'm_coa6.InternalID', '=', 't_salesorder_header.ACC6InternalID')
                ->where('Closed', 0)
                ->where('Status', 1)
                ->where('VAT', '!=', Auth::user()->SeeNPPN)
                ->where(function($query) use ($input, $date) {
                    $query->where("SalesOrderID", "like", $input)
                    ->orWhere("SalesOrderDate", "like", '%' . $date . '%')
                    ->orWhere("ACC6Name", "like", '%' . $input . '%');
                })
                ->OrderBy('SalesOrderDate', 'desc')
                ->select('t_salesorder_header.*', 'm_coa6.ACC6Name')
                ->get();
        if (count($salesOrderHeader) == 0) {
            ?>
            <span>Sales Order with keywords <i><u><?php echo Input::get("id"); ?></u></i> can't be found</span>
            <?php
        } else {
            ?>
            <select class="chosen-select choosen-modal" id="sales" style="" name="sales">
                <?php
                foreach ($salesOrderHeader as $sales) {
                    if (checkSalesAdd($sales->InternalID) && $hitung < 100) {
                        if (checkAmountSales($sales->InternalID) <= 0 && $sales->isCash == 2) {
                            ?>
                            <option value="<?php echo $sales->SalesOrderID ?>"><?php echo $sales->SalesOrderID . ' | ' . date("d-m-Y", strtotime($sales->SalesOrderDate)) . ' | ' . $sales->ACC6Name ?></option>
                            <?php
                        } else if ($sales->isCash == 4 && checkDpShipping($sales->InternalID)) {
                            ?>
                            <option value="<?php echo $sales->SalesOrderID ?>"><?php echo $sales->SalesOrderID . ' | ' . date("d-m-Y", strtotime($sales->SalesOrderDate)) . ' | ' . $sales->ACC6Name ?></option>
                            <?php
                        } else if ($sales->isCash != 2 && $sales->isCash != 4) {
                            ?>
                            <option value="<?php echo $sales->SalesOrderID ?>"><?php echo $sales->SalesOrderID . ' | ' . date("d-m-Y", strtotime($sales->SalesOrderDate)) . ' | ' . $sales->ACC6Name ?></option>
                            <?php
                        }
                        $hitung++;
                    }
                }
                ?>
            </select>
            <script>
                $(document).ready(function () {
                    var hitung = '<?php echo $hitung; ?>';
                    if (hitung == 0) {
                        $('#sales').after('<span>There is no result.</span>');
                        $('#sales').remove();
                    } else {
                        $("#btn-add-so").removeAttr("disabled");
                    }
                });
            </script>
            <?php
        }
    }

    public function salesPrintStruk($id) {
        $html = '<!DOCTYPE html>
        <html id="printSales" style="width: 118mm; height: 150mm;">
            <head>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Receipt</title>
                <style>
                    @media print{@page { size: 89mm auto;}}
                    @page {
                        margin-top: 0;
                        margin-bottom: 0;
                        size: 89mm auto;
                    }

                    body {
                        font-family: calibri; 
                        width: 100%;
                        margin: 0 auto;
                    }
                    
                    body * {
                        letter-spacing: 0.6mm !important;
                    }
                    
                    h4 {
                        font-size: 18px;
                        letter-spacing: 0.4px;
                        margin-bottom: 2px;
                    }

                    h3 {
                        font-size: 16px;
                        letter-spacing: 0.4px;
                        margin-top: 0px;
                        margin-bottom: 2px;
                    }

                    p {
                        font-size: 14px;
                        letter-spacing: 0.2px;
                    }

                    table {
                        border-collapse: separate;
                        font-size: 16px;
                        letter-spacing: 0.2px;
                        width: 100%;
                        margin: 0 auto;
                    }

                    table td {
                        vertical-align: middle;
                        padding: -10px;
                    }

                </style>
            </head>';
        $html .= '<body>
        <h4 style="text-align: center;">' . Auth::user()->company->CompanyName . '</h4>
        <p style="text-align: center;">' . Auth::user()->company->Address . ', ' . Auth::user()->company->City . '</p>
        <p style="text-align: center;">Telp : ' . Auth::user()->company->Phone . ', Fax : ' . Auth::user()->company->Fax . '</p>    
        <h3 style="text-align: center;">Sales</h3>';
        $id = SalesAddHeader::getIdsales($id);
        $header = SalesAddHeader::find($id);
        $detail = SalesAddHeader::find($id)->salesDetail()->get();
        $headerOrder = SalesOrderHeader::find($header->SalesOrderInternalID);
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $html .= '<table style="border-top: 0.4px solid #000000; border-bottom: 0.4px solid #000000;">
                <tr>
                    <td>' . $header->SalesID . '</td>
                    <td colspan="2" style="text-align:right;">' . Auth::user()->UserID . '</td>
                </tr>
                <tr>
                    <td colspan="3">
                       ' . date("d-m-Y", strtotime($header->SalesDate)) . '
                    </td>
                </tr>
                </table>';

            $html .= '<table style="border-bottom: 0.4px solid #000000;font-size: 18px;">';
//loop data inventory
            $count = 0;
            $total = 0;
            $totalVAT = 0;
            $i = 1;
            foreach ($detail as $data) {
                if ($data->SalesParcelInternalID == 0) {
                    $inventory = Inventory::find($data->InventoryInternalID);
                    if ($inventory->TextPrint != '' || $inventory->TextPrint != Null) {
                        $inv = $inventory->InventoryID . '- ' . $inventory->TextPrint;
                    } else {
                        $inv = $inventory->InventoryID . '- ' . $inventory->InventoryName;
                    }

                    $html .= '<tr>
                                <td colspan="4">
                                    ' . $i . '. ' . $inv . '
                                </td>
                            </tr>';
                    $html .= '<tr>
                                    <td style="text-align: right; width: 20%">' . number_format($data->Qty, 0, '.', ',') . ' ' . $data->Uom->UomID . '</td>
                                    <td style="text-align: right; width: 10%">x</td>
                                    <td style="text-align: right; width: 20%">' . number_format($data->Price, 2, ".", ",") . '</td>
                                    <td style="text-align: right; width: 30%">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                              </tr>';
                    $html .= '<tr>  
                                    <td colspan="4" style="font-size:12px;" ><div style="margin-left:64px">( Disc : ' . $data->Discount . '% , Disc : ' . number_format($data->DiscountNominal, '2', '.', ',') . ')</div></td>
                              </tr>';
                    $count++;
                    $i++;
                    $totalVAT += $data->VAT;
                    $total += $data->SubTotal;
                    if ($totalVAT != 0) {
                        $totalVAT = $totalVAT - ($header->DiscountGlobal * 0.1);
                    }
                }
            }
            $Parcel = SalesAddHeader::searchParcel($id);
            foreach ($Parcel as $data) {

                $html .= '<tr>
                                <td colspan="4">
                                    ' . $i . '. ' . $data->ParcelID . ' - ' . $data->ParcelName . '
                                </td>
                            </tr>';
                $html .= '<tr>
                                    <td style="text-align: right; width: 20%">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                    <td style="text-align: right; width: 10%">x</td>
                                    <td style="text-align: right; width: 20%">' . number_format($data->Price, 2, ".", ",") . '</td>
                                    <td style="text-align: right; width: 30%">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                              </tr>';
                $html .= '<tr>
                                    <td colspan="4" style="font-size:10px; ">( Disc : ' . $data->Discount . '% , Disc : ' . number_format($data->DiscountNominal, '2', '.', ',') . ')</td>
                              </tr>';
                $count++;
                $i++;
                if ($data->VAT != 0) {
                    $totalVAT += ($data->Price * $data->Qty) * 0.1;
                }
                $total += $data->Price * $data->Qty;
            }

            $html .= '</table>';


            $html .= '<table style="margin-bottom: 7px;font-size: 18px;">
                        <tr>
                            <td>Sum : ' . $count . ' item</td>
                            <td style="text-align: right;">Total</td>
                            <td style="text-align: right;">' . number_format($total, '2', '.', ',') . '</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: right;">Discount</td>
                            <td style="text-align: right;">' . number_format($header->DiscountGlobal, '2', '.', ',') . '</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: right;">Grand Total</td>
                            <td style="text-align: right;">' . number_format($total - $header->DiscountGlobal, '2', '.', ',') . '</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: right;">Tax</td>
                            <td style="text-align: right;">(' . $totalVAT . ')</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: right;"><strong>Grand Total</strong></td>
                            <td style="border-bottom: 1px solid #000000; text-align: right;">' . number_format($header->GrandTotal, '2', '.', ',') . '</td>
                        </tr>';
            $html .= '</table>

                    <ul style="page-break-after:always; font-size: 12px; list-style: none; display: block; padding: 0; margin: 0 auto;">
                        <li style="margin-bottom: 2px; line-height: 14px; text-align: center;">Thank you for your visit.</li>
                        <li style="line-height: 14px; text-align: center;">Please come again later.</li>
                    </ul>
                    <script src="' . Asset("") . 'lib/bootstrap/js/jquery-1.11.1.min.js"></script>
                    <script>
                    
                        $(document).ready(function(e){
                        window.print();
                        });
                        $(document).click(function(e){
                        window.location.assign("' . URL("/") . '");
                        });
                    </script>
                </body>
            </html>';

            echo $html;
        }
    }

    public function salesReport() {
        Excel::create('Sales_Report', function($excel) {
            $excel->sheet('Sales_Report', function($sheet) {
                if (Input::get("hide") == "hide") {
                    $sheet->getColumnDimension('G')->setVisible(false);
                }
                $startT = explode('-', Input::get('sDate'));
                $endT = explode('-', Input::get('eDate'));
                $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
                $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
//                $supplier = Input::get('customer');
//                if ($supplier == '-1') {
//                    $tanda = '!=';
//                } else {
//                    $tanda = '=';
//                }
                $sheet->mergeCells('B1:C1');
                $sheet->setCellValueByColumnAndRow(1, 1, "BUKU PENJUALAN");
//                $sheet->mergeCells('B2:C2');
                $sheet->setCellValueByColumnAndRow(1, 2, "Periode :");
                $sheet->setCellValueByColumnAndRow(2, 2, date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))));
                $row = 4;

//                foreach (Coa6::where('CompanyInternalID', Auth::user()->CompanyInternalID)->where('Type', 'c')->where('InternalID', $tanda, $supplier)->get() as $sup) {
//                    if (PurchaseHeader::where('ACC6InternalID', $sup->InternalID)->where('CompanyInternalID', Auth::user()->CompanyInternalID)->count() > 0) {
//                        $sheet->cells('B' . $row . ':C' . $row, function($cells) {
////                            $cells->setBackground('#eaf6f7');
//                            $cells->setFontSize('14');
//                            $cells->setValignment('middle');
//                            $cells->setFontWeight('bold');
//                        });
//                        $sheet->mergeCells('B' . $row . ':C' . $row);
//                        $sheet->setCellValueByColumnAndRow(1, $row, 'Supplier : ' . $sup->ACC6Name);
//                        $row++;

                $sheet->setCellValueByColumnAndRow(1, $row, 'Tanggal Invoice');
                $sheet->setCellValueByColumnAndRow(2, $row, 'Nota');
                $sheet->setCellValueByColumnAndRow(3, $row, 'Customer');
                $sheet->setCellValueByColumnAndRow(4, $row, 'Nama Barang');
                $sheet->setCellValueByColumnAndRow(5, $row, 'Unit');
                $sheet->setCellValueByColumnAndRow(6, $row, 'Harga @');
                $sheet->setCellValueByColumnAndRow(7, $row, 'Harga Total');
                $sheet->setCellValueByColumnAndRow(8, $row, 'PPN 10%');
                $sheet->setCellValueByColumnAndRow(9, $row, 'Sub Total');
                $sheet->setCellValueByColumnAndRow(10, $row, 'Tgl Bayar & Nominal');
                $sheet->setCellValueByColumnAndRow(11, $row, 'Keterangan');
                $sheet->setBorder('B' . $row . ':L' . $row, 'thin');
                $sheet->cells('B' . $row . ':L' . $row, function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $row++;
                foreach (SalesHeader::join("m_warehouse", "m_warehouse.InternalID", "=", "t_sales_header.WarehouseInternalID")
                        ->where('t_sales_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                        ->whereBetween('SalesDate', Array($start, $end))
                        ->where("VAT", "!=", Auth::user()->SeeNPPN)
                        ->where("m_warehouse.Type", Auth::user()->WarehouseCheck)
                        ->orderBy('SalesDate', 'asc')->get() as $si) {
                    $count = 1;
                    $total = 0;
                    foreach (SalesAddDetail::where('SalesInternalID', $si->InternalID)->where('SalesParcelInternalID', 0)->get() as $sd) {
                        if ($count == 1) {
                            $sheet->setBorder('B' . $row . ':L' . $row, 'thin');
                            $sheet->setCellValueByColumnAndRow(1, $row, date("d/m/Y", strtotime($si->SalesDate)));
                            $sheet->setCellValueByColumnAndRow(2, $row, $si->SalesID);
                            $sheet->setCellValueByColumnAndRow(3, $row, Coa6::find($si->ACC6InternalID)->ACC6Name);
                            $sheet->setCellValueByColumnAndRow(4, $row, Inventory::find($sd->InventoryInternalID)->InventoryName);
                            $sheet->setCellValueByColumnAndRow(5, $row, $sd->Qty);
                            $sheet->setCellValueByColumnAndRow(6, $row, number_format($sd->Price, 0, ".", ","));
                            $sheet->setCellValueByColumnAndRow(7, $row, number_format($sd->Price * $sd->Qty, 0, ".", ","));
                        } else {
                            $sheet->setBorder('B' . $row . ':L' . $row, 'thin');
                            $sheet->setCellValueByColumnAndRow(4, $row, Inventory::find($sd->InventoryInternalID)->InventoryName);
                            $sheet->setCellValueByColumnAndRow(5, $row, $sd->Qty);
                            $sheet->setCellValueByColumnAndRow(6, $row, number_format($sd->Price, 0, ".", ","));
                            $sheet->setCellValueByColumnAndRow(7, $row, number_format($sd->Price * $sd->Qty, 0, ".", ","));
                        }
                        $row++;
                        $count++;
                        $total += $sd->Price * $sd->Qty;
                    }
                    if (SalesAddDetail::where('SalesInternalID', $si->InternalID)->where('SalesParcelInternalID', 0)->count() > 0) {
                        $sheet->setBorder('B' . $row . ':L' . $row, 'thin');
                        $sheet->setCellValueByColumnAndRow(7, $row, number_format($total, 0, ".", ","));
                        $sheet->setCellValueByColumnAndRow(8, $row, number_format($total / 10, 2, ".", ","));
                        $sheet->setCellValueByColumnAndRow(9, $row, number_format($total * 1.1, 2, ".", ","));
                        $row++;
                        $sheet->setBorder('B' . $row . ':L' . $row, 'thin');
                        $row++;
                    }
                }
                $row++;
//                    }
//                }
//                $row--;
//                $sheet->setBorder('B2:C' . $row, 'thin');
                $sheet->cells('B2:L2', function($cells) {
                    $cells->setFontWeight('bold');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('left');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
//                $sheet->cells('B3:G' . $row, function($cells) {
//                    $cells->setAlignment('left');
//                    $cells->setValignment('middle');
//                });
//                $sheet->cells('B3:B' . $row, function($cells) {
//                    $cells->setAlignment('center');
//                });
            });
        })->export('xls');
    }

    public function customerManagerReport() {
        Excel::create('Customer_Manager_Report', function($excel) {
            $excel->sheet('Customer_Manager_Report', function($sheet) {
                if (Input::get("hide") == "hide") {
                    $sheet->getColumnDimension('G')->setVisible(false);
                }
                $startT = explode('-', Input::get('sDate'));
                $endT = explode('-', Input::get('eDate'));
                $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
                $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
//                $supplier = Input::get('customer');
//                if ($supplier == '-1') {
//                    $tanda = '!=';
//                } else {
//                    $tanda = '=';
//                }
                $sheet->mergeCells('B1:C1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Customer Manager Sales Report");
//                $sheet->mergeCells('B2:C2');
                $sheet->setCellValueByColumnAndRow(1, 2, "Periode :");
                $sheet->setCellValueByColumnAndRow(2, 2, date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))));
                $row = 4;

//                foreach (Coa6::where('CompanyInternalID', Auth::user()->CompanyInternalID)->where('Type', 'c')->where('InternalID', $tanda, $supplier)->get() as $sup) {
//                    if (PurchaseHeader::where('ACC6InternalID', $sup->InternalID)->where('CompanyInternalID', Auth::user()->CompanyInternalID)->count() > 0) {
//                        $sheet->cells('B' . $row . ':C' . $row, function($cells) {
////                            $cells->setBackground('#eaf6f7');
//                            $cells->setFontSize('14');
//                            $cells->setValignment('middle');
//                            $cells->setFontWeight('bold');
//                        });
//                        $sheet->mergeCells('B' . $row . ':C' . $row);
//                        $sheet->setCellValueByColumnAndRow(1, $row, 'Supplier : ' . $sup->ACC6Name);
//                        $row++;

                $sheet->setCellValueByColumnAndRow(1, $row, 'Tanggal Invoice');
                $sheet->setCellValueByColumnAndRow(2, $row, 'Nota');
                $sheet->setCellValueByColumnAndRow(3, $row, 'Customer');
                $sheet->setCellValueByColumnAndRow(4, $row, 'Nama Barang');
                $sheet->setCellValueByColumnAndRow(5, $row, 'Unit');
                $sheet->setCellValueByColumnAndRow(6, $row, 'Harga @');
                $sheet->setCellValueByColumnAndRow(7, $row, 'Harga Total');
                $sheet->setCellValueByColumnAndRow(8, $row, 'PPN 10%');
                $sheet->setCellValueByColumnAndRow(9, $row, 'Sub Total');
                $sheet->setCellValueByColumnAndRow(10, $row, 'Tgl Bayar & Nominal');
                $sheet->setCellValueByColumnAndRow(11, $row, 'Keterangan');
                $sheet->setBorder('B' . $row . ':L' . $row, 'thin');
                $sheet->cells('B' . $row . ':L' . $row, function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $row++;
                foreach (SalesHeader::join("m_warehouse", "m_warehouse.InternalID", "=", "t_sales_header.WarehouseInternalID")
                        ->where('t_sales_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                        ->where("m_warehouse.Type", Auth::user()->WarehouseCheck)
                        ->whereBetween('SalesDate', Array($start, $end))->orderBy('SalesDate', 'asc')->get() as $si) {
                    $count = 1;
                    $total = 0;
                    foreach (SalesAddDetail::where('SalesInternalID', $si->InternalID)->where('SalesParcelInternalID', 0)->get() as $sd) {
                        if ($count == 1) {
                            $sheet->setBorder('B' . $row . ':L' . $row, 'thin');
                            $sheet->setCellValueByColumnAndRow(1, $row, date("d/m/Y", strtotime($si->SalesDate)));
                            $sheet->setCellValueByColumnAndRow(2, $row, Coa6::find($si->ACC6InternalID)->ACC6Name);
                            $sheet->setCellValueByColumnAndRow(3, $row, $si->SalesID);
                            $sheet->setCellValueByColumnAndRow(4, $row, Inventory::find($sd->InventoryInternalID)->InventoryName);
                            $sheet->setCellValueByColumnAndRow(5, $row, $sd->Qty);
                            $sheet->setCellValueByColumnAndRow(6, $row, number_format($sd->Price, 0, ".", ","));
                            $sheet->setCellValueByColumnAndRow(7, $row, number_format($sd->Price * $sd->Qty, 0, ".", ","));
                        } else {
                            $sheet->setBorder('B' . $row . ':L' . $row, 'thin');
                            $sheet->setCellValueByColumnAndRow(4, $row, Inventory::find($sd->InventoryInternalID)->InventoryName);
                            $sheet->setCellValueByColumnAndRow(5, $row, $sd->Qty);
                            $sheet->setCellValueByColumnAndRow(6, $row, number_format($sd->Price, 0, ".", ","));
                            $sheet->setCellValueByColumnAndRow(7, $row, number_format($sd->Price * $sd->Qty, 0, ".", ","));
                        }
                        $row++;
                        $count++;
                        $total += $sd->Price * $sd->Qty;
                    }
                    if (SalesAddDetail::where('SalesInternalID', $si->InternalID)->where('SalesParcelInternalID', 0)->count() > 0) {
                        $sheet->setBorder('B' . $row . ':L' . $row, 'thin');
                        $sheet->setCellValueByColumnAndRow(7, $row, number_format($total, 0, ".", ","));
                        $sheet->setCellValueByColumnAndRow(8, $row, number_format($total / 10, 2, ".", ","));
                        $sheet->setCellValueByColumnAndRow(9, $row, number_format($total * 1.1, 2, ".", ","));
                        $row++;
                        $sheet->setBorder('B' . $row . ':L' . $row, 'thin');
                        $row++;
                    }
                }
                $row++;
//                    }
//                }
//                $row--;
//                $sheet->setBorder('B2:C' . $row, 'thin');
                $sheet->cells('B2:L2', function($cells) {
                    $cells->setFontWeight('bold');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('left');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
//                $sheet->cells('B3:G' . $row, function($cells) {
//                    $cells->setAlignment('left');
//                    $cells->setValignment('middle');
//                });
//                $sheet->cells('B3:B' . $row, function($cells) {
//                    $cells->setAlignment('center');
//                });
            });
        })->export('xls');
    }

    static function salesDataBackup($data) {
        $explode = explode('---;---', $data);
        $coa6 = $explode[0];
        $typePayment = $explode[1];
        $typeTax = $explode[2];
        $start = $explode[3];
        $end = $explode[4];
        $where = '';
        if ($typePayment != '-1' && $typePayment != '') {
            $where .= 'isCash = "' . $typePayment . '" ';
        }
        if ($coa6 != '-1' && $coa6 != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'ACC6InternalID = ' . $coa6 . ' ';
        }
        if (Auth::user()->SeeNPPN == 1) {
//            if ($typeTax != '-1' && $typeTax != '') {
//                if ($where != '') {
//                    $where .= ' AND ';
//                }
//                $where .= 'VAT = "' . $typeTax . '" ';
//            }
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'VAT = "0" ';
        } else {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'VAT = "1" ';
        }

        if ($where != '') {
            $where .= ' AND ';
        }
        $where .= 'Type = ' . Auth::user()->WarehouseCheck . ' ';

        if ($start != '' && $end != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $start = explode('-', $start);
            $end = explode('-', $end);
            $startSearch = $start[2] . '-' . $start[1] . '-' . $start[0];
            $endSearch = $end[2] . '-' . $end[1] . '-' . $end[0];
            $where .= 'SalesDate between "' . $startSearch . ' 00:00:00" AND "' . $endSearch . ' 23:59:59"';
        }
        $table = 'v_wsales';
//        $table = 't_sales_header';
        $primaryKey = 'InternalID';
//        $primaryKey = 't_sales_header`.`InternalID';
        $columns = array(
            array('db' => 'InternalID', 'dt' => 0, 'formatter' => function($d, $row) {
                    return $d;
                }),
            array('db' => 'SalesID', 'dt' => 1),
            array('db' => 'SalesOrderID', 'dt' => 2),
            array('db' => 'isCash', 'dt' => 3, 'formatter' => function( $d, $row ) {
                    if ($d == 0) {
                        return 'Cash';
                    } else if ($d == 1) {
                        return 'Credit';
                    } else if ($d == 2) {
                        return 'CBD';
                    } else if ($d == 3) {
                        return 'Deposit';
                    } else {
                        return 'Down Payment';
                    }
                },
                'field' => 't_sales_header`.`InternalID'),
            array(
                'db' => 'SalesDate',
                'dt' => 4,
                'formatter' => function( $d, $row ) {
                    return date("d-m-Y", strtotime($d));
                }
            ),
//            array('db' => 'CurrencyName', 'dt' => 3),
//            array(
//                'db' => 'CurrencyRate',
//                'dt' => 4,
//                'formatter' => function( $d, $row ) {
//                    return number_format($d, '2', '.', ',');
//                }
//            ),
            array(
                'db' => 'ACC6Name',
                'dt' => 5,
                'formatter' => function( $d, $row ) {
                    return $d;
                }
            ),
//            array(
//                'db' => 'VAT',
//                'dt' => 6,
//                'formatter' => function( $d, $row ) {
//                    if ($d == 0) {
//                        return 'Non Tax';
//                    } else {
//                        return 'Tax';
//                    }
//                }
//            ),
            array(
                'db' => 'GrandTotal',
                'dt' => 6,
                'formatter' => function( $d, $row ) {
                    return number_format($d, '2', '.', ',');
                }
            ),
//            array(
//                'db' => 'DownPayment',
//                'dt' => 6,
//                'formatter' => function( $d, $row ) {
//                    return number_format($d, '2', '.', ',');
//                }
//            ),
            array(
                'db' => 'SalesID',
                'dt' => 7,
                'formatter' => function( $d, $row ) {
                    $tampReceiv = 'Completed';
                    if (SalesHeader::getSalesReceivableID($d)[0]->total > 0) {
                        $tampReceiv = 'Uncompleted ';
                    }
                    return $tampReceiv;
                }
            ),
            array('db' => 'TaxNumber', 'dt' => 8),
            array('db' => 'Print',
                'dt' => 9,
                'formatter' => function($d, $row) {
                    return $d . " times";
                }
            ),
//                    't_sales_header`.`InternalID'
            array('db' => 'InternalID', 'dt' => 10, 'formatter' => function( $d, $row ) {
                    $data = SalesHeader::find($d);
                    $action = '<td class="text-center">
                                    <a href="' . Route('salesDetail', $data->SalesID) . '">
                                        <button id="btn-' . $data->SalesID . '-detail"
                                                class="btn btn-pure-xs btn-xs btn-detail" title="detail">
                                            <span class="glyphicon glyphicon-zoom-in"></span>
                                        </button>
                                    </a>
                                    ';
                    if (!SalesHeader::isReturn($data->SalesID)) {
                        if ((JournalDetail::where('JournalTransactionID', $data->SalesID)->sum('JournalCreditMU') == 0 && $data->isCash != 0) || $data->isCash == 0) {
                            $action .= '
                            <a href="' . Route('salesUpdate', $data->SalesID) . '">
                                        <button id="btn-' . $data->SalesID . '-update"
                                                class="btn btn-pure-xs btn-xs btn-edit" title="update">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </button>
                                    </a>';
                            if (checkMatrix('DeleteSales')) {
                                $action .= ' <button data-target="#m_salesDelete" data-internal="' . $data->InternalID . '"  data-toggle="modal" role="dialog" title="delete"
                                           onclick="deleteAttach(this)" data-id="' . $data->SalesID . '" data-name=' . $data->SalesID . ' class="btn btn-pure-xs btn-xs btn-delete">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>';
                            } else {
                                $action .= '
                                    <button disabled class="btn btn-pure-xs btn-xs btn-delete"><span class="glyphicon glyphicon-trash"></span></button>';
                            }
                        } else {
                            $action .= '<button disabled class="btn btn-pure-xs btn-xs btn-edit"><span class="glyphicon glyphicon-edit"></span></button>
                                    <button disabled class="btn btn-pure-xs btn-xs btn-delete"><span class="glyphicon glyphicon-trash"></span></button>';
                        }
                    } else {
                        $action .= '<button disabled class="btn btn-pure-xs btn-xs btn-edit"><span class="glyphicon glyphicon-edit"></span></button>
                                    <button disabled class="btn btn-pure-xs btn-xs btn-delete"><span class="glyphicon glyphicon-trash"></span></button>';
                    }
                    if (checkModul('O05')) {
                        $action .= '<a href="' . Route('salesCSV', $data->SalesID) . '" target="_blank">
                                        <button id="btn-' . $data->SalesID . '-print"
                                                class="btn btn-pure-xs btn-xs">
                                            <span class="glyphicon glyphicon-download"></span> CSV
                                        </button>
                                    </a>';
                    }
//                    $action .= '<a title="print" href="' . Route('salesPrint', $data->SalesID) . '">'
//                            . '<button id="btn-' . $data->SalesID . '"'
//                            . 'class="btn btn-pure-xs btn-xs btn-print">'
//                            . '<span class="glyphicon glyphicon-print"></span>'
//                            . '</button></a>';
//                    $action .= '<button data-target="#r_print" data-internal="' . $data->SalesID . '"  data-toggle="modal" role="dialog" title="print"
//                                           onclick="printAttach(this)" data-id="' . $data->SalesID . '" data-name=' . $data->SalesID . ' class="btn btn-pure-xs btn-xs btn-closed">
//                                        <span class="glyphicon glyphicon-print"></span>
//                                    </button>';
                    $action .= '<a target="_blank" title="print" href="' . route('salesPrint', $data->SalesID) . '">'
                            . '<button id="btn-' . $data->SalesID . '-print"'
                            . 'class="btn btn-pure-xs btn-xs btn-print" title="print">'
                            . '<span class="glyphicon glyphicon-print"></span>'
                            . '</button></a>';
//                    $action .= '<a href="' . Route('salesPrintStruk', $data->SalesID) . '" target="_blank" >
//                                        <button id="btn-' . $data->SalesID . '-print"
//                                                class="btn btn-pure-xs btn-xs btn-edit">
//                                            <span class="glyphicon glyphicon-print"></span> Struk   
//                                        </button>
//                                    </a>';
                    return $action;
                },
                'field' => 't_sales_header`.`InternalID')
        );
        $sql_details = getConnection();

        require('ssp.class.php');
        $ID_CLIENT_VALUE = Auth::user()->CompanyInternalID;
        if ($where != '') {
            $extraCondition = $where . ' AND ' . 'CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        } else {
            $extraCondition = 'CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        }
//        $join = ' INNER JOIN m_currency on m_currency.InternalID = t_sales_header.CurrencyInternalID '
//                . 'INNER JOIN m_coa6 on m_coa6.InternalID = t_sales_header.ACC6InternalID';

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $extraCondition, $join = null));
    }

    //===================================//ajax=======================================
}
