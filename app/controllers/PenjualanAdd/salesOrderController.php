<?php

class SalesOrderController extends BaseController {

    public function showSalesOrder() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteSalesOrder') {
                return $this->deleteSalesOrder();
            } else if (Input::get('jenis') == 'summarySalesOrder') {
                return $this->summarySalesOrder();
            } else if (Input::get('jenis') == 'detailSalesOrder') {
                return $this->detailSalesOrder();
            } else if (Input::get('jenis') == 'closeSalesOrder') {
                return $this->closeSalesOrder();
            } else if (Input::get('jenis') == 'approveSalesOrder') {
                return $this->approveSalesOrder();
            } else if (Input::get('jenis') == 'printSalesOrder') {
                if (Input::get('type') == 'exclude') {
                    return Redirect::route('salesOrderPrint', Input::get('internalID'));
                } else {
                    return Redirect::route('salesOrderIncludePrint', Input::get('internalID'));
                }
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = SalesOrderHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
            return View::make('penjualanAdd.salesOrderSearch')
                            ->withToogle('transaction')->withAktif('salesOrder')
                            ->withData($data);
        }
        $end = date('d-m-Y');
        $start = date('d-m-Y', strtotime('-7 days', strtotime($end)));
        $data = SalesOrderHeader::advancedSearch(-1, -1, '', '');
        return View::make('penjualanAdd.salesOrderSearch')
                        ->withToogle('transaction')->withAktif('salesOrder')
                        ->withData($data)->withStart($start)->withEnd($end);
//        return View::make('penjualanAdd.salesOrder')
//                        ->withToogle('transaction')->withAktif('salesOrder');
    }

    public function salesOrderNew() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteSalesOrder') {
                return $this->deleteSalesOrder();
            } else if (Input::get('jenis') == 'summarySalesOrder') {
                return $this->summarySalesOrder();
            } else if (Input::get('jenis') == 'detailSalesOrder') {
                return $this->detailSalesOrder();
            } else if (Input::get('jenis') == 'printSalesOrder') {
                if (Input::get('type') == 'exclude') {
                    return Redirect::route('salesOrderPrint', Input::get('internalID'));
                } else {
                    return Redirect::route('salesOrderIncludePrint', Input::get('internalID'));
                }
            } else if (Input::get('jenis') == 'approveSalesOrder') {
                return $this->approveSalesOrder();
            } else if (Input::get('jenis') == 'insertCustomer') {
                $this->insertCustomer();
                return $this->insertQuotation2();
            } else {
                return $this->insertSalesOrder();
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = SalesOrderHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
            return View::make('penjualanAdd.salesOrderSearch')
                            ->withToogle('transaction')->withAktif('salesOrder')
                            ->withData($data);
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            if (Input::get('jenis') == 'insertQuotation') {
                return $this->insertQuotation();
            }
        }
        $salesOrder = $this->createID(0) . '.';
        $coa6 = 0;
        $arrQuotation = 0;
        return View::make('penjualanAdd.salesOrderNew')
                        ->withCustomer($coa6)
                        ->withArrquotation($arrQuotation)
                        ->withToogle('transaction')->withAktif('salesOrder')
                        ->withSalesorder($salesOrder);
    }

    public function salesOrderDetail($id) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'summarySalesOrder') {
                return $this->summarySalesOrder();
            } else if (Input::get('jenis') == 'detailSalesOrder') {
                return $this->detailSalesOrder();
            } else if (Input::get('jenis') == 'deleteSalesOrder') {
                return $this->deleteSalesOrder();
            } else if (Input::get('jenis') == 'printSalesOrder') {
                if (Input::get('type') == 'exclude') {
                    return Redirect::route('salesOrderPrint', Input::get('internalID'));
                } else {
                    return Redirect::route('salesOrderIncludePrint', Input::get('internalID'));
                }
            } else if (Input::get('jenis') == 'approveSalesOrder') {
                return $this->approveSalesOrder();
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = SalesOrderHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
            return View::make('penjualanAdd.salesOrderSearch')
                            ->withToogle('transaction')->withAktif('salesOrder')
                            ->withData($data);
        }
        $id = SalesOrderHeader::getIdsalesOrder($id);
        $header = SalesOrderHeader::find($id);
        $detail = SalesOrderHeader::find($id)->salesOrderDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $a = SalesOrderQuotation::where('SalesOrderInternalID', $id)->get();
            $string = '';
            foreach ($a as $b) {
                $string .= $b->quotation->QuotationID . ',';
            }
            return View::make('penjualanAdd.salesOrderDetail')
                            ->withToogle('transaction')->withAktif('salesOrder')
                            ->withHeader($header)
                            ->withId($string)
                            ->withDetail($detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showSalesOrder');
        }
    }

    public function salesOrderUpdate($id) {
        $id = SalesOrderHeader::getIdsalesOrder($id);
        $header = SalesOrderHeader::find($id);
        $detail = SalesOrderHeader::find($id)->salesOrderDetail()->get();
        $description = SalesOrderHeader::find($id)->salesOrderDescription()->get();
//        && SalesOrderHeader::isInvoice($header->InternalID) == false
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                if (Input::get('jenis') == 'summarySalesOrder') {
                    return $this->summarySalesOrder();
                } else if (Input::get('jenis') == 'detailSalesOrder') {
                    return $this->detailSalesOrder();
                } else if (Input::get('jenis') == 'deleteSalesOrder') {
                    return $this->deleteSalesOrder();
                } else if (Input::get('jenis') == 'printSalesOrder') {
                    if (Input::get('type') == 'exclude') {
                        return Redirect::route('salesOrderPrint', Input::get('internalID'));
                    } else {
                        return Redirect::route('salesOrderIncludePrint', Input::get('internalID'));
                    }
                } else if (Input::get('jenis') == 'approveSalesOrder') {
                    return $this->approveSalesOrder();
                } else {
                    return $this->updateSalesOrder($id);
                }
            } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
                $data = SalesOrderHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
                return View::make('penjualanAdd.salesOrderSearch')
                                ->withToogle('transaction')->withAktif('salesOrder')
                                ->withData($data);
            }
            $salesOrder = $this->createID(0) . '.';
            return View::make('penjualanAdd.salesOrderUpdate')
                            ->withToogle('transaction')->withAktif('salesOrder')
                            ->withHeader($header)
                            ->withDetail($detail)
                            ->withDescription($description)
                            ->withSalesorder($salesOrder);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showSalesOrder');
        }
    }

    static function insertCustomer() {
        //rule
        $rule = array(
            'AccID' => 'required|max:200|unique:m_coa6,ACC6ID,NULL,ACC6ID,CompanyInternalID,' . Auth::user()->Company->InternalID . '',
            'AccName' => 'required|max:200',
            'Address' => 'max:1000',
            'City' => 'max:1000',
            'Block' => 'max:200',
            'AddressNumber' => 'max:200',
            'RT' => 'max:200',
            'RW' => 'max:200',
            'District' => 'max:200',
            'Subdistrict' => 'max:200',
            'Province' => 'max:200',
            'PostalCode' => 'max:200',
            'Origin' => 'max:200',
            'Phone' => 'max:200',
            'Fax' => 'max:200',
            'CreditLimit' => 'numeric',
            'Type' => 'required|max:100',
            'ContactPerson' => 'max:200',
            'remark' => 'max:1000'
        );
        $messages = array(
            'AccID.unique' => 'Account ID has already been taken.',
            'AccID.required' => 'Account ID field is required.',
            'AccID.max' => 'Account ID may not be greater than 200 characters.',
            'AccName.required' => 'Account name field is required.',
            'AccName.max' => 'Account name may not be greater than 200 characters.'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule, $messages);
        if ($validator->fails()) {
            //tidak valid
            return View::make('penjualanAdd.salesOrderNew')
                            ->withMessages('gagalInsert')
                            ->withError($validator->messages())
                            ->withToogle('transaction')->withAktif('salesOrder');
        } else {
            //valid
            $taxID = "";
            if (Input::get("taxID") == "") {
                $taxID = "...-.";
            } else {
                $taxID = Input::get("taxID");
            }
            $coa6 = new Coa6;
            $coa6->ACC6ID = Input::get('AccID');
            $coa6->ACC6Name = Input::get('AccName');
            $coa6->Address = Input::get('Address');
            $coa6->City = Input::get('City');
            $coa6->Block = Input::get('Block');
            $coa6->AddressNumber = Input::get('AddressNumber');
            $coa6->RT = Input::get('RT');
            $coa6->RW = Input::get('RW');
            $coa6->District = Input::get('District');
            $coa6->Subdistrict = Input::get('Subdistrict');
            $coa6->Province = Input::get('Province');
            $coa6->PostalCode = Input::get('PostalCode');
            $coa6->TaxID = $taxID;
            $coa6->Origin = Input::get('Origin');
            $coa6->Email = Input::get('Email');
            $coa6->Phone = Input::get('Phone');
            $coa6->CustomerManager = Input::get('customerManager');
            $coa6->COAInternalID = customerCOAInternalID();
            $coa6->SalesAssistant = Input::get('salesAssistant');
            $coa6->Fax = Input::get('Fax');
            $coa6->CreditLimit = Input::get('CreditLimit');
            $coa6->Type = Input::get('Type');
            if (Input::get('Type') == "c") {
                $coa6->typeUser = Input::get('userType');
            }
            $coa6->ContactPerson = Input::get('ContactPerson');
            $coa6->UserRecord = Auth::user()->UserID;
            $coa6->CompanyInternalID = Auth::user()->Company->InternalID;
            $coa6->UserModified = '0';
            $coa6->Remark = Input::get('remark');
            $coa6->save();

            return View::make('coa.coa6')
                            ->withMessages('suksesInsert')
                            ->withToogle('accounting')->withAktif('coa6');
        }
    }

    //sales quotation
    public function insertQuotation() {
        //sales order dari quotation itu get
        //bug msh ada di diskon
        $data = Input::all();
        $rule = array(
            "customerSales" => "required",
            "Quotation" => "required"
        );
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            return View::make('penjualanAdd.salesOrderNew')
                            ->withToogle('transaction')->withAktif('salesOrder');
        } else {
            $coa6 = Input::get("customerSales");
            $inputquot = Input::get("Quotation");
//            $cnt = Coa6::where('InternalID', "=", $coa6)->where('Type', 'c')->count();
            $cnt = Coa6::where('InternalID', "=", $coa6)->where('Type', 'c')->orWhere('ACC6Name', "=", $coa6)->where('Type', 'c')->where('InternalID', "!=", 0)->count();
//            dd(Coa6::where('InternalID', "=", $coa6)->where('Type','c')->orWhere('ACC6Name', "=", $coa6)->where('Type','c')->where('InternalID', "!=", 0)->get());
            //customer lama
            if ($cnt > 0) {
                $typecustomer = "lama";
            } else {
                $typecustomer = "baru";
            }

            $arrQuotation = [];
            $count = 0;
            foreach (Input::get("Quotation") as $data) {
                $arrQuotation[$count] = $data;
                $count++;
            }
            $Quotation = QuotationHeader::where('QuotationID', $arrQuotation[0])->first();
            $salesOrder = $this->createID(0) . '.';
            return View::make('penjualanAdd.salesOrderNew')
                            ->withToogle('transaction')->withAktif('salesOrder')
                            ->withCustomer($coa6)
                            ->withInputquot($inputquot)
                            ->withType($typecustomer)
                            ->withArrquotation($arrQuotation)
                            ->withQuotation($Quotation)
                            ->withSalesorder($salesOrder);
        }
    }

    public function insertQuotation2() {
        //sales order dari quotation itu get
        //bug msh ada di diskon
        $data = Input::all();
        $rule = array(
            "customerSales2" => "required",
            "Quotation2" => "required"
        );
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            return View::make('penjualanAdd.salesOrderNew')
                            ->withToogle('transaction')->withAktif('salesOrder');
        } else {
            $coa6 = Input::get("customerSales");
            $cnt = Coa6::where('ACC6Name', "=", $coa6)->where('Type', 'c')->where('InternalID', "!=", 0)->count();
//            $cnt = Coa6::where('InternalID', "=", $coa6)->orWhere('ACC6Name', "=", $coa6)->where('InternalID', "!=", 0)->count();
            //customer lama
            if ($cnt > 0) {
                $typecustomer = "lama";
            } else {
                $typecustomer = "baru";
            }

            $arrQuotation = [];
            $count = 0;
            foreach (Input::get("Quotation") as $data) {
                $arrQuotation[$count] = $data;
                $count++;
            }
            $Quotation = QuotationHeader::where('QuotationID', $arrQuotation[0])->first();
            $salesOrder = $this->createID(0) . '.';
            return View::make('penjualanAdd.salesOrderNew')
                            ->withToogle('transaction')->withAktif('salesOrder')
                            ->withCustomer($coa6)
                            ->withType($typecustomer)
                            ->withArrquotation($arrQuotation)
                            ->withQuotation($Quotation)
                            ->withSalesorder($salesOrder);
        }
    }

    public function insertSalesOrder() {
        //rule
        $jenis = Input::get('isCash');
        if ($jenis == '0' || $jenis == '2' || $jenis == '3') {
            $rule = array(
                'date' => 'required',
                'coa6' => 'required',
//                'remark' => 'required|max:1000',
                'currency' => 'required',
//                'warehouse' => 'required',
                'rate' => 'required',
                'purchasingCommission' => 'required',
                'inventoryDescription' => 'required',
                'salesman' => 'required',
                'isShipping' => 'required'
            );
            $longTerm = 0;
        } else {
            if ($jenis == '1') {
                $rule = array(
                    'date' => 'required',
                    'longTerm' => 'required|integer',
                    'coa6' => 'required',
//                'remark' => 'required|max:1000',
                    'currency' => 'required',
//                'warehouse' => 'required',
                    'rate' => 'required',
                    'purchasingCommission' => 'required',
                    'inventoryDescription' => 'required',
                    'salesman' => 'required',
                    'isShipping' => 'required'
                );
            } else {
                $rule = array(
                    'date' => 'required',
                    'longTerm' => 'required|integer',
                    'downPayment' => 'required',
                    'coa6' => 'required',
//                'remark' => 'required|max:1000',
                    'currency' => 'required',
//                'warehouse' => 'required',
                    'rate' => 'required',
                    'purchasingCommission' => 'required',
                    'inventoryDescription' => 'required',
                    'salesman' => 'required',
                    'isShipping' => 'required'
                );
            }
            $longTerm = Input::get('longTerm');
        }
        $salesOrderNumber = '';
        //validasi
        $data = Input::all();
//        echo "<pre>";
//        print_r($data);
//        echo "</pre>";
//        exit();

        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalInsert';
            $error = $validator->messages();
        } else {
            //valid
            //insert header
            $tax = '';
            if (Input::get('vat') == '') {
                $tax = "NonTax";
            } else {
                $tax = "Tax";
            }
            $header = new SalesOrderHeader;
            $salesOrder = $this->createID(1, $tax) . '.';
            $date = explode('-', Input::get('date'));
            $yearDigit = substr($date[2], 2);
            $salesOrder .= $date[1] . $yearDigit . '.';
            $salesOrderNumber = SalesOrderHeader::getNextIDSalesOrder($salesOrder);
            $header->SalesOrderID = $salesOrderNumber;
            $header->SalesOrderDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $header->ACC6InternalID = Input::get('coa6');
//            dd(Input::get('coa6'));
            $header->PurchasingCommission = str_replace(',', '', Input::get('purchasingCommission'));
            $header->SalesManInternalID = Input::get('salesman');
            $header->LongTerm = $longTerm;
            $header->isCash = $jenis;
            $header->PickupType = Input::get('isShipping');
            $currency = explode('---;---', Input::get('currency'));
            $header->WarehouseInternalID = Input::get('warehouse');
            $header->CurrencyInternalID = $currency[0];
            $header->CurrencyRate = str_replace(',', '', Input::get('rate'));
            if (Input::get('vat') == '') {
                $header->VAT = '0';
            } else {
                $header->VAT = Input::get('vat');
            }
            $header->DiscountGlobal = str_replace(",", "", Input::get('DiscountGlobal'));
            $header->GrandTotal = Input::get('grandTotalValue');
            $header->POCustomer = Input::get('POCustomer');
            if (Input::get('jenisDP') == 0){
                $header->DownPayment = str_replace(",", "", Input::get('downPayment'));
                $header->DPValue = 0;
            }
            else{
                $header->DPValue = str_replace(",", "", Input::get('downPaymentValue'));
                $header->DownPayment = 0;
            }
            $header->DeliveryTerms = Input::get('DeliveryTerms');
            $header->DeliveryTime = Input::get('DeliveryTime');
            $header->Print = 1;
            $header->UserRecord = Auth::user()->UserID;
            $header->CompanyInternalID = Auth::user()->Company->InternalID;
            $header->UserModified = '0';
            $header->Remark = Input::get('remark');
            $header->save();
            $gt = 0;
            $total = 0;
            $indexterakhirinventory = 0;
            for ($a = 0; $a < count(Input::get('inventoryDescription')); $a++) {
                $uomDescriptionValue = Input::get('uomDescription')[$a];
                $spesifikasi = Input::get('spesifikasi')[$a];
                $qtyDescriptionValue = str_replace(',', '', Input::get('qtyDescription')[$a]);
                $priceDescriptionValue = str_replace(',', '', Input::get('priceDescription')[$a]);
                $discDescriptionValue = str_replace(',', '', Input::get('discountNominalDescription')[$a]);
                $subTotalDescription = ($priceDescriptionValue * $qtyDescriptionValue) - (($priceDescriptionValue * $qtyDescriptionValue) * Input::get('discountDescription')[$a] / 100) - $discDescriptionValue * $qtyDescriptionValue;
                if (Input::get('vat') == '1') {
                    $vatValue = $subTotalDescription / 10;
                } else {
                    $vatValue = 0;
                }
                if ($qtyDescriptionValue > 0) {
                    $description = new SalesOrderDescription();
                    $description->SalesOrderInternalID = $header->InternalID;
                    $description->InventoryText = Input::get('inventoryDescription')[$a];
                    $description->Spesifikasi = $spesifikasi;
                    $description->UomText = $uomDescriptionValue;
                    $description->Qty = $qtyDescriptionValue;
                    $description->Price = $priceDescriptionValue;
                    $description->Discount = 0.00;
//                    $description->Discount = Input::get('discountDescription')[$a];
                    $description->DiscountNominal = $discDescriptionValue;
                    $description->VAT = $vatValue;
                    $description->SubTotal = $subTotalDescription;
                    $description->UserRecord = Auth::user()->UserID;
                    $description->UserModified = '0';
                    $description->save();
                }
                $gt += $subTotalDescription;
//insert detail
                $plus = 1;
                if (count(Input::get('inventory' . ($indexterakhirinventory + $plus))) == 0) {
                    while (count(Input::get('inventory' . ($indexterakhirinventory + $plus))) == 0) {
                        $plus++;
                    }
                }
                $indexterakhirinventory = $indexterakhirinventory + $plus;

                for ($c = 0; $c < count(Input::get('inventory' . ($indexterakhirinventory))); $c++) {
                    $tamp = explode("---;---", Input::get('inventory' . ($indexterakhirinventory))[$c]);
                    if ($tamp[1] != "parcel") {
                        //jika bukan paket
                        $qtyValue = str_replace(',', '', Input::get('qty' . ($indexterakhirinventory))[$c]);
                        $priceValue = str_replace(',', '', Input::get('price' . ($indexterakhirinventory))[$c]);
                        $discValue = str_replace(',', '', Input::get('discountNominal' . ($indexterakhirinventory))[$c]);
                        $subTotal = ($priceValue * $qtyValue) - (($priceValue * $qtyValue) * Input::get('discount' . ($indexterakhirinventory))[$c] / 100) - $discValue * $qtyValue;
                        if (Input::get('vat') == '1') {
                            $vatValue = $subTotal / 10;
                        } else {
                            $vatValue = 0;
                        }
                        if ($qtyValue > 0) {
                            $detail = new SalesOrderDetail();
                            $detail->SalesOrderInternalID = $header->InternalID;
                            $detail->DescriptionInternalID = $description->InternalID;
                            $detail->InventoryInternalID = Input::get('inventory' . ($indexterakhirinventory))[$c];
                            $detail->UomInternalID = Input::get('uom' . ($indexterakhirinventory))[$c];
                            $detail->Qty = $qtyValue;
                            $detail->Price = $priceValue;
                            $detail->Discount = Input::get('discount' . ($indexterakhirinventory))[$c];
                            $detail->DiscountNominal = $discValue;
                            $detail->VAT = $vatValue;
                            $detail->SubTotal = $subTotal;
                            $detail->UserRecord = Auth::user()->UserID;
                            $detail->UserModified = '0';
                            $detail->save();
                        }
                        $total += $subTotal;
                    } else {
                        //jika paket
                        $parcelDetail = ParcelInventory::where("ParcelInternalID", $tamp[0])->where("CompanyInternalID", Auth::user()->Company->InternalID)->get();

                        $qtyValue = str_replace(',', '', Input::get('qty' . ($indexterakhirinventory))[$c]);
                        $priceValue = str_replace(',', '', Input::get('price' . ($indexterakhirinventory))[$c]);
                        $discValue = str_replace(',', '', Input::get('discountNominal' . ($indexterakhirinventory))[$c]);
                        $subTotal = ($priceValue * $qtyValue) - (($priceValue * $qtyValue) * Input::get('discount' . ($indexterakhirinventory))[$c] / 100) - $discValue * $qtyValue;
                        if (Input::get('vat') == '1') {
                            $vatValue = $subTotal / 10;
                        } else {
                            $vatValue = 0;
                        }

                        if ($qtyValue > 0) {
                            $SOParcel = new SalesOrderParcel();
                            $SOParcel->SalesOrderInternalID = $header->InternalID;
                            $SOParcel->DescriptionInternalID = $description->InternalID;
                            $SOParcel->ParcelInternalID = $tamp[0];
                            $SOParcel->Qty = $qtyValue;
                            $SOParcel->Price = $priceValue;
                            $SOParcel->Discount = Input::get('discount' . ($indexterakhirinventory))[$c];
                            $SOParcel->VAT = $vatValue;
                            $SOParcel->DiscountNominal = $discValue;
                            $SOParcel->SubTotal = $subTotal;
                            $SOParcel->CompanyInternalID = Auth::user()->Company->InternalID;
                            $SOParcel->UserRecord = Auth::user()->UserID;
                            $SOParcel->UserModified = "0";
                            $SOParcel->Remark = "-";
                            $SOParcel->save();

                            foreach ($parcelDetail as $dataParcel) {
                                $uom1 = $dataParcel->UomInternalID;
                                $qtyValue1 = $dataParcel->Qty * $qtyValue;
                                $priceValue1 = $dataParcel->Price;
                                $subTotal1 = ($priceValue1 * $qtyValue1);
                                if (Input::get('vat') == '1') {
                                    $vatValue1 = $subTotal1 / 10;
                                } else {
                                    $vatValue1 = 0;
                                }
                                $detail = new SalesOrderDetail();
                                $detail->SalesOrderInternalID = $header->InternalID;
                                $detail->DescriptionInternalID = $description->InternalID;
                                $detail->InventoryInternalID = $dataParcel->InventoryInternalID;
                                $detail->UomInternalID = $uom1;
                                $detail->SalesOrderParcelInternalID = $SOParcel->InternalID;
                                $detail->Qty = $qtyValue1;
                                $detail->Price = $priceValue1;
                                $detail->Discount = 0;
                                $detail->DiscountNominal = 0;
                                $detail->VAT = $vatValue1;
                                $detail->SubTotal = $subTotal1;
                                $detail->UserRecord = Auth::user()->UserID;
                                $detail->UserModified = '0';
                                $detail->save();
                            }
                        }
                        $total += $subTotal;
                    }
                }
            }
//            $header->GrandTotal = $gt;
//            $header->save();
            $messages = 'suksesInsert';
            $error = '';
        }
        if (Input::get('idQuotation') != NULL) {
            $quotationid = explode(",", Input::get('arrQuotation'));
            for ($a = 0; $a < count($quotationid) - 1; $a++) {
                $id = QuotationHeader::getIdquotation($quotationid[$a]);
                $orderquotation = new SalesOrderQuotation();
                $orderquotation->SalesOrderInternalID = $header->InternalID;
                $orderquotation->QuotationInternalID = $id;
                $orderquotation->UserRecord = Auth::user()->UserID;
                $orderquotation->UserModified = '0';
                $orderquotation->CompanyInternalID = Auth::user()->Company->InternalID;
                $orderquotation->Remark = "-";
                $orderquotation->save();
            }
        }
        if (isset($header) && !is_null($header)) {
            $print = Route('salesOrderPrint', $header->SalesOrderID);
            if ($messages == 'suksesInsert') {
                return Redirect::route('salesOrderDetail', $header->SalesOrderID)->with("msg", "print");
            }
        }
        $salesOrder = $this->createID(0) . '.';

        if ($messages == 'suksesInsert') {
            return Redirect::route('salesOrderDetail', $header->SalesOrderID)->with("msg", "print");
        }
        $print = '';
        return View::make('penjualanAdd.salesOrderNew')
                        ->withToogle('transaction')->withAktif('salesOrder')
                        ->withSalesorder($salesOrder)
                        ->withError($error)
                        ->withPrint($print)
                        ->withMessages($messages);
    }

    public function updateSalesOrder($id) {
        //tipe
        $headerUpdate = SalesOrderHeader::find($id);
        $detailUpdate = SalesOrderHeader::find($id)->salesOrderDetail()->get();
        $description = SalesOrderHeader::find($id)->salesOrderDescription()->get();
        //rule
        if (Input::get('isCash') == '0') {
            $rule = array(
                'coa6' => 'required',
//                'remark' => 'required|max:1000',
                'currency' => 'required',
//                'warehouse' => 'required',
                'rate' => 'required',
                'purchasingCommission' => 'required',
                'inventoryDescription' => 'required',
                'salesman' => 'required',
                'isShipping' => 'required'
            );
            $longTerm = 0;
        } else {
            $rule = array(
                'longTerm' => 'required|integer',
                'coa6' => 'required',
//                'remark' => 'required|max:1000',
                'currency' => 'required',
//                'warehouse' => 'required',
                'rate' => 'required',
                'purchasingCommission' => 'required',
                'inventoryDescription' => 'required',
                'salesman' => 'required',
                'isShipping' => 'required'
            );
            $longTerm = Input::get('longTerm');
        }

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalUpdate';
            $error = $validator->messages();
        } else {
            $header = SalesOrderHeader::find(Input::get('SalesOrderInternalID'));
            $header->ACC6InternalID = Input::get('coa6');
            $header->SalesManInternalID = Input::get('salesman');
            $header->isCash = Input::get('isCash');
            $header->LongTerm = $longTerm;
            $currency = explode('---;---', Input::get('currency'));
            $header->PurchasingCommission = str_replace(',', '', Input::get('purchasingCommission'));
            $header->WarehouseInternalID = Input::get('warehouse');
            $header->CurrencyInternalID = $currency[0];
            $header->PickupType = Input::get('isShipping');
            $header->CurrencyRate = str_replace(',', '', Input::get('rate'));
            if (Input::get('vat') == '') {
                $header->VAT = '0';
            } else {
                $header->VAT = Input::get('vat');
            }
            if (Input::get('jenisDP') == 0){
                $header->DownPayment = str_replace(",", "", Input::get('downPayment'));
                $header->DPValue = 0;
            }
            else{
                $header->DPValue = str_replace(",", "", Input::get('downPaymentValue'));
                $header->DownPayment = 0;
            }
            $header->DiscountGlobal = str_replace(",", "", Input::get('DiscountGlobal'));
            $header->GrandTotal = Input::get('grandTotalValue');
            $header->POCustomer = Input::get('POCustomer');
            $header->DeliveryTerms = Input::get('DeliveryTerms');
            $header->DeliveryTime = Input::get('DeliveryTime');
            $header->UserModified = Auth::user()->UserID;
            $header->Remark = Input::get('remark');
            $header->save();

            SalesOrderDetail::where('SalesOrderInternalID', '=', Input::get('SalesOrderInternalID'))->update(array('is_deleted' => 1));
            SalesOrderParcel::where('SalesOrderInternalID', '=', Input::get('SalesOrderInternalID'))->update(array('is_deleted' => 1));
            SalesOrderDescription::where('SalesOrderInternalID', '=', Input::get('SalesOrderInternalID'))->update(array('is_deleted' => 1));
            $indexterakhirinventory = 0;
            for ($a = 0; $a < count(Input::get('inventoryDescription')); $a++) {
                $uomDescriptionValue = Input::get('uomDescription')[$a];
                $spesifikasi = Input::get('spesifikasi')[$a];
                $qtyDescriptionValue = str_replace(',', '', Input::get('qtyDescription')[$a]);
                $priceDescriptionValue = str_replace(',', '', Input::get('priceDescription')[$a]);
                $discDescriptionValue = str_replace(',', '', Input::get('discountNominalDescription')[$a]);
                $subTotalDescription = ($priceDescriptionValue * $qtyDescriptionValue) - (($priceDescriptionValue * $qtyDescriptionValue) * Input::get('discountDescription')[$a] / 100) - $discDescriptionValue * $qtyDescriptionValue;
                if (Input::get('vat') == '1') {
                    $vatValue = $subTotalDescription / 10;
                } else {
                    $vatValue = 0;
                }
                if ($qtyDescriptionValue > 0) {
                    $description = new SalesOrderDescription();
                    $description->SalesOrderInternalID = $header->InternalID;
                    $description->InventoryText = Input::get('inventoryDescription')[$a];
                    $description->Spesifikasi = $spesifikasi;
                    $description->UomText = $uomDescriptionValue;
                    $description->Qty = $qtyDescriptionValue;
                    $description->Price = $priceDescriptionValue;
                    $description->Discount = 0.00;
//                    $description->Discount = Input::get('discountDescription')[$a];
                    $description->DiscountNominal = $discDescriptionValue;
                    $description->VAT = $vatValue;
                    $description->SubTotal = $subTotalDescription;
                    $description->UserRecord = Auth::user()->UserID;
                    $description->UserModified = '0';
                    $description->save();
                }
                //insert detail
                $plus = 1;
                if (count(Input::get('inventory' . ($indexterakhirinventory + $plus))) == 0) {
                    while (count(Input::get('inventory' . ($indexterakhirinventory + $plus))) == 0) {
                        $plus++;
                    }
                }
                $indexterakhirinventory = $indexterakhirinventory + $plus;
                $total = 0;
                for ($c = 0; $c < count(Input::get('inventory' . ($indexterakhirinventory))); $c++) {
                    $tamp = explode("---;---", Input::get('inventory' . ($indexterakhirinventory))[$c]);
                    if ($tamp[1] != "parcel") {
                        //jika bukan paket
                        $qtyValue = str_replace(',', '', Input::get('qty' . ($indexterakhirinventory))[$c]);
                        $priceValue = str_replace(',', '', Input::get('price' . ($indexterakhirinventory))[$c]);
                        $discValue = str_replace(',', '', Input::get('discountNominal' . ($indexterakhirinventory))[$c]);
                        $subTotal = ($priceValue * $qtyValue) - ($priceValue * $qtyValue) * Input::get('discount' . ($indexterakhirinventory))[$c] / 100 - $discValue * $qtyValue;
                        $total += $subTotal;
                        if (Input::get('vat') == '1') {
                            $vatValue = $subTotal / 10;
                        } else {
                            $vatValue = 0;
                        }
                        if ($qtyValue > 0) {
                            $detail = new SalesOrderDetail();
                            $detail->SalesOrderInternalID = Input::get('SalesOrderInternalID');
                            $detail->InventoryInternalID = Input::get('inventory' . ($indexterakhirinventory))[$c];
                            $detail->UomInternalID = Input::get('uom' . ($indexterakhirinventory))[$c];
                            $detail->DescriptionInternalID = $description->InternalID;
                            $detail->SalesOrderParcelInternalID = 0;
                            $detail->Qty = $qtyValue;
                            $detail->Price = $priceValue;
                            $detail->Discount = Input::get('discount' . ($indexterakhirinventory))[$c];
                            $detail->DiscountNominal = $discValue;
                            $detail->VAT = $vatValue;
                            $detail->SubTotal = $subTotal;
                            $detail->UserRecord = Auth::user()->UserID;
                            $detail->UserModified = Auth::user()->UserID;
                            $detail->save();
                        }
                        $total += $subTotal;
                    } else {
                        //jika paket
                        $parcelDetail = ParcelInventory::where("ParcelInternalID", $tamp[0])->where("CompanyInternalID", Auth::user()->Company->InternalID)->get();

                        $qtyValue = str_replace(',', '', Input::get('qty' . ($indexterakhirinventory))[$c]);
                        $priceValue = str_replace(',', '', Input::get('price' . ($indexterakhirinventory))[$c]);
                        $discValue = str_replace(',', '', Input::get('discountNominal' . ($indexterakhirinventory))[$c]);
                        $subTotal = ($priceValue * $qtyValue) - (($priceValue * $qtyValue) * Input::get('discount' . ($indexterakhirinventory))[$c] / 100) - $discValue * $qtyValue;
                        if (Input::get('vat') == '1') {
                            $vatValue = $subTotal / 10;
                        } else {
                            $vatValue = 0;
                        }
                        if ($qtyValue > 0) {
                            $SOParcel = new SalesOrderParcel();
                            $SOParcel->SalesOrderInternalID = $header->InternalID;
                            $SOParcel->ParcelInternalID = $tamp[0];
                            $SOParcel->Qty = $qtyValue;
                            $SOParcel->DescriptionInternalID = $description->InternalID;
                            $SOParcel->Price = $priceValue;
                            $SOParcel->Discount = Input::get('discount' . ($indexterakhirinventory))[$c];
                            $SOParcel->VAT = $vatValue;
                            $SOParcel->DiscountNominal = $discValue;
                            $SOParcel->SubTotal = $subTotal;
                            $SOParcel->CompanyInternalID = Auth::user()->CompanY->InternalID;
                            $SOParcel->UserRecord = Auth::user()->UserID;
                            $SOParcel->UserModified = "0";
                            $SOParcel->Remark = "-";
                            $SOParcel->save();

                            foreach ($parcelDetail as $dataParcel) {
                                $uom1 = $dataParcel->UomInternalID;
                                $qtyValue1 = $dataParcel->Qty * $qtyValue;
                                $priceValue1 = $dataParcel->Price;
                                $subTotal1 = ($priceValue1 * $qtyValue1);
                                if (Input::get('vat') == '1') {
                                    $vatValue1 = $subTotal1 / 10;
                                } else {
                                    $vatValue1 = 0;
                                }
                                $detail = new SalesOrderDetail();
                                $detail->SalesOrderInternalID = $header->InternalID;
                                $detail->InventoryInternalID = $dataParcel->InventoryInternalID;
                                $detail->UomInternalID = $uom1;
                                $detail->DescriptionInternalID = $description->InternalID;
                                $detail->SalesOrderParcelInternalID = $SOParcel->InternalID;
                                $detail->Qty = $qtyValue1;
                                $detail->Price = $priceValue1;
                                $detail->Discount = 0;
                                $detail->DiscountNominal = 0;
                                $detail->VAT = $vatValue1;
                                $detail->SubTotal = $subTotal1;
                                $detail->UserRecord = Auth::user()->UserID;
                                $detail->UserModified = Auth::user()->UserID;
                                $detail->save();
                            }
                        }
                        $total += $subTotal;
                    }
                }
            }
            SalesOrderDetail::where('SalesOrderInternalID', '=', Input::get('SalesOrderInternalID'))->where('is_deleted', 1)->delete();
            SalesOrderParcel::where('SalesOrderInternalID', '=', Input::get('SalesOrderInternalID'))->where('is_deleted', 1)->delete();
            SalesOrderDescription::where('SalesOrderInternalID', '=', Input::get('SalesOrderInternalID'))->where('is_deleted', 1)->delete();
            $messages = 'suksesUpdate';
            $error = '';
        }

        if ($messages == 'suksesUpdate') {
            return Redirect::route('salesOrderDetail', $header->SalesOrderID)->with("msg", "print");
        }
        
        //tipe
        $header = SalesOrderHeader::find($id);
        $description = SalesOrderHeader::find($id)->salesOrderDescription;
        $detail = SalesOrderHeader::find($id)->salesOrderDetail()->get();
        $salesOrder = $this->createID(0) . '.';
        return View::make('penjualanAdd.salesOrderUpdate')
                        ->withToogle('transaction')->withAktif('salesOrder')->withHeader($header)
                        ->withDetail($detail)
                        ->withDescription($description)
                        ->withSalesorder($salesOrder)->withError($error)
                        ->withMessages($messages);
    }

    public function deleteSalesOrder() {
        //dicek dulu sales dan transfer order ada ga yang pakai SO itu
        $salesOrder = DB::select(DB::raw('SELECT * FROM t_sales_header WHERE CompanyInternalID = "' . Auth::user()->CompanyInternalID . '" AND SalesOrderInternalID = "' . Input::get('InternalID') . '"'));
        $data = DB::select(DB::raw('SELECT * FROM t_shipping_header WHERE CompanyInternalID = "' . Auth::user()->CompanyInternalID . '" AND SalesOrderInternalID = "' . Input::get('InternalID') . '"'));
        $tforder = DB::select(DB::raw('SELECT * FROM t_transfer_salesorder WHERE CompanyInternalID = "' . Auth::user()->CompanyInternalID . '" AND SalesOrderInternalID = "' . Input::get('InternalID') . '"'));
        $transformation = DB::select(DB::raw('SELECT * FROM t_transformation_header WHERE CompanyInternalID = "' . Auth::user()->CompanyInternalID . '" AND SalesOrderInternalID = "' . Input::get('InternalID') . '"'));
        $topup = DB::select(DB::raw('SELECT * FROM m_topup WHERE CompanyInternalID = "' . Auth::user()->CompanyInternalID . '" AND SalesOrderInternalID = "' . Input::get('InternalID') . '"'));
        if (is_null($salesOrder) == '' && is_null($tforder) == '' && is_null($data) == '' && is_null($transformation) == '' && is_null($topup) == '') {
            //tidak ada yang menggunakan data salesOrder maka data boleh dihapus
            $salesOrderHeader = SalesOrderHeader::find(Input::get('InternalID'));
            if ($salesOrderHeader->CompanyInternalID == Auth::user()->Company->InternalID) {
                //hapus detil
                $detilData = SalesOrderHeader::find(Input::get('InternalID'))->salesOrderDetail;
                foreach ($detilData as $value) {
                    $detil = salesOrderDetail::find($value->InternalID);
                    $detil->delete();
                }
                //hapus deskripsion
                $descriptionData = SalesOrderHeader::find(Input::get('InternalID'))->salesOrderDescription;
                foreach ($descriptionData as $value) {
                    $description = salesOrderDescription::find($value->InternalID);
                    $description->delete();
                }
//                //hapus relasi pada quotation
//                $quotation = QuotationSales::where('SalesOrderInternalID',Input::get('InternalID'))->get();
//                $quotation->delete();
                //hapus sales order parcel
                SalesOrderParcel::where("SalesOrderInternalID", Input::get('InternalID'))->delete();
                //hapus quotation sales
                SalesOrderQuotation::where("SalesOrderInternalID", Input::get('InternalID'))->delete();
                //hapus salesOrder
                $salesOrder = SalesOrderHeader::find(Input::get('InternalID'));
                $soParcel = SalesOrderParcel::where("SalesOrderInternalID", $salesOrder->InternalID)->delete();
                $salesOrder->delete();
                $messages = 'suksesDelete';
            } else {
                $messages = 'accessDenied';
            }
        } else {
            //ada maka data tidak boleh dihapus
            $messages = 'gagalDelete';
        }
        $end = date('d-m-Y');
        $start = date('d-m-Y', strtotime('-7 days', strtotime($end)));
        $data = SalesOrderHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
        return View::make('penjualanAdd.salesOrderSearch')
                        ->withToogle('transaction')->withAktif('salesOrder')
                        ->withMessages($messages)
                        ->withStart($start)->withEnd($end)
                        ->withData($data);
    }

    public function approveSalesOrder() {
        //tidak ada yang menggunakan data salesOrder maka data boleh dihapus
        $salesOrderHeader = SalesOrderHeader::find(Input::get('InternalID'));
        if ($salesOrderHeader->CompanyInternalID == Auth::user()->Company->InternalID) {
            $salesOrderHeader->Status = 1;
            $salesOrderHeader->save();
            $messages = 'suksesUpdate';
        } else {
            $messages = 'accessDenied';
        }
        $data = SalesOrderHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
        $end = date('d-m-Y');
        $start = date('d-m-Y', strtotime('-7 days', strtotime($end)));
        return View::make('penjualanAdd.salesOrderSearch')
                        ->withToogle('transaction')->withAktif('salesOrder')
                        ->withMessages($messages)
                        ->withStart($start)
                        ->withEnd($end)
                        ->withData($data);
    }

    public function closeSalesOrder() {
        //tidak ada yang menggunakan data salesOrder maka data boleh dihapus
        $salesOrderHeader = SalesOrderHeader::find(Input::get('InternalID'));
        if ($salesOrderHeader->CompanyInternalID == Auth::user()->Company->InternalID) {
            $salesOrderHeader->Closed = 1;
            $salesOrderHeader->save();
            $messages = 'suksesUpdate';
        } else {
            $messages = 'accessDenied';
        }
        $end = date('d-m-Y');
        $start = date('d-m-Y', strtotime('-7 days', strtotime($end)));
        $data = SalesOrderHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
        return View::make('penjualanAdd.salesOrderSearch')
                        ->withToogle('transaction')->withAktif('salesOrder')
                        ->withMessages($messages)
                        ->withStart($start)
                        ->withEnd($end)
                        ->withData($data);
    }

    function salesOrderIncludePrint($id) {
        $id = SalesOrderHeader::getIdsalesOrder($id);
        $header = SalesOrderHeader::find($id);
        $header->Print = $header->Print + 1;
        $header->save();
//        $warehouse = Warehouse::find($header->WarehouseInternalID)->WarehouseName;
        $detail = SalesOrderHeader::find($id)->salesOrderDetail()->get();
        $description = SalesOrderDescription::where('SalesOrderInternalID', $id)->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $coa6 = SalesOrderHeader::find($header->InternalID)->coa6;
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-')
                $cp = 'CP: ' . $coa6->ContactPerson . '<br>';
            else
                $cp = '';
            $customer = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . '<br>' . $cp . $coa6->Phone . '<br>' . $coa6->Fax . '<br>' . $coa6->Email;
            if ($header->VAT == 0) {
                $vat = 'Non Tax';
            } else {
                $vat = 'Tax';
            }
            $currency = Currency::find($header->CurrencyInternalID);
            $currencyName = $currency->CurrencyName;
            if ($header->isCash == 0) {
                $payment = 'Cash';
            } else if ($header->isCash == 1) {
                $payment = 'Credit';
            } else if ($header->isCash == 2) {
                $payment = 'CBD';
            } else if ($header->isCash == 3) {
                $payment = 'Deposit';
            } else {
                $payment = 'Down Payment';
            }
            $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 6px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 195px;border-bottom:1px solid #ddd;margin-bottom:20px;">
                        <div style="height: 133px">
                        <img height="100%" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                        </div>
                        <div style="width: 420px; float: left;">
                            <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->Company->CompanyName . '</b>
                        </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Address</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Sales Order</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;padding-left:6px;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span><br>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                            <table>
                            <tr>
                            <td width="275px" style="vertical-align: text-top;">
                                <table>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Customer</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $customer . '</td>
                                 </tr>';
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-') {
                $html .= '     <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Contact Person</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->ContactPerson . '</td>
                                 </tr>';
            }
            $html .= '     <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Telp</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->Phone . '</td>
                                 </tr>                                
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->Fax . '</td>
                                 </tr>                                
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Email</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->Email . '</td>
                                 </tr>         ';
            if ($header->isCash != 0) {
                $html .= '<tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Due Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime("+" . $header->LongTerm . " day", strtotime($header->SalesOrderDate))) . '</td>
                                 </tr>';
            }
            $html .= '
                                </table>
                                </td>
                                <td style="vertical-align: text-top;">
                                <table>
                                <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Order ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->SalesOrderID . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->SalesOrderDate)) . '</td>
                                 </tr>
                                <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">PO Customer</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->POCustomer . '</td>
                                     </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Currency</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $currencyName . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Rate</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . number_format($header->CurrencyRate, '2', '.', ',') . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Vat</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $vat . '</td>
                                     </tr>
                                </table></td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">No.</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="25%">Inventory</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Uom</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Qty</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Price</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc (%)</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            $total = 0;
            $counter = 1;
            $price = 0;
            $discount = 0;
            $subtotal = 0;
            if (count($description) > 0) {
                foreach ($description as $data) {
                    if ($header->VAT == 1) {
                        $price = $data->Price + ($data->Price * 0.1);
                        $discount = $data->DiscountNominal + ($data->DiscountNominal * 0.1);
                    } else {
                        $price = $data->Price;
                        $discount = $data->DiscountNominal;
                    }
                    $subtotal = ($price - $discount) * $data->Qty;
                    $subtotal = $subtotal - (($data->Discount / 100) * $subtotal);
                    $html .= '<tr>
                                <td rowspan="' . (1 + ($data->Spesifikasi != '' ? 1 : 0)) . '" style="vertical-align: top;font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $counter . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $data->InventoryText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->UomText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($discount, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($subtotal, '2', '.', ',') . '</td>
                            </tr>';
                    if ($data->Spesifikasi != '') {
                        $html .= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . nl2br($data->Spesifikasi) . '</td>
                            </tr>';
                    }
                    $total += $subtotal;
                    $counter++;
                }
            } else {
                $html .= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this salesOrder.</td>
                        </tr>';
            }
            $html .= '</tbody>
                            </table>
                            
                            <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                                <table>
                                <tr>
                                <td width="275px" style="vertical-align: text-top;">
                            <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">Delivery Terms</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $header->DeliveryTerms . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">Delivery Time</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $header->DeliveryTime . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">Terms Of Payment</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $payment . '</td>
                                 </tr>
                                  <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">Remark</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $header->Remark . '</td>
                                 </tr>
                                </table>
                                    </td>
                                    <td width="200px" style="float:right;vertical-align: text-top;">
                                     <table style="margin-left:30px;">
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Discount</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Grand Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total - $header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                </table>
                                    </td>
                                    </tr>
                                </table>
                            </div>
                            
                            <table>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>                            
                            <tr style="background-color:none;">
                                <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Hormat Kami, </td>
                                <td style="text-align: center;width:100px;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;"><br/></td>
                                <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Disetujui oleh, </td>
                            </tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr>
                            <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">(  ' . Auth::user()->UserName . '  )</td>
                            <td style="text-align: center;width:100px;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;"><br/></td>
                            <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">(.........................)</td>
                            </tr>
                            </table>
                    </div>
                </body>
            </html>';
            //return PDF ::load($html, 'A5', 'portrait')->show();

            return View::make('template.print.salesOrderIncludePrint')
                            ->with('header', $header)
                            ->with('customer', $customer)
                            ->with('vat', $vat)
                            ->with('id', $id)
                            ->with('description', $description)
                            ->with('currencyName', $currencyName)
                            ->with('payment', $payment)
                            ->with('detail', $detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showSalesOrder');
        }
    }

    function invoiceUMPrint($id) {
        $id = SalesOrderHeader::getIdsalesOrder($id);
        $header = SalesOrderHeader::find($id);
        $header->Print = $header->Print + 1;
        $header->save();
        $detail = SalesOrderHeader::find($id)->salesOrderDetail()->get();
        $description = SalesOrderDescription::where('SalesOrderInternalID', $id)->get();
//        $warehouse = Warehouse::find($header->WarehouseInternalID)->WarehouseName;
        $coa6 = SalesOrderHeader::find($header->InternalID)->coa6;
        $namacustomer = $coa6->ACC6Name;
        if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-')
            $cp = 'CP: ' . $coa6->ContactPerson . '<br>';
        else
            $cp = '';
        $customer = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . '<br>' . $cp . $coa6->Phone . '<br>' . $coa6->Fax . '<br>' . $coa6->Email;
        if ($header->VAT == 0) {
            $vat = 'Non Tax';
        } else {
            $vat = 'Tax';
        }
        $currency = Currency::find($header->CurrencyInternalID);
        $currencyName = $currency->CurrencyName;
        if ($header->isCash == 0) {
            $payment = 'Cash';
        } else if ($header->isCash == 1) {
            $payment = 'Credit';
        } else if ($header->isCash == 2) {
            $payment = 'CBD';
        } else if ($header->isCash == 3) {
            $payment = 'Deposit';
        } else {
            $payment = 'Down Payment';
        }

        return View::make('template.print.invoiceUMPrint')
                        ->with('header', $header)
                        ->with('customer', $customer)
                        ->with('namacustomer', $namacustomer)
                        ->with('vat', $vat)
                        ->with('id', $id)
                        ->with('description', $description)
                        ->with('currencyName', $currencyName)
                        ->with('payment', $payment)
                        ->with('detail', $detail);
    }

    function salesOrderPrint($id) {
        $id = SalesOrderHeader::getIdsalesOrder($id);
        $header = SalesOrderHeader::find($id);
        $header->Print = $header->Print + 1;
        $header->save();
        $detail = SalesOrderHeader::find($id)->salesOrderDetail()->get();
        $description = SalesOrderDescription::where('SalesOrderInternalID', $id)->get();
//        $warehouse = Warehouse::find($header->WarehouseInternalID)->WarehouseName;
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $coa6 = SalesOrderHeader::find($header->InternalID)->coa6;
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-')
                $cp = 'CP: ' . $coa6->ContactPerson . '<br>';
            else
                $cp = '';
            $customer = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . '<br>' . $cp . $coa6->Phone . '<br>' . $coa6->Fax . '<br>' . $coa6->Email;
            if ($header->VAT == 0) {
                $vat = 'Non Tax';
            } else {
                $vat = 'Tax';
            }
            $currency = Currency::find($header->CurrencyInternalID);
            $currencyName = $currency->CurrencyName;
            if ($header->isCash == 0) {
                $payment = 'Cash';
            } else if ($header->isCash == 1) {
                $payment = 'Credit';
            } else if ($header->isCash == 2) {
                $payment = 'CBD';
            } else if ($header->isCash == 3) {
                $payment = 'Deposit';
            } else {
                $payment = 'Down Payment';
            }
            $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 6px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 195px;border-bottom:1px solid #ddd;margin-bottom:20px;">
                        <div style="height: 133px">
                        <img height="100%" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                        </div>
                        <div style="width: 420px; float: left;">
                            <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->Company->CompanyName . '</b>
                        </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Address</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Sales Order</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;padding-left:6px;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span><br>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                            <table>
                            <tr>
                            <td width="275px" style="vertical-align: text-top;">
                                <table>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Customer</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $customer . '</td>
                                 </tr>';
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-') {
                $html .= '     <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Contact Person</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->ContactPerson . '</td>
                                 </tr>';
            }
            $html .= '     <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Telp</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->Phone . '</td>
                                 </tr>                                
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->Fax . '</td>
                                 </tr>                                
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Email</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->Email . '</td>
                                 </tr>         ';
            if ($header->isCash != 0) {
                $html .= '<tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Due Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime("+" . $header->LongTerm . " day", strtotime($header->SalesOrderDate))) . '</td>
                                 </tr>';
            }
            $html .= '
                                </table>
                                </td>
                                <td style="vertical-align: text-top;">
                                <table>
                                <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Order ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->SalesOrderID . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->SalesOrderDate)) . '</td>
                                 </tr>
                                <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">PO Customer</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->POCustomer . '</td>
                                     </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Currency</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $currencyName . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Rate</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . number_format($header->CurrencyRate, '2', '.', ',') . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Vat</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $vat . '</td>
                                     </tr>
                                </table></td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">No.</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="25%">Inventory</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Uom</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Qty</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Price</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc (%)</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            $total = 0;
            $totalVAT = 0;
            $counter = 1;
            if (count($description) > 0) {
                foreach ($description as $data) {
                    $html .= '<tr>
                                <td rowspan="' . (1 + ($data->Spesifikasi != '' ? 1 : 0)) . '" style="vertical-align: top;font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $counter . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $data->InventoryText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->UomText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
                    if ($data->Spesifikasi != '') {
                        $html .= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . nl2br($data->Spesifikasi) . '</td>
                            </tr>';
                    }
                    $totalVAT += $data->VAT;
                    $total += $data->SubTotal;
                    $counter++;
                }
                if ($totalVAT != 0) {
                    $totalVAT = $totalVAT - ($header->DiscountGlobal * 0.1);
                }
            } else {
                $html .= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this salesOrder.</td>
                        </tr>';
            }
            $html .= '</tbody>
                            </table>
                            
                            <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                                <table>
                                <tr>
                                <td width="275px" style="vertical-align: text-top;">
                            <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">Delivery Terms</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $header->DeliveryTerms . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">Delivery Time</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $header->DeliveryTime . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">Terms Of Payment</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $payment . '</td>
                                 </tr>
                                  <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">Remark</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $header->Remark . '</td>
                                 </tr>
                                </table>
                                    </td>
                                    <td width="200px" style="float:right;vertical-align: text-top;">
                                     <table style="margin-left:30px;">
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Discount</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Grand Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total - $header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Tax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($totalVAT, '2', '.', ',') . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; ">Grand Total (Tax)</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">'
                    . number_format($total - $header->DiscountGlobal + $totalVAT, '2', '.', ',') . '</td>
                                 </tr>
                                </table>
                                    </td>
                                    </tr>
                                </table>
                            </div>
                            
                            <table>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>                            
                            <tr style="background-color:none;">
                                <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Hormat Kami, </td>
                                <td style="text-align: center;width:100px;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;"><br/></td>
                                <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Disetujui oleh, </td>
                            </tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr>
                            <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">(  ' . Auth::user()->UserName . '  )</td>
                            <td style="text-align: center;width:100px;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;"><br/></td>
                            <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">(.........................)</td>
                            </tr>
                            </table>
                    </div>
                </body>
            </html>';
            //return PDF ::load($html, 'A5', 'portrait')->show();

            return View::make('template.print.salesOrderPrint')
                            ->with('header', $header)
                            ->with('customer', $customer)
                            ->with('vat', $vat)
                            ->with('id', $id)
                            ->with('description', $description)
                            ->with('currencyName', $currencyName)
                            ->with('payment', $payment)
                            ->with('detail', $detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showSalesOrder');
        }
    }

    function salesOrderPrintSJ($id) {
        $id = SalesOrderHeader::getIdsalesOrder($id);
        $header = SalesOrderHeader::find($id);
        $detail = SalesOrderHeader::find($id)->salesOrderDetail()->get();
        $description = SalesOrderDescription::where('SalesOrderInternalID', $id)->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $coa6 = SalesOrderHeader::find($header->InternalID)->coa6;
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-')
                $cp = 'CP: ' . $coa6->ContactPerson . '<br>';
            else
                $cp = '';
            $customer = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . '<br>' . $cp . $coa6->Phone . '<br>' . $coa6->Fax . '<br>' . $coa6->Email;
            if ($header->isCash == 0) {
                $payment = 'Cash';
            } else {
                $payment = 'Credit';
            }
            $currency = Currency::find($header->CurrencyInternalID);
            $currencyName = $currency->CurrencyName;
            if ($header->VAT == 0) {
                $vat = 'Non Tax';
            } else {
                $vat = 'Tax';
            }
            $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 6px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 195px;border-bottom:1px solid #ddd;margin-bottom:20px;">
                        <div style="height: 133px">
                        <img height="100%" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                        </div>
                        <div style="width: 420px; float: left;">
                            <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->Company->CompanyName . '</b>
                        </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Address</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Sales Order</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;padding-left:6px;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span><br>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                            <table>
                            <tr>
                            <td width="275px" style="vertical-align: text-top;">
                                <table>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Customer</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $customer . '</td>
                                 </tr>';
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-') {
                $html .= '     <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Contact Person</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->ContactPerson . '</td>
                                 </tr>';
            }
            $html .= '     <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Telp</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->Phone . '</td>
                                 </tr>                                
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->Fax . '</td>
                                 </tr>                                
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Email</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->Email . '</td>
                                 </tr>         ';
            if ($header->isCash != 0) {
                $html .= '<tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Due Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime("+" . $header->LongTerm . " day", strtotime($header->SalesOrderDate))) . '</td>
                                 </tr>';
            }
            $html .= '
                                </table>
                                </td>
                                <td style="vertical-align: text-top;">
                                <table>
                                <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Order ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->SalesOrderID . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->SalesOrderDate)) . '</td>
                                 </tr>
                                <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">PO Customer</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->POCustomer . '</td>
                                     </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Currency</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $currencyName . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Rate</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . number_format($header->CurrencyRate, '2', '.', ',') . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Vat</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $vat . '</td>
                                     </tr>
                                </table></td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">No.</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="25%">Inventory</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Uom</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Qty</th>
                                        </tr>
                                    </thead>
                                    <tbody>';

            $counter = 1;
            if (count($description) > 0) {
                foreach ($description as $data) {
                    $html .= '<tr>
                                <td rowspan="' . (1 + ($data->Spesifikasi != '' ? 1 : 0)) . '" style="vertical-align: top;font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $counter . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $data->InventoryText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->UomText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                            </tr>';
                    if ($data->Spesifikasi != '') {
                        $html .= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . nl2br($data->Spesifikasi) . '</td>
                            </tr>';
                    }
                    $counter++;
                }
            } else {
                $html .= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this salesOrder.</td>
                        </tr>';
            }
            $html .= '</tbody>
                            </table>
                            
                            <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                                <table>
                                <tr>
                                <td width="275px" style="vertical-align: text-top;">
                            <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">Delivery Terms</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $header->DeliveryTerms . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">Delivery Time</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $header->DeliveryTime . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">Terms Of Payment</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $payment . '</td>
                                 </tr>
                                  <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">Remark</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $header->Remark . '</td>
                                 </tr>
                                </table>
                                    </td>
                                   
                                    </tr>
                                </table>
                            </div>
                            
                            <table>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>                            
                            <tr style="background-color:none;">
                                <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Hormat Kami, </td>
                                <td style="text-align: center;width:100px;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;"><br/></td>
                                <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Disetujui oleh, </td>
                            </tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr>
                            <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">(  ' . Auth::user()->UserName . '  )</td>
                            <td style="text-align: center;width:100px;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;"><br/></td>
                            <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">(.........................)</td>
                            </tr>
                            </table>
                    </div>
                </body>
            </html>';
//            return PDF ::load($html, 'A5', 'portrait')->show();

            return View::make('template.print.salesOrderSJPrint')
                            ->with('header', $header)
                            ->with('customer', $customer)
                            ->with('vat', $vat)
                            ->with('id', $id)
                            ->with('description', $description)
                            ->with('currencyName', $currencyName)
                            ->with('payment', $payment)
                            ->with('detail', $detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showSalesOrder');
        }
    }

    function salesOrderInternalPrint($id) {
        $id = SalesOrderHeader::getIdsalesOrder($id);
        $header = SalesOrderHeader::find($id);
        $detail = SalesOrderHeader::find($id)->salesOrderDetail()->get();
        $description = SalesOrderDescription::where('SalesOrderInternalID', $id)->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $coa6 = SalesOrderHeader::find($header->InternalID)->coa6;
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-')
                $cp = 'CP: ' . $coa6->ContactPerson . '<br>';
            else
                $cp = '';
            $customer = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . '<br>' . $cp . $coa6->Phone . '<br>' . $coa6->Fax . '<br>' . $coa6->Email;
            if ($header->isCash == 0) {
                $payment = 'Cash';
            } else if ($header->isCash == 1) {
                $payment = 'Credit';
            } else if ($header->isCash == 2) {
                $payment = 'CBD';
            } else if ($header->isCash == 3) {
                $payment = 'Deposit';
            } else {
                $payment = 'Down Payment';
            }
            $currency = Currency::find($header->CurrencyInternalID);
            $currencyName = $currency->CurrencyName;
            if ($header->VAT == 0) {
                $vat = 'Non Tax';
            } else {
                $vat = 'Tax';
            }
            $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 6px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 70px;border-bottom:1px solid #ddd;margin-bottom:20px;">
                        <div style="width: 420px; float: left;">
                            <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->Company->CompanyName . '</b>
                        </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Address</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Sales Order</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;padding-left:6px;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span><br>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                            <table>
                            <tr>
                            <td width="275px" style="vertical-align: text-top;">
                                <table>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Customer</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $customer . '</td>
                                 </tr>';
            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-') {
                $html .= '     <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Contact Person</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->ContactPerson . '</td>
                                 </tr>';
            }
            $html .= '     <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Telp</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->Phone . '</td>
                                 </tr>                                
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->Fax . '</td>
                                 </tr>                                
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Email</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->Email . '</td>
                                 </tr>         ';
            if ($header->isCash != 0) {
                $html .= '<tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Due Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime("+" . $header->LongTerm . " day", strtotime($header->SalesOrderDate))) . '</td>
                                 </tr>';
            }
            $html .= '
                                </table>
                                </td>
                                <td style="vertical-align: text-top;">
                                <table>
                                <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Order ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->SalesOrderID . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->SalesOrderDate)) . '</td>
                                 </tr>
                                <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">PO Customer</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->POCustomer . '</td>
                                     </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Currency</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $currencyName . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Rate</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . number_format($header->CurrencyRate, '2', '.', ',') . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Vat</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $vat . '</td>
                                     </tr>
                                </table></td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">No.</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="25%">Inventory</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Uom</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Qty</th>
                                        </tr>
                                    </thead>
                                    <tbody>';

            $counter = 1;
            if (count($description) > 0) {
                foreach ($description as $data) {
                    $detail = SalesOrderDetail::where('DescriptionInternalID', $data->InternalID)->where('SalesOrderParcelInternalID', 0)->get();
                    $parcel = SalesOrderParcel::where('DescriptionInternalID', $data->InternalID)->get();
                    $html .= '<tr>
                                <td rowspan="' . (1 + ($data->Spesifikasi != '' ? 1 : 0) + count($detail) + count($parcel)) . '" style="vertical-align: top;font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $counter . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $data->InventoryText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->UomText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>

                            </tr>';
                    if ($data->Spesifikasi != '') {
                        $html .= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . nl2br($data->Spesifikasi) . '</td>
                            </tr>';
                    }
                    foreach ($detail as $data2) {
                        $html .= '<tr>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;"><i>' . $data2->inventory->InventoryName . '</i></td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data2->uom->UomID . '</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data2->Qty, 0, '.', ',') . '</td>
                                </tr>';
                    }
                    foreach ($parcel as $data2) {
                        $html .= '<tr>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;"><i>' . $data2->parcel->ParcelName . '</i></td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">-</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data2->Qty, 0, '.', ',') . '</td>
                                </tr>';
                    }
                    $counter++;
                }
            } else {
                $html .= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this salesOrder.</td>
                        </tr>';
            }
            $html .= '</tbody>
                            </table>
                            
                            <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                                <table>
                                <tr>
                                <td width="275px" style="vertical-align: text-top;">
                            <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">Delivery Terms</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $header->DeliveryTerms . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">Delivery Time</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $header->DeliveryTime . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">Terms Of Payment</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $payment . '</td>
                                 </tr>
                                  <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">Remark</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;vertical-align: text-top;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $header->Remark . '</td>
                                 </tr>
                                </table>
                                    </td>
                                    
                                    </tr>
                                </table>
                            </div>
                            
                            <table>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>                            
                            <tr style="background-color:none;">
                                <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Hormat Kami, </td>
                                <td style="text-align: center;width:100px;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;"><br/></td>
                                <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Disetujui oleh, </td>
                            </tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr>
                            <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">(  ' . Auth::user()->UserName . '  )</td>
                            <td style="text-align: center;width:100px;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;"><br/></td>
                            <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">(.........................)</td>
                            </tr>
                            </table>
                    </div>
                </body>
            </html>';
            //return PDF ::load($html, 'A5', 'portrait')->show();

            return View::make('template.print.salesOrderInternalPrint')
                            ->with('header', $header)
                            ->with('customer', $customer)
                            ->with('vat', $vat)
                            ->with('id', $id)
                            ->with('description', $description)
                            ->with('currencyName', $currencyName)
                            ->with('payment', $payment)
                            ->with('detail', $detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showSalesOrder');
        }
    }

    function createID($tipe, $tax = null) {
        if ($tax == "Tax")
            $salesOrder = 'SO';
        elseif ($tax == "NonTax")
            $salesOrder = 'SON';
        else
            $salesOrder = 'SO';

        if ($tipe == 0) {
            $salesOrder .= '.' . date('m') . date('y');
        }
        return $salesOrder;
    }

    public function formatCariIDSalesOrder() {
        $date = explode('-', Input::get('date'));
        $id = $this->createID(1);
        $yearDigit = substr($date[2], 2);
        $id .= '.' . $date[1] . $yearDigit . '.';
        echo SalesOrderHeader::getNextIDSalesOrder($id);
    }

    public function summarySalesOrder() {
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $totalSO = 0;
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <!--<div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>-->
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Sales Order Summary Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span>
                            <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span>';
        $hitung = 0;
        foreach (Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $dataCustomer) {
            if (SalesOrderHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                            ->where('ACC6InternalID', $dataCustomer->InternalID)
                            ->where("VAT", "!=", Auth::user()->SeeNPPN)
                            ->whereBetween('SalesOrderDate', Array($start, $end))->count() > 0) {
                $html .= '<table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=7>' . $dataCustomer->ACC6Name . '</th>
                                        </tr>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales Order ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Currency</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Rate</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total (After Discount)</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Vat</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Grand Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                $sumGrandTotal = 0;
                foreach (SalesOrderHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where('ACC6InternalID', $dataCustomer->InternalID)
                        ->where("VAT", "!=", Auth::user()->SeeNPPN)
                        ->whereBetween('SalesOrderDate', Array($start, $end))->get() as $data) {
                    $grandTotal = $data->GrandTotal;
                    $sumGrandTotal += $grandTotal;
                    $totalSO += $grandTotal;
                    $total = $grandTotal;
                    $vat = 0;
                    if ($data->VAT == 1) {
                        $total = $total * 10 / 11;
                        $vat = $total / 10;
                    }
                    $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->SalesOrderID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->SalesOrderDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->Currency->CurrencyName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->CurrencyRate, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format(ceil($total), '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format(floor($vat), '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($grandTotal, '2', '.', ',') . '</td>
                            </tr>';
                }
                $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="80%" colspan="6">Total : </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . number_format($sumGrandTotal, '2', '.', ',') . '</td>
                            </tr>';
                $html .= '</tbody>
            </table>';

                $html .= '</tbody>
            </table>';
                $hitung++;
            }
        }

        if ($hitung == 0) {
            $html .= '<br><span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no sales order.</span>';
        }
        $html .= '<table class="tableBorder" width="100%"  style="margin-top: 10px; clear: both;  top: 50px;">
                    <thead>
                        <tr>
                            <th style="font-family: helvetica,sans-serif;font-size: 14px; margin: 5px !important; font-weight: 700; text-align: right" width="80%" colspan="6">Total Sales Order : </th>
                            <th style="font-family: helvetica,sans-serif;font-size: 14px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . number_format($totalSO, '2', '.', ',') . '</th>
                        </tr>
                    <thead>
                </table>';
        $html .= '</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('sales_order_summary');
    }

    public function uncomplateSalesOrder() {
        $totalSO = 0;
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <!--<div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>-->
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Uncomplate Sales Order Report</h5>
                            <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span>';
        foreach (Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->CompanyInternalID)->get() as $dataCustomer) {
            if (SalesOrderHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)
                            ->where('ACC6InternalID', $dataCustomer->InternalID)
                            ->where("VAT", "!=", Auth::user()->SeeNPPN)
                            ->count() > 0) {
                $so = SalesOrderHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)
                        ->where('ACC6InternalID', $dataCustomer->InternalID)
                        ->where("VAT", "!=", Auth::user()->SeeNPPN)
                        ->first();
                if (checkSalesAdd($so->InternalID)) {
                    $html .= '<table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=7>' . $dataCustomer->ACC6Name . '</th>
                                        </tr>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales Order ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Currency</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Rate</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total (After Discount)</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Vat</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Grand Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                    $sumGrandTotal = 0;
                    foreach (SalesOrderHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)
                            ->where('ACC6InternalID', $dataCustomer->InternalID)
                            ->where("VAT", "!=", Auth::user()->SeeNPPN)
                            ->get() as $data) {
                        if (checkSalesAdd($data->InternalID)) {
                            $grandTotal = $data->GrandTotal;
                            $sumGrandTotal += $grandTotal;
                            $totalSO += $grandTotal;
                            $total = $grandTotal;
                            $vat = 0;
                            if ($data->VAT == 1) {
                                $total = $total * 10 / 11;
                                $vat = $total / 10;
                            }
                            $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->SalesOrderID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->SalesOrderDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->Currency->CurrencyName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->CurrencyRate, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($total, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($vat, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($grandTotal, '2', '.', ',') . '</td>
                            </tr>';
                        }
                    }
                    $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="80%" colspan="6">Total : </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . number_format($sumGrandTotal, '2', '.', ',') . '</td>
                            </tr>';
                    $html .= '</tbody>
            </table>';
                }
            }
        }
        $html .= '<table class="tableBorder" width="100%"  style="margin-top: 10px; clear: both;  top: 50px;">
                    <thead>
                        <tr>
                            <th style="font-family: helvetica,sans-serif;font-size: 14px; margin: 5px !important; font-weight: 700; text-align: right" width="80%" colspan="6">Total Sales Order : </th>
                            <th style="font-family: helvetica,sans-serif;font-size: 14px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . number_format($totalSO, '2', '.', ',') . '</th>
                        </tr>
                    <thead>
                </table>';
        $html .= '</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('uncomplate_sales_order');
    }

    public function detailSalesOrder() {
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $dateTamp = '';
        $coa6Tamp = '';
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <!--<div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>-->
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Sales Order Detail Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span>
                            <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span> <br><br>';
        if (SalesOrderHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                        ->whereBetween('SalesOrderDate', Array($start, $end))
                        ->where("VAT", "!=", Auth::user()->SeeNPPN)
                        ->orderBy('SalesOrderDate')->orderBy('ACC6InternalID')->count() > 0) {
            foreach (SalesOrderHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                    ->whereBetween('SalesOrderDate', Array($start, $end))
                    ->where("VAT", "!=", Auth::user()->SeeNPPN)
                    ->orderBy('SalesOrderDate')->orderBy('ACC6InternalID')->get() as $dataPenjualan) {
                if ($dateTamp != date("d-M-Y", strtotime($dataPenjualan->SalesOrderDate))) {
                    $html .= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales Order Date : ' . date("d-M-Y", strtotime($dataPenjualan->SalesOrderDate)) . '</span><br><br>';
                    $dateTamp = date("d-M-Y", strtotime($dataPenjualan->SalesOrderDate));
                    $coa6Tamp = '';
                }
                if ($coa6Tamp != $dataPenjualan->ACC6InternalID) {
                    $html .= '<span style="padding-left:23px;font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Customer : ' . $dataPenjualan->coa6->ACC6Name . '</span>';
                    $coa6Tamp = $dataPenjualan->ACC6InternalID;
                }
                if ($dataPenjualan->coa6->ContactPerson != null && $dataPenjualan->coa6->ContactPerson != '-' && $dataPenjualan->coa6->ContactPerson != '') {
                    $html .= '<span style="padding-left:23px;font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Contact Person : ' . $dataPenjualan->coa6->ContactPerson . '</span>';
                }
                $html .= '<table class="tableBorder" style="width:95%; margin-top: 18px; clear: both; position: relative; left: 4%">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=8>' . $dataPenjualan->SalesOrderID . ' | ' . $dataPenjualan->Currency->CurrencyName . ' | Rate : ' . number_format($dataPenjualan->CurrencyRate, '2', '.', ',') . ' </th>
                                        </tr>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory ID</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Name</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Uom</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Qty</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Price</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc (%)</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                $total = 0;
                $vat = 0;
                foreach ($dataPenjualan->salesOrderDetail as $data) {
                    if ($data->SalesOrderParcelInternalID == 0) {
                        $total += ceil($data->SubTotal);
                        $vat += floor(ceil($data->SubTotal) / 10);
//                        $vat += $data->VAT;
                        $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->inventory->InventoryID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->inventory->InventoryName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->Uom->UomID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format(ceil($data->SubTotal), '2', '.', ',') . '</td>
                            </tr>';
                    }
                }
                foreach (SalesOrderParcel::where("SalesOrderInternalID", $dataPenjualan->InternalID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->get() as $data) {
                    //jika parcel
                    $total += ceil($data->SubTotal);
                    $vat += floor(ceil($data->SubTotal) / 10);
//                    $vat += $data->VAT;
                    $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $data->Parcel->ParcelID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $data->Parcel->ParcelName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right"> - </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format(ceil($data->SubTotal), '2', '.', ',') . '</td>
                            </tr>';
                }
                if ($vat != 0) {
                    $vat = $vat - ($dataPenjualan->DiscountGlobal * 0.1);
                }
                $html .= '<tr>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=5></td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=2>Total <br> Discount <br> Grand Total <br> VAT <br> Grand Total (Tax)</td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '<br>'
                        . '' . number_format($dataPenjualan->DiscountGlobal, '2', '.', ',') . '<br>'
                        . '' . number_format($total - $dataPenjualan->DiscountGlobal, '2', '.', ',') . '<br>'
                        . '' . number_format($vat, '2', '.', ',') . '<br>'
                        . '' . number_format($dataPenjualan->GrandTotal, '2', '.', ',') . '</td>
                    </tr>
                </tbody>
            </table>';
            }
        } else {
            $html .= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no sales order.</span><br><br>';
        }
        $html .= '</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('sales_order_detail');
    }

    //=================ajax=====================
    public function getUomThisInventorySalesOrder() {
        $inventoryUom = InventoryUom::where("InventoryInternalID", Input::get("id"))->get();
        foreach ($inventoryUom as $data) {
            ?>
            <option value="<?php echo $data->UomInternalID ?>" <?php echo ($data->Default == 1 ? "selected" : "") ?>><?php echo $data->Uom->UomID; ?></option>
            <?php
        }
    }

    public function getPriceUomThisInventory() {
        $inventoryInternalID = Input::get("InventoryID");
        $uomInternalID = Input::get("UomID");
        $InventoryPrice = InventoryUom::where("InventoryInternalID", $inventoryInternalID)
                        ->where("UomInternalID", $uomInternalID)->first();
        return $InventoryPrice->Price . '---;---' . Input::get("urutan");
    }

    public function getPriceRangeThisInventorySO() {
        $hasil = getPriceRangeInventory(Input::get("InventoryID"), Input::get("UomID"), Input::get("Qty"));
        return $hasil . '---;---' . Input::get("urutan");
    }

    public function getPriceThisParcel() {
        $InternalID = Input::get("id");
        $parcelPrice = Parcel::find($InternalID)->GrandTotal;
        return $parcelPrice;
    }

    public function checkRecieveable() {
        $total = Input::get("total");
        $coa6 = Input::get("coa6");
        $sisaHutang = 0;
        $balance = 0;
        //pikirkan currency
        //hasil dari ini adalah headernya saja 
        $detail = SalesOrderHeader::getSalesReceivable($coa6);
        foreach ($detail as $data) {
            $balance += ($data->GrandTotal - JournalDetail::where('JournalTransactionID', $data->ID)->sum('JournalCreditMU')) * $data->CurrencyRate;
        }

        $creditLimitCustomer = Coa6::find($coa6)->CreditLimit;
        $sisaHutang = $creditLimitCustomer - $balance;
        if ($sisaHutang < $total) {
            return '1' . '---;---' . $creditLimitCustomer . '---;---' . abs($sisaHutang);
        } else {
            return '0' . '---;---0---;---0';
        }
    }

    public function getSearchResultInventoryForSO() {
        $idnih = "%" . explode(" ", Input::get("id"))[0] . "%";
        $pass = str_replace(explode(" ", Input::get("id"))[0], '', Input::get('id'));
        $input = splitSearchValue($pass);
//        $input = splitSearchValue(Input::get("id"));
        $dataInventory = Inventory::select('m_inventory.*')->distinct()
                ->join('m_inventory_uom', 'm_inventory_uom.InventoryInternalID', '=', 'm_inventory.InternalID')
                ->where("m_inventory.Status", 0)
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where(function($query) use ($idnih, $input) {
                    $query->where("m_inventory.InventoryID", "like", $idnih)
                    ->where("m_inventory.InventoryName", "like", $input)
                    ->orWhere("m_inventory.InventoryName", "like", $idnih);
                })
                ->where("m_inventory.Status", 0)
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->get();

        $dataParcel = Parcel::where("CompanyInternalID", Auth::user()->Company->InternalID)
                ->where("ParcelID", "like", $input)
                ->orWhere("ParcelName", "like", $input)
                ->get();

        if (count($dataInventory) == 0 && count($dataParcel) == 0) {
            ?>
            <span>Inventory with keywords <i><u><?php echo Input::get("id"); ?></u></i> can't be found</span>
            <?php
        } else {
            ?>
            <select class="input-theme left inventory" id="inventory-0" data-toggle="popover" data-placement="bottom" data-html="true">
                <?php
                foreach ($dataInventory as $inventory) {
                    ?>
                    <option id="inventory<?php echo $inventory->InternalID ?>" value="<?php echo $inventory->InternalID ?>---;---inventory">
                        <?php echo $inventory->InventoryID . ' ' . $inventory->InventoryName . ' (power: ' . $inventory->Power . ')'; ?>
                    </option>
                    <?php
                }
                ?>
                <?php
                foreach ($dataParcel as $parcel) {
                    ?>
                    <!--foreach untuk paketnya-->
                    <option id="parcel<?php echo $parcel->InternalID ?>" value="<?php echo $parcel->InternalID ?>---;---parcel">
                        <?php echo $parcel->ParcelID . " " . $parcel->ParcelName ?>
                    </option>
                    <!--foreach untuk paketnya-->
                    <?php
                }
                ?>
            </select>
            <script type="text/javascript" src="<?php echo Asset('js/entry-js-controller/controllerSO.js') ?>"></script>
            <?php
        }
    }

    public function getStockInventorySO() {
        $tooltip = "";
        $tamp = explode("---;---", Input::get("InventoryInternalID"));
        $inventorySimilarity = InventorySimilarity::where("CompanyInternalID", Auth::user()->Company->InternalID)->where("InventoryInternalID", $tamp[0])->get();

        if (count($inventorySimilarity) > 0) {
            foreach ($inventorySimilarity as $data) {
                $tooltip .= $data->Inventory->InventoryID . ' ' . $data->Inventory->InventoryName . ' <br/> ';
            }
        } else {
            $tooltip = "-";
        }
        return getEndStockInventory($tamp[0]) . '---;---' . $tooltip;
    }

    public function getStockInventorySO2() {
        $tooltip = "";
        $tamp = explode("---;---", Input::get("InventoryInternalID"));
        $tamp2 = Input::get("warehouse");
        $inventorySimilarity = InventorySimilarity::where("CompanyInternalID", Auth::user()->Company->InternalID)->where("InventoryInternalID", $tamp[0])->get();

        if (count($inventorySimilarity) > 0) {
            foreach ($inventorySimilarity as $data) {
                $tooltip .= $data->Inventory->InventoryID . ' ' . $data->Inventory->InventoryName . ' <br/> ';
            }
        } else {
            $tooltip = "-";
        }
        return getEndStockInventoryWithWarehouse($tamp[0], $tamp2) . '---;---' . $tooltip;
    }

    public function getPriceRangeSO() {
        $tamp = explode("---;---", Input::get("InventoryInternalID"));
        return getInformationPriceRangeInventory($tamp[0]);
    }

    //================/ajax=====================
    //=============================export excel==================================
    public function exportSODariFile() {
        Excel::load('file_excel/TRANS JUAL HEADER 1.xlsx', function($reader) {

            // Getting all results
            $results = $reader->get();
            $str = "";
            foreach ($results as $datas) {
                foreach ($datas as $data) {
                    $countCoa6 = Coa6::where("ACC6ID", $data->kode_cust)->count();
                    if ($countCoa6 == 0) {
                        //gak ada
                        $coa6 = new Coa6();
                        $coa6->ACC6ID = $data->kode_cust;
                        $coa6->ACC6Name = $data->nama_cust;
                        $coa6->Email = $data->kode_cust . "@gmail.com";
                        $coa6->UserRecord = "makmur";
                        $coa6->Type = "c";
                        $coa6->CompanyInternalID = 1;
                        $coa6->Remark = "";
                        $coa6->save();
                    } else {
                        $coa6 = Coa6::where("ACC6ID", $data->kode_cust)->first();
                    }

                    $countS0 = SalesOrderHeader::where("SalesOrderID", $data->nomer_jual)->count();
                    if ($countS0 == 0) {
                        $soHeader = new SalesOrderHeader();
                        $soHeader->SalesOrderID = $data->nomer_jual;
                        $soHeader->SalesOrderDate = $data->tgl_jual;
                        $soHeader->ACC6InternalID = $coa6->InternalID;
                        $soHeader->LongTerm = $data->term;
                        $soHeader->isCash = ($data->tunai == "TRUE" ? 0 : 1);
                        $soHeader->CurrencyInternalID = 5;
                        $soHeader->CurrencyRate = 1;
                        $soHeader->VAT = 0;
                        $soHeader->DiscountGlobal = $data->total_disc;
                        $soHeader->GrandTotal = $data->total_nota;
                        $soHeader->WarehouseInternalID = 1;
                        $soHeader->dtRecord = $data->tgl_entry;
                        $soHeader->UserRecord = "makmur";
                        $soHeader->CompanyInternalID = 1;
                        $soHeader->Remark = ($data->keterangan == NULL ? "-" : $data->keterangan);
                        $soHeader->save();
                    }
                }
            }
        });
        echo "selesai";
    }

    public function exportSODetailDariFile() {
        Excel::load('file_excel/TRANS JUAL DETAIL 1.xlsx', function($reader) {

            // Getting all results
            $results = $reader->get();
//            foreach ($results as $datas) {
            foreach ($results as $data) {
                //cari header InternalIDnya

                $SOHeader = SalesOrderHeader::where("SalesOrderID", $data->nomer_jual)->first()->InternalID;

                //dapat InternalID maka insert
                //cari kode barang dulu
                $inventory = Inventory::where("InventoryID", $data->kode_barang)->first()->InternalID;
                $uom = Uom::where("UomID", $data->satuan)->first()->InternalID;

                //insert ke sales detail
                $SODetail = new SalesOrderDetail();
                $SODetail->SalesOrderInternalID = $SOHeader;
                $SODetail->InventoryInternalID = $inventory;
                $SODetail->UomInternalID = $uom;
                $SODetail->SalesOrderParcelInternalID = 0;
                $SODetail->Qty = $data->jumlah;
                $SODetail->Price = $data->harga_jual;
                $SODetail->Discount = $data->disc1;
                $SODetail->VAT = 0;
                $SODetail->DiscountNominal = $data->disc2_amount;
                $SODetail->SubTotal = ($data->harga_jual * $data->jumlah) - $data->disc1_amount = $data->disc2_amount;
                $SODetail->UserRecord = "makmur";
                $SODetail->Remark = "";
                $SODetail->save();
            }
//            }
            echo "selesai";
        });
    }

    public function exportPODariFile() {
        Excel::load('file_excel/TRANS BELI HEADER 1.xlsx', function($reader) {

            // Getting all results
            $results = $reader->get();
            $str = "";
            foreach ($results as $datas) {
                foreach ($datas as $data) {
                    $countCoa6 = Coa6::where("ACC6ID", $data->kodesupp)->count();
                    if ($countCoa6 == 0) {
                        //gak ada
                        $coa6 = new Coa6();
                        $coa6->ACC6ID = $data->kodesupp;
                        $coa6->ACC6Name = $data->nama_supp;
                        $coa6->Email = $data->kodesupp . "@gmail.com";
                        $coa6->UserRecord = "makmur";
                        $coa6->Type = "s";
                        $coa6->CompanyInternalID = 1;
                        $coa6->Remark = "";
                        $coa6->save();
                    } else {
                        $coa6 = Coa6::where("ACC6ID", $data->kodesupp)->first();
                    }

                    $countP0 = PurcahseOrderHeader::where("PurcahseOrderID", $data->nomerbeli)->count();
                    if ($countP0 == 0) {
                        $poHeader = new PurchaseOrderHeader();
                        $poHeader->PurchaseOrderID = $data->nomerbeli;
                        $poHeader->PurchaseOrderDate = $data->tgl_beli;
                        $poHeader->ACC6InternalID = $coa6->InternalID;
                        $poHeader->LongTerm = $data->term_bayar;
                        $poHeader->isCash = ($data->term_bayar == 0 ? 0 : 1);
                        $poHeader->CurrencyInternalID = 5;
                        $poHeader->CurrencyRate = 1;
                        $poHeader->VAT = 0;
                        $poHeader->DiscountGlobal = $data->tot_disc;
                        $poHeader->GrandTotal = $data->total_nota;
                        $poHeader->WarehouseInternalID = 1;
                        $poHeader->dtRecord = $data->tgl_entry;
                        $poHeader->UserRecord = "makmur";
                        $poHeader->CompanyInternalID = 1;
                        $poHeader->Remark = ($data->keterangan == NULL ? "-" : $data->keterangan);
                        $poHeader->save();
                    }
                }
            }
        });
        echo "selesai";
    }

    public function exportPODetailDariFile() {
        Excel::load('file_excel/TRANS BELI DETAIL 1.xlsx', function($reader) {

            // Getting all results
            $results = $reader->get();
            foreach ($results as $datas) {
                foreach ($datas as $data) {
                    //cari header InternalIDnya

                    $POHeader = PurchaseOrderHeader::where("PurchaseOrderID", $data->nomer_beli)->first()->InternalID;

                    //dapat InternalID maka insert
                    //cari kode barang dulu
                    $inventory = Inventory::where("InventoryID", $data->kode_barang)->first()->InternalID;
                    $uom = Uom::where("UomID", $data->satuan)->first()->InternalID;

                    //insert ke sales detail
                    $PODetail = new SalesOrderDetail();
                    $PODetail->PurchaseOrderInternalID = $POHeader;
                    $PODetail->InventoryInternalID = $inventory;
                    $PODetail->UomInternalID = $uom;
                    $PODetail->SalesOrderParcelInternalID = 0;
                    $PODetail->Qty = $data->jumlah;
                    $PODetail->Price = $data->harga_beli;
                    $PODetail->Discount = $data->disc1;
                    $PODetail->VAT = 0;
                    $PODetail->DiscountNominal = $data->disc2_amount;
                    $PODetail->SubTotal = ($data->harga_beli * $data->jumlah) - $data->disc1_amount = $data->disc2_amount;
                    $PODetail->UserRecord = "makmur";
                    $PODetail->Remark = "";
                    $PODetail->save();
                }
            }
            echo "selesai";
        });
    }

    //============================/export excel==================================
    public function salesOrderPrintStruk($id) {
        $id = SalesOrderHeader::getIdsalesOrder($id);
        $header = SalesOrderHeader::find($id);
        $detail = SalesOrderHeader::find($id)->salesOrderDetail()->get();
        $html = '<!DOCTYPE html>
        <html id="printSalesOrder" style="width: 118mm; height: 150mm;">
            <head>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Receipt</title>
                <style>
                    @media print{@page { size: 89mm auto;}}
                    @page {
                        margin-top: 0;
                        margin-bottom: 0;
                        size: 89mm auto;
                    }

                    body {
                        font-family: calibri; 
                        width: 100%;
                        margin: 0 auto;
                    }
                    
                    body * {
                        letter-spacing: 0.6mm !important;
                    }
                    
                    h4 {
                        font-size: 18px;
                        letter-spacing: 0.4px;
                        margin-bottom: 2px;
                    }

                    h3 {
                        font-size: 16px;
                        letter-spacing: 0.4px;
                        margin-top: 0px;
                        margin-bottom: 2px;
                    }

                    p {
                        font-size: 14px;
                        letter-spacing: 0.2px;
                    }

                    table {
                        border-collapse: separate;
                        font-size: 16px;
                        letter-spacing: 0.2px;
                        width: 100%;
                        margin: 0 auto;
                    }

                    table td {
                        vertical-align: middle;
                        padding: -10px;
                    }

                </style>
            </head>';
        $html .= '<body>
        <h4 style="text-align: center;">' . Auth::user()->company->CompanyName . '</h4>
        <p style="text-align: center;">' . Auth::user()->company->Address . ', ' . Auth::user()->company->City . '</p>
        <p style="text-align: center;">Telp : ' . Auth::user()->company->Phone . ',Fax : ' . Auth::user()->company->Fax . '</p>    
        <h3 style="text-align: center;">Sales Order</h3>';
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $html .= '<table style="border-top: 0.4px solid #000000; border-bottom: 0.4px solid #000000;">
                <tr>
                    <td>' . $header->SalesOrderID . '</td>
                    <td colspan="2" style="text-align:right;">' . Auth::user()->UserID . '</td>
                </tr>
                <tr>
                    <td colspan="3">
                       ' . date("d-m-Y", strtotime($header->SalesOrderDate)) . '
                    </td>
                </tr>
                </table>';

            $html .= '<table style="border-bottom: 0.4px solid #000000;font-size: 18px;">';
//loop data inventory
            $count = 0;
            $total = 0;
            $totalVAT = 0;
            $i = 1;
            foreach ($detail as $data) {
                if ($data->SalesOrderParcelInternalID == 0) {
                    $inventory = Inventory::find($data->InventoryInternalID);
                    if ($inventory->TextPrint != '' || $inventory->TextPrint != Null) {
                        $inv = $inventory->InventoryID . '- ' . $inventory->TextPrint;
                    } else {
                        $inv = $inventory->InventoryID . '- ' . $inventory->InventoryName;
                    }

                    $html .= '<tr>
                                <td colspan="4">
                                    ' . $i . '. ' . $inv . '
                                </td>
                            </tr>';
                    $html .= '<tr>
                                    <td style="text-align: right; width: 20%">' . number_format($data->Qty, 0, '.', ',') . ' ' . $data->Uom->UomID . '</td>
                                    <td style="text-align: right; width: 10%">x</td>
                                    <td style="text-align: right; width: 20%">' . number_format($data->Price, 2, ".", ",") . '</td>
                                    <td style="text-align: right; width: 30%">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                              </tr>';
                    $html .= '<tr>  
                                    <td colspan="4" style="font-size:12px;" ><div style="margin-left:64px">( Disc : ' . $data->Discount . '% , Disc : ' . number_format($data->DiscountNominal, '2', '.', ',') . ')</div></td>
                              </tr>';
                    $count++;
                    $i++;
                    $totalVAT += $data->VAT;
                    $total += $data->SubTotal;
                    if ($totalVAT != 0) {
                        $totalVAT = $totalVAT - ($header->DiscountGlobal * 0.1);
                    }
                }
            }
            foreach (SalesOrderParcel::where("SalesOrderInternalID", $header->InternalID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->get() as $data) {

                $html .= '<tr>
                                <td colspan="4">
                                    ' . $i . '. ' . $data->Parcel->ParcelID . ' - ' . $data->Parcel->ParcelName . '
                                </td>
                            </tr>';
                $html .= '<tr>
                                    <td style="text-align: right; width: 20%">' . number_format($data->Qty, 0, '.', ',') . ' ' . $data->Uom->UomID . '</td>
                                    <td style="text-align: right; width: 10%">x</td>
                                    <td style="text-align: right; width: 20%">' . number_format($data->Price, 2, ".", ",") . '</td>
                                    <td style="text-align: right; width: 30%">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                              </tr>';
                $html .= '<tr>
                                    <td colspan="4" style="font-size:10px; ">( Disc : ' . $data->Discount . '% , Disc : ' . number_format($data->DiscountNominal, '2', '.', ',') . ')</td>
                              </tr>';
                $count++;
                $i++;
                if ($data->VAT != 0) {
                    $totalVAT += ($data->Price * $data->Qty) * 0.1;
                }
                $total += $data->Price * $data->Qty;
            }

            $html .= '</table>';


            $html .= '<table style="margin-bottom: 7px;font-size: 18px;">
                        <tr>
                            <td>Sum : ' . $count . ' item</td>
                            <td style="text-align: right;">Total</td>
                            <td style="text-align: right;">' . number_format($total, '2', '.', ',') . '</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: right;">Discount</td>
                            <td style="text-align: right;">' . number_format($header->DiscountGlobal, '2', '.', ',') . '</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: right;">Grand Total</td>
                            <td style="text-align: right;">' . number_format($total - $header->DiscountGlobal, '2', '.', ',') . '</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: right;">Tax</td>
                            <td style="text-align: right;">(' . $totalVAT . ')</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: right;"><strong>Grand Total</strong></td>
                            <td style="border-bottom: 1px solid #000000; text-align: right;">' . number_format($header->GrandTotal, '2', '.', ',') . '</td>
                        </tr>';
            $html .= '</table>

                    <ul style="page-break-after:always; font-size: 12px; list-style: none; display: block; padding: 0; margin: 0 auto;">
                        <li style="margin-bottom: 2px; line-height: 14px; text-align: center;">Thank you for your visit.</li>
                        <li style="line-height: 14px; text-align: center;">Please come again later.</li>
                    </ul>
                    <script src="' . Asset("") . 'lib/bootstrap/js/jquery-1.11.1.min.js"></script>
                    <script>
                    
                        $(document).ready(function(e){
                        window.print();
                        });
                        $(document).click(function(e){
                        window.location.assign("' . URL("/") . '");
                        });
                    </script>
                </body>
            </html>';

            echo $html;
        }
    }

    static function salesOrderDataBackup($data) {
        $explode = explode('---;---', $data);
//        dd($explode);
        $coa6 = $explode[0];
        $typePayment = $explode[1];
        $typeTax = $explode[2];
        $start = $explode[3];
        $end = $explode[4];
        $where = '';
        if ($typePayment != '-1' && $typePayment != '') {
            $where .= 'isCash = "' . $typePayment . '" ';
        }
        if ($coa6 != '-1' && $coa6 != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'ACC6InternalID = ' . $coa6 . ' ';
        }
        if (Auth::user()->SeeNPPN == 1) {
//            if ($typeTax != '-1' && $typeTax != '') {
//                if ($where != '') {
//                    $where .= ' AND ';
//                }
//                $where .= 'VAT = "' . $typeTax . '" ';
//            }
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'VAT = "0" ';
        } else {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'VAT = "1" ';
        }
        if ($start != '' && $end != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $start = explode('-', $start);
            $end = explode('-', $end);
            $startSearch = $start[2] . '-' . $start[1] . '-' . $start[0];
            $endSearch = $end[2] . '-' . $end[1] . '-' . $end[0];
            $where .= 'SalesOrderDate between "' . $startSearch . ' 00:00:00" AND "' . $endSearch . ' 23:59:59"';
        }
        $table = 't_salesorder_header';
        $primaryKey = 't_salesorder_header`.`InternalID';
//        if (checkSeeNPPN()) {
        $columns = array(
            array('db' => 't_salesorder_header`.`InternalID', 'dt' => 0, 'formatter' => function($d, $row) {
                    return $d;
                }),
            array('db' => 'SalesOrderID', 'dt' => 1),
            array('db' => 'isCash', 'dt' => 2, 'formatter' => function( $d, $row ) {
                    if ($d == 0) {
                        return 'Cash';
                    } else if ($d == 1) {
                        return 'Credit';
                    } else if ($d == 2) {
                        return 'CBD';
                    } else if ($d == 3) {
                        return 'Deposit';
                    } else {
                        return 'Down Payment';
                    }
                },
                'field' => 't_salesorder_header`.`InternalID'),
            array(
                'db' => 'SalesOrderDate',
                'dt' => 3,
                'formatter' => function( $d, $row ) {
                    return date("d-m-Y", strtotime($d));
                }
            ),
//                array('db' => 'CurrencyName', 'dt' => 3),
//                array(
//                    'db' => 'CurrencyRate',
//                    'dt' => 4,
//                    'formatter' => function( $d, $row ) {
//                        return number_format($d, '2', '.', ',');
//                    }
//                ),
            array(
                'db' => 'ACC6Name',
                'dt' => 4,
                'formatter' => function( $d, $row ) {
                    return $d;
                }
            ),
//                array(
//                    'db' => 'VAT',
//                    'dt' => 6,
//                    'formatter' => function( $d, $row ) {
//                        if ($d == 0) {
//                            return 'Non Tax';
//                        } else {
//                            return 'Tax';
//                        }
//                    }
//                ),
            array(
                'db' => 'GrandTotal',
                'dt' => 5,
                'formatter' => function( $d, $row ) {
                    return number_format($d, '2', '.', ',');
                }
            ),
            array(
                'db' => 'PurchasingCommission',
                'dt' => 6,
                'formatter' => function( $d, $row ) {
                    return number_format($d, '2', '.', ',');
                }
            ),
            array(
                'db' => 't_salesorder_header`.`InternalID',
                'dt' => 7,
                'formatter' => function($d, $row ) {
                    return (SalesOrderHeader::find($d)->Status == 0 ? 'Not Approved' : 'Approved');
                }
            ),
            array(
                'db' => 't_salesorder_header`.`InternalID',
                'dt' => 8,
                'formatter' => function($d, $row ) {
                    if (SalesOrderHeader::find($d)->Closed == 1)
                        return 'Closed';
                    else {
                        if (checkSalesAdd($d) == true || checkShippingAdd($d) == true)
                            return 'Undone';
                        else
                            return 'Done';
                    }
//                    return ((checkSalesAdd($d) == true || checkShippingAdd($d) == true) && SalesOrderHeader::find($d)->Closed == 0 ? 'Undone' : 'Done');
                }
            ),
            array(
                'db' => 'Print',
                'dt' => 9,
                'formatter' => function($d, $row) {
                    return $d . " times";
                }
            ),
            array('db' => 't_salesorder_header`.`InternalID', 'dt' => 10, 'formatter' => function( $d, $row ) {
                    $data = SalesOrderHeader::find($d);
                    $action = '<td class="text-center">
                                    <a href="' . Route('salesOrderDetail', $data->SalesOrderID) . '">
                                        <button id="btn-' . $data->SalesOrderID . '-detail"
                                                class="btn btn-pure-xs btn-xs btn-detail" title="detail">
                                            <span class="glyphicon glyphicon-zoom-in"></span>
                                        </button>
                                    </a>';


                    if (!SalesOrderHeader::isInvoice($data->InternalID) && !SalesOrderHeader::isShipping($data->InternalID)) {
                        $action .= ' <a href="' . Route('salesOrderUpdate', $data->SalesOrderID) . '">
                                        <button id="btn-' . $data->SalesOrderID . '-update"
                                                class="btn btn-pure-xs btn-xs btn-edit" title="update">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </button>
                                    </a>';
                        $action .= '
                                    <button data-target="#m_salesOrderDelete" data-internal="' . $data->InternalID . '"  data-toggle="modal" role="dialog"
                                           onclick="deleteAttach(this)" data-id="' . $data->SalesOrderID . '" data-name=' . $data->SalesOrderID . ' class="btn btn-pure-xs btn-xs btn-delete" title="delete">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>';
                    } else {

                        $action .= '
                            <button disabled class="btn btn-pure-xs btn-xs btn-edit" title="update"><span class="glyphicon glyphicon-edit"></span></button>
                                    <button disabled class="btn btn-pure-xs btn-xs btn-delete" title="delete"><span class="glyphicon glyphicon-trash"></span></button>';
                    }

                    $action .= '<button data-target="#m_salesOrderApprove" data-internal="' . $data->InternalID . '"  data-toggle="modal" role="dialog" title="approve"
                                           onclick="approveAttach(this)" data-id="' . $data->SalesOrderID . '" data-name=' . $data->SalesOrderID . ' class="btn btn-pure-xs btn-xs btn-closed">
                                        <span class="glyphicon glyphicon-ok"></span>
                                    </button>';


                    $action .= '<button data-target="#m_salesOrderClose" data-internal="' . $data->InternalID . '"  data-toggle="modal" role="dialog"
                                           onclick="closeAttach(this)" data-id="' . $data->SalesOrderID . '" data-name=' . $data->SalesOrderID . ' class="btn btn-pure-xs btn-xs btn-closed" title="close">
                                        <span class="glyphicon glyphicon-ban-circle"></span>
                                    </button>';
//                    $action .= '<a title="print" href="'.Route('salesOrderPrint',$data->SalesOrderID) .'" target="_blank">'
//                            . '<button id="btn-'.$data->SalesOrderID.'-print"'
//                            . 'class="btn btn-pure-xs btn-xs btn-print">'
//                            . '<span class="glyphicon glyphicon-print"></span>'
//                            . '</button></a>';
//                    $action .= '<button data-target="#r_print" data-internal="' . $data->SalesOrderID . '"  data-toggle="modal" role="dialog" title="print"
//                                           onclick="printAttach(this)" data-id="' . $data->SalesOrderID . '" data-name=' . $data->SalesOrderID . ' class="btn btn-pure-xs btn-xs btn-closed">
//                                        <span class="glyphicon glyphicon-print"></span>
//                                    </button>';
                    $action .= '<a target="_blank" title="print" href="' . route('salesOrderPrint', $data->SalesOrderID) . '">'
                            . '<button id="btn-' . $data->SalesOrderID . '-print"'
                            . 'class="btn btn-pure-xs btn-xs btn-print" title="print">'
                            . '<span class="glyphicon glyphicon-print"></span>'
                            . '</button></a>';
//                    dd(checkShippingAdd($data->InternalID));
                    if (checkShippingAdd($data->InternalID) && $data->Closed == 0 && $data->Status == 1) {
                        if ((checkAmountSales($data->InternalID) <= 0 && $data->isCash == 2) || ($data->isCash == 4 && checkDpShipping($data->InternalID)) || ($data->isCash != 2 && $data->isCash != 4)) {
                            $action .= '<a href="' . Route('shippingNew', $data->SalesOrderID) . '" target="_blank">
                                        <button id="btn-' . $data->SalesOrderID . '-shipping"
                                                class="btn btn-pure-xs btn-xs" title="shipping">
                                            <b>Sh</b>
                                        </button>
                                    </a>';
                        } else {
                            $action .= '<button disabled class="btn btn-pure-xs btn-xs btn-edit"><b>Sh</b></button>';
                        }
                    } else {
                        $action .= '<button disabled class="btn btn-pure-xs btn-xs btn-edit"><b>Sh</b></button>';
                    }
                    if (checkSalesAdd($data->InternalID) && $data->Closed == 0 && $data->Status == 1) {
                        if ((checkAmountSales($data->InternalID) <= 0 && $data->isCash == 2) || ($data->isCash == 4 && checkDpShipping($data->InternalID)) || ($data->isCash != 2 && $data->isCash != 4)) {
                            $action .= '<a href="' . Route('salesNew', $data->SalesOrderID) . '" target="_blank">
                                        <button id="btn-' . $data->SalesOrderID . '-sales"
                                                class="btn btn-pure-xs btn-xs" title="sales">
                                            <b>Sa</b>
                                        </button>
                                    </a>';
                        } else {
                            $action .= '<button disabled class="btn btn-pure-xs btn-xs btn-edit"><b>Sa</b></button>';
                        }
                    } else {
                        $action .= '<button disabled class="btn btn-pure-xs btn-xs btn-edit"><b>Sa</b></button>';
                    }
                    if ($data->Closed == 0 && $data->Status == 1) {
                        $action .= '<a href="' . Route('transferNew', "jenis=insertTransfer&SalesOrder=" . $data->SalesOrderID) . '" target="_blank">
                                        <button id="btn-' . $data->SalesOrderID . '-shipping"
                                                class="btn btn-pure-xs btn-xs" title="transfer">
                                            <span class="glyphicon glyphicon-transfer"></span>
                                        </button>
                                    </a>';
                    } else {
                        $action .= '<button disabled class="btn btn-pure-xs btn-xs btn-edit"><span class="glyphicon glyphicon-transfer"></span></button>';
                    }
                    if ($data->Closed == 0 && $data->Status == 1) {
                        $action .= '<a href="' . Route('transformationNew', "jenis=insertTransformation&SalesOrder=" . $data->SalesOrderID) . '" target="_blank">
                                        <button id="btn-' . $data->SalesOrderID . '-shipping"
                                                class="btn btn-pure-xs btn-xs" title="transformation">
                                            <b>Tr</b>
                                        </button>
                                    </a>';
                    } else {
                        $action .= '<button disabled class="btn btn-pure-xs btn-xs btn-edit"><b>Tr</b></button>';
                    }
//                     <a href="' . Route('salesOrderPrintStruk', $data->SalesOrderID) . '" target="_blank">
//                                        <button id="btn-' . $data->SalesOrderID . '-update"
//                                                class="btn btn-pure-xs btn-xs btn-edit">
//                                            <span class="glyphicon glyphicon-print"></span> Struk
//                                        </button>
//                                    </a>  
                    if ($data->isCash == 4 && TopUp::where("SalesOrderInternalID", $data->InternalID)->count() == 0) {
                        $action .= '<a target="_blank" title="print" href="' . route('invoiceUMPrint', $data->SalesOrderID) . '">'
                                . '<button id="btn-' . $data->SalesOrderID . '-print"'
                                . 'class="btn btn-pure-xs btn-xs btn-print" title="print">'
                                . '<span class="glyphicon glyphicon-print"></span> <b>UM</b>'
                                . '</button></a>';
                    }
                    return $action;
                },
                'field' => 't_salesorder_header`.`InternalID')
        );
//        } else {
//            $columns = array(
//                array('db' => 'SalesOrderID', 'dt' => 0),
//                array('db' => 'isCash', 'dt' => 1, 'formatter' => function( $d, $row ) {
//                        if ($d == 0) {
//                            return 'Cash';
//                        } else {
//                            return 'Credit';
//                        }
//                    },
//                    'field' => 't_salesorder_header`.`InternalID'),
//                array(
//                    'db' => 'SalesOrderDate',
//                    'dt' => 2,
//                    'formatter' => function( $d, $row ) {
//                        return date("d-m-Y", strtotime($d));
//                    }
//                ),
//                array('db' => 'CurrencyName', 'dt' => 3),
//                array(
//                    'db' => 'CurrencyRate',
//                    'dt' => 4,
//                    'formatter' => function( $d, $row ) {
//                        return number_format($d, '2', '.', ',');
//                    }
//                ),
//                array(
//                    'db' => 'ACC6Name',
//                    'dt' => 5,
//                    'formatter' => function( $d, $row ) {
//                        return $d;
//                    }
//                ),
////            array(
////                'db' => 'VAT',
////                'dt' => 6,
////                'formatter' => function( $d, $row ) {
////                    if ($d == 0) {
////                        return 'Non Tax';
////                    } else {
////                        return 'Tax';
////                    }
////                }
////            ),
//                array(
//                    'db' => 'GrandTotal',
//                    'dt' => 6,
//                    'formatter' => function( $d, $row ) {
//                        return number_format($d, '2', '.', ',');
//                    }
//                ),
//                array(
//                    'db' => 'PurchasingCommission',
//                    'dt' => 7,
//                    'formatter' => function( $d, $row ) {
//                        return number_format($d, '2', '.', ',');
//                    }
//                ),
//                array(
//                    'db' => 't_salesorder_header`.`InternalID',
//                    'dt' => 8,
//                    'formatter' => function($d, $row ) {
//                        return (SalesOrderHeader::find($d)->Status == 0 ? 'Not Approved' : 'Approved');
//                    }
//                ),
//                array(
//                    'db' => 't_salesorder_header`.`InternalID',
//                    'dt' => 9,
//                    'formatter' => function($d, $row ) {
//                        return (checkSalesAdd($d) == true && SalesOrderHeader::find($d)->Closed == 0 ? 'Undone' : 'Done');
//                    }
//                ),
//                array(
//                    'db' => 'Print',
//                    'dt' => 10,
//                    'formatter' => function($d, $row) {
//                        return $d . " times";
//                    }
//                ),
//                array('db' => 't_salesorder_header`.`InternalID', 'dt' => 11, 'formatter' => function( $d, $row ) {
//                        $data = SalesOrderHeader::find($d);
//                        $action = '<td class="text-center">
//                                    <a href="' . Route('salesOrderDetail', $data->SalesOrderID) . '">
//                                        <button id="btn-' . $data->SalesOrderID . '-detail"
//                                                class="btn btn-pure-xs btn-xs btn-detail" title="detail">
//                                            <span class="glyphicon glyphicon-zoom-in"></span>
//                                        </button>
//                                    </a>';
//                        if (!SalesOrderHeader::isInvoice($data->InternalID) && !SalesOrderHeader::isShipping($data->InternalID)) {
//                            $action .= '<a href="' . Route('salesOrderUpdate', $data->SalesOrderID) . '">
//                                        <button id="btn-' . $data->SalesOrderID . '-update"
//                                                class="btn btn-pure-xs btn-xs btn-edit" title="update">
//                                            <span class="glyphicon glyphicon-edit"></span>
//                                        </button>
//                                    </a>
//                                    <button data-target="#m_salesOrderDelete" data-internal="' . $data->InternalID . '"  data-toggle="modal" role="dialog"
//                                           onclick="deleteAttach(this)" data-id="' . $data->SalesOrderID . '" data-name=' . $data->SalesOrderID . ' class="btn btn-pure-xs btn-xs btn-delete" title="delete">
//                                        <span class="glyphicon glyphicon-trash"></span>
//                                    </button>';
//                        } else {
//                            $action .= '<button disabled class="btn btn-pure-xs btn-xs btn-edit" title="update"><span class="glyphicon glyphicon-edit"></span></button>
//                                    <button disabled class="btn btn-pure-xs btn-xs btn-delete" title="delete"><span class="glyphicon glyphicon-trash"></span></button>';
//                        }
//
//                        $action .= '<button data-target="#m_salesOrderApprove" data-internal="' . $data->InternalID . '"  data-toggle="modal" role="dialog" title="approve"
//                                           onclick="approveAttach(this)" data-id="' . $data->SalesOrderID . '" data-name=' . $data->SalesOrderID . ' class="btn btn-pure-xs btn-xs btn-closed">
//                                        <span class="glyphicon glyphicon-ok"></span>
//                                    </button>';
//
//
//                        $action .= '<button data-target="#m_salesOrderClose" data-internal="' . $data->InternalID . '"  data-toggle="modal" role="dialog"
//                                           onclick="closeAttach(this)" data-id="' . $data->SalesOrderID . '" data-name=' . $data->SalesOrderID . ' class="btn btn-pure-xs btn-xs btn-closed" title="close">
//                                        <span class="glyphicon glyphicon-ban-circle"></span>
//                                    </button>';
////                    $action .= '<a title="print" href="'.Route('salesOrderPrint',$data->SalesOrderID) .'" target="_blank">'
////                            . '<button id="btn-'.$data->SalesOrderID.'-print"'
////                            . 'class="btn btn-pure-xs btn-xs btn-print">'
////                            . '<span class="glyphicon glyphicon-print"></span>'
////                            . '</button></a>';
//                        $action .= '<button data-target="#r_print" data-internal="' . $data->SalesOrderID . '"  data-toggle="modal" role="dialog" title="print"
//                                           onclick="printAttach(this)" data-id="' . $data->SalesOrderID . '" data-name=' . $data->SalesOrderID . ' class="btn btn-pure-xs btn-xs btn-closed">
//                                        <span class="glyphicon glyphicon-print"></span>
//                                    </button>';
//                        if (checkShippingAdd($data->InternalID) && $data->Closed == 0) {
//                            $action .= '<a href="' . Route('shippingNew', $data->SalesOrderID) . '" target="_blank">
//                                        <button id="btn-' . $data->SalesOrderID . '-shipping"
//                                                class="btn btn-pure-xs btn-xs" title="shipping">
//                                            <b>Sh</b>
//                                        </button>
//                                    </a>';
//                        } else {
//                            $action .= '<button disabled class="btn btn-pure-xs btn-xs btn-edit"><b>Sh</b></button>';
//                        }
//                        if (checkSalesAdd($data->InternalID) && $data->Closed == 0) {
//                            $action .= '<a href="' . Route('salesNew', $data->SalesOrderID) . '" target="_blank">
//                                        <button id="btn-' . $data->SalesOrderID . '-sales"
//                                                class="btn btn-pure-xs btn-xs" title="sales">
//                                            <b>Sa</b>
//                                        </button>
//                                    </a>';
//                        } else {
//                            $action .= '<button disabled class="btn btn-pure-xs btn-xs btn-edit"><b>Sa</b></button>';
//                        }
//                        $action .= '<a href="' . Route('transferNew', "jenis=insertTransfer&SalesOrder=" . $data->SalesOrderID) . '" target="_blank">
//                                        <button id="btn-' . $data->SalesOrderID . '-shipping"
//                                                class="btn btn-pure-xs btn-xs" title="transfer">
//                                            <span class="glyphicon glyphicon-transfer"></span>
//                                        </button>
//                                    </a>';
//                        $action .= '<a href="' . Route('transformationNew', "jenis=insertTransformation&SalesOrder=" . $data->SalesOrderID) . '" target="_blank">
//                                        <button id="btn-' . $data->SalesOrderID . '-shipping"
//                                                class="btn btn-pure-xs btn-xs" title="transformation">
//                                            <b>Tr</b>
//                                        </button>
//                                    </a>';
////                     <a href="' . Route('salesOrderPrintStruk', $data->SalesOrderID) . '" target="_blank">
////                                        <button id="btn-' . $data->SalesOrderID . '-update"
////                                                class="btn btn-pure-xs btn-xs btn-edit">
////                                            <span class="glyphicon glyphicon-print"></span> Struk
////                                        </button>
////                                    </a>  
//                        return $action;
//                    },
//                    'field' => 't_salesorder_header`.`InternalID')
//            );
//        }

        $sql_details = getConnection();

        require('ssp.class.php');
        $ID_CLIENT_VALUE = Auth::user()->CompanyInternalID;
        if ($where != '') {
            $extraCondition = $where . ' AND ' . 't_salesorder_header.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        } else {
            $extraCondition = 't_salesorder_header.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        }
        $join = ' INNER JOIN m_currency on m_currency.InternalID = t_salesorder_header.CurrencyInternalID '
                . 'INNER JOIN m_coa6 on m_coa6.InternalID = t_salesorder_header.ACC6InternalID';

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $extraCondition, $join));
    }

}
