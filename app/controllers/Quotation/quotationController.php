<?php

class QuotationController extends BaseController {

    public function showQuotation() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteQuotation') {
                return $this->deleteQuotation();
            } else if (Input::get('jenis') == 'summaryQuotation') {
                return $this->summaryQuotation();
            } else if (Input::get('jenis') == 'detailQuotation') {
                return $this->detailQuotation();
            } else if (Input::get('jenis') == 'printQuotation') {
                if (Input::get('type') == 'exclude') {
                    return Redirect::route('quotationPrint', Input::get('internalID'));
                } else {
                    return Redirect::route('quotationIncludePrint', Input::get('internalID'));
                }
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = QuotationHeader::advancedSearch(Input::get('coa6'), Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
//            dd($data);
            return View::make('quotation.quotationSearch')
                            ->withToogle('transaction')->withAktif('quotation')
                            ->withData($data);
        }
        return View::make('quotation.quotation')
                        ->withToogle('transaction')->withAktif('quotation');
    }

    public function quotationNew() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteQuotation') {
                return $this->deleteQuotation();
            } else if (Input::get('jenis') == 'summaryQuotation') {
                return $this->summaryQuotation();
            } else if (Input::get('jenis') == 'detailQuotation') {
                return $this->detailQuotation();
            } else if (Input::get('jenis') == 'printQuotation') {
                if (Input::get('type') == 'exclude') {
                    return Redirect::route('quotationPrint', Input::get('internalID'));
                } else {
                    return Redirect::route('quotationIncludePrint', Input::get('internalID'));
                }
            } else {
                return $this->insertQuotation();
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = QuotationHeader::advancedSearch(Input::get('coa6'), Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
            return View::make('quotation.quotationSearch')
                            ->withToogle('transaction')->withAktif('quotation')
                            ->withData($data);
        }
        $quotation = $this->createID(0) . '.';
        return View::make('quotation.quotationNew')
                        ->withToogle('transaction')->withAktif('quotation')
                        ->withQuotation($quotation);
    }

    public function quotationDetail($id) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'summaryQuotation') {
                return $this->summaryQuotation();
            } else if (Input::get('jenis') == 'detailQuotation') {
                return $this->detailQuotation();
            } else if (Input::get('jenis') == 'deleteQuotation') {
                return $this->deleteQuotation();
            } else if (Input::get('jenis') == 'printQuotation') {
                if (Input::get('type') == 'exclude') {
                    return Redirect::route('quotationPrint', Input::get('internalID'));
                } else {
                    return Redirect::route('quotationIncludePrint', Input::get('internalID'));
                }
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = QuotationHeader::advancedSearch(Input::get('coa6'), Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
            return View::make('quotation.quotationSearch')
                            ->withToogle('transaction')->withAktif('quotation')
                            ->withData($data);
        }

        $id = QuotationHeader::getIdquotation($id);
        $header = QuotationHeader::find($id);
        $detail = QuotationHeader::find($id)->quotationDetail()->get();
        $description = QuotationHeader::find($id)->quotationDescription;

        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            if (Session::has("msg")) {
                if (Session::get("msg") == "print") {
                    $print = Route('quotationPrint', $header->QuotationID);

                    return View::make('quotation.quotationDetail')
                                    ->withToogle('transaction')->withAktif('quotation')
                                    ->withHeader($header)
                                    ->withDetail($detail)
                                    ->withPrint($print)
                                    ->withDescription($description);
                }
            }
            return View::make('quotation.quotationDetail')
                            ->withToogle('transaction')->withAktif('quotation')
                            ->withHeader($header)
                            ->withDetail($detail)
                            ->withDescription($description);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showQuotation');
        }
    }

    public function quotationUpdate($id) {
        $id = QuotationHeader::getIdquotation($id);
        $header = QuotationHeader::find($id);
        $detail = QuotationHeader::find($id)->quotationDetail()->get();
        $description = QuotationHeader::find($id)->quotationDescription;
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                if (Input::get('jenis') == 'summaryQuotation') {
                    return $this->summaryQuotation();
                } else if (Input::get('jenis') == 'detailQuotation') {
                    return $this->detailQuotation();
                } else if (Input::get('jenis') == 'deleteQuotation') {
                    return $this->deleteQuotation();
                } else if (Input::get('jenis') == 'printQuotation') {
                    if (Input::get('type') == 'exclude') {
                        return Redirect::route('quotationPrint', Input::get('internalID'));
                    } else {
                        return Redirect::route('quotationIncludePrint', Input::get('internalID'));
                    }
                } else {
                    return $this->updateQuotation($id);
                }
            } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
                $data = QuotationHeader::advancedSearch(Input::get('coa6'), Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
                return View::make('quotation.quotationSearch')
                                ->withToogle('transaction')->withAktif('quotation')
                                ->withData($data);
            }
            if (strpos(URL::previous(), 'quotationCopy'))
                $print = Route('quotationPrint', $header->QuotationID);
            else
                $print = '';
            $quotation = $this->createID(0) . '.';
            return View::make('quotation.quotationUpdate')
                            ->withToogle('transaction')->withAktif('quotation')
                            ->withHeader($header)
                            ->withDetail($detail)
                            ->withPrint($print)
                            ->withDescription($description)
                            ->withQuotation($quotation);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showQuotation');
        }
    }

    public function quotationCopy($id) {
        $id = QuotationHeader::getIdquotation($id);
        $header = QuotationHeader::find($id);
        $detail = QuotationHeader::find($id)->quotationDetail()->get();
        $description = QuotationHeader::find($id)->quotationDescription;
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                if (Input::get('jenis') == 'summaryQuotation') {
                    return $this->summaryQuotation();
                } else if (Input::get('jenis') == 'detailQuotation') {
                    return $this->detailQuotation();
                } else if (Input::get('jenis') == 'deleteQuotation') {
                    return $this->deleteQuotation();
                } else if (Input::get('jenis') == 'printQuotation') {
                    if (Input::get('type') == 'exclude') {
                        return Redirect::route('quotationPrint', Input::get('internalID'));
                    } else {
                        return Redirect::route('quotationIncludePrint', Input::get('internalID'));
                    }
                } else {
                    return $this->copyQuotation($id);
                }
            } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
                $data = QuotationHeader::advancedSearch(Input::get('coa6'), Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
                return View::make('quotation.quotationSearch')
                                ->withToogle('transaction')->withAktif('quotation')
                                ->withData($data);
            }
            $quotation = $this->createID(0) . '.';
            return View::make('quotation.quotationCopy')
                            ->withToogle('transaction')->withAktif('quotation')
                            ->withHeader($header)
                            ->withDetail($detail)
                            ->withDescription($description)
                            ->withQuotation($quotation);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showQuotation');
        }
    }

    public function insertQuotation() {
        //rule
        $jenis = Input::get('isCash');
        if ($jenis == '0' || $jenis == '2' || $jenis == '3') {
            $rule = array(
                'date' => 'required',
//                'coa6' => 'required',
//                'remark' => 'required|max:1000',
                'currency' => 'required',
                'warehouse' => 'required',
                'rate' => 'required',
//                'salesman' => 'required',
                'inventoryDescription' => 'required'
            );
            $longTerm = 0;
        } else {
            if ($jenis == '1') {
                $rule = array(
                    'date' => 'required',
                    'longTerm' => 'required|integer',
//                'coa6' => 'required',
//                'remark' => 'required|max:1000',
                    'currency' => 'required',
                    'warehouse' => 'required',
                    'rate' => 'required',
//                'salesman' => 'required',
                    'inventoryDescription' => 'required'
                );
            } else {
                $rule = array(
                    'date' => 'required',
                    'longTerm' => 'required|integer',
//                'coa6' => 'required',
//                'remark' => 'required|max:1000',
                    'downPayment' => 'required',
                    'currency' => 'required',
                    'warehouse' => 'required',
                    'rate' => 'required',
//                'salesman' => 'required',
                    'inventoryDescription' => 'required'
                );
            }
            $longTerm = Input::get('longTerm');
        }
        $quotationNumber = '';
        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        $print = "";
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalInsert';
            $error = $validator->messages();
        } else {
            //valid
            //insert header
            $tax = '';
            if (Input::get('vat') == '') {
                $tax = "NonTax";
            } else {
                $tax = "Tax";
            }
            $header = new QuotationHeader();
            $quotation = $this->createID(1, $tax) . '.';
            $date = explode('-', Input::get('date'));
            $yearDigit = substr($date[2], 2);
            $quotation .= $date[1] . $yearDigit . '.';
            $quotationNumber = QuotationHeader::getNextIDQuotation($quotation);
            $header->QuotationID = $quotationNumber;
            $header->QuotationDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            if (Input::get('jeniscustomer') == 'cuslama') {
                $header->TypeCustomer = 0;
                $header->ACC6InternalID = Input::get('coa6');
                $header->SalesManInternalID = Input::get('salesman');
                $header->AttnInternalID = Input::get('attn');
            } else {
                $header->TypeCustomer = 1;
                $header->CustomerName = Input::get('name');
                $header->CustomerAddress = Input::get('address');
                $header->CustomerPhone = Input::get('phone');
                $header->CustomerFax = Input::get('fax');
                $header->CustomerEmail = Input::get('email');
                $header->CustomerCP = Input::get('cp');
                $header->SalesManInternalID = Input::get('salesman_baru');
            }
            $header->LongTerm = $longTerm;
            $header->isCash = $jenis;
            $currency = explode('---;---', Input::get('currency'));
            $header->WarehouseInternalID = Input::get('warehouse');
            $header->CurrencyInternalID = $currency[0];
            $header->CurrencyRate = str_replace(',', '', Input::get('rate'));
            if (Input::get('vat') == '') {
                $header->VAT = '0';
            } else {
                $header->VAT = Input::get('vat');
            }
            $header->DiscountGlobal = str_replace(",", "", Input::get('DiscountGlobal'));
            $header->GrandTotal = Input::get('grandTotalValue');
            $header->DeliveryTerms = Input::get('DeliveryTerms');
            $header->DeliveryTime = Input::get('DeliveryTime');
            $header->Validity = Input::get('Validity');
            $header->TermOfPayment = Input::get('TermOfPayment');
            $header->RefNumber = Input::get('RefNumber');
            $header->Print = 1;
            $header->UserRecord = Auth::user()->UserID;
            $header->CompanyInternalID = Auth::user()->Company->InternalID;
            $header->UserModified = '0';
            $header->Remark = Input::get('remark');
            $header->save();
            //insert detail
            $total = 0;

            //insert quotation description
            $indexterakhirinventory = 0;
            for ($a = 0; $a < count(Input::get('inventoryDescription')); $a++) {
                $uomDescriptionValue = Input::get('uomDescription')[$a];
                $spesifikasi = Input::get('spesifikasi')[$a];
                $qtyDescriptionValue = str_replace(',', '', Input::get('qtyDescription')[$a]);
                $priceDescriptionValue = str_replace(',', '', Input::get('priceDescription')[$a]);
                $discDescriptionValue = str_replace(',', '', Input::get('discountNominalDescription')[$a]);
                $subTotalDescription = ($priceDescriptionValue * $qtyDescriptionValue) - (($priceDescriptionValue * $qtyDescriptionValue) * Input::get('discountDescription')[$a] / 100) - $discDescriptionValue * $qtyDescriptionValue;
                if (Input::get('vat') == '1') {
                    $vatValue = $subTotalDescription / 10;
                } else {
                    $vatValue = 0;
                }
                if ($qtyDescriptionValue > 0) {
                    $description = new QuotationDescription();
                    $description->QuotationInternalID = $header->InternalID;
                    $description->InventoryText = Input::get('inventoryDescription')[$a];
                    $description->UomText = $uomDescriptionValue;
                    $description->Spesifikasi = $spesifikasi;
                    $description->Qty = $qtyDescriptionValue;
                    $description->Price = $priceDescriptionValue;
                    $description->Discount = 0.00;
//                    $description->Discount = Input::get('discountDescription')[$a];
                    $description->DiscountNominal = $discDescriptionValue;
                    $description->VAT = $vatValue;
                    $description->SubTotal = $subTotalDescription;
                    $description->UserRecord = Auth::user()->UserID;
                    $description->UserModified = '0';
                    $description->save();
                }

                $plus = 1;
                if (count(Input::get('inventory' . ($indexterakhirinventory + $plus))) == 0) {
                    while (count(Input::get('inventory' . ($indexterakhirinventory + $plus))) == 0) {
                        $plus++;
                    }
                }
                $indexterakhirinventory = $indexterakhirinventory + $plus;
                for ($c = 0; $c < count(Input::get('inventory' . ($indexterakhirinventory))); $c++) {
//                    echo '<pre>';
//                    print_r(Input::get('inventory' . ($indexterakhirinventory))[$c]);
//                    echo '</pre>';
                    $tamp = explode("---;---", Input::get('inventory' . ($indexterakhirinventory))[$c]);
                    if ($tamp[1] != "parcel") {
                        //jika bukan paket
                        $qtyValue = str_replace(',', '', Input::get('qty' . ($indexterakhirinventory))[$c]);
                        $priceValue = str_replace(',', '', Input::get('price' . ($indexterakhirinventory))[$c]);
                        $discValue = str_replace(',', '', Input::get('discountNominal' . ($indexterakhirinventory))[$c]);
                        $subTotal = ($priceValue * $qtyValue) - (($priceValue * $qtyValue) * Input::get('discount' . ($indexterakhirinventory))[$c] / 100) - $discValue * $qtyValue;
                        if (Input::get('vat') == '1') {
                            $vatValue = $subTotal / 10;
                        } else {
                            $vatValue = 0;
                        }
                        if ($qtyValue > 0) {
//                            echo("masuk");
                            $detail = new QuotationDetail();
                            $detail->QuotationInternalID = $header->InternalID;
                            $detail->InventoryInternalID = Input::get('inventory' . ($indexterakhirinventory))[$c];
                            $detail->DescriptionInternalID = $description->InternalID;
                            $detail->UomInternalID = Input::get('uom' . ($indexterakhirinventory))[$c];
                            $detail->Qty = $qtyValue;
                            $detail->Price = $priceValue;
                            $detail->Discount = Input::get('discount' . ($indexterakhirinventory))[$c];
                            $detail->DiscountNominal = $discValue;
                            $detail->VAT = $vatValue;
                            $detail->SubTotal = $subTotal;
                            $detail->UserRecord = Auth::user()->UserID;
                            $detail->UserModified = '0';
                            $detail->save();
                        }
                        $total += $subTotal;
                    } else {
                        //jika paket
                        $parcelDetail = ParcelInventory::where("ParcelInternalID", $tamp[0])->where("CompanyInternalID", Auth::user()->Company->InternalID)->get();

                        $qtyValue = str_replace(',', '', Input::get('qty' . ($indexterakhirinventory))[$c]);
                        $priceValue = str_replace(',', '', Input::get('price' . ($indexterakhirinventory))[$c]);
                        $discValue = str_replace(',', '', Input::get('discountNominal' . ($indexterakhirinventory))[$c]);
                        $subTotal = ($priceValue * $qtyValue) - (($priceValue * $qtyValue) * Input::get('discount' . ($indexterakhirinventory))[$c] / 100) - $discValue * $qtyValue;
                        if (Input::get('vat') == '1') {
                            $vatValue = $subTotal / 10;
                        } else {
                            $vatValue = 0;
                        }

                        if ($qtyValue > 0) {
//                             echo("masuk");
                            $QUParcel = new QuotationParcel();
                            $QUParcel->QuotationInternalID = $header->InternalID;
                            $QUParcel->ParcelInternalID = $tamp[0];
                            $QUParcel->DescriptionInternalID = $description->InternalID;
                            $QUParcel->Qty = $qtyValue;
                            $QUParcel->Price = $priceValue;
                            $QUParcel->Discount = Input::get('discount' . ($indexterakhirinventory))[$c];
                            $QUParcel->VAT = $vatValue;
                            $QUParcel->DiscountNominal = $discValue;
                            $QUParcel->SubTotal = $subTotal;
                            $QUParcel->CompanyInternalID = Auth::user()->Company->InternalID;
                            $QUParcel->UserRecord = Auth::user()->UserID;
                            $QUParcel->UserModified = "0";
                            $QUParcel->Remark = "-";
                            $QUParcel->save();

                            foreach ($parcelDetail as $dataParcel) {
                                $uom1 = $dataParcel->UomInternalID;
                                $qtyValue1 = $dataParcel->Qty * $qtyValue;
                                $priceValue1 = $dataParcel->Price;
                                $subTotal1 = ($priceValue1 * $qtyValue1);
                                if (Input::get('vat') == '1') {
                                    $vatValue1 = $subTotal1 / 10;
                                } else {
                                    $vatValue1 = 0;
                                }
                                $detail = new QuotationDetail();
                                $detail->QuotationInternalID = $header->InternalID;
                                $detail->InventoryInternalID = $dataParcel->InventoryInternalID;
                                $detail->DescriptionInternalID = $description->InternalID;
                                $detail->UomInternalID = $uom1;
                                $detail->QuotationParcelInternalID = $QUParcel->InternalID;
                                $detail->Qty = $qtyValue1;
                                $detail->Price = $priceValue1;
                                $detail->Discount = 0;
                                $detail->DiscountNominal = 0;
                                $detail->VAT = $vatValue1;
                                $detail->SubTotal = $subTotal1;
                                $detail->UserRecord = Auth::user()->UserID;
                                $detail->UserModified = '0';
                                $detail->save();
                            }
                        }
                        $total += $subTotal1;
                    }
                }
            }
//            exit();
            $messages = 'suksesInsert';
            $error = '';
            $print = Route('quotationPrint', $header->QuotationID);
        }
        $quotation = $this->createID(0) . '.';
        if ($messages == 'suksesInsert') {
            return Redirect::route('quotationDetail', $header->QuotationID)->with("msg", "print");
        }
        return View::make('quotation.quotationNew')
                        ->withToogle('transaction')->withAktif('quotation')
                        ->withQuotation($quotation)
                        ->withError($error)
                        ->withPrint($print)
                        ->withMessages($messages);
    }

    public function updateQuotation($id) {
        //tipe
        $headerUpdate = QuotationHeader::find($id);
        $detailUpdate = QuotationHeader::find($id)->quotationDetail()->get();

        //rule
        if (Input::get('isCash') == '0') {
            $rule = array(
//                'uom' => 'required',
                'coa6' => 'required',
//                'remark' => 'required|max:1000',
                'currency' => 'required',
                'warehouse' => 'required',
                'rate' => 'required',
                'inventoryDescription' => 'required',
                'salesman' => 'required'
            );
            $longTerm = 0;
        } else {
            $rule = array(
                'longTerm' => 'required|integer',
//                'uom' => 'required',
                'coa6' => 'required',
//                'remark' => 'required|max:1000',
                'currency' => 'required',
                'warehouse' => 'required',
                'rate' => 'required',
                'inventoryDescription' => 'required',
                'salesman' => 'required'
            );
            $longTerm = Input::get('longTerm');
        }

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalUpdate';
            $error = $validator->messages();
        } else {
            $header = QuotationHeader::find(Input::get('QuotationInternalID'));
//            $header->ACC6InternalID = Input::get('coa6');
//            $header->SalesManInternalID = Input::get('salesman');
//            $header->AttnInternalID = Input::get('attn');
            if (Input::get('jeniscustomer') == 'cuslama') {
                $header->TypeCustomer = 0;
                $header->ACC6InternalID = Input::get('coa6');
                $header->SalesManInternalID = Input::get('salesman');
                $header->AttnInternalID = Input::get('attn');
            } else {
                $header->TypeCustomer = 1;
                $header->CustomerName = Input::get('name');
                $header->CustomerAddress = Input::get('address');
                $header->CustomerPhone = Input::get('phone');
                $header->CustomerEmail = Input::get('email');
                $header->CustomerCP = Input::get('cp');
                $header->SalesManInternalID = Input::get('salesman_baru');
            }
            $header->isCash = Input::get('isCash');
            $header->LongTerm = $longTerm;
            $currency = explode('---;---', Input::get('currency'));
            $header->WarehouseInternalID = Input::get('warehouse');
            $header->CurrencyInternalID = $currency[0];
            $header->CurrencyRate = str_replace(',', '', Input::get('rate'));
            if (Input::get('vat') == '') {
                $header->VAT = '0';
            } else {
                $header->VAT = Input::get('vat');
            }
//            $header->DownPayment = str_replace(",", "",Input::get('downPayment'));
            $header->DiscountGlobal = str_replace(",", "", Input::get('DiscountGlobal'));
            $header->GrandTotal = Input::get('grandTotalValue');
            $header->DeliveryTerms = Input::get('DeliveryTerms');
            $header->DeliveryTime = Input::get('DeliveryTime');
            $header->TermOfPayment = Input::get('TermOfPayment');
            $header->RefNumber = Input::get('RefNumber');
            $header->Validity = Input::get('Validity');
            $header->UserModified = Auth::user()->UserID;
            $header->Remark = Input::get('remark');
            $header->save();

            QuotationDetail::where('QuotationInternalID', '=', Input::get('QuotationInternalID'))->update(array('is_deleted' => 1));
            QuotationParcel::where('QuotationInternalID', '=', Input::get('QuotationInternalID'))->update(array('is_deleted' => 1));
            QuotationDescription::where('QuotationInternalID', '=', Input::get('QuotationInternalID'))->update(array('is_deleted' => 1));
            $indexterakhirinventory = 0;
            //update Desctiption
            for ($a = 0; $a < count(Input::get('inventoryDescription')); $a++) {
                $uomDescriptionValue = Input::get('uomDescription')[$a];
                $spesifikasi = Input::get('spesifikasi')[$a];
                $qtyDescriptionValue = str_replace(',', '', Input::get('qtyDescription')[$a]);
                $priceDescriptionValue = str_replace(',', '', Input::get('priceDescription')[$a]);
                $discDescriptionValue = str_replace(',', '', Input::get('discountNominalDescription')[$a]);
                $subTotalDescription = ($priceDescriptionValue * $qtyDescriptionValue) - (($priceDescriptionValue * $qtyDescriptionValue) * Input::get('discountDescription')[$a] / 100) - $discDescriptionValue * $qtyDescriptionValue;
                if (Input::get('vat') == '1') {
                    $vatValue = $subTotalDescription / 10;
                } else {
                    $vatValue = 0;
                }
//                $disk = Input::get('discountDescription')[$a];
                $disk = 0.00;
                if ($qtyDescriptionValue > 0) {
                    $description = new QuotationDescription();
                    $description->QuotationInternalID = $header->InternalID;
                    $description->InventoryText = Input::get('inventoryDescription')[$a];
                    $description->Spesifikasi = $spesifikasi;
                    $description->UomText = $uomDescriptionValue;
                    $description->Qty = $qtyDescriptionValue;
                    $description->Price = $priceDescriptionValue;
                    $description->Discount = $disk;
                    $description->DiscountNominal = $discDescriptionValue;
                    $description->VAT = $vatValue;
                    $description->SubTotal = $subTotalDescription;
                    $description->UserRecord = Auth::user()->UserID;
                    $description->UserModified = '0';
                    $description->save();
                }
                //insert detail
                $plus = 1;
                if (count(Input::get('inventory' . ($indexterakhirinventory + $plus))) == 0) {
                    while (count(Input::get('inventory' . ($indexterakhirinventory + $plus))) == 0) {
                        $plus++;
                    }
                }
                $indexterakhirinventory = $indexterakhirinventory + $plus;
                $total = 0;
                for ($c = 0; $c < count(Input::get('inventory' . ($indexterakhirinventory))); $c++) {
                    $tamp = explode("---;---", Input::get('inventory' . ($indexterakhirinventory))[$c]);
                    if ($tamp[1] != "parcel") {
                        //jika bukan paket
                        $qtyValue = str_replace(',', '', Input::get('qty' . ($indexterakhirinventory))[$c]);
                        $priceValue = str_replace(',', '', Input::get('price' . ($indexterakhirinventory))[$c]);
                        $discValue = str_replace(',', '', Input::get('discountNominal' . ($indexterakhirinventory))[$c]);
                        $subTotal = ($priceValue * $qtyValue) - ($priceValue * $qtyValue) * Input::get('discount' . ($indexterakhirinventory))[$c] / 100 - $discValue * $qtyValue;
                        $total += $subTotal;
                        if (Input::get('vat') == '1') {
                            $vatValue = $subTotal / 10;
                        } else {
                            $vatValue = 0;
                        }
                        if ($qtyValue > 0) {
                            $detail = new QuotationDetail();
                            $detail->QuotationInternalID = Input::get('QuotationInternalID');
                            $detail->InventoryInternalID = Input::get('inventory' . ($indexterakhirinventory))[$c];
                            $detail->UomInternalID = Input::get('uom' . ($indexterakhirinventory))[$c];
                            $detail->DescriptionInternalID = $description->InternalID;
                            $detail->QuotationParcelInternalID = 0;
                            $detail->Qty = $qtyValue;
                            $detail->Price = $priceValue;
                            $detail->Discount = Input::get('discount' . ($indexterakhirinventory))[$c];
                            $detail->DiscountNominal = $discValue;
                            $detail->VAT = $vatValue;
                            $detail->SubTotal = $subTotal;
                            $detail->UserRecord = Auth::user()->UserID;
                            $detail->UserModified = Auth::user()->UserID;
                            $detail->save();
                        }
                        $total += $subTotal;
                    } else {
                        //jika paket
                        $parcelDetail = ParcelInventory::where("ParcelInternalID", $tamp[0])->where("CompanyInternalID", Auth::user()->Company->InternalID)->get();

                        $qtyValue = str_replace(',', '', Input::get('qty' . ($indexterakhirinventory))[$c]);
                        $priceValue = str_replace(',', '', Input::get('price' . ($indexterakhirinventory))[$c]);
                        $discValue = str_replace(',', '', Input::get('discountNominal' . ($indexterakhirinventory))[$c]);
                        $subTotal = ($priceValue * $qtyValue) - (($priceValue * $qtyValue) * Input::get('discount' . ($indexterakhirinventory))[$c] / 100) - $discValue * $qtyValue;
                        if (Input::get('vat') == '1') {
                            $vatValue = $subTotal / 10;
                        } else {
                            $vatValue = 0;
                        }
                        if ($qtyValue > 0) {
                            $QUParcel = new QuotationParcel();
                            $QUParcel->QuotationInternalID = $header->InternalID;
                            $QUParcel->ParcelInternalID = $tamp[0];
                            $QUParcel->DescriptionInternalID = $description->InternalID;
                            $QUParcel->Qty = $qtyValue;
                            $QUParcel->Price = $priceValue;
                            $QUParcel->Discount = Input::get('discount' . ($indexterakhirinventory))[$c];
                            $QUParcel->VAT = $vatValue;
                            $QUParcel->DiscountNominal = $discValue;
                            $QUParcel->SubTotal = $subTotal;
                            $QUParcel->CompanyInternalID = Auth::user()->CompanY->InternalID;
                            $QUParcel->UserRecord = Auth::user()->UserID;
                            $QUParcel->UserModified = "0";
                            $QUParcel->Remark = "-";
                            $QUParcel->save();

                            foreach ($parcelDetail as $dataParcel) {
                                $uom1 = $dataParcel->UomInternalID;
                                $qtyValue1 = $dataParcel->Qty * $qtyValue;
                                $priceValue1 = $dataParcel->Price;
                                $subTotal1 = ($priceValue1 * $qtyValue1);
                                if (Input::get('vat') == '1') {
                                    $vatValue1 = $subTotal1 / 10;
                                } else {
                                    $vatValue1 = 0;
                                }
                                $detail = new QuotationDetail();
                                $detail->QuotationInternalID = $header->InternalID;
                                $detail->InventoryInternalID = $dataParcel->InventoryInternalID;
                                $detail->UomInternalID = $uom1;
                                $detail->DescriptionInternalID = $description->InternalID;
                                $detail->QuotationParcelInternalID = $QUParcel->InternalID;
                                $detail->Qty = $qtyValue1;
                                $detail->Price = $priceValue1;
                                $detail->Discount = 0;
                                $detail->DiscountNominal = 0;
                                $detail->VAT = $vatValue1;
                                $detail->SubTotal = $subTotal1;
                                $detail->UserRecord = Auth::user()->UserID;
                                $detail->UserModified = Auth::user()->UserID;
                                $detail->save();
                            }
                        }
                        $total += $subTotal1;
                    }
                }
            }
            QuotationDetail::where('QuotationInternalID', '=', Input::get('QuotationInternalID'))->where('is_deleted', 1)->delete();
            QuotationParcel::where('QuotationInternalID', '=', Input::get('QuotationInternalID'))->where('is_deleted', 1)->delete();
            QuotationDescription::where('QuotationInternalID', '=', Input::get('QuotationInternalID'))->where('is_deleted', 1)->delete();
            $messages = 'suksesUpdate';
            $error = '';
        }

        if ($messages == 'suksesUpdate') {
            return Redirect::route('quotationDetail', $header->QuotationID)->with("msg", "print");
        }
        
        //tipe
        $header = QuotationHeader::find($id);
        $detail = QuotationHeader::find($id)->quotationDetail()->get();
        $description = QuotationHeader::find($id)->quotationDescription;
        $quotation = $this->createID(0) . '.';
        return View::make('quotation.quotationUpdate')
                        ->withToogle('transaction')->withAktif('quotation')->withHeader($header)
                        ->withDetail($detail)
                        ->withDescription($description)
                        ->withQuotation($quotation)->withError($error)
                        ->withMessages($messages);
    }

    public function copyQuotation($id) {
        $headerUpdate = QuotationHeader::find($id);
        //$detailUpdate = QuotationHeader::find($id)->quotationDetail()->get();
        //rule
        $jenis = Input::get('isCash');
        if ($jenis == '0') {
            $rule = array(
                'date' => 'required',
                'coa6' => 'required',
                'remark' => 'required|max:1000',
                'currency' => 'required',
                'warehouse' => 'required',
                'rate' => 'required',
                'inventoryDescription' => 'required'
            );
            $longTerm = 0;
        } else {
            $rule = array(
                'date' => 'required',
                'longTerm' => 'required|integer',
                'coa6' => 'required',
                'remark' => 'required|max:1000',
                'currency' => 'required',
                'warehouse' => 'required',
                'rate' => 'required',
                'inventoryDescription' => 'required'
            );
            $longTerm = Input::get('longTerm');
        }
        $quotationNumber = '';
        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalInsert';
            $error = $validator->messages();
        } else {
            //valid
            //insert header
            $tax = '';
            if (Input::get('vat') == '') {
                $tax = "NonTax";
            } else {
                $tax = "Tax";
            }
            $header = new QuotationHeader();
            $quotation = $this->createID(1, $tax) . '.';
            $date = explode('-', Input::get('date'));
            $yearDigit = substr($date[2], 2);
            $quotation .= $date[1] . $yearDigit . '.';
            $quotationNumber = QuotationHeader::getNextIDQuotation($quotation);
            $header->QuotationID = $quotationNumber;
            $header->QuotationDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $header->ACC6InternalID = Input::get('coa6');
            $header->LongTerm = $longTerm;
            $header->SalesManInternalID = Input::get('salesman');
            $header->AttnInternalID = Input::get('attn');
            $header->isCash = $jenis;
            $currency = explode('---;---', Input::get('currency'));
            $header->WarehouseInternalID = Input::get('warehouse');
            $header->CurrencyInternalID = $currency[0];
            $header->CurrencyRate = str_replace(',', '', Input::get('rate'));
            if (Input::get('vat') == '') {
                $header->VAT = '0';
            } else {
                $header->VAT = Input::get('vat');
            }
            if ($headerUpdate->Parent == '') {
                $header->Parent = $headerUpdate->QuotationID;
            } else {
                $header->Parent = $headerUpdate->Parent . ' - ' . $headerUpdate->QuotationID;
            }
            $header->DiscountGlobal = str_replace(",", "", Input::get('DiscountGlobal'));
            $header->GrandTotal = Input::get('grandTotalValue');
            $header->DeliveryTerms = Input::get('DeliveryTerms');
            $header->DeliveryTime = Input::get('DeliveryTime');
            $header->Validity = Input::get('Validity');
            $header->TermOfPayment = Input::get('TermOfPayment');
            $header->RefNumber = Input::get('RefNumber');
            $header->Print = 1;
            $header->UserRecord = Auth::user()->UserID;
            $header->CompanyInternalID = Auth::user()->Company->InternalID;
            $header->UserModified = '0';
            $header->Remark = Input::get('remark');
            $header->save();
            //insert detail
            $total = 0;

            //insert quotation description
            for ($a = 0; $a < count(Input::get('inventoryDescription')); $a++) {
                $uomDescriptionValue = Input::get('uomDescription')[$a];
                $spesifikasi = Input::get('spesifikasi')[$a];
                $qtyDescriptionValue = str_replace(',', '', Input::get('qtyDescription')[$a]);
                $priceDescriptionValue = str_replace(',', '', Input::get('priceDescription')[$a]);
                $discDescriptionValue = str_replace(',', '', Input::get('discountNominalDescription')[$a]);
                $subTotalDescription = ($priceDescriptionValue * $qtyDescriptionValue) - (($priceDescriptionValue * $qtyDescriptionValue) * Input::get('discountDescription')[$a] / 100) - $discDescriptionValue * $qtyDescriptionValue;
                if (Input::get('vat') == '1') {
                    $vatValue = $subTotalDescription / 10;
                } else {
                    $vatValue = 0;
                }
                if ($qtyDescriptionValue > 0) {
                    $description = new QuotationDescription();
                    $description->QuotationInternalID = $header->InternalID;
                    $description->InventoryText = Input::get('inventoryDescription')[$a];
                    $description->UomText = $uomDescriptionValue;
                    $description->Spesifikasi = $spesifikasi;
                    $description->Qty = $qtyDescriptionValue;
                    $description->Price = $priceDescriptionValue;
                    $description->Discount = Input::get('discountDescription')[$a];
                    $description->DiscountNominal = $discDescriptionValue;
                    $description->VAT = $vatValue;
                    $description->SubTotal = $subTotalDescription;
                    $description->UserRecord = Auth::user()->UserID;
                    $description->UserModified = '0';
                    $description->save();
                }

                for ($c = 0; $c < count(Input::get('inventory' . ($a + 1))); $c++) {
//                    echo '<pre>';
//                    print_r(Input::get('inventory' . ($a + 1))[$c]);
//                    echo '</pre>';
                    $tamp = explode("---;---", Input::get('inventory' . ($a + 1))[$c]);
                    if ($tamp[1] != "parcel") {
                        //jika bukan paket
                        $qtyValue = str_replace(',', '', Input::get('qty' . ($a + 1))[$c]);
                        $priceValue = str_replace(',', '', Input::get('price' . ($a + 1))[$c]);
                        $discValue = str_replace(',', '', Input::get('discountNominal' . ($a + 1))[$c]);
                        $subTotal = ($priceValue * $qtyValue) - (($priceValue * $qtyValue) * Input::get('discount' . ($a + 1))[$c] / 100) - $discValue * $qtyValue;
                        if (Input::get('vat') == '1') {
                            $vatValue = $subTotal / 10;
                        } else {
                            $vatValue = 0;
                        }
                        if ($qtyValue > 0) {
//                            echo("masuk");
                            $detail = new QuotationDetail();
                            $detail->QuotationInternalID = $header->InternalID;
                            $detail->InventoryInternalID = Input::get('inventory' . ($a + 1))[$c];
                            $detail->DescriptionInternalID = $description->InternalID;
                            $detail->UomInternalID = Input::get('uom' . ($a + 1))[$c];
                            $detail->Qty = $qtyValue;
                            $detail->Price = $priceValue;
                            $detail->Discount = Input::get('discount' . ($a + 1))[$c];
                            $detail->DiscountNominal = $discValue;
                            $detail->VAT = $vatValue;
                            $detail->SubTotal = $subTotal;
                            $detail->UserRecord = Auth::user()->UserID;
                            $detail->UserModified = '0';
                            $detail->save();
                        }
                        $total += $subTotal;
                    } else {
                        //jika paket
                        $parcelDetail = ParcelInventory::where("ParcelInternalID", $tamp[0])->where("CompanyInternalID", Auth::user()->Company->InternalID)->get();

                        $qtyValue = str_replace(',', '', Input::get('qty' . ($a + 1))[$c]);
                        $priceValue = str_replace(',', '', Input::get('price' . ($a + 1))[$c]);
                        $discValue = str_replace(',', '', Input::get('discountNominal' . ($a + 1))[$c]);
                        $subTotal = ($priceValue * $qtyValue) - (($priceValue * $qtyValue) * Input::get('discount' . ($a + 1))[$c] / 100) - $discValue * $qtyValue;
                        if (Input::get('vat') == '1') {
                            $vatValue = $subTotal / 10;
                        } else {
                            $vatValue = 0;
                        }

                        if ($qtyValue > 0) {
//                             echo("masuk");
                            $QUParcel = new QuotationParcel();
                            $QUParcel->QuotationInternalID = $header->InternalID;
                            $QUParcel->ParcelInternalID = $tamp[0];
                            $QUParcel->DescriptionInternalID = $description->InternalID;
                            $QUParcel->Qty = $qtyValue;
                            $QUParcel->Price = $priceValue;
                            $QUParcel->Discount = Input::get('discount' . ($a + 1))[$c];
                            $QUParcel->VAT = $vatValue;
                            $QUParcel->DiscountNominal = $discValue;
                            $QUParcel->SubTotal = $subTotal;
                            $QUParcel->CompanyInternalID = Auth::user()->Company->InternalID;
                            $QUParcel->UserRecord = Auth::user()->UserID;
                            $QUParcel->UserModified = "0";
                            $QUParcel->Remark = "-";
                            $QUParcel->save();

                            foreach ($parcelDetail as $dataParcel) {
                                $uom1 = $dataParcel->UomInternalID;
                                $qtyValue1 = $dataParcel->Qty * $qtyValue;
                                $priceValue1 = $dataParcel->Price;
                                $subTotal1 = ($priceValue1 * $qtyValue1);
                                if (Input::get('vat') == '1') {
                                    $vatValue1 = $subTotal1 / 10;
                                } else {
                                    $vatValue1 = 0;
                                }
                                $detail = new QuotationDetail();
                                $detail->QuotationInternalID = $header->InternalID;
                                $detail->InventoryInternalID = $dataParcel->InventoryInternalID;
                                $detail->DescriptionInternalID = $description->InternalID;
                                $detail->UomInternalID = $uom1;
                                $detail->QuotationParcelInternalID = $QUParcel->InternalID;
                                $detail->Qty = $qtyValue1;
                                $detail->Price = $priceValue1;
                                $detail->Discount = 0;
                                $detail->DiscountNominal = 0;
                                $detail->VAT = $vatValue1;
                                $detail->SubTotal = $subTotal1;
                                $detail->UserRecord = Auth::user()->UserID;
                                $detail->UserModified = '0';
                                $detail->save();
                            }
                        }
                        $total += $subTotal1;
                    }
                }
            }
//            exit();
            $messages = 'suksesInsert';
            $error = '';
        }
        $header = QuotationHeader::find($header->InternalID);
        $detail = QuotationHeader::find($id)->quotationDetail()->get();
        $description = QuotationHeader::find($id)->quotationDescription;
        $quotation = $this->createID(0) . '.';
        $print = Route('quotationPrint', $header->QuotationID);
//        return View::make('quotation.quotationNew')
//                        ->withToogle('transaction')->withAktif('quotation')
//                        ->withQuotation($quotation)
//                        ->withError($error)
//                        //->withPrint($print)
//                        ->withMessages($messages);
        return Redirect::action('QuotationController@quotationUpdate', $header->QuotationID);
    }

    public function deleteQuotation() {
        //cek dulu dipakai atau ga
        $quotation = QuotationSales::where('QuotationInternalID', Input::get('InternalID'))->count();
        if (is_null($quotation) == 0) {
            //tidak ada yang menggunakan data quotation maka data boleh dihapus
            $quotationHeader = QuotationHeader::find(Input::get('InternalID'));
            if ($quotationHeader->CompanyInternalID == Auth::user()->Company->InternalID) {
                //hapus detil
                $detilData = QuotationHeader::find(Input::get('InternalID'))->quotationDetail;
                foreach ($detilData as $value) {
                    $detil = quotationDetail::find($value->InternalID);
                    $detil->delete();
                }
                $descriptionData = QuotationHeader::find(Input::get('InternalID'))->quotationDescription;
                foreach ($descriptionData as $value) {
                    $description = quotationDescription::find($value->InternalID);
                    $description->delete();
                }
                //hapus quotation parcel
                QuotationParcel::where("QuotationInternalID", Input::get('InternalID'))->delete();
                //hapus quotation
                $quotation = QuotationHeader::find(Input::get('InternalID'));
                $soParcel = QuotationParcel::where("QuotationInternalID", $quotation->InternalID)->delete();
                $quotation->delete();
                $messages = 'suksesDelete';
            } else {
                $messages = 'accessDenied';
            }
        } else {
            //ada maka data tidak boleh dihapus
            $messages = 'gagalDelete';
        }
        $data = QuotationHeader::advancedSearch(Input::get('coa6'), Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
        return View::make('quotation.quotationSearch')
                        ->withToogle('transaction')->withAktif('quotation')
                        ->withMessages($messages)
                        ->withData($data);
    }

    function quotationPrint($id) {
        $id = QuotationHeader::getIdquotation($id);
        $header = QuotationHeader::find($id);
        $header->Print = $header->Print + 1;
        $header->save();
        $detail = QuotationHeader::find($id)->quotationDetail()->get();
        $description = QuotationDescription::where('QuotationInternalID', $id)->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            if ($header->TypeCustomer == 0) {
                $coa6 = QuotationHeader::find($header->InternalID)->coa6;
                if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-')
                    $cp = 'CP: ' . $coa6->ContactPerson . '<br>';
                else
                    $cp = '';
                $phone = $coa6->Phone;
                $fax = $coa6->Fax;
                if ($header->AttnInternalID == null || CustomerDetail::find($header->AttnInternalID)->Email == null || CustomerDetail::find($header->AttnInternalID)->Email == '' || CustomerDetail::find($header->AttnInternalID)->Email == '-')
                    $email = $coa6->Email;
                else
                    $email = CustomerDetail::find($header->AttnInternalID)->Email;
                $customer = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . '<br>Phone: ' . $phone . '<br>Fax: ' . $fax . '<br>Email: ' . $email;
                $name = $coa6->ACC6Name;
            }else {
                $customer = $header->CustomerName . '<br>Phone: ' . $header->CustomerPhone . '<br>Email: ' . $header->CustomerEmail;
                $name = $header->CustomerName;
            }
            if (count($header->AttnInternalID) > 0 && $header->AttnInternalID != null && $header->AttnInternalID != '' && $header->AttnInternalID != 0) {
                $attnleng = CustomerDetail::find($header->AttnInternalID);
//                dd($header->AttnInternalID);
                $attn = $attnleng->CustomerDetailID . ' ' . $attnleng->DetailName;
            } elseif ($header->CustomerCP != null && $header->CustomerCP != '') {
                $attn = $header->CustomerCP;
            } else {
                $attn = '-';
            }
            if ($header->isCash == 0) {
                $payment = 'Cash';
            } else if ($header->isCash == 1) {
                $payment = 'Credit';
            } else if ($header->isCash == 2) {
                $payment = 'CBD';
            } else if ($header->isCash == 3) {
                $payment = 'Deposit';
            } else {
                $payment = 'Down Payment';
            }
            $currency = Currency::find($header->CurrencyInternalID);
            $currencyName = $currency->CurrencyName;
            if ($header->VAT == 0) {
                $vat = 'Non Tax';
            } else {
                $vat = 'Tax';
            }
            $html = '
            <html>
                <head>
                    <style>
                    <!--
            @page { size :  9.4in 5.5in; }
            table { page : 9.4in 5.5in; }
            -->
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 6px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 195px;border-bottom:1px solid #ddd;margin-bottom:20px;">
                        <div style="height: 133px">
                        <img height="100%" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                        </div>
                        <div style="width: 420px; float: left;">
                            <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->Company->CompanyName . '</b>
                        </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Address</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Quotation</h5>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                            <table>
                            <tr>
                            <td width="275px">
                                <table>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Customer</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $customer . '</td>
                                 </tr>';
//            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-') {
//                $html .= '     <tr style="background: none;">
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Contact Person</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;"></td>
//                                 </tr>';
//            }
            $html .= '
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Telp</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;"></td>
                                 </tr>                                
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;"></td>
                                 </tr>                                
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Email</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;"></td>
                                 </tr>                                
</table>
                                </td>
                                <td>
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Quotation ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->QuotationID . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->QuotationDate)) . '</td>
                                 </tr>
                                 <tr><td><br></td></tr>
                                 <tr><td><br></td></tr>
                                </table></td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">No.</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="25%">Inventory</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="10%">Uom</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Qty</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Price</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc (%)</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            $total = 0;
            $totalVAT = 0;
            $counter = 1;
            if (count($description) > 0) {
                foreach ($description as $data) {
                    $html .= '<tr>
                                <td rowspan="' . (1 + ($data->Spesifikasi != '' ? 1 : 0)) . '" style="vertical-align: top;font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $counter . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $data->InventoryText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->UomText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
                    if ($data->Spesifikasi != '') {
                        $html .= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . nl2br($data->Spesifikasi) . '</td>
                            </tr>';
                    }
                    $counter++;
                    $totalVAT += $data->VAT;
                    $total += $data->SubTotal;
                }
                if ($totalVAT != 0) {
                    $totalVAT = $totalVAT - ($header->DiscountGlobal * 0.1);
                }
            } else {
                $html .= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this quotation.</td>
                        </tr>';
            }
            $html .= '</tbody>
                            </table>
                            <table style="width:100%;text-align:right">
                            <tr><td style="width: 90%"> 
                            <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Delivery Terms</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $header->DeliveryTerms . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Delivery Time</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $header->DeliveryTime . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Terms Of Payment</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $payment . '</td>
                                 </tr>
                                  <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Validity</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $header->Validity . '</td>
                                 </tr>
                                  <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Remark</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $header->Remark . '</td>
                                 </tr>
                                </table></td><td style="width: 10%;text-align:right;">                       
                                <table style="margin-left:20%; width:80%">
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Discount</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Grand Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total - $header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr><td><br></td></tr>
                                </table>
                           </td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr>
                            <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">(  ' . Auth::user()->UserName . '  )</td>
                            <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">(.........................)</td>
                            </tr>
                            </table>
                    </div>
                </body>
            </html>';
            //return PDF ::load($html, 'A5', 'landscape')->show();


            $data['header'] = $header;
            $data['customer'] = $customer;
            $data['attn'] = $attn;
            $data['vat'] = $vat;
            $data['id'] = $id;
            $data['description'] = $description;
            $data['currencyName'] = $currencyName;
            $data['payment'] = $payment;
            $data['detail'] = $detail;
            $pdf = PDFview::loadView('template.print.quotationPrint', $data);
            return $pdf->download($header->QuotationID . '-' . $name . '.pdf');
//            return $pdf->download('quotationPrint.pdf');
//            return View::make('template.print.quotationPrint')
//                            ->with('header', $header)
//                            ->with('customer', $customer)
//                            ->with('attn', $attn)
//                            ->with('vat', $vat)
//                            ->with('id', $id)
//                            ->with('description', $description)
//                            ->with('currencyName', $currencyName)
//                            ->with('payment', $payment)
//                            ->with('detail', $detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showQuotation');
        }
    }

    function quotationIncludePrint($id) {
        $id = QuotationHeader::getIdquotation($id);
        $header = QuotationHeader::find($id);
        $header->Print = $header->Print + 1;
        $header->save();
        $detail = QuotationHeader::find($id)->quotationDetail()->get();
        $description = QuotationDescription::where('QuotationInternalID', $id)->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            if ($header->TypeCustomer == 0) {
                $coa6 = QuotationHeader::find($header->InternalID)->coa6;
                if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-')
                    $cp = 'CP: ' . $coa6->ContactPerson . '<br>';
                else
                    $cp = '';
                $customer = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . '<br>Phone: ' . $coa6->Phone . '<br>Fax: ' . $coa6->Fax . '<br>Email: ' . $coa6->Email;
                $name = $coa6->ACC6Name;
            }else {
                $customer = $header->CustomerName . '<br>Phone: ' . $header->CustomerPhone . '<br>Email: ' . $header->CustomerEmail;
                $name = $header->CustomerName;
            }
            if (count($header->AttnInternalID) > 0 && $header->AttnInternalID != null && $header->AttnInternalID != '' && $header->AttnInternalID != 0) {
                $attnleng = CustomerDetail::find($header->AttnInternalID);
                $attn = $attnleng->CustomerDetailID . ' ' . $attnleng->DetailName;
            } elseif ($header->CustomerCP != null && $header->CustomerCP != '') {
                $attn = $header->CustomerCP;
            } else {
                $attn = '-';
            }
            if ($header->isCash == 0) {
                $payment = 'Cash';
            } else if ($header->isCash == 1) {
                $payment = 'Credit';
            } else if ($header->isCash == 2) {
                $payment = 'CBD';
            } else if ($header->isCash == 3) {
                $payment = 'Deposit';
            } else {
                $payment = 'Down Payment';
            }
            $currency = Currency::find($header->CurrencyInternalID);
            $currencyName = $currency->CurrencyName;
            if ($header->VAT == 0) {
                $vat = 'Non Tax';
            } else {
                $vat = 'Tax';
            }
            $html = '
            <html>
                <head>
                    <style>
                    <!--
            @page { size :  9.4in 5.5in; }
            table { page : 9.4in 5.5in; }
            -->
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 6px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 195px;border-bottom:1px solid #ddd;margin-bottom:20px;">
                        <div style="height: 133px">
                        <img height="100%" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                        </div>
                        <div style="width: 420px; float: left;">
                            <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->Company->CompanyName . '</b>
                        </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Address</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Quotation</h5>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                            <table>
                            <tr>
                            <td width="275px">
                                <table>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Customer</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $customer . '</td>
                                 </tr>';
//            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-') {
//                $html .= '     <tr style="background: none;">
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Contact Person</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;"></td>
//                                 </tr>';
//            }
            $html .= '
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Telp</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;"></td>
                                 </tr>                                
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;"></td>
                                 </tr>                                
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Email</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;"></td>
                                 </tr>                                
</table>
                                </td>
                                <td>
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Quotation ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->QuotationID . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->QuotationDate)) . '</td>
                                 </tr>
                                 <tr><td><br></td></tr>
                                 <tr><td><br></td></tr>
                                </table></td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">No.</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="25%">Inventory</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="10%">Uom</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Qty</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Price</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc (%)</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            $total = 0;
            $counter = 1;
            $price = 0;
            $discount = 0;
            $subtotal = 0;
            if (count($description) > 0) {
                foreach ($description as $data) {
                    if ($header->VAT == 1) {
                        $price = $data->Price + ($data->Price * 0.1);
                        $discount = $data->DiscountNominal + ($data->DiscountNominal * 0.1);
                    } else {
                        $price = $data->Price;
                        $discount = $data->DiscountNominal;
                    }
                    $subtotal = ($price - $discount) * $data->Qty;
                    $subtotal = $subtotal - (($data->Discount / 100) * $subtotal);
                    $html .= '<tr>
                                <td rowspan="' . (1 + ($data->Spesifikasi != '' ? 1 : 0)) . '" style="vertical-align: top;font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $counter . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $data->InventoryText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->UomText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($discount, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($subtotal, '2', '.', ',') . '</td>
                            </tr>';
                    if ($data->Spesifikasi != '') {
                        $html .= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . nl2br($data->Spesifikasi) . '</td>
                            </tr>';
                    }
                    $counter++;
                    $total += $subtotal;
                }
            } else {
                $html .= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this quotation.</td>
                        </tr>';
            }
            $html .= '</tbody>
                            </table>
                            <table style="width:100%;text-align:right">
                            <tr><td style="width: 90%"> 
                            <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Delivery Terms</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $header->DeliveryTerms . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Delivery Time</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $header->DeliveryTime . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Terms Of Payment</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $payment . '</td>
                                 </tr>
                                  <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Validity</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $header->Validity . '</td>
                                 </tr>
                                  <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Remark</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $header->Remark . '</td>
                                 </tr>
                                </table></td><td style="width: 10%;text-align:right;">                       
                                <table style="margin-left:20%; width:80%">
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Discount</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Grand Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total - $header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr><td><br></td></tr>
                                </table>
                           </td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr>
                            <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">(  ' . Auth::user()->UserName . '  )</td>
                            <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">(.........................)</td>
                            </tr>
                            </table>
                    </div>
                </body>
            </html>';
            //return PDF ::load($html, 'A5', 'landscape')->show();
//            return View::make('template.print.quotationIncludePrint')
//                            ->with('header', $header)
//                            ->with('customer', $customer)
//                            ->with('attn', $attn)
//                            ->with('vat', $vat)
//                            ->with('id', $id)
//                            ->with('description', $description)
//                            ->with('currencyName', $currencyName)
//                            ->with('payment', $payment)
//                            ->with('detail', $detail);

            $data['header'] = $header;
            $data['customer'] = $customer;
            $data['attn'] = $attn;
            $data['vat'] = $vat;
            $data['id'] = $id;
            $data['description'] = $description;
            $data['currencyName'] = $currencyName;
            $data['payment'] = $payment;
            $data['detail'] = $detail;
            $pdf = PDFview::loadView('template.print.quotationIncludePrint', $data);
            return $pdf->download($header->QuotationID . '_include.pdf');
//            return $pdf->download('quotationIncludePrint.pdf');
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showQuotation');
        }
    }

    function quotationInternalPrint($id) {
        $id = QuotationHeader::getIdquotation($id);
        $header = QuotationHeader::find($id);
        $detail = QuotationHeader::find($id)->quotationDetail()->get();
        $description = QuotationDescription::where('QuotationInternalID', $id)->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            if ($header->TypeCustomer == 0) {
                $coa6 = QuotationHeader::find($header->InternalID)->coa6;
                if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-')
                    $cp = 'CP: ' . $coa6->ContactPerson . '<br>';
                else
                    $cp = '';
                $customer = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . '<br>Phone: ' . $coa6->Phone . '<br>Fax: ' . $coa6->Fax . '<br>Email: ' . $coa6->Email;
                $name = $coa6->ACC6Name;
            }else {
                $customer = $header->CustomerName . '<br>Phone: ' . $header->CustomerPhone . '<br>Email: ' . $header->CustomerEmail;
                $name = $header->CustomerName;
            }
            if (count($header->AttnInternalID) > 0 && $header->AttnInternalID != null && $header->AttnInternalID != '' && $header->AttnInternalID != 0) {
                $attnleng = CustomerDetail::find($header->AttnInternalID);
                $attn = $attnleng->CustomerDetailID . ' ' . $attnleng->DetailName;
            } elseif ($header->CustomerCP != null && $header->CustomerCP != '') {
                $attn = $header->CustomerCP;
            } else {
                $attn = '-';
            }
            if ($header->isCash == 0) {
                $payment = 'Cash';
            } else if ($header->isCash == 1) {
                $payment = 'Credit';
            } else if ($header->isCash == 2) {
                $payment = 'CBD';
            } else if ($header->isCash == 3) {
                $payment = 'Deposit';
            } else {
                $payment = 'Down Payment';
            }
            $currency = Currency::find($header->CurrencyInternalID);
            $currencyName = $currency->CurrencyName;
            if ($header->VAT == 0) {
                $vat = 'Non Tax';
            } else {
                $vat = 'Tax';
            }
            $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 6px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 70px;border-bottom:1px solid #ddd;margin-bottom:20px;">
                        <div style="width: 420px; float: left;">
                            <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->Company->CompanyName . '</b>
                        </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Address</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Quotation</h5>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                            <table>
                            <tr>
                            <td width="275px">
                                <table>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Customer</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $customer . '</td>
                                 </tr>';
//            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-') {
//                $html .= '     <tr style="background: none;">
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Contact Person</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;"></td>
//                                 </tr>';
//            }
            $html .= '
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Telp</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;"></td>
                                 </tr>                                
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;"></td>
                                 </tr>                                
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Email</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;"></td>
                                 </tr>                                
</table>
                                </td>
                                <td>
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Quotation ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->QuotationID . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->QuotationDate)) . '</td>
                                 </tr>
                                 <tr><td></td></tr>
                                 <tr><td></td></tr>
                                 <tr><td><br></td></tr>
                                </table></td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">No.</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="25%">Inventory</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="10%">Uom</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Qty</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Price</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc (%)</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            $total = 0;
            $totalVAT = 0;
            $counter = 1;
            if (count($description) > 0) {
                foreach ($description as $data) {
                    $detail = QuotationDetail::where('DescriptionInternalID', $data->InternalID)->where('QuotationParcelInternalID', 0)->get();
                    $parcel = QuotationParcel::where('DescriptionInternalID', $data->InternalID)->get();
                    $html .= '<tr>
                                <td rowspan="' . (1 + ($data->Spesifikasi != '' ? 1 : 0) + count($detail) + count($parcel)) . '" style="vertical-align: top;font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $counter . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;"><b>' . $data->InventoryText . '</b></td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->UomText . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
                    if ($data->Spesifikasi != '') {
                        $html .= '<tr>
                                <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . nl2br($data->Spesifikasi) . '</td>
                            </tr>';
                    }
                    foreach ($detail as $data2) {
                        $html .= '<tr>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;"><i>' . $data2->inventory->InventoryName . '</i></td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data2->uom->UomID . '</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data2->Qty, 0, '.', ',') . '</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data2->Price, '2', '.', ',') . '</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data2->Discount . '' . '</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data2->DiscountNominal, '2', '.', ',') . '</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data2->SubTotal, '2', '.', ',') . '</td>
                                </tr>';
                    }
                    foreach ($parcel as $data2) {
                        $html .= '<tr>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;"><i>' . $data2->parcel->ParcelName . '</i></td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">-</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data2->Qty, 0, '.', ',') . '</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data2->Price, '2', '.', ',') . '</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data2->Discount . '' . '</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data2->DiscountNominal, '2', '.', ',') . '</td>
                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data2->SubTotal, '2', '.', ',') . '</td>
                                </tr>';
                    }
                    $totalVAT += $data->VAT;
                    $total += $data->SubTotal;
                    $counter++;
                }
                if ($totalVAT != 0) {
                    $totalVAT = $totalVAT - ($header->DiscountGlobal * 0.1);
                }
            } else {
                $html .= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this quotation.</td>
                        </tr>';
            }
            $html .= '</tbody>
                            </table>
                            <table>
                            <tr><td>
                            <div style="box-sizing: border-box;min-width: 300px; display: inline-block; clear: both;">   
                            <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Delivery Terms</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $header->DeliveryTerms . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Delivery Time</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $header->DeliveryTime . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Terms Of Payment</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $payment . '</td>
                                 </tr>
                                  <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Validity</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $header->Validity . '</td>
                                 </tr>
                                  <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Remark</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $header->Remark . '</td>
                                 </tr>
                                </table></div></td><td>
                            <div style="box-sizing: border-box;min-width: 200px; margin-left: 130px; display: inline-block; clear: both;">                                
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Discount</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Grand Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total - $header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr><td><br></td></tr>
                                </table>
                            </div></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr><td><br></td></tr>
                            <tr>
                            <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">(  ' . Auth::user()->UserName . '  )</td>
                            <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">(.........................)</td>
                            </tr>
                            </table>
                    </div>
                </body>
            </html>';
//            return PDF ::load($html, 'A5', 'portrait')->show();
//            return View::make('template.print.quotationInternalPrint')
//                            ->with('header', $header)
//                            ->with('customer', $customer)
//                            ->with('attn', $attn)
//                            ->with('vat', $vat)
//                            ->with('id', $id)
//                            ->with('description', $description)
//                            ->with('currencyName', $currencyName)
//                            ->with('payment', $payment)
//                            ->with('detail', $detail);

            $data['header'] = $header;
            $data['customer'] = $customer;
            $data['attn'] = $attn;
            $data['vat'] = $vat;
            $data['id'] = $id;
            $data['description'] = $description;
            $data['currencyName'] = $currencyName;
            $data['payment'] = $payment;
            $data['detail'] = $detail;
            $pdf = PDFview::loadView('template.print.quotationInternalPrint', $data);
            return $pdf->download($header->QuotationID . '_internal.pdf');
//            return $pdf->download('quotationInternalPrint.pdf');
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showQuotation');
        }
    }

//    function quotationInternalPrint($id) {
//        $header = QuotationHeader::find($id);
//        $header->Print = $header->Print + 1;
//        $header->save();
//        $detail = QuotationHeader::find($id)->quotationDetail()->get();
//        $description = QuotationDescription::where('QuotationInternalID', $id)->get();
//        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
//            $coa6 = QuotationHeader::find($header->InternalID)->coa6;
//            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-')
//                $cp = 'CP: ' . $coa6->ContactPerson . '<br>';
//            else
//                $cp = '';
//            $customer = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . '<br>' . $coa6->Phone . '<br>' . $coa6->Fax . '<br>' . $coa6->Email;
//            if ($header->isCash == 0) {
//                $payment = 'Cash';
//            } else {
//                $payment = 'Credit';
//            }
//            $currency = Currency::find($header->CurrencyInternalID);
//            $currencyName = $currency->CurrencyName;
//            if ($header->VAT == 0) {
//                $vat = 'Non Tax';
//            } else {
//                $vat = 'Tax';
//            }
//            $html = '
//            <html>
//                <head>
//                    <style>
//                        .tableBorder {
//                            border-spacing: 0;
//                            border: 0px;
//                        }
//                        .tableBorder th{
//                            padding: 3px;
//                            border-spacing: 0;
//                            border: 0.5px solid #ddd;
//                            text-align: center;
//                        }
//                        .tableBorder td{
//                            border-spacing: 0;
//                            padding: 8px;
//                            border: 0.5px solid #ddd;
//                        }
//                        .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
//                        .footer .page:after { 
//                        font-family: helvetica,sans-serif; font-size: 6px; content: "Page " counter(page);}
//                    </style>
//                </head>
//                <body>
//                    <div class="footer">
//                        <span class="page"></span>
//                    </div>
//                    <div style="padding:0px; box-sizing: border-box;">
//                        <div style="height: 70px;border-bottom:1px solid #ddd;margin-bottom:20px;">
//                        <div style="width: 420px; float: left;">
//                            <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->Company->CompanyName . '</b>
//                        </div> 
//                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 2px; float: right;">
//                                 <table>
//                                 <tr style="background: none;">
//                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Address</td>
//                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
//                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
//                                 </tr>
//                                <tr style="background: none;">
//                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
//                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
//                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
//                                 </tr>
//                                <tr style="background: none;">
//                                    <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Phone Number</td>
//                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
//                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
//                                 </tr>
//                                 </table>
//                            </div>           
//                        </div>
//                        <h5 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Quotation</h5>
//                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
//                            <table>
//                            <tr>
//                            <td width="275px">
//                                <table>
//                                <tr style="background: none;">
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Customer</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $customer . '</td>
//                                 </tr>';
//            if ($coa6->ContactPerson != null && $coa6->ContactPerson != '' && $coa6->ContactPerson != '-') {
//                $html .= '     <tr style="background: none;">
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Contact Person</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;"></td>
//                                 </tr>';
//            }
//            $html .= '
//                                <tr style="background: none;">
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Telp</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->Phone . '</td>
//                                 </tr>                                
//                                <tr style="background: none;">
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Fax</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->Fax . '</td>
//                                 </tr>                                
//                                <tr style="background: none;">
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Email</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $coa6->Email . '</td>
//                                 </tr>                                
//</table>
//                                </td>
//                                <td>
//                                <table>
//                                 <tr style="background: none;">
//                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Quotation ID</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->QuotationID . '</td>
//                                 </tr>
//                                <tr style="background: none;">
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Date</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->QuotationDate)) . '</td>
//                                 </tr>
//                                 <tr><td></td></tr>
//                                 <tr><td></td></tr>
//                                 <tr><td><br></td></tr>
//                                </table></td>
//                                </tr>
//                            </table>
//                        </div>    
//                            <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
//                                    <thead >
//                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
//                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">No.</th>
//                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="25%">Inventory</th>
//                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="10%">Uom</th>
//                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Qty</th>
//                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Price</th>
//                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc (%)</th>
//                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc</th>
//                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Subtotal</th>
//                                        </tr>
//                                    </thead>
//                                    <tbody>';
//            $total = 0;
//            $totalVAT = 0;
//            $counter = 1;
//            if (count($description) > 0) {
//                foreach ($description as $data) {
//                    $detail = QuotationDetail::where('DescriptionInternalID', $data->InternalID)->where('QuotationParcelInternalID', 0)->get();
//                    $parcel = QuotationParcel::where('DescriptionInternalID', $data->InternalID)->get();
//                    if ($header->VAT == 1) {
//                        $price = $data->Price + ($data->Price * 0.1);
//                        $discount = $data->DiscountNominal + ($data->DiscountNominal * 0.1);
//                    } else {
//                        $price = $data->Price;
//                        $discount = $data->DiscountNominal;
//                    }
//                    $subtotal = ($price - $discount) * $data->Qty;
//                    $subtotal = $subtotal - (($data->Discount / 100) * $subtotal);
//                    $html .= '<tr>
//                                <td rowspan="' . (1 + ($data->Spesifikasi != '' ? 1 : 0) + count($detail) + count($parcel)) . '" style="vertical-align: top;font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $counter . '</td>
//                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;"><b>' . $data->InventoryText . '</b></td>
//                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->UomText . '</td>
//                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
//                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($price, '2', '.', ',') . '</td>
//                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
//                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($discount, '2', '.', ',') . '</td>
//                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($subtotal, '2', '.', ',') . '</td>
//                            </tr>';
//                    if ($data->Spesifikasi != '') {
//                        $html .= '<tr>
//                                <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . nl2br($data->Spesifikasi) . '</td>
//                            </tr>';
//                    }
//                    $total += $subtotal;
//                    foreach ($detail as $data2) {
//                        if ($header->VAT == 1) {
//                            $price = $data2->Price + ($data2->Price * 0.1);
//                            $discount = $data2->DiscountNominal + ($data2->DiscountNominal * 0.1);
//                        } else {
//                            $price = $data2->Price;
//                            $discount = $data2->DiscountNominal;
//                        }
//                        $subtotal = ($price - $discount) * $data2->Qty;
//                        $subtotal = $subtotal - (($data2->Discount / 100) * $subtotal);
//                        $html .= '<tr>
//                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;"><i>' . $data2->inventory->InventoryName . '</i></td>
//                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data2->uom->UomID . '</td>
//                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data2->Qty, 0, '.', ',') . '</td>
//                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($price, '2', '.', ',') . '</td>
//                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data2->Discount . '' . '</td>
//                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($discount, '2', '.', ',') . '</td>
//                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($subtotal, '2', '.', ',') . '</td>
//                                </tr>';
//                    }
//                    foreach ($parcel as $data2) {
//                        if ($header->VAT == 1) {
//                            $price = $data2->Price + ($data2->Price * 0.1);
//                            $discount = $data2->DiscountNominal + ($data2->DiscountNominal * 0.1);
//                        } else {
//                            $price = $data2->Price;
//                            $discount = $data2->DiscountNominal;
//                        }
//                        $subtotal = ($price - $discount) * $data2->Qty;
//                        $subtotal = $subtotal - (($data2->Discount / 100) * $subtotal);
//                        $html .= '<tr>
//                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;"><i>' . $data2->parcel->ParcelName . '</i></td>
//                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">-</td>
//                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data2->Qty, 0, '.', ',') . '</td>
//                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($price, '2', '.', ',') . '</td>
//                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data2->Discount . '' . '</td>
//                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($discount, '2', '.', ',') . '</td>
//                                    <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($subtotal, '2', '.', ',') . '</td>
//                                </tr>';
//                    }
//
//                    $counter++;
//                }
//            } else {
//                $html .= '<tr>
//                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this quotation.</td>
//                        </tr>';
//            }
//            $html .= '</tbody>
//                            </table>
//                            <table>
//                            <tr><td>
//                            <div style="box-sizing: border-box;min-width: 300px; display: inline-block; clear: both;">   
//                            <table>
//                                 <tr style="background: none;">
//                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Delivery Terms</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $header->DeliveryTerms . '</td>
//                                 </tr>
//                                 <tr style="background: none;">
//                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Delivery Time</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $header->DeliveryTime . '</td>
//                                 </tr>
//                                 <tr style="background: none;">
//                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Terms Of Payment</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $payment . '</td>
//                                 </tr>
//                                  <tr style="background: none;">
//                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Validity</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $header->Validity . '</td>
//                                 </tr>
//                                  <tr style="background: none;">
//                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Remark</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: left;">' . $header->Remark . '</td>
//                                 </tr>
//                                </table></div></td><td>
//                            <div style="box-sizing: border-box;min-width: 200px; margin-left: 130px; display: inline-block; clear: both;">                                
//                                <table>
//                                 <tr style="background: none;">
//                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Total</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '</td>
//                                 </tr>
//                                 <tr style="background: none;">
//                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Discount</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($header->DiscountGlobal, '2', '.', ',') . '</td>
//                                 </tr>
//                                 <tr style="background: none;">
//                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Grand Total</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
//                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total - $header->DiscountGlobal, '2', '.', ',') . '</td>
//                                 </tr>
//                                 <tr><td><br></td></tr>
//                                </table>
//                            </div></td></tr>
//                            <tr><td><br></td></tr>
//                            <tr><td><br></td></tr>
//                            <tr><td><br></td></tr>
//                            <tr><td><br></td></tr>
//                            <tr><td><br></td></tr>
//                            <tr>
//                            <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">(  ' . Auth::user()->UserName . '  )</td>
//                            <td style="text-align: center;width:50%;font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">(.........................)</td>
//                            </tr>
//                            </table>
//                    </div>
//                </body>
//            </html>';
//            //return PDF ::load($html, 'A5', 'portrait')->show();
//
//            return View::make('template.print.quotationInternalPrint')
//                            ->with('header', $header)
//                            ->with('customer', $customer)
//                            ->with('vat', $vat)
//                            ->with('id', $id)
//                            ->with('description', $description)
//                            ->with('currencyName', $currencyName)
//                            ->with('payment', $payment)
//                            ->with('detail', $detail);
//        } else {
//            $messages = 'accessDenied';
//            Session::flash('messages', $messages);
//            return Redirect::Route('showQuotation');
//        }
//    }

    function createID($tipe, $tax = null) {
        if ($tax == "Tax")
            $quotation = 'QU';
        else if ($tax == "NonTax")
            $quotation = 'QUN';
        else
            $quotation = 'QU';
        if ($tipe == 0) {
            $quotation .= '.' . date('m') . date('y');
        }
        return $quotation;
    }

    public function formatCariIDQuotation() {
        $date = explode('-', Input::get('date'));
        $id = $this->createID(1);
        $yearDigit = substr($date[2], 2);
        $id .= '.' . $date[1] . $yearDigit . '.';
        echo QuotationHeader::getNextIDQuotation($id);
    }

    public function summaryQuotation() {
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $totalQU = 0;
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <!--<div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>-->
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Quotation Summary Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span><br> '
                . '     <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;">Printed Date : ' . date("d-M-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span>';
        $hitung = 0;
        foreach (Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $dataCustomer) {
            if (QuotationHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                            ->where('ACC6InternalID', $dataCustomer->InternalID)
                            ->where("VAT", "!=", Auth::user()->SeeNPPN)
                            ->whereBetween('QuotationDate', Array($start, $end))->count() > 0) {
                $html .= '<table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=6>' . $dataCustomer->ACC6Name . '</th>
                                        </tr>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Quotation ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Currency</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Rate</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total (After Discount)</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Grand Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                $sumGrandTotal = 0;
                foreach (QuotationHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where('ACC6InternalID', $dataCustomer->InternalID)
                        ->where("VAT", "!=", Auth::user()->SeeNPPN)
                        ->whereBetween('QuotationDate', Array($start, $end))->get() as $data) {
                    $grandTotal = $data->GrandTotal;
                    $sumGrandTotal += ceil($grandTotal);
                    $totalQU += ceil($grandTotal);
                    $total = $grandTotal;
                    $vat = 0;
                    if ($data->VAT == 1) {
                        $total = $total * 10 / 11;
                        $vat = $total / 10;
                    }
                    $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->QuotationID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->QuotationDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->Currency->CurrencyName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->CurrencyRate, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format(ceil($total), '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format(ceil($grandTotal), '2', '.', ',') . '</td>
                            </tr>';
                }
                $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="80%" colspan="5">Total : </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . number_format($sumGrandTotal, '2', '.', ',') . '</td>
                            </tr>';
                $html .= '</tbody>
            </table>';
                $hitung++;
            }
        }

        if ($hitung == 0) {
            $html .= '<br><span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no quotation.</span>';
        }
        $html .= '<table class="tableBorder" width="100%"  style="margin-top: 10px; clear: both;  top: 50px;">
                    <thead>
                        <tr>
                            <th style="font-family: helvetica,sans-serif;font-size: 14px; margin: 5px !important; font-weight: 700; text-align: right" width="80%" colspan="6">Total Quotation : </th>
                            <th style="font-family: helvetica,sans-serif;font-size: 14px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . number_format($totalQU, '2', '.', ',') . '</th>
                        </tr>
                    <thead>
                </table>';
        $html .= '</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('quotation_summary');
    }

    public function detailQuotation() {
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $dateTamp = '';
        $coa6Tamp = '';
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <!--<div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>-->
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Quotation Detail Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span>
                        <br>    <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span><br>';
        if (QuotationHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                        ->whereBetween('QuotationDate', Array($start, $end))
                        ->where("VAT", "!=", Auth::user()->SeeNPPN)
                        ->orderBy('QuotationDate')->orderBy('ACC6InternalID')->count() > 0) {
            foreach (QuotationHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                    ->whereBetween('QuotationDate', Array($start, $end))
                    ->where("VAT", "!=", Auth::user()->SeeNPPN)
                    ->orderBy('QuotationDate')->orderBy('ACC6InternalID')->get() as $dataPenjualan) {
                if ($dateTamp != date("d-M-Y", strtotime($dataPenjualan->QuotationDate))) {
                    $html .= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Quotation Date : ' . date("d-M-Y", strtotime($dataPenjualan->QuotationDate)) . '</span><br><br>';
                    $dateTamp = date("d-M-Y", strtotime($dataPenjualan->QuotationDate));
                    $coa6Tamp = '';
                }
                if ($coa6Tamp != $dataPenjualan->ACC6InternalID) {
                    $html .= '<span style="padding-left:23px;font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Customer : ' . $dataPenjualan->coa6->ACC6Name . '</span>';
                    $coa6Tamp = $dataPenjualan->ACC6InternalID;
                }
                $html .= '<table class="tableBorder" style="width:95%; margin-top: 18px; clear: both; position: relative; left: 4%">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=8>' . $dataPenjualan->QuotationID . ' | ' . $dataPenjualan->Currency->CurrencyName . ' | Rate : ' . number_format($dataPenjualan->CurrencyRate, '2', '.', ',') . ' </th>
                                        </tr>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory ID</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Name</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Uom</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Qty</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Price</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc (%)</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                $total = 0;
                $vat = 0;
                foreach ($dataPenjualan->quotationDetail as $data) {
                    if ($data->QuotationParcelInternalID == 0) {
                        //jika bukan parcel
                        $total += $data->SubTotal;
                        $vat += $data->VAT;
                        $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->inventory->InventoryID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->inventory->InventoryName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->Uom->UomID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format(ceil($data->SubTotal), '2', '.', ',') . '</td>
                            </tr>';
                    }
                }
                foreach (QuotationParcel::where("QuotationInternalID", $dataPenjualan->InternalID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->get() as $data) {
                    //jika parcel
                    $total += ceil($data->SubTotal);
                    $vat += $data->VAT;
                    $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $data->Parcel->ParcelID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $data->Parcel->ParcelName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right"> - </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format(ceil($data->SubTotal), '2', '.', ',') . '</td>
                            </tr>';
                }
                if ($vat != 0) {
                    $vat = $vat - ($dataPenjualan->DiscountGlobal * 0.1);
                }
                $html .= '<tr>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=5></td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=2>Total <br> Discount <br> Grand Total</td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: right;">' . number_format(ceil($total), '2', '.', ',') . '<br>'
                        . '' . number_format($dataPenjualan->DiscountGlobal, '2', '.', ',') . '<br>'
                        . '' . number_format(ceil($total) - $dataPenjualan->DiscountGlobal, '2', '.', ',') . '<br>
                    </tr>
                </tbody>
            </table>';
            }
        } else {
            $html .= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no quotation in this period.</span><br><br>';
        }
        $html .= '</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('quotation_detail');
    }

    //=================ajax=====================
    public function getUomThisInventory() {
        $inventoryUom = InventoryUom::where("InventoryInternalID", Input::get("id"))->get();
        foreach ($inventoryUom as $data) {
            ?>
            <option value="<?php echo $data->UomInternalID ?>" <?php echo ($data->Default == 1 ? "selected" : "") ?>><?php echo $data->Uom->UomID; ?></option>
            <?php
        }
    }

    public function getPriceUomThisInventory() {
        $inventoryInternalID = Input::get("InventoryID");
        $uomInternalID = Input::get("UomID");
        $InventoryPrice = InventoryUom::where("InventoryInternalID", $inventoryInternalID)
                        ->where("UomInternalID", $uomInternalID)->first();
        return $InventoryPrice->Price . '---;---' . Input::get("urutan");
    }

    public function getPriceDefault() {
        $inv = Input::get('inventory');
        $uom = Input::get('uom');

        $price = InventoryUom::where("InventoryInternalID", $inv)
                        ->where("UomInternalID", $uom)->first();
        return number_format($price->PriceA, 2, ".", ",");
    }

    public function getPriceRangeThisInventoryQuotation() {
        $hasil = getPriceRangeInventory(Input::get("InventoryID"), Input::get("UomID"), Input::get("Qty"));
        return $hasil . '---;---' . Input::get("urutan");
    }

    public function getPriceThisParcel() {
        $InternalID = Input::get("id");
        $parcelPrice = Parcel::find($InternalID)->GrandTotal;
        return $parcelPrice;
    }

    public function getSearchResultInventoryForQuotation() {
        $idnih = "%" . explode(" ", Input::get("id"))[0] . "%";
//        dd($idnih);
        $pass = str_replace(explode(" ", Input::get("id"))[0], '', Input::get('id'));
        $input = splitSearchValue($pass);
//        $input = splitSearchValue(Input::get("id"));
        $dataInventory = Inventory::select('m_inventory.*')->distinct()
                ->join('m_inventory_uom', 'm_inventory_uom.InventoryInternalID', '=', 'm_inventory.InternalID')
                ->where("m_inventory.Status", 0)
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where(function($query) use ($idnih, $input) {
                    $query->where("m_inventory.InventoryID", "like", $idnih)
                    ->where("m_inventory.InventoryName", "like", $input)
                    ->orWhere("m_inventory.InventoryName", "like", $idnih);
                })
                ->where("m_inventory.Status", 0)
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->get();

        $dataParcel = Parcel::where("CompanyInternalID", Auth::user()->Company->InternalID)
                ->where("ParcelID", "like", $input)
                ->orWhere("ParcelName", "like", $input)
                ->get();

        if (count($dataInventory) == 0 && count($dataParcel) == 0) {
            ?>
            <span>Inventory with keywords <i><u><?php echo Input::get("id"); ?></u></i> can't be found</span>
            <?php
        } else {
            ?>
            <select class="input-theme left inventory" id="inventory-0">
                <?php
                foreach ($dataInventory as $inventory) {
                    ?>
                    <option id="inventory<?php echo $inventory->InternalID ?>" value="<?php echo $inventory->InternalID ?>---;---inventory">
                        <?php echo $inventory->InventoryID . ' ' . $inventory->InventoryName . ' (Power: ' . $inventory->Power . ')'; ?>
                    </option>
                    <?php
                }
                ?>
                <?php
                foreach ($dataParcel as $parcel) {
                    ?>
                    <!--foreach untuk paketnya-->
                    <option id="parcel<?php echo $parcel->InternalID ?>" value="<?php echo $parcel->InternalID ?>---;---parcel">
                        <?php echo $parcel->ParcelID . " " . $parcel->ParcelName ?>
                    </option>
                    <!--foreach untuk paketnya-->
                    <?php
                }
                ?>
            </select>
            <script type="text/javascript" src="<?php echo Asset('js/entry-js-controller/controller.js') ?>"></script>
            <?php
        }
    }

    public function getSearchResultInventory() {
//        $input = splitSearchValue(Input::get("id"));
//        $dataInventory = Inventory::select('m_inventory.*')->distinct()
//                ->join('m_inventory_uom', 'm_inventory_uom.InventoryInternalID', '=', 'm_inventory.InternalID')
//                ->where("m_inventory.Status", 0)
//                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->where("m_inventory.InventoryName", "like", $input)
//                ->orWhere("m_inventory.InventoryID", "like", $input)
//                ->where("m_inventory.Status", 0)
//                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->get();

        $idnih = "%" . explode(" ", Input::get("id"))[0] . "%";
//        dd($idnih);
        $pass = str_replace(explode(" ", Input::get("id"))[0], '', Input::get('id'));
        $input = splitSearchValue($pass);
//        $input = splitSearchValue(Input::get("id"));
        $dataInventory = Inventory::select('m_inventory.*')->distinct()
                ->join('m_inventory_uom', 'm_inventory_uom.InventoryInternalID', '=', 'm_inventory.InternalID')
                ->where("m_inventory.Status", 0)
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where(function($query) use ($idnih, $input) {
                    $query->where("m_inventory.InventoryID", "like", $idnih)
                    ->where("m_inventory.InventoryName", "like", $input)
                    ->orWhere("m_inventory.InventoryName", "like", $idnih);
                })
                ->where("m_inventory.Status", 0)
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->get();

        $dataParcel = Parcel::where("CompanyInternalID", Auth::user()->Company->InternalID)
                ->where("ParcelID", "like", $input)
                ->orWhere("ParcelName", "like", $input)
                ->get();

        if (count($dataInventory) == 0 && count($dataParcel) == 0) {
            ?>
            <span>Inventory with keywords <i><u><?php echo Input::get("id"); ?></u></i> can't be found</span>
            <?php
        } else {
            ?>
            <select class="input-theme left inventory" id="inventory-0">
                <?php
                foreach ($dataInventory as $inventory) {
                    ?>
                    <option id="inventory<?php echo $inventory->InternalID ?>" value="<?php echo $inventory->InternalID ?>">
                        <?php echo $inventory->InventoryID . ' ' . $inventory->InventoryName . ' (Power: ' . $inventory->Power . ')'; ?>
                    </option>
                    <?php
                }
                ?>
                <?php
                foreach ($dataParcel as $parcel) {
                    ?>
                    <!--foreach untuk paketnya-->
                    <option id="parcel<?php echo $parcel->InternalID ?>" value="<?php echo $parcel->InternalID ?>---;---parcel">
                        <?php echo $parcel->ParcelID . " " . $parcel->ParcelName ?>
                    </option>
                    <!--foreach untuk paketnya-->
                    <?php
                }
                ?>
            </select>
            <script type="text/javascript" src="<?php echo Asset('js/entry-js-controller/controller.js') ?>"></script>
            <?php
        }
    }

    public function getSearchResultInventoryMemoIn() {
//        $input = splitSearchValue(Input::get("id"));
//        $dataInventory = Inventory::select('m_inventory.*')->distinct()
//                ->join('m_inventory_uom', 'm_inventory_uom.InventoryInternalID', '=', 'm_inventory.InternalID')
//                ->where("m_inventory.Status", 0)
//                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->where("m_inventory.InventoryName", "like", $input)
//                ->orWhere("m_inventory.InventoryID", "like", $input)
//                ->where("m_inventory.Status", 0)
//                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->get();

        $idnih = "%" . explode(" ", Input::get("id"))[0] . "%";
//        dd($idnih);
        $pass = str_replace(explode(" ", Input::get("id"))[0], '', Input::get('id'));
        $input = splitSearchValue($pass);
//        $input = splitSearchValue(Input::get("id"));
        $dataInventory = Inventory::select('m_inventory.*')->distinct()
                ->join('m_inventory_uom', 'm_inventory_uom.InventoryInternalID', '=', 'm_inventory.InternalID')
                ->where("m_inventory.Status", 0)
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where(function($query) use ($idnih, $input) {
                    $query->where("m_inventory.InventoryID", "like", $idnih)
                    ->where("m_inventory.InventoryName", "like", $input)
                    ->orWhere("m_inventory.InventoryName", "like", $idnih);
                })
                ->where("m_inventory.Status", 0)
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->get();

//        $dataParcel = Parcel::where("CompanyInternalID", Auth::user()->Company->InternalID)
//                ->where("ParcelID", "like", $input)
//                ->orWhere("ParcelName", "like", $input)
//                ->get();

        if (count($dataInventory) == 0) {
            ?>
            <span>Inventory with keywords <i><u><?php echo Input::get("id"); ?></u></i> can't be found</span>
            <?php
        } else {
            ?>
            <select class="input-theme left inventory" id="inventory-0">
                <?php
                foreach ($dataInventory as $inventory) {
                    ?>
                    <option id="inventory<?php echo $inventory->InternalID ?>" value="<?php echo $inventory->InternalID ?>">
                        <?php echo $inventory->InventoryID . ' ' . $inventory->InventoryName . ' (Power: ' . $inventory->Power . ')'; ?>
                    </option>
                    <?php
                }
                ?>
            </select>
            <script type="text/javascript" src="<?php echo Asset('js/entry-js-controller/controllerMemoIn.js') ?>"></script>
            <?php
        }
    }

    public function getSearchResultInventoryMemoOut() {
//        $input = splitSearchValue(Input::get("id"));
//        $dataInventory = Inventory::select('m_inventory.*')->distinct()
//                ->join('m_inventory_uom', 'm_inventory_uom.InventoryInternalID', '=', 'm_inventory.InternalID')
//                ->where("m_inventory.Status", 0)
//                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->where("m_inventory.InventoryName", "like", $input)
//                ->orWhere("m_inventory.InventoryID", "like", $input)
//                ->where("m_inventory.Status", 0)
//                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->get();

        $idnih = "%" . explode(" ", Input::get("id"))[0] . "%";
//        dd($idnih);
        $pass = str_replace(explode(" ", Input::get("id"))[0], '', Input::get('id'));
        $input = splitSearchValue($pass);
//        $input = splitSearchValue(Input::get("id"));
        $dataInventory = Inventory::select('m_inventory.*')->distinct()
                ->join('m_inventory_uom', 'm_inventory_uom.InventoryInternalID', '=', 'm_inventory.InternalID')
                ->where("m_inventory.Status", 0)
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where(function($query) use ($idnih, $input) {
                    $query->where("m_inventory.InventoryID", "like", $idnih)
                    ->where("m_inventory.InventoryName", "like", $input)
                    ->orWhere("m_inventory.InventoryName", "like", $idnih);
                })
                ->where("m_inventory.Status", 0)
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->get();

//        $dataParcel = Parcel::where("CompanyInternalID", Auth::user()->Company->InternalID)
//                ->where("ParcelID", "like", $input)
//                ->orWhere("ParcelName", "like", $input)
//                ->get();

        if (count($dataInventory) == 0) {
            ?>
            <span>Inventory with keywords <i><u><?php echo Input::get("id"); ?></u></i> can't be found</span>
            <?php
        } else {
            ?>
            <select class="input-theme left inventory" id="inventory-0">
                <?php
                foreach ($dataInventory as $inventory) {
                    ?>
                    <option id="inventory<?php echo $inventory->InternalID ?>" value="<?php echo $inventory->InternalID ?>">
                        <?php echo $inventory->InventoryID . ' ' . $inventory->InventoryName . ' (Power: ' . $inventory->Power . ')'; ?>
                    </option>
                    <?php
                }
                ?>
            </select>
            <script type="text/javascript" src="<?php echo Asset('js/entry-js-controller/controllerMemoOut.js') ?>"></script>
            <?php
        }
    }

    public function getSearchResultInventoryTransfer() {
//        $input = splitSearchValue(Input::get("id"));
//        $dataInventory = Inventory::select('m_inventory.*')->distinct()
//                ->join('m_inventory_uom', 'm_inventory_uom.InventoryInternalID', '=', 'm_inventory.InternalID')
//                ->where("m_inventory.Status", 0)
//                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->where("m_inventory.InventoryName", "like", $input)
//                ->orWhere("m_inventory.InventoryID", "like", $input)
//                ->where("m_inventory.Status", 0)
//                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->get();

        $idnih = "%" . explode(" ", Input::get("id"))[0] . "%";
//        dd($idnih);
        $pass = str_replace(explode(" ", Input::get("id"))[0], '', Input::get('id'));
        $input = splitSearchValue($pass);
//        $input = splitSearchValue(Input::get("id"));
        $dataInventory = Inventory::select('m_inventory.*')->distinct()
                ->join('m_inventory_uom', 'm_inventory_uom.InventoryInternalID', '=', 'm_inventory.InternalID')
                ->where("m_inventory.Status", 0)
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where(function($query) use ($idnih, $input) {
                    $query->where("m_inventory.InventoryID", "like", $idnih)
                    ->where("m_inventory.InventoryName", "like", $input)
                    ->orWhere("m_inventory.InventoryName", "like", $idnih);
                })
                ->where("m_inventory.Status", 0)
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->get();

//        $dataParcel = Parcel::where("CompanyInternalID", Auth::user()->Company->InternalID)
//                ->where("ParcelID", "like", $input)
//                ->orWhere("ParcelName", "like", $input)
//                ->get();
//        
        if (count($dataInventory) == 0) {
            ?>
            <span>Inventory with keywords <i><u><?php echo Input::get("id"); ?></u></i> can't be found</span>
            <?php
        } else {
            ?>
            <select class="input-theme left inventory" id="inventory-0">
                <?php
                foreach ($dataInventory as $inventory) {
                    ?>
                    <option id="inventory<?php echo $inventory->InternalID ?>" value="<?php echo $inventory->InternalID ?>">
                        <?php echo $inventory->InventoryID . ' ' . $inventory->InventoryName . ' (Power: ' . $inventory->Power . ')'; ?>
                    </option>
                    <?php
                }
                ?>
            </select>
            <script type="text/javascript" src="<?php echo Asset('js/entry-js-controller/controllerTransfer.js') ?>"></script>
            <?php
        }
    }

    public function getSearchResultInventoryTransformation() {
//        $input = splitSearchValue(Input::get("id"));
//        $dataInventory = Inventory::select('m_inventory.*')->distinct()
//                ->join('m_inventory_uom', 'm_inventory_uom.InventoryInternalID', '=', 'm_inventory.InternalID')
//                ->where("m_inventory.Status", 0)
//                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->where("m_inventory.InventoryName", "like", $input)
//                ->orWhere("m_inventory.InventoryID", "like", $input)
//                ->where("m_inventory.Status", 0)
//                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->get();

        $idnih = "%" . explode(" ", Input::get("id"))[0] . "%";
//        dd($idnih);
        $pass = str_replace(explode(" ", Input::get("id"))[0], '', Input::get('id'));
        $input = splitSearchValue($pass);
//        $input = splitSearchValue(Input::get("id"));
        $dataInventory = Inventory::select('m_inventory.*')->distinct()
                ->join('m_inventory_uom', 'm_inventory_uom.InventoryInternalID', '=', 'm_inventory.InternalID')
                ->where("m_inventory.Status", 0)
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where(function($query) use ($idnih, $input) {
                    $query->where("m_inventory.InventoryID", "like", $idnih)
                    ->where("m_inventory.InventoryName", "like", $input)
                    ->orWhere("m_inventory.InventoryName", "like", $idnih);
                })
                ->where("m_inventory.Status", 0)
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->get();

//        $dataParcel = Parcel::where("CompanyInternalID", Auth::user()->Company->InternalID)
//                ->where("ParcelID", "like", $input)
//                ->orWhere("ParcelName", "like", $input)
//                ->get();

        if (count($dataInventory) == 0) {
            ?>
            <span>Inventory with keywords <i><u><?php echo Input::get("id"); ?></u></i> can't be found</span>
            <?php
        } else {
            ?>
            <select class="input-theme left inventory" id="inventory-0">
                <?php
                foreach ($dataInventory as $inventory) {
                    ?>
                    <option id="inventory<?php echo $inventory->InternalID ?>" value="<?php echo $inventory->InternalID ?>">
                        <?php echo $inventory->InventoryID . ' ' . $inventory->InventoryName . ' (Power: ' . $inventory->Power . ')'; ?>
                    </option>
                    <?php
                }
                ?>
            </select>
            <script type="text/javascript" src="<?php echo Asset('js/entry-js-controller/controllerTransformation.js') ?>"></script>
            <?php
        }
    }

    public function getSearchResultInventoryTransformation2() {
        $idnih = "%" . explode(" ", Input::get("id"))[0] . "%";
        $pass = str_replace(explode(" ", Input::get("id"))[0], '', Input::get('id'));
        $input = splitSearchValue($pass);
        $dataInventory = Inventory::select('m_inventory.*')->distinct()
                ->join('m_inventory_uom', 'm_inventory_uom.InventoryInternalID', '=', 'm_inventory.InternalID')
                ->where("m_inventory.Status", 0)
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where(function($query) use ($idnih, $input) {
                    $query->where("m_inventory.InventoryID", "like", $idnih)
                    ->where("m_inventory.InventoryName", "like", $input)
                    ->orWhere("m_inventory.InventoryName", "like", $idnih);
                })
                ->where("m_inventory.Status", 0)
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->get();

        if (count($dataInventory) == 0) {
            ?>
            <span>Inventory with keywords <i><u><?php echo Input::get("id"); ?></u></i> can't be found</span>
            <?php
        } else {
            ?>
            <select class="input-theme left inventory2" id="inventory2-0">
                <?php
                foreach ($dataInventory as $inventory) {
                    ?>
                    <option id="inventory<?php echo $inventory->InternalID ?>" value="<?php echo $inventory->InternalID ?>">
                        <?php echo $inventory->InventoryID . ' ' . $inventory->InventoryName . ' (Power: ' . $inventory->Power . ')'; ?>
                    </option>
                    <?php
                }
                ?>
            </select>
            <script type="text/javascript" src="<?php echo Asset('js/entry-js-controller/controllerTransformation2.js') ?>"></script>
            <?php
        }
    }

    public function getCustomerManager() {
        $cus = Coa6::find(Input::get('id'));
        $dataDetail = SalesMan::where('Status', 1)->where('CompanyInternalID', Auth::user()->CompanyInternalID)->get();
//        $dataDetail = CustomerDetail::where('ACC6InternalID', Input::get('id'))->get();
        $dis = $hidden = '';
//        if ($cus->CustomerManager != 0) {
//            $cm = SalesMan::find($cus->CustomerManager);
//            if ($cm->Status == 1) {
//                $dis = 'disabled=""';
//                $hidden = "<input type='hidden' name='salesman' value='" . $cm->InternalID . "'>";
//            }
//        }
        if (count($dataDetail) == 0) {
            ?>
            <label for="coa6"></label>
            <span><i>There is no salesman registered.</i></span>
            <?php
        } else {
            if (Input::get('update') == 'no') {
                ?>
                <label for="coa6">Salesman *</label>
                <select class="chosen-select" id="customerdetail" style="" name="salesman" <?php echo $dis ?> >
                    <?php
                    foreach ($dataDetail as $cusdetail) {
                        ?>
                        <option <?php if ($cusdetail->InternalID == $cus->CustomerManager) echo "selected" ?> id="detail<?php echo $cusdetail->InternalID ?>" value="<?php echo $cusdetail->InternalID ?>">
                            <?php echo $cusdetail->SalesManID . ' ' . $cusdetail->SalesManName; ?>
                        </option>
                        <?php
                    }
                    ?>
                </select>
                <?php echo $hidden ?>
                <?php
            } else {
                if (Input::get('update2') == '' || Input::get('update2') == null)
                    $d = QuotationHeader::find(Input::get('update'));
                else
                    $d = SalesOrderHeader::find(Input::get('update2'));
                ?>
                <label for="coa6">Salesman *</label>
                <select class="chosen-select" id="customerdetail" style="" name="salesman" <?php echo $dis ?>>
                    <?php
                    foreach ($dataDetail as $cusdetail) {
                        if ($cusdetail->InternalID != $d->SalesManInternalID) {
                            ?>
                            <option id="detail<?php echo $cusdetail->InternalID ?>" value="<?php echo $cusdetail->InternalID ?>">
                                <?php echo $cusdetail->SalesManID . ' ' . $cusdetail->SalesManName; ?>
                            </option>
                            <?php
                        } else {
                            ?>
                            <option selected="" id="detail<?php echo $cusdetail->InternalID ?>" value="<?php echo $cusdetail->InternalID ?>">
                                <?php echo $cusdetail->SalesManID . ' ' . $cusdetail->SalesManName; ?>
                            </option>
                            <?php
                        }
                    }
                    ?>
                </select>
                <?php echo $hidden ?>
                <?php
            }
        }
    }

    public function getCustomerCp() {
        $cus = Coa6::find(Input::get('id'));
        $dataDetail = CustomerDetail::where('ACC6InternalID', $cus->InternalID)->get();

        if (count($dataDetail) == 0) {
            ?>
            <label for="coa6"></label>
            <span><i>There is no contact person registered.</i></span>
            <?php
        } else {
            if (Input::get('update') == 'no') {
                ?>
                <label for="coa6">Attn *</label>
                <select class="chosen-select" id="customercp" style="" name="attn">
                    <?php
                    foreach ($dataDetail as $cusdetail) {
                        ?>
                        <option id="cp<?php echo $cusdetail->InternalID ?>" value="<?php echo $cusdetail->InternalID ?>">
                            <?php echo $cusdetail->CustomerDetailID . ' ' . $cusdetail->DetailName; ?>
                        </option>
                        <?php
                    }
                    ?>
                </select>
                <?php
            } else {
                if (Input::get('update2') == '' || Input::get('update2') == null)
                    $d = QuotationHeader::find(Input::get('update'));
                else
                    $d = SalesOrderHeader::find(Input::get('update2'));
                ?>
                <label for="coa6">Attn *</label>
                <select class="chosen-select" id="customercp" style="" name="attn">
                    <?php
                    foreach ($dataDetail as $cusdetail) {
                        if ($cusdetail->InternalID != $d->AttnInternalID) {
                            ?>
                            <option id="cp<?php echo $cusdetail->InternalID ?>" value="<?php echo $cusdetail->InternalID ?>">
                                <?php echo $cusdetail->CustomerDetailID . ' ' . $cusdetail->DetailName; ?>
                            </option>
                            <?php
                        } else {
                            ?>
                            <option selected="" id="cp<?php echo $cusdetail->InternalID ?>" value="<?php echo $cusdetail->InternalID ?>">
                                <?php echo $cusdetail->CustomerDetailID . ' ' . $cusdetail->DetailName; ?>
                            </option>
                            <?php
                        }
                    }
                    ?>
                </select>
                <?php
            }
        }
    }

    public function quotationPrintStruk($id) {
        $id = QuotationHeader::getIdquotation($id);
        $header = QuotationHeader::find($id);
        $detail = QuotationHeader::find($id)->quotationDetail()->get();
        $html = '<!DOCTYPE html>
        <html id="printSales" style="width: 118mm; height: 150mm;">
            <head>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Receipt</title>
                <style>
                    @media print{@page { size: 89mm auto;}}
                    @page {
                        margin-top: 0;
                        margin-bottom: 0;
                        size: 89mm auto;
                    }

                    body {
                        font-family: calibri; 
                        width: 100%;
                        margin: 0 auto;
                    }
                    
                    body * {
                        letter-spacing: 0.6mm !important;
                    }
                    
                    h4 {
                        font-size: 18px;
                        letter-spacing: 0.4px;
                        margin-bottom: 2px;
                    }

                    h3 {
                        font-size: 16px;
                        letter-spacing: 0.4px;
                        margin-top: 0px;
                        margin-bottom: 2px;
                    }

                    p {
                        font-size: 14px;
                        letter-spacing: 0.2px;
                    }

                    table {
                        border-collapse: separate;
                        font-size: 16px;
                        letter-spacing: 0.2px;
                        width: 100%;
                        margin: 0 auto;
                    }

                    table td {
                        vertical-align: middle;
                        padding: -10px;
                    }

                </style>
            </head>';
        $html .= '<body>
        <h4 style="text-align: center;">' . Auth::user()->company->CompanyName . '</h4>
        <p style="text-align: center;">' . Auth::user()->company->Address . ', ' . Auth::user()->company->City . '</p>
        <p style="text-align: center;">Telp : ' . Auth::user()->company->Phone . ', Fax : ' . Auth::user()->company->Fax . '</p>    
        <h3 style="text-align: center;">Quotation</h3>';

        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $html .= '<table style="border-top: 0.4px solid #000000; border-bottom: 0.4px solid #000000;">
                <tr>
                    <td>' . $header->QuotationID . '</td>
                    <td colspan="2" style="text-align:right;">' . Auth::user()->UserID . '</td>
                </tr>
                <tr>
                    <td colspan="3">
                       ' . date("d-m-Y", strtotime($header->QuotationDate)) . '
                    </td>
                </tr>
                </table>';

            $html .= '<table style="border-bottom: 0.4px solid #000000;font-size: 18px;">';
//loop data inventory
            $count = 0;
            $total = 0;
            $totalVAT = 0;
            $i = 1;
            foreach ($detail as $data) {
                if ($data->QuotationParcelInternalID == 0) {
                    $inventory = Inventory::find($data->InventoryInternalID);
                    if ($inventory->TextPrint != '' || $inventory->TextPrint != Null) {
                        $inv = $inventory->InventoryID . '- ' . $inventory->TextPrint;
                    } else {
                        $inv = $inventory->InventoryID . '- ' . $inventory->InventoryName;
                    }

                    $html .= '<tr>
                                <td colspan="4">
                                    ' . $i . '. ' . $inv . '
                                </td>
                            </tr>';
                    $html .= '<tr>
                                    <td style="text-align: right; width: 20%">' . number_format($data->Qty, 0, '.', ',') . ' ' . $data->Uom->UomID . '</td>
                                    <td style="text-align: right; width: 10%">x</td>
                                    <td style="text-align: right; width: 20%">' . number_format($data->Price, 2, ".", ",") . '</td>
                                    <td style="text-align: right; width: 30%">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                              </tr>';
                    $html .= '<tr>  
                                    <td colspan="4" style="font-size:12px;" ><div style="margin-left:64px">( Disc : ' . $data->Discount . '% , Disc : ' . number_format($data->DiscountNominal, '2', '.', ',') . ')</div></td>
                              </tr>';
                    $count++;
                    $i++;
                    $totalVAT += $data->VAT;
                    $total += $data->SubTotal;
                    if ($totalVAT != 0) {
                        $totalVAT = $totalVAT - ($header->DiscountGlobal * 0.1);
                    }
                }
            }
            foreach (QuotationParcel::where("QuotationInternalID", $header->InternalID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->get() as $data) {

                $html .= '<tr>
                                <td colspan="4">
                                    ' . $i . '. ' . $data->Parcel->ParcelID . ' - ' . $data->Parcel->ParcelName . '
                                </td>
                            </tr>';
                $html .= '<tr>
                                    <td style="text-align: right; width: 20%">' . number_format($data->Qty, 0, '.', ',') . '  -</td>
                                    <td style="text-align: right; width: 10%">x</td>
                                    <td style="text-align: right; width: 20%">' . number_format($data->Price, 2, ".", ",") . '</td>
                                    <td style="text-align: right; width: 30%">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                              </tr>';
                $html .= '<tr>
                                    <td colspan="4" style="font-size:10px; ">( Disc : ' . $data->Discount . '% , Disc : ' . number_format($data->DiscountNominal, '2', '.', ',') . ')</td>
                              </tr>';
                $count++;
                $i++;
                if ($data->VAT != 0) {
                    $totalVAT += ($data->Price * $data->Qty) * 0.1;
                }
                $total += $data->Price * $data->Qty;
            }

            $html .= '</table>';


            $html .= '<table style="margin-bottom: 7px;font-size: 18px;">
                        <tr>
                            <td>Sum : ' . $count . ' item</td>
                            <td style="text-align: right;">Total</td>
                            <td style="text-align: right;">' . number_format($total, '2', '.', ',') . '</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: right;">Discount</td>
                            <td style="text-align: right;">' . number_format($header->DiscountGlobal, '2', '.', ',') . '</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: right;"><strong>Grand Total</strong></td>
                            <td style="border-bottom: 1px solid #000000; text-align: right;">' . number_format($total - $header->DiscountGlobal, '2', '.', ',') . '</td>
                        </tr>
                        ';
            $html .= '</table>

                    <ul style="page-break-after:always; font-size: 12px; list-style: none; display: block; padding: 0; margin: 0 auto;">
                        <li style="margin-bottom: 2px; line-height: 14px; text-align: center;">Thank you for your visit.</li>
                        <li style="line-height: 14px; text-align: center;">Please come again later.</li>
                    </ul>
                    <script src="' . Asset("") . 'lib/bootstrap/js/jquery-1.11.1.min.js"></script>
                    <script>
                    
                        $(document).ready(function(e){
                        window.print();
                        });
                        $(document).click(function(e){
                        window.location.assign("' . URL("/") . '");
                        });
                    </script>
                </body>
            </html>';

            echo $html;
        }
    }

    //================/ajax=====================

    static function quotationDataBackup($data) {
//        dd($data);
        $explode = explode('---;---', $data);
//        dd($explode);
        $coa6 = $explode[0];
        $typePayment = $explode[1];
        $typeTax = $explode[2];
        $start = $explode[3];
        $end = $explode[4];
        $where = '';
//        if ($typePayment != '-1' && $typePayment != '') {
//            $where .= 'isCash = "' . $typePayment . '" ';
//        }
        if ($coa6 != '-1' && $coa6 != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            //customer lama
            if (Coa6::where('InternalID', $coa6)->count() > 0) {
                $where .= 'ACC6InternalID = ' . $coa6 . ' ';
            } else {
                $where .= 'CustomerName = ' . $coa6 . ' ';
            }
        }
        if (Auth::user()->SeeNPPN == 1) {
//            if ($typeTax != '-1' && $typeTax != '') {
//                if ($where != '') {
//                    $where .= ' AND ';
//                }
//                $where .= 'VAT = "' . $typeTax . '" ';
//            }
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'VAT = "0" ';
        } else {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'VAT = "1" ';
        }
        if ($start != '' && $end != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $start = explode('-', $start);
            $end = explode('-', $end);
            $startSearch = $start[2] . '-' . $start[1] . '-' . $start[0];
            $endSearch = $end[2] . '-' . $end[1] . '-' . $end[0];
            $where .= 'QuotationDate between "' . $startSearch . ' 00:00:00" AND "' . $endSearch . ' 23:59:59"';
        }
        $table = 'v_wquotation';
//        $table = 't_quotation_header';
        $primaryKey = 'InternalID';
//        if (checkSeeNPPN()) {
        $columns = array(
            array('db' => 'InternalID', 'dt' => 0, 'formatter' => function($d, $row) {
                    return $d;
                }),
            array('db' => 'QuotationID', 'dt' => 1),
            array(
                'db' => 'QuotationDate',
                'dt' => 2,
                'formatter' => function( $d, $row ) {
                    return date("d-m-Y", strtotime($d));
                }
            ),
            array('db' => 'CurrencyName', 'dt' => 3),
            array(
                'db' => 'CurrencyRate',
                'dt' => 4,
                'formatter' => function( $d, $row ) {
                    return number_format($d, '2', '.', ',');
                }
            ),
            array(
                'db' => 'customer',
                'dt' => 5,
                'formatter' => function( $d, $row ) {
                    return $d;
                }
            ),
//                array(
//                    'db' => 'VAT',
//                    'dt' => 6,
//                    'formatter' => function( $d, $row ) {
//                        if ($d == 0) {
//                            return 'Non Tax';
//                        } else {
//                            return 'Tax';
//                        }
//                    }
//                ),
            array(
                'db' => 'GrandTotal',
                'dt' => 6,
                'formatter' => function( $d, $row ) {
                    return number_format($d, '2', '.', ',');
                }
            ),
            array(
                'db' => 'Print',
                'dt' => 7,
                'formatter' => function($d, $row) {
                    return $d . " times";
                }
            ),
            array('db' => 'InternalID', 'dt' => 8, 'formatter' => function( $d, $row ) {
                    $data = QuotationHeader::find($d);
                    $action = '<td class="text-center">
                                    <a href="' . Route('quotationDetail', $data->QuotationID) . '">
                                        <button id="btn-' . $data->QuotationID . '-detail"
                                                class="btn btn-pure-xs btn-xs btn-detail" title="detail">
                                            <span class="glyphicon glyphicon-zoom-in"></span>
                                        </button>
                                    </a>';
                    $action .= '<a href="' . Route('quotationUpdate', $data->QuotationID) . '">
                                        <button id="btn-' . $data->QuotationID . '-update"
                                                class="btn btn-pure-xs btn-xs btn-edit" title="edit">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </button>
                                    </a>
                                    <button data-target="#m_quotationDelete" data-internal="' . $data->InternalID . '"  data-toggle="modal" role="dialog"
                                           onclick="deleteAttach(this)"  data-id="' . $data->QuotationID . '" data-name=' . $data->QuotationID . ' class="btn btn-pure-xs btn-xs btn-delete" title="delete">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button> 
                                ';
//                    $action .= '<a title="print" href="' . Route('quotationPrint', $data->QuotationID) . '">'
//                            . '<button id="btn-' . $data->QuotationID . '-print"'
//                            . 'class="btn btn-pure-xs btn-xs btn-print">'
//                            . '<span class="glyphicon glyphicon-print"></span>'
//                            . '</button></a>';
//                    $action .= '<button data-target="#r_print" data-internal="' . $data->QuotationID . '"  data-toggle="modal" role="dialog"
//                                           onclick="printAttach(this)" data-id="' . $data->QuotationID . '" data-name=' . $data->QuotationID . ' class="btn btn-pure-xs btn-xs btn-closed" title="print">
//                                        <span class="glyphicon glyphicon-print"></span>
//                                    </button>';
                    $action .= '<a target="_blank" title="print" href="' . route('quotationPrint', $data->QuotationID) . '">'
                                . '<button id="btn-' . $data->QuotationID . '-print"'
                                . 'class="btn btn-pure-xs btn-xs btn-print" title="print">'
                                . '<span class="glyphicon glyphicon-print"></span>'
                                . '</button></a>';
                    $action .= '<a href="' . Route('quotationCopy', $data->QuotationID) . '">
                                        <button id="btn-' . $data->QuotationID . '-update"
                                                class="btn btn-pure-xs btn-xs btn-edit" title="copy">
                                            <span class="glyphicon glyphicon-copy"></span>
                                        </button>
                                    </a>';
                    if ($data->TypeCustomer == 0) {
                        $action .= '<a href="' . Asset("/") . 'salesOrderNew?jenis=insertQuotation&customerSales=' . QuotationHeader::find($data->InternalID)->coa6->InternalID . '&Quotation%5B%5D=' . $data->QuotationID . '" target="_blank">
                    <button type="button" class="btn btn-pure-xs btn-xs btn-edit" title="sales order">
                        SO
                    </button>
                </a>';
                    } else {
                        $action .= '<a href="' . Asset("/") . 'salesOrderNew?jenis=insertQuotation&customerSales=' . QuotationHeader::find($data->InternalID)->CustomerName . '&Quotation%5B%5D=' . $data->QuotationID . '" target="_blank">
                    <button type="button" class="btn btn-pure-xs btn-xs btn-edit" title="sales order">
                        SO
                    </button>
                </a>';
                    }
                    return $action;
                },
                'field' => 'InternalID')
        );
//        } else {
//            $columns = array(
//                array('db' => 'QuotationID', 'dt' => 0),
//                array('db' => 'isCash', 'dt' => 1, 'formatter' => function( $d, $row ) {
//                        if ($d == 0) {
//                            return 'Cash';
//                        } else if ($d == 1) {
//                            return 'Credit';
//                        } else if ($d == 2) {
//                            return 'CBD';
//                        } else if ($d == 3) {
//                            return 'Deposit';
//                        } else {
//                            return 'Down Payment';
//                        }
//                    },
//                    'field' => 'InternalID'),
//                array(
//                    'db' => 'QuotationDate',
//                    'dt' => 2,
//                    'formatter' => function( $d, $row ) {
//                        return date("d-m-Y", strtotime($d));
//                    }
//                ),
//                array('db' => 'CurrencyName', 'dt' => 3),
//                array(
//                    'db' => 'CurrencyRate',
//                    'dt' => 4,
//                    'formatter' => function( $d, $row ) {
//                        return number_format($d, '2', '.', ',');
//                    }
//                ),
//                array(
//                    'db' => 'customer',
//                    'dt' => 5,
//                    'formatter' => function( $d, $row ) {
//                        return $d;
//                    }
//                ),
////            array(
////                'db' => 'VAT',
////                'dt' => 6,
////                'formatter' => function( $d, $row ) {
////                    if ($d == 0) {
////                        return 'Non Tax';
////                    } else {
////                        return 'Tax';
////                    }
////                }
////            ),
//                array(
//                    'db' => 'GrandTotal',
//                    'dt' => 6,
//                    'formatter' => function( $d, $row ) {
//                        return number_format($d, '2', '.', ',');
//                    }
//                ),
//                array(
//                    'db' => 'Print',
//                    'dt' => 7,
//                    'formatter' => function($d, $row) {
//                        return $d . " times";
//                    }
//                ),
//                array('db' => 'InternalID', 'dt' => 8, 'formatter' => function( $d, $row ) {
//                        $data = QuotationHeader::find($d);
//                        $action = '<td class="text-center">
//                                    <a href="' . Route('quotationDetail', $data->QuotationID) . '">
//                                        <button id="btn-' . $data->QuotationID . '-detail"
//                                                class="btn btn-pure-xs btn-xs btn-detail" title="detail">
//                                            <span class="glyphicon glyphicon-zoom-in"></span>
//                                        </button>
//                                    </a>';
//                        $action .= '<a href="' . Route('quotationUpdate', $data->QuotationID) . '">
//                                        <button id="btn-' . $data->QuotationID . '-update"
//                                                class="btn btn-pure-xs btn-xs btn-edit" title="edit">
//                                            <span class="glyphicon glyphicon-edit"></span>
//                                        </button>
//                                    </a>
//                                    <button data-target="#m_quotationDelete" data-internal="' . $data->InternalID . '"  data-toggle="modal" role="dialog"
//                                           onclick="deleteAttach(this)"  data-id="' . $data->QuotationID . '" data-name=' . $data->QuotationID . ' class="btn btn-pure-xs btn-xs btn-delete" title="delete">
//                                        <span class="glyphicon glyphicon-trash"></span>
//                                    </button> 
//                                ';
////                    $action .= '<a title="print" href="' . Route('quotationPrint', $data->QuotationID) . '">'
////                            . '<button id="btn-' . $data->QuotationID . '-print"'
////                            . 'class="btn btn-pure-xs btn-xs btn-print">'
////                            . '<span class="glyphicon glyphicon-print"></span>'
////                            . '</button></a>';
//                        $action .= '<button data-target="#r_print" data-internal="' . $data->QuotationID . '"  data-toggle="modal" role="dialog"
//                                           onclick="printAttach(this)" data-id="' . $data->QuotationID . '" data-name=' . $data->QuotationID . ' class="btn btn-pure-xs btn-xs btn-closed" title="print">
//                                        <span class="glyphicon glyphicon-print"></span>
//                                    </button>';
//                        $action .= '<a href="' . Route('quotationCopy', $data->QuotationID) . '">
//                                        <button id="btn-' . $data->QuotationID . '-update"
//                                                class="btn btn-pure-xs btn-xs btn-edit" title="copy">
//                                            <span class="glyphicon glyphicon-copy"></span>
//                                        </button>
//                                    </a>';
//                        if ($data->TypeCustomer == 0) {
//                            $action .= '<a href="' . Asset("/") . 'salesOrderNew?jenis=insertQuotation&customerSales=' . QuotationHeader::find($data->InternalID)->coa6->InternalID . '&Quotation%5B%5D=' . $data->QuotationID . '" target="_blank">
//                    <button type="button" class="btn btn-pure-xs btn-xs btn-edit" title="sales order">
//                        SO
//                    </button>
//                </a>';
//                        } else {
//                            $action .= '<a href="' . Asset("/") . 'salesOrderNew?jenis=insertQuotation&customerSales=' . QuotationHeader::find($data->InternalID)->CustomerName . '&Quotation%5B%5D=' . $data->QuotationID . '" target="_blank">
//                    <button type="button" class="btn btn-pure-xs btn-xs btn-edit" title="sales order">
//                        SO
//                    </button>
//                </a>';
//                        }
//                        return $action;
//                    },
//                    'field' => 'InternalID')
//            );
//        }
//<a href="' . Route('quotationPrintStruk', $data->QuotationID) . '" target="_blank">
//                                        <button id="btn-' . $data->QuotationID . '-print"
//                                                class="btn btn-pure-xs btn-xs btn-print">
//                                            <span class="glyphicon glyphicon-print"></span> Struk
//                                        </button>
//                                    </a>   
        $sql_details = getConnection();

        require('ssp.class.php');
        $ID_CLIENT_VALUE = Auth::user()->CompanyInternalID;
        if ($where != '') {
            $extraCondition = $where . ' AND ' . 'CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        } else {
            $extraCondition = 'CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        }
//        $join = ' INNER JOIN m_currency on m_currency.InternalID = t_quotation_header.CurrencyInternalID ';
//                . 'INNER JOIN m_coa6 on m_coa6.InternalID = t_quotation_header.ACC6InternalID';

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $extraCondition, $join = null));
    }

}
