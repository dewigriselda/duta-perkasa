<?php

class ConvertionController extends BaseController {

    public function showConvertion() {
        if ($_SERVER["REQUEST_METHOD"] === "POST") {
            if (Input::get("jenis") == "insertConvertion") {
                return $this->insertConvertion();
            } else if (Input::get("jenis") == "updateConvertion") {
                return $this->updateConvertion();
            } else if (Input::get("jenis") == "deleteConvertion") {
                return $this->deleteConvertion();
            }
        }
        return View::make("transfer.convertionDetail")
                        ->withToogle('transaction')
                        ->withAktif('convertion');
    }

    public function insertConvertion() {
        $data = Input::all();
        $rule = array(
            "InventoryUomInternalID1" => "required",
            "InventoryUomInternalID2" => "required",
            "Quantity" => "required",
            "QuantityResult" => "required",
            "Remark" => "required"
        );

        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            return View::make("transfer.convertionDetail")
                            ->withMessages("gagalInsert")
                            ->withErrors($validator->messages())
                            ->withToogle('transaction')
                            ->withAktif('convertion');
        } else {
            $convertion = new Convertion();
            $convertion->InventoryUomInternalID1 = Input::get("InventoryUomInternalID1");
            $convertion->InventoryUomInternalID2 = Input::get("InventoryUomInternalID2");
            $convertion->Quantity = Input::get("Quantity");
            $convertion->QuantityResult = Input::get("QuantityResult");
            $convertion->DateConvertion = date("Y-m-d");
            $convertion->WarehouseInternalID = Input::get("WarehouseInternalID");
            $convertion->dtRecord = date("Y-m-d h:i:s");
            $convertion->UserRecord = Auth::user()->InternalID;
            $convertion->CompanyInternalID = Auth::user()->Company->InternalID;
            $convertion->Remark = Input::get("Remark");
            $convertion->save();

            return View::make("transfer.convertionDetail")
                            ->withMessages("suksesInsert")
                            ->withToogle('transaction')
                            ->withAktif('convertion');
        }
    }

    public function updateConvertion() {
        $data = Input::all();
        $rule = array(
            "InventoryUomInternalID1" => "required",
            "InventoryUomInternalID2" => "required",
            "Quantity" => "required",
            "QuantityResult" => "required",
            "Remark" => "required"
        );

        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            return View::make("transfer.convertionDetail")
                            ->withMessages("gagalUpdate")
                            ->withErrors($validator->messages())
                            ->withToogle('transaction')
                            ->withAktif('convertion');
        } else {
            $convertion = Convertion::find(Input::get("InternalID"));
            $convertion->InventoryUomInternalID1 = Input::get("InventoryUomInternalID1");
            $convertion->InventoryUomInternalID2 = Input::get("InventoryUomInternalID2");
            $convertion->Quantity = Input::get("Quantity");
            $convertion->QuantityResult = Input::get("QuantityResult");
            $convertion->DateConvertion = date("Y-m-d");
            $convertion->WarehouseInternalID = Input::get("WarehouseInternalID");
            $convertion->dtRecord = date("Y-m-d h:i:s");
            $convertion->UserRecord = Auth::user()->InternalID;
            $convertion->Remark = Input::get("Remark");
            $convertion->save();

            return View::make("transfer.convertionDetail")
                            ->withMessages("suksesUpdate")
                            ->withToogle('transaction')
                            ->withAktif('convertion');
        }
    }

    public function deleteConvertion() {
        $convertion = Convertion::find(Input::get("InternalID"));

        if (!is_null($convertion)) {
            if ($convertion->CompanyInternalID == Auth::user()->Company->InternalID) {
                $convertion->delete();
                return View::make("transfer.convertionDetail")
                                ->withMessages("suksesDelete")
                                ->withToogle('transaction')
                                ->withAktif('convertion');
            } else {
                return View::make("transfer.convertionDetail")
                                ->withMessages("accessDenied")
                                ->withToogle('transaction')
                                ->withAktif('convertion');
            }
        }
        return View::make("transfer.convertionDetail")
                        ->withMessages("accessDenied")
                        ->withToogle('transaction')
                        ->withAktif('convertion');
    }

    //======================ajax========================
    public function getSelectedInventoryUom2() {
        $id = Input::get("id");
        ?>
        <select class="chosen-select choosen-modal" id="inventoryUom2" style="" name="InventoryUomInternalID2">
            <?php foreach (InventoryUom::where("InventoryInternalID", $id)->where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('InternalID')->get() as $InventoryUom) { ?>
                <option value="<?php echo $InventoryUom->InternalID . "---;---" . $InventoryUom->Value . "---;---" . $InventoryUom->Uom->UomID ?> "  >
                    <?php echo $InventoryUom->Inventory->InventoryName . " - " . $InventoryUom->Uom->UomID ?>
                </option>
            <?php } ?>
        </select>
        <script>
            $("document").ready(function () {
                nilaiAwal = parseFloat(uom1[1]);

                tampUom2 = $("#inventoryUom2").val();
                uom2 = tampUom2.split("---;---");
                nilaiAkhir = parseFloat(uom2[1]);
                uom = uom2[2];

                qty = $("#quantity").val();
                result = qty * (nilaiAwal / nilaiAkhir);
                $("#quantityResult").val(result);
                $("#quantityResultLabel").html(" " + result + " " + uom);
            });
            $("#inventoryUom1").change(function () {
                tamp = $(this).val();
                tamp1 = tamp.split("---;---");
                nilaiAwal = parseFloat(tamp1[1]);

                qty = $("#quantity").val();
                result = qty * (nilaiAwal / nilaiAkhir);
                $("#quantityResult").val(result);
                $("#quantityResultLabel").html(" " + result + " " + uom);
            });

            $("#inventoryUom2").change(function () {
                tamp = $(this).val();
                tamp1 = tamp.split("---;---");
                nilaiAkhir = parseFloat(tamp1[1]);
                uom = tamp1[2];

                qty = $("#quantity").val();
                result = qty * (nilaiAwal / nilaiAkhir);
                $("#quantityResult").val(result);
                $("#quantityResultLabel").html(" " + result + " " + uom);
            });

            $("#quantity").keyup(function () {
                qty = $("#quantity").val();
                result = qty * (nilaiAwal / nilaiAkhir);
                $("#quantityResult").val(result);
                $("#quantityResultLabel").html(" " + result + " " + uom);
            });
        </script>
        <?php
    }

    public function getSelectedInventoryUom2Update() {
        $id = InventoryUom::find(Input::get("id"))->InventoryInternalID;
        ?>
        <select class="chosen-select choosen-modal" id="inventoryUom2Update" style="" name="InventoryUomInternalID2">
            <?php foreach (InventoryUom::where("InventoryInternalID", $id)->where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('InternalID')->get() as $InventoryUom) { ?>
                <option value="<?php echo $InventoryUom->InternalID . "---;---" . $InventoryUom->Value . "---;---" . $InventoryUom->Uom->UomID ?> "  >
                    <?php echo $InventoryUom->Inventory->InventoryName . " - " . $InventoryUom->Uom->UomID ?>
                </option>
            <?php } ?>
        </select>
        <script>
            $("document").ready(function () {
                nilaiAwalUpdate = parseFloat(uom1Update[1]);
                tampUom2Update = $("#inventoryUom2Update").val();
                uom2Update = tampUom2Update.split("---;---");
                nilaiAkhirUpdate = parseFloat(uom2Update[1]);
                uomUpdate = uom2Update[2];

                qtyUpdate = $("#quantityUpdate").val();
                resultUpdate = qtyUpdate * (nilaiAwalUpdate / nilaiAkhirUpdate);
                $("#quantityResultUpdate").val(resultUpdate);
                $("#quantityResultLabelUpdate").html(" " + resultUpdate + " " + uomUpdate);
            });
            $("#inventoryUom1Update").change(function () {
                tampUpdate = $(this).val();
                tamp1Update = tampUpdate.split("---;---");
                nilaiAwalUpdate = parseFloat(tamp1Update[1]);

                qtyUpdate = $("#quantityUpdate").val();
                resultUpdate = qtyUpdate * (nilaiAwalUpdate / nilaiAkhirUpdate);
                $("#quantityResultUpdate").val(resultUpdate);
                $("#quantityResultLabelUpdate").html(" " + resultUpdate + " " + uomUpdate);
            });

            $("#inventoryUom2Update").change(function () {
                tampUpdate = $(this).val();
                tamp1Update = tampUpdate.split("---;---");
                nilaiAkhirUpdate = parseFloat(tamp1Update[1]);
                uomUpdate = tamp1Update[2];

                qtyUpdate = $("#quantityUpdate").val();
                resultUpdate = qtyUpdate * (nilaiAwalUpdate / nilaiAkhirUpdate);
                $("#quantityResultUpdate").val(resultUpdate);
                $("#quantityResultLabelUpdate").html(" " + resultUpdate + " " + uomUpdate);
            });

            $("#quantityUpdate").keyup(function () {
                qtyUpdate = $("#quantityUpdate").val();
                resultUpdate = qtyUpdate * (nilaiAwalUpdate / nilaiAkhirUpdate);
                $("#quantityResultUpdate").val(resultUpdate);
                $("#quantityResultLabelUpdate").html(" " + resultUpdate + " " + uomUpdate);
            });
        </script>
        <?php
    }

    public function getInventoryUomInternalID() {
        $id = Input::get("id");
        $string = "";
        $InventoryUom = InventoryUom::Find($id);
        $string = $InventoryUom->InternalID . "---;---" . $InventoryUom->Value . "---;---" . $InventoryUom->InventoryInternalID;
        return $string;
    }

    public function getResultSearchConvertion() {
        $input = splitSearchValue(Input::get("id"));
        $inventoryUom = InventoryUom::join("m_inventory", "m_inventory.InternalID", "=", "m_inventory_uom.InventoryInternalID")
                ->where("m_inventory.Status", 0)
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where("m_inventory.InventoryID", "like", $input)
                ->orWhere("m_inventory.InventoryName", "like", $input)
                ->where("m_inventory.Status", 0)
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->orderBy('m_inventory_uom.InternalID')
                ->select(DB::raw('m_inventory_uom.InternalID as InventoryUomInternalID,'
                                . ' m_inventory_uom.Value as Value, '
                                . ' m_inventory_uom.InventoryInternalID as InventoryInternalID, '
                                . 'm_inventory_uom.UomInternalID as UomInternalID'))
                ->take(100)
                ->get();
        if (count($inventoryUom) == 0) {
            ?>
            <span>Inventory Uom with keywords <i><u><?php echo Input::get("id"); ?></u></i> can't be found</span>
            <?php
        } else {
            ?>
            <select class="chosen-select choosen-modal" id="inventoryUom1" style="" name="InventoryUomInternalID1">
                <?php
                foreach ($inventoryUom as $data) {
                    $inventory = Inventory::find($data->InventoryInternalID);
                    $uom = Uom::find($data->UomInternalID);
                    ?>
                    <option value="<?php echo $data->InventoryUomInternalID . '---;---' . $data->Value . '---;---' . $data->InventoryInternalID ?>"><?php echo $inventory->InventoryName . ' - ' . $uom->UomID ?></option>
                    <?php
                }
                ?>
            </select>
            <script>
                $(document).ready(function () {
                    var nilaiAwal = 0;
                    var nilaiAkhir = 0;
                    var qty = 0;
                    var result = 0;
                    var uom = "";
                    if ($('select[name=InventoryUomInternalID1]').val() != "") {
                        //deklarasi awal
                        tampUom1 = $("#inventoryUom1").val();
                        uom1 = tampUom1.split("---;---");
                        $.post(getSelectedInventoryUom2, {id: uom1[2]}).done(function (data) {
                            $("#liSelect").html(data);
                        });
                    }
                    $("#inventoryUom1").change(function () {

                        tampUom1 = $("#inventoryUom1").val();
                        uom1 = tampUom1.split("---;---");
                        $.post(getSelectedInventoryUom2, {id: uom1[2]}).done(function (data) {
                            $("#liSelect").html(data);
                        });

                        tamp = $(this).val();
                        tamp1 = tamp.split("---;---");
                        nilaiAwal = parseFloat(tamp1[1]);

                        qty = $("#quantity").val();
                        result = qty * (nilaiAwal / nilaiAkhir);
                        $("#quantityResult").val(result);
                        $("#quantityResultLabel").html(" " + result + " " + uom);
                    });

                    $("#quantity").keyup(function () {
                        qty = $("#quantity").val();
                        result = qty * (nilaiAwal / nilaiAkhir);
                        $("#quantityResult").val(result);
                        $("#quantityResultLabel").html(" " + result + " " + uom);
                    });
                });
            </script>
            <?php
        }
    }

    public function getResultSearchConvertionUpdate() {
        $input = splitSearchValue(Input::get("id"));
        $inventoryUom = InventoryUom::join("m_inventory", "m_inventory.InternalID", "=", "m_inventory_uom.InventoryInternalID")
                ->where("m_inventory.Status", 0)
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where("m_inventory.InventoryID", "like", $input)
                ->orWhere("m_inventory.InventoryName", "like", $input)
                ->where("m_inventory.Status", 0)
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->orderBy('m_inventory_uom.InternalID')
                ->select(DB::raw('m_inventory_uom.InternalID as InventoryUomInternalID,'
                                . ' m_inventory_uom.Value as Value, '
                                . ' m_inventory_uom.InventoryInternalID as InventoryInternalID, '
                                . 'm_inventory_uom.UomInternalID as UomInternalID'))
                ->take(100)
                ->get();
        if (count($inventoryUom) == 0) {
            ?>
            <span>Inventory Uom with keywords <i><u><?php echo Input::get("id"); ?></u></i> can't be found</span>
            <?php
        } else {
            ?>
            <select class="chosen-select choosen-modal" id="inventoryUom1Update" style="" name="InventoryUomInternalID1">
                <?php
                foreach ($inventoryUom as $data) {
                    $inventory = Inventory::find($data->InventoryInternalID);
                    $uom = Uom::find($data->UomInternalID);
                    ?>
                    <option value="<?php echo $data->InventoryUomInternalID . '---;---' . $data->Value . '---;---' . $data->InventoryUomInternalID ?>"><?php echo $inventory->InventoryName . ' - ' . $uom->UomID ?></option>
                    <?php
                }
                ?>
            </select>
            <script>
                $(document).ready(function () {
                    var nilaiAwal = 0;
                    var nilaiAkhir = 0;
                    var qty = 0;
                    var result = 0;
                    var uom = "";
                    if ($('select[name=InventoryUomInternalID1]').val() != "") {
                        //deklarasi awal
                        tampUom1 = $("#inventoryUom1Update").val();
                        uom1 = tampUom1.split("---;---");
                        $.post(getSelectedInventoryUom2Update, {id: uom1[2]}).done(function (data) {
                            $("#liSelectUpdate").html(data);
                        });
                    }
                    $("#inventoryUom1Update").change(function () {

                        tampUom1 = $("#inventoryUom1Update").val();
                        uom1 = tampUom1.split("---;---");
                        $.post(getSelectedInventoryUom2Update, {id: uom1[2]}).done(function (data) {
                            $("#liSelectUpdate").html(data);
                        });

                        tamp = $(this).val();
                        tamp1 = tamp.split("---;---");
                        nilaiAwal = parseFloat(tamp1[1]);

                        qty = $("#quantityUpdate").val();
                        result = qty * (nilaiAwal / nilaiAkhir);
                        $("#quantityResultUpdate").val(result);
                        $("#quantityResultLabelUpdate").html(" " + result + " " + uom);
                    });

                    $("#quantityUpdate").keyup(function () {
                        qty = $("#quantityUpdate").val();
                        result = qty * (nilaiAwal / nilaiAkhir);
                        $("#quantityResultUpdate").val(result);
                        $("#quantityResultLabelUpdate").html(" " + result + " " + uom);
                    });
                });
            </script>
            <?php
        }
    }

    //=====================/ajax========================
}
