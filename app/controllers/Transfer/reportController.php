<?php

class ReportController extends BaseController {

    public function showReport() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'barangmasuk') {
                return $this->reportInventoryIn();
            }
            if (Input::get('jenis') == 'barangkeluar') {
                return $this->reportInventoryOut();
            }
        }
        return View::make('transfer.report')
                        ->withToogle('transaction')->withAktif('report');
    }

    public function reportInventoryIn() {
        Excel::create('LaporanGudang_BarangMasuk', function($excel) {
            $excel->sheet('LaporanGudang_BarangMasuk', function($sheet) {
//                if (Input::get("hide") == "hide") {
//                    $sheet->getColumnDimension('G')->setVisible(false);
//                }
                $startT = explode('-', Input::get('sDate'));
                $endT = explode('-', Input::get('eDate'));
                $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
                $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
                $warehouse = Input::get('warehouse');
                $wherewarehouse = '';
                if ($warehouse == 'all') {
                    $tanda = '!=';
                    $wherewarehouse = 'm_warehouse.Type = ' . Auth::user()->WarehouseCheck;
                } else {
                    $tanda = '=';
                    $wherewarehouse = 'm_warehouse.Type > -1';
                }
                $sheet->mergeCells('B1:C1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Laporan Barang Masuk Gudang");
                $sheet->setCellValueByColumnAndRow(1, 2, "Periode :");
                $sheet->setCellValueByColumnAndRow(2, 2, date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))));
                $row = 4;
                $rowawal = 4;

                $sheet->setCellValueByColumnAndRow(1, $row, 'TGL MASUK');
                $sheet->setCellValueByColumnAndRow(2, $row, 'NO.TRANSAKSI');
                $sheet->setCellValueByColumnAndRow(3, $row, 'GB');
                $sheet->setCellValueByColumnAndRow(4, $row, 'SUPPLIER / CUSTOMER');
                $sheet->setCellValueByColumnAndRow(5, $row, 'NAMA BARANG');
                $sheet->setCellValueByColumnAndRow(6, $row, 'UNIT');
                $sheet->setCellValueByColumnAndRow(7, $row, 'TTD PENERIMA');
                $sheet->setCellValueByColumnAndRow(8, $row, 'KET');
                $row++;

                $cekada = '';
                foreach (DB::table("v_wreportstock")->join("m_warehouse", "v_wreportstock.Warehouse", "=", "m_warehouse.InternalID")
                        ->whereRaw($wherewarehouse)
                        ->whereBetween('Date', Array($start, $end))
                        ->where("Warehouse", $tanda, $warehouse)->where("v_wreportstock.Type", "in")
                        ->orderBy('Date', 'asc')->orderBy('ID', 'asc')->get() as $si) {
                    $rowawal = $row;

                    if ($cekada != $si->ID) {
                        if ($cekada != '')
                            $row++;
                        $cekada = $si->ID;
                        $sheet->setCellValueByColumnAndRow(1, $row, date("d/m/Y", strtotime($si->Date)));
                        $sheet->setCellValueByColumnAndRow(2, $row, $si->ID);
                        $sheet->setCellValueByColumnAndRow(3, $row, Warehouse::find($si->Warehouse)->WarehouseName);
                        if (strpos($si->ID, "MRV") !== false) {
                            $sheet->setCellValueByColumnAndRow(4, $row, Coa6::find(MrvHeader::where("MrvID", $si->ID)->first()->ACC6InternalID)->ACC6Name);
                        } else if (strpos($si->ID, "SI") !== false) {
                            $sheet->setCellValueByColumnAndRow(4, $row, Coa6::find(SalesReturnHeader::where("SalesReturnID", $si->ID)->first()->ACC6InternalID)->ACC6Name);
                        } else {
                            $sheet->setCellValueByColumnAndRow(4, $row, "-");
                        }
                    }

                    $sheet->setCellValueByColumnAndRow(5, $row, "[" . Variety::find(Inventory::find($si->Inventory)->VarietyInternalID)->VarietyName . '] ' . Inventory::find($si->Inventory)->InventoryName . " (" . Inventory::find($si->Inventory)->Power . ")");
//                    $sheet->setCellValueByColumnAndRow(5, $row, Inventory::find($si->Inventory)->InventoryName);
                    $sheet->setCellValueByColumnAndRow(6, $row, $si->Qty);
                    $row++;
                }
                $row++;
                $row--;
                $row--;
                $sheet->setBorder('B4:I' . $row, 'thin');
                $sheet->cells('B2:C2', function($cells) {
                    $cells->setFontWeight('bold');
                });
                $sheet->cells('B4:I4', function($cells) {
                    $cells->setFontWeight('bold');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('left');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
            });
        })->export('xls');
    }

    public function reportInventoryOut() {
        Excel::create('LaporanGudang_BarangKeluar', function($excel) {
            $excel->sheet('LaporanGudang_BarangKeluar', function($sheet) {
//                if (Input::get("hide") == "hide") {
//                    $sheet->getColumnDimension('G')->setVisible(false);
//                }
                $startT = explode('-', Input::get('sDate'));
                $endT = explode('-', Input::get('eDate'));
                $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
                $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
                $warehouse = Input::get('warehouse');
                $wherewarehouse = '';
                if ($warehouse == 'all') {
                    $tanda = '!=';
                    $wherewarehouse = 'm_warehouse.Type = ' . Auth::user()->WarehouseCheck;
                } else {
                    $tanda = '=';
                    $wherewarehouse = 'm_warehouse.Type > -1';
                } $tanda = '=';

                $sheet->mergeCells('B1:C1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Laporan Barang Keluar Gudang");
                $sheet->setCellValueByColumnAndRow(1, 2, "Periode :");
                $sheet->setCellValueByColumnAndRow(2, 2, date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))));
                $row = 4;
                $rowawal = 4;

                $sheet->setCellValueByColumnAndRow(1, $row, 'TGL KELUAR');
                $sheet->setCellValueByColumnAndRow(2, $row, 'NO.TRANSAKSI');
                $sheet->setCellValueByColumnAndRow(3, $row, 'GB');
                $sheet->setCellValueByColumnAndRow(4, $row, 'CUSTOMER / SUPPLIER');
                $sheet->setCellValueByColumnAndRow(5, $row, 'NAMA BARANG');
                $sheet->setCellValueByColumnAndRow(6, $row, 'UNIT');
                $sheet->setCellValueByColumnAndRow(7, $row, 'TTD PENERIMA');
                $sheet->setCellValueByColumnAndRow(8, $row, 'KET');
                $row++;

                $cekada = '';
                foreach (DB::table("v_wreportstock")->join("m_warehouse", "v_wreportstock.Warehouse", "=", "m_warehouse.InternalID")
                        ->whereRaw($wherewarehouse)
                        ->whereBetween('Date', Array($start, $end))
                        ->where("Warehouse", $tanda, $warehouse)->where("v_wreportstock.Type", "out")
                        ->orderBy('Date', 'asc')->orderBy('ID', 'asc')->get() as $si) {
                    $rowawal = $row;

                    if ($cekada != $si->ID) {
                        if ($cekada != '')
                            $row++;
                        $cekada = $si->ID;
                        $sheet->setCellValueByColumnAndRow(1, $row, date("d/m/Y", strtotime($si->Date)));
                        $sheet->setCellValueByColumnAndRow(2, $row, $si->ID);
                        $sheet->setCellValueByColumnAndRow(3, $row, Warehouse::find($si->Warehouse)->WarehouseName);
                        if (strpos($si->ID, "SH") !== false) {
                            $sheet->setCellValueByColumnAndRow(4, $row, Coa6::find(ShippingAddHeader::where("ShippingID", $si->ID)->first()->ACC6InternalID)->ACC6Name);
                        } else if (strpos($si->ID, "PI") !== false) {
                            $sheet->setCellValueByColumnAndRow(4, $row, Coa6::find(PurchaseReturnHeader::where("PurchaseReturnID", $si->ID)->first()->ACC6InternalID)->ACC6Name);
                        } else {
                            $sheet->setCellValueByColumnAndRow(4, $row, "-");
                        }
                    }

                    $sheet->setCellValueByColumnAndRow(5, $row, "[" . Variety::find(Inventory::find($si->Inventory)->VarietyInternalID)->VarietyName . '] ' . Inventory::find($si->Inventory)->InventoryName . " (" . Inventory::find($si->Inventory)->Power . ")");
//                    $sheet->setCellValueByColumnAndRow(5, $row, Inventory::find($si->Inventory)->InventoryName);
                    $sheet->setCellValueByColumnAndRow(6, $row, $si->Qty);
                    $row++;
                }
                $row++;
                $row--;
                $row--;
                $sheet->setBorder('B4:I' . $row, 'thin');
                $sheet->cells('B2:C2', function($cells) {
                    $cells->setFontWeight('bold');
                });
                $sheet->cells('B4:I4', function($cells) {
                    $cells->setFontWeight('bold');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('left');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
            });
        })->export('xls');
    }

}
