<?php

class TransferController extends BaseController {

    public function showTransfer() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteTransfer') {
                return $this->deleteTransfer();
            }
            if (Input::get('jenis') == 'inventoryIn') {
                return $this->reportInventoryIn();
            }
            if (Input::get('jenis') == 'inventoryOut') {
                return $this->reportInventoryOut();
            }
        }
        return View::make('transfer.transfer')
                        ->withToogle('transaction')->withAktif('transfer');
    }

    public function transferNew() {
        $so = Input::get('so');
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteTransfer') {
                return $this->deleteTransfer();
            } else if (Input::get('jenis') == 'inventoryIn') {
                return $this->reportInventoryIn();
            } else if (Input::get('jenis') == 'inventoryOut') {
                return $this->reportInventoryOut();
            } else {
                return $this->insertTransfer();
            }
        }
        if (Input::get('jenis') == 'insertTransfer') {
            if ($so == null) {
                return $this->insertTransferOrder();
            } else {
                return $this->insertTransferOrder($so);
            }
        }

        $transfer = $this->createID(0) . '.';
        return View::make('transfer.transferNew')
                        ->withToogle('transaction')->withAktif('transfer')
                        ->withTransfer($transfer);
    }

    //insert transfer order, transfer dari sales order
    public function insertTransferOrder($salesorder = null) {
        $data = Input::all();
        $rule = array(
            "SalesOrder" => "required"
        );
        $validator = Validator::make($data, $rule);
        if ($validator->fails() && $salesorder == null) {
            return View::make('transfer.transferNew')
                            ->withToogle('transaction')->withAktif('transfer');
        } else {
            if ($salesorder != null)
                $so = SalesOrderHeader::where("SalesOrderID", $salesorder)->first();
            else
                $so = SalesOrderHeader::where("SalesOrderID", Input::get('SalesOrder'))->first();
            $transfer = $this->createID(0) . '.';
            return View::make('transfer.transferNew')
                            ->withToogle('transaction')->withAktif('transfer')
                            ->withSalesorder($so)
                            ->withTransfer($transfer);
        }
    }

    public function transferDetail($id) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteTransfer') {
                return $this->deleteTransfer();
            }
            if (Input::get('jenis') == 'inventoryIn') {
                return $this->reportInventoryIn();
            }
            if (Input::get('jenis') == 'inventoryOut') {
                return $this->reportInventoryOut();
            }
        }
        $id = TransferHeader::getIdtransfer($id);
        $header = TransferHeader::find($id);
        $detail = TransferHeader::find($id)->transferDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            return View::make('transfer.transferDetail')
                            ->withToogle('transaction')->withAktif('transfer')
                            ->withHeader($header)
                            ->withDetail($detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showTransfer');
        }
    }

    public function transferUpdate($id) {
        $id = TransferHeader::getIdtransfer($id);
        $header = TransferHeader::find($id);
        $detail = TransferHeader::find($id)->transferDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                if (Input::get('jenis') == 'deleteTransfer') {
                    return $this->deleteTransfer();
                } else if (Input::get('jenis') == 'inventoryIn') {
                    return $this->reportInventoryIn();
                } else if (Input::get('jenis') == 'inventoryOut') {
                    return $this->reportInventoryOut();
                } else {
                    return $this->updateTransfer($id);
                }
            }
            $transfer = $this->createID(0) . '.';
            return View::make('transfer.transferUpdate')
                            ->withToogle('transaction')->withAktif('transfer')
                            ->withHeader($header)
                            ->withDetail($detail)
                            ->withTransfer($transfer);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showTransfer');
        }
    }

    public function insertTransfer() {
        //rule
        $rule = array(
            'date' => 'required',
            'remark' => 'required|max:1000',
            'warehouseSource' => 'required',
            'warehouseDestiny' => 'required',
            'type' => 'required',
            'inventory' => 'required',
            'uom' => 'required'
        );
        $transferNumber = '';
        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalInsert';
            $error = $validator->messages();
        } else {
            //valid
            //insert header
            $header = new TransferHeader;
            $transfer = $this->createID(1) . '.';
            $date = explode('-', Input::get('date'));
            $yearDigit = substr($date[2], 2);
            $transfer .= $date[1] . $yearDigit . '.';
            $transferNumber = TransferHeader::getNextIDTransfer($transfer);
            $header->TransferID = $transferNumber;
            $header->TransferDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $header->WarehouseInternalID = Input::get('warehouseSource');
            $header->WarehouseDestinyInternalID = Input::get('warehouseDestiny');
            $header->TransferType = Input::get('type');
            $header->UserRecord = Auth::user()->UserID;
            $header->CompanyInternalID = Auth::user()->Company->InternalID;
            $header->UserModified = '0';
            $header->Remark = Input::get('remark');
            $header->save();
            //insert detail
            $total = 0;
            for ($a = 0; $a < count(Input::get('inventory')); $a++) {
                $qtyValue = Input::get('qty')[$a];
                if ($qtyValue > 0) {
                    $detail = new TransferDetail();
                    $detail->TransferInternalID = $header->InternalID;
                    $detail->InventoryInternalID = Input::get('inventory')[$a];
                    $detail->UomInternalID = Input::get('uom')[$a];
                    $detail->Qty = $qtyValue;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = '0';
                    $detail->save();
                    setTampInventory(Input::get('inventory')[$a]);
                }
            }

            if (Input::get('idSalesOrder') != NULL) {
                $transferorder = new TransferOrder();
                $transferorder->SalesOrderInternalID = Input::get('idSalesOrder');
                $transferorder->TransferInternalID = $header->InternalID;
                $transferorder->UserRecord = Auth::user()->UserID;
                $transferorder->UserModified = '0';
                $transferorder->CompanyInternalID = Auth::user()->Company->InternalID;
                $transferorder->Remark = "-";
                $transferorder->save();
            }

            $messages = 'suksesInsert';
            $error = '';
        }
        
        if ($messages == 'suksesInsert') {
            return Redirect::route('transferDetail', $header->TransferID);
        }
        
        $transfer = $this->createID(0) . '.';
        return View::make('transfer.transferNew')
                        ->withToogle('transaction')->withAktif('transfer')
                        ->withTransfer($transfer)
                        ->withError($error)
                        ->withMessages($messages);
    }

    public function updateTransfer($id) {
        //tipe
        $headerUpdate = TransferHeader::find($id);
        $detailUpdate = TransferHeader::find($id)->transferDetail()->get();
        //rule
        $rule = array(
            'remark' => 'required|max:1000',
            'warehouseSource' => 'required',
            'warehouseDestiny' => 'required',
            'type' => 'required',
            'inventory' => 'required',
            'uom' => 'required'
        );
        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalUpdate';
            $error = $validator->messages();
        } else {
            $header = TransferHeader::find(Input::get('TransferInternalID'));
            $header->WarehouseInternalID = Input::get('warehouseSource');
            $header->WarehouseDestinyInternalID = Input::get('warehouseDestiny');
            $header->TransferType = Input::get('type');
            $header->UserModified = Auth::user()->UserID;
            $header->Remark = Input::get('remark');
            $header->save();

            //delete transfer detail -- nantinya insert ulang
            TransferDetail::where('TransferInternalID', '=', Input::get('TransferInternalID'))->update(array('is_deleted' => 1));
            //insert detail
            $total = 0;
            for ($a = 0; $a < count(Input::get('inventory')); $a++) {
                $qtyValue = Input::get('qty')[$a];
                if ($qtyValue > 0) {
                    $detail = new TransferDetail();
                    $detail->TransferInternalID = Input::get('TransferInternalID');
                    $detail->InventoryInternalID = Input::get('inventory')[$a];
                    $detail->UomInternalID = Input::get('uom')[$a];
                    $detail->Qty = $qtyValue;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = Auth::user()->UserID;
                    $detail->save();
                    setTampInventory(Input::get('inventory')[$a]);
                }
            }
            TransferDetail::where('TransferInternalID', '=', Input::get('TransferInternalID'))->where('is_deleted', 1)->delete();
            $messages = 'suksesUpdate';
            $error = '';
        }

        if ($messages == 'suksesUpdate') {
            return Redirect::route('transferDetail', $header->TransferID);
        }
        
        //tipe
        $header = TransferHeader::find($id);
        $detail = TransferHeader::find($id)->transferDetail()->get();
        $transfer = $this->createID(0) . '.';
        return View::make('transfer.transferUpdate')
                        ->withToogle('transaction')->withAktif('transfer')->withHeader($header)
                        ->withDetail($detail)
                        ->withTransfer($transfer)->withError($error)
                        ->withMessages($messages);
    }

    public function deleteTransfer() {
        $transfer = null;
        if (is_null($transfer)) {
            //tidak ada yang menggunakan data transfer maka data boleh dihapus
            //hapus journal
            $transferHeader = TransferHeader::find(Input::get('InternalID'));
            if ($transferHeader->CompanyInternalID == Auth::user()->Company->InternalID) {
                //hapus detil
                $detilData = TransferHeader::find(Input::get('InternalID'))->transferDetail;
                foreach ($detilData as $value) {
                    $detil = transferDetail::find($value->InternalID);
                    $detil->delete();
                    setTampInventory($detil->InventoryInternalID);
                }
                //hapus transfer order
                TransferOrder::where("TransferInternalID", Input::get('InternalID'))->delete();
                //hapus transfer
                $transfer = TransferHeader::find(Input::get('InternalID'));
                $transfer->delete();
                $messages = 'suksesDelete';
            } else {
                $messages = 'accessDenied';
            }
        } else {
            //ada maka data tidak boleh dihapus
            $messages = 'gagalDelete';
        }
        return View::make('transfer.transfer')
                        ->withToogle('transaction')->withAktif('transfer')
                        ->withMessages($messages);
    }

    function transferPrint($id) {
        $id = TransferHeader::getIdtransfer($id);
        $header = TransferHeader::find($id);
        $detail = TransferHeader::find($id)->transferDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Transfer</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;padding-left:6px;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 18px; width: 100%;">
                            <table>
                            <tr>
                            <td width="400px">
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Transfer ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $header->TransferID . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->TransferDate)) . '</td>
                                 </tr>

                                </table>
                                </td>
                                <td>
                                <table>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Source</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $header->WarehouseSource->WarehouseName . '</td>
                                     </tr>                                
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Destiny</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $header->WarehouseDestiny->WarehouseName . '</td>
                                     </tr>
                                </table></td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Uom</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Quantity Transfer</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            $total = 0;
            if (count($detail) > 0) {
                foreach ($detail as $data) {
                    $inventory = Inventory::find($data->InventoryInternalID);
                    $inv = $inventory->InventoryID . ' ' . $inventory->InventoryName;
                    $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $inv . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->uom->UomID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Qty . '</td>
                            </tr>';
                    $total += $data->SubTotal;
                }
            } else {
                $html.= '<tr>
                            <td colspan="2" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;  text-align: center">There is no inventory registered in this transfer.</td>
                        </tr>';
            }
            $html.= '</tbody>
                            </table>
                            <br>
                                <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                                    <table>
                                    <tr>
                                    <td width="275px" style="vertical-align: text-top;">
                                        <table>
                                            <tr style="background: none;">
                                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Remark</td>
                                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $header->Remark . '</td>
                                             </tr>
                                        </table>
                                    </td>
                                    </tr>
                                    </table>
                                </div>                                

                    </div>
                </body>
            </html>';
            return PDF ::load($html, 'A4', 'portrait')->show();
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showTransfer');
        }
    }

    function createID($tipe) {
        $transfer = 'TF';
        if ($tipe == 0) {
            $transfer .= '.' . date('m') . date('y');
        }
        return $transfer;
    }

    public function formatCariIDTransfer() {
        $date = explode('-', Input::get('date'));
        $id = $this->createID(1);
        $yearDigit = substr($date[2], 2);
        $id .= '.' . $date[1] . $yearDigit . '.';
        echo TransferHeader::getNextIDTransfer($id);
    }

    //==================ajax==========================
    public function getUomThisInventoryTransfer() {
        $inventoryUom = InventoryUom::where("InventoryInternalID", Input::get("id"))->get();
        foreach ($inventoryUom as $data) {
            ?>
            <option value="<?php echo $data->UomInternalID ?>" <?php echo ($data->Default == 1 ? "selected" : "") ?>><?php echo $data->Uom->UomID; ?></option>
            <?php
        }
    }

    public function getResultSearchTransferSO() {
        $hitung = 0;
        $input = splitSearchValue(Input::get("id"));
        $date = date("Y-m-d", strtotime(Input::get("id")));
        $salesOrderHeader = SalesOrderHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('Closed', 0)
                ->where('Status', 1)
                ->where("VAT", "!=", Auth::user()->SeeNPPN)
                ->where(function($query) use ($input, $date) {
                    $query->where("SalesOrderID", "like", $input)
                    ->orWhere("SalesOrderDate", "like", '%' . $date . '%')
                    ->orWhere("UserRecord", "like", $input);
                })
                ->OrderBy('SalesOrderDate', 'desc')
                ->get();
        if (count($salesOrderHeader) == 0) {
            ?>
            <span>Sales Order with keywords <i><u><?php echo Input::get("id"); ?></u></i> can't be found</span>
        <?php } else {
            ?>
            <select class="chosen-select choosen-modal" id="salesorder" style="" name="SalesOrder">
                <?php
                foreach ($salesOrderHeader as $shipping) {
                    if (checkSalesOrder($shipping->InternalID) && $hitung < 100) {
                        ?>
                        <option value="<?php echo $shipping->SalesOrderID ?>"><?php echo $shipping->SalesOrderID . ' | ' . date("d-m-Y", strtotime($shipping->SalesOrderDate)) . ' | ' . $shipping->coa6->ACC6Name ?></option>
                        <?php
                        $hitung++;
                    }
                }
                ?>
            </select>
            <script>
                $(document).ready(function () {
                    var hitung = '<?php echo $hitung; ?>';
                    if (hitung == 0) {
                        $('#selectSalesOrder').after('<span>There is no result.</span>');
                        $('#selectSalesOrder').remove();
                    } else {
                        $("#btn-add-so").removeAttr("disabled");
                    }
                });
            </script>
            <?php
        }
    }

    public function transferPrintGrid($id) {
        $id = TransferHeader::getIdtransfer($id);
        $header = TransferHeader::find($id);
        $detail = TransferHeader::find($id)->transferDetail()->get();

        return View::make('template.print.transferPrint')
                        ->withHeader($header)
                        ->withDetail($detail);
    }

    public function reportInventoryIn() {
        Excel::create('LaporanGudang_BarangMasuk', function($excel) {
            $excel->sheet('LaporanGudang_BarangMasuk', function($sheet) {
//                if (Input::get("hide") == "hide") {
//                    $sheet->getColumnDimension('G')->setVisible(false);
//                }
                $startT = explode('-', Input::get('sDate'));
                $endT = explode('-', Input::get('eDate'));
                $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
                $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
//                $supplier = Input::get('customer');
//                if ($supplier == '-1') {
//                    $tanda = '!=';
//                } else {
//                    $tanda = '=';
//                }
                $sheet->mergeCells('B1:C1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Laporan Barang Masuk Gudang");
//                $sheet->mergeCells('B2:C2');
                $sheet->setCellValueByColumnAndRow(1, 2, "Periode :");
                $sheet->setCellValueByColumnAndRow(2, 2, date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))));
                $row = 4;
                $rowawal = 4;
                $number = 1;

//                foreach (Coa6::where('CompanyInternalID', Auth::user()->CompanyInternalID)->where('Type', 'c')->where('InternalID', $tanda, $supplier)->get() as $sup) {
//                    if (PurchaseHeader::where('ACC6InternalID', $sup->InternalID)->where('CompanyInternalID', Auth::user()->CompanyInternalID)->count() > 0) {
//                        $sheet->cells('B' . $row . ':C' . $row, function($cells) {
////                            $cells->setBackground('#eaf6f7');
//                            $cells->setFontSize('14');
//                            $cells->setValignment('middle');
//                            $cells->setFontWeight('bold');
//                        });
//                        $sheet->mergeCells('B' . $row . ':C' . $row);
//                        $sheet->setCellValueByColumnAndRow(1, $row, 'Supplier : ' . $sup->ACC6Name);
//                        $row++;
                $sheet->setCellValueByColumnAndRow(1, $row, ' TGL SJ/KRM');
                $sheet->setCellValueByColumnAndRow(2, $row, 'NO.SJ');
                $sheet->setCellValueByColumnAndRow(3, $row, 'GB');
                $sheet->setCellValueByColumnAndRow(4, $row, 'SUPPLIER');
                $sheet->setCellValueByColumnAndRow(5, $row, 'NAMA BARANG');
                $sheet->setCellValueByColumnAndRow(6, $row, 'UNIT');
                $sheet->setCellValueByColumnAndRow(7, $row, 'TTD PENERIMA');
                $sheet->setCellValueByColumnAndRow(8, $row, 'KET');

//                $sheet->setCellValueByColumnAndRow(6, $row, 'Harga @');
//                $sheet->setCellValueByColumnAndRow(7, $row, 'Harga Total');
//                $sheet->setCellValueByColumnAndRow(8, $row, 'PPN 10%');
//                $sheet->setCellValueByColumnAndRow(9, $row, 'Sub Total');
//                $sheet->setCellValueByColumnAndRow(10, $row, 'Tgl Bayar & Nominal');
//                $sheet->setCellValueByColumnAndRow(11, $row, 'Keterangan');
//                $sheet->setBorder('B' . $row . ':L' . $row, 'thin');
//                $sheet->cells('B' . $row . ':L' . $row, function($cells) {
//                    $cells->setBackground('#eaf6f7');
//                    $cells->setValignment('middle');
//                    $cells->setAlignment('center');
//                });
                $row++;

                foreach (MrvHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)->whereBetween('MrvDate', Array($start, $end))->orderBy('MrvDate', 'asc')->get() as $si) {
                    $rowawal = $row;
                    $sheet->setCellValueByColumnAndRow(1, $row, date("d/m/Y", strtotime($si->MrvDate)));
                    $sheet->setCellValueByColumnAndRow(2, $row, $si->MrvID);
                    $sheet->setCellValueByColumnAndRow(3, $row, Warehouse::find($si->WarehouseInternalID)->WarehouseName);
                    $sheet->setCellValueByColumnAndRow(4, $row, Coa6::find($si->ACC6InternalID)->ACC6Name);
//                    $tampung_row = $row;
//                    $totalbarisout = TransformationDetail::where('TransformationInternalID', $si->InternalID)->where("Type", "out")->count();
//                    $totalbarisin = TransformationDetail::where('TransformationInternalID', $si->InternalID)->where("Type", "in")->count();
//                    if ($totalbarisin > $totalbarisout)
//                        $totalbaris = $totalbarisin;
//                    else
//                        $totalbaris = $totalbarisout;
////                    dd('B' . $rowawal . ':B' . ($rowawal + $totalbaris - 1));
//                    $sheet->mergeCells('B' . $rowawal . ':B' . ($rowawal + $totalbaris - 1));
//                    $sheet->mergeCells('C' . $rowawal . ':C' . ($rowawal + $totalbaris - 1));
//                    $sheet->mergeCells('D' . $rowawal . ':D' . ($rowawal + $totalbaris - 1));
                    foreach (MrvDetail::where('MrvInternalID', $si->InternalID)->get() as $sd) {
//                        $sheet->setBorder('B' . $row . ':L' . $row, 'thin');
                        $sheet->setCellValueByColumnAndRow(5, $row, Inventory::find($sd->InventoryInternalID)->InventoryName);
                        $sheet->setCellValueByColumnAndRow(6, $row, $sd->Qty);
                        $row++;
                    }
//                    $row = $tampung_row;
//                    if (SalesAddDetail::where('SalesInternalID', $si->InternalID)->where('SalesParcelInternalID', 0)->count() > 0) {
//                        $sheet->setBorder('B' . $row . ':L' . $row, 'thin');
//                        $sheet->setCellValueByColumnAndRow(7, $row, number_format($total, 0, ".", ","));
//                        $sheet->setCellValueByColumnAndRow(8, $row, number_format($total / 10, 2, ".", ","));
//                        $sheet->setCellValueByColumnAndRow(9, $row, number_format($total * 1.1, 2, ".", ","));
//                        $row++;
//                        $sheet->setBorder('B' . $row . ':L' . $row, 'thin');
//                        $row++;
//                    }
                    $row++;
                }
                $row++;
//                    }
//                }
                $row--;
                $row--;
                $sheet->setBorder('B4:I' . $row, 'thin');
                $sheet->cells('B2:C2', function($cells) {
                    $cells->setFontWeight('bold');
                });
                $sheet->cells('B4:I4', function($cells) {
                    $cells->setFontWeight('bold');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('left');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
//                $sheet->cells('B' . $rowawal . ':G' . $row, function($cells) {
//                    $cells->setAlignment('center');
//                    $cells->setValignment('top');
//                });
//                $sheet->cells('B3:B' . $row, function($cells) {
//                    $cells->setAlignment('center');
//                });
            });
        })->export('xls');
    }

    public function reportInventoryOut() {
        Excel::create('LaporanGudang_BarangKeluar', function($excel) {
            $excel->sheet('LaporanGudang_BarangKeluar', function($sheet) {
                $startT = explode('-', Input::get('sDate'));
                $endT = explode('-', Input::get('eDate'));
                $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
                $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
                $sheet->mergeCells('B1:C1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Laporan Barang Keluars Gudang");
                $sheet->setCellValueByColumnAndRow(1, 2, "Periode :");
                $sheet->setCellValueByColumnAndRow(2, 2, date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))));
                $row = 4;
                $rowawal = 4;
                $number = 1;

                $sheet->setCellValueByColumnAndRow(1, $row, ' TGL SJ/KRM');
                $sheet->setCellValueByColumnAndRow(2, $row, 'NO.SJ');
                $sheet->setCellValueByColumnAndRow(3, $row, 'GB');
                $sheet->setCellValueByColumnAndRow(4, $row, 'CUSTOMER');
                $sheet->setCellValueByColumnAndRow(5, $row, 'NAMA BARANG');
                $sheet->setCellValueByColumnAndRow(6, $row, 'UNIT');
                $sheet->setCellValueByColumnAndRow(7, $row, 'TTD PENERIMA');
                $sheet->setCellValueByColumnAndRow(8, $row, 'KET');

                $row++;

                foreach (ShippingAddHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)->whereBetween('ShippingDate', Array($start, $end))->orderBy('ShippingDate', 'asc')->get() as $si) {
                    $rowawal = $row;
                    $sheet->setCellValueByColumnAndRow(1, $row, date("d/m/Y", strtotime($si->ShippingDate)));
                    $sheet->setCellValueByColumnAndRow(2, $row, $si->ShippingID);
                    $sheet->setCellValueByColumnAndRow(3, $row, Warehouse::find($si->WarehouseInternalID)->WarehouseName);
                    $sheet->setCellValueByColumnAndRow(4, $row, Coa6::find($si->ACC6InternalID)->ACC6Name);
                    foreach (ShippingAddDetail::where('ShippingInternalID', $si->InternalID)->get() as $sd) {
                        $sheet->setCellValueByColumnAndRow(5, $row, Inventory::find($sd->InventoryInternalID)->InventoryName);
                        $sheet->setCellValueByColumnAndRow(6, $row, $sd->Qty);
                        $row++;
                    }
                    $row++;
                }
                $row++;

                $row--;
                $row--;
                $sheet->setBorder('B4:I' . $row, 'thin');
                $sheet->cells('B2:C2', function($cells) {
                    $cells->setFontWeight('bold');
                });
                $sheet->cells('B4:I4', function($cells) {
                    $cells->setFontWeight('bold');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('left');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
//                $sheet->cells('B' . $rowawal . ':G' . $row, function($cells) {
//                    $cells->setAlignment('center');
//                    $cells->setValignment('top');
//                });
//                $sheet->cells('B3:B' . $row, function($cells) {
//                    $cells->setAlignment('center');
//                });
            });
        })->export('xls');
    }

    //==================ajax==========================
}
