<?php

class TransformationController extends BaseController {

    public function showTransformation() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteTransformation') {
                return $this->deleteTransformation();
            } else if (Input::get('jenis') == 'detailReport') {
                return $this->detailReport();
            }
        }
        return View::make('transformation.transformation')
                        ->withToogle('transaction')->withAktif('transformation');
    }

    public function transformationNew() {
        $so = Input::get('so');
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteTransformation') {
                return $this->deleteTransformation();
            } else {
                return $this->insertTransformation();
            }
        }
        if (Input::get('jenis') == 'insertTransformation') {
            if ($so == null) {
                return $this->insertTransformationOrder();
            } else if (Input::get('jenis') == 'detailReport') {
                return $this->detailReport();
            } else {
                return $this->insertTransformationOrder($so);
            }
        }
        $transformationin = $this->createID(0) . '.';
        return View::make('transformation.transformationNew')
                        ->withToogle('transaction')->withAktif('transformation')
                        ->withtransformation($transformationin);
    }

    //insert transformation order, transformation dari sales order
    public function insertTransformationOrder($salesorder = null) {
        $data = Input::all();
        $rule = array(
            "SalesOrder" => "required"
        );
        $validator = Validator::make($data, $rule);
        if ($validator->fails() && $salesorder == null) {
            return View::make('transformation.transformationNew')
                            ->withToogle('transaction')->withAktif('transformation');
        } else {
            if ($salesorder != null)
                $so = SalesOrderHeader::where("SalesOrderID", $salesorder)->first();
            else
                $so = SalesOrderHeader::where("SalesOrderID", Input::get('SalesOrder'))->first();
            $transformation = $this->createID(0) . '.';
            return View::make('transformation.transformationNew')
                            ->withToogle('transaction')->withAktif('transformation')
                            ->withSalesorder($so)
                            ->withTransformation($transformation);
        }
    }

    public function transformationDetail($id) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteTransformation') {
                return $this->deleteTransformation();
            } else if (Input::get('jenis') == 'detailReport') {
                return $this->detailReport();
            }
        }
        $idTransformation = TransformationHeader::getIdTransformation($id);
        $headerTransformation = TransformationHeader::find($idTransformation);
        $detailTransformationIn = TransformationDetail::where("TransformationInternalID", $idTransformation)->where('Type', 'in')->get();
        $detailTransformationOut = TransformationDetail::where("TransformationInternalID", $idTransformation)->where('Type', 'out')->get();

//        $idMemoOut = MemoOutHeader::getIdtransformationMemoOut($id);
//        $headerMemoOut = MemoOutHeader::find($idMemoOut);
//        $detailMemoOut = MemoOutHeader::find($idMemoOut)->memoOutDetail()->get();

        if ($headerTransformation->CompanyInternalID == Auth::user()->Company->InternalID) {
            return View::make('transformation.transformationDetail')
                            ->withToogle('transaction')->withAktif('transformation')
                            ->withHeadermemoin($headerTransformation)
                            ->withHeadermemoout($headerTransformation)
                            ->withDetailmemoin($detailTransformationIn)
                            ->withDetailmemoout($detailTransformationOut);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showTransformation');
        }
    }

    public function transformationUpdate($id) {
        $idTransformation = TransformationHeader::getIdTransformation($id);
        $headerTransformation = TransformationHeader::find($idTransformation);
        $detailTransformationIn = TransformationDetail::where("TransformationInternalID", $idTransformation)->where('Type', 'in')->get();
        $detailTransformationOut = TransformationDetail::where("TransformationInternalID", $idTransformation)->where('Type', 'out')->get();

//        $idMemoOut = MemoOutHeader::getIdtransformationMemoOut($id);
//        $headerMemoOut = MemoOutHeader::find($idMemoOut);
//        $detailMemoOut = MemoOutHeader::find($idMemoOut)->memoOutDetail()->get();

        if ($headerTransformation->CompanyInternalID == Auth::user()->Company->InternalID) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                if (Input::get('jenis') == 'deleteTransformation') {
                    return $this->deleteTransformation();
                } else if (Input::get('jenis') == 'detailReport') {
                    return $this->detailReport();
                } else {
                    return $this->updateTransformation($id);
                }
            }
            $transformation = $this->createID(0) . '.';
            return View::make('transformation.transformationUpdate')
                            ->withToogle('transaction')->withAktif('transformation')
                            ->withHeadermemoin($headerTransformation)
                            ->withHeadermemoout($headerTransformation)
                            ->withDetailmemoin($detailTransformationIn)
                            ->withDetailmemoout($detailTransformationOut)
                            ->withtransformation($transformation);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showTransformation');
        }
    }

    public function insertTransformation() {
//        dd(Input::get('SalesOrderInternalID'));
        //rule
        $rule = array(
            'date' => 'required',
//            'remark' => 'required|max:1000',
            'currency' => 'required',
//            'warehouse' => 'required',
            'rate' => 'required',
//            'uom' => 'required',
//            'inventory' => 'required'
        );
        $transformationNumber = '';
        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalInsert';
            $error = $validator->messages();
        } else {
            //valid
            $countTransformationIn = 0;
            $countTransformationOut = 0;
            //insert header
            //create memo in
//            $headerMemoIn = new MemoInHeader;
            $transformation = $this->createID(1) . '.';
//            $memoIn = $this->createIDMemoIn(1) . '.';
            $date = explode('-', Input::get('date'));
            $yearDigit = substr($date[2], 2);
            $transformation .= $date[1] . $yearDigit . '.';
            $transformationNumber = TransformationHeader::getNextIDTransformation($transformation);
//            $transformationNumber = MemoInHeader::getNextIDTransformation($transformation);
//            $memoInNumber = MemoInHeader::getNextIDMemoIn($memoIn);
//            $headerMemoIn->MemoInID = $memoInNumber;
//            $headerMemoIn->MemoInDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $currency = explode('---;---', Input::get('currency'));
//            $headerMemoIn->WarehouseInternalID = Input::get('warehouse');
//            $headerMemoIn->CurrencyInternalID = $currency[0];
//            $headerMemoIn->CurrencyRate = str_replace(',', '', Input::get('rate'));
//            $headerMemoIn->Type = 1;
//            $headerMemoIn->TransformationID = $transformationNumber;
//            $headerMemoIn->GrandTotal = Input::get('grandTotalValueIn');
//            $headerMemoIn->UserRecord = Auth::user()->UserID;
//            $headerMemoIn->CompanyInternalID = Auth::user()->Company->InternalID;
//            $headerMemoIn->UserModified = '0';
//            $headerMemoIn->Remark = Input::get('remark');
//            $headerMemoIn->save();
            //create transformation in
            $tin = new TransformationHeader();
            $tin->TransformationID = $transformationNumber;
//            $tin->WarehouseInternalID = Input::get('warehouse');
            if (Input::get('SalesOrderInternalID') != null && Input::get('SalesOrderInternalID') != '')
                $tin->SalesOrderInternalID = Input::get('SalesOrderInternalID');
            $tin->TransformationDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $tin->CurrencyInternalID = $currency[0];
            $tin->CurrencyRate = str_replace(',', '', Input::get('rate'));
//            $tin->Type = "in";
            $tin->GrandTotal = Input::get('grandTotalValueIn');
            $tin->UserRecord = Auth::user()->UserID;
            $tin->CompanyInternalID = Auth::user()->Company->InternalID;
            $tin->UserModified = '0';
            $tin->Remark = Input::get('remark');
            $tin->save();

            //create memo out
//            $headerMemoOut = new MemoOutHeader;
//            $transformation = $this->createID(1) . '.';
//            $date = explode('-', Input::get('date'));
//            $yearDigit = substr($date[2], 2);
//            $transformation .= $date[1] . $yearDigit . '.';
//            $memoOut = $this->createIDMemoOut(1) . '.';
//            $memoOutNumber = MemoOutHeader::getNextIDMemoOut($memoOut);
//            $headerMemoOut->MemoOutID = $memoOutNumber;
//            $headerMemoOut->MemoOutDate = $date[2] . '-' . $date[1] . '-' . $date[0];
//            $currency = explode('---;---', Input::get('currency'));
//            $headerMemoOut->WarehouseInternalID = Input::get('warehouse');
//            $headerMemoOut->CurrencyInternalID = $currency[0];
//            $headerMemoOut->CurrencyRate = str_replace(',', '', Input::get('rate'));
//            $headerMemoOut->Type = 1;
//            $headerMemoOut->TransformationID = $transformationNumber;
//            $headerMemoOut->GrandTotal = Input::get('grandTotalValueOut');
//            $headerMemoOut->UserRecord = Auth::user()->UserID;
//            $headerMemoOut->CompanyInternalID = Auth::user()->Company->InternalID;
//            $headerMemoOut->UserModified = '0';
//            $headerMemoOut->Remark = Input::get('remark');
//            $headerMemoOut->save();
//            $transformation2 = $this->createID(1) . '.';
////            $memoIn = $this->createIDMemoIn(1) . '.';
//            $transformation2 .= $date[1] . $yearDigit . '.';
//            $transformationNumber2 = TransformationHeader::getNextIDTransformation($transformation2);
//            //create transformation in
//            $tout = new TransformationHeader();
//            $tout->TransformationID = $transformationNumber2;
////            $tout->WarehouseInternalID = Input::get('warehouse');
//            if (Input::get('SalesOrderInternalID') != null && Input::get('SalesOrderInternalID') != '')
//                $tout->SalesOrderInternalID = Input::get('SalesOrderInternalID');
//            $tout->TransformationDate = $date[2] . '-' . $date[1] . '-' . $date[0];
//            $tout->CurrencyInternalID = $currency[0];
//            $tout->CurrencyRate = str_replace(',', '', Input::get('rate'));
////            $tout->Type = "out";
//            $tout->GrandTotal = Input::get('grandTotalValueOut');
//            $tout->UserRecord = Auth::user()->UserID;
//            $tout->CompanyInternalID = Auth::user()->Company->InternalID;
//            $tout->UserModified = '0';
//            $tout->Remark = Input::get('remark');
//            $tout->save();


            $totalTransformationIn = 0;
            $totalTransformationOut = 0;
            //insert detail
            for ($a = 0; $a < count(Input::get('inventory')); $a++) {
                if (Input::get('Type')[$a] == "OUT") {
                    $qtyValue = str_replace(',', '', Input::get('qty')[$a]);
                    $priceValue = str_replace(',', '', Input::get('price')[$a]);
                    $subTotal = ($priceValue * $qtyValue);
                    if ($qtyValue > 0) {
                        $detail = new TransformationDetail();
                        $detail->TransformationInternalID = $tin->InternalID;
                        $detail->InventoryInternalID = Input::get('inventory')[$a];
                        $detail->WarehouseInternalID = Input::get('warehouse')[$a];
                        $detail->UomInternalID = Input::get('uom')[$a];
                        $detail->Qty = $qtyValue;
                        $detail->Price = $priceValue;
                        $detail->SubTotal = $subTotal;
                        $detail->Type = "out";
                        $detail->UserRecord = Auth::user()->UserID;
                        $detail->UserModified = '0';
                        $detail->save();
                        setTampInventory(Input::get('inventory')[$a]);
                        $countTransformationOut++;
                    }
                    $totalTransformationOut += $subTotal;
                } else {
                    $qtyValue = str_replace(',', '', Input::get('qty')[$a]);
                    $priceValue = str_replace(',', '', Input::get('price')[$a]);
                    $subTotal = ($priceValue * $qtyValue);
                    if ($qtyValue > 0) {
                        $detail = new TransformationDetail();
                        $detail->TransformationInternalID = $tin->InternalID;
                        $detail->InventoryInternalID = Input::get('inventory')[$a];
                        $detail->WarehouseInternalID = Input::get('warehouse')[$a];
                        $detail->UomInternalID = Input::get('uom')[$a];
                        $detail->Qty = $qtyValue;
                        $detail->Price = $priceValue;
                        $detail->SubTotal = $subTotal;
                        $detail->Type = "in";
                        $detail->UserRecord = Auth::user()->UserID;
                        $detail->UserModified = '0';
                        $detail->save();
                        setTampInventory(Input::get('inventory')[$a]);
                        $countTransformationIn++;
                    }
                    $totalTransformationIn += $subTotal;
                }
            }

            for ($a = 0; $a < count(Input::get('inventory2')); $a++) {
                if (Input::get('Type2')[$a] == "OUT") {
                    $qtyValue = str_replace(',', '', Input::get('qty2')[$a]);
                    $priceValue = str_replace(',', '', Input::get('price2')[$a]);
                    $subTotal = ($priceValue * $qtyValue);
                    if ($qtyValue > 0) {
                        $detail = new TransformationDetail();
                        $detail->TransformationInternalID = $tin->InternalID;
                        $detail->InventoryInternalID = Input::get('inventory2')[$a];
                        $detail->WarehouseInternalID = Input::get('warehouse2')[$a];
                        $detail->UomInternalID = Input::get('uom2')[$a];
                        $detail->Qty = $qtyValue;
                        $detail->Price = $priceValue;
                        $detail->SubTotal = $subTotal;
                        $detail->Type = "out";
                        $detail->UserRecord = Auth::user()->UserID;
                        $detail->UserModified = '0';
                        $detail->save();
                        setTampInventory(Input::get('inventory2')[$a]);
                        $countTransformationOut++;
                    }
                    $totalTransformationOut += $subTotal;
                } else {
                    $qtyValue = str_replace(',', '', Input::get('qty2')[$a]);
                    $priceValue = str_replace(',', '', Input::get('price2')[$a]);
                    $subTotal = ($priceValue * $qtyValue);
                    if ($qtyValue > 0) {
                        $detail = new TransformationDetail();
                        $detail->TransformationInternalID = $tin->InternalID;
                        $detail->InventoryInternalID = Input::get('inventory2')[$a];
                        $detail->WarehouseInternalID = Input::get('warehouse2')[$a];
                        $detail->UomInternalID = Input::get('uom2')[$a];
                        $detail->Qty = $qtyValue;
                        $detail->Price = $priceValue;
                        $detail->SubTotal = $subTotal;
                        $detail->Type = "in";
                        $detail->UserRecord = Auth::user()->UserID;
                        $detail->UserModified = '0';
                        $detail->save();
                        setTampInventory(Input::get('inventory2')[$a]);
                        $countTransformationIn++;
                    }
                    $totalTransformationIn += $subTotal;
                }
            }

            //cek headernya dipakai atau tidak
            if ($countTransformationIn == 0) {
                TransformationHeader::find($tin->InternalID)->delete();
            } else {
                $tin->GrandTotal = $totalTransformationIn;
                $tin->save();
//                $dataType = TransformationDetail::getTipeInventoryData($tin->InternalID);
                $dataTypeIn = TransformationDetail::getTipeInventoryDataIn($tin->InternalID);
                $dataTypeOut = TransformationDetail::getTipeInventoryDataOut($tin->InternalID);
                $slip = NULL;
                $this->insertJournal($transformationNumber, $totalTransformationIn, $currency[0], str_replace(',', '', Input::get('rate')), $date, $slip, $dataTypeIn, $dataTypeOut);
            }
//            if ($countTransformationOut == 0) {
//                TransformationHeader::find($tout->InternalID)->delete();
//            } else {
//                $tout->GrandTotal = $totalTransformationOut;
//                $tout->save();
//            }
            $messages = 'suksesInsert';
            $error = '';
        }
        
        if ($messages == 'suksesInsert') {
            return Redirect::route('transformationDetail', $tin->TransformationID);
        }
        
        $transformation = $this->createID(0) . '.';
        return View::make('transformation.transformationNew')
                        ->withToogle('transaction')->withAktif('transformation')
                        ->withtransformation($transformation)
                        ->withError($error)
                        ->withMessages($messages);
    }

    public function updateTransformation($id) {
        //tipe
        $idTransformation = TransformationHeader::getIdTransformation($id);
        $headerTransformation = TransformationHeader::find($idTransformation);

//        $idMemoOut = MemoOutHeader::getIdtransformationMemoOut($id);
//        $headerMemoOut = MemoOutHeader::find($idMemoOut);
        //rule
        $rule = array(
//            'remark' => 'required|max:1000',
            'currency' => 'required',
//            'warehouse' => 'required',
            'rate' => 'required',
//            'uom' => 'required',
//            'inventory' => 'required'
        );
        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalUpdate';
            $error = $validator->messages();
        } else {
            $currency = explode('---;---', Input::get('currency'));
//            $headerTransformation->WarehouseInternalID = Input::get('warehouse');
            $headerTransformation->CurrencyInternalID = $currency[0];
            $headerTransformation->CurrencyRate = str_replace(',', '', Input::get('rate'));
            $headerTransformation->GrandTotal = Input::get('grandTotalValueIn');
            $headerTransformation->UserRecord = Auth::user()->UserID;
            $headerTransformation->UserModified = '0';
            $headerTransformation->Remark = Input::get('remark');
            $headerTransformation->save();


//            $currency = explode('---;---', Input::get('currency'));
//            $headerMemoOut->WarehouseInternalID = Input::get('warehouse');
//            $headerMemoOut->CurrencyInternalID = $currency[0];
//            $headerMemoOut->CurrencyRate = str_replace(',', '', Input::get('rate'));
//            $headerMemoOut->GrandTotal = Input::get('grandTotalValueOut');
//            $headerMemoOut->UserRecord = Auth::user()->UserID;
//            $headerMemoOut->UserModified = '0';
//            $headerMemoOut->Remark = Input::get('remark');
//            $headerMemoOut->save();
            //delete memo in memo out detail -- nantinya insert ulang
            TransformationDetail::where('TransformationInternalID', '=', $idTransformation)->update(array('is_deleted' => 1));
//            MemoOutDetail::where('MemoOutInternalID', '=', $idMemoOut)->update(array('is_deleted' => 1));

            $totalTransformation = 0;
            $totalMemoOut = 0;
            //insert detail
            for ($a = 0; $a < count(Input::get('inventory')); $a++) {
                if (Input::get('Type')[$a] == "OUT") {
                    $qtyValue = str_replace(',', '', Input::get('qty')[$a]);
                    $priceValue = str_replace(',', '', Input::get('price')[$a]);
                    $subTotal = ($priceValue * $qtyValue);
                    if ($qtyValue > 0) {
                        $detail = new TransformationDetail();
                        $detail->TransformationInternalID = $headerTransformation->InternalID;
                        $detail->InventoryInternalID = Input::get('inventory')[$a];
                        $detail->WarehouseInternalID = Input::get('warehouse')[$a];
                        $detail->UomInternalID = Input::get('uom')[$a];
                        $detail->Qty = $qtyValue;
                        $detail->Price = $priceValue;
                        $detail->SubTotal = $subTotal;
                        $detail->Type = "out";
                        $detail->UserRecord = Auth::user()->UserID;
                        $detail->UserModified = '0';
                        $detail->save();
                        setTampInventory(Input::get('inventory')[$a]);
                    }
                    $totalMemoOut += $subTotal;
                } else {
                    $qtyValue = str_replace(',', '', Input::get('qty')[$a]);
                    $priceValue = str_replace(',', '', Input::get('price')[$a]);
                    $subTotal = ($priceValue * $qtyValue);
                    if ($qtyValue > 0) {
                        $detail = new TransformationDetail();
                        $detail->TransformationInternalID = $headerTransformation->InternalID;
                        $detail->InventoryInternalID = Input::get('inventory')[$a];
                        $detail->WarehouseInternalID = Input::get('warehouse')[$a];
                        $detail->UomInternalID = Input::get('uom')[$a];
                        $detail->Qty = $qtyValue;
                        $detail->Price = $priceValue;
                        $detail->SubTotal = $subTotal;
                        $detail->Type = "in";
                        $detail->UserRecord = Auth::user()->UserID;
                        $detail->UserModified = '0';
                        $detail->save();
                        setTampInventory(Input::get('inventory')[$a]);
                    }
                    $totalTransformation += $subTotal;
                }
            }
            for ($a = 0; $a < count(Input::get('inventory2')); $a++) {
                if (Input::get('Type2')[$a] == "OUT") {
                    $qtyValue = str_replace(',', '', Input::get('qty2')[$a]);
                    $priceValue = str_replace(',', '', Input::get('price2')[$a]);
                    $subTotal = ($priceValue * $qtyValue);
                    if ($qtyValue > 0) {
                        $detail = new TransformationDetail();
                        $detail->TransformationInternalID = $headerTransformation->InternalID;
                        $detail->InventoryInternalID = Input::get('inventory2')[$a];
                        $detail->WarehouseInternalID = Input::get('warehouse2')[$a];
                        $detail->UomInternalID = Input::get('uom2')[$a];
                        $detail->Qty = $qtyValue;
                        $detail->Price = $priceValue;
                        $detail->SubTotal = $subTotal;
                        $detail->Type = "out";
                        $detail->UserRecord = Auth::user()->UserID;
                        $detail->UserModified = '0';
                        $detail->save();
                        setTampInventory(Input::get('inventory2')[$a]);
                    }
                    $totalMemoOut += $subTotal;
                } else {
                    $qtyValue = str_replace(',', '', Input::get('qty2')[$a]);
                    $priceValue = str_replace(',', '', Input::get('price2')[$a]);
                    $subTotal = ($priceValue * $qtyValue);
                    if ($qtyValue > 0) {
                        $detail = new TransformationDetail();
                        $detail->TransformationInternalID = $headerTransformation->InternalID;
                        $detail->InventoryInternalID = Input::get('inventory2')[$a];
                        $detail->WarehouseInternalID = Input::get('warehouse2')[$a];
                        $detail->UomInternalID = Input::get('uom2')[$a];
                        $detail->Qty = $qtyValue;
                        $detail->Price = $priceValue;
                        $detail->SubTotal = $subTotal;
                        $detail->Type = "in";
                        $detail->UserRecord = Auth::user()->UserID;
                        $detail->UserModified = '0';
                        $detail->save();
                        setTampInventory(Input::get('inventory2')[$a]);
                    }
                    $totalTransformation += $subTotal;
                }
            }

            $headerTransformation->GrandTotal = $totalTransformation;
            $headerTransformation->save();

//            $headerMemoOut->GrandTotal = $totalMemoOut;
//            $headerMemoOut->save();

            $journal = JournalHeader::where('TransactionID', '=', $headerTransformation->TransformationID)->get();
            foreach ($journal as $value) {
                JournalDetail::where('JournalInternalID', '=', $value->InternalID)->delete();
                JournalHeader::where('InternalID', '=', $value->InternalID)->delete();
            }

            TransformationDetail::where('TransformationInternalID', '=', $idTransformation)->where('is_deleted', 1)->delete();
//            MemoOutDetail::where('MemoOutInternalID', '=', $idMemoOut)->where('is_deleted', 1)->delete();

            $dataTypeIn = TransformationDetail::getTipeInventoryDataIn($headerTransformation->InternalID);
            $dataTypeOut = TransformationDetail::getTipeInventoryDataOut($headerTransformation->InternalID);
            $slip = NULL;
            $this->insertJournal($headerTransformation->TransformationID, $totalTransformation, $currency[0], str_replace(',', '', Input::get('rate')), $headerTransformation->TransformationDate, $slip, $dataTypeIn, $dataTypeOut);

            $messages = 'suksesUpdate';
            $error = '';
        }

        if ($messages == 'suksesUpdate') {
            return Redirect::route('transformationDetail', $headerTransformation->TransformationID);
        }
        
        //tipe
        $idTransformation = TransformationHeader::getIdTransformation($id);
        $headerTransformation = TransformationHeader::find($idTransformation);
//        $detailTransformation = TransformationHeader::find($idTransformation)->memoInDetail()->get();
        $detailTransformationIn = TransformationDetail::where("TransformationInternalID", $idTransformation)->where('Type', 'in')->get();
        $detailTransformationOut = TransformationDetail::where("TransformationInternalID", $idTransformation)->where('Type', 'out')->get();
//        $idMemoOut = MemoOutHeader::getIdtransformationMemoOut($id);
//        $headerMemoOut = MemoOutHeader::find($idMemoOut);
//        $detailMemoOut = MemoOutHeader::find($idMemoOut)->memoOutDetail()->get();

        $transformation = $this->createID(0) . '.';
        return View::make('transformation.transformationUpdate')
                        ->withToogle('transaction')
                        ->withAktif('transformation')
                        ->withHeadermemoin($headerTransformation)
                        ->withHeadermemoout($headerTransformation)
                        ->withDetailmemoin($detailTransformationIn)
                        ->withDetailmemoout($detailTransformationOut)
                        ->withTransformation($transformation)
                        ->withError($error)
                        ->withMessages($messages);
    }

    public function deleteTransformation() {
        $transformation = null;
        if (is_null($transformation)) {
            //tidak ada yang menggunakan data transformation maka data boleh dihapus
            //hapus journal
            $idMemoIn = Input::get('InternalID');
//            $idMemoIn = TransformationHeader::getIdTransformation(Input::get('InternalID'));
            $headerMemoIn = TransformationHeader::find($idMemoIn);
//            $idMemoOut = MemoOutHeader::getIdtransformationMemoOut(Input::get('InternalID'));

            if ($headerMemoIn->CompanyInternalID == Auth::user()->Company->InternalID) {
                //hapus detil
                $detilDataMemoIn = TransformationHeader::find($idMemoIn)->memoInDetail;
                foreach ($detilDataMemoIn as $value) {
                    $detil = TransformationDetail::find($value->InternalID);
                    $detil->delete();
                    setTampInventory($detil->InventoryInternalID);
                }
//                $detilDataMemoOut = MemoOutHeader::find($idMemoOut)->memoOutDetail;
//                foreach ($detilDataMemoOut as $value) {
//                    $detil = MemoOutDetail::find($value->InternalID);
//                    $detil->delete();
//                    setTampInventory($detil->InventoryInternalID);
//                }
                $journal = JournalHeader::where('TransactionID', '=', $headerMemoIn->TransformationID)->get();
                foreach ($journal as $value) {
                    JournalDetail::where('JournalInternalID', '=', $value->InternalID)->delete();
                    JournalHeader::where('InternalID', '=', $value->InternalID)->delete();
                }
                //hapus transformation
                TransformationHeader::find($idMemoIn)->delete();
//                MemoOutHeader::find($idMemoOut)->delete();

                $messages = 'suksesDelete';
            } else {
                $messages = 'accessDenied';
            }
        } else {
            //ada maka data tidak boleh dihapus
            $messages = 'gagalDelete';
        }
        return View::make('transformation.transformation')
                        ->withToogle('transaction')->withAktif('transformation')
                        ->withMessages($messages);
    }

    function insertJournal($memoNumber, $total, $currency, $rate, $date, $slip, $dataType, $dataType2) {
        //data type = in
        //data type 2 = out
        $header = new JournalHeader;
        $yearDigit = substr($date[2], 2);
        $dateText = $date[1] . $yearDigit;
//        $defaultPembelian = Default_s::find(4)->DefaultID;
//        $defaultHutang = Default_s::find(5)->DefaultID;
//        $defaultOutcome = Default_s::find(6)->DefaultID;
//        $defaultDiscount = Default_s::find(10)->DefaultID;
//        $defaultDP = Default_s::find(12)->DefaultID;
        $defaultHPP = Default_s::find(9)->DefaultID;

        $cari = 'ME-' . $dateText;
        $header->JournalType = 'Memorial';
        $header->JournalID = JournalHeader::getNextIDJournal($cari . '-');
        $header->SlipInternalID = Null;
        $header->JournalDate = $date[2] . '-' . $date[1] . '-' . $date[0];
        $header->JournalFrom = Auth::user()->UserID;
        $header->Notes = '';
        $department = Department::where('Default', '1')->first();
        $header->DepartmentInternalID = $department->InternalID;
        $header->TransactionID = $memoNumber;
        $header->ACC5InternalID = '0';
        $header->Lock = '0';
        $header->Check = '0';
        $header->Flag = '0';
        $header->UserRecord = Auth::user()->UserID;
        $header->CompanyInternalID = Auth::user()->Company->InternalID;
        $header->UserModified = '0';
        $header->Remark = '';
        $header->save();

        //insert detail
        $count = 1;
        $tampTotal = 0;
        foreach ($dataType as $data2) {
            $tipe = InventoryType::find($data2->InternalID);
            $detail = new JournalDetail();
            $detail->JournalInternalID = $header->InternalID;
            $detail->JournalIndex = $count;
            $detail->JournalNotes = $tipe->InventoryTypeName;
            $detail->JournalDebetMU = $data2->total;
            $detail->JournalCreditMU = 0;
            $detail->CurrencyInternalID = $currency;
            $detail->CurrencyRate = $rate;
            $detail->JournalDebet = $data2->total * $rate;
            $detail->JournalCredit = 0;
            $detail->JournalTransactionID = NULL;
            $detail->ACC1InternalID = $tipe->ACC1InternalID;
            $detail->ACC2InternalID = $tipe->ACC2InternalID;
            $detail->ACC3InternalID = $tipe->ACC3InternalID;
            $detail->ACC4InternalID = $tipe->ACC4InternalID;
            $detail->ACC5InternalID = $tipe->ACC5InternalID;
            $detail->ACC6InternalID = $tipe->ACC6InternalID;
            $coa = Coa::getInternalID($tipe->ACC1InternalID, $tipe->ACC2InternalID, $tipe->ACC3InternalID, $tipe->ACC4InternalID, $tipe->ACC5InternalID, $tipe->ACC6InternalID);
            $detail->COAName = Coa::find($coa)->COAName;
            $detail->UserRecord = Auth::user()->UserID;
            $detail->UserModified = '0';
            $detail->save();
            $count++;
        }
        foreach ($dataType2 as $data3) {
            $tipe = InventoryType::find($data3->InternalID);
            $detail = new JournalDetail();
            $detail->JournalInternalID = $header->InternalID;
            $detail->JournalIndex = $count;
            $detail->JournalNotes = $tipe->InventoryTypeName;
            $detail->JournalDebetMU = 0;
            $detail->JournalCreditMU = $data3->total;
            $detail->CurrencyInternalID = $currency;
            $detail->CurrencyRate = $rate;
            $detail->JournalDebet = 0;
            $detail->JournalCredit = $data3->total * $rate;
            $detail->JournalTransactionID = NULL;
            $detail->ACC1InternalID = $tipe->ACC1InternalID;
            $detail->ACC2InternalID = $tipe->ACC2InternalID;
            $detail->ACC3InternalID = $tipe->ACC3InternalID;
            $detail->ACC4InternalID = $tipe->ACC4InternalID;
            $detail->ACC5InternalID = $tipe->ACC5InternalID;
            $detail->ACC6InternalID = $tipe->ACC6InternalID;
            $coa = Coa::getInternalID($tipe->ACC1InternalID, $tipe->ACC2InternalID, $tipe->ACC3InternalID, $tipe->ACC4InternalID, $tipe->ACC5InternalID, $tipe->ACC6InternalID);
            $detail->COAName = Coa::find($coa)->COAName;
            $detail->UserRecord = Auth::user()->UserID;
            $detail->UserModified = '0';
            $detail->save();
            $count++;
        }

//        $detail = new JournalDetail();
//        $detail->JournalInternalID = $header->InternalID;
//        $detail->JournalIndex = $count;
//        $detail->JournalNotes = 'Cost of Goods Sold';
//        $detail->JournalDebetMU = 0;
//        $detail->JournalCreditMU = $total;
//        $detail->CurrencyInternalID = $currency;
//        $detail->CurrencyRate = $rate;
//        $detail->JournalDebet = 0;
//        $detail->JournalCredit = $total * $rate;
//        $detail->JournalTransactionID = NULL;
//        $default = Default_s::getInternalCoa('Cost of Goods Sold');
//        $detail->ACC1InternalID = $default->ACC1InternalID;
//        $detail->ACC2InternalID = $default->ACC2InternalID;
//        $detail->ACC3InternalID = $default->ACC3InternalID;
//        $detail->ACC4InternalID = $default->ACC4InternalID;
//        $detail->ACC5InternalID = $default->ACC5InternalID;
//        $detail->ACC6InternalID = $default->ACC6InternalID;
//        $coa = Coa::getInternalID($default->ACC1InternalID, $default->ACC2InternalID, $default->ACC3InternalID, $default->ACC4InternalID, $default->ACC5InternalID, $default->ACC6InternalID);
//        $detail->COAName = Coa::find($coa)->COAName;
//        $detail->UserRecord = Auth::user()->UserID;
//        $detail->UserModified = '0';
//        $detail->save();
    }

    function transformationPrint($id) {
        //tipe
        $idMemoIn = MemoInHeader::getIdtransformationMemoIn($id);
        $headerMemoIn = MemoInHeader::find($idMemoIn);
        $detailMemoIn = MemoInHeader::find($idMemoIn)->memoInDetail()->get();

        $idMemoOut = MemoOutHeader::getIdtransformationMemoOut($id);
        $headerMemoOut = MemoOutHeader::find($idMemoOut);
        $detailMemoOut = MemoOutHeader::find($idMemoOut)->memoOutDetail()->get();

        if ($headerMemoIn->CompanyInternalID == Auth::user()->Company->InternalID) {
            $currency = Currency::find($headerMemoIn->CurrencyInternalID);
            $currencyName = $currency->CurrencyName;
            $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                    .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Transformation</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; text-align: center;">Printed Date : ' . date("d-m-Y H:i:s", strtotime(date("Y-m-d H:i:s"))) . '</span>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 18px; width: 100%;">
                            <table>
                            <tr>
                            <td width="400px">
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Transformation ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $headerMemoIn->TransformationID . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . date("d-M-Y", strtotime($headerMemoIn->MemoInDate)) . '</td>
                                 </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Currency</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $currencyName . '</td>
                                     </tr>
                                </table>
                                </td>
                                <td>
                                <table>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Rate</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . number_format($headerMemoIn->CurrencyRate, '2', '.', ',') . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Warehouse</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $headerMemoIn->Warehouse->WarehouseName . '</td>
                                     </tr>
                                </table></td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Uom</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Qty</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Price</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Type</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            $totalIn = 0;
            $totalOut = 0;
            if (count($detailMemoIn) > 0) {
                foreach ($detailMemoOut as $data) {
                    $inventory = Inventory::find($data->InventoryInternalID);
                    $inv = $inventory->InventoryID . ' ' . $inventory->InventoryName;
                    $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $inv . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->uom->UomID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">OUT</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
                    $totalOut += $data->SubTotal;
                }
                foreach ($detailMemoIn as $data) {
                    $inventory = Inventory::find($data->InventoryInternalID);
                    $inv = $inventory->InventoryID . ' ' . $inventory->InventoryName;
                    $html .= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $inv . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->uom->UomID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">IN</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
                    $totalIn += $data->SubTotal;
                }
            } else {
                $html .= '<tr>
                            <td colspan="6" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;  text-align: center">There is no inventory registered in this transformation.</td>
                        </tr>';
            }
            $html .= '</tbody>
                            </table>
                            <br>

                            <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                                <table>
                                <tr>
                                    <td width="500px" style="vertical-align: text-top;background-color:none;">
                                    <table>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Remark</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $headerMemoIn->Remark . '</td>
                                     </tr>
                                    </table>
                                    </td>
                                    <td width="200px" style="background-color:none;">
                                     <table style="margin-left:30px;">
                                        <tr style="background: none;">
                                           <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Grand Total Out</td>
                                           <td style="padding: 0px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                           <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700; text-align: right;">' . number_format($totalOut, '2', '.', ',') . '</td>
                                        </tr>
                                       </table>
                                        <table style="margin-left:30px;">
                                        <tr style="background: none;">
                                           <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Grand Total In</td>
                                           <td style="padding: 4px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                           <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700; text-align: right;">' . number_format($totalIn, '2', '.', ',') . '</td>
                                        </tr>
                                       </table>
                                    </td>
                                    </tr>
                                </table>
                            </div>

                    </div>
                </body>
            </html>';
            return PDF ::load($html, 'A4', 'portrait')->show();
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showTransformation');
        }
    }

    function createID($tipe) {
        $transformation = 'TAF';
        if ($tipe == 0) {
            $transformation .= '.' . date('m') . date('y');
        }
        return $transformation;
    }

    function createIDMemoIn($tipe) {
        $memoIn = 'ME-IN';
        if ($tipe == 0) {
            $memoIn .= '.' . date('m') . date('y');
        }
        return $memoIn;
    }

    function createIDMemoOut($tipe) {
        $memoOut = 'ME-OUT';
        if ($tipe == 0) {
            $memoOut .= '.' . date('m') . date('y');
        }
        return $memoOut;
    }

    public function formatCariIDTransformation() {
        $date = explode('-', Input::get('date'));
        $id = $this->createID(1);
        $yearDigit = substr($date[2], 2);
        $id .= '.' . $date[1] . $yearDigit . '.';
        echo MemoInHeader::getNextIDTransformation($id);
    }

    //====================ajax==========================
    public function getHPPValueInventoryTransformation() {
        $date = Input::get("date");
        $arrDate = explode("-", $date);
        $inventoryInternalID = Input::get("inventory");

        $month = $arrDate[1] - 1;
        $year = $arrDate[2];
        if ($month == 0) {
            $month = 12;
            $year = $arrDate[2] - 1;
        }
        $hpp = 0;
        $value = InventoryValue::where("InventoryInternalID", $inventoryInternalID)
                ->where("Month", $month)
                ->where("Year", $year)
                ->get();
        if (count($value) == 0) {
            $hpp = Inventory::find($inventoryInternalID)->InitialValue;
        } else {
            $hpp = $value[0]->Value;
        }
        if ($hpp == 0) {
            $hpp = InventoryUom::where('InventoryInternalID', $inventoryInternalID)
                            ->where('Default', 1)->pluck('PurchasePrice');
        }

        return $hpp;
    }

    public function getUomThisInventoryTransformation() {
        $inventoryUom = InventoryUom::where("InventoryInternalID", Input::get("id"))->get();
        foreach ($inventoryUom as $data) {
            ?>
            <option value="<?php echo $data->UomInternalID ?>" <?php echo ($data->Default == 1 ? "selected" : "") ?>><?php echo $data->Uom->UomID; ?></option>
            <?php
        }
    }

    public function transformationPrintGrid($id) {
        $idMemoIn = MemoInHeader::getIdtransformationMemoIn($id);
        $headerMemoIn = MemoInHeader::find($idMemoIn);
        $detailMemoIn = MemoInHeader::find($idMemoIn)->memoInDetail()->get();

        $idMemoOut = MemoOutHeader::getIdtransformationMemoOut($id);
        $headerMemoOut = MemoOutHeader::find($idMemoOut);
        $detailMemoOut = MemoOutHeader::find($idMemoOut)->memoOutDetail()->get();

        if ($headerMemoIn->CompanyInternalID == Auth::user()->Company->InternalID) {
            return View::make('template.print.transformationPrint')
                            ->withHeadermemoin($headerMemoIn)
                            ->withHeadermemoout($headerMemoOut)
                            ->withDetailmemoin($detailMemoIn)
                            ->withDetailmemoout($detailMemoOut);
        } else {
            return "Access Denied";
        }
    }

    public function detailReport() {
        Excel::create('Detail_Report', function($excel) {
            $excel->sheet('Detail_Report', function($sheet) {
//                if (Input::get("hide") == "hide") {
//                    $sheet->getColumnDimension('G')->setVisible(false);
//                }
                $startT = explode('-', Input::get('sDate'));
                $endT = explode('-', Input::get('eDate'));
                $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
                $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
//                $supplier = Input::get('customer');
//                if ($supplier == '-1') {
//                    $tanda = '!=';
//                } else {
//                    $tanda = '=';
//                }
                $sheet->mergeCells('B1:C1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Transformation Detail Report");
//                $sheet->mergeCells('B2:C2');
                $sheet->setCellValueByColumnAndRow(1, 2, "Periode :");
                $sheet->setCellValueByColumnAndRow(2, 2, date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))));
                $row = 4;
                $rowawal = 4;
                $number = 1;

//                foreach (Coa6::where('CompanyInternalID', Auth::user()->CompanyInternalID)->where('Type', 'c')->where('InternalID', $tanda, $supplier)->get() as $sup) {
//                    if (PurchaseHeader::where('ACC6InternalID', $sup->InternalID)->where('CompanyInternalID', Auth::user()->CompanyInternalID)->count() > 0) {
//                        $sheet->cells('B' . $row . ':C' . $row, function($cells) {
////                            $cells->setBackground('#eaf6f7');
//                            $cells->setFontSize('14');
//                            $cells->setValignment('middle');
//                            $cells->setFontWeight('bold');
//                        });
//                        $sheet->mergeCells('B' . $row . ':C' . $row);
//                        $sheet->setCellValueByColumnAndRow(1, $row, 'Supplier : ' . $sup->ACC6Name);
//                        $row++;
                $sheet->setCellValueByColumnAndRow(1, $row, ' No');
                $sheet->setCellValueByColumnAndRow(2, $row, 'Transformation ID');
                $sheet->setCellValueByColumnAndRow(3, $row, 'Transformation Date');
                $sheet->setCellValueByColumnAndRow(4, $row, 'From');
                $sheet->mergeCells('E' . $row . ':F' . $row);
                $sheet->setCellValueByColumnAndRow(6, $row, 'To');
                $sheet->mergeCells('G' . $row . ':H' . $row);
//                $sheet->setCellValueByColumnAndRow(6, $row, 'Harga @');
//                $sheet->setCellValueByColumnAndRow(7, $row, 'Harga Total');
//                $sheet->setCellValueByColumnAndRow(8, $row, 'PPN 10%');
//                $sheet->setCellValueByColumnAndRow(9, $row, 'Sub Total');
//                $sheet->setCellValueByColumnAndRow(10, $row, 'Tgl Bayar & Nominal');
//                $sheet->setCellValueByColumnAndRow(11, $row, 'Keterangan');
//                $sheet->setBorder('B' . $row . ':L' . $row, 'thin');
//                $sheet->cells('B' . $row . ':L' . $row, function($cells) {
//                    $cells->setBackground('#eaf6f7');
//                    $cells->setValignment('middle');
//                    $cells->setAlignment('center');
//                });
                $row++;

                foreach (TransformationHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)->whereBetween('TransformationDate', Array($start, $end))->orderBy('TransformationDate', 'asc')->get() as $si) {
                    $rowawal = $row;
                    $sheet->setCellValueByColumnAndRow(1, $row, $number);
                    $number++;
                    $sheet->setCellValueByColumnAndRow(2, $row, $si->TransformationID);
                    $sheet->setCellValueByColumnAndRow(3, $row, date("d/m/Y", strtotime($si->TransformationDate)));
                    $tampung_row = $row;
                    $totalbarisout = TransformationDetail::where('TransformationInternalID', $si->InternalID)->where("Type", "out")->count();
                    $totalbarisin = TransformationDetail::where('TransformationInternalID', $si->InternalID)->where("Type", "in")->count();
                    if ($totalbarisin > $totalbarisout)
                        $totalbaris = $totalbarisin;
                    else
                        $totalbaris = $totalbarisout;
//                    dd('B' . $rowawal . ':B' . ($rowawal + $totalbaris - 1));
                    $sheet->mergeCells('B' . $rowawal . ':B' . ($rowawal + $totalbaris - 1));
                    $sheet->mergeCells('C' . $rowawal . ':C' . ($rowawal + $totalbaris - 1));
                    $sheet->mergeCells('D' . $rowawal . ':D' . ($rowawal + $totalbaris - 1));
                    foreach (TransformationDetail::where('TransformationInternalID', $si->InternalID)->where("Type", "out")->get() as $sd) {
//                        $sheet->setBorder('B' . $row . ':L' . $row, 'thin');
                        $sheet->setCellValueByColumnAndRow(4, $row, Inventory::find($sd->InventoryInternalID)->InventoryName);
                        $sheet->setCellValueByColumnAndRow(5, $row, $sd->Qty);
                        $row++;
                    }
                    $row = $tampung_row;
                    foreach (TransformationDetail::where('TransformationInternalID', $si->InternalID)->where("Type", "in")->get() as $sd) {
//                        $sheet->setBorder('B' . $row . ':L' . $row, 'thin');
                        $sheet->setCellValueByColumnAndRow(6, $row, Inventory::find($sd->InventoryInternalID)->InventoryName);
                        $sheet->setCellValueByColumnAndRow(7, $row, $sd->Qty);
                        $row++;
                    }
                    $row = $rowawal + $totalbaris;

//                    if (SalesAddDetail::where('SalesInternalID', $si->InternalID)->where('SalesParcelInternalID', 0)->count() > 0) {
//                        $sheet->setBorder('B' . $row . ':L' . $row, 'thin');
//                        $sheet->setCellValueByColumnAndRow(7, $row, number_format($total, 0, ".", ","));
//                        $sheet->setCellValueByColumnAndRow(8, $row, number_format($total / 10, 2, ".", ","));
//                        $sheet->setCellValueByColumnAndRow(9, $row, number_format($total * 1.1, 2, ".", ","));
//                        $row++;
//                        $sheet->setBorder('B' . $row . ':L' . $row, 'thin');
//                        $row++;
//                    }
                }
                $row++;
//                    }
//                }
                $row--;
                $row--;
                $sheet->setBorder('B4:H' . $row, 'thin');
                $sheet->cells('B2:C2', function($cells) {
                    $cells->setFontWeight('bold');
                });
                $sheet->cells('B4:H4', function($cells) {
                    $cells->setFontWeight('bold');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('left');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B' . $rowawal . ':G' . $row, function($cells) {
                    $cells->setAlignment('center');
                    $cells->setValignment('top');
                });
//                $sheet->cells('B3:B' . $row, function($cells) {
//                    $cells->setAlignment('center');
//                });
            });
        })->export('xls');
    }

    public function TransformationDataBackup() {
        $table = 't_transformation_header';
        $primaryKey = 't_transformation_header`.`InternalID';
        $columns = array(
            array('db' => 't_transformation_header`.`InternalID', 'dt' => 0, 'formatter' => function($d, $row) {
                    return $d;
                }),
            array('db' => 'TransformationID', 'dt' => 1),
            array('db' => 'TransformationDate', 'dt' => 2, 'formatter' => function($d, $row) {
                    return date("d-m-Y", strtotime($d));
                }),
            array('db' => 'SalesOrderID', 'dt' => 3, 'formatter' => function($d, $row) {
                    if ($d == null)
                        return '-';
                    else
                        return $d;
                }),
            array('db' => 'CurrencyName', 'dt' => 4),
            array('db' => 't_transformation_header`.`InternalID', 'dt' => 5, 'formatter' => function($d, $row) {
                    return TransformationHeader::find($d)->CurrencyRate;
                }),
//            array('db' => 'GrandTotal', 'dt' => 4, 'formatter' => function($d, $row) {
//                    return number_format($d, '2', '.', ',');
//                }),
            array('db' => 't_transformation_header`.`InternalID', 'dt' => 6, 'formatter' => function( $d, $row ) {
                    $data = TransformationHeader::find($d);
                    $return = "<a href='" . Route('transformationDetail', $data->TransformationID) . "'>
                                        <button id='btn-" . $data->TransformationID . "-detail'
                                                class='btn btn-pure-xs btn-xs btn-detail' title='detail'>
                                            <span class='glyphicon glyphicon-zoom-in'></span>
                                        </button>
                                    </a>";
                    $return .= "<a href='" . Route('transformationUpdate', $data->TransformationID) . "'>
                                        <button id='btn-" . $data->TransformationID . "-update'
                                                class='btn btn-pure-xs btn-xs btn-edit' title='update'>
                                            <span class='glyphicon glyphicon-edit'></span>
                                        </button>
                                    </a>";
                    $return .= "<button data-target='#m_transformationDelete' data-internal='" . $data->InternalID . "'  data-toggle='modal' role='dialog'
                                            data-id='" . $data->TransformationID . "' onclick='deleteAttach(this)' data-name='" . $data->TransformationID . "' class='btn btn-pure-xs btn-xs btn-delete' title='delete'>
                                        <span class='glyphicon glyphicon-trash'></span>
                                    </button>";

                    $return .= '<a target="_blank" title="print" href="' . route('transformationPrintGrid', $data->TransformationID) . '">'
                            . '<button id="btn-' . $data->TransformationID . '-print"'
                            . 'class="btn btn-pure-xs btn-xs btn-print" title="print">'
                            . '<span class="glyphicon glyphicon-print"></span>'
                            . '</button>'
                            . '</a>';

                    return $return;
                },
                'field' => 't_transformation_header.InternalID')
        );
        $sql_details = getConnection();
        require('ssp.class.php');
        $ID_CLIENT_VALUE = Auth::user()->CompanyInternalID;
        $extraCondition = 't_salesorder_header.VAT != ' . Auth::user()->SeeNPPN . ' AND t_transformation_header.CompanyInternalID=' . $ID_CLIENT_VALUE;
        $join = 'INNER JOIN m_currency on m_currency.InternalID=t_transformation_header.CurrencyInternalID ';
        $join .= 'LEFT JOIN t_salesorder_header on t_salesorder_header.InternalID=t_transformation_header.SalesOrderInternalID';

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $extraCondition, $join));
    }

    //====================ajax==========================
}
