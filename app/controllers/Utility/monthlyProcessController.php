<?php

class MonthlyProcessController extends BaseController {

    public function showMonthlyProcess() {
        $yearMinDepreciation = DepreciationHeader::getYearMin();
        $yearMinPurchase = PurchaseHeader::getYearMin();
        $yearMinSales = SalesHeader::getYearMin();
        $yearMinCOGS = ($yearMinPurchase < $yearMinSales ? $yearMinPurchase : $yearMinSales);
        if (($yearMinPurchase == 0 && $yearMinSales != 0) || ($yearMinPurchase != 0 && $yearMinSales == 0)) {
            $yearMinCOGS = $yearMinPurchase;
            if ($yearMinCOGS == 0) {
                $yearMinCOGS = $yearMinSales;
            }
        }
        $yearMinClosingBalance = JournalHeader::getYearMin();
        $yearMax = date("Y");
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'depreciation') {
                return $this->journalDepreciation();
            }
            if (Input::get('jenis') == 'cogs') {
                return $this->journalCogs();
            }
            if (Input::get('jenis') == 'closing') {
                return $this->journalClosing();
            }
        }
        return View::make('utility.monthlyProcess')
                        ->withToogle('utility')->withAktif('monthlyProcess')
                        ->withYearmindepreciation($yearMinDepreciation)
                        ->withYearmincogs($yearMinCOGS)
                        ->withYearminclosingbalance($yearMinClosingBalance)
                        ->withYearmax($yearMax);
    }

    public function journalDepreciation() {
        $month = Input::get('month');
        $year = Input::get('year');

        $tampJournal = JournalHeader::where("TransactionID", "Depreciation")
                ->where("CompanyInternalID", Auth::user()->Company->InternalID)
                ->whereRAW("YEAR(JournalDate)= " . $year . " AND MONTH(JournalDate) =" . $month . "")
                ->first();
        if ($tampJournal != '') {
            $journal = JournalHeader::find($tampJournal->InternalID);
            $tampDetail = $journal->JournalDetail;
            foreach ($tampDetail as $detail) {
                $journalDetail = JournalDetail::find($detail->InternalID);
                $journalDetail->delete();
            }
            $journal->delete();
        }
        $dataDepreciation = DepreciationDetail::journalDepreciation($month, $year);

        if (count($dataDepreciation) <= 0) {
            $message = "gagalDepreciation";
        } else {
            $departemenID = Department::where("CompanyInternalID", Auth::user()->Company->InternalID)->first();

            $CurrencyID = Currency::where("CompanyInternalID", Auth::user()->Company->InternalID)->where("Default", 1)->first();

            $journalHeader = new JournalHeader();
            $cari = 'ME-' . str_pad($month, 2, '0', STR_PAD_LEFT) . substr($year, 2, 2);
            $jurnalID = JournalHeader::getNextIDJournal($cari . '-');

            $journalHeader->JournalID = $jurnalID;
            $journalHeader->JournalDate = date("Y-m-t", strtotime('01-' . str_pad($month, 2, '0', STR_PAD_LEFT) . '-' . $year));
            $journalHeader->JournalType = "Memorial";
            $journalHeader->Flag = 0;
            $journalHeader->JournalFrom = Auth::user()->UserID;
            $journalHeader->Notes = "";
            $journalHeader->DepartmentInternalID = $departemenID->InternalID;
            $journalHeader->SlipInternalID = null;
            $journalHeader->TransactionID = 'Depreciation';
            $journalHeader->ACC5InternalID = 0;
            $journalHeader->Lock = 0;
            $journalHeader->LockUser = null;
            $journalHeader->LockDate = null;
            $journalHeader->Check = 0;
            $journalHeader->CheckUser = null;
            $journalHeader->CheckDate = null;
            $journalHeader->UserRecord = Auth::user()->UserID;
            $journalHeader->UserModified = 0;
            $journalHeader->CompanyInternalID = Auth::user()->Company->InternalID;
            $journalHeader->Remark = "";
            $journalHeader->save();

            foreach ($dataDepreciation as $data) {
                $count = 1;
                $journalDetail = new JournalDetail();
                $journalDetail->JournalInternalID = $journalHeader->InternalID;
                $journalDetail->JournalIndex = $count;
                $journalDetail->JournalNotes = "";
                $journalDetail->JournalDebet = 0;
                $journalDetail->JournalCredit = $data->Value_Detail;
                $journalDetail->CurrencyInternalID = $CurrencyID->InternalID;
                $journalDetail->CurrencyRate = 1;
                $journalDetail->JournalDebetMU = 0;
                $journalDetail->JournalCreditMU = $data->Value_Detail;
                $journalDetail->JournalTransactionID = $data->DepreciationID;
                $journalDetail->ACC1InternalID = $data->ACC1InternalIDDebet;
                $journalDetail->ACC2InternalID = $data->ACC2InternalIDDebet;
                $journalDetail->ACC3InternalID = $data->ACC3InternalIDDebet;
                $journalDetail->ACC4InternalID = $data->ACC4InternalIDDebet;
                $journalDetail->ACC5InternalID = $data->ACC5InternalIDDebet;
                $journalDetail->ACC6InternalID = $data->ACC6InternalIDDebet;
                $journalDetail->COAName = Coa::find(Coa::getInternalID($data->ACC1InternalIDDebet, $data->ACC2InternalIDDebet, $data->ACC3InternalIDDebet, $data->ACC4InternalIDDebet, $data->ACC5InternalIDDebet, $data->ACC6InternalIDDebet))->COAName;
                $journalDetail->UserRecord = Auth::user()->UserID;
                $journalDetail->UserModified = 0;
                $journalDetail->Remark = "";
                $journalDetail->save();

                $count++;
                $journalDetail = new JournalDetail();
                $journalDetail->JournalInternalID = $journalHeader->InternalID;
                $journalDetail->JournalIndex = $count;
                $journalDetail->JournalNotes = "";
                $journalDetail->JournalDebet = $data->Value_Detail;
                $journalDetail->JournalCredit = 0;
                $journalDetail->CurrencyInternalID = $CurrencyID->InternalID;
                $journalDetail->CurrencyRate = 1;
                $journalDetail->JournalDebetMU = $data->Value_Detail;
                $journalDetail->JournalCreditMU = 0;
                $journalDetail->JournalTransactionID = $data->DepreciationID;
                $journalDetail->ACC1InternalID = $data->ACC1InternalIDCredit;
                $journalDetail->ACC2InternalID = $data->ACC2InternalIDCredit;
                $journalDetail->ACC3InternalID = $data->ACC3InternalIDCredit;
                $journalDetail->ACC4InternalID = $data->ACC4InternalIDCredit;
                $journalDetail->ACC5InternalID = $data->ACC5InternalIDCredit;
                $journalDetail->ACC6InternalID = $data->ACC6InternalIDCredit;
                $journalDetail->COAName = Coa::find(Coa::getInternalID($data->ACC1InternalIDCredit, $data->ACC2InternalIDCredit, $data->ACC3InternalIDCredit, $data->ACC4InternalIDCredit, $data->ACC5InternalIDCredit, $data->ACC6InternalIDCredit))->COAName;
                $journalDetail->UserRecord = Auth::user()->UserID;
                $journalDetail->UserModified = 0;
                $journalDetail->Remark = "";
                $journalDetail->save();
            }
            $message = "suksesDepreciation";
        }

        $yearMinDepreciation = DepreciationHeader::getYearMin();
        $yearMinPurchase = PurchaseHeader::getYearMin();
        $yearMinSales = SalesHeader::getYearMin();
        $yearMinCOGS = ($yearMinPurchase < $yearMinSales ? $yearMinPurchase : $yearMinSales);
        if (($yearMinPurchase == 0 && $yearMinSales != 0) || ($yearMinPurchase != 0 && $yearMinSales == 0)) {
            $yearMinCOGS = $yearMinPurchase;
            if ($yearMinCOGS == 0) {
                $yearMinCOGS = $yearMinSales;
            }
        }
        $yearMinClosingBalance = JournalHeader::getYearMin();
        $yearMax = date("Y");

        return View::make('utility.monthlyProcess')
                        ->withToogle('utility')->withAktif('monthlyProcess')
                        ->withYearmindepreciation($yearMinDepreciation)
                        ->withYearmincogs($yearMinCOGS)
                        ->withYearminclosingbalance($yearMinClosingBalance)
                        ->withYearmax($yearMax)
                        ->withMessages($message);
    }

    public function journalCogs() {
        $bulan = Input::get('month');
        $tahun = Input::get('year');
        //hapus dulu data jika sudah ada
        $tampJournal = JournalHeader::where('TransactionID', 'Cost of Goods Sold')
                ->where('CompanyInternalID', Auth::user()->CompanyInternalID)
                ->whereRaw('YEAR(JournalDate) = "' . $tahun . '" AND MONTH(JournalDate) = "' . $bulan . '"')
                ->first();
        if ($tampJournal != '') {
            $journal = JournalHeader::find($tampJournal->InternalID);
            //hapus detil
            $detilData = $journal->journalDetail;
            foreach ($detilData as $value) {
                $detil = JournalDetail::find($value->InternalID);
                $detil->delete();
            }
            //hapus journal
            $journal->delete();
        }
        $inventory = Inventory::join('m_inventorytype', 'm_inventorytype.InternalID', '=', 'm_inventory.InventoryTypeInternalID')
                        ->select('*', DB::raw('m_inventory.InternalID as InventoryInternalID'))
                        ->where('m_inventorytype.Flag', 1)
                        ->where('m_inventorytype.CompanyInternalID', Auth::user()->CompanyInternalID)->get();
        $checkKosong = 0;
        //insert header
        $header = new JournalHeader;
        $cari = 'ME-' . str_pad($bulan, 2, '0', STR_PAD_LEFT) . substr($tahun, 2, 2);
        $header->JournalID = JournalHeader::getNextIDJournal($cari . '-');
        $header->JournalDate = date("Y-m-t", strtotime('01-' . str_pad($bulan, 2, '0', STR_PAD_LEFT) . '-' . $tahun));
        $header->JournalType = 'Memorial';
        $header->JournalFrom = Auth::user()->UserID;
        $header->Notes = '';
        $header->DepartmentInternalID = Department::where('CompanyInternalID', Auth::user()->CompanyInternalID)->first()->InternalID;
        $header->SlipInternalID = NULL;
        $header->TransactionID = 'Cost of Goods Sold';
        $header->ACC5InternalID = 0;
        $header->Lock = '0';
        $header->Check = '0';
        $header->Flag = '0';
        $header->UserRecord = Auth::user()->UserID;
        $header->CompanyInternalID = Auth::user()->Company->InternalID;
        $header->UserModified = '0';
        $header->Remark = '';
        $header->save();
        $a = 1;
        $tampValue = 0;
        foreach ($inventory as $data) {
            $qtySales = SalesHeader::qtyInventory($data->InventoryInternalID, $bulan, $tahun);
            $qtySalesR = SalesReturnHeader::qtyInventory($data->InventoryInternalID, $bulan, $tahun);
            $qtyMemoOut = MemoOutHeader::qtyInventory($data->InventoryInternalID, $bulan, $tahun);
            $qtyPurchase = PurchaseHeader::qtyInventory($data->InventoryInternalID, $bulan, $tahun);
            $qtyPurchaseR = PurchaseReturnHeader::qtyInventory($data->InventoryInternalID, $bulan, $tahun);
            $qtyMemoIn = MemoInHeader::qtyInventory($data->InventoryInternalID, $bulan, $tahun);
            $purchase = PurchaseHeader::valueInventory($data->InventoryInternalID, $bulan, $tahun);
            $purchaseR = PurchaseReturnHeader::valueInventory($data->InventoryInternalID, $bulan, $tahun);
            $memoIn = MemoInHeader::valueInventory($data->InventoryInternalID, $bulan, $tahun);
            $valueBefore = InventoryValue::valueInventoryBefore($data->InventoryInternalID, $bulan, $tahun);
            $qtyBefore = InventoryValue::qtyInventoryBefore($data->InventoryInternalID, $bulan, $tahun);

            $dataInitialQuantity = $data->InitialQuantity;
            $dataInitialValue = $data->InitialValue;
            if (InventoryValue::where('InventoryInternalID', $data->InternalID)
                            ->orderBy("Year", 'desc')
                            ->orderBy("Month", 'desc')
                            ->whereRaw("CONCAT(Year,'-',Month,'-31') < " . $tahun . '-' . $bulan . '-01')
                            ->count() > 0) {
                $dataInitialQuantity = 0;
                $dataInitialValue = 0;
            }
            $qtyDividen = $dataInitialQuantity + $qtyBefore + $qtyPurchase - $qtyPurchaseR + $qtyMemoIn;
            if ($qtyDividen == 0) {
                $average = 0;
            } else {
                $average = (($dataInitialValue * $dataInitialQuantity) + ($valueBefore * $qtyBefore) + $purchase - $purchaseR + $memoIn) / $qtyDividen;
            }
            $value = $average * ($qtySales - $qtySalesR);
            if ($value != 0) {
                $checkKosong = 1;
                //insert detail
                $detail = new JournalDetail();
                $detail->JournalInternalID = $header->InternalID;
                $detail->JournalIndex = $a;
                $detail->JournalNotes = '';
                $detail->JournalDebetMU = 0;
                $detail->JournalCreditMU = $value;
                $detail->CurrencyInternalID = Currency::where('CompanyInternalID', Auth::user()->CompanyInternalID)->where('Default', 1)->first()->InternalID;
                $detail->CurrencyRate = 1;
                $detail->JournalDebet = 0;
                $detail->JournalCredit = $value;
                $detail->JournalTransactionID = $data->InventoryID;
                $detail->ACC1InternalID = $data->ACC1InternalID;
                $detail->ACC2InternalID = $data->ACC2InternalID;
                $detail->ACC3InternalID = $data->ACC3InternalID;
                $detail->ACC4InternalID = $data->ACC4InternalID;
                $detail->ACC5InternalID = $data->ACC5InternalID;
                $detail->ACC6InternalID = $data->ACC6InternalID;
                $detail->COAName = Coa::find(Coa::getInternalID($data->ACC1InternalID, $data->ACC2InternalID, $data->ACC3InternalID, $data->ACC4InternalID, $data->ACC5InternalID, $data->ACC6InternalID))->COAName;
                $detail->UserRecord = Auth::user()->UserID;
                $detail->UserModified = '0';
                $detail->save();

                $tampValue += $value;
                $a++;
            }
            $tampQty = $dataInitialQuantity + $qtyBefore + $qtyPurchase - $qtyPurchaseR - $qtySales + $qtySalesR + $qtyMemoIn - $qtyMemoOut;
            if ($average != 0 || $tampQty != 0) {
                $check = InventoryValue::where('InventoryInternalID', $data->InventoryInternalID)->where('Month', $bulan)->where('Year', $tahun)->count();
                if ($check > 0) {
                    $internalInventoryValue = InventoryValue::where('InventoryInternalID', $data->InventoryInternalID)->where('Month', $bulan)->where('Year', $tahun)->first();
                    $inventoryValue = InventoryValue::find($internalInventoryValue->InternalID);
                } else {
                    $inventoryValue = new InventoryValue();
                }
                $inventoryValue->InventoryInternalID = $data->InventoryInternalID;
                $inventoryValue->Month = $bulan;
                $inventoryValue->Year = $tahun;
                $inventoryValue->Quantity = $tampQty;
                $inventoryValue->Value = $average;
                $inventoryValue->CompanyInternalID = Auth::user()->Company->InternalID;
                $inventoryValue->UserRecord = Auth::user()->UserID;
                $inventoryValue->UserModified = '0';
                $inventoryValue->save();
            }
        }
        if ($checkKosong == 1) {
            //insert detail
            $detail = new JournalDetail();
            $detail->JournalInternalID = $header->InternalID;
            $detail->JournalIndex = $a;
            $detail->JournalNotes = '';
            $detail->JournalDebetMU = $tampValue;
            $detail->JournalCreditMU = 0;
            $detail->CurrencyInternalID = Currency::where('CompanyInternalID', Auth::user()->CompanyInternalID)->where('Default', 1)->first()->InternalID;
            $detail->CurrencyRate = 1;
            $detail->JournalDebet = $tampValue;
            $detail->JournalCredit = 0;
            $default = Default_s::find(9)->DefaultID;
            $defaultHPP = Default_s::getInternalCoa($default);
            $detail->JournalTransactionID = $default;
            $detail->ACC1InternalID = $defaultHPP->ACC1InternalID;
            $detail->ACC2InternalID = $defaultHPP->ACC2InternalID;
            $detail->ACC3InternalID = $defaultHPP->ACC3InternalID;
            $detail->ACC4InternalID = $defaultHPP->ACC4InternalID;
            $detail->ACC5InternalID = $defaultHPP->ACC5InternalID;
            $detail->ACC6InternalID = $defaultHPP->ACC6InternalID;
            $detail->COAName = Coa::find(Coa::getInternalID($defaultHPP->ACC1InternalID, $defaultHPP->ACC2InternalID, $defaultHPP->ACC3InternalID, $defaultHPP->ACC4InternalID, $defaultHPP->ACC5InternalID, $defaultHPP->ACC6InternalID))->COAName;
            $detail->UserRecord = Auth::user()->UserID;
            $detail->UserModified = '0';
            $detail->save();

            //Memo In Out
            $this->journalMemo($bulan, $tahun, $inventory);
            $messages = 'suksesCOGS';
        } else {
            $header->delete();
            $messages = 'gagalCOGS';
        }
        //view data
        $yearMinDepreciation = DepreciationHeader::getYearMin();
        $yearMinPurchase = PurchaseHeader::getYearMin();
        $yearMinSales = SalesHeader::getYearMin();
        $yearMinCOGS = ($yearMinPurchase < $yearMinSales ? $yearMinPurchase : $yearMinSales);
        if (($yearMinPurchase == 0 && $yearMinSales != 0) || ($yearMinPurchase != 0 && $yearMinSales == 0)) {
            $yearMinCOGS = $yearMinPurchase;
            if ($yearMinCOGS == 0) {
                $yearMinCOGS = $yearMinSales;
            }
        }
        $yearMinClosingBalance = JournalHeader::getYearMin();
        $yearMax = date("Y");
        return View::make('utility.monthlyProcess')
                        ->withToogle('utility')->withAktif('monthlyProcess')
                        ->withYearmindepreciation($yearMinDepreciation)
                        ->withYearmincogs($yearMinCOGS)
                        ->withYearminclosingbalance($yearMinClosingBalance)
                        ->withYearmax($yearMax)
                        ->withMessages($messages);
    }

    public function journalMemo($bulan, $tahun, $inventory) {
        //hapus dulu data jika sudah ada
        $tampJournal2 = JournalHeader::where('TransactionID', 'Class Memo')
                ->where('CompanyInternalID', Auth::user()->CompanyInternalID)
                ->whereRaw('YEAR(JournalDate) = "' . $tahun . '" AND MONTH(JournalDate) = "' . $bulan . '"')
                ->first();
        if ($tampJournal2 != '') {
            $journal2 = JournalHeader::find($tampJournal2->InternalID);
            //hapus detil
            $detilData2 = $journal2->journalDetail;
            foreach ($detilData2 as $value2) {
                $detil2 = JournalDetail::find($value2->InternalID);
                $detil2->delete();
            }
            //hapus journal
            $journal2->delete();
        }

        //header
        $header = new JournalHeader;
        $cari = 'ME-' . str_pad($bulan, 2, '0', STR_PAD_LEFT) . substr($tahun, 2, 2);
        $header->JournalID = JournalHeader::getNextIDJournal($cari . '-');
        $header->JournalDate = date("Y-m-t", strtotime('01-' . str_pad($bulan, 2, '0', STR_PAD_LEFT) . '-' . $tahun));
        $header->JournalType = 'Memorial';
        $header->JournalFrom = Auth::user()->UserID;
        $header->Notes = '';
        $header->DepartmentInternalID = Department::where('CompanyInternalID', Auth::user()->CompanyInternalID)->first()->InternalID;
        $header->SlipInternalID = NULL;
        $header->TransactionID = 'Class Memo';
        $header->ACC5InternalID = 0;
        $header->Lock = '0';
        $header->Check = '0';
        $header->Flag = '0';
        $header->UserRecord = Auth::user()->UserID;
        $header->CompanyInternalID = Auth::user()->Company->InternalID;
        $header->UserModified = '0';
        $header->Remark = '';
        $header->save();
        $checkKosong = 0;
        $a = 0;
        $tampValue = 0;

        foreach ($inventory as $data) {
            $qtySales = SalesHeader::qtyInventory($data->InventoryInternalID, $bulan, $tahun);
            $qtySalesR = SalesReturnHeader::qtyInventory($data->InventoryInternalID, $bulan, $tahun);
            $qtyMemoOut = MemoOutHeader::qtyInventory($data->InventoryInternalID, $bulan, $tahun);
            $qtyPurchase = PurchaseHeader::qtyInventory($data->InventoryInternalID, $bulan, $tahun);
            $qtyPurchaseR = PurchaseReturnHeader::qtyInventory($data->InventoryInternalID, $bulan, $tahun);
            $qtyMemoIn = MemoInHeader::qtyInventory($data->InventoryInternalID, $bulan, $tahun);
            $purchase = PurchaseHeader::valueInventory($data->InventoryInternalID, $bulan, $tahun);
            $purchaseR = PurchaseReturnHeader::valueInventory($data->InventoryInternalID, $bulan, $tahun);
            $memoIn = MemoInHeader::valueInventory($data->InventoryInternalID, $bulan, $tahun);
            $valueBefore = InventoryValue::valueInventoryBefore($data->InventoryInternalID, $bulan, $tahun);
            $qtyBefore = InventoryValue::qtyInventoryBefore($data->InventoryInternalID, $bulan, $tahun);

            $dataInitialQuantity = $data->InitialQuantity;
            $dataInitialValue = $data->InitialValue;
            if (InventoryValue::where('InventoryInternalID', $data->InternalID)
                            ->orderBy("Year", 'desc')
                            ->orderBy("Month", 'desc')
                            ->whereRaw("CONCAT(Year,'-',Month,'-31') < " . $tahun . '-' . $bulan . '-01')
                            ->count() > 0) {
                $dataInitialQuantity = 0;
                $dataInitialValue = 0;
            }
            $qtyDividen = $dataInitialQuantity + $qtyBefore + $qtyPurchase - $qtyPurchaseR + $qtyMemoIn;
            if ($qtyDividen == 0) {
                $average = 0;
            } else {
                $average = (($dataInitialValue * $dataInitialQuantity) + ($valueBefore * $qtyBefore) + $purchase - $purchaseR + $memoIn) / $qtyDividen;
            }
            $value = $average * ($qtyMemoOut);
            if ($value != 0) {
                $checkKosong = 1;
                //insert detail
                $detail = new JournalDetail();
                $detail->JournalInternalID = $header->InternalID;
                $detail->JournalIndex = $a;
                $detail->JournalNotes = '';
                $detail->JournalDebetMU = $value < 0 ? $value : 0;
                $detail->JournalCreditMU = $value >= 0 ? $value : 0;
                $detail->CurrencyInternalID = Currency::where('CompanyInternalID', Auth::user()->CompanyInternalID)->where('Default', 1)->first()->InternalID;
                $detail->CurrencyRate = 1;
                $detail->JournalDebet = $value < 0 ? $value : 0;
                $detail->JournalCredit = $value >= 0 ? $value : 0;
                $detail->JournalTransactionID = $data->InventoryID;
                $detail->ACC1InternalID = $data->ACC1InternalID;
                $detail->ACC2InternalID = $data->ACC2InternalID;
                $detail->ACC3InternalID = $data->ACC3InternalID;
                $detail->ACC4InternalID = $data->ACC4InternalID;
                $detail->ACC5InternalID = $data->ACC5InternalID;
                $detail->ACC6InternalID = $data->ACC6InternalID;
                $detail->COAName = Coa::find(Coa::getInternalID($data->ACC1InternalID, $data->ACC2InternalID, $data->ACC3InternalID, $data->ACC4InternalID, $data->ACC5InternalID, $data->ACC6InternalID))->COAName;
                $detail->UserRecord = Auth::user()->UserID;
                $detail->UserModified = '0';
                $detail->save();
                $tampValue += $value;
                $a++;
            }
        }

        if ($checkKosong == 1) {
            //insert detail
            $detail = new JournalDetail();
            $detail->JournalInternalID = $header->InternalID;
            $detail->JournalIndex = $a;
            $detail->JournalNotes = '';
            $detail->JournalDebetMU = $tampValue > 0 ? $tampValue : 0;
            $detail->JournalCreditMU = $tampValue <= 0 ? $tampValue : 0;
            $detail->CurrencyInternalID = Currency::where('CompanyInternalID', Auth::user()->CompanyInternalID)->where('Default', 1)->first()->InternalID;
            $detail->CurrencyRate = 1;
            $detail->JournalDebet = $tampValue > 0 ? $tampValue : 0;
            $detail->JournalCredit = $tampValue <= 0 ? $tampValue : 0;
            $default = Default_s::find(9)->DefaultID;
            $defaultHPP = Default_s::getInternalCoa($default);
            $detail->JournalTransactionID = $default;
            $detail->ACC1InternalID = $defaultHPP->ACC1InternalID;
            $detail->ACC2InternalID = $defaultHPP->ACC2InternalID;
            $detail->ACC3InternalID = $defaultHPP->ACC3InternalID;
            $detail->ACC4InternalID = $defaultHPP->ACC4InternalID;
            $detail->ACC5InternalID = $defaultHPP->ACC5InternalID;
            $detail->ACC6InternalID = $defaultHPP->ACC6InternalID;
            $detail->COAName = Coa::find(Coa::getInternalID($defaultHPP->ACC1InternalID, $defaultHPP->ACC2InternalID, $defaultHPP->ACC3InternalID, $defaultHPP->ACC4InternalID, $defaultHPP->ACC5InternalID, $defaultHPP->ACC6InternalID))->COAName;
            $detail->UserRecord = Auth::user()->UserID;
            $detail->UserModified = '0';
            $detail->save();
        } else {
            $header->delete();
        }
    }

    public function insertCoaDetail($bulan, $tahun) {
        $account = Coa::where('CompanyInternalID', Auth::user()->CompanyInternalID)->get();

        foreach ($account as $data) {
            //nilai debet dan kredit akun bulan itu
            $valueDebet = JournalHeader::getDebetCOA($bulan, $tahun, $data->ACC1InternalID, $data->ACC2InternalID, $data->ACC3InternalID, $data->ACC4InternalID, $data->ACC5InternalID, $data->ACC6InternalID);
            $valueCredit = JournalHeader::getCreditCOA($bulan, $tahun, $data->ACC1InternalID, $data->ACC2InternalID, $data->ACC3InternalID, $data->ACC4InternalID, $data->ACC5InternalID, $data->ACC6InternalID);

            $dataInitialDebet = AccountValue::debetAccountBefore($data->InternalID, $bulan, $tahun);
            $dataInitialCredit = AccountValue::creditAccountBefore($data->InternalID, $bulan, $tahun);
            if (AccountValue::where('COAInternalID', $data->InternalID)
                            ->orderBy("Year", 'desc')
                            ->orderBy("Month", 'desc')
                            ->whereRaw("CONCAT(Year,'-',Month,'-31') < " . $tahun . '-' . $bulan . '-01')
                            ->count() > 0) {
                $dataInitialDebet = 0;
                $dataInitialCredit = 0;
            }
            $InitialBalance = $dataInitialDebet - $dataInitialCredit;
            $debet = $dataInitialDebet + $valueDebet;
            $credit = $dataInitialCredit + $valueCredit;
            $EndBalance = $debet - $credit + $InitialBalance;
            //insert
            $check = AccountValue::where('COAInternalID', $data->InternalID)->where('Month', $bulan)->where('Year', $tahun)->count();
            if ($check > 0) {
                $internalAccountValue = AccountValue::where('COAInternalID', $data->InternalID)->where('Month', $bulan)->where('Year', $tahun)->first();
                $accValue = AccountValue::find($internalAccountValue->InternalID);
            } else {
                $accValue = new AccountValue();
            }
            $accValue->COAInternalID = $data->InternalID;
            $accValue->Month = $bulan;
            $accValue->Year = $tahun;
            $accValue->Debet = $debet;
            $accValue->Credit = $credit;
            $accValue->Locked = 0;
            $accValue->InitialBalance = $InitialBalance;
            $accValue->EndBalance = $EndBalance;
            $accValue->CompanyInternalID = Auth::user()->Company->InternalID;
            $accValue->UserRecord = Auth::user()->UserID;
            $accValue->UserModified = '0';
            $accValue->save();
        }
    }

    public function journalClosing() {
        $bulan = Input::get('month');
        $tahun = Input::get('year');
        $tampJournal = '0ed';
        while ($tampJournal != '') {
            //hapus dulu data jika sudah ada
            $tampJournal = JournalHeader::where('TransactionID', 'Journal Closing')
                    ->where('CompanyInternalID', Auth::user()->CompanyInternalID)
                    ->whereRaw('YEAR(JournalDate) = "' . $tahun . '" AND MONTH(JournalDate) = "' . $bulan . '"')
                    ->first();
            if ($tampJournal != '') {
                $journal = JournalHeader::find($tampJournal->InternalID);
                //hapus detil
                $detilData = $journal->journalDetail;
                foreach ($detilData as $value) {
                    $detil = JournalDetail::find($value->InternalID);
                    $detil->delete();
                }
                //hapus journal
                $journal->delete();
                $tampJournal = JournalHeader::where('TransactionID', 'Journal Closing Reverse')
                        ->where('CompanyInternalID', Auth::user()->CompanyInternalID)
                        ->whereRaw('YEAR(JournalDate) = "' . $tahun . '" AND MONTH(JournalDate) = "' . $bulan . '"')
                        ->first();
                if ($tampJournal != '') {
                    $journal = JournalHeader::find($tampJournal->InternalID);
                    //hapus detil
                    $detilData = $journal->journalDetail;
                    foreach ($detilData as $value) {
                        $detil = JournalDetail::find($value->InternalID);
                        $detil->delete();
                    }
                    //hapus journal
                    $journal->delete();
                }
            }
        }
        $account = Coa::where('Flag', 1)
                        ->where('CompanyInternalID', Auth::user()->CompanyInternalID)->get();
        //insert header
        $header = new JournalHeader;
        $cari = 'ME-' . str_pad($bulan, 2, '0', STR_PAD_LEFT) . substr($tahun, 2, 2);
        $header->JournalID = JournalHeader::getNextIDJournal($cari . '-');
        $header->JournalDate = date("Y-m-t", strtotime('01-' . str_pad($bulan, 2, '0', STR_PAD_LEFT) . '-' . $tahun));
        $header->JournalType = 'Memorial';
        $header->JournalFrom = Auth::user()->UserID;
        $header->Notes = '';
        $header->DepartmentInternalID = Department::where('CompanyInternalID', Auth::user()->CompanyInternalID)->first()->InternalID;
        $header->SlipInternalID = NULL;
        $header->TransactionID = 'Journal Closing';
        $header->ACC5InternalID = 0;
        $header->Lock = '0';
        $header->Check = '0';
        $header->Flag = '0';
        $header->UserRecord = Auth::user()->UserID;
        $header->CompanyInternalID = Auth::user()->Company->InternalID;
        $header->UserModified = '0';
        $header->Remark = '';
        $header->save();
        $a = 1;
        $checkKosong = 0;
        $tampValue = 0;
        foreach ($account as $data) {
            $checkKosong = 1;
            $valueDebet = JournalHeader::getDebetCOA($bulan, $tahun, $data->ACC1InternalID, $data->ACC2InternalID, $data->ACC3InternalID, $data->ACC4InternalID, $data->ACC5InternalID, $data->ACC6InternalID);
            $valueCredit = JournalHeader::getCreditCOA($bulan, $tahun, $data->ACC1InternalID, $data->ACC2InternalID, $data->ACC3InternalID, $data->ACC4InternalID, $data->ACC5InternalID, $data->ACC6InternalID);
            $value = $valueDebet - $valueCredit;
            $valueDebet = 0;
            $valueCredit = $value;
            $tampValue -= $value;
            if ($value < 0) {
                $value *= -1;
                $valueDebet = $value;
                $valueCredit = 0;
            }
            //insert detail
            $detail = new JournalDetail();
            $detail->JournalInternalID = $header->InternalID;
            $detail->JournalIndex = $a;
            $detail->JournalNotes = '';
            $detail->JournalDebetMU = $valueDebet;
            $detail->JournalCreditMU = $valueCredit;
            $detail->CurrencyInternalID = Currency::where('CompanyInternalID', Auth::user()->CompanyInternalID)->where('Default', 1)->first()->InternalID;
            $detail->CurrencyRate = 1;
            $detail->JournalDebet = $valueDebet;
            $detail->JournalCredit = $valueCredit;
            $detail->JournalTransactionID = Coa::formatCoa($data->ACC1InternalID, $data->ACC2InternalID, $data->ACC3InternalID, $data->ACC4InternalID, $data->ACC5InternalID, $data->ACC6InternalID, 1);
            $detail->ACC1InternalID = $data->ACC1InternalID;
            $detail->ACC2InternalID = $data->ACC2InternalID;
            $detail->ACC3InternalID = $data->ACC3InternalID;
            $detail->ACC4InternalID = $data->ACC4InternalID;
            $detail->ACC5InternalID = $data->ACC5InternalID;
            $detail->ACC6InternalID = $data->ACC6InternalID;
            $detail->COAName = Coa::find(Coa::getInternalID($data->ACC1InternalID, $data->ACC2InternalID, $data->ACC3InternalID, $data->ACC4InternalID, $data->ACC5InternalID, $data->ACC6InternalID))->COAName;
            $detail->UserRecord = Auth::user()->UserID;
            $detail->UserModified = '0';
            $detail->save();
            $a++;
        }
        if ($checkKosong == 1) {
            if ($tampValue < 0) {
                $valueDebet = $tampValue * -1;
                $valueCredit = 0;
            } else {
                $valueDebet = 0;
                $valueCredit = $tampValue;
            }
            //insert detail
            $detail = new JournalDetail();
            $detail->JournalInternalID = $header->InternalID;
            $detail->JournalIndex = $a;
            $detail->JournalNotes = '';
            $detail->JournalDebetMU = $valueDebet;
            $detail->JournalCreditMU = $valueCredit;
            $detail->CurrencyInternalID = Currency::where('CompanyInternalID', Auth::user()->CompanyInternalID)->where('Default', 1)->first()->InternalID;
            $detail->CurrencyRate = 1;
            $detail->JournalDebet = $valueDebet;
            $detail->JournalCredit = $valueCredit;
            $default = Default_s::find(7)->DefaultID;
            $defaultHPP = Default_s::getInternalCoa($default);
            $detail->JournalTransactionID = $default;
            $detail->ACC1InternalID = $defaultHPP->ACC1InternalID;
            $detail->ACC2InternalID = $defaultHPP->ACC2InternalID;
            $detail->ACC3InternalID = $defaultHPP->ACC3InternalID;
            $detail->ACC4InternalID = $defaultHPP->ACC4InternalID;
            $detail->ACC5InternalID = $defaultHPP->ACC5InternalID;
            $detail->ACC6InternalID = $defaultHPP->ACC6InternalID;
            $detail->COAName = Coa::find(Coa::getInternalID($defaultHPP->ACC1InternalID, $defaultHPP->ACC2InternalID, $defaultHPP->ACC3InternalID, $defaultHPP->ACC4InternalID, $defaultHPP->ACC5InternalID, $defaultHPP->ACC6InternalID))->COAName;
            $detail->UserRecord = Auth::user()->UserID;
            $detail->UserModified = '0';
            $detail->save();
            //insert Journal baru buat laba rugi jalan
            $header = new JournalHeader;
            $cari = 'ME-' . str_pad($bulan, 2, '0', STR_PAD_LEFT) . substr($tahun, 2, 2);
            $header->JournalID = JournalHeader::getNextIDJournal($cari . '-');
            $header->JournalDate = date("Y-m-t", strtotime('01-' . str_pad($bulan, 2, '0', STR_PAD_LEFT) . '-' . $tahun));
            $header->JournalType = 'Memorial';
            $header->JournalFrom = Auth::user()->UserID;
            $header->Notes = '';
            $header->DepartmentInternalID = Department::where('CompanyInternalID', Auth::user()->CompanyInternalID)->first()->InternalID;
            $header->SlipInternalID = NULL;
            $header->TransactionID = 'Journal Closing Reverse';
            $header->ACC5InternalID = 0;
            $header->Lock = '0';
            $header->Check = '0';
            $header->Flag = '0';
            $header->UserRecord = Auth::user()->UserID;
            $header->CompanyInternalID = Auth::user()->Company->InternalID;
            $header->UserModified = '0';
            $header->Remark = '';
            $header->save();
            //insert detail closed
            $detail = new JournalDetail();
            $detail->JournalInternalID = $header->InternalID;
            $detail->JournalIndex = $a;
            $detail->JournalNotes = '';
            $detail->JournalDebetMU = $valueCredit;
            $detail->JournalCreditMU = $valueDebet;
            $detail->CurrencyInternalID = Currency::where('CompanyInternalID', Auth::user()->CompanyInternalID)->where('Default', 1)->first()->InternalID;
            $detail->CurrencyRate = 1;
            $detail->JournalDebet = $valueCredit;
            $detail->JournalCredit = $valueDebet;
            $default = Default_s::find(7)->DefaultID;
            $defaultHPP = Default_s::getInternalCoa($default);
            $detail->JournalTransactionID = $default;
            $detail->ACC1InternalID = $defaultHPP->ACC1InternalID;
            $detail->ACC2InternalID = $defaultHPP->ACC2InternalID;
            $detail->ACC3InternalID = $defaultHPP->ACC3InternalID;
            $detail->ACC4InternalID = $defaultHPP->ACC4InternalID;
            $detail->ACC5InternalID = $defaultHPP->ACC5InternalID;
            $detail->ACC6InternalID = $defaultHPP->ACC6InternalID;
            $detail->COAName = Coa::find(Coa::getInternalID($defaultHPP->ACC1InternalID, $defaultHPP->ACC2InternalID, $defaultHPP->ACC3InternalID, $defaultHPP->ACC4InternalID, $defaultHPP->ACC5InternalID, $defaultHPP->ACC6InternalID))->COAName;
            $detail->UserRecord = Auth::user()->UserID;
            $detail->UserModified = '0';
            $detail->save();
            //insert detail laba rugi jalan
            $detail = new JournalDetail();
            $detail->JournalInternalID = $header->InternalID;
            $detail->JournalIndex = $a;
            $detail->JournalNotes = '';
            $detail->JournalDebetMU = $valueDebet;
            $detail->JournalCreditMU = $valueCredit;
            $detail->CurrencyInternalID = Currency::where('CompanyInternalID', Auth::user()->CompanyInternalID)->where('Default', 1)->first()->InternalID;
            $detail->CurrencyRate = 1;
            $detail->JournalDebet = $valueDebet;
            $detail->JournalCredit = $valueCredit;
            $default = Default_s::find(8)->DefaultID;
            $defaultHPP = Default_s::getInternalCoa($default);
            $detail->JournalTransactionID = $default;
            $detail->ACC1InternalID = $defaultHPP->ACC1InternalID;
            $detail->ACC2InternalID = $defaultHPP->ACC2InternalID;
            $detail->ACC3InternalID = $defaultHPP->ACC3InternalID;
            $detail->ACC4InternalID = $defaultHPP->ACC4InternalID;
            $detail->ACC5InternalID = $defaultHPP->ACC5InternalID;
            $detail->ACC6InternalID = $defaultHPP->ACC6InternalID;
            $detail->COAName = Coa::find(Coa::getInternalID($defaultHPP->ACC1InternalID, $defaultHPP->ACC2InternalID, $defaultHPP->ACC3InternalID, $defaultHPP->ACC4InternalID, $defaultHPP->ACC5InternalID, $defaultHPP->ACC6InternalID))->COAName;
            $detail->UserRecord = Auth::user()->UserID;
            $detail->UserModified = '0';
            $detail->save();
            $messages = 'suksesClosing';

            $this->insertCoaDetail($bulan,$tahun);
        } else {
            $header->delete();
            $messages = 'gagalClosing';
        }
        //view data
        $yearMinDepreciation = DepreciationHeader::getYearMin();
        $yearMinPurchase = PurchaseHeader::getYearMin();
        $yearMinSales = SalesHeader::getYearMin();
        $yearMinCOGS = ($yearMinPurchase < $yearMinSales ? $yearMinPurchase : $yearMinSales);
        if (($yearMinPurchase == 0 && $yearMinSales != 0) || ($yearMinPurchase != 0 && $yearMinSales == 0)) {
            $yearMinCOGS = $yearMinPurchase;
            if ($yearMinCOGS == 0) {
                $yearMinCOGS = $yearMinSales;
            }
        }
        $yearMinClosingBalance = JournalHeader::getYearMin();
        $yearMax = date("Y");
        return View::make('utility.monthlyProcess')
                        ->withToogle('utility')->withAktif('monthlyProcess')
                        ->withYearmindepreciation($yearMinDepreciation)
                        ->withYearmincogs($yearMinCOGS)
                        ->withYearminclosingbalance($yearMinClosingBalance)
                        ->withYearmax($yearMax)
                        ->withMessages($messages);
    }

}
