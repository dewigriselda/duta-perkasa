<!DOCTYPE html>

<html lang="id">
    <head>
        <meta charset="utf-8" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1" />-->
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta name="description" content="tokotab adalah toko online yang menjual peralatan alat tulis kantor" />
        <meta name="keywords" content="stationery, alat tulis, toko alat tulis, atk" />
        <meta name="author" content="genesys integrated indonesia" />

        <title>Tabah Stationery | not found</title>

        <link rel="stylesheet" href="{{Asset('lib/css/bootstrap.min.css')}}" />
        <link rel="stylesheet" href="{{Asset('lib/css/material-icons.css')}}" />
        <link rel="stylesheet" href="{{Asset('css/dist/template/header-footer.css')}}" />
        <link rel="stylesheet" href="{{Asset('css/dist/congratulation.css')}}">
    </head>

    <body>

        <section id="main-section">
            <div class="container">
                <div class="row">
                    <div class="col-xs-24 text-center margbot10">
                        <a href="{{Route('showHome')}}"><img draggable="false" alt="stationeryone" src="{{Asset('images/logo_stationeryone@2x.png')}}"></a>
                        <h4 class="logo-text text-center"><a class="cwhite" href="{{Route('showHome')}}">Tokotab.com</a></h4>
                    </div>

                    <div class="col-xs-14 col-xs-offset-5">
                        <div class="card clearfix">
                            <div class="form-real">
                                <h4 class="text-center fw400">Oops, sepertinya halaman yang anda cari tidak ada.</h4>
                                <h1 class="text-center"><i class="material-icons">error</i></h1>
                                <h5 class="text-center margbot20 fw300">coba cek kembali link yang anda tulis.</h5>
                                <div class="text-center">
                                    <a href="{{Route("showShop","semua/1/ASC")}}" class="button btnaccent">Kembali Belanja</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <script src="{{Asset('lib/js/jquery-1.11.3.min.js')}}"></script>
        <script src="{{Asset('lib/jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
        <script src="{{Asset('lib/js/bootstrap.min.js')}}"></script>
        <script src="{{Asset('js/color.select.js')}}"></script>
        <script src="{{Asset('js/header-footer.js')}}"></script>
    </body>
</html>