<?php

/*
  |--------------------------------------------------------------------------
  | Application & Route Filters
  |--------------------------------------------------------------------------
  |
  | Below you will find the "before" and "after" events for the application
  | which may be used to do any work before or after a request into your
  | application. Here you may also register your custom route filters.
  |
 */

App::before(function($request) {
    //
});


App::after(function($request, $response) {
    //
});

/*
  |--------------------------------------------------------------------------
  | Authentication Filters
  |--------------------------------------------------------------------------
  |
  | The following filters are used to verify that the user of the current
  | session is logged into this application. The "basic" filter easily
  | integrates HTTP Basic authentication for quick, simple checking.
  |
 */

Route::filter('auth', function() {
    if (Auth::guest()) {
        if (Request::ajax()) {
            return Response::make('Unauthorized', 401);
        } else {
            return Redirect::guest('login')
                            ->withMessages('belumLogin');
        }
    }
});


Route::filter('auth.basic', function() {
    return Auth::basic();
});

/*
  |--------------------------------------------------------------------------
  | Guest Filter
  |--------------------------------------------------------------------------
  |
  | The "guest" filter is the counterpart of the authentication filters as
  | it simply checks that the current user is not logged in. A redirect
  | response will be issued if they are, which you may freely change.
  |
 */

Route::filter('guest', function() {
    if (Auth::check())
        return Redirect::to('/');
});

/*
  |--------------------------------------------------------------------------
  | CSRF Protection Filter
  |--------------------------------------------------------------------------
  |
  | The CSRF filter is responsible for protecting your application against
  | cross-site request forgery attacks. If this special token in a user
  | session does not match the one given in this request, we'll bail.
  |
 */

Route::filter('csrf', function() {
    if (Session::token() !== Input::get('_token')) {
        throw new Illuminate\Session\TokenMismatchException;
    }
});

Route::filter('client', function() {
    if (Auth::user()->Company->InternalID == '-1') {
        $messages = 'accessDenied';
        Session::flash('messages', $messages);
        return Redirect::Route('showUser');
    }
    if (Auth::user()->Company->InternalID == '-2') {
        $messages = 'accessDenied';
        Session::flash('messages', $messages);
        return Redirect::Route('showHomeAgent');
    }
});

Route::filter('superAdmin', function() {
    if (Auth::user()->Company->InternalID == '-2') {
        $messages = 'accessDenied';
        Session::flash('messages', $messages);
        return Redirect::Route('showHomeAgent');
    }
    if (Auth::user()->Company->InternalID != '-1') {
        $messages = 'accessDenied';
        Session::flash('messages', $messages);
        return Redirect::Route('coaLevel');
    }
});
Route::filter('agent', function() {
    if (Auth::user()->Company->InternalID == '-1') {
        $messages = 'accessDenied';
        Session::flash('messages', $messages);
        return Redirect::Route('showUser');
    }
    if (Auth::user()->Company->InternalID != '-2') {
        $messages = 'accessDenied';
        Session::flash('messages', $messages);
        return Redirect::Route('coaLevel');
    }
});
Route::filter('status', function() {
    $dataUser = User::find(Auth::user()->InternalID);
    $statusUser = $dataUser->Status;
    $statusCompany = $dataUser->Company->Status;
    if ($statusCompany == 0) {
        Session::flash('status', 'company');
        Auth::logout();
        return Redirect::intended('login');
    } else if ($statusUser == 0) {
        Session::flash('status', 'user');
        Auth::logout();
        return Redirect::intended('login');
    }
});

Route::filter('payment', function() {
    $status = Auth::user()->Company->StatusPayment;
    $expiredDate = Auth::user()->Company->ExpiredDate;

    if ($status == 0 || (date("Y-m-d", strtotime($expiredDate)) < date("Y-m-d", strtotime(date("Y-m-d"))))) {
        return Redirect::Route('showContact');
    }
});

Route::filter('memory', function() {
    $memory = countMemory();
    $maxMemory = Auth::user()->Company->Package->Memory;
    if ($memory >= $maxMemory) {
        return Redirect::Route('showContact');
    }
});

Route::filter('module', function($route, $request, $value) {
    $idModul = $value;
    $modul = Modul::where('ModulID', $idModul)->pluck('InternalID');
    $package = Auth::user()->Company->PackageInternalID;
    $result = PackageDetail::where('PackageInternalID', $package)
                    ->where('ModulInternalID', $modul)->count();
    if ($result == 0 && Auth::user()->CompanyInternalID != -1) {
        $messages = 'accessDenied';
        Session::flash('messages', $messages);
        return Redirect::Route('coaLevel');
    }
});

Route::filter('matrix', function($route, $request, $value) {
    $idMatrix = $value;
    $matrix = Matrix::where('MatrixID', $idMatrix)->pluck('InternalID');
    $result = UserDetail::where('UserInternalID', Auth::user()->InternalID)
                    ->where('MatrixInternalID', $matrix)->count();
    if ($result == 0 && Auth::user()->CompanyInternalID != -1) {
        $messages = 'accessDenied';
        Session::flash('messages', $messages);
        return Redirect::Route('coaLevel');
    }
});

Route::filter('userAdmin', function() {
    $matrix = Auth::user()->Company->Package->TotalMatrix;
    $userMatrix = UserDetail::where('UserInternalID', Auth::user()->InternalID)->count();
    if ($userMatrix != $matrix) {
        $messages = 'accessDenied';
        Session::flash('messages', $messages);
        return Redirect::Route('coaLevel');
    }
});
