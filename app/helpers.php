<?php

function getCompanyWebsite() {
    return "www.dutaperkasa.com";
}

function customerCOAInternalID() {
    return 53;
}

function supplierCOAInternalID() {
    //idr, usd = 16
    return 15;
}

// My common functions
function myEscapeStringData($dataArray) {
//implode
    $tampString = '"' . implode('", "', $dataArray) . '"';
    $tampString = str_replace(',","', '!#44!","', $tampString);
    $tampString = str_replace(',"}"', '!#44!"}"', $tampString);
    $tampString = str_replace(',"', '!#4344!', $tampString);
    $tampString = str_replace(",", "!#44!", $tampString);
    $tampString = str_replace('!#4344!', ',"', $tampString);
    $tampString = str_replace("\/", "!#47!", $tampString);
    $tampString = str_replace('\"', "!#34!", $tampString);
    $tampString = str_replace("'", "!#39!", $tampString);
    return $tampString;
}

function myEscapeStringDataLevel($dataArray) {
//implode
    $tampString = '"{' . implode(',', $dataArray) . '}"';
    $tampString = str_replace(',","', '!#44!","', $tampString);
    $tampString = str_replace(',"}"', '!#44!"}"', $tampString);
    $tampString = str_replace(',"', '!#4344!', $tampString);
    $tampString = str_replace(",", "!#44!", $tampString);
    $tampString = str_replace('!#4344!', ',"', $tampString);
    $tampString = str_replace("/", "!#47!", $tampString);
    $tampString = str_replace('"{"', '!#1111!', $tampString);
    $tampString = str_replace('"}"', '!#2222!', $tampString);
    $tampString = str_replace('":"', '!#3333!', $tampString);
    $tampString = str_replace('","', '!#4444!', $tampString);
    $tampString = str_replace('"', "!#34!", $tampString);
    $tampString = str_replace('!#1111!', '"{"', $tampString);
    $tampString = str_replace('!#2222!', '"}"', $tampString);
    $tampString = str_replace('!#3333!', '":"', $tampString);
    $tampString = str_replace('!#4444!', '","', $tampString);
    $tampString = str_replace("'", "!#39!", $tampString);
    return $tampString;
}

function myEncryptNumeric($text) {
    $text = crypt($text, '$1$g$');
    $text = str_replace("~", "21", $text);
    $text = str_replace("`", "22", $text);
    $text = str_replace("!", "23", $text);
    $text = str_replace("@", "24", $text);
    $text = str_replace("#", "25", $text);
    $text = str_replace("$", "26", $text);
    $text = str_replace("%", "27", $text);
    $text = str_replace("^", "28", $text);
    $text = str_replace("&", "29", $text);
    $text = str_replace("*", "33", $text);
    $text = str_replace("(", "43", $text);
    $text = str_replace(")", "53", $text);
    $text = str_replace("-", "63", $text);
    $text = str_replace("_", "73", $text);
    $text = str_replace("+", "83", $text);
    $text = str_replace("=", "93", $text);
    $text = str_replace("{", "30", $text);
    $text = str_replace("[", "31", $text);
    $text = str_replace("]", "32", $text);
    $text = str_replace("}", "34", $text);
    $text = str_replace("|", "35", $text);
    $text = str_replace(";", "36", $text);
    $text = str_replace(":", "37", $text);
    $text = str_replace('"', "38", $text);
    $text = str_replace("'", "39", $text);
    $text = str_replace("<", "40", $text);
    $text = str_replace(">", "41", $text);
    $text = str_replace(",", "42", $text);
    $text = str_replace(".", "44", $text);
    $text = str_replace("?", "45", $text);
    $text = str_replace("/", "46", $text);
    return $text;
}

function myCheckIsEmpty($text) {
    $textSplit = explode(';', $text);
    $countCek = 0;
    foreach ($textSplit as $data) {
        $count = 0;
        if ($data == 'Company') {
            $count += Company::where('InternalID', "<>", '-1')->count();
        }
        if ($data == 'Coa1') {
            $count += Coa1::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
        }
        if ($data == 'Coa') {
            $count += Coa::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
        }
        if ($data == 'Slip') {
            $count += Slip::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
        }
        if ($data == 'Cost') {
            $count += Cost::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
        }
        if ($data == 'Department') {
            $count += Department::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
        }
        if ($data == 'DepartmentDefault') {
            $count += Department::where('CompanyInternalID', Auth::user()->CompanyInternalID)->where('Default', '1')->count();
        }
        if ($data == 'Coa5') {
            $count += Coa5::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
        }
        if ($data == 'Currency') {
            $count += Currency::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
        }
        if ($data == 'GroupDepreciation') {
            $count += GroupDepreciation::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
        }
        if ($data == 'Depreciation') {
            $count += DepreciationHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
        }
        if ($data == 'InventoryType') {
            $count += InventoryType::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
        }
        if ($data == 'Inventory') {
            $count += Inventory::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
        }
        if ($data == 'InventoryUom') {
            $count += InventoryUom::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
        }
        if ($data == 'Uom') {
            $count += Uom::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
        }
        if ($data == 'Warehouse') {
            $count += Warehouse::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
        }
        if ($data == 'Customer') {
            $count += Coa6::where('CompanyInternalID', Auth::user()->CompanyInternalID)->where('Type', 'c')->count();
        }
        if ($data == 'Supplier') {
            $count += Coa6::where('CompanyInternalID', Auth::user()->CompanyInternalID)->where('Type', 's')->count();
        }
        if ($data == 'Sales') {
            $count += SalesHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
        }
        if ($data == 'Shipping') {
            $count += ShippingAddHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
        }
        if ($data == 'Packing') {
            $count += Packing::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
        }
        if ($data == 'SalesOrder') {
            $count += SalesOrderHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
        }
        if ($data == 'SalesReturn') {
            $count += SalesReturnHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
        }
        if ($data == 'Purchase') {
            $count += PurchaseHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
        }
        if ($data == 'Mrv') {
            $count += MrvHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
        }
        if ($data == 'PurchaseOrder') {
            $count += PurchaseOrderHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
        }
        if ($data == 'PurchaseReturn') {
            $count += PurchaseReturnHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
        }
        if ($data == 'Journal') {
            $count += JournalHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
        }
        if ($data == 'SalesPurchase') {
            $count += SalesHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
            $count += PurchaseHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
        }
        if ($data == 'Default') {
            $count += Default_s::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
        }
        if ($data == 'DefaultCurrency') {
            $count += Currency::where('CompanyInternalID', Auth::user()->CompanyInternalID)->where('Default', 1)->count();
        }
        if ($data == 'Variety') {
            $count += Variety::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
        }
        if ($data == 'Brand') {
            $count += Brand::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
        }
        if ($data == 'Group') {
            $count += Group::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
        }
        if ($data == 'Quotation') {
            $count += QuotationHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
        }
        if ($count > 0) {
            $countCek++;
        }
    }
    if ($countCek == count($textSplit)) {
        return false;
    } else {
        return true;
    }
}

function checkSeeNPPN() {
    if (Auth::user()->SeeNPPN == 0)
        return false;
    else
        return true;
}

function convertNol($numeric) {
    if ($numeric == 0) {
        return '-';
    }
    return $numeric;
}

function checkAmountSales($id) {
    $amount = TopUp::where('SalesOrderInternalID', $id)->sum('GrandTotalTopUp');
    $selisihbayar = TopUp::where('SalesOrderInternalID', $id)->sum('SelisihBayar');
    $salesOrder = SalesOrderHeader::find($id)->GrandTotal;
    $price = $salesOrder - ($amount + $selisihbayar);
    return $price;
}

function checkAmountPurchase($id) {
    $amount = TopUp::where('PurchaseOrderInternalID', $id)->sum('GrandTotalTopUp');
    $selisihbayar = TopUp::where('PurchaseOrderInternalID', $id)->sum('SelisihBayar');
    $salesOrder = PurchaseOrderHeader::find($id)->GrandTotal;
    $price = $salesOrder - ($amount + $selisihbayar);
    return $price;
}

function checkDpShipping($id) {
    $salesOrder = SalesOrderHeader::find($id);
//    $gt = $salesOrder->GrandTotal-($salesOrder->GrandTotal/1.1*0.1);
//    $gt = $salesOrder->GrandTotal;
    $gt = $salesOrder->GrandTotal;
    $grandTotal = round($gt * ($salesOrder->DownPayment / 100));
    if($grandTotal == 0)
        $grandTotal = $salesOrder->DPValue;
    $amount = TopUp::where('PaymentType', 2)
            ->where('SalesOrderInternalID', $id)
            ->where('SlipInternalID', "!=", -1)
            ->sum('GrandTotalTopUp');
    if ($grandTotal + $amount == 0)
        return False;

    $selba = TopUp::where('PaymentType', 2)
            ->where('SalesOrderInternalID', $id)
            ->where('SlipInternalID', "!=", -1)
            ->sum('SelisihBayar');

    $price = $grandTotal - $amount - $selba;

    if ($price <= 0) {
        return true;
    } else {
        return false;
    }
}

function checkDownPaymentSales($id) {
    $so = SalesOrderHeader::join('t_shipping_header', 't_salesorder_header.InternalID', '=', 't_shipping_header.SalesOrderInternalID')
                    ->where('t_shipping_header.ShippingID', $id)
                    ->select('t_salesorder_header.InternalID')->first();
    if (count($so) <= 0) {
        $amount = 0;
    } else {
        $amount = TopUp::where('PaymentType', 2)
                ->where('SalesOrderInternalID', $so->InternalID)
                ->sum('GrandTotalTopUp');
    }
    return $amount;
}

function checkDownPaymentPurchase($id) {
    $so = PurchaseOrderHeader::join('t_mrv_header', 't_salesorder_header.InternalID', '=', 't_mrv_header.SalesOrderInternalID')
                    ->where('t_mrv_header.MrvID', $id)
                    ->select('t_salesorder_header.InternalID')->first();
    if (count($so) <= 0) {
        $amount = 0;
    } else {
        $amount = TopUp::where('PaymentType', 2)
                ->where('PurchaseOrderInternalID', $so->InternalID)
                ->sum('GrandTotalTopUp');
    }
    return $amount;
}

function checkDpMrv($id) {
    $salesOrder = PurchaseOrderHeader::find($id);
//    $gt = $salesOrder->GrandTotal-($salesOrder->GrandTotal/1.1*0.1);
//    $gt = $salesOrder->GrandTotal;
    $gt = $salesOrder->GrandTotal;
    $grandTotal = round($gt * ($salesOrder->DownPayment / 100));
    if($grandTotal == 0)
        $grandTotal = $salesOrder->DPValue;
    $amount = TopUp::where('PaymentType', 2)
            ->where('PurchaseOrderInternalID', $id)
            ->where('SlipInternalID', "!=", -1)
            ->sum('GrandTotalTopUp');
    if ($grandTotal + $amount == 0)
        return False;

    $selba = TopUp::where('PaymentType', 2)
            ->where('PurchaseOrderInternalID', $id)
            ->where('SlipInternalID', "!=", -1)
            ->sum('SelisihBayar');

    $price = $grandTotal - $amount - $selba;

    if ($price <= 0) {
        return TRUE;
    } else {
        return False;
    }
}

function checkPembayaran($soInternalID) {
    $so = SalesOrderHeader::find($soInternalID);
    if ($so->isCash == 0 || $so->isCash == 1) {
        return true;
    } else {
        //cbd
        if ($so->isCash == 2) {
            if ($so->is_CBD_Finished == 1) {
                return true;
            } else {
                return false;
            }
        }
        //deposit
        else if ($so->isCash == 3) {
            
        }
        //down payment
        else {
            
        }
    }
}

function checkSalesAdd($idSO) {
    /*
      $salesOrderHeader = SalesOrderHeader::find($idSO);
      $tampJum = 0;
      $detailSales = $salesOrderHeader->SalesOrderDetail;
      foreach ($detailSales as $data) {
      $sumSalesReturn = SalesReturnHeader::getSumReturnOrder($data->InventoryInternalID, $data->SalesOrderInternalID, $data->InternalID);
      if ($sumSalesReturn == '') {
      $sumSalesReturn = '0';
      }
      $tampJum+= ($data->Qty) - SalesAddHeader::getSumSales($data->InventoryInternalID, $data->SalesOrderInternalID, $data->InternalID) + $sumSalesReturn;
      }
      if ($tampJum > 0) {
      return TRUE;
      }
      return FALSE;
     * 
     */



    $salesOrderHeader = SalesOrderHeader::find($idSO);
    $tampJum = 0;
    $detailSales = $salesOrderHeader->SalesOrderDetail;
    //sudah benar
    foreach ($detailSales as $data) {
        if ($data->SalesOrderParcelInternalID == 0) {
            $sumSalesReturn = SalesReturnHeader::getSumReturnOrder($data->InventoryInternalID, $data->SalesOrderInternalID, $data->InternalID);
            if ($sumSalesReturn == '') {
                $sumSalesReturn = '0';
            }
            $tampJum += ($data->Qty) - SalesAddHeader::getSumSales($data->InventoryInternalID, $data->SalesOrderInternalID, $data->InternalID) + $sumSalesReturn;
        }
    }
    //parcel
    foreach (SalesOrderParcel::where("SalesOrderInternalID", $salesOrderHeader->InternalID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->get() as $data) {
        $sumSalesReturn = SalesReturnHeader::getSumReturnOrderParcel($data->ParcelInternalID, $data->SalesOrderInternalID, $data->InternalID);
        if ($sumSalesReturn == '') {
            $sumSalesReturn = '0';
        }
        $tampJum += ($data->Qty) - SalesAddHeader::getSumSalesParcel($data->ParcelInternalID, $data->SalesOrderInternalID, $data->InternalID) + $sumSalesReturn;
    }
    if ($tampJum > 0) {
        return TRUE;
    }
    return FALSE;
}

function detailReportInventoryWarehouse($data, $warehouse, $bulan, $tahun) {
    if ($warehouse != -1) {
        $qtySales = ShippingAddHeader::qtyInventoryWarehouse($data->InternalID, $warehouse, $bulan, $tahun); //
        $qtySalesR = SalesReturnHeader::qtyInventoryWarehouse($data->InternalID, $warehouse, $bulan, $tahun);
        $qtyMemoOut = MemoOutHeader::qtyInventoryWarehouse($data->InternalID, $warehouse, $bulan, $tahun);
        $qtyPurchase = MrvHeader::qtyInventoryWarehouse($data->InternalID, $warehouse, $bulan, $tahun);
        $qtyPurchaseR = PurchaseReturnHeader::qtyInventoryWarehouse($data->InternalID, $warehouse, $bulan, $tahun);
        $qtyMemoIn = MemoInHeader::qtyInventoryWarehouse($data->InternalID, $warehouse, $bulan, $tahun);
//        $purchase = MrvHeader::valueInventoryWarehouse($data->InternalID, $warehouse, $bulan, $tahun);
        $purchase = PurchaseHeader::valueInventoryWarehouse($data->InternalID, $warehouse, $bulan, $tahun);
        if ($purchase == NULL) {
            $purchase = 0;
        }
        $purchaseR = PurchaseReturnHeader::valueInventoryWarehouse($data->InternalID, $warehouse, $bulan, $tahun);
        if ($purchaseR == NULL) {
            $purchaseR = 0;
        }
        $memoIn = MemoInHeader::valueInventoryWarehouse($data->InternalID, $warehouse, $bulan, $tahun);
        if ($memoIn == NULL) {
            $memoIn = 0;
        }
        $qtyTransformationIn = TransformationHeader::qtyInInventoryWarehouse($data->InternalID, $warehouse, $bulan, $tahun);
        $qtyTransformationOut = TransformationHeader::qtyOutInventoryWarehouse($data->InternalID, $warehouse, $bulan, $tahun);
        $transformationIn = TransformationHeader::valueInInventoryWarehouse($data->InternalID, $warehouse, $bulan, $tahun);
        $transformationOut = TransformationHeader::valueOutInventoryWarehouse($data->InternalID, $warehouse, $bulan, $tahun);
        $qtyTransferIn = TransferHeader::qtyInInventoryWarehouse($data->InternalID, $warehouse, $bulan, $tahun);
        ;
        $qtyTransferOut = TransferHeader::qtyOutInventoryWarehouse($data->InternalID, $warehouse, $bulan, $tahun);
        ;
    } else {
        $qtySales = ShippingAddHeader::qtyInventory($data->InternalID, $bulan, $tahun);
        $qtySalesR = SalesReturnHeader::qtyInventory($data->InternalID, $bulan, $tahun);
        $qtyMemoOut = MemoOutHeader::qtyInventory($data->InternalID, $bulan, $tahun);
        $qtyPurchase = MrvHeader::qtyInventory($data->InternalID, $bulan, $tahun);
//        $qtyPurchase = PurchaseHeader::qtyInventory($data->InternalID, $bulan, $tahun);
        $qtyPurchaseR = PurchaseReturnHeader::qtyInventory($data->InternalID, $bulan, $tahun);
        $qtyMemoIn = MemoInHeader::qtyInventory($data->InternalID, $bulan, $tahun);
        $purchase = PurchaseHeader::valueInventory($data->InternalID, $bulan, $tahun);
        if ($purchase == NULL) {
            $purchase = 0;
        }
        $purchaseR = PurchaseReturnHeader::valueInventory($data->InternalID, $bulan, $tahun);
        if ($purchaseR == NULL) {
            $purchaseR = 0;
        }
        $memoIn = MemoInHeader::valueInventory($data->InternalID, $bulan, $tahun);
        if ($memoIn == NULL) {
            $memoIn = 0;
        }
        $qtyTransformationIn = TransformationHeader::qtyInInventory($data->InternalID, $bulan, $tahun);
        $qtyTransformationOut = TransformationHeader::qtyOutInventory($data->InternalID, $bulan, $tahun);
        $transformationIn = TransformationHeader::valueInInventory($data->InternalID, $bulan, $tahun);
        $transformationOut = TransformationHeader::valueOutInventory($data->InternalID, $bulan, $tahun);
        $qtyTransferIn = 0;
        $qtyTransferOut = 0;
    }

    $valueBefore = InventoryValue::valueInventoryBefore($data->InternalID, $bulan, $tahun);
    $qtyBefore = InventoryValue::qtyInventoryBefore($data->InternalID, $bulan, $tahun);

    $dataInitialQuantity = $data->InitialQuantity;
    $dataInitialValue = $data->InitialValue;
    if (InventoryValue::where('InventoryInternalID', $data->InternalID)
                    ->orderBy("Year", 'desc')
                    ->orderBy("Month", 'desc')
                    ->whereRaw("CONCAT(Year,'-',Month,'-31') < " . $tahun . '-' . $bulan . '-01')
                    ->count() > 0) {
        $dataInitialQuantity = 0;
        $dataInitialValue = 0;
    }
    //belum trmasuk transformation dan transfer?
    $qtyDividen = $dataInitialQuantity + $qtyBefore + $qtyPurchase - $qtyPurchaseR + $qtyMemoIn;
    if ($qtyDividen == 0) {
        $average = 0;
    } else {
        $average = (($dataInitialValue * $dataInitialQuantity) + ($valueBefore * $qtyBefore) + $purchase - $purchaseR + $memoIn) / $qtyDividen;
    }

    if ($dataInitialValue > 0) {
        $InitialValue = $dataInitialValue * $dataInitialQuantity;
        $InitialQty = $dataInitialQuantity;
    } else {
        $InitialValue = $valueBefore * $qtyBefore;
        $InitialQty = $qtyBefore;
    }
    $SalesValue = $average * $qtySales;
    $RSalesValue = $average * $qtySalesR;
    $MemoOutValue = $average * $qtyMemoOut;
    $endStock = $InitialQty + $qtyPurchase + $qtySalesR - $qtySales - $qtyPurchaseR + $qtyMemoIn - $qtyMemoOut;
    $endValue = $InitialValue + $purchase + $RSalesValue - $SalesValue - $purchaseR + $memoIn - $MemoOutValue;

    $data = $InitialQty . '^_^' . $qtyPurchase . '^_^' . $qtySalesR . '^_^' . $qtySales . '^_^' . $qtyPurchaseR . '^_^' . $qtyMemoIn . '^_^' . $qtyMemoOut . '^_^' . $endStock .
            '^_^' . $InitialValue . '^_^' . $purchase . '^_^' . $RSalesValue . '^_^' . $SalesValue . '^_^' . $purchaseR . '^_^' . $memoIn . '^_^' . $MemoOutValue . '^_^' . $endValue
            . '^_^' . $qtyTransformationIn . '^_^' . $transformationIn . '^_^' . $qtyTransformationOut . '^_^' . $transformationOut . '^_^' . $qtyTransferOut . '^_^' . $qtyTransferIn
    ;
    return $data;
}

function checkShippingAdd($idSO) {

    $salesOrderHeader = SalesOrderHeader::find($idSO);
    $tampJum = 0;
    $detailSales = $salesOrderHeader->SalesOrderDetail;
    //sudah benar
    foreach ($detailSales as $data) {
        if ($data->SalesOrderParcelInternalID == 0) {
            $tampJum += ($data->Qty) - ShippingAddHeader::getSumShipping($data->InventoryInternalID, $data->SalesOrderInternalID, $data->InternalID);
        }
    }
    //parcel
    foreach (SalesOrderParcel::where("SalesOrderInternalID", $salesOrderHeader->InternalID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->get() as $data) {
        $tampJum += ($data->Qty) - ShippingAddHeader::getSumShippingParcel($data->ParcelInternalID, $data->SalesOrderInternalID, $data->InternalID);
    }
//    return $tampJum;
    if ($tampJum > 0) {
        return TRUE;
    }
    return FALSE;
}

function checkSalesReturn($idSI) {
//    $salesHeader = SalesHeader::find($idSI);
//    $tampJum = 0;
//    $detailReturn = $salesHeader->SalesDetail;
//    foreach ($detailReturn as $data) {
//        $tampJum+= ($data->Qty) - SalesReturnHeader::getSumReturn($data->InventoryInternalID, $salesHeader->SalesID, $data->InternalID);
//    }
//    if ($tampJum > 0) {
//        return TRUE;
//    }
//    return FALSE;
    $salesHeader = SalesHeader::find($idSI);
    $tampJum = 0;
    $detailReturn = $salesHeader->SalesDetail;
    foreach ($detailReturn as $data) {
        if ($data->SalesParcelInternalID == 0) {
            $tampJum += ($data->Qty) - SalesReturnHeader::getSumReturn($data->InventoryInternalID, $salesHeader->SalesID, $data->InternalID);
        }
    }
    foreach (SalesAddParcel::where("SalesInternalID", $salesHeader->InternalID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->get() as $data) {
        $tampJum += ($data->Qty) - SalesReturnHeader::getSumReturnParcel($data->ParcelInternalID, $salesHeader->SalesHeaderID, $data->InternalID);
    }
    if ($tampJum > 0) {
        return TRUE;
    }
    return FALSE;
}

//ganti dari mrv bukan PO
function checkPurchaseAdd($idPO) {
    $mrvHeader = MrvHeader::find($idPO);
    $tampJum = 0;
    $detailPurchase = $mrvHeader->MrvDetail;
    foreach ($detailPurchase as $data) {
        $sumPurchaseReturn = PurchaseReturnHeader::getSumReturnMrv($data->InventoryInternalID, $data->MrvInternalID, $data->InternalID);
        if ($sumPurchaseReturn == '') {
            $sumPurchaseReturn = '0';
        }
        $tampJum += ($data->Qty) - PurchaseAddHeader::getSum($data->InventoryInternalID, $data->MrvInternalID, $data->InternalID) + $sumPurchaseReturn;
    }
    if ($tampJum > 0) {
        return TRUE;
    }
    return FALSE;
}

function checkMrv($idPO) {
    $purchaseOrderHeader = PurchaseOrderHeader::find($idPO);
    $tampJum = 0;
    $detailPurchase = $purchaseOrderHeader->PurchaseOrderDetail;
    foreach ($detailPurchase as $data) {
        $tampJum += ($data->Qty) - MrvHeader::getSum($data->InventoryInternalID, $data->PurchaseOrderInternalID, $data->InternalID);
    }
    if ($tampJum > 0) {
        return TRUE;
    }
    return FALSE;
}

function checkQuotation($idQuotation) {
//jika quotationnya sudah ada di tabel mn maka tidak keluar di pilihan
    $tampJum = 0;
    $countQuotationSales = QuotationSales::where("QuotationInternalID", $idQuotation)->count();
    $tampJum = $countQuotationSales;
    if ($tampJum == 0) {
        return TRUE;
    }
    return FALSE;
}

function checkSalesOrder($idSO) {
//jika sales order udah ada di table mn maka tidak akan keluar di pilihan
    $tampJum = 0;
    $countSoTransfer = TransferOrder::where("SalesOrderInternalID", $idSO)->count();
    $tampJum = $countSoTransfer;
    if ($tampJum == 0) {
        return TRUE;
    }
    return FALSE;
}

function checkPurchaseReturn($idPI) {
    $purchaseHeader = PurchaseHeader::find($idPI);
    $tampJum = 0;
    $detailReturn = $purchaseHeader->PurchaseDetail;
    foreach ($detailReturn as $data) {
        $tampJum += ($data->Qty) - PurchaseReturnHeader::getSumReturn($data->InventoryInternalID, $purchaseHeader->PurchaseID, $data->InternalID);
    }
    if ($tampJum > 0) {
        return TRUE;
    }
    return FALSE;
}

function countMemorySuperAdmin($company) {
    $purchaseO = PurchaseOrderHeader::where('CompanyInternalID', $company)->count();
    $purchaseI = PurchaseHeader::where('CompanyInternalID', $company)->count();
    $purchaseR = PurchaseReturnHeader::where('CompanyInternalID', $company)->count();
    $salesO = SalesOrderHeader::where('CompanyInternalID', $company)->count();
    $salesI = SalesHeader::where('CompanyInternalID', $company)->count();
    $salesR = SalesReturnHeader::where('CompanyInternalID', $company)->count();
    $quotation = QuotationHeader::where('CompanyInternalID', $company)->count();
    $shipping = ShippingAddHeader::where('CompanyInternalID', $company)->count();
//Transaction
    $totalTransaction = $purchaseO + $purchaseI + $purchaseR + $salesO + $salesI + $salesR + $quotation + $shipping;
    $totalTransaction *= 20;

    $user = User::where('CompanyInternalID', $company)->where('Status', 1)->count();
//user
    $totalUser = $user * 10000;
//Total
    $inventory = Inventory::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
    $totalInventory = $inventory * 20;
    $total = ($totalTransaction + $totalUser + $totalInventory) / 1000;

    return $total . 'Mb';
}

function countMemory() {
    $purchaseO = PurchaseOrderHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
    $purchaseI = PurchaseHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
    $purchaseR = PurchaseReturnHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
    $salesO = SalesOrderHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
    $salesI = SalesHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
    $salesR = SalesReturnHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
    $quotation = QuotationHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
    $shipping = ShippingAddHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
//Transaction
    $totalTransaction = $purchaseO + $purchaseI + $purchaseR + $salesO + $salesI + $salesR + $quotation + $shipping;
    $totalTransaction *= 20;

    $user = User::where('CompanyInternalID', Auth::user()->CompanyInternalID)->where('Status', 1)->count();
//user
    $totalUser = $user * 10000;

    $inventory = Inventory::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count();
    $totalInventory = $inventory * 20;
//Total
    $total = ($totalTransaction + $totalUser + $totalInventory) / 1000;

    return $total;
}

function checkModul($idModul) {
    $modul = Modul::where('ModulID', $idModul)->pluck('InternalID');
    if (!Auth::check()) {
        return false;
    }
    $package = Auth::user()->Company->PackageInternalID;
    $result = PackageDetail::where('PackageInternalID', $package)
                    ->where('ModulInternalID', $modul)->count();
    if ($result == 0) {
        return false;
    }
    return true;
}

function checkMatrix($idMatrix) {
    $matrix = Matrix::where('MatrixID', $idMatrix)->pluck('InternalID');
    $result = UserDetail::where('UserInternalID', Auth::user()->InternalID)
                    ->where('MatrixInternalID', $matrix)->count();
    if ($result == 0) {
        return false;
    }
    return true;
}

function checkTypeMatrix($type) {
    $result = UserDetail::join('m_matrix', 'm_matrix.InternalID', '=', 'MatrixInternalID')
                    ->where('UserInternalID', Auth::user()->InternalID)
                    ->where('Type', $type)->count();
    if ($result == 0) {
        return false;
    }
    return true;
}

function myEncryptEmail($text) {
    $text = str_replace("=", "EEE93PPP", $text);
    $text = str_replace("?", "CCC45TTT", $text);
    $text = str_replace("/", "EEE46CCC", $text);
    return $text;
}

function myDecryptEmail($text) {
    $text = str_replace("EEE93PPP", "=", $text);
    $text = str_replace("CCC45TTT", "?", $text);
    $text = str_replace("EEE46CCC", "/", $text);
    return $text;
}

function autoDetailID($id) {
    $id = Coa6::find($id)->ACC6ID;
    $detailID = CustomerDetail::orderBy('InternalID', 'desc')
            ->where('CustomerDetailID', 'LIKE', $id . '-%')
            ->where('CompanyInternalID', Auth::user()->Company->InternalID)
            ->first();
    if (count($detailID) > 0) {
        $detailID = $detailID->CustomerDetailID;
    } else {
        $detailID = 000;
    }
    $idSecond = substr($detailID, -3) + 1;
    $idSecond = str_pad($idSecond, 3, '0', STR_PAD_LEFT);
    $idDetail = $id . "-" . $idSecond;

    return $idDetail;
}

function findAlfaNumeric($cari, $arr, $rand) {
    $ke = -1;
    for ($i = 0; $i < count($arr); $i++) {
        if ($cari == $arr[$i]) {
            $ke = $i;
        }
    }
    if ($ke == -1) {
        return '-1';
    }
    $index = 0;
    if (($ke + $rand) > 75) {
        $index = ($ke + $rand) - 76;
    } else {
        $index = $ke + $rand;
    }
    return $index;
}

function myEncryptJavaScript($text, $rand) {
// $text berupa array
    $arr = array(
        'h', '!', '3', 'z', 'a', 'g', '8', '%', '9', 'k',
        'y', '@', 'b', 'f', '-', 'o', 'v', 'q', 'd', '7',
        '0', 'i', '^', '6', '#', '5', 'c', 'j', '*', 'e',
        '&', '(', 'm', 'l', '4', ')', '=', 'p', 'u', '_',
        's', '2', 't', '+', 'r', '$', 'x', 'w', 'n', '1',
        'H', 'Z', 'A', 'G', 'K', 'Y', 'B', 'F', 'O', 'V',
        'Q', 'D', 'I', 'C', 'J', 'E', 'M', 'L', 'P', 'U',
        'S', 'T', 'R', 'X', 'W', 'N'
    );
    $hasil = "";
    for ($i = 0; $i < count($text); $i++) {
        if ($i < (count($text) - 1)) {
            $hasil .= $text[$i] . ',';
        } else {
            $hasil .= $text[$i];
        }
    }

    $kal = "";
    for ($i = 0; $i < strlen($hasil); $i++) {
        $ke = findAlfaNumeric($hasil[$i], $arr, $rand);
        if ($ke != '-1') {
            $kal .= $arr[$ke];
        } else {
            $kal .= $hasil[$i];
        }
    }
    return '[' . $kal . ']';
}

function myEncryptJavaScriptText($text, $rand) {
// $text berupa string
    $arr = array(
        'h', '!', '3', 'z', 'a', 'g', '8', '%', '9', 'k',
        'y', '@', 'b', 'f', '-', 'o', 'v', 'q', 'd', '7',
        '0', 'i', '^', '6', '#', '5', 'c', 'j', '*', 'e',
        '&', '(', 'm', 'l', '4', ')', '=', 'p', 'u', '_',
        's', '2', 't', '+', 'r', '$', 'x', 'w', 'n', '1',
        'H', 'Z', 'A', 'G', 'K', 'Y', 'B', 'F', 'O', 'V',
        'Q', 'D', 'I', 'C', 'J', 'E', 'M', 'L', 'P', 'U',
        'S', 'T', 'R', 'X', 'W', 'N'
    );

    $kal = "";
    for ($i = 0; $i < strlen($text); $i++) {
        $ke = findAlfaNumeric($text[$i], $arr, $rand);
        if ($ke != '-1') {
            $kal .= $arr[$ke];
        } else {
            $kal .= $text[$i];
        }
    }
    return $kal;
}

function splitSearchValue($text) {
    $tamp = "%";
    for ($i = 0; $i < strlen($text); $i++) {
        if ($text[$i] == " ")
            $tamp .= "%";
        else
            $tamp .= $text[$i];
    }
    return $tamp . "%";
}

function getEndStockInventory($InventoryInternalID) {
    $html = "";
    $totalPieces = 0;
    $tampUom = '';
    $initialStockTamp = Inventory::find($InventoryInternalID)->InitialQuantity;
    foreach (InventoryUom::where('InventoryInternalID', $InventoryInternalID)->get() as $data) {
        $pembelian = MrvDetail::
                join('t_mrv_header', 't_mrv_header.InternalID', '=', 't_mrv_detail.MrvInternalID')
                ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_mrv_header.WarehouseInternalID')
                ->where("m_warehouse.Type", Auth::user()->WarehouseCheck)
                ->where('t_mrv_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_mrv_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_mrv_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
//        $pembelian = PurchaseDetail::
//                join('t_purchase_header', 't_purchase_header.InternalID', '=', 't_purchase_detail.PurchaseInternalID')
//                ->where('t_purchase_header.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->where('t_purchase_detail.InventoryInternalID', $data->InventoryInternalID)
//                ->where('t_purchase_detail.UomInternalID', $data->UomInternalID)
//                ->sum('Qty');
//        $penjualan = SalesDetail::
//                join('t_sales_header', 't_sales_header.InternalID', '=', 't_sales_detail.SalesInternalID')
//                ->where('t_sales_header.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->where('t_sales_detail.InventoryInternalID', $data->InventoryInternalID)
//                ->where('t_sales_detail.UomInternalID', $data->UomInternalID)
//                ->sum('Qty');
        $penjualan = ShippingAddDetail::
                join('t_shipping_header', 't_shipping_header.InternalID', '=', 't_shipping_detail.ShippingInternalID')
                ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_shipping_header.WarehouseInternalID')
                ->where("m_warehouse.Type", Auth::user()->WarehouseCheck)
                ->where('t_shipping_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_shipping_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_shipping_detail.UomInternalID', $data->UomInternalID)
                ->where("t_shipping_header.VAT",1)
                ->sum('Qty');
        $Rpembelian = PurchaseReturnDetail::
                join('t_purchasereturn_header', 't_purchasereturn_header.InternalID', '=', 't_purchasereturn_detail.PurchaseReturnInternalID')
                ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_purchasereturn_header.WarehouseInternalID')
                ->where("m_warehouse.Type", Auth::user()->WarehouseCheck)
                ->where('t_purchasereturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_purchasereturn_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_purchasereturn_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Rpenjualan = SalesReturnDetail::
                join('t_salesreturn_header', 't_salesreturn_header.InternalID', '=', 't_salesreturn_detail.SalesReturnInternalID')
                ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_salesreturn_header.WarehouseInternalID')
                ->where("m_warehouse.Type", Auth::user()->WarehouseCheck)
                ->where('t_salesreturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_salesreturn_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_salesreturn_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Min = MemoInDetail::
                join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.MemoInInternalID')
                ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_memoin_header.WarehouseInternalID')
                ->where("m_warehouse.Type", Auth::user()->WarehouseCheck)
                ->where('t_memoin_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_memoin_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_memoin_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Mout = MemoOutDetail::
                join('t_memoout_header', 't_memoout_header.InternalID', '=', 't_memoout_detail.MemoOutInternalID')
                ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_memoout_header.WarehouseInternalID')
                ->where("m_warehouse.Type", Auth::user()->WarehouseCheck)
                ->where('t_memoout_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_memoout_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_memoout_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Tin = TransferDetail::
                join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_transfer_header.WarehouseInternalID')
                ->where("m_warehouse.Type", Auth::user()->WarehouseCheck)
                ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transfer_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transfer_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Tout = TransferDetail::
                join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_transfer_header.WarehouseInternalID')
                ->where("m_warehouse.Type", Auth::user()->WarehouseCheck)
                ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transfer_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transfer_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Trin = TransformationDetail::
                join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_transformation_detail.WarehouseInternalID')
                ->where("m_warehouse.Type", Auth::user()->WarehouseCheck)
                ->where('t_transformation_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transformation_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transformation_detail.UomInternalID', $data->UomInternalID)
                ->where('t_transformation_detail.Type', "in")
                ->sum('Qty');
        $Trout = TransformationDetail::
                join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_transformation_detail.WarehouseInternalID')
                ->where("m_warehouse.Type", Auth::user()->WarehouseCheck)
                ->where('t_transformation_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transformation_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transformation_detail.UomInternalID', $data->UomInternalID)
                ->where('t_transformation_detail.Type', "out")
                ->sum('Qty');
        $ConvIn = Convertion::
                join('m_inventory_uom', 'm_inventory_uom.InternalID', '=', 'h_convertion.InventoryUomInternalID2')
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('h_convertion.InventoryUomInternalID2', $data->InternalID)
                ->sum('QuantityResult');
        $ConvOut = Convertion::
                join('m_inventory_uom', 'm_inventory_uom.InternalID', '=', 'h_convertion.InventoryUomInternalID1')
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('h_convertion.InventoryUomInternalID1', $data->InternalID)
                ->sum('Quantity');

        $initialStock = $initialStockTamp;
        if ($data->Default == 0) {
            $initialStock = 0;
        }
        $endStock = $pembelian + $Rpenjualan - $penjualan - $Rpembelian + $Min - $Mout + $Tin - $Tout + $Trin - $Trout + $ConvIn - $ConvOut;
        $totalPieces += $endStock * $data->Value;
    }
    $totalPieces += $initialStock;
    $UomInternalID = InventoryUom::where("CompanyInternalID", Auth::user()->Company->InternalID)->where("InventoryInternalID", $InventoryInternalID)->where("Default", 1)->first()->UomInternalID;
    $tampUom = Uom::find($UomInternalID)->UomID;
    return $totalPieces . ' ' . $tampUom;
}

function getInitialStockInventoryWithWarehouse($InventoryInternalID, $ware, $date) {
    $html = "";
    $totalPieces = 0;
    $tampUom = '';
    $initialStockTamp = Inventory::find($InventoryInternalID)->InitialQuantity;
    foreach (InventoryUom::where('InventoryInternalID', $InventoryInternalID)->get() as $data) {
        $pembelian = MrvDetail::
                join('t_mrv_header', 't_mrv_header.InternalID', '=', 't_mrv_detail.MrvInternalID')
                ->where('t_mrv_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_mrv_header.WarehouseInternalID', $ware)
                ->where('t_mrv_header.MrvDate', '<', $date)
                ->where('t_mrv_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_mrv_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
//        $pembelian = PurchaseDetail::
//                join('t_purchase_header', 't_purchase_header.InternalID', '=', 't_purchase_detail.PurchaseInternalID')
//                ->where('t_purchase_header.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->where('t_purchase_header.WarehouseInternalID', $ware)
//                ->where('t_purchase_detail.InventoryInternalID', $data->InventoryInternalID)
//                ->where('t_purchase_detail.UomInternalID', $data->UomInternalID)
//                ->sum('Qty');
//        $penjualan = SalesDetail::
//                join('t_sales_header', 't_sales_header.InternalID', '=', 't_sales_detail.SalesInternalID')
//                ->where('t_sales_header.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->where('t_sales_detail.InventoryInternalID', $data->InventoryInternalID)
//                ->where('t_sales_detail.UomInternalID', $data->UomInternalID)
//                ->sum('Qty');
        $penjualan = ShippingAddDetail::
                join('t_shipping_header', 't_shipping_header.InternalID', '=', 't_shipping_detail.ShippingInternalID')
                ->where('t_shipping_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_shipping_header.WarehouseInternalID', $ware)
                ->where('t_shipping_header.ShippingDate', '<', $date)
                ->where('t_shipping_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_shipping_detail.UomInternalID', $data->UomInternalID)
                ->where("t_shipping_header.VAT",1)
                ->sum('Qty');
        $Rpembelian = PurchaseReturnDetail::
                join('t_purchasereturn_header', 't_purchasereturn_header.InternalID', '=', 't_purchasereturn_detail.PurchaseReturnInternalID')
                ->where('t_purchasereturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_purchasereturn_header.WarehouseInternalID', $ware)
                ->where('t_purchasereturn_header.PurchaseReturnDate', '<', $date)
                ->where('t_purchasereturn_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_purchasereturn_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Rpenjualan = SalesReturnDetail::
                join('t_salesreturn_header', 't_salesreturn_header.InternalID', '=', 't_salesreturn_detail.SalesReturnInternalID')
                ->where('t_salesreturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_salesreturn_header.WarehouseInternalID', $ware)
                ->where('t_salesreturn_header.SalesReturnDate', '<', $date)
                ->where('t_salesreturn_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_salesreturn_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Min = MemoInDetail::
                join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.MemoInInternalID')
                ->where('t_memoin_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_memoin_header.WarehouseInternalID', $ware)
                ->where('t_memoin_header.MemoInDate', '<', $date)
                ->where('t_memoin_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_memoin_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Mout = MemoOutDetail::
                join('t_memoout_header', 't_memoout_header.InternalID', '=', 't_memoout_detail.MemoOutInternalID')
                ->where('t_memoout_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_memoout_header.WarehouseInternalID', $ware)
                ->where('t_memoout_header.MemoOutDate', '<', $date)
                ->where('t_memoout_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_memoout_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Tin = TransferDetail::
                join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transfer_header.WarehouseDestinyInternalID', $ware)
                ->where('t_transfer_header.TransferDate', '<', $date)
                ->where('t_transfer_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transfer_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Tout = TransferDetail::
                join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transfer_header.WarehouseInternalID', $ware)
                ->where('t_transfer_header.TransferDate', '<', $date)
                ->where('t_transfer_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transfer_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Trin = TransformationDetail::
                join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                ->where('t_transformation_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transformation_detail.WarehouseInternalID', $ware)
                ->where('t_transformation_header.TransformationDate', '<', $date)
                ->where('t_transformation_detail.Type', "in")
                ->where('t_transformation_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transformation_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Trout = TransformationDetail::
                join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                ->where('t_transformation_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transformation_detail.WarehouseInternalID', $ware)
                ->where('t_transformation_header.TransformationDate', '<', $date)
                ->where('t_transformation_detail.Type', "out")
                ->where('t_transformation_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transformation_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $ConvIn = Convertion::
                join('m_inventory_uom', 'm_inventory_uom.InternalID', '=', 'h_convertion.InventoryUomInternalID2')
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('h_convertion.WarehouseInternalID', $ware)
                ->where('h_convertion.InventoryUomInternalID2', $data->InternalID)
                ->sum('QuantityResult');
        $ConvOut = Convertion::
                join('m_inventory_uom', 'm_inventory_uom.InternalID', '=', 'h_convertion.InventoryUomInternalID1')
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('h_convertion.WarehouseInternalID', $ware)
                ->where('h_convertion.InventoryUomInternalID1', $data->InternalID)
                ->sum('Quantity');

        $initialStock = $initialStockTamp;
        if ($data->Default == 0) {
            $initialStock = 0;
        }
        $endStock = $initialStock + $pembelian + $Rpenjualan - $penjualan - $Rpembelian + $Min - $Mout + $Tin - $Tout + $Trin - $Trout + $ConvIn - $ConvOut;
        $totalPieces += $endStock * $data->Value;
//        return $ConvOut;
    }
//    $totalPieces += $initialStock ;
    $UomInternalID = InventoryUom::where("CompanyInternalID", Auth::user()->Company->InternalID)->where("InventoryInternalID", $InventoryInternalID)->where("Default", 1)->first()->UomInternalID;
    $tampUom = Uom::find($UomInternalID)->UomID;
    return $totalPieces;
//    return $totalPieces . ' ' . $tampUom;
}

function getInitialStockInventory($InventoryInternalID, $date) {
    $html = "";
    $totalPieces = 0;
    $tampUom = '';
    $initialStockTamp = Inventory::find($InventoryInternalID)->InitialQuantity;
    foreach (InventoryUom::where('InventoryInternalID', $InventoryInternalID)->get() as $data) {
        $pembelian = MrvDetail::
                join('t_mrv_header', 't_mrv_header.InternalID', '=', 't_mrv_detail.MrvInternalID')
                ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_mrv_header.WarehouseInternalID')
                ->where('m_warehouse.Type', Auth::user()->WarehouseCheck)
                ->where('t_mrv_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_mrv_header.MrvDate', '<', $date)
                ->where('t_mrv_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_mrv_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
//        $pembelian = PurchaseDetail::
//                join('t_purchase_header', 't_purchase_header.InternalID', '=', 't_purchase_detail.PurchaseInternalID')
//                ->where('t_purchase_header.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->where('t_purchase_header.WarehouseInternalID', $ware)
//                ->where('t_purchase_detail.InventoryInternalID', $data->InventoryInternalID)
//                ->where('t_purchase_detail.UomInternalID', $data->UomInternalID)
//                ->sum('Qty');
//        $penjualan = SalesDetail::
//                join('t_sales_header', 't_sales_header.InternalID', '=', 't_sales_detail.SalesInternalID')
//                ->where('t_sales_header.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->where('t_sales_detail.InventoryInternalID', $data->InventoryInternalID)
//                ->where('t_sales_detail.UomInternalID', $data->UomInternalID)
//                ->sum('Qty');
        $penjualan = ShippingAddDetail::
                join('t_shipping_header', 't_shipping_header.InternalID', '=', 't_shipping_detail.ShippingInternalID')
                ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_shipping_header.WarehouseInternalID')
                ->where('m_warehouse.Type', Auth::user()->WarehouseCheck)
                ->where('t_shipping_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_shipping_header.ShippingDate', '<', $date)
                ->where('t_shipping_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_shipping_detail.UomInternalID', $data->UomInternalID)
                ->where("t_shipping_header.VAT",1)
                ->sum('Qty');
        $Rpembelian = PurchaseReturnDetail::
                join('t_purchasereturn_header', 't_purchasereturn_header.InternalID', '=', 't_purchasereturn_detail.PurchaseReturnInternalID')
                ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_purchasereturn_header.WarehouseInternalID')
                ->where('m_warehouse.Type', Auth::user()->WarehouseCheck)
                ->where('t_purchasereturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_purchasereturn_header.PurchaseReturnDate', '<', $date)
                ->where('t_purchasereturn_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_purchasereturn_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Rpenjualan = SalesReturnDetail::
                join('t_salesreturn_header', 't_salesreturn_header.InternalID', '=', 't_salesreturn_detail.SalesReturnInternalID')
                ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_salesreturn_header.WarehouseInternalID')
                ->where('m_warehouse.Type', Auth::user()->WarehouseCheck)
                ->where('t_salesreturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_salesreturn_header.SalesReturnDate', '<', $date)
                ->where('t_salesreturn_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_salesreturn_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Min = MemoInDetail::
                join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.MemoInInternalID')
                ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_memoin_header.WarehouseInternalID')
                ->where('m_warehouse.Type', Auth::user()->WarehouseCheck)
                ->where('t_memoin_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_memoin_header.MemoInDate', '<', $date)
                ->where('t_memoin_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_memoin_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Mout = MemoOutDetail::
                join('t_memoout_header', 't_memoout_header.InternalID', '=', 't_memoout_detail.MemoOutInternalID')
                ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_memoout_header.WarehouseInternalID')
                ->where('m_warehouse.Type', Auth::user()->WarehouseCheck)
                ->where('t_memoout_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_memoout_header.MemoOutDate', '<', $date)
                ->where('t_memoout_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_memoout_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Tin = TransferDetail::
                join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_transfer_header.WarehouseInternalID')
                ->where('m_warehouse.Type', Auth::user()->WarehouseCheck)
                ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transfer_header.TransferDate', '<', $date)
                ->where('t_transfer_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transfer_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Tout = TransferDetail::
                join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_transfer_header.WarehouseInternalID')
                ->where('m_warehouse.Type', Auth::user()->WarehouseCheck)
                ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transfer_header.TransferDate', '<', $date)
                ->where('t_transfer_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transfer_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Trin = TransformationDetail::
                join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_transformation_detail.WarehouseInternalID')
                ->where('m_warehouse.Type', Auth::user()->WarehouseCheck)
                ->where('t_transformation_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transformation_header.TransformationDate', '<', $date)
                ->where('t_transformation_detail.Type', "in")
                ->where('t_transformation_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transformation_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Trout = TransformationDetail::
                join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_transformation_detail.WarehouseInternalID')
                ->where('m_warehouse.Type', Auth::user()->WarehouseCheck)
                ->where('t_transformation_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transformation_header.TransformationDate', '<', $date)
                ->where('t_transformation_detail.Type', "out")
                ->where('t_transformation_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transformation_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $ConvIn = Convertion::
                join('m_inventory_uom', 'm_inventory_uom.InternalID', '=', 'h_convertion.InventoryUomInternalID2')
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('h_convertion.InventoryUomInternalID2', $data->InternalID)
                ->sum('QuantityResult');
        $ConvOut = Convertion::
                join('m_inventory_uom', 'm_inventory_uom.InternalID', '=', 'h_convertion.InventoryUomInternalID1')
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('h_convertion.InventoryUomInternalID1', $data->InternalID)
                ->sum('Quantity');

        $initialStock = $initialStockTamp;
        if ($data->Default == 0) {
            $initialStock = 0;
        }
        $endStock = $initialStock + $pembelian + $Rpenjualan - $penjualan - $Rpembelian + $Min - $Mout + $Tin - $Tout + $Trin - $Trout + $ConvIn - $ConvOut;
        $totalPieces += $endStock * $data->Value;
//        return $ConvOut;
    }
//    $totalPieces += $initialStock ;
    $UomInternalID = InventoryUom::where("CompanyInternalID", Auth::user()->Company->InternalID)->where("InventoryInternalID", $InventoryInternalID)->where("Default", 1)->first()->UomInternalID;
    $tampUom = Uom::find($UomInternalID)->UomID;
    return $totalPieces;
//    return $totalPieces . ' ' . $tampUom;
}

function getEndStockInventoryWithWarehouse($InventoryInternalID, $ware) {
    $html = "";
    $totalPieces = 0;
    $tampUom = '';
    $initialStockTamp = Inventory::find($InventoryInternalID)->InitialQuantity;
    foreach (InventoryUom::where('InventoryInternalID', $InventoryInternalID)->get() as $data) {
        $pembelian = MrvDetail::
                join('t_mrv_header', 't_mrv_header.InternalID', '=', 't_mrv_detail.MrvInternalID')
                ->where('t_mrv_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_mrv_header.WarehouseInternalID', $ware)
                ->where('t_mrv_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_mrv_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
//        $pembelian = PurchaseDetail::
//                join('t_purchase_header', 't_purchase_header.InternalID', '=', 't_purchase_detail.PurchaseInternalID')
//                ->where('t_purchase_header.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->where('t_purchase_header.WarehouseInternalID', $ware)
//                ->where('t_purchase_detail.InventoryInternalID', $data->InventoryInternalID)
//                ->where('t_purchase_detail.UomInternalID', $data->UomInternalID)
//                ->sum('Qty');
//        $penjualan = SalesDetail::
//                join('t_sales_header', 't_sales_header.InternalID', '=', 't_sales_detail.SalesInternalID')
//                ->where('t_sales_header.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->where('t_sales_detail.InventoryInternalID', $data->InventoryInternalID)
//                ->where('t_sales_detail.UomInternalID', $data->UomInternalID)
//                ->sum('Qty');
        $penjualan = ShippingAddDetail::
                join('t_shipping_header', 't_shipping_header.InternalID', '=', 't_shipping_detail.ShippingInternalID')
                ->where('t_shipping_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_shipping_header.WarehouseInternalID', $ware)
                ->where('t_shipping_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_shipping_detail.UomInternalID', $data->UomInternalID)
                ->where("t_shipping_header.VAT",1)
                ->sum('Qty');
        $Rpembelian = PurchaseReturnDetail::
                join('t_purchasereturn_header', 't_purchasereturn_header.InternalID', '=', 't_purchasereturn_detail.PurchaseReturnInternalID')
                ->where('t_purchasereturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_purchasereturn_header.WarehouseInternalID', $ware)
                ->where('t_purchasereturn_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_purchasereturn_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Rpenjualan = SalesReturnDetail::
                join('t_salesreturn_header', 't_salesreturn_header.InternalID', '=', 't_salesreturn_detail.SalesReturnInternalID')
                ->where('t_salesreturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_salesreturn_header.WarehouseInternalID', $ware)
                ->where('t_salesreturn_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_salesreturn_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Min = MemoInDetail::
                join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.MemoInInternalID')
                ->where('t_memoin_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_memoin_header.WarehouseInternalID', $ware)
                ->where('t_memoin_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_memoin_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Mout = MemoOutDetail::
                join('t_memoout_header', 't_memoout_header.InternalID', '=', 't_memoout_detail.MemoOutInternalID')
                ->where('t_memoout_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_memoout_header.WarehouseInternalID', $ware)
                ->where('t_memoout_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_memoout_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Tin = TransferDetail::
                join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transfer_header.WarehouseDestinyInternalID', $ware)
                ->where('t_transfer_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transfer_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Tout = TransferDetail::
                join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transfer_header.WarehouseInternalID', $ware)
                ->where('t_transfer_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transfer_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Trin = TransformationDetail::
                join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                ->where('t_transformation_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transformation_detail.WarehouseInternalID', $ware)
                ->where('t_transformation_detail.Type', "in")
                ->where('t_transformation_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transformation_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Trout = TransformationDetail::
                join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                ->where('t_transformation_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transformation_detail.WarehouseInternalID', $ware)
                ->where('t_transformation_detail.Type', "out")
                ->where('t_transformation_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transformation_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $ConvIn = Convertion::
                join('m_inventory_uom', 'm_inventory_uom.InternalID', '=', 'h_convertion.InventoryUomInternalID2')
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('h_convertion.WarehouseInternalID', $ware)
                ->where('h_convertion.InventoryUomInternalID2', $data->InternalID)
                ->sum('QuantityResult');
        $ConvOut = Convertion::
                join('m_inventory_uom', 'm_inventory_uom.InternalID', '=', 'h_convertion.InventoryUomInternalID1')
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('h_convertion.WarehouseInternalID', $ware)
                ->where('h_convertion.InventoryUomInternalID1', $data->InternalID)
                ->sum('Quantity');

        $initialStock = $initialStockTamp;
        if ($data->Default == 0) {
            $initialStock = 0;
        }
        $endStock = $initialStock + $pembelian + $Rpenjualan - $penjualan - $Rpembelian + $Min - $Mout + $Tin - $Tout + $Trin - $Trout + $ConvIn - $ConvOut;
        $totalPieces += $endStock * $data->Value;
//        return $ConvOut;
    }
//    $totalPieces += $initialStock ;
    $UomInternalID = InventoryUom::where("CompanyInternalID", Auth::user()->Company->InternalID)->where("InventoryInternalID", $InventoryInternalID)->where("Default", 1)->first()->UomInternalID;
    $tampUom = Uom::find($UomInternalID)->UomID;
    return $totalPieces . ' ' . $tampUom;
}

function getEndStockInventoryWithWarehouse_angka($InventoryInternalID, $ware) {
    $html = "";
    $totalPieces = 0;
    $tampUom = '';
    $initialStockTamp = Inventory::find($InventoryInternalID)->InitialQuantity;
    foreach (InventoryUom::where('InventoryInternalID', $InventoryInternalID)->get() as $data) {
        $pembelian = MrvDetail::
                join('t_mrv_header', 't_mrv_header.InternalID', '=', 't_mrv_detail.MrvInternalID')
                ->where('t_mrv_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_mrv_header.WarehouseInternalID', $ware)
                ->where('t_mrv_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_mrv_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
//        $pembelian = PurchaseDetail::
//                join('t_purchase_header', 't_purchase_header.InternalID', '=', 't_purchase_detail.PurchaseInternalID')
//                ->where('t_purchase_header.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->where('t_purchase_header.WarehouseInternalID', $ware)
//                ->where('t_purchase_detail.InventoryInternalID', $data->InventoryInternalID)
//                ->where('t_purchase_detail.UomInternalID', $data->UomInternalID)
//                ->sum('Qty');
//        $penjualan = SalesDetail::
//                join('t_sales_header', 't_sales_header.InternalID', '=', 't_sales_detail.SalesInternalID')
//                ->where('t_sales_header.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->where('t_sales_detail.InventoryInternalID', $data->InventoryInternalID)
//                ->where('t_sales_detail.UomInternalID', $data->UomInternalID)
//                ->sum('Qty');
        $penjualan = ShippingAddDetail::
                join('t_shipping_header', 't_shipping_header.InternalID', '=', 't_shipping_detail.ShippingInternalID')
                ->where('t_shipping_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_shipping_header.WarehouseInternalID', $ware)
                ->where('t_shipping_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_shipping_detail.UomInternalID', $data->UomInternalID)
                ->where("t_shipping_header.VAT",1)
                ->sum('Qty');
        $Rpembelian = PurchaseReturnDetail::
                join('t_purchasereturn_header', 't_purchasereturn_header.InternalID', '=', 't_purchasereturn_detail.PurchaseReturnInternalID')
                ->where('t_purchasereturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_purchasereturn_header.WarehouseInternalID', $ware)
                ->where('t_purchasereturn_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_purchasereturn_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Rpenjualan = SalesReturnDetail::
                join('t_salesreturn_header', 't_salesreturn_header.InternalID', '=', 't_salesreturn_detail.SalesReturnInternalID')
                ->where('t_salesreturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_salesreturn_header.WarehouseInternalID', $ware)
                ->where('t_salesreturn_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_salesreturn_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Min = MemoInDetail::
                join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.MemoInInternalID')
                ->where('t_memoin_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_memoin_header.WarehouseInternalID', $ware)
                ->where('t_memoin_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_memoin_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Mout = MemoOutDetail::
                join('t_memoout_header', 't_memoout_header.InternalID', '=', 't_memoout_detail.MemoOutInternalID')
                ->where('t_memoout_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_memoout_header.WarehouseInternalID', $ware)
                ->where('t_memoout_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_memoout_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Tin = TransferDetail::
                join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transfer_header.WarehouseDestinyInternalID', $ware)
                ->where('t_transfer_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transfer_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Tout = TransferDetail::
                join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transfer_header.WarehouseInternalID', $ware)
                ->where('t_transfer_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transfer_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Trin = TransformationDetail::
                join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                ->where('t_transformation_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transformation_detail.WarehouseInternalID', $ware)
                ->where('t_transformation_detail.Type', "in")
                ->where('t_transformation_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transformation_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Trout = TransformationDetail::
                join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                ->where('t_transformation_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transformation_detail.WarehouseInternalID', $ware)
                ->where('t_transformation_detail.Type', "out")
                ->where('t_transformation_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transformation_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $ConvIn = Convertion::
                join('m_inventory_uom', 'm_inventory_uom.InternalID', '=', 'h_convertion.InventoryUomInternalID2')
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('h_convertion.WarehouseInternalID', $ware)
                ->where('h_convertion.InventoryUomInternalID2', $data->InternalID)
                ->sum('QuantityResult');
        $ConvOut = Convertion::
                join('m_inventory_uom', 'm_inventory_uom.InternalID', '=', 'h_convertion.InventoryUomInternalID1')
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('h_convertion.WarehouseInternalID', $ware)
                ->where('h_convertion.InventoryUomInternalID1', $data->InternalID)
                ->sum('Quantity');

        $initialStock = $initialStockTamp;
        if ($data->Default == 0) {
            $initialStock = 0;
        }
        $endStock = $initialStock + $pembelian + $Rpenjualan - $penjualan - $Rpembelian + $Min - $Mout + $Tin - $Tout + $Trin - $Trout + $ConvIn - $ConvOut;
        $totalPieces += $endStock * $data->Value;
//        return $ConvOut;
    }
//    $totalPieces += $initialStock ;
    $UomInternalID = InventoryUom::where("CompanyInternalID", Auth::user()->Company->InternalID)->where("InventoryInternalID", $InventoryInternalID)->where("Default", 1)->first()->UomInternalID;
    $tampUom = Uom::find($UomInternalID)->UomID;
    return $totalPieces;
}

function getEndStockInventoryWithWarehouseWithoutShipping($InventoryInternalID, $ware, $shipping) {
    $html = "";
    $totalPieces = 0;
    $tampUom = '';
    $initialStockTamp = Inventory::find($InventoryInternalID)->InitialQuantity;
    foreach (InventoryUom::where('InventoryInternalID', $InventoryInternalID)->get() as $data) {
        $pembelian = MrvDetail::
                join('t_mrv_header', 't_mrv_header.InternalID', '=', 't_mrv_detail.MrvInternalID')
                ->where('t_mrv_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_mrv_header.WarehouseInternalID', $ware)
                ->where('t_mrv_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_mrv_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
//        $pembelian = PurchaseDetail::
//                join('t_purchase_header', 't_purchase_header.InternalID', '=', 't_purchase_detail.PurchaseInternalID')
//                ->where('t_purchase_header.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->where('t_purchase_header.WarehouseInternalID', $ware)
//                ->where('t_purchase_detail.InventoryInternalID', $data->InventoryInternalID)
//                ->where('t_purchase_detail.UomInternalID', $data->UomInternalID)
//                ->sum('Qty');
//        $penjualan = SalesDetail::
//                join('t_sales_header', 't_sales_header.InternalID', '=', 't_sales_detail.SalesInternalID')
//                ->where('t_sales_header.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->where('t_sales_detail.InventoryInternalID', $data->InventoryInternalID)
//                ->where('t_sales_detail.UomInternalID', $data->UomInternalID)
//                ->sum('Qty');
        $penjualan = ShippingAddDetail::
                join('t_shipping_header', 't_shipping_header.InternalID', '=', 't_shipping_detail.ShippingInternalID')
                ->where('t_shipping_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_shipping_header.WarehouseInternalID', $ware)
                ->where('t_shipping_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_shipping_detail.UomInternalID', $data->UomInternalID)
                ->where('t_shipping_header.InternalID', "!=", $shipping)
                ->where("t_shipping_header.VAT",1)
                ->sum('Qty');
        $Rpembelian = PurchaseReturnDetail::
                join('t_purchasereturn_header', 't_purchasereturn_header.InternalID', '=', 't_purchasereturn_detail.PurchaseReturnInternalID')
                ->where('t_purchasereturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_purchasereturn_header.WarehouseInternalID', $ware)
                ->where('t_purchasereturn_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_purchasereturn_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Rpenjualan = SalesReturnDetail::
                join('t_salesreturn_header', 't_salesreturn_header.InternalID', '=', 't_salesreturn_detail.SalesReturnInternalID')
                ->where('t_salesreturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_salesreturn_header.WarehouseInternalID', $ware)
                ->where('t_salesreturn_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_salesreturn_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Min = MemoInDetail::
                join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.MemoInInternalID')
                ->where('t_memoin_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_memoin_header.WarehouseInternalID', $ware)
                ->where('t_memoin_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_memoin_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Mout = MemoOutDetail::
                join('t_memoout_header', 't_memoout_header.InternalID', '=', 't_memoout_detail.MemoOutInternalID')
                ->where('t_memoout_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_memoout_header.WarehouseInternalID', $ware)
                ->where('t_memoout_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_memoout_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Tin = TransferDetail::
                join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transfer_header.WarehouseDestinyInternalID', $ware)
                ->where('t_transfer_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transfer_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Tout = TransferDetail::
                join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transfer_header.WarehouseInternalID', $ware)
                ->where('t_transfer_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transfer_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Trin = TransformationDetail::
                join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                ->where('t_transformation_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transformation_detail.WarehouseInternalID', $ware)
                ->where('t_transformation_detail.Type', "in")
                ->where('t_transformation_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transformation_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Trout = TransformationDetail::
                join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                ->where('t_transformation_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transformation_detail.WarehouseInternalID', $ware)
                ->where('t_transformation_detail.Type', "out")
                ->where('t_transformation_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transformation_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $ConvIn = Convertion::
                join('m_inventory_uom', 'm_inventory_uom.InternalID', '=', 'h_convertion.InventoryUomInternalID2')
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('h_convertion.WarehouseInternalID', $ware)
                ->where('h_convertion.InventoryUomInternalID2', $data->InternalID)
                ->sum('QuantityResult');
        $ConvOut = Convertion::
                join('m_inventory_uom', 'm_inventory_uom.InternalID', '=', 'h_convertion.InventoryUomInternalID1')
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('h_convertion.WarehouseInternalID', $ware)
                ->where('h_convertion.InventoryUomInternalID1', $data->InternalID)
                ->sum('Quantity');

        $initialStock = $initialStockTamp;
        if ($data->Default == 0) {
            $initialStock = 0;
        }
        $endStock = $initialStock + $pembelian + $Rpenjualan - $penjualan - $Rpembelian + $Min - $Mout + $Tin - $Tout + $Trin - $Trout + $ConvIn - $ConvOut;
        $totalPieces += $endStock * $data->Value;
//        return $ConvOut;
    }
//    $totalPieces += $initialStock ;
    $UomInternalID = InventoryUom::where("CompanyInternalID", Auth::user()->Company->InternalID)->where("InventoryInternalID", $InventoryInternalID)->where("Default", 1)->first()->UomInternalID;
    $tampUom = Uom::find($UomInternalID)->UomID;
    return $totalPieces . ' ' . $tampUom;
}

function getEndStockInventoryWithWarehouseWithoutShipping_angka($InventoryInternalID, $ware, $shipping) {
    $html = "";
    $totalPieces = 0;
    $tampUom = '';
    $initialStockTamp = Inventory::find($InventoryInternalID)->InitialQuantity;
    foreach (InventoryUom::where('InventoryInternalID', $InventoryInternalID)->get() as $data) {
        $pembelian = MrvDetail::
                join('t_mrv_header', 't_mrv_header.InternalID', '=', 't_mrv_detail.MrvInternalID')
                ->where('t_mrv_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_mrv_header.WarehouseInternalID', $ware)
                ->where('t_mrv_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_mrv_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
//        $pembelian = PurchaseDetail::
//                join('t_purchase_header', 't_purchase_header.InternalID', '=', 't_purchase_detail.PurchaseInternalID')
//                ->where('t_purchase_header.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->where('t_purchase_header.WarehouseInternalID', $ware)
//                ->where('t_purchase_detail.InventoryInternalID', $data->InventoryInternalID)
//                ->where('t_purchase_detail.UomInternalID', $data->UomInternalID)
//                ->sum('Qty');
//        $penjualan = SalesDetail::
//                join('t_sales_header', 't_sales_header.InternalID', '=', 't_sales_detail.SalesInternalID')
//                ->where('t_sales_header.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->where('t_sales_detail.InventoryInternalID', $data->InventoryInternalID)
//                ->where('t_sales_detail.UomInternalID', $data->UomInternalID)
//                ->sum('Qty');
        $penjualan = ShippingAddDetail::
                join('t_shipping_header', 't_shipping_header.InternalID', '=', 't_shipping_detail.ShippingInternalID')
                ->where('t_shipping_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_shipping_header.WarehouseInternalID', $ware)
                ->where('t_shipping_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_shipping_detail.UomInternalID', $data->UomInternalID)
                ->where('t_shipping_header.InternalID', "!=", $shipping)
                ->where("t_shipping_header.VAT",1)
                ->sum('Qty');
        $Rpembelian = PurchaseReturnDetail::
                join('t_purchasereturn_header', 't_purchasereturn_header.InternalID', '=', 't_purchasereturn_detail.PurchaseReturnInternalID')
                ->where('t_purchasereturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_purchasereturn_header.WarehouseInternalID', $ware)
                ->where('t_purchasereturn_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_purchasereturn_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Rpenjualan = SalesReturnDetail::
                join('t_salesreturn_header', 't_salesreturn_header.InternalID', '=', 't_salesreturn_detail.SalesReturnInternalID')
                ->where('t_salesreturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_salesreturn_header.WarehouseInternalID', $ware)
                ->where('t_salesreturn_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_salesreturn_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Min = MemoInDetail::
                join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.MemoInInternalID')
                ->where('t_memoin_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_memoin_header.WarehouseInternalID', $ware)
                ->where('t_memoin_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_memoin_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Mout = MemoOutDetail::
                join('t_memoout_header', 't_memoout_header.InternalID', '=', 't_memoout_detail.MemoOutInternalID')
                ->where('t_memoout_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_memoout_header.WarehouseInternalID', $ware)
                ->where('t_memoout_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_memoout_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Tin = TransferDetail::
                join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transfer_header.WarehouseDestinyInternalID', $ware)
                ->where('t_transfer_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transfer_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Tout = TransferDetail::
                join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transfer_header.WarehouseInternalID', $ware)
                ->where('t_transfer_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transfer_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Trin = TransformationDetail::
                join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                ->where('t_transformation_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transformation_detail.WarehouseInternalID', $ware)
                ->where('t_transformation_detail.Type', "in")
                ->where('t_transformation_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transformation_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Trout = TransformationDetail::
                join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                ->where('t_transformation_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transformation_detail.WarehouseInternalID', $ware)
                ->where('t_transformation_detail.Type', "out")
                ->where('t_transformation_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transformation_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $ConvIn = Convertion::
                join('m_inventory_uom', 'm_inventory_uom.InternalID', '=', 'h_convertion.InventoryUomInternalID2')
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('h_convertion.WarehouseInternalID', $ware)
                ->where('h_convertion.InventoryUomInternalID2', $data->InternalID)
                ->sum('QuantityResult');
        $ConvOut = Convertion::
                join('m_inventory_uom', 'm_inventory_uom.InternalID', '=', 'h_convertion.InventoryUomInternalID1')
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('h_convertion.WarehouseInternalID', $ware)
                ->where('h_convertion.InventoryUomInternalID1', $data->InternalID)
                ->sum('Quantity');

        $initialStock = $initialStockTamp;
        if ($data->Default == 0) {
            $initialStock = 0;
        }
        $endStock = $initialStock + $pembelian + $Rpenjualan - $penjualan - $Rpembelian + $Min - $Mout + $Tin - $Tout + $Trin - $Trout + $ConvIn - $ConvOut;
        $totalPieces += $endStock * $data->Value;
//        return $ConvOut;
    }
//    $totalPieces += $initialStock ;
    $UomInternalID = InventoryUom::where("CompanyInternalID", Auth::user()->Company->InternalID)->where("InventoryInternalID", $InventoryInternalID)->where("Default", 1)->first()->UomInternalID;
    $tampUom = Uom::find($UomInternalID)->UomID;
    return $totalPieces;
}

function getEndStockInventoryWithWarehouseWithoutTransformation($InventoryInternalID, $ware, $transformation) {
    $html = "";
    $totalPieces = 0;
    $tampUom = '';
    $initialStockTamp = Inventory::find($InventoryInternalID)->InitialQuantity;
    foreach (InventoryUom::where('InventoryInternalID', $InventoryInternalID)->get() as $data) {
        $pembelian = MrvDetail::
                join('t_mrv_header', 't_mrv_header.InternalID', '=', 't_mrv_detail.MrvInternalID')
                ->where('t_mrv_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_mrv_header.WarehouseInternalID', $ware)
                ->where('t_mrv_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_mrv_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
//        $pembelian = PurchaseDetail::
//                join('t_purchase_header', 't_purchase_header.InternalID', '=', 't_purchase_detail.PurchaseInternalID')
//                ->where('t_purchase_header.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->where('t_purchase_header.WarehouseInternalID', $ware)
//                ->where('t_purchase_detail.InventoryInternalID', $data->InventoryInternalID)
//                ->where('t_purchase_detail.UomInternalID', $data->UomInternalID)
//                ->sum('Qty');
//        $penjualan = SalesDetail::
//                join('t_sales_header', 't_sales_header.InternalID', '=', 't_sales_detail.SalesInternalID')
//                ->where('t_sales_header.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->where('t_sales_detail.InventoryInternalID', $data->InventoryInternalID)
//                ->where('t_sales_detail.UomInternalID', $data->UomInternalID)
//                ->sum('Qty');
        $penjualan = ShippingAddDetail::
                join('t_shipping_header', 't_shipping_header.InternalID', '=', 't_shipping_detail.ShippingInternalID')
                ->where('t_shipping_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_shipping_header.WarehouseInternalID', $ware)
                ->where('t_shipping_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_shipping_detail.UomInternalID', $data->UomInternalID)
                ->where("t_shipping_header.VAT",1)
                ->sum('Qty');
        $Rpembelian = PurchaseReturnDetail::
                join('t_purchasereturn_header', 't_purchasereturn_header.InternalID', '=', 't_purchasereturn_detail.PurchaseReturnInternalID')
                ->where('t_purchasereturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_purchasereturn_header.WarehouseInternalID', $ware)
                ->where('t_purchasereturn_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_purchasereturn_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Rpenjualan = SalesReturnDetail::
                join('t_salesreturn_header', 't_salesreturn_header.InternalID', '=', 't_salesreturn_detail.SalesReturnInternalID')
                ->where('t_salesreturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_salesreturn_header.WarehouseInternalID', $ware)
                ->where('t_salesreturn_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_salesreturn_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Min = MemoInDetail::
                join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.MemoInInternalID')
                ->where('t_memoin_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_memoin_header.WarehouseInternalID', $ware)
                ->where('t_memoin_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_memoin_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Mout = MemoOutDetail::
                join('t_memoout_header', 't_memoout_header.InternalID', '=', 't_memoout_detail.MemoOutInternalID')
                ->where('t_memoout_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_memoout_header.WarehouseInternalID', $ware)
                ->where('t_memoout_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_memoout_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Tin = TransferDetail::
                join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transfer_header.WarehouseDestinyInternalID', $ware)
                ->where('t_transfer_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transfer_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Tout = TransferDetail::
                join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transfer_header.WarehouseInternalID', $ware)
                ->where('t_transfer_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transfer_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Trin = TransformationDetail::
                join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                ->where('t_transformation_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transformation_detail.WarehouseInternalID', $ware)
                ->where('t_transformation_detail.Type', "in")
                ->where('t_transformation_header.InternalID', "!=", $transformation)
                ->where('t_transformation_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transformation_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Trout = TransformationDetail::
                join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                ->where('t_transformation_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transformation_detail.WarehouseInternalID', $ware)
                ->where('t_transformation_detail.Type', "out")
                ->where('t_transformation_header.InternalID', "!=", $transformation)
                ->where('t_transformation_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transformation_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $ConvIn = Convertion::
                join('m_inventory_uom', 'm_inventory_uom.InternalID', '=', 'h_convertion.InventoryUomInternalID2')
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('h_convertion.WarehouseInternalID', $ware)
                ->where('h_convertion.InventoryUomInternalID2', $data->InternalID)
                ->sum('QuantityResult');
        $ConvOut = Convertion::
                join('m_inventory_uom', 'm_inventory_uom.InternalID', '=', 'h_convertion.InventoryUomInternalID1')
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('h_convertion.WarehouseInternalID', $ware)
                ->where('h_convertion.InventoryUomInternalID1', $data->InternalID)
                ->sum('Quantity');

        $initialStock = $initialStockTamp;
        if ($data->Default == 0) {
            $initialStock = 0;
        }
        $endStock = $initialStock + $pembelian + $Rpenjualan - $penjualan - $Rpembelian + $Min - $Mout + $Tin - $Tout + $Trin - $Trout + $ConvIn - $ConvOut;
        $totalPieces += $endStock * $data->Value;
//        return $ConvOut;
    }
//    $totalPieces += $initialStock ;
    $UomInternalID = InventoryUom::where("CompanyInternalID", Auth::user()->Company->InternalID)->where("InventoryInternalID", $InventoryInternalID)->where("Default", 1)->first()->UomInternalID;
    $tampUom = Uom::find($UomInternalID)->UomID;
    return $totalPieces . ' ' . $tampUom;
}

function getEndStockInventoryWithWarehouseWithoutPacking($InventoryInternalID, $ware, $packing) {
    $html = "";
    $totalPieces = 0;
    $tampUom = '';
    $initialStockTamp = Inventory::find($InventoryInternalID)->InitialQuantity;
    foreach (InventoryUom::where('InventoryInternalID', $InventoryInternalID)->get() as $data) {
        $pembelian = MrvDetail::
                join('t_mrv_header', 't_mrv_header.InternalID', '=', 't_mrv_detail.MrvInternalID')
                ->where('t_mrv_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_mrv_header.WarehouseInternalID', $ware)
                ->where('t_mrv_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_mrv_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
//        $pembelian = PurchaseDetail::
//                join('t_purchase_header', 't_purchase_header.InternalID', '=', 't_purchase_detail.PurchaseInternalID')
//                ->where('t_purchase_header.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->where('t_purchase_header.WarehouseInternalID', $ware)
//                ->where('t_purchase_detail.InventoryInternalID', $data->InventoryInternalID)
//                ->where('t_purchase_detail.UomInternalID', $data->UomInternalID)
//                ->sum('Qty');
//        $penjualan = SalesDetail::
//                join('t_sales_header', 't_sales_header.InternalID', '=', 't_sales_detail.SalesInternalID')
//                ->where('t_sales_header.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->where('t_sales_detail.InventoryInternalID', $data->InventoryInternalID)
//                ->where('t_sales_detail.UomInternalID', $data->UomInternalID)
//                ->sum('Qty');
        $penjualan = ShippingAddDetail::
                join('t_shipping_header', 't_shipping_header.InternalID', '=', 't_shipping_detail.ShippingInternalID')
                ->where('t_shipping_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_shipping_header.WarehouseInternalID', $ware)
                ->where('t_shipping_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_shipping_detail.UomInternalID', $data->UomInternalID)
                ->where("t_shipping_header.VAT",1)
                ->where('PackingID', '!=', $packing)
                ->sum('Qty');
        $Rpembelian = PurchaseReturnDetail::
                join('t_purchasereturn_header', 't_purchasereturn_header.InternalID', '=', 't_purchasereturn_detail.PurchaseReturnInternalID')
                ->where('t_purchasereturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_purchasereturn_header.WarehouseInternalID', $ware)
                ->where('t_purchasereturn_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_purchasereturn_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Rpenjualan = SalesReturnDetail::
                join('t_salesreturn_header', 't_salesreturn_header.InternalID', '=', 't_salesreturn_detail.SalesReturnInternalID')
                ->where('t_salesreturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_salesreturn_header.WarehouseInternalID', $ware)
                ->where('t_salesreturn_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_salesreturn_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Min = MemoInDetail::
                join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.MemoInInternalID')
                ->where('t_memoin_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_memoin_header.WarehouseInternalID', $ware)
                ->where('t_memoin_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_memoin_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Mout = MemoOutDetail::
                join('t_memoout_header', 't_memoout_header.InternalID', '=', 't_memoout_detail.MemoOutInternalID')
                ->where('t_memoout_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_memoout_header.WarehouseInternalID', $ware)
                ->where('t_memoout_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_memoout_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Tin = TransferDetail::
                join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transfer_header.WarehouseDestinyInternalID', $ware)
                ->where('t_transfer_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transfer_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Tout = TransferDetail::
                join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transfer_header.WarehouseInternalID', $ware)
                ->where('t_transfer_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transfer_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Trin = TransformationDetail::
                join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                ->where('t_transformation_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transformation_detail.WarehouseInternalID', $ware)
                ->where('t_transformation_detail.Type', "in")
                ->where('t_transformation_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transformation_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Trout = TransformationDetail::
                join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                ->where('t_transformation_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transformation_detail.WarehouseInternalID', $ware)
                ->where('t_transformation_detail.Type', "out")
                ->where('t_transformation_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transformation_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $ConvIn = Convertion::
                join('m_inventory_uom', 'm_inventory_uom.InternalID', '=', 'h_convertion.InventoryUomInternalID2')
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('h_convertion.WarehouseInternalID', $ware)
                ->where('h_convertion.InventoryUomInternalID2', $data->InternalID)
                ->sum('QuantityResult');
        $ConvOut = Convertion::
                join('m_inventory_uom', 'm_inventory_uom.InternalID', '=', 'h_convertion.InventoryUomInternalID1')
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('h_convertion.WarehouseInternalID', $ware)
                ->where('h_convertion.InventoryUomInternalID1', $data->InternalID)
                ->sum('Quantity');

        $initialStock = $initialStockTamp;
        if ($data->Default == 0) {
            $initialStock = 0;
        }
        $endStock = $initialStock + $pembelian + $Rpenjualan - $penjualan - $Rpembelian + $Min - $Mout + $Tin - $Tout + $Trin - $Trout + $ConvIn - $ConvOut;
        $totalPieces += $endStock * $data->Value;
//        return $ConvOut;
    }
//    $totalPieces += $initialStock ;
    $UomInternalID = InventoryUom::where("CompanyInternalID", Auth::user()->Company->InternalID)->where("InventoryInternalID", $InventoryInternalID)->where("Default", 1)->first()->UomInternalID;
    $tampUom = Uom::find($UomInternalID)->UomID;
    return $totalPieces . ' ' . $tampUom;
}

function getEndStockInventoryWithWarehouseWithoutMemoOut($InventoryInternalID, $ware, $memoout) {
    $html = "";
    $totalPieces = 0;
    $tampUom = '';
    $initialStockTamp = Inventory::find($InventoryInternalID)->InitialQuantity;
    foreach (InventoryUom::where('InventoryInternalID', $InventoryInternalID)->get() as $data) {
        $pembelian = MrvDetail::
                join('t_mrv_header', 't_mrv_header.InternalID', '=', 't_mrv_detail.MrvInternalID')
                ->where('t_mrv_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_mrv_header.WarehouseInternalID', $ware)
                ->where('t_mrv_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_mrv_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
//        $pembelian = PurchaseDetail::
//                join('t_purchase_header', 't_purchase_header.InternalID', '=', 't_purchase_detail.PurchaseInternalID')
//                ->where('t_purchase_header.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->where('t_purchase_header.WarehouseInternalID', $ware)
//                ->where('t_purchase_detail.InventoryInternalID', $data->InventoryInternalID)
//                ->where('t_purchase_detail.UomInternalID', $data->UomInternalID)
//                ->sum('Qty');
//        $penjualan = SalesDetail::
//                join('t_sales_header', 't_sales_header.InternalID', '=', 't_sales_detail.SalesInternalID')
//                ->where('t_sales_header.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->where('t_sales_detail.InventoryInternalID', $data->InventoryInternalID)
//                ->where('t_sales_detail.UomInternalID', $data->UomInternalID)
//                ->sum('Qty');
        $penjualan = ShippingAddDetail::
                join('t_shipping_header', 't_shipping_header.InternalID', '=', 't_shipping_detail.ShippingInternalID')
                ->where('t_shipping_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_shipping_header.WarehouseInternalID', $ware)
                ->where('t_shipping_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_shipping_detail.UomInternalID', $data->UomInternalID)
                ->where("t_shipping_header.VAT",1)
                ->sum('Qty');
        $Rpembelian = PurchaseReturnDetail::
                join('t_purchasereturn_header', 't_purchasereturn_header.InternalID', '=', 't_purchasereturn_detail.PurchaseReturnInternalID')
                ->where('t_purchasereturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_purchasereturn_header.WarehouseInternalID', $ware)
                ->where('t_purchasereturn_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_purchasereturn_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Rpenjualan = SalesReturnDetail::
                join('t_salesreturn_header', 't_salesreturn_header.InternalID', '=', 't_salesreturn_detail.SalesReturnInternalID')
                ->where('t_salesreturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_salesreturn_header.WarehouseInternalID', $ware)
                ->where('t_salesreturn_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_salesreturn_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Min = MemoInDetail::
                join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.MemoInInternalID')
                ->where('t_memoin_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_memoin_header.WarehouseInternalID', $ware)
                ->where('t_memoin_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_memoin_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Mout = MemoOutDetail::
                join('t_memoout_header', 't_memoout_header.InternalID', '=', 't_memoout_detail.MemoOutInternalID')
                ->where('t_memoout_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_memoout_header.WarehouseInternalID', $ware)
                ->where('t_memoout_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_memoout_detail.UomInternalID', $data->UomInternalID)
                ->where('t_memoout_header.InternalID', '!=', $memoout)
                ->sum('Qty');
        $Tin = TransferDetail::
                join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transfer_header.WarehouseDestinyInternalID', $ware)
                ->where('t_transfer_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transfer_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Tout = TransferDetail::
                join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transfer_header.WarehouseInternalID', $ware)
                ->where('t_transfer_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transfer_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Trin = TransformationDetail::
                join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                ->where('t_transformation_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transformation_detail.WarehouseInternalID', $ware)
                ->where('t_transformation_detail.Type', "in")
                ->where('t_transformation_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transformation_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Trout = TransformationDetail::
                join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                ->where('t_transformation_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transformation_detail.WarehouseInternalID', $ware)
                ->where('t_transformation_detail.Type', "out")
                ->where('t_transformation_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transformation_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $ConvIn = Convertion::
                join('m_inventory_uom', 'm_inventory_uom.InternalID', '=', 'h_convertion.InventoryUomInternalID2')
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('h_convertion.WarehouseInternalID', $ware)
                ->where('h_convertion.InventoryUomInternalID2', $data->InternalID)
                ->sum('QuantityResult');
        $ConvOut = Convertion::
                join('m_inventory_uom', 'm_inventory_uom.InternalID', '=', 'h_convertion.InventoryUomInternalID1')
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('h_convertion.WarehouseInternalID', $ware)
                ->where('h_convertion.InventoryUomInternalID1', $data->InternalID)
                ->sum('Quantity');

        $initialStock = $initialStockTamp;
        if ($data->Default == 0) {
            $initialStock = 0;
        }
        $endStock = $initialStock + $pembelian + $Rpenjualan - $penjualan - $Rpembelian + $Min - $Mout + $Tin - $Tout + $Trin - $Trout + $ConvIn - $ConvOut;
        $totalPieces += $endStock * $data->Value;
//        return $ConvOut;
    }
//    $totalPieces += $initialStock ;
    $UomInternalID = InventoryUom::where("CompanyInternalID", Auth::user()->Company->InternalID)->where("InventoryInternalID", $InventoryInternalID)->where("Default", 1)->first()->UomInternalID;
    $tampUom = Uom::find($UomInternalID)->UomID;
    return $totalPieces . ' ' . $tampUom;
}

function getEndStockInventoryWithWarehouseWithoutTransfer($InventoryInternalID, $ware, $transfer) {
    $html = "";
    $totalPieces = 0;
    $tampUom = '';
    $initialStockTamp = Inventory::find($InventoryInternalID)->InitialQuantity;
    foreach (InventoryUom::where('InventoryInternalID', $InventoryInternalID)->get() as $data) {
        $pembelian = MrvDetail::
                join('t_mrv_header', 't_mrv_header.InternalID', '=', 't_mrv_detail.MrvInternalID')
                ->where('t_mrv_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_mrv_header.WarehouseInternalID', $ware)
                ->where('t_mrv_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_mrv_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
//        $pembelian = PurchaseDetail::
//                join('t_purchase_header', 't_purchase_header.InternalID', '=', 't_purchase_detail.PurchaseInternalID')
//                ->where('t_purchase_header.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->where('t_purchase_header.WarehouseInternalID', $ware)
//                ->where('t_purchase_detail.InventoryInternalID', $data->InventoryInternalID)
//                ->where('t_purchase_detail.UomInternalID', $data->UomInternalID)
//                ->sum('Qty');
//        $penjualan = SalesDetail::
//                join('t_sales_header', 't_sales_header.InternalID', '=', 't_sales_detail.SalesInternalID')
//                ->where('t_sales_header.CompanyInternalID', Auth::user()->Company->InternalID)
//                ->where('t_sales_detail.InventoryInternalID', $data->InventoryInternalID)
//                ->where('t_sales_detail.UomInternalID', $data->UomInternalID)
//                ->sum('Qty');
        $penjualan = ShippingAddDetail::
                join('t_shipping_header', 't_shipping_header.InternalID', '=', 't_shipping_detail.ShippingInternalID')
                ->where('t_shipping_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_shipping_header.WarehouseInternalID', $ware)
                ->where('t_shipping_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_shipping_detail.UomInternalID', $data->UomInternalID)
                ->where("t_shipping_header.VAT",1)
                ->sum('Qty');
        $Rpembelian = PurchaseReturnDetail::
                join('t_purchasereturn_header', 't_purchasereturn_header.InternalID', '=', 't_purchasereturn_detail.PurchaseReturnInternalID')
                ->where('t_purchasereturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_purchasereturn_header.WarehouseInternalID', $ware)
                ->where('t_purchasereturn_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_purchasereturn_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Rpenjualan = SalesReturnDetail::
                join('t_salesreturn_header', 't_salesreturn_header.InternalID', '=', 't_salesreturn_detail.SalesReturnInternalID')
                ->where('t_salesreturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_salesreturn_header.WarehouseInternalID', $ware)
                ->where('t_salesreturn_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_salesreturn_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Min = MemoInDetail::
                join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.MemoInInternalID')
                ->where('t_memoin_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_memoin_header.WarehouseInternalID', $ware)
                ->where('t_memoin_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_memoin_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Mout = MemoOutDetail::
                join('t_memoout_header', 't_memoout_header.InternalID', '=', 't_memoout_detail.MemoOutInternalID')
                ->where('t_memoout_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_memoout_header.WarehouseInternalID', $ware)
                ->where('t_memoout_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_memoout_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Tin = TransferDetail::
                join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transfer_header.WarehouseDestinyInternalID', $ware)
                ->where('t_transfer_header.InternalID', "!=", $transfer)
                ->where('t_transfer_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transfer_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Tout = TransferDetail::
                join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transfer_header.WarehouseInternalID', $ware)
                ->where('t_transfer_header.InternalID', "!=", $transfer)
                ->where('t_transfer_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transfer_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Trin = TransformationDetail::
                join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                ->where('t_transformation_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transformation_detail.WarehouseInternalID', $ware)
                ->where('t_transformation_detail.Type', "in")
                ->where('t_transformation_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transformation_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $Trout = TransformationDetail::
                join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                ->where('t_transformation_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transformation_detail.WarehouseInternalID', $ware)
                ->where('t_transformation_detail.Type', "out")
                ->where('t_transformation_detail.InventoryInternalID', $data->InventoryInternalID)
                ->where('t_transformation_detail.UomInternalID', $data->UomInternalID)
                ->sum('Qty');
        $ConvIn = Convertion::
                join('m_inventory_uom', 'm_inventory_uom.InternalID', '=', 'h_convertion.InventoryUomInternalID2')
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('h_convertion.WarehouseInternalID', $ware)
                ->where('h_convertion.InventoryUomInternalID2', $data->InternalID)
                ->sum('QuantityResult');
        $ConvOut = Convertion::
                join('m_inventory_uom', 'm_inventory_uom.InternalID', '=', 'h_convertion.InventoryUomInternalID1')
                ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('h_convertion.WarehouseInternalID', $ware)
                ->where('h_convertion.InventoryUomInternalID1', $data->InternalID)
                ->sum('Quantity');

        $initialStock = $initialStockTamp;
        if ($data->Default == 0) {
            $initialStock = 0;
        }
        $endStock = $initialStock + $pembelian + $Rpenjualan - $penjualan - $Rpembelian + $Min - $Mout + $Tin - $Tout + $Trin - $Trout + $ConvIn - $ConvOut;
        $totalPieces += $endStock * $data->Value;
//        return $ConvOut;
    }
//    $totalPieces += $initialStock ;
    $UomInternalID = InventoryUom::where("CompanyInternalID", Auth::user()->Company->InternalID)->where("InventoryInternalID", $InventoryInternalID)->where("Default", 1)->first()->UomInternalID;
    $tampUom = Uom::find($UomInternalID)->UomID;
    return $totalPieces . ' ' . $tampUom;
}

function getLastPriceThisInventory($InventoryInternalID) {
    $purchaseDetail = PurchaseAddDetail::join("t_purchase_header", "t_purchase_header.InternalID", "=", "t_purchase_detail.PurchaseInternalID")
                    ->where("t_purchase_header.CompanyInternalID", Auth::user()->Company->InternalID)
                    ->where("t_purchase_detail.InventoryInternalID", $InventoryInternalID)
                    ->orderBy("t_purchase_header.PurchaseDate", "DESC")->first();
    if (count($purchaseDetail) > 0) {
        return number_format($purchaseDetail->Price, 2, '.', ',') . " (" . Uom::find($purchaseDetail->UomInternalID)->UomID . ")";
    }
    return 0;
}

function getPriceRangeInventory($inventoryInternalID, $uomInternalID, $qty) {
    $value = 0;
    $result = 0;
    $price = 0;
    $value = InventoryUom::where("CompanyInternalID", Auth::user()->Company->InternalID)
                    ->where("InventoryInternalID", $inventoryInternalID)
                    ->where("UomInternalID", $uomInternalID)->first()->Value;
    $result = $value * $qty;

    $inventory = Inventory::find($inventoryInternalID);

    if ($result <= $inventory->SmallQty) {
        $price = $inventory->SmallValue;
    } else if ($result <= $inventory->MediumQty) {
        $price = $inventory->MediumValue;
    } else {
        $price = $inventory->BigValue;
    }

    return $price;
}

function getStockInventorySOHelper($InternalID) {
    $tooltip = "";
    $inventorySimilarity = InventorySimilarity::where("CompanyInternalID", Auth::user()->Company->InternalID)->where("InventoryInternalID", $InternalID)->get();

    if (count($inventorySimilarity) > 0) {
        foreach ($inventorySimilarity as $data) {
            $tooltip .= $data->Inventory->InventoryID . ' ' . $data->Inventory->InventoryName . ' <br/> ';
        }
    } else {
        $tooltip = "-";
    }
    return getEndStockInventory($InternalID) . '---;---' . $tooltip;
}

function getInformationPriceRangeInventory($inventoryInternalID) {
    $inventory = Inventory::find($inventoryInternalID);
    $html = "";
    $html .= "Small Qty : " . $inventory->SmallQty . ' - Rp. ' . number_format($inventory->SmallValue, 2, '.', ',') . "<br/>";
    $html .= "Medium Qty : " . $inventory->MediumQty . ' - Rp. ' . number_format($inventory->MediumValue, 2, '.', ',') . "<br/>";
    $html .= "Big Qty : " . $inventory->BigQty . ' - Rp. ' . number_format($inventory->BigValue, 2, '.', ',') . "<br/>";
    return $html;
}

function getHPPValueInventory($inventoryInternalID) {
    $date = date("d-m-Y", strtotime(date("y-m-d")));
    $arrDate = explode("-", $date);
    $bulan = $arrDate[1];
    $tahun = $arrDate[2];
    $qtyPurchase = MrvHeader::qtyInventory($inventoryInternalID, $bulan, $tahun);
    $qtyPurchaseR = PurchaseReturnHeader::qtyInventory($inventoryInternalID, $bulan, $tahun);
    $qtyMemoIn = MemoInHeader::qtyInventory($inventoryInternalID, $bulan, $tahun);
    $purchase = PurchaseHeader::valueInventory($inventoryInternalID, $bulan, $tahun);
    $purchaseR = PurchaseReturnHeader::valueInventory($inventoryInternalID, $bulan, $tahun);
    $memoIn = MemoInHeader::valueInventory($inventoryInternalID, $bulan, $tahun);
    $valueBefore = InventoryValue::valueInventoryBefore($inventoryInternalID, $bulan, $tahun);
    $qtyBefore = InventoryValue::qtyInventoryBefore($inventoryInternalID, $bulan, $tahun);

    $data = Inventory::find($inventoryInternalID);
    $dataInitialQuantity = $data->InitialQuantity;
    $dataInitialValue = $data->InitialValue;
    if (InventoryValue::where('InventoryInternalID', $data->InternalID)
                    ->orderBy("Year", 'desc')
                    ->orderBy("Month", 'desc')
                    ->whereRaw("CONCAT(Year,'-',Month,'-31') < " . $tahun . '-' . $bulan . '-01')
                    ->count() > 0) {
        $dataInitialQuantity = 0;
        $dataInitialValue = 0;
    }
    $qtyDividen = $dataInitialQuantity + $qtyBefore + $qtyPurchase - $qtyPurchaseR + $qtyMemoIn;
    if ($qtyDividen == 0) {
        $average = 0;
    } else {
        $average = (($dataInitialValue * $dataInitialQuantity) + ($valueBefore * $qtyBefore) + $purchase - $purchaseR + $memoIn) / $qtyDividen;
    }
    return number_format($average, 2, '.', ',');
}

function cekGantiValue($inventoryInternalID, $uomInternalID) {
    $where = ['InventoryInternalID' => $inventoryInternalID, 'UomInternalID' => $uomInternalID];

    $cekQuotation1 = QuotationDetail::where($where)->count();
    $cekQuotation2 = QuotationParcel::Where(DB::raw("ParcelInternalID IN (SELECT InternalID FROM m_parcel_inventory WHERE "
                            . "InventoryInternalID = $inventoryInternalID AND UomInternalID = $uomInternalID)"))->count();

    $cekSales1 = SalesOrderDetail::where($where)->count();
    $cekSales2 = SalesOrderParcel::Where(DB::raw("ParcelInternalID IN (SELECT InternalID FROM m_parcel_inventory WHERE "
                            . "InventoryInternalID = $inventoryInternalID AND UomInternalID = $uomInternalID)"))->count();

    $cekPurchase = PurchaseOrderDetail::where($where)->count();
//    print_r($cekSales1);
//    exit();
    //transfotmation sudah di memo in/out
    $cekMemoIn = MemoInDetail::where($where)->count();
    $cekMemoOut = MemoOutDetail::where($where)->count();
    $cekTransfer = TransferDetail::where($where)->count();

//    $cekConvertion = Convertion::where();

    $convertion1 = Convertion::join("m_inventory_uom", "m_inventory_uom.InternalID", "=", "h_convertion.InventoryUomInternalID1")->where($where)->count();
    $convertion2 = Convertion::join("m_inventory_uom", "m_inventory_uom.InternalID", "=", "h_convertion.InventoryUomInternalID2")->where($where)->count();

    if ($cekMemoIn > 0 || $cekMemoOut > 0 || $cekPurchase > 0 || $cekSales1 > 0 || $cekSales2 > 0 || $cekQuotation1 > 0 || $cekQuotation2 > 0 || $cekTransfer > 0 || $convertion1 > 0 || $convertion2 > 0) {
        return 'false';
    } else {
        return 'true';
    }
}

function setTampInventory($inventoryInternalID) {
    $inventory = Inventory::find($inventoryInternalID);
    $inventory->TampLastPrice = getLastPriceThisInventory($inventory->InternalID);
    $inventory->TampStock = getEndStockInventory($inventory->InternalID);
    $inventory->TampHPP = getHPPValueInventory($inventory->InternalID);
    $inventory->save();
}

function getSlipType($text) {
    if ($text == 'Cash In' || $text == 'Cash Out') {
        return 0;
    }
    if ($text == 'Bank In' || $text == 'Bank Out') {
        return 1;
    }
    if ($text == 'Piutang Giro') {
        return 2;
    }
    if ($text == 'Hutang Giro') {
        return 3;
    }
}

function getConnection() {
//    return array(
//        'user' => 'gsalmon',
//        'pass' => 'genIT007',
//        'db' => 'genesys_akun_vista',
//        'host' => 'rumahdata.salmonacc.com'
//    );
    return array(
        'user' => 'root',
        'pass' => '',
//        'pass' => 'genit900',
        'db' => 'genesys_akun_dutaperkasa',
        'host' => '127.0.0.1'
    );
}

?>
