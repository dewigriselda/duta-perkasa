<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class AccountValue extends Eloquent implements UserInterface, RemindableInterface {
    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'h_account_value';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showAccountValue() {
        return AccountValue::all();
    }

    public static function debetAccountBefore($coa, $bulan, $tahun) {
        $bulan = $bulan - 1;
        if ($bulan == 0) {
            $bulan = 12;
            $tahun = $tahun - 1;
        }
        $result = AccountValue::where('COAInternalID', $coa)
                ->where('Month', $bulan)
                ->where('Year', $tahun)
                ->sum('Debet');
        if (count($result) <= 0) {
            return ;
        }
        return $result;
    }

    public static function creditAccountBefore($coa, $bulan, $tahun) {
        $bulan = $bulan - 1;
        if ($bulan == 0) {
            $bulan = 12;
            $tahun = $tahun - 1;
        }
        $result = AccountValue::where('COAInternalID', $coa)
                ->where('Month', $bulan)
                ->where('Year', $tahun)
                ->sum('Credit');
        if (count($result) <= 0) {
            return 0;
        }
        return $result;
    }
}
