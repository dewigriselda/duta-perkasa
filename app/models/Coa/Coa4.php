<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Coa4 extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_coa4';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showCoa4() {
        return Coa4::all();
    }

    public static function idCoa4() {
        return Coa4::select('InternalID', 'ACC4ID', 'ACC4Name')
                ->where('InternalID','!=','0')
                ->where('CompanyInternalID',Auth::user()->Company->InternalID)
                ->orderBy('ACC4ID','asc')
                ->get();
    }

    public static function coa4inCoa3($ACC3ID) {
        $results = DB::select(DB::raw("Select * From m_coa4"
                                . " where LEFT(ACC4ID," . strlen($ACC3ID) . ") = '" . $ACC3ID . "'"
                                . "AND CompanyInternalID = '".Auth::user()->Company->InternalID."'"
                                . "order by ACC4ID asc"));
        return $results;
    }
    
    public function coa() {
        return $this->hasMany('Coa', 'ACC4InternalID', 'InternalID');
    }
    
    public function journalDetail() {
        return $this->hasMany('JournalDetail', 'ACC4InternalID', 'InternalID');
    }

}
