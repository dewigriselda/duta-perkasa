<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Cost extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_cost';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showCost() {
        return Cost::all();
    }

    public static function coa($idCost) {
        $result = DB::select(DB::raw('select `m_cost`.*, `m_coa`.`COAName`, `m_coa`.`InternalID` as COAInternalID '
                                . 'from `m_cost` '
                                . 'inner join `m_coa` on `m_cost`.`ACC1InternalID` = `m_coa`.`ACC1InternalID` '
                                . 'AND `m_cost`.`ACC2InternalID` = `m_coa`.`ACC2InternalID` '
                                . 'AND `m_cost`.`ACC3InternalID` = `m_coa`.`ACC3InternalID` '
                                . 'AND `m_cost`.`ACC4InternalID` = `m_coa`.`ACC4InternalID` '
                                . 'AND `m_cost`.`ACC5InternalID` = `m_coa`.`ACC5InternalID` '
                                . 'AND `m_cost`.`ACC6InternalID` = `m_coa`.`ACC6InternalID` '
                                . 'where `m_cost`.`InternalID` = ' . $idCost . ' '
                                . 'AND `m_cost`.`CompanyInternalID` = ' . Auth::user()->Company->InternalID . ' '));
        return $result;
    }

    public static function coaID() {
        $result = DB::select(DB::raw('select m_cost.InternalID, CONCAT(m_coa.ACC1InternalID,"-",m_coa.ACC2InternalID,"-",m_coa.ACC3InternalID,"-",'
                                . 'm_coa.ACC4InternalID,"-",m_coa.ACC5InternalID,"-",m_coa.ACC6InternalID) as COAID, m_coa.COAName '
                                . 'from `m_cost` '
                                . 'inner join `m_coa` on `m_cost`.`ACC1InternalID` = `m_coa`.`ACC1InternalID` '
                                . 'AND `m_cost`.`ACC2InternalID` = `m_coa`.`ACC2InternalID` '
                                . 'AND `m_cost`.`ACC3InternalID` = `m_coa`.`ACC3InternalID` '
                                . 'AND `m_cost`.`ACC4InternalID` = `m_coa`.`ACC4InternalID` '
                                . 'AND `m_cost`.`ACC5InternalID` = `m_coa`.`ACC5InternalID` '
                                . 'AND `m_cost`.`ACC6InternalID` = `m_coa`.`ACC6InternalID` '
                                . 'AND `m_cost`.`CompanyInternalID` = ' . Auth::user()->Company->InternalID . ' '));
        return $result;
    }

    public static function reportCost($cost, $start, $end)
    {
        return JournalDetail::join("t_journal_header", 't_journal_detail.JournalInternalID', '=', 't_journal_header.InternalID')
                ->whereBetween("t_journal_header.JournalDate", Array($start, $end))
                ->where('t_journal_detail.ACC1InternalID', $cost->ACC1InternalID)
                ->where('t_journal_detail.ACC2InternalID', $cost->ACC2InternalID)
                ->where("t_journal_detail.ACC3InternalID", $cost->ACC3InternalID)
                ->where('t_journal_detail.ACC4InternalID', $cost->ACC4InternalID)
                ->where('t_journal_detail.ACC5InternalID', $cost->ACC5InternalID)
                ->where('t_journal_detail.ACC6InternalID', $cost->ACC6InternalID)
                ->orderBy('t_journal_header.JournalDate', 'ASC')
                ->get();
    }
    
    public static function reportCostInitial($cost, $start, $end)
    {
        return JournalDetail::join("t_journal_header", 't_journal_detail.JournalInternalID', '=', 't_journal_header.InternalID')
                ->where("t_journal_header.JournalDate", '<', $start)
                ->where('t_journal_detail.ACC1InternalID', $cost->ACC1InternalID)
                ->where('t_journal_detail.ACC2InternalID', $cost->ACC2InternalID)
                ->where("t_journal_detail.ACC3InternalID", $cost->ACC3InternalID)
                ->where('t_journal_detail.ACC4InternalID', $cost->ACC4InternalID)
                ->where('t_journal_detail.ACC5InternalID', $cost->ACC5InternalID)
                ->where('t_journal_detail.ACC6InternalID', $cost->ACC6InternalID)
                ->get();
    }
    
    public static function getInternalCoa($id) {
        $default = Cost::select(DB::raw('ACC1InternalID,ACC2InternalID,ACC3InternalID,ACC4InternalID,ACC5InternalID,ACC6InternalID'))
                ->where('InternalID', '=', $id)
                ->where('CompanyInternalID', '=', Auth::user()->Company->InternalID)
                ->first();
        return $default;
    }

    public function currency() {
        return $this->belongsTo('Currency', 'CurrencyInternalID', 'InternalID');
    }

    public function journalHeader() {
        return $this->hasMany('JournalHeader', 'CostInternalID', 'InternalID');
    }

}
