<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class CustomerDetail extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_customer_detail';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showCustomerDetail() {
        return CustomerDetail::all();
    }

    public function customer() {
        return $this->belongsTo('Coa6', 'ACC6InternalID', 'InternalID');
    }

    public function area() {
        return $this->belongsTo('Area', 'AreaInternalID', 'InternalID');
    }

}
