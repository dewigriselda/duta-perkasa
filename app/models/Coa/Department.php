<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Department extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_department';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';


    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showDepartment() {
        return Department::all();
    }
    
    public function slip() {
        return $this->hasMany('Slip', 'DepartmentInternalID', 'InternalID');
    }
    
    public function journalHeader() {
        return $this->hasMany('JournalHeader', 'DepartmentInternalID', 'InternalID');
    }
}
