<?php

use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\UserTrait;

class DepreciationDetail extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_depreciation_detail';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function getCheck2FromDetail($internalHeader) {
        $month = date('m');
        $year = date('Y');
        return DepreciationDetail::where('DepreciationInternalID', $internalHeader)
                        ->where('Month', $month)
                        ->where('Year', $year)->pluck('Check2');
    }

    public static function journalDepreciation($month, $year) {
        return DepreciationDetail::select("*", DB::raw("m_depreciation_detail.InternalID as DepreciationInternalID_Detail, m_depreciation_header.InternalID as InternalID_Header, 
                m_depreciation_detail.Value as Value_Detail, m_depreciation_header.CompanyInternalID as CompanyInternalID_Header"))
                ->join("m_depreciation_header", "m_depreciation_detail.DepreciationInternalID", "=", "m_depreciation_header.InternalID")
                        ->where("m_depreciation_detail.Month", $month)
                        ->where("m_depreciation_detail.Year", $year)
                        ->where("m_depreciation_detail.Check1", 0)
                        ->where("m_depreciation_detail.Check2", 0)
                        ->where("m_depreciation_header.CompanyInternalID", Auth::user()->Company->InternalID)
                        ->orderBy("m_depreciation_header.InternalID")
                        ->get();
    }

    public static function showDepreciationDetail() {
        return DepreciationDetail::all();
    }

    public function depreciationHeader() {
        return $this->belongsTo('DepreciationHeader', 'DepreciationInternalID', 'InternalID');
    }

}
