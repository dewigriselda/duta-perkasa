<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class GroupDepreciation extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_groupdepreciation';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showGroupDepreciation() {
        return GroupDepreciation::all();
    }

    public static function coaDebet($idGroupDepreciation) {
        $result = DB::select(DB::raw('select `m_groupdepreciation`.*, `m_coa`.`COAName`, `m_coa`.`InternalID` as COAInternalID '
                                . 'from `m_groupdepreciation` '
                                . 'inner join `m_coa` on `m_groupdepreciation`.`ACC1InternalIDDebet` = `m_coa`.`ACC1InternalID` '
                                . 'AND `m_groupdepreciation`.`ACC2InternalIDDebet` = `m_coa`.`ACC2InternalID` '
                                . 'AND `m_groupdepreciation`.`ACC3InternalIDDebet` = `m_coa`.`ACC3InternalID` '
                                . 'AND `m_groupdepreciation`.`ACC4InternalIDDebet` = `m_coa`.`ACC4InternalID` '
                                . 'AND `m_groupdepreciation`.`ACC5InternalIDDebet` = `m_coa`.`ACC5InternalID` '
                                . 'AND `m_groupdepreciation`.`ACC6InternalIDDebet` = `m_coa`.`ACC6InternalID` '
                                . 'where `m_groupdepreciation`.`InternalID` = ' . $idGroupDepreciation . ' '
                                . 'AND `m_groupdepreciation`.`CompanyInternalID` = ' . Auth::user()->Company->InternalID . ' '));
        return $result;
    }

    public static function coaCredit($idGroupDepreciation) {
        $result = DB::select(DB::raw('select `m_groupdepreciation`.*, `m_coa`.`COAName`, `m_coa`.`InternalID` as COAInternalID '
                                . 'from `m_groupdepreciation` '
                                . 'inner join `m_coa` on `m_groupdepreciation`.`ACC1InternalIDCredit` = `m_coa`.`ACC1InternalID` '
                                . 'AND `m_groupdepreciation`.`ACC2InternalIDCredit` = `m_coa`.`ACC2InternalID` '
                                . 'AND `m_groupdepreciation`.`ACC3InternalIDCredit` = `m_coa`.`ACC3InternalID` '
                                . 'AND `m_groupdepreciation`.`ACC4InternalIDCredit` = `m_coa`.`ACC4InternalID` '
                                . 'AND `m_groupdepreciation`.`ACC5InternalIDCredit` = `m_coa`.`ACC5InternalID` '
                                . 'AND `m_groupdepreciation`.`ACC6InternalIDCredit` = `m_coa`.`ACC6InternalID` '
                                . 'where `m_groupdepreciation`.`InternalID` = ' . $idGroupDepreciation . ' '
                                . 'AND `m_groupdepreciation`.`CompanyInternalID` = ' . Auth::user()->Company->InternalID . ' '));
        return $result;
    }

    public static function coaIDDebet() {
        $result = DB::select(DB::raw('select m_groupdepreciation.InternalID, CONCAT(m_coa.ACC1InternalID,"-",m_coa.ACC2InternalID,"-",m_coa.ACC3InternalID,"-",'
                                . 'm_coa.ACC4InternalID,"-",m_coa.ACC5InternalID,"-",m_coa.ACC6InternalID) as COAID, m_coa.COAName '
                                . 'from `m_groupdepreciation` '
                                . 'inner join `m_coa` on `m_groupdepreciation`.`ACC1InternalIDDebet` = `m_coa`.`ACC1InternalID` '
                                . 'AND `m_groupdepreciation`.`ACC2InternalIDDebet` = `m_coa`.`ACC2InternalID` '
                                . 'AND `m_groupdepreciation`.`ACC3InternalIDDebet` = `m_coa`.`ACC3InternalID` '
                                . 'AND `m_groupdepreciation`.`ACC4InternalIDDebet` = `m_coa`.`ACC4InternalID` '
                                . 'AND `m_groupdepreciation`.`ACC5InternalIDDebet` = `m_coa`.`ACC5InternalID` '
                                . 'AND `m_groupdepreciation`.`ACC6InternalIDDebet` = `m_coa`.`ACC6InternalID` '
                                . 'AND `m_groupdepreciation`.`CompanyInternalID` = ' . Auth::user()->Company->InternalID . ' '));
        return $result;
    }
    
    public static function coaIDCredit() {
        $result = DB::select(DB::raw('select m_groupdepreciation.InternalID, CONCAT(m_coa.ACC1InternalID,"-",m_coa.ACC2InternalID,"-",m_coa.ACC3InternalID,"-",'
                                . 'm_coa.ACC4InternalID,"-",m_coa.ACC5InternalID,"-",m_coa.ACC6InternalID) as COAID, m_coa.COAName '
                                . 'from `m_groupdepreciation` '
                                . 'inner join `m_coa` on `m_groupdepreciation`.`ACC1InternalIDCredit` = `m_coa`.`ACC1InternalID` '
                                . 'AND `m_groupdepreciation`.`ACC2InternalIDCredit` = `m_coa`.`ACC2InternalID` '
                                . 'AND `m_groupdepreciation`.`ACC3InternalIDCredit` = `m_coa`.`ACC3InternalID` '
                                . 'AND `m_groupdepreciation`.`ACC4InternalIDCredit` = `m_coa`.`ACC4InternalID` '
                                . 'AND `m_groupdepreciation`.`ACC5InternalIDCredit` = `m_coa`.`ACC5InternalID` '
                                . 'AND `m_groupdepreciation`.`ACC6InternalIDCredit` = `m_coa`.`ACC6InternalID` '
                                . 'AND `m_groupdepreciation`.`CompanyInternalID` = ' . Auth::user()->Company->InternalID . ' '));
        return $result;
    }

    public static function getInternalCoa($id) {
        $default = GroupDepreciation::select(DB::raw('ACC1InternalIDDebet,ACC2InternalIDDebet,ACC3InternalIDDebet,ACC4InternalIDDebet,ACC5InternalIDDebet,ACC6InternalIDDebet,'
                . 'ACC1InternalIDCredit,ACC2InternalIDCredit,ACC3InternalIDCredit,ACC4InternalIDCredit,ACC5InternalIDCredit,ACC6InternalIDCredit'))
                ->where('InternalID', '=', $id)
                ->where('CompanyInternalID', '=', Auth::user()->Company->InternalID)
                ->first();
        return $default;
    }

    public function depreciationHeader() {
        return $this->hasMany('DepreciationHeader', 'GroupDepreciationInternalID', 'InternalID');
    }
}
