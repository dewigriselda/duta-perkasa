<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Slip extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_slip';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showSlip() {
        return Slip::all();
    }

    public static function coa($idSlip) {
        $result = DB::select(DB::raw('select `m_slip`.*, `m_coa`.`COAName`, `m_coa`.`InternalID` as COAInternalID '
                                . 'from `m_slip` '
                                . 'inner join `m_coa` on `m_slip`.`ACC1InternalID` = `m_coa`.`ACC1InternalID` '
                                . 'AND `m_slip`.`ACC2InternalID` = `m_coa`.`ACC2InternalID` '
                                . 'AND `m_slip`.`ACC3InternalID` = `m_coa`.`ACC3InternalID` '
                                . 'AND `m_slip`.`ACC4InternalID` = `m_coa`.`ACC4InternalID` '
                                . 'AND `m_slip`.`ACC5InternalID` = `m_coa`.`ACC5InternalID` '
                                . 'AND `m_slip`.`ACC6InternalID` = `m_coa`.`ACC6InternalID` '
                                . 'where `m_slip`.`InternalID` = ' . $idSlip . ' '
                                . 'AND `m_slip`.`CompanyInternalID` = ' . Auth::user()->Company->InternalID . ' '));
        return $result;
    }

    public static function coaID() {
        $result = DB::select(DB::raw('select m_slip.InternalID, CONCAT(m_coa.ACC1InternalID,"-",m_coa.ACC2InternalID,"-",m_coa.ACC3InternalID,"-",'
                                . 'm_coa.ACC4InternalID,"-",m_coa.ACC5InternalID,"-",m_coa.ACC6InternalID) as COAID, m_coa.COAName '
                                . 'from `m_slip` '
                                . 'inner join `m_coa` on `m_slip`.`ACC1InternalID` = `m_coa`.`ACC1InternalID` '
                                . 'AND `m_slip`.`ACC2InternalID` = `m_coa`.`ACC2InternalID` '
                                . 'AND `m_slip`.`ACC3InternalID` = `m_coa`.`ACC3InternalID` '
                                . 'AND `m_slip`.`ACC4InternalID` = `m_coa`.`ACC4InternalID` '
                                . 'AND `m_slip`.`ACC5InternalID` = `m_coa`.`ACC5InternalID` '
                                . 'AND `m_slip`.`ACC6InternalID` = `m_coa`.`ACC6InternalID` '
                                . 'AND `m_slip`.`CompanyInternalID` = ' . Auth::user()->Company->InternalID . ' '));
        return $result;
    }

    public static function reportSlip($slip, $start, $end)
    {
        return JournalDetail::join("t_journal_header", 't_journal_detail.JournalInternalID', '=', 't_journal_header.InternalID')
                ->whereBetween("t_journal_header.JournalDate", Array($start, $end))
                ->where('t_journal_detail.ACC1InternalID', $slip->ACC1InternalID)
                ->where('t_journal_detail.ACC2InternalID', $slip->ACC2InternalID)
                ->where("t_journal_detail.ACC3InternalID", $slip->ACC3InternalID)
                ->where('t_journal_detail.ACC4InternalID', $slip->ACC4InternalID)
                ->where('t_journal_detail.ACC5InternalID', $slip->ACC5InternalID)
                ->where('t_journal_detail.ACC6InternalID', $slip->ACC6InternalID)
                ->orderBy('t_journal_header.JournalDate', 'ASC')
                ->get();
    }
    
    public static function reportSlipInitial($slip, $start, $end)
    {
        return JournalDetail::join("t_journal_header", 't_journal_detail.JournalInternalID', '=', 't_journal_header.InternalID')
                ->where("t_journal_header.JournalDate", '<', $start)
                ->where('t_journal_detail.ACC1InternalID', $slip->ACC1InternalID)
                ->where('t_journal_detail.ACC2InternalID', $slip->ACC2InternalID)
                ->where("t_journal_detail.ACC3InternalID", $slip->ACC3InternalID)
                ->where('t_journal_detail.ACC4InternalID', $slip->ACC4InternalID)
                ->where('t_journal_detail.ACC5InternalID', $slip->ACC5InternalID)
                ->where('t_journal_detail.ACC6InternalID', $slip->ACC6InternalID)
                ->get();
    }
    
    public static function getInternalCoa($id) {
        $default = Slip::select(DB::raw('ACC1InternalID,ACC2InternalID,ACC3InternalID,ACC4InternalID,ACC5InternalID,ACC6InternalID'))
                ->where('InternalID', '=', $id)
                ->where('CompanyInternalID', '=', Auth::user()->Company->InternalID)
                ->first();
        return $default;
    }

    public function currency() {
        return $this->belongsTo('Currency', 'CurrencyInternalID', 'InternalID');
    }

    public function journalHeader() {
        return $this->hasMany('JournalHeader', 'SlipInternalID', 'InternalID');
    }

}
