<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class TopUp extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_topup';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showTopup() {
        return Topup::all();
    }
               
    public function slip() {
        return $this->belongsTo('Slip', 'SlipInternalID', 'InternalID');
    }

    public function currency() {
        return $this->belongsTo('Currency', 'CurrencyInternalID', 'InternalID');
    }

    public function customer() {
        return $this->belongsTo('Coa6', 'ACC6InternalID', 'InternalID');
    }

    public function journalDetail() {
        return $this->hasMany('JournalDetail', 'CustomertopupInternalID', 'InternalID');
    }

    public function salesHeader() {
        return $this->hasMany('SalesHeader', 'CustomertopupInternalID', 'InternalID');
    }

    public function region() {
        return $this->hasMany('Region', 'CustomertopupInternalID', 'InternalID');
    }
    
    public function salesOrder(){
        return $this->belongsTo('SalesOrderHeader','SalesOrderInternalID','InternalID');
    }
    
    public static function getIdtopUp($salesOrderID) {
        $internalID = TopUp::where('TopUpID', '=', $salesOrderID)->where('CompanyInternalID', Auth::user()->Company->InternalID)->pluck('InternalID');
        return $internalID;
    }
    
    public static function getNextIDCustomerTopUp($text) {
        $query = 'SELECT TopUpID From m_topup Where Type = "customer" AND TopUpID LIKE "' . $text . '%" AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" order by TopUpID desc';
        $salesOrderID = DB::select(DB::raw($query));

        if (count($salesOrderID) <= 0) {
            $salesOrderID = '';
        } else {
            $salesOrderID = $salesOrderID[0]->TopUpID;
        }

        if ($salesOrderID == '') {
            $salesOrderID = $text . '0001';
        } else {
            $textTamp = $salesOrderID;
            $salesOrderID = substr($textTamp, (strlen($textTamp) - 4), 4) + 1;
            $salesOrderID = str_pad($salesOrderID, 4, '0', STR_PAD_LEFT);
            $salesOrderID = $text . $salesOrderID;
        }
        return $salesOrderID;
    }
    public static function getNextIDSupplierTopUp($text) {
        $query = 'SELECT TopUpID From m_topup Where Type = "supplier" AND TopUpID LIKE "' . $text . '%" AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" order by TopUpID desc';
        $salesOrderID = DB::select(DB::raw($query));

        if (count($salesOrderID) <= 0) {
            $salesOrderID = '';
        } else {
            $salesOrderID = $salesOrderID[0]->TopUpID;
        }

        if ($salesOrderID == '') {
            $salesOrderID = $text . '0001';
        } else {
            $textTamp = $salesOrderID;
            $salesOrderID = substr($textTamp, (strlen($textTamp) - 4), 4) + 1;
            $salesOrderID = str_pad($salesOrderID, 4, '0', STR_PAD_LEFT);
            $salesOrderID = $text . $salesOrderID;
        }
        return $salesOrderID;
    }
    
    public static function outstandingDP(){
        $query = "SELECT a.SalesOrderDate,a.ExpiredDate,a.DownPayment,a.GrandTotal, a.SalesOrderID,sum(b.GrandTotalTopUp) as 'total' "
                . " FROM m_customertopup b"
                . " INNER JOIN t_salesorder_header a"
                . " ON a.InternalID=b.SalesOrderInternalID"
                . " where b.PaymentType=2"
                . " GROUP BY b.SalesOrderInternalID"
                . " HAVING sum(b.GrandTotalTopUp) > 0";
        $result = DB::select(DB::raw($query));
        return $result;
    }
    
    public static function getDepositPayable(){
        $query = "select a.*,a.TopUpID as ID,a.TopUpDate as Date,b.ACC6Name as coa6,sum(a.GrandTotalTopUp) as 'grandtotal',cur.CurrencyName,cur.Rate from m_customertopup a "
                . "INNER JOIN m_coa6 b ON a.ACC6InternalID=b.InternalID "
                . "INNER JOIN m_currency as cur on cur.InternalID = a.CurrencyInternalID "
                . "WHERE a.PaymentType=1 "
                . "GROUP BY a.ACC6InternalID";
        
        $result = DB::select(DB::raw($query));
        return $result;
    }
    
    public static function outstandingCustDP($customer){
        $query = "SELECT a.SalesOrderDate,a.ExpiredDate,a.DownPayment,a.GrandTotal, a.SalesOrderID,sum(b.GrandTotalTopUp) as 'total' "
                . " FROM m_customertopup b"
                . " INNER JOIN t_salesorder_header a"
                . " ON a.InternalID=b.SalesOrderInternalID"
                . " where b.PaymentType=2"
                . " and b.ACC6InternalID='".$customer."'"
                . " GROUP BY b.SalesOrderInternalID"
                . " HAVING sum(b.GrandTotalTopUp) > 0";
        $result = DB::select(DB::raw($query));
        return $result;
    }
    
    public static function outstandingDeposit($customer,$startDate,$endDate){
        
        $query = "SELECT TopUpID,TopUpdate,GrandTotalTopUp from m_customertopup "
                . "WHERE PaymentType='1' "
                . "AND SalesOrderInternalID IS NULL "
                . "AND ACC6InternalID ='".$customer."' "
                . "AND Void ='Active' "
                . "AND TopUpdate Between '".$startDate."' and '".$endDate."'"
                . "ORDER BY TopUpdate desc";
        $deposit = DB::select(DB::raw($query));
        
        $query = "SELECT TopUpID,TopUpdate,GrandTotalTopUp from m_customertopup "
                . "WHERE PaymentType='1' "
                . "AND ACC6InternalID ='".$customer."' "
                . "AND Void ='Active' "
                . "AND TopUpdate Between '".$startDate."' and '".$endDate."'"
                . "AND SalesOrderInternalID !=''";
        $depositSO = DB::select(DB::raw($query));
        
         $tamp = array();
        $depositNominal=0;
        $gDeposit = 0;
        foreach($depositSO as $dataSO){
                 $depositNominal += $dataSO->GrandTotalTopUp; 
          }
        foreach($deposit as $data){
            $gDeposit += $data->GrandTotalTopUp;

        }
//        dd($depositNominal);
        $tamp=array(
                'grandTotal'=>$gDeposit,
                'deposit'=> $gDeposit+$depositNominal
            );
        return $tamp;
        
    }
    
    public static function getTopUpPayable() {
        $query = 'SELECT th.*, th.TopUpID as ID, th.TopUpDate as Date, coa.ACC6Name as coa6, cur.CurrencyName '
                . 'FROM m_topup th INNER JOIN m_coa6 as coa on th.ACC6InternalID = coa.InternalID '
                . 'INNER JOIN m_currency as cur on cur.InternalID = th.CurrencyInternalID '
                . 'where th.PaymentType != 0 AND th.Type = "supplier"'
                . 'AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '" '
                . 'AND th.`GrandTotalTopUp`*th.`CurrencyRate` > (Select IFNULL(SUM(td.JournalDebet),0) From t_journal_detail td Where td.JournalTransactionID = th.`TopUpID`) '
                . 'order by coa.ACC6Name Asc, th.InternalID DESC';
        $purchase = DB::select(DB::raw($query));
        $tamp = array();
//        $value->LongTerm
        foreach ($purchase as $value) {
            $data = date("Y-m-d", strtotime("+" . 0 . " day", strtotime($value->Date)));
            array_push($tamp, $data);
        }
        array_multisort($tamp, $purchase);
        return $purchase;
    }
    
    public static function getTopUpReceivable() {
        $query = 'SELECT th.*, th.TopUpID as ID, th.TopUpDate as Date, coa.ACC6Name as coa6, cur.CurrencyName '
                . 'FROM m_topup th INNER JOIN m_coa6 as coa on th.ACC6InternalID = coa.InternalID '
                . 'INNER JOIN m_currency as cur on cur.InternalID = th.CurrencyInternalID '
                . 'where th.PaymentType != 0 AND th.Type = "customer"'
                . 'AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '" '
                . 'AND th.`GrandTotalTopUp`*th.`CurrencyRate` > (Select IFNULL(SUM(td.JournalCredit),0) From t_journal_detail td Where td.JournalTransactionID = th.`TopUpID`) '
                . 'order by coa.ACC6Name Asc, th.InternalID DESC';
        $sales = DB::select(DB::raw($query));
        $tamp = array();
        foreach ($sales as $value) {
            $data = date("Y-m-d", strtotime("+" . 0 . " day", strtotime($value->Date)));
            array_push($tamp, $data);
        }
        array_multisort($tamp,$sales);
        return $sales;
    }
}
