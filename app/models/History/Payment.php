<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Payment extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'h_payment';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';
    
    public static function showPayment() {
        return Payment::all();
    }
    
    public static function getNextIDPayment($text, $CompanyInternalID) {
        $query = 'SELECT PaymentID From h_payment Where PaymentID LIKE "' . $text . '%" AND CompanyInternalID = "' . $CompanyInternalID . '" order by PaymentID desc';
        $paymentID = DB::select(DB::raw($query));

        if (count($paymentID) <= 0) {
            $paymentID = '';
        } else {
            $paymentID = $paymentID[0]->PaymentID;
        }

        if ($paymentID == '') {
            $paymentID = $text . '0001';
        } else {
            $textTamp = $paymentID;
            $paymentID = substr($textTamp, (strlen($textTamp) - 4), 4) + 1;
            $paymentID = str_pad($paymentID, 4, '0', STR_PAD_LEFT);
            $paymentID = $text . $paymentID;
        }
        return $paymentID;
    }
    
    public function company() {
        return $this->belongsTo('Company', 'CompanyInternalID', 'InternalID');
    }
}