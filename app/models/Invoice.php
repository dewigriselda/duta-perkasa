<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Invoice extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_invoice';
    public $timestamps = false;
    protected $primaryKey = 'InternalID';
    
    public static function showInvoice() {
        return Invoice::all();
    }
    
    public static function getNextIDInvoice($text, $CompanyInternalID) {
        $query = 'SELECT InvoiceID From t_invoice Where InvoiceID LIKE "' . $text . '%" AND CompanyInternalID = "' . $CompanyInternalID . '" order by InvoiceID desc';
        $invoiceID = DB::select(DB::raw($query));

        if (count($invoiceID) <= 0) {
            $invoiceID = '';
        } else {
            $invoiceID = $invoiceID[0]->InvoiceID;
        }

        if ($invoiceID == '') {
            $invoiceID = $text . '0001';
        } else {
            $textTamp = $invoiceID;
            $invoiceID = substr($textTamp, (strlen($textTamp) - 4), 4) + 1;
            $invoiceID = str_pad($invoiceID, 4, '0', STR_PAD_LEFT);
            $invoiceID = $text . $invoiceID;
        }
        return $invoiceID;
    }
    
    public function company() {
        return $this->belongsTo('Company', 'CompanyInternalID', 'InternalID');
    }
}