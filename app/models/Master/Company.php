<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Company extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_company';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';


    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showCompany() {
        return Company::all();
    }
    
    public function user() {
        return $this->hasMany('User', 'CompanyInternalID', 'InternalID');
    }
    
    public function invoice() {
        return $this->hasMany('Invoice', 'CompanyInternalID', 'InternalID');
    }
    
    public function package() {
        return $this->belongsTo('Package', 'PackageInternalID', 'InternalID');
    }
    
    public function region() {
        return $this->belongsTo('Region', 'RegionInternalID', 'InternalID');
    }
}
