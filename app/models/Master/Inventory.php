<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Inventory extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_inventory';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showInventory() {
        return Inventory::all();
    }

    public static function getInitialStockBaru($inv, $date, $ware, $delware) {
        $in = DB::table('v_wreportstock')->where('Type', "in")->where("Date", "<", $date)
                        ->where("Warehouse", $delware, $ware)
                        ->where("Inventory", $inv)->sum("Qty");
        $out = DB::table('v_wreportstock')->where('Type', "out")->where("Date", "<", $date)
                        ->where("Warehouse", $delware, $ware)
                        ->where("Inventory", $inv)->sum("Qty");
        $ini = $in - $out;
        return $ini;
    }

    public static function getEndStock($inv, $dateawal, $dateakhir, $ware, $delware) {
        $in = DB::table('v_wreportstock')->join("m_warehouse", "m_warehouse.InternalID", "=", "v_wreportstock.Warehouse")
                        ->where("m_warehouse.Type", Auth::user()->WarehouseCheck)
                        ->where('v_wreportstock.Type', "in")->whereBetween('Date', array($dateawal, $dateakhir))
                        ->where("Warehouse", $delware, $ware)
                        ->where("Inventory", $inv)->sum("Qty");
        $out = DB::table('v_wreportstock')->join("m_warehouse", "m_warehouse.InternalID", "=", "v_wreportstock.Warehouse")
                        ->where("m_warehouse.Type", Auth::user()->WarehouseCheck)
                        ->where('v_wreportstock.Type', "out")->whereBetween('Date', array($dateawal, $dateakhir))
                        ->where("Warehouse", $delware, $ware)
                        ->where("Inventory", $inv)->sum("Qty");
        $ini = $in - $out;
        return $ini;
    }

    public static function getEndStockNow($inv, $date, $ware, $delware) {
        if ($ware == '-1') {
            $in = DB::table('v_wreportstock')->join("m_warehouse", "m_warehouse.InternalID", "=", "v_wreportstock.Warehouse")
                    ->where("m_warehouse.Type", Auth::user()->WarehouseCheck)
                    ->where('v_wreportstock.Type', "in")->where('Date', "<=", $date)
//                ->where("Warehouse", $delware, $ware)
                    ->where("Inventory", $inv)
//                ->toSql();
                    ->sum("Qty");
            $out = DB::table('v_wreportstock')->join("m_warehouse", "m_warehouse.InternalID", "=", "v_wreportstock.Warehouse")
                    ->where("m_warehouse.Type", Auth::user()->WarehouseCheck)
                    ->where('v_wreportstock.Type', "out")->where('Date', "<=", $date)
//                ->where("Warehouse", $delware, $ware)
                    ->where("Inventory", $inv)
//                ->toSql();
                    ->sum("Qty");
        } else {
            $in = DB::table('v_wreportstock')->join("m_warehouse", "m_warehouse.InternalID", "=", "v_wreportstock.Warehouse")
                    ->where("m_warehouse.Type", Auth::user()->WarehouseCheck)
                    ->where('v_wreportstock.Type', "in")->where('Date', "<=", $date)
                    ->where("Warehouse", $delware, $ware)
                    ->where("Inventory", $inv)
//                ->toSql();
                    ->sum("Qty");
            $out = DB::table('v_wreportstock')->join("m_warehouse", "m_warehouse.InternalID", "=", "v_wreportstock.Warehouse")
                    ->where("m_warehouse.Type", Auth::user()->WarehouseCheck)
                    ->where('v_wreportstock.Type', "out")->where('Date', "<=", $date)
                    ->where("Warehouse", $delware, $ware)
                    ->where("Inventory", $inv)
//                ->toSql();
                    ->sum("Qty");
        }
//        dd($in."<br>".$out);
        $ini = $in - $out;
        return $ini;
    }

    public static function stockWarehouseNow($inv, $date, $ware) {
        $in = DB::table('v_wreportstock')->where('Type', "in")->where('Date', "<=", $date)
                ->where("Warehouse", $ware)
                ->where("Inventory", $inv)
//                 ->toSql();
                ->sum("Qty");
        $out = DB::table('v_wreportstock')->where('Type', "out")->where('Date', "<=", $date)
                ->where("Warehouse", $ware)
                ->where("Inventory", $inv)
//                 ->toSql();
                ->sum("Qty");
//         dd($in."<br>".$out);
        $ini = $in - $out;
        return $ini;
    }

    public static function getMRV($inv, $bulan, $tahun, $ware, $delware) {
        $in = DB::table('v_wreportstock')->where('Type', "in")
                        ->whereRaw("MONTH(Date) = " . $bulan)->whereRaw("YEAR(Date) = " . $tahun)
                        ->where("Warehouse", $delware, $ware)
                        ->where("ID", "like", "%MRV%")
                        ->where("Inventory", $inv)->sum("Qty");
        return $in;
    }

    public static function getShipping($inv, $bulan, $tahun, $ware, $delware) {
        $out = DB::table('v_wreportstock')
                ->whereRaw("MONTH(Date) = " . $bulan)->whereRaw("YEAR(Date) = " . $tahun)
                ->where("Warehouse", $delware, $ware)
                ->where("ID", "like", "%SH%")
                ->where("Inventory", $inv)
                ->orWhere("ID", "like", "%SHN%")
                ->where("Warehouse", $delware, $ware)
                ->whereRaw("MONTH(Date) = " . $bulan)->whereRaw("YEAR(Date) = " . $tahun)
                ->where("Inventory", $inv)
                ->sum("Qty");
        return $out;
    }

    public static function getSR($inv, $bulan, $tahun, $ware, $delware) {
        $in = DB::table('v_wreportstock')->where('Type', "in")
                        ->whereRaw("MONTH(Date) = " . $bulan)->whereRaw("YEAR(Date) = " . $tahun)
                        ->where("Warehouse", $delware, $ware)
                        ->where("ID", "like", "%R.%")
                        ->where("Inventory", $inv)->sum("Qty");
        return $in;
    }

    public static function getPR($inv, $bulan, $tahun, $ware, $delware) {
        $in = DB::table('v_wreportstock')->where('Type', "out")
                        ->whereRaw("MONTH(Date) = " . $bulan)->whereRaw("YEAR(Date) = " . $tahun)
                        ->where("Warehouse", $delware, $ware)
                        ->where("ID", "like", "%R.%")
                        ->where("Inventory", $inv)->sum("Qty");
        return $in;
    }

    public static function getMemoIn($inv, $bulan, $tahun, $ware, $delware) {
        $in = DB::table('v_wreportstock')->where('Type', "in")
                        ->whereRaw("MONTH(Date) = " . $bulan)->whereRaw("YEAR(Date) = " . $tahun)
                        ->where("Warehouse", $delware, $ware)
                        ->where("ID", "like", "%ME-IN%")
                        ->where("Inventory", $inv)->sum("Qty");
        return $in;
    }

    public static function getMemoOut($inv, $bulan, $tahun, $ware, $delware) {
        $in = DB::table('v_wreportstock')->where('Type', "out")
                        ->whereRaw("MONTH(Date) = " . $bulan)->whereRaw("YEAR(Date) = " . $tahun)
                        ->where("Warehouse", $delware, $ware)
                        ->where("ID", "like", "%ME-OUT%")
                        ->where("Inventory", $inv)->sum("Qty");
        return $in;
    }

    public static function getTransformationIn($inv, $bulan, $tahun, $ware, $delware) {
        $in = DB::table('v_wreportstock')->where('Type', "in")
                        ->whereRaw("MONTH(Date) = " . $bulan)->whereRaw("YEAR(Date) = " . $tahun)
                        ->where("Warehouse", $delware, $ware)
                        ->where("ID", "like", "%TAF%")
                        ->where("Inventory", $inv)->sum("Qty");
        return $in;
    }

    public static function getTransformationOut($inv, $bulan, $tahun, $ware, $delware) {
        $in = DB::table('v_wreportstock')->where('Type', "out")
                        ->whereRaw("MONTH(Date) = " . $bulan)->whereRaw("YEAR(Date) = " . $tahun)
                        ->where("Warehouse", $delware, $ware)
                        ->where("ID", "like", "%TAF%")
                        ->where("Inventory", $inv)->sum("Qty");
        return $in;
    }

    public static function getTransferIn($inv, $bulan, $tahun, $ware, $delware) {
        $in = DB::table('v_wreportstock')->where('Type', "in")
                        ->whereRaw("MONTH(Date) = " . $bulan)->whereRaw("YEAR(Date) = " . $tahun)
                        ->where("Warehouse", $delware, $ware)
                        ->where("ID", "like", "%TF%")
                        ->where("Inventory", $inv)->sum("Qty");
        return $in;
    }

    public static function getTransferOut($inv, $bulan, $tahun, $ware, $delware) {
        $in = DB::table('v_wreportstock')->where('Type', "out")
                        ->whereRaw("MONTH(Date) = " . $bulan)->whereRaw("YEAR(Date) = " . $tahun)
                        ->where("Warehouse", $delware, $ware)
                        ->where("ID", "like", "%TF%")
                        ->where("Inventory", $inv)->sum("Qty");
        return $in;
    }

    public static function getInitialStock($date, $if, $internal, $warehouse, $delimiterWarehouse) {
        $initial = Inventory::find($internal)->InitialQuantity;
        $tamp = 0;
        foreach (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->where('InventoryInternalID', $internal)->get() as $data) {
            $uom = $data->UomInternalID;
            $pembelian = MrvDetail::
                    join('t_mrv_header', 't_mrv_header.InternalID', '=', 't_mrv_detail.MrvInternalID')
                    ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_mrv_header.WarehouseInternalID')
                    ->where('m_warehouse.Type', Auth::user()->WarehouseCheck)
                    ->where('t_mrv_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_mrv_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_mrv_detail.InventoryInternalID', $internal)
                    ->where('t_mrv_detail.UomInternalID', $uom)
                    ->where('t_mrv_header.MrvDate', $if, $date)
                    ->sum('Qty');
//            $pembelian = PurchaseDetail::
//                    join('t_purchase_header', 't_purchase_header.InternalID', '=', 't_purchase_detail.PurchaseInternalID')
//                    ->where('t_purchase_header.CompanyInternalID', Auth::user()->Company->InternalID)
//                    ->where('t_purchase_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
//                    ->where('t_purchase_detail.InventoryInternalID', $internal)
//                    ->where('t_purchase_detail.UomInternalID', $uom)
//                    ->where('t_purchase_header.PurchaseDate', $if, $date)
//                    ->sum('Qty');
//            $penjualan = SalesDetail::
//                    join('t_sales_header', 't_sales_header.InternalID', '=', 't_sales_detail.SalesInternalID')
//                    ->where('t_sales_header.CompanyInternalID', Auth::user()->Company->InternalID)
//                    ->where('t_sales_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
//                    ->where('t_sales_detail.InventoryInternalID', $internal)
//                    ->where('t_sales_detail.UomInternalID', $uom)
//                    ->where('t_sales_header.SalesDate', $if, $date)
//                    ->sum('Qty');
            $penjualan = ShippingAddDetail::
                    join('t_shipping_header', 't_shipping_header.InternalID', '=', 't_shipping_detail.ShippingInternalID')
                    ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_shipping_header.WarehouseInternalID')
                    ->where('m_warehouse.Type', Auth::user()->WarehouseCheck)
                    ->where('t_shipping_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_shipping_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_shipping_detail.InventoryInternalID', $internal)
                    ->where('t_shipping_detail.UomInternalID', $uom)
                    ->where('t_shipping_header.ShippingDate', $if, $date)
                    ->sum('Qty');
            $Rpembelian = PurchaseReturnDetail::
                    join('t_purchasereturn_header', 't_purchasereturn_header.InternalID', '=', 't_purchasereturn_detail.PurchaseReturnInternalID')
                    ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_purchasereturn_header.WarehouseInternalID')
                    ->where('m_warehouse.Type', Auth::user()->WarehouseCheck)
                    ->where('t_purchasereturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_purchasereturn_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_purchasereturn_detail.InventoryInternalID', $internal)
                    ->where('t_purchasereturn_detail.UomInternalID', $uom)
                    ->where('t_purchasereturn_header.PurchaseReturnDate', $if, $date)
                    ->sum('Qty');
            $Rpenjualan = SalesReturnDetail::
                    join('t_salesreturn_header', 't_salesreturn_header.InternalID', '=', 't_salesreturn_detail.SalesReturnInternalID')
                    ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_salesreturn_header.WarehouseInternalID')
                    ->where('m_warehouse.Type', Auth::user()->WarehouseCheck)
                    ->where('t_salesreturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_salesreturn_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_salesreturn_detail.InventoryInternalID', $internal)
                    ->where('t_salesreturn_detail.UomInternalID', $uom)
                    ->where('t_salesreturn_header.SalesReturnDate', $if, $date)
                    ->sum('Qty');
            $Min = MemoInDetail::
                    join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.MemoInInternalID')
                    ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_memoin_header.WarehouseInternalID')
                    ->where('m_warehouse.Type', Auth::user()->WarehouseCheck)
                    ->where('t_memoin_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_memoin_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_memoin_detail.InventoryInternalID', $internal)
                    ->where('t_memoin_detail.UomInternalID', $uom)
                    ->where('t_memoin_header.MemoInDate', $if, $date)
                    ->sum('Qty');
            $Mout = MemoOutDetail::
                    join('t_memoout_header', 't_memoout_header.InternalID', '=', 't_memoout_detail.MemoOutInternalID')
                    ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_memoout_header.WarehouseInternalID')
                    ->where('m_warehouse.Type', Auth::user()->WarehouseCheck)
                    ->where('t_memoout_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_memoout_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_memoout_detail.InventoryInternalID', $internal)
                    ->where('t_memoout_detail.UomInternalID', $uom)
                    ->where('t_memoout_header.MemoOutDate', $if, $date)
                    ->sum('Qty');
            $Tin = TransferDetail::
                    join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                    ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_transfer_header.WarehouseDestinyInternalID')
                    ->where('m_warehouse.Type', Auth::user()->WarehouseCheck)
                    ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_transfer_header.WarehouseDestinyInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_transfer_detail.InventoryInternalID', $internal)
                    ->where('t_transfer_detail.UomInternalID', $uom)
                    ->where('t_transfer_header.TransferDate', $if, $date)
                    ->sum('Qty');
            $Tout = TransferDetail::
                    join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                    ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_transfer_header.WarehouseInternalID')
                    ->where('m_warehouse.Type', Auth::user()->WarehouseCheck)
                    ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_transfer_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_transfer_detail.InventoryInternalID', $internal)
                    ->where('t_transfer_detail.UomInternalID', $uom)
                    ->where('t_transfer_header.TransferDate', $if, $date)
                    ->sum('Qty');
            $ConvIn = Convertion::
                    join('m_inventory_uom', 'm_inventory_uom.InternalID', '=', 'h_convertion.InventoryUomInternalID2')
                    ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('h_convertion.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('h_convertion.InventoryUomInternalID2', $data->InternalID)
                    ->where('h_convertion.DateConvertion', $if, $date)
                    ->sum('QuantityResult');
            $ConvOut = Convertion::
                    join('m_inventory_uom', 'm_inventory_uom.InternalID', '=', 'h_convertion.InventoryUomInternalID1')
                    ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('h_convertion.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('h_convertion.InventoryUomInternalID1', $data->InternalID)
                    ->where('h_convertion.DateConvertion', $if, $date)
                    ->sum('Quantity');
            $Trin = TransformationDetail::
                    join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                    ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_transformation_detail.WarehouseInternalID')
                    ->where('m_warehouse.Type', Auth::user()->WarehouseCheck)
                    ->where('t_transformation_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_transformation_detail.WarehouseInternalID', $warehouse)
                    ->where('t_transformation_detail.Type', "in")
                    ->where('t_transformation_detail.InventoryInternalID', $data->InventoryInternalID)
                    ->where('t_transformation_detail.UomInternalID', $data->UomInternalID)
                    ->where('t_transformation_header.TransformationDate', $if, $date)
                    ->sum('Qty');
            $Trout = TransformationDetail::
                    join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                    ->join('m_warehouse', 'm_warehouse.InternalID', '=', 't_transformation_detail.WarehouseInternalID')
                    ->where('m_warehouse.Type', Auth::user()->WarehouseCheck)
                    ->where('t_transformation_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_transformation_detail.WarehouseInternalID', $warehouse)
                    ->where('t_transformation_detail.Type', "out")
                    ->where('t_transformation_detail.InventoryInternalID', $data->InventoryInternalID)
                    ->where('t_transformation_detail.UomInternalID', $data->UomInternalID)
                    ->where('t_transformation_header.TransformationDate', $if, $date)
                    ->sum('Qty');
            $tamp += ($pembelian + $Rpenjualan - $penjualan - $Rpembelian + $Min - $Mout + $Tin - $Tout + $ConvIn - $ConvOut + $Trin - $Trout) * $data->Value;
//            return $penjualan;
        }
        return $tamp + $initial;
    }

    public static function getInitialStockWithoutShipping($date, $if, $internal, $warehouse, $delimiterWarehouse, $shipping) {
        $initial = Inventory::find($internal)->InitialQuantity;
        $tamp = 0;
        foreach (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->where('InventoryInternalID', $internal)->get() as $data) {
            $uom = $data->UomInternalID;
            $pembelian = MrvDetail::
                    join('t_mrv_header', 't_mrv_header.InternalID', '=', 't_mrv_detail.MrvInternalID')
                    ->where('t_mrv_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_mrv_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_mrv_detail.InventoryInternalID', $internal)
                    ->where('t_mrv_detail.UomInternalID', $uom)
                    ->where('t_mrv_header.MrvDate', $if, $date)
                    ->sum('Qty');
//            $pembelian = PurchaseDetail::
//                    join('t_purchase_header', 't_purchase_header.InternalID', '=', 't_purchase_detail.PurchaseInternalID')
//                    ->where('t_purchase_header.CompanyInternalID', Auth::user()->Company->InternalID)
//                    ->where('t_purchase_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
//                    ->where('t_purchase_detail.InventoryInternalID', $internal)
//                    ->where('t_purchase_detail.UomInternalID', $uom)
//                    ->where('t_purchase_header.PurchaseDate', $if, $date)
//                    ->sum('Qty');
//            $penjualan = SalesDetail::
//                    join('t_sales_header', 't_sales_header.InternalID', '=', 't_sales_detail.SalesInternalID')
//                    ->where('t_sales_header.CompanyInternalID', Auth::user()->Company->InternalID)
//                    ->where('t_sales_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
//                    ->where('t_sales_detail.InventoryInternalID', $internal)
//                    ->where('t_sales_detail.UomInternalID', $uom)
//                    ->where('t_sales_header.SalesDate', $if, $date)
//                    ->sum('Qty');
            $penjualan = ShippingAddDetail::
                    join('t_shipping_header', 't_shipping_header.InternalID', '=', 't_shipping_detail.ShippingInternalID')
                    ->where('t_shipping_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_shipping_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_shipping_detail.InventoryInternalID', $internal)
                    ->where('t_shipping_detail.UomInternalID', $uom)
                    ->where('t_shipping_header.ShippingDate', $if, $date)
                    ->where('t_shipping_header.InternalID', "!=", $shipping)
                    ->sum('Qty');
            $Rpembelian = PurchaseReturnDetail::
                    join('t_purchasereturn_header', 't_purchasereturn_header.InternalID', '=', 't_purchasereturn_detail.PurchaseReturnInternalID')
                    ->where('t_purchasereturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_purchasereturn_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_purchasereturn_detail.InventoryInternalID', $internal)
                    ->where('t_purchasereturn_detail.UomInternalID', $uom)
                    ->where('t_purchasereturn_header.PurchaseReturnDate', $if, $date)
                    ->sum('Qty');
            $Rpenjualan = SalesReturnDetail::
                    join('t_salesreturn_header', 't_salesreturn_header.InternalID', '=', 't_salesreturn_detail.SalesReturnInternalID')
                    ->where('t_salesreturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_salesreturn_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_salesreturn_detail.InventoryInternalID', $internal)
                    ->where('t_salesreturn_detail.UomInternalID', $uom)
                    ->where('t_salesreturn_header.SalesReturnDate', $if, $date)
                    ->sum('Qty');
            $Min = MemoInDetail::
                    join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.MemoInInternalID')
                    ->where('t_memoin_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_memoin_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_memoin_detail.InventoryInternalID', $internal)
                    ->where('t_memoin_detail.UomInternalID', $uom)
                    ->where('t_memoin_header.MemoInDate', $if, $date)
                    ->sum('Qty');
            $Mout = MemoOutDetail::
                    join('t_memoout_header', 't_memoout_header.InternalID', '=', 't_memoout_detail.MemoOutInternalID')
                    ->where('t_memoout_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_memoout_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_memoout_detail.InventoryInternalID', $internal)
                    ->where('t_memoout_detail.UomInternalID', $uom)
                    ->where('t_memoout_header.MemoOutDate', $if, $date)
                    ->sum('Qty');
            $Tin = TransferDetail::
                    join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                    ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_transfer_header.WarehouseDestinyInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_transfer_detail.InventoryInternalID', $internal)
                    ->where('t_transfer_detail.UomInternalID', $uom)
                    ->where('t_transfer_header.TransferDate', $if, $date)
                    ->sum('Qty');
            $Tout = TransferDetail::
                    join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                    ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_transfer_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_transfer_detail.InventoryInternalID', $internal)
                    ->where('t_transfer_detail.UomInternalID', $uom)
                    ->where('t_transfer_header.TransferDate', $if, $date)
                    ->sum('Qty');
            $ConvIn = Convertion::
                    join('m_inventory_uom', 'm_inventory_uom.InternalID', '=', 'h_convertion.InventoryUomInternalID2')
                    ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('h_convertion.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('h_convertion.InventoryUomInternalID2', $data->InternalID)
                    ->where('h_convertion.DateConvertion', $if, $date)
                    ->sum('QuantityResult');
            $ConvOut = Convertion::
                    join('m_inventory_uom', 'm_inventory_uom.InternalID', '=', 'h_convertion.InventoryUomInternalID1')
                    ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('h_convertion.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('h_convertion.InventoryUomInternalID1', $data->InternalID)
                    ->where('h_convertion.DateConvertion', $if, $date)
                    ->sum('Quantity');
            $Trin = TransformationDetail::
                    join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                    ->where('t_transformation_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_transformation_detail.WarehouseInternalID', $warehouse)
                    ->where('t_transformation_detail.Type', "in")
                    ->where('t_transformation_detail.InventoryInternalID', $data->InventoryInternalID)
                    ->where('t_transformation_detail.UomInternalID', $data->UomInternalID)
                    ->where('t_transformation_header.TransformationDate', $if, $date)
                    ->sum('Qty');
            $Trout = TransformationDetail::
                    join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                    ->where('t_transformation_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_transformation_detail.WarehouseInternalID', $warehouse)
                    ->where('t_transformation_detail.Type', "out")
                    ->where('t_transformation_detail.InventoryInternalID', $data->InventoryInternalID)
                    ->where('t_transformation_detail.UomInternalID', $data->UomInternalID)
                    ->where('t_transformation_header.TransformationDate', $if, $date)
                    ->sum('Qty');
            $tamp += ($pembelian + $Rpenjualan - $penjualan - $Rpembelian + $Min - $Mout + $Tin - $Tout + $ConvIn - $ConvOut + $Trin - $Trout) * $data->Value;
//            return $penjualan;
        }
        return $tamp + $initial;
    }

    public static function getStockNew($id) {

        $query = "SELECT * FROM v_wstock"
                . " WHERE InventoryID='" . $id . "'";
        $result = DB::select(DB::raw($query));

        return $result;
    }

    public static function getStock($internal) {
        $initial = Inventory::find($internal)->InitialQuantity;
        $tamp = 0;
        foreach (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->where('InventoryInternalID', $internal)->get() as $data) {
            $uom = $data->UomInternalID;
            $pembelian = MrvDetail::
                    join('t_mrv_header', 't_mrv_header.InternalID', '=', 't_mrv_detail.MrvInternalID')
                    ->where('t_mrv_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_mrv_detail.InventoryInternalID', $internal)
                    ->where('t_mrv_detail.UomInternalID', $uom)
                    ->sum('Qty');
//            $pembelian = PurchaseDetail::
//                    join('t_purchase_header', 't_purchase_header.InternalID', '=', 't_purchase_detail.PurchaseInternalID')
//                    ->where('t_purchase_header.CompanyInternalID', Auth::user()->Company->InternalID)
//                    ->where('t_purchase_detail.InventoryInternalID', $internal)
//                    ->where('t_purchase_detail.UomInternalID', $uom)
//                    ->sum('Qty');
            $penjualan = ShippingAddDetail::
                    join('t_shipping_header', 't_shipping_header.InternalID', '=', 't_shipping_detail.ShippingInternalID')
                    ->where('t_shipping_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_shipping_detail.InventoryInternalID', $internal)
                    ->where('t_shipping_detail.UomInternalID', $uom)
                    ->sum('Qty');
            $Rpembelian = PurchaseReturnDetail::
                    join('t_purchasereturn_header', 't_purchasereturn_header.InternalID', '=', 't_purchasereturn_detail.PurchaseReturnInternalID')
                    ->where('t_purchasereturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_purchasereturn_detail.InventoryInternalID', $internal)
                    ->where('t_purchasereturn_detail.UomInternalID', $uom)
                    ->sum('Qty');
            $Rpenjualan = SalesReturnDetail::
                    join('t_salesreturn_header', 't_salesreturn_header.InternalID', '=', 't_salesreturn_detail.SalesReturnInternalID')
                    ->where('t_salesreturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_salesreturn_detail.InventoryInternalID', $internal)
                    ->where('t_salesreturn_detail.UomInternalID', $uom)
                    ->sum('Qty');
            $Min = MemoInDetail::
                    join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.MemoInInternalID')
                    ->where('t_memoin_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_memoin_detail.InventoryInternalID', $internal)
                    ->where('t_memoin_detail.UomInternalID', $uom)
                    ->sum('Qty');
            $Mout = MemoOutDetail::
                    join('t_memoout_header', 't_memoout_header.InternalID', '=', 't_memoout_detail.MemoOutInternalID')
                    ->where('t_memoout_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_memoout_detail.InventoryInternalID', $internal)
                    ->where('t_memoout_detail.UomInternalID', $uom)
                    ->sum('Qty');
            $Tin = TransferDetail::
                    join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                    ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_transfer_detail.InventoryInternalID', $internal)
                    ->where('t_transfer_detail.UomInternalID', $uom)
                    ->sum('Qty');
            $Tout = TransferDetail::
                    join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                    ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_transfer_detail.InventoryInternalID', $internal)
                    ->where('t_transfer_detail.UomInternalID', $uom)
                    ->sum('Qty');
            $tamp += ($pembelian + $Rpenjualan - $penjualan - $Rpembelian + $Min - $Mout + $Tin - $Tout) * $data->Value;
        }
        return $tamp + $initial;
    }

    public static function getIncomingStock($date, $if, $internal, $warehouse, $delimiterWarehouse) {
        $tamp = 0;
        foreach (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->where('InventoryInternalID', $internal)->get() as $data) {
            $uom = $data->UomInternalID;
            $pembelian = PurchaseOrderDetail::
                    join('t_purchaseorder_header', 't_purchaseorder_header.InternalID', '=', 't_purchaseorder_detail.PurchaseOrderInternalID')
                    ->whereNotIn('t_purchaseorder_header.InternalID', function($query) {
                        $query->select('PurchaseOrderInternalID')
                        ->from('t_purchase_header');
                    })
                    ->where('t_purchaseorder_header.CompanyInternalID', Auth::user()->Company->InternalID)
                    ->where('t_purchaseorder_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_purchaseorder_detail.InventoryInternalID', $internal)
                    ->where('t_purchaseorder_detail.UomInternalID', $uom)
                    ->where('t_purchaseorder_header.PurchaseOrderDate', $if, $date)
                    ->sum('Qty');
            $tamp += $pembelian * $data->Value;
        }
        return $tamp;
    }

    public static function getInitialStockSuperAdmin($date, $if, $internal, $warehouse, $delimiterWarehouse, $company) {
        $initial = Inventory::find($internal)->InitialQuantity;
        $tamp = 0;
        foreach (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->where('InventoryInternalID', $internal)->get() as $data) {
            $uom = $data->UomInternalID;
            $pembelian = MrvDetail::
                    join('t_mrv_header', 't_mrv_header.InternalID', '=', 't_mrv_detail.MrvInternalID')
                    ->where('t_mrv_header.CompanyInternalID', $company)
                    ->where('t_mrv_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_mrv_detail.InventoryInternalID', $internal)
                    ->where('t_mrv_detail.UomInternalID', $uom)
                    ->where('t_mrv_header.MrvDate', $if, $date)
                    ->sum('Qty');
//            $pembelian = PurchaseDetail::
//                    join('t_purchase_header', 't_purchase_header.InternalID', '=', 't_purchase_detail.PurchaseInternalID')
//                    ->where('t_purchase_header.CompanyInternalID', $company)
//                    ->where('t_purchase_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
//                    ->where('t_purchase_detail.InventoryInternalID', $internal)
//                    ->where('t_purchase_detail.UomInternalID', $uom)
//                    ->where('t_purchase_header.PurchaseDate', $if, $date)
//                    ->sum('Qty');
//            $penjualan = SalesDetail::
//                    join('t_sales_header', 't_sales_header.InternalID', '=', 't_sales_detail.SalesInternalID')
//                    ->where('t_sales_header.CompanyInternalID', $company)
//                    ->where('t_sales_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
//                    ->where('t_sales_detail.InventoryInternalID', $internal)
//                    ->where('t_sales_detail.UomInternalID', $uom)
//                    ->where('t_sales_header.SalesDate', $if, $date)
//                    ->sum('Qty');
            $penjualan = ShippingAddDetail::
                    join('t_shipping_header', 't_shipping_header.InternalID', '=', 't_shipping_detail.ShippingInternalID')
                    ->where('t_shipping_header.CompanyInternalID', $company)
                    ->where('t_shipping_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_shipping_detail.InventoryInternalID', $internal)
                    ->where('t_shipping_detail.UomInternalID', $uom)
                    ->where('t_shipping_header.ShippingDate', $if, $date)
                    ->sum('Qty');
            $Rpembelian = PurchaseReturnDetail::
                    join('t_purchasereturn_header', 't_purchasereturn_header.InternalID', '=', 't_purchasereturn_detail.PurchaseReturnInternalID')
                    ->where('t_purchasereturn_header.CompanyInternalID', $company)
                    ->where('t_purchasereturn_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_purchasereturn_detail.InventoryInternalID', $internal)
                    ->where('t_purchasereturn_detail.UomInternalID', $uom)
                    ->where('t_purchasereturn_header.PurchaseReturnDate', $if, $date)
                    ->sum('Qty');
            $Rpenjualan = SalesReturnDetail::
                    join('t_salesreturn_header', 't_salesreturn_header.InternalID', '=', 't_salesreturn_detail.SalesReturnInternalID')
                    ->where('t_salesreturn_header.CompanyInternalID', $company)
                    ->where('t_salesreturn_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_salesreturn_detail.InventoryInternalID', $internal)
                    ->where('t_salesreturn_detail.UomInternalID', $uom)
                    ->where('t_salesreturn_header.SalesReturnDate', $if, $date)
                    ->sum('Qty');
            $Min = MemoInDetail::
                    join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.MemoInInternalID')
                    ->where('t_memoin_header.CompanyInternalID', $company)
                    ->where('t_memoin_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_memoin_detail.InventoryInternalID', $internal)
                    ->where('t_memoin_detail.UomInternalID', $uom)
                    ->where('t_memoin_header.MemoInDate', $if, $date)
                    ->sum('Qty');
            $Mout = MemoOutDetail::
                    join('t_memoout_header', 't_memoout_header.InternalID', '=', 't_memoout_detail.MemoOutInternalID')
                    ->where('t_memoout_header.CompanyInternalID', $company)
                    ->where('t_memoout_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_memoout_detail.InventoryInternalID', $internal)
                    ->where('t_memoout_detail.UomInternalID', $uom)
                    ->where('t_memoout_header.MemoOutDate', $if, $date)
                    ->sum('Qty');
            $Tin = TransferDetail::
                    join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                    ->where('t_transfer_header.CompanyInternalID', $company)
                    ->where('t_transfer_header.WarehouseDestinyInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_transfer_detail.InventoryInternalID', $internal)
                    ->where('t_transfer_detail.UomInternalID', $uom)
                    ->where('t_transfer_header.TransferDate', $if, $date)
                    ->sum('Qty');
            $Tout = TransferDetail::
                    join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                    ->where('t_transfer_header.CompanyInternalID', $company)
                    ->where('t_transfer_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                    ->where('t_transfer_detail.InventoryInternalID', $internal)
                    ->where('t_transfer_detail.UomInternalID', $uom)
                    ->where('t_transfer_header.TransferDate', $if, $date)
                    ->sum('Qty');
            $tamp += ($pembelian + $Rpenjualan - $penjualan - $Rpembelian + $Min - $Mout + $Tin - $Tout) * $data->Value;
        }
        return $tamp + $initial;
    }

    public function salesDetail() {
        return $this->hasMany('SalesDetail', 'InventoryInternalID', 'InternalID');
    }

    public function purchaseDetail() {
        return $this->hasMany('PurchaseDetail', 'InventoryInternalID', 'InternalID');
    }

    public function salesReturnDetail() {
        return $this->hasMany('SalesReturnDetail', 'InventoryInternalID', 'InternalID');
    }

    public function purchaseReturnDetail() {
        return $this->hasMany('PurchaseReturnDetail', 'InventoryInternalID', 'InternalID');
    }

    public function inventoryUom() {
        return $this->hasMany('InventoryUom', 'InventoryInternalID', 'InternalID');
    }

    public function inventoryType() {
        return $this->belongsTo('InventoryType', 'InventoryTypeInternalID', 'InternalID');
    }

    public function group() {
        return $this->belongsTo('Group', 'GroupInternalID', 'InternalID');
    }

    public function brand() {
        return $this->belongsTo('Brand', 'BrandInternalID', 'InternalID');
    }

    public function variety() {
        return $this->belongsTo('Variety', 'VarietyInternalID', 'InternalID');
    }

    public function parcelInventory() {
        return $this->hasMany('ParcelInventory', 'InventoryInternalID', 'InternalID');
    }

}
