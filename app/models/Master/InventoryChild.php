<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class InventoryChild extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_inventory_child';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showInventoryChild() {
        return InventoryChild::all();
    }

    public function inventory() {
        return $this->belongsTo('Inventory', 'InventoryInternalID', 'InternalID');
    }
    public function inventoryChilds() {
        return $this->belongsTo('Inventory', 'ChildInventoryInternalID', 'InternalID');
    }
    public function inventoryUom() {
        return $this->belongsTo('InventoryUom', 'UomInternalID', 'InternalID');
    }

}
