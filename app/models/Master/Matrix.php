<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Matrix extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_matrix';
    public $timestamps = false;
    protected $primaryKey = 'InternalID';
    
    public static function showMatrix() {
        return Matrix::all();
    }
    
    public static function getUserMatrixType(){
        return Matrix::join('m_user_detail','m_user_detail.MatrixInternalID','=','m_matrix.InternalID')
                ->where('m_user_detail.UserInternalID', Auth::user()->InternalID)->select('m_matrix.Type')->where("Type","!=","report")->orderBy('m_matrix.MatrixNumber')->distinct()->get();
    }
    
    public static function getUserMatrix($type){
        return Matrix::join('m_user_detail','m_user_detail.MatrixInternalID','=','m_matrix.InternalID')
                ->where('m_user_detail.UserInternalID', Auth::user()->InternalID)
                ->where('m_matrix.TypeClass','!=','accdecend')
                ->where('m_matrix.TypeClass','!=','salesdecend')
                ->where('m_matrix.TypeClass','!=','purchasedecend')
                ->where('m_matrix.TypeClass','!=','invdecend')
                ->where('m_matrix.Type', $type)->orderBy('m_matrix.MatrixNumber','asc')->get();
    }
    
    public static function getUserMatrixByChild($typeClass){
        return Matrix::join('m_user_detail','m_user_detail.MatrixInternalID','=','m_matrix.InternalID')
                ->where('m_user_detail.UserInternalID', Auth::user()->InternalID)->where('TypeClass', $typeClass)->orderBy('m_matrix.MatrixNumber')->get();
    }

    public function userDetail() {
        return $this->hasMany('UserDetail', 'MatrixInternalID', 'InternalID');
    }
}
