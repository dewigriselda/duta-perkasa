<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Package extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_package';
    public $timestamps = false;
    protected $primaryKey = 'InternalID';
    
    public static function showPackage() {
        return Package::all();
    }
    
    public function packageDetail() {
        return $this->hasMany('PackageDetail', 'PackageInternalID', 'InternalID');
    }
    
    public function company() {
        return $this->hasMany('Company', 'PackageInternalID', 'InternalID');
    }
}
