<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class PackageDetail extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_package_detail';
    public $timestamps = false;
    protected $primaryKey = 'InternalID';

    public static function showPackageDetail() {
        return PackageDetail::all();
    }
    
    public function package() {
        return $this->belongsTo('Package', 'PackageInternalID', 'InternalID');
    }
    
    public function modul() {
        return $this->belongsTo('Modul', 'ModulInternalID', 'InternalID');
    }
}
