<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Parcel extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_parcel';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showParcel() {
        return Parcel::all();
    }

    public static function getIdParcel($ParcelID) {
        $internalID = Parcel::where('ParcelID', '=', $ParcelID)->where('CompanyInternalID', Auth::user()->Company->InternalID)->pluck('InternalID');
        return $internalID;
    }

    public static function getNextIDParcel($text) {
        $query = 'SELECT ParcelID From m_parcel Where ParcelID LIKE "' . $text . '%" AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" order by ParcelID desc';
        $ParcelID = DB::select(DB::raw($query));

        if (count($ParcelID) <= 0) {
            $ParcelID = '';
        } else {
            $ParcelID = $ParcelID[0]->MemoInID;
        }

        if ($ParcelID == '') {
            $ParcelID = $text . '0001';
        } else {
            $textTamp = $ParcelID;
            $ParcelID = substr($textTamp, (strlen($textTamp) - 4), 4) + 1;
            $ParcelID = str_pad($ParcelID, 4, '0', STR_PAD_LEFT);
            $ParcelID = $text . $ParcelID;
        }
        return $ParcelID;
    }

    public function currency() {
        return $this->belongsTo('Currency', 'CurrencyInternalID', 'InternalID');
    }

    public function parcelInventory() {
        return $this->hasMany('ParcelInventory', 'ParcelInternalID', 'InternalID');
    }

    public function salesOrderParcel() {
        return $this->hasMany('SalesOrderParcel', 'ParcelInternalID', 'InternalID');
    }

    public function shippingParcel() {
        return $this->hasMany('ShippingParcel', 'ParcelInternalID', 'InternalID');
    }

    public function salesAddParcel() {
        return $this->hasMany('SalesAddParcel', 'ParcelInternalID', 'InternalID');
    }

    public function salesReturnParcel() {
        return $this->hasMany('SalesReturnParcel', 'ParcelInternalID', 'InternalID');
    }
    
    public function salesCopyParcel() {
        return $this->hasMany('SalesCopyParcel', 'ParcelInternalID', 'InternalID');
    }
}
