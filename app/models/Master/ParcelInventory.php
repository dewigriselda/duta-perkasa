<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class ParcelInventory extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_parcel_inventory';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showParcelInventory() {
        return ParcelInventory::all();
    }

    public function parcel() {
        return $this->belongsTo('Parcel', 'ParcelInternalID', 'InternalID');
    }

    public function inventory() {
        return $this->belongsTo('Inventory', 'InventoryInternalID', 'InternalID');
    }

    public function uom() {
        return $this->belongsTo('Uom', 'UomInternalID', 'InternalID');
    }

}
