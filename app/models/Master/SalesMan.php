<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class SalesMan extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_sales_man';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public function company() {
        return $this->belongsTo('Company', 'CompanyInternalID', 'InternalID');
    }

    public function salesOrderHeader() {
        return $this->hasMany('SalesOrderHeader', 'SalesManInternalID', 'InternalID');
    }

    public function salesAddHeader() {
        return $this->hasMany('SalesAddHeader', 'SalesManInternalID', 'InternalID');
    }

}
