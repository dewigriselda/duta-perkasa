<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Uom extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_uom';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showUom() {
        return Uom::all();
    }

    public function inventoryUom() {
        return $this->hasMany("InventoryUom", "UomInternalID", "InternalID");
    }

    public function salesOrderDetail() {
        return $this->hasMany("SalesOrderDetail", "UomInternalID", "InternalID");
    }

    public function salesDetail() {
        return $this->hasMany("SalesDetail", "UomInternalID", "InternalID");
    }

    public function salesReturnDetail() {
        return $this->hasMany("SalesReturnDetail", "UomInternalID", "InternalID");
    }

    public function purchaseOrderDetail() {
        return $this->hasMany("PurchaseOrderDetail", "UomInternalID", "InternalID");
    }

    public function purchaseDetail() {
        return $this->hasMany("PurchaseDetail", "UomInternalID", "InternalID");
    }

    public function purchaseAddDetail() {
        return $this->hasMany("PurchaseAddDetail", "UomInternalID", "InternalID");
    }

    public function purchaseReturnDetail() {
        return $this->hasMany("PurchaseReturnDetail", "UomInternalID", "InternalID");
    }

    public function memoInDetail() {
        return $this->hasMany("MemoInDetail", "UomInternalID", "InternalID");
    }

    public function memoOutDetail() {
        return $this->hasMany("MemoOutDetail", "UomInternalID", "InternalID");
    }

    public function transferDetail() {
        return $this->hasMany("TransferDetail", "UomInternalID", "InternalID");
    }

    public function parcelInventory() {
        return $this->hasMany("ParcelInventory", "UomInternalID", "InternalID");
    }

}
