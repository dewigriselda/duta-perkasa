<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_user';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';


    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';
    
    protected $hidden = array('Password', 'Remember_token');
    
    public function getAuthPassword() {
        return $this->UserPwd;
    }
    
    public static function checkAdmin(){
        $matrix = Auth::user()->Company->Package->TotalMatrix;
        $userMatrix = UserDetail::where('UserInternalID',Auth::user()->InternalID)->count();
        if($userMatrix == $matrix){
            return true;
        }
        return false;
    }

    public function userDetail() {
        return $this->hasMany('UserDetail', 'UserInternalID', 'InternalID');
    }
    
    public function company() {
        return $this->belongsTo('Company', 'CompanyInternalID', 'InternalID');
    }
}
