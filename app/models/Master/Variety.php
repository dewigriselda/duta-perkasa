<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Variety extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_variety';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showVariety() {
        return Variety::all();
    }

    public function group() {
        return $this->belongsTo("Group", "GroupInternalID", "InternalID");
    }

    public function inventory() {
        return $this->hasMany("Inventory", "VarietyInternalID", "InternalID");
    }

}
