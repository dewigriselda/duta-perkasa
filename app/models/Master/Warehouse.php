<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Warehouse extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_warehouse';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showWarehouse() {
        return Warehouse::all();
    }

    public function memoIn() {
        return $this->hasMany('MemoIn', 'WarehouseInternalID', 'InternalID');
    }

    public function memoOut() {
        return $this->hasMany('MemoOut', 'WarehouseInternalID', 'InternalID');
    }

    public function salesHeader() {
        return $this->hasMany('SalesHeader', 'WarehouseInternalID', 'InternalID');
    }

    public function PurchaseHeader() {
        return $this->hasMany('SalesHeader', 'WarehouseInternalID', 'InternalID');
    }

    public function PurchaseReturnHeader() {
        return $this->hasMany('SalesHeader', 'WarehouseInternalID', 'InternalID');
    }

    public function salesReturnHeader() {
        return $this->hasMany('SalesHeader', 'WarehouseInternalID', 'InternalID');
    }

    public function transferHeaderSource() {
        return $this->hasMany('TransferHeader', 'WarehouseInternalID', 'InternalID');
    }

    public function transferHeaderDestiny() {
        return $this->hasMany('TransferHeader', 'WarehouseDestinyInternalID', 'InternalID');
    }

    public function convertion() {
        return $this->hasMany('Convertion', 'WarehouseInternalID', 'InternalID');
    }

}
