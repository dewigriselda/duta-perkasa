<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class MemoInDetail extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_memoin_detail';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showMemoInDetail() {
        return MemoInDetail::all();
    }

    public static function getTipeInventoryData($headerInternalID) {
        return $result = DB::select(DB::raw('SELECT SUM(tpd.SubTotal) as total, mit.InternalID FROM t_memoin_detail tpd '
                                . 'INNER JOIN m_inventory mi on mi.InternalID = tpd.InventoryInternalID '
                                . 'INNER JOIN m_inventorytype mit on mit.InternalID = mi.InventoryTypeInternalID '
                                . 'WHERE tpd.MemoInInternalID = ' . $headerInternalID . ' '
                                . 'GROUP BY mit.InternalID'));
    }

    public function memoInHeader() {
        return $this->belongsTo('MemoInHeader', 'MemoInInternalID', 'InternalID');
    }

    public function inventory() {
        return $this->belongsTo('Inventory', 'InventoryInternalID', 'InternalID');
    }

    public function uom() {
        return $this->belongsTo('Uom', 'UomInternalID', 'InternalID');
    }

}
