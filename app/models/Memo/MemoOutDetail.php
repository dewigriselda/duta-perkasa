<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class MemoOutDetail extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_memoout_detail';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';


    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showMemoOutDetail() {
        return MemoOutDetail::all();
    }
    
    public function memoOutHeader() {
        return $this->belongsTo('MemoOutHeader', 'MemoOutInternalID', 'InternalID');
    }

    public function inventory() {
        return $this->belongsTo('Inventory', 'InventoryInternalID', 'InternalID');
    }
    
    public function uom() {
        return $this->belongsTo('Uom', 'UomInternalID', 'InternalID');
    }
}
