<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class PurchaseReturnDetail extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_purchasereturn_detail';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function getTipeInventoryData($headerInternalID) {
        return $result = DB::select(DB::raw('SELECT SUM(tprd.SubTotal) as total, mit.InternalID FROM t_purchasereturn_detail tprd '
                                . 'INNER JOIN m_inventory mi on mi.InternalID = tprd.InventoryInternalID '
                                . 'INNER JOIN m_inventorytype mit on mit.InternalID = mi.InventoryTypeInternalID '
                                . 'WHERE tprd.PurchaseReturnInternalID = ' . $headerInternalID . ' '
                                . 'GROUP BY mit.InternalID'));
    }

    public static function showPurchaseReturnDetail() {
        return PurchaseReturnDetail::all();
    }

    public function purchaseReturnHeader() {
        return $this->belongsTo('PurchaseReturnHeader', 'PurchaseReturnInternalID', 'InternalID');
    }

    public function inventory() {
        return $this->belongsTo('Inventory', 'InventoryInternalID', 'InternalID');
    }

    public function uom() {
        return $this->belongsTo('Uom', 'UomInternalID', 'InternalID');
    }

}
