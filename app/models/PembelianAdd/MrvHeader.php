<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class MrvHeader extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_mrv_header';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showMrvHeader() {
        return MrvHeader::all();
    }

    public static function advancedSearch($typePayment, $typeTax, $start, $end) {
        $where = '';
        if ($typePayment != '-1' && $typePayment != '') {
            $where .= 'isCash = "' . $typePayment . '" ';
        }
        if ($typeTax != '-1' && $typeTax != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'VAT = "' . $typeTax . '" ';
        }
        if ($start != '' && $end != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $start = explode('-', $start);
            $end = explode('-', $end);
            $startSearch = $start[2] . '-' . $start[1] . '-' . $start[0];
            $endSearch = $end[2] . '-' . $end[1] . '-' . $end[0];
            $where .= 'MrvDate between "' . $startSearch . ' 00:00:00" AND "' . $endSearch . ' 23:59:59"';
        }

        $query = 'SELECT sh.*, c.CurrencyName, cc.ACC6Name, cc.ACC6ID '
                . 'FROM t_mrv_header sh '
                . 'INNER JOIN m_currency c on c.InternalID = sh.CurrencyInternalID '
                . 'INNER JOIN m_coa6 cc on cc.InternalID = sh.ACC6InternalID';
        if ($where != '') {
            $query .= ' WHERE ' . $where . ' AND ' . 'sh.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        } else {
            $query .= ' WHERE sh.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        }
        return $result = DB::select(DB::raw($query));
    }

    public static function getIdmrv($mrvID) {
        $internalID = MrvHeader::where('MrvID', '=', $mrvID)->where('CompanyInternalID', Auth::user()->Company->InternalID)->pluck('InternalID');
        return $internalID;
    }

    public static function getNextIDMrv($text) {
        $query = 'SELECT MrvID From t_mrv_header Where MrvID LIKE "' . $text . '%" AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" order by MrvID desc';
        $mrvID = DB::select(DB::raw($query));

        if (count($mrvID) <= 0) {
            $mrvID = '';
        } else {
            $mrvID = $mrvID[0]->MrvID;
        }

        if ($mrvID == '') {
            $mrvID = $text . '0001';
        } else {
            $textTamp = $mrvID;
            $mrvID = substr($textTamp, (strlen($textTamp) - 4), 4) + 1;
            $mrvID = str_pad($mrvID, 4, '0', STR_PAD_LEFT);
            $mrvID = $text . $mrvID;
        }
        return $mrvID;
    }

    public static function qtyInventoryWarehouse($inventory, $warehouse, $bulan, $tahun) {
        $result = MrvDetail::join('t_mrv_header', 't_mrv_header.InternalID', '=', 't_mrv_detail.MrvInternalID')
                ->whereRaw('YEAR(MrvDate) = "' . $tahun . '" AND MONTH(MrvDate) = "' . $bulan . '"')
                ->where('InventoryInternalID', $inventory)
                ->where('WarehouseInternalID', $warehouse)
                ->where('t_mrv_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                ->groupBy('t_mrv_detail.UomInternalID')
                ->select(DB::raw('sum(Qty) as sumqty, t_mrv_detail.UomInternalID'))
                ->get();
        $tampQty = 0;
        foreach ($result as $data) {
            $invenUom = InventoryUom::where('UomInternalID', $data->UomInternalID)->where('InventoryInternalID', $inventory)->first();
            $tampQty += $data->sumqty * $invenUom->Value;
        }
        return $tampQty;
    }

    public static function getTopTen() {
        $query = 'SELECT table2.* FROM ('
                . 'SELECT MrvDate, GrandTotal*CurrencyRate as hasil From t_mrv_header '
                . 'WHERE CompanyInternalID = "' . Auth::user()->Company->InternalID . '" '
                . 'Order by MrvDate desc Limit 0,10'
                . ') as table2 '
                . 'Order by table2.MrvDate asc';
        $top = DB::select(DB::raw($query));
        return $top;
    }

    public static function getMrv10($id) {
        $query = 'SELECT SUM(table2.hasil) as hasil'
                . ' FROM (SELECT s.ACC6Name,th.GrandTotal*th.CurrencyRate as hasil '
                . ' From t_mrv_header th INNER JOIN m_coa6 as s on s.InternalID = th.ACC6InternalID '
                . ' Where s.InternalID = "' . $id . '" '
                . ' AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"'
                . ' Order by th.MrvDate desc Limit 0,10) as table2';
        $mrv = DB::select(DB::raw($query));
        return $mrv;
    }

    public static function getMrvPayable() {
        $query = 'SELECT th.*, th.MrvID as ID, th.MrvDate as Date, coa.ACC6Name as coa6, cur.CurrencyName '
                . 'FROM t_mrv_header th INNER JOIN m_coa6 as coa on th.ACC6InternalID = coa.InternalID '
                . 'INNER JOIN m_currency as cur on cur.InternalID = th.CurrencyInternalID '
                . 'where th.isCash = 1 '
                . 'AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '" '
                . 'AND th.`GrandTotal`*th.`CurrencyRate` > (Select IFNULL(SUM(td.JournalDebetMU),0) From t_journal_detail td Where td.JournalTransactionID = th.`MrvID`)';
        $mrv = DB::select(DB::raw($query));
        $tamp = array();
        foreach ($mrv as $value) {
            $data = date("Y-m-d", strtotime("+" . $value->LongTerm . " day", strtotime($value->Date)));
            array_push($tamp, $data);
        }
        array_multisort($tamp, $mrv);
        return $mrv;
    }

    public static function getMrvPayableAging() {
        $query = 'SELECT DATEDIFF(DATE_ADD(th.MrvDate,INTERVAL th.LongTerm DAY),Now()) as selisihHari, th.GrandTotal*th.CurrencyRate as GrandTotal '
                . 'FROM t_mrv_header th INNER JOIN m_coa6 as coa on th.ACC6InternalID = coa.InternalID '
                . 'where th.isCash = 1 '
                . 'AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '" '
                . 'AND th.`GrandTotal`*th.`CurrencyRate` > (Select IFNULL(SUM(td.JournalDebetMU),0) From t_journal_detail td Where td.JournalTransactionID = th.`MrvID`)';
        $mrv = DB::select(DB::raw($query));
        return $mrv;
    }

    public static function getSlipInternalID($mrvID) {
        $journal = JournalHeader::where('TransactionID', $mrvID)->first();
        $detail = Slip::find($journal->SlipInternalID);
        $slip = Slip::where('ACC1InternalID', $detail->ACC1InternalID)
                        ->where('ACC2InternalID', $detail->ACC2InternalID)
                        ->where('ACC3InternalID', $detail->ACC3InternalID)
                        ->where('ACC4InternalID', $detail->ACC4InternalID)
                        ->where('ACC5InternalID', $detail->ACC5InternalID)
                        ->where('ACC6InternalID', $detail->ACC6InternalID)->first();
        return $slip->InternalID;
    }

    public static function getSum($inventoryID, $mrvID, $InternalID) {
        $query = 'SELECT SUM(td.Qty) as total '
                . 'From t_mrv_detail td INNER JOIN t_mrv_header th on td.MrvInternalID = th.InternalID '
                . 'Where td.InventoryInternalID = "' . $inventoryID . '" AND PurchaseOrderInternalID = "' . $mrvID . '"'
                . 'AND td.PurchaseOrderDetailInternalID = "' . $InternalID . '" '
                . 'AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" ';
        $mrvReturnID = DB::select(DB::raw($query));
        return $mrvReturnID[0]->total;
    }

    public static function getSumExcept($inventoryID, $purchaseOrderID, $MrvID, $InternalID) {
        $query = 'SELECT SUM(td.Qty) as total '
                . 'From t_mrv_detail td INNER JOIN t_mrv_header th on td.MrvInternalID = th.InternalID '
                . 'Where td.InventoryInternalID = "' . $inventoryID . '" AND PurchaseOrderInternalID = "' . $purchaseOrderID . '"'
                . 'AND th.InternalID != "' . $MrvID . '" '
                . 'AND td.is_deleted = 0 '
                . 'AND td.PurchaseOrderDetailInternalID = "' . $InternalID . '" '
                . 'AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" ';
        $mrvReturnID = DB::select(DB::raw($query));
        return $mrvReturnID[0]->total;
    }

    public static function isPurchase($mrvID) {
        $query = 'SELECT COUNT(*) as total '
                . 'From t_purchase_header th '
                . 'Where MrvInternalID = ' . $mrvID . ' '
                . 'AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" ';
        $mrvReturnID = DB::select(DB::raw($query));
        if ($mrvReturnID[0]->total > 0) {
            return true;
        }
        return false;
    }

    public static function getYearMin() {
        $result = MrvHeader::orderBy('MrvDate')->select(DB::raw('YEAR(MrvDate) as MrvDates'))->where("CompanyInternalID", Auth::User()->CompanyInternalID)->first();
        if ($result == '') {
            return '0';
        }
        return $result->MrvDates;
    }

//    public static function qtyInventory($inventory, $bulan, $tahun) {
//        $result = MrvDetail::join('t_mrv_header', 't_mrv_header.InternalID', '=', 't_mrv_detail.MrvInternalID')
//                ->whereRaw('YEAR(MrvDate) = "' . $tahun . '" AND MONTH(MrvDate) = "' . $bulan . '"')
//                ->where('InventoryInternalID', $inventory)
//                ->where('t_mrv_header.CompanyInternalID', Auth::user()->CompanyInternalID)
//                ->sum('Qty');
//        return $result;
//    }

    public static function qtyInventory($inventory, $bulan, $tahun) {
        $result = MrvDetail::join('t_mrv_header', 't_mrv_header.InternalID', '=', 't_mrv_detail.MrvInternalID')
                ->whereRaw('YEAR(MrvDate) = "' . $tahun . '" AND MONTH(MrvDate) = "' . $bulan . '"')
                ->where('InventoryInternalID', $inventory)
                ->where('t_mrv_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                ->groupBy('t_mrv_detail.UomInternalID')
                ->select(DB::raw('sum(Qty) as sumqty, t_mrv_detail.UomInternalID'))
                ->get();
        $tampQty = 0;
        foreach ($result as $data) {
            $invenUom = InventoryUom::where('UomInternalID', $data->UomInternalID)->where('InventoryInternalID', $inventory)->first();
            $tampQty += $data->sumqty * $invenUom->Value;
        }
        return $tampQty;
    }

    public static function valueInventory($inventory, $bulan, $tahun) {
        $result = MrvDetail::join('t_mrv_header', 't_mrv_header.InternalID', '=', 't_mrv_detail.MrvInternalID')
                ->whereRaw('YEAR(MrvDate) = "' . $tahun . '" AND MONTH(MrvDate) = "' . $bulan . '"')
                ->where('InventoryInternalID', $inventory)
                ->where('t_mrv_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                ->select(DB::raw('sum(SubTotal*CurrencyRate) as SubTotalCurrency'))
                ->pluck('SubTotalCurrency');
        return $result;
    }

    public static function qtyInventorySuperAdmin($inventory, $company) {
        $result = MrvDetail::join('t_mrv_header', 't_mrv_header.InternalID', '=', 't_mrv_detail.MrvInternalID')
                ->where('InventoryInternalID', $inventory)
                ->where('t_mrv_header.CompanyInternalID', $company)
                ->sum('Qty');
        return $result;
    }

    public static function valueInventorySuperAdmin($inventory, $company) {
        $result = MrvDetail::join('t_mrv_header', 't_mrv_header.InternalID', '=', 't_mrv_detail.MrvInternalID')
                ->where('InventoryInternalID', $inventory)
                ->where('t_mrv_header.CompanyInternalID', $company)
                ->select(DB::raw('sum(SubTotal*CurrencyRate) as SubTotalCurrency'))
                ->pluck('SubTotalCurrency');
        return $result;
    }

    public static function getSumDiscountGlobalMrv($mrvID) {
        $query = 'SELECT SUM(th.DiscountGlobal) as total '
                . 'From t_mrv_header th '
                . 'Where th.PurchaseOrderInternalID = "' . $mrvID . '" '
                . 'AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" ';
        $mrvID = DB::select(DB::raw($query));
        return $mrvID[0]->total;
    }

    public static function getSumDiscountGlobalExcept($mrvID, $purchaseOrder) {
        $query = 'SELECT SUM(th.DiscountGlobal) as total '
                . 'From t_mrv_header th '
                . 'Where th.PurchaseOrderInternalID = "' . $purchaseOrder . '" '
                . 'AND th.InternalID != "' . $mrvID . '" '
                . 'AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" ';
        $mrvID = DB::select(DB::raw($query));
        return $mrvID[0]->total;
    }

    public function mrvDetail() {
        return $this->hasMany('MrvDetail', 'MrvInternalID', 'InternalID');
    }

    public function coa6() {
        return $this->belongsTo('Coa6', 'ACC6InternalID', 'InternalID');
    }

    public function currency() {
        return $this->belongsTo('Currency', 'CurrencyInternalID', 'InternalID');
    }

    public function warehouse() {
        return $this->belongsTo('Warehouse', 'WarehouseInternalID', 'InternalID');
    }

    public function purchaseOrder() {
        return $this->belongsTo('PurchaseOrderHeader', 'PurchaseOrderInternalID', 'InternalID');
    }

    public function PurchaseOrderHeader() {
        return $this->belongsTo('PurchaseOrderHeader', 'PurchaseOrderInternalID', 'InternalID');
    }

}
