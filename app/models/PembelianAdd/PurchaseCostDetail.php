<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class PurchaseCostDetail extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_purchase_cost_detail';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function getTipeInventoryData($headerInternalID) {
        return $result = DB::select(DB::raw('SELECT SUM(tpd.TotalCost) as total, mi.InternalID FROM t_purchase_cost_detail tpd '
                                . 'INNER JOIN m_cost mi on mi.InternalID = tpd.CostInternalID '
                                . 'WHERE tpd.PurchaseInternalID = ' . $headerInternalID . ' '
                                . 'GROUP BY mi.InternalID'));
    }
    
    public static function showPurchaseCostDetail() {
        return PurchaseCostDetail::all();
    }

    public function purchaseHeader() {
        return $this->belongsTo('PurchaseHeader', 'PurchaseInternalID', 'InternalID');
    }

    public function cost() {
        return $this->belongsTo('Cost', 'CostInternalID', 'InternalID');
    }
}
