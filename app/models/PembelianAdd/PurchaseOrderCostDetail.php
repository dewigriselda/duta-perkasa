<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class PurchaseOrderCostDetail extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_purchaseorder_cost_detail';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showPurchaseOrderCostDetail() {
        return PurchaseOrderCostDetail::all();
    }

    public function purchaseOrderHeader() {
        return $this->belongsTo('PurchaseOrderHeader', 'PurchaseOrderInternalID', 'InternalID');
    }

    public function cost() {
        return $this->belongsTo('Cost', 'CostInternalID', 'InternalID');
    }
}
