<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class PurchaseOrderDetail extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_purchaseorder_detail';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';


    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showPurchaseOrderDetail() {
        return PurchaseOrderDetail::all();
    }
    
    public function purchaseOrderHeader() {
        return $this->belongsTo('PurchaseOrderHeader', 'PurchaseOrderInternalID', 'InternalID');
    }

    public function inventory() {
        return $this->belongsTo('Inventory', 'InventoryInternalID', 'InternalID');
    }
    
    public function Uom() {
        return $this->belongsTo('Uom', 'UomInternalID', 'InternalID');
    }
    
}
