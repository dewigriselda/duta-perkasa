<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class SalesReturnHeader extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_salesreturn_header';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showSalesReturnHeader() {
        return SalesReturnHeader::all();
    }

    public static function salesReceivableReport($id, $status) {
        $stts = "";
        $query = SalesReturnHeader::where('SalesReturnID', $id)->first();

        $pelunasan = JournalDetail::where('JournalTransactionID', $query->SalesReturnID)->sum('JournalCreditMU');
        if ($query->isCash == '3' || $query->isCash == '2' || $query->isCash == '0') {
            $stts = 'Complete';
        } else {
            if ($query->GrandTotal - $pelunasan <= 0) {
                $stts = 'Complete';
            } else if ($query->DownPayment != 0) {
                $stts = "Paid";
            } else {
                $stts = 'No Paid';
            }
        }
        return $stts;
    }
    
    public static function advancedSearch($typePayment, $typeTax, $start, $end) {
        $where = '';
        if ($typePayment != '-1' && $typePayment != '') {
            $where .= 'isCash = "' . $typePayment . '" ';
        }
        if ($typeTax != '-1' && $typeTax != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'VAT = "' . $typeTax . '" ';
        }
        if ($start != '' && $end != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $start = explode('-', $start);
            $end = explode('-', $end);
            $startSearch = $start[2] . '-' . $start[1] . '-' . $start[0];
            $endSearch = $end[2] . '-' . $end[1] . '-' . $end[0];
            $where .= 'SalesReturnDate between "' . $startSearch . ' 00:00:00" AND "' . $endSearch . ' 23:59:59"';
        }

        $query = 'SELECT sh.*, c.CurrencyName, cc.ACC6Name, cc.ACC6ID '
                . 'FROM t_salesreturn_header sh '
                . 'INNER JOIN m_currency c on c.InternalID = sh.CurrencyInternalID '
                . 'INNER JOIN m_coa6 cc on cc.InternalID = sh.ACC6InternalID';
        if ($where != '') {
            $query .= ' WHERE ' . $where . ' AND ' . 'sh.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        } else {
            $query .= ' WHERE sh.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        }
        return $result = DB::select(DB::raw($query));
    }

    public static function getIdsalesReturn($salesReturnID) {
        $internalID = SalesReturnHeader::where('SalesReturnID', '=', $salesReturnID)->where('CompanyInternalID', Auth::user()->Company->InternalID)->pluck('InternalID');
        return $internalID;
    }

    public static function getNextIDSalesReturn($text) {
        $query = 'SELECT SalesReturnID '
                . 'From t_salesreturn_header '
                . 'Where SalesReturnID LIKE "%-' . $text . '" '
                . 'AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" '
                . 'order by SalesReturnID desc';
        $salesReturnID = DB::select(DB::raw($query));

        if (count($salesReturnID) <= 0) {
            $salesReturnID = '';
        } else {
            $salesReturnID = $salesReturnID[0]->SalesReturnID;
        }

        if ($salesReturnID == '') {
            $salesReturnID = 'R.0001-' . $text;
        } else {
            $textTamp = $salesReturnID;
            $salesReturnID = substr($textTamp, 2, 4) + 1;
            $salesReturnID = str_pad($salesReturnID, 4, '0', STR_PAD_LEFT);
            $salesReturnID = 'R.' . $salesReturnID . '-' . $text;
        }
        return $salesReturnID;
    }

    public static function getSumReturn($inventoryID, $salesID, $InternalID) {
        $query = 'SELECT SUM(td.Qty) as total '
                . 'From t_salesreturn_detail td INNER JOIN t_salesreturn_header th on td.SalesReturnInternalID = th.InternalID '
                . 'Where td.InventoryInternalID = "' . $inventoryID . '" AND SalesReturnID LIKE "%-' . $salesID . '" '
                . 'AND td.SalesDetailInternalID = "' . $InternalID . '" '
                . 'AND td.SalesReturnParcelInternalID = 0 '
                . 'AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" ';
        $salesReturnID = DB::select(DB::raw($query));
        return $salesReturnID[0]->total;
    }

    public static function getSumReturnParcel($inventoryID, $salesID, $InternalID) {
        $query = 'SELECT SUM(td.Qty) as total '
                . 'From t_salesreturn_parcel td INNER JOIN t_salesreturn_header th on td.SalesReturnInternalID = th.InternalID '
                . 'Where td.ParcelInternalID = "' . $inventoryID . '" AND th.SalesReturnID LIKE "%-' . $salesID . '" '
                . 'AND td.SalesParcelDetailInternalID = "' . $InternalID . '" '
                . 'AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '" ';
        $salesReturnID = DB::select(DB::raw($query));
        return $salesReturnID[0]->total;
    }

    public static function getSumReturnExcept($inventoryID, $salesReturnID, $InternalID) {
        $salesID = substr($salesReturnID, 7);
        $query = 'SELECT SUM(td.Qty) as total '
                . 'From t_salesreturn_detail td INNER JOIN t_salesreturn_header th on td.SalesReturnInternalID = th.InternalID '
                . 'Where td.InventoryInternalID = "' . $inventoryID . '" AND SalesReturnID LIKE "%-' . $salesID . '" '
                . 'AND th.SalesReturnID != "' . $salesReturnID . '" '
                . 'AND td.SalesDetailInternalID = "' . $InternalID . '" '
                . 'AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" ';
        $salesReturnID = DB::select(DB::raw($query));
        return $salesReturnID[0]->total;
    }

    public static function getSumReturnDescription2($descriptionID, $salesOrderID) {
        $query = 'SELECT SUM(srd.Qty) as total '
                . 'From t_salesreturn_detail td INNER JOIN t_salesreturn_header th on td.SalesReturnInternalID = th.InternalID '
                . 'INNER JOIN t_sales_detail td2 on td.SalesDetailInternalID = td2.InternalID '
                . 'INNER JOIN t_sales_header th2 on td2.SalesInternalID = th2.InternalID '
                . 'INNER JOIN t_sales_detail_description sd on sd.SalesInternalID = th2.InternalID '
                . 'INNER JOIN t_salesreturn_detail_description srd on srd.SalesReturnInternalID = th.InternalID '
                . 'Where sd.SalesOrderDescriptionInternalID = "' . $descriptionID . '" '
                . ' AND th2.SalesOrderInternalID = "' . $salesOrderID . '" '
                . 'AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        $salesID = DB::select(DB::raw($query));
        return $salesID[0]->total;
    }

    public static function getSumReturnDescription($descriptionID) {
        $query = 'SELECT SUM(td.Qty) as total '
                . 'From t_salesreturn_detail_description td '
                . 'INNER JOIN t_salesreturn_header th on td.SalesReturnInternalID = th.InternalID '
                . 'INNER JOIN t_salesreturn_detail de on de.SalesReturnInternalID = th.InternalID '
                . 'INNER JOIN t_sales_detail td2 on de.SalesDetailInternalID = td2.InternalID '
                . 'INNER JOIN t_sales_header th2 on td2.SalesInternalID = th2.InternalID '
                . 'INNER JOIN t_sales_detail_description de2 on de2.SalesInternalID = th2.InternalID '
                . 'Where de2.SalesOrderDescriptionInternalID = "' . $descriptionID . '" '
                . 'AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        $salesID = DB::select(DB::raw($query));
        return $salesID[0]->total;
    }

    public static function getSumReturnDescriptionExcept($descriptionID, $except) {
        $query = 'SELECT SUM(td.Qty) as total '
                . 'From t_salesreturn_detail_description td INNER JOIN t_salesreturn_header th on td.SalesReturnInternalID = th.InternalID '
                . 'Where td.SalesDescriptionInternalID = "' . $descriptionID . '" '
                . 'AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '"'
                . ' AND th.InternalID != "' . $except . '"';
        $salesID = DB::select(DB::raw($query));
        return $salesID[0]->total;
    }

    public static function getSumReturnDescriptionSales($descriptionID, $salesOrderID) {
        $query = 'SELECT SUM(srd.Qty) as total '
                . 'From t_salesreturn_detail td INNER JOIN t_salesreturn_header th on td.SalesReturnInternalID = th.InternalID '
                . 'INNER JOIN t_sales_detail td2 on td.SalesDetailInternalID = td2.InternalID '
                . 'INNER JOIN t_sales_header th2 on td2.SalesInternalID = th2.InternalID '
                . 'INNER JOIN t_sales_detail_description sd on sd.SalesInternalID = th2.InternalID '
                . 'INNER JOIN t_salesreturn_detail_description srd on srd.SalesReturnInternalID = th.InternalID '
                . 'Where srd.SalesDescriptionInternalID = "' . $descriptionID . '" '
                . ' AND th2.InternalID = "' . $salesOrderID . '" '
                . 'AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        $salesID = DB::select(DB::raw($query));
        return $salesID[0]->total;
    }

    public static function getSumReturnOrder($inventoryID, $salesOrderID, $InternalID) {
        $query = 'SELECT SUM(td.Qty) as total '
                . 'From t_salesreturn_detail td INNER JOIN t_salesreturn_header th on td.SalesReturnInternalID = th.InternalID '
                . 'INNER JOIN t_sales_detail td2 on td.SalesDetailInternalID = td2.InternalID '
                . 'INNER JOIN t_sales_header th2 on td2.SalesInternalID = th2.InternalID '
                . 'Where td.InventoryInternalID = "' . $inventoryID . '" AND th2.SalesOrderInternalID = "' . $salesOrderID . '" '
                . 'AND td2.SalesOrderDetailInternalID = "' . $InternalID . '" '
                . 'AND td2.is_deleted = 0 '
                . 'AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '" ';
        $salesReturnID = DB::select(DB::raw($query));
        return $salesReturnID[0]->total;
    }

    public static function getSumReturnOrderParcel($inventoryID, $salesOrderID, $InternalID) {
        $query = 'SELECT SUM(td.Qty) as total '
                . 'From t_salesreturn_parcel td INNER JOIN t_salesreturn_header th on td.SalesReturnInternalID = th.InternalID '
                . 'INNER JOIN t_sales_parcel td2 on td.SalesParcelDetailInternalID = td2.InternalID '
                . 'INNER JOIN t_sales_header th2 on td2.SalesInternalID = th2.InternalID '
                . 'Where td.ParcelInternalID = "' . $inventoryID . '" AND th2.SalesOrderInternalID = "' . $salesOrderID . '" '
                . 'AND td2.SalesOrderParcelDetailInternalID = "' . $InternalID . '" '
                . 'AND td2.is_deleted = 0 '
                . 'AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '" ';
        $salesReturnID = DB::select(DB::raw($query));
        return $salesReturnID[0]->total;
    }

    public static function getSumReturnExceptParcel($inventoryID, $salesReturnID, $InternalID) {
        $salesID = substr($salesReturnID, 7);
        $query = 'SELECT SUM(td.Qty) as total '
                . 'From t_salesreturn_parcel td INNER JOIN t_salesreturn_header th on td.SalesReturnInternalID = th.InternalID '
                . 'Where td.ParcelInternalID = "' . $inventoryID . '" AND th.SalesReturnID LIKE "%-' . $salesID . '" '
                . 'AND th.SalesReturnID != "' . $salesReturnID . '" '
                . 'AND td.SalesParcelDetailInternalID = "' . $InternalID . '" '
                . 'AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '" ';
        $salesReturnID = DB::select(DB::raw($query));
        return $salesReturnID[0]->total;
    }

    public static function getTopTen() {
        $query = 'SELECT table2.* FROM ('
                . 'SELECT SalesReturnDate, GrandTotal*CurrencyRate as hasil From t_salesreturn_header '
                . 'WHERE CompanyInternalID = "' . Auth::user()->Company->InternalID . '" '
                . 'Order by SalesReturnDate desc Limit 0,10'
                . ') as table2 '
                . 'Order by table2.SalesReturnDate asc';
        $top = DB::select(DB::raw($query));
        return $top;
    }

    public static function getSalesReturn10($id) {
        $query = 'SELECT SUM(table2.hasil) as hasil'
                . ' FROM '
                . '(SELECT c.ACC6Name,th.GrandTotal*th.CurrencyRate as hasil '
                . ' From t_salesreturn_header th INNER JOIN m_coa6 as c on c.InternalID = th.ACC6InternalID '
                . ' Where c.InternalID = "' . $id . '" '
                . ' AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"'
                . ' Order by th.SalesReturnDate desc Limit 0,10) as table2';
        $salesreturn = DB::select(DB::raw($query));
        return $salesreturn;
    }

    public static function getSalesReturnReceivable() {
        $query = 'SELECT th.*, th.SalesReturnID as ID, th.SalesReturnDate as Date, coa.ACC6Name as coa6, cur.CurrencyName '
                . 'FROM t_salesreturn_header th INNER JOIN m_coa6 as coa on th.ACC6InternalID = coa.InternalID '
                . 'INNER JOIN m_currency as cur on cur.InternalID = th.CurrencyInternalID '
                . 'Where th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '" AND th.VAT != ' . Auth::user()->SeeNPPN
                . ' AND th.`GrandTotal`*th.`CurrencyRate` > (Select IFNULL(SUM(td.JournalDebet),0) From t_journal_detail td Where td.JournalTransactionID = th.`SalesReturnID`)'
                . 'order by coa.ACC6Name Asc, th.InternalID DESC';
        $sales = DB::select(DB::raw($query));
        return $sales;
    }

    public static function qtyInventory($inventory, $bulan, $tahun) {
        $result = SalesReturnDetail::join('t_salesreturn_header', 't_salesreturn_header.InternalID', '=', 't_salesreturn_detail.SalesReturnInternalID')
                ->whereRaw('YEAR(SalesReturnDate) = "' . $tahun . '" AND MONTH(SalesReturnDate) = "' . $bulan . '"')
                ->where('InventoryInternalID', $inventory)
                ->where('t_salesreturn_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                ->groupBy('t_salesreturn_detail.UomInternalID')
                ->select(DB::raw('sum(Qty) as sumqty, t_salesreturn_detail.UomInternalID'))
                ->get();
        $tampQty = 0;
        foreach ($result as $data) {
            $invenUom = InventoryUom::where('UomInternalID', $data->UomInternalID)->where('InventoryInternalID', $inventory)->first();
            $tampQty += $data->sumqty * $invenUom->Value;
        }
        return $tampQty;
    }

    public static function qtyInventoryWarehouse($inventory, $warehouse, $bulan, $tahun) {
        $result = SalesReturnDetail::join('t_salesreturn_header', 't_salesreturn_header.InternalID', '=', 't_salesreturn_detail.SalesReturnInternalID')
                ->whereRaw('YEAR(SalesReturnDate) = "' . $tahun . '" AND MONTH(SalesReturnDate) = "' . $bulan . '"')
                ->where('InventoryInternalID', $inventory)
                ->where('WarehouseInternalID', $warehouse)
                ->where('t_salesreturn_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                ->groupBy('t_salesreturn_detail.UomInternalID')
                ->select(DB::raw('sum(Qty) as sumqty, t_salesreturn_detail.UomInternalID'))
                ->get();
        $tampQty = 0;
        foreach ($result as $data) {
            $invenUom = InventoryUom::where('UomInternalID', $data->UomInternalID)->where('InventoryInternalID', $inventory)->first();
            $tampQty += $data->sumqty * $invenUom->Value;
        }
        return $tampQty;
    }

    public static function qtyInventorySuperAdmin($inventory, $company) {
        $result = SalesReturnDetail::join('t_salesreturn_header', 't_salesreturn_header.InternalID', '=', 't_salesreturn_detail.SalesReturnInternalID')
                ->where('InventoryInternalID', $inventory)
                ->where('t_salesreturn_header.CompanyInternalID', $company)
                ->groupBy('t_salesreturn_detail.UomInternalID')
                ->select(DB::raw('sum(Qty) as sumqty, t_salesreturn_detail.UomInternalID'))
                ->get();
        $tampQty = 0;
        foreach ($result as $data) {
            $invenUom = InventoryUom::where('UomInternalID', $data->UomInternalID)->where('InventoryInternalID', $inventory)->first();
            $tampQty += $data->sumqty * $invenUom->Value;
        }
        return $tampQty;
    }

    public static function getSumDiscountGlobalSalesReturn($salesID) {
        $query = 'SELECT SUM(th.DiscountGlobal) as total '
                . 'From t_salesreturn_header th '
                . 'Where SalesReturnID LIKE "%-' . $salesID . '" '
                . 'AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" ';
        $salesID = DB::select(DB::raw($query));
        return $salesID[0]->total;
    }

    public static function getSumDiscountGlobalReturnExcept($salesID, $salesReturnID) {
        $query = 'SELECT SUM(th.DiscountGlobal) as total '
                . 'From t_salesreturn_header th '
                . 'Where SalesReturnID LIKE "%-' . $salesID . '" '
                . 'AND th.SalesReturnID != "' . $salesReturnID . '" '
                . 'AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" ';
        $salesID = DB::select(DB::raw($query));
        return $salesID[0]->total;
    }

    public function salesReturnDetail() {
        return $this->hasMany('SalesReturnDetail', 'SalesReturnInternalID', 'InternalID');
    }

    public function salesReturnDescription() {
        return $this->hasMany('SalesReturnDescription', 'SalesReturnInternalID', 'InternalID');
    }

    public function coa6() {
        return $this->belongsTo('Coa6', 'ACC6InternalID', 'InternalID');
    }

    public function currency() {
        return $this->belongsTo('Currency', 'CurrencyInternalID', 'InternalID');
    }

    public function warehouse() {
        return $this->belongsTo('Warehouse', 'WarehouseInternalID', 'InternalID');
    }

}
