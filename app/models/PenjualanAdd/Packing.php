<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Packing extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_packing';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showPacking() {
        return Packing::all();
    }

    public static function advancedSearch($start, $end) {
        $where = '';
//        if ($typePayment != '-1' && $typePayment != '') {
//            $where .= 'isCash = "' . $typePayment . '" ';
//        }
//        if ($typeTax != '-1' && $typeTax != '') {
//            if ($where != '') {
//                $where .= ' AND ';
//            }
//            $where .= 'VAT = "' . $typeTax . '" ';
//        }
        if ($start != '' && $end != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $start = explode('-', $start);
            $end = explode('-', $end);
            $startSearch = $start[2] . '-' . $start[1] . '-' . $start[0];
            $endSearch = $end[2] . '-' . $end[1] . '-' . $end[0];
            $where .= 'PackingDate between "' . $startSearch . ' 00:00:00" AND "' . $endSearch . ' 23:59:59"';
        }

        $query = 'SELECT sh.*, cc.ACC6Name, cc.ACC6ID '
                . 'FROM t_packing sh '
                . 'INNER JOIN m_coa6 cc on cc.InternalID = sh.ACC6InternalID';
        if ($where != '') {
            $query .= ' WHERE ' . $where . ' AND ' . 'sh.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        } else {
            $query .= ' WHERE sh.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        }
        return $result = DB::select(DB::raw($query));
    }

    public static function getIdPacking($packingID) {
        $internalID = Packing::where('PackingID', '=', $packingID)->where('CompanyInternalID', Auth::user()->Company->InternalID)->pluck('InternalID');
        return $internalID;
    }

    public static function getNextIDPacking($text) {
        $query = 'SELECT PackingID From t_shipping_header Where PackingID LIKE "' . $text . '%" AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" order by PackingID desc';
        $packingID = DB::select(DB::raw($query));

        if (count($packingID) <= 0) {
            $packingID = '';
        } else {
            $packingID = $packingID[0]->PackingID;
        }

        if ($packingID == '') {
            $packingID = $text . '0001';
        } else {
            $textTamp = $packingID;
            $packingID = substr($textTamp, (strlen($textTamp) - 4), 4) + 1;
            $packingID = str_pad($packingID, 4, '0', STR_PAD_LEFT);
            $packingID = $text . $packingID;
        }
        return $packingID;
    }

    public function packingShipping() {
        return $this->hasMany('PackingShipping', 'PackingInternalID', 'InternalID');
    }

    public function coa6() {
        return $this->belongsTo('Coa6', 'ACC6InternalID', 'InternalID');
    }
}
