<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class PackingShipping extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_packing_shipping';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showPackingShipping() {
        return PackingShipping::all();
    }
    
    public function packing() {
        return $this->belongsTo('Packing', 'PackingInternalID', 'InternalID');
    }

    public function shipping() {
        return $this->belongsTo('ShippingAddHeader', 'ShippingInternalID', 'InternalID');
    }
}
