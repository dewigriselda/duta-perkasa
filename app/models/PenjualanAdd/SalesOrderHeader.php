<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class SalesOrderHeader extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_salesorder_header';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showSalesOrderHeader() {
        return SalesOrderHeader::all();
    }

    public static function checkNotShipping($cust, $iscash) {
        $query = 'SELECT Distinct a.* FROM t_salesorder_header as a '
                . 'WHERE a.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"'
                . ' and a.ACC6InternalID = "' . $cust . '" and a.isCash="' . $iscash . '"'
                . ' and a.Status="1" and is_CBD_Finished = 0'
                . ' and a.InternalID not in(select distinct SalesOrderInternalID from t_shipping_header)'
                . ' and a.InternalID not in(select distinct SalesOrderInternalID from t_sales_header)'
                . ' order by a.SalesOrderDate desc';
        $salesOrder = DB::select(DB::raw($query));
        return $salesOrder;
    }
    
    public static function bisaUpdateTopUp($cust, $iscash) {
        $query = 'SELECT Distinct a.* FROM t_salesorder_header as a '
                . 'WHERE a.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"'
                . ' and a.ACC6InternalID = "' . $cust . '" and a.isCash="' . $iscash . '"'
                . ' and a.Status="1" and is_CBD_Finished = 0'
                . ' and a.InternalID not in(select distinct SalesOrderInternalID from t_shipping_header)'
                . ' and a.InternalID not in(select distinct SalesOrderInternalID from t_sales_header)'
                . ' order by a.SalesOrderDate desc';
        $salesOrder = DB::select(DB::raw($query));
        return $salesOrder;
    }
    
    public static function advancedSearch($typePayment, $typeTax, $start, $end) {
        $where = '';
        if($start == '' && $end == ''){
            $end = date('d-m-Y');
            $start = date('d-m-Y', strtotime('-7 days', strtotime($end)));
            //$where .= 'SalesOrderDate between "' . $start . ' 00:00:00" AND "' . $end. ' 23:59:59"';
        }
        
        if ($typePayment != '-1' && $typePayment != '') {
            $where .= 'isCash = "' . $typePayment . '" ';
        }
        if ($typeTax != '-1' && $typeTax != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'VAT = "' . $typeTax . '" ';
        }
        if ($start != '' && $end != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $start = explode('-', $start);
            $end = explode('-', $end);
            $startSearch = $start[2] . '-' . $start[1] . '-' . $start[0];
            $endSearch = $end[2] . '-' . $end[1] . '-' . $end[0];
            $where .= 'SalesOrderDate between "' . $startSearch . ' 00:00:00" AND "' . $endSearch . ' 23:59:59"';
        }

        $query = 'SELECT sh.*, c.CurrencyName, cc.ACC6Name, cc.ACC6ID '
                . 'FROM t_salesorder_header sh '
                . 'INNER JOIN m_currency c on c.InternalID = sh.CurrencyInternalID '
                . 'INNER JOIN m_coa6 cc on cc.InternalID = sh.ACC6InternalID';
        if ($where != '') {
            $query .= ' WHERE ' . $where . ' AND ' . 'sh.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        } else {
            $query .= ' WHERE sh.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        }
        
        $result = DB::select(DB::raw($query));
        //dd($query);
        return $result;
    }

    public static function getIdsalesOrder($salesOrderID) {
        $internalID = SalesOrderHeader::where('SalesOrderID', '=', $salesOrderID)->where('CompanyInternalID', Auth::user()->Company->InternalID)->pluck('InternalID');
        return $internalID;
    }

    public static function getNextIDSalesOrder($text) {
        $query = 'SELECT SalesOrderID From t_salesorder_header Where SalesOrderID LIKE "' . $text . '%" AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" order by SalesOrderID desc';
        $salesOrderID = DB::select(DB::raw($query));

        if (count($salesOrderID) <= 0) {
            $salesOrderID = '';
        } else {
            $salesOrderID = $salesOrderID[0]->SalesOrderID;
        }

        if ($salesOrderID == '') {
            $salesOrderID = $text . '0001';
        } else {
            $textTamp = $salesOrderID;
            $salesOrderID = substr($textTamp, (strlen($textTamp) - 4), 4) + 1;
            $salesOrderID = str_pad($salesOrderID, 4, '0', STR_PAD_LEFT);
            $salesOrderID = $text . $salesOrderID;
        }
        return $salesOrderID;
    }

    public static function getTopTen() {
        $query = 'SELECT table2.* FROM ('
                . 'SELECT SalesOrderDate, GrandTotal*CurrencyRate as hasil From t_salesorder_header '
                . 'WHERE CompanyInternalID = "' . Auth::user()->Company->InternalID . '" '
                . 'Order by SalesOrderDate desc Limit 0,10'
                . ') as table2 '
                . 'Order by table2.SalesOrderDate asc';
        $top = DB::select(DB::raw($query));
        return $top;
    }

    public static function getSalesOrder10($id) {
        $query = 'SELECT SUM(table2.hasil) as hasil'
                . ' FROM '
                . '(SELECT c.ACC6Name,th.GrandTotal*th.CurrencyRate as hasil '
                . ' From t_salesorder_header th INNER JOIN m_coa6 as c on c.InternalID = th.ACC6InternalID '
                . ' Where c.InternalID = "' . $id . '" '
                . ' AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"'
                . ' Order by th.SalesOrderDate desc Limit 0,10) as table2';
        $salesOrder = DB::select(DB::raw($query));
        return $salesOrder;
    }

    public static function getSalesOrderReceivable() {
        $query = 'SELECT th.*, th.SalesOrderID as ID, th.SalesOrderDate as Date, coa.ACC6Name as coa6, cur.CurrencyName '
                . 'FROM t_salesorder_header th INNER JOIN m_coa6 as coa on th.ACC6InternalID = coa.InternalID '
                . 'INNER JOIN m_currency as cur on cur.InternalID = th.CurrencyInternalID '
                . 'where th.isCash = 1 '
                . 'AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '" '
                . 'AND th.`GrandTotal`*th.`CurrencyRate` > (Select IFNULL(SUM(td.JournalCreditMU),0) From t_journal_detail td Where td.JournalTransactionID = th.`SalesOrderID`)';
        $salesOrder = DB::select(DB::raw($query));
        return $salesOrder;
    }

    public static function getSalesOrderReceivableAging() {
        $query = 'SELECT DATEDIFF(DATE_ADD(th.SalesOrderDate,INTERVAL th.LongTerm DAY),Now()) as selisihHari, th.GrandTotal*th.CurrencyRate as GrandTotal '
                . 'FROM t_salesorder_header th INNER JOIN m_coa6 as coa on th.ACC6InternalID = coa.InternalID '
                . 'where th.isCash = 1 '
                . 'AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '" '
                . 'AND th.`GrandTotal`*th.`CurrencyRate` > (Select IFNULL(SUM(td.JournalCreditMU),0) From t_journal_detail td Where td.JournalTransactionID = th.`SalesOrderID`)';
        $salesOrder = DB::select(DB::raw($query));
        return $salesOrder;
    }

    public static function getSlipInternalID($salesOrderID) {
        $journal = JournalHeader::where('TransactionID', $salesOrderID)->first();
        $detail = JournalDetail::where('JournalInternalID', $journal->InternalID)->where('JournalIndex', '2')->first();
        $slip = Slip::where('ACC1InternalID', $detail->ACC1InternalID)
                        ->where('ACC2InternalID', $detail->ACC2InternalID)
                        ->where('ACC3InternalID', $detail->ACC3InternalID)
                        ->where('ACC4InternalID', $detail->ACC4InternalID)
                        ->where('ACC5InternalID', $detail->ACC5InternalID)
                        ->where('ACC6InternalID', $detail->ACC6InternalID)->first();
        return $slip->InternalID;
    }

    public static function isInvoice($internalID) {
        $sales = SalesHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)
                        ->where('SalesOrderInternalID', $internalID)->count();
        if ($sales > 0) {
            return true;
        }
        return false;
    }
    public static function isShipping($internalID) {
        $sales = ShippingAddHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)
                        ->where('SalesOrderInternalID', $internalID)->count();
        if ($sales > 0) {
            return true;
        }
        return false;
    }

    public static function getSalesReceivable($Coa6InternalID) {
        $query = 'SELECT th.*, th.SalesID as ID, th.SalesDate as Date, coa.ACC6Name as coa6, cur.CurrencyName '
                . 'FROM t_sales_header th INNER JOIN m_coa6 as coa on th.ACC6InternalID = coa.InternalID '
                . 'INNER JOIN m_currency as cur on cur.InternalID = th.CurrencyInternalID '
                . 'where th.isCash = 1 '
                . 'AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '" '
                . 'AND th.ACC6InternalID = "' . $Coa6InternalID . '" '
                . 'AND th.`GrandTotal`*th.`CurrencyRate` > (Select IFNULL(SUM(td.JournalCreditMU),0) From t_journal_detail td Where td.JournalTransactionID = th.`SalesID`)'
                . 'order by coa.ACC6Name Asc, th.InternalID DESC';
        $sales = DB::select(DB::raw($query));
        return $sales;
    }

    public static function getIdQuotation($quotationID) {
        $internalID = QuotationHeader::where('QuotationID', '=', $quotationID)->where('CompanyInternalID', Auth::user()->Company->InternalID)->pluck('InternalID');
        return $internalID;
    }

    public function salesOrderDetail() {
        return $this->hasMany('SalesOrderDetail', 'SalesOrderInternalID', 'InternalID');
    }
    
    public function salesOrderDescription() {
        return $this->hasMany('SalesOrderDescription', 'SalesOrderInternalID', 'InternalID');
    }

    public function coa6() {
        return $this->belongsTo('Coa6', 'ACC6InternalID', 'InternalID');
    }

    public function currency() {
        return $this->belongsTo('Currency', 'CurrencyInternalID', 'InternalID');
    }

    public function warehouse() {
        return $this->belongsTo('Warehouse', 'WarehouseInternalID', 'InternalID');
    }

    public function salesHeader() {
        return $this->hasMany('SalesHeader', 'SalesOrderInternalID', 'InternalID');
    }

    public function salesAddHeader() {
        return $this->hasMany('SalesAddHeader', 'SalesOrderInternalID', 'InternalID');
    }

    public function quotationSales() {
        return $this->hasMany('quotationSales', 'SalesOrderInternalID', 'InternalID');
    }

}
