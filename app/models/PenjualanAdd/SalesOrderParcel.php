<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class SalesOrderParcel extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_salesorder_parcel';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showSalesOrderParcel() {
        return SalesOrderParcel::all();
    }

    public function salesOrderDetail() {
        return $this->hasMany('SalesOrderDetail', 'SalesOrderParcelInternalID', 'InternalID');
    }

    public function parcel() {
        return $this->belongsTo('Parcel', 'ParcelInternalID', 'InternalID');
    }

}
