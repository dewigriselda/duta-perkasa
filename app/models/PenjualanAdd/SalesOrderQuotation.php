<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class SalesOrderQuotation extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_salesorder_quotation';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showSalesOrderQuotation() {
        return SalesOrderQuotation::all();
    }

    public function salesOrder() {
        return $this->hasMany('SalesOrderHeader', 'SalesOrderInternalID', 'InternalID');
    }
    
    public function quotation() {
        return $this->belongsTo('QuotationHeader', 'QuotationInternalID', 'InternalID');
    }


}
