<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class ShippingAddHeader extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_shipping_header';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showShippingHeader() {
        return ShippingAddHeader::all();
    }

    public static function advancedSearch($typePayment, $typeTax, $start, $end) {
        $where = '';
        if ($typePayment != '-1' && $typePayment != '') {
            $where .= 'isCash = "' . $typePayment . '" ';
        }
        if ($typeTax != '-1' && $typeTax != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'VAT = "' . $typeTax . '" ';
        }
        if ($start != '' && $end != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $start = explode('-', $start);
            $end = explode('-', $end);
            $startSearch = $start[2] . '-' . $start[1] . '-' . $start[0];
            $endSearch = $end[2] . '-' . $end[1] . '-' . $end[0];
            $where .= 'ShippingDate between "' . $startSearch . ' 00:00:00" AND "' . $endSearch . ' 23:59:59"';
        }

        $query = 'SELECT sh.*, cc.ACC6Name, cc.ACC6ID '
                . 'FROM t_shipping_header sh '
                . 'INNER JOIN m_coa6 cc on cc.InternalID = sh.ACC6InternalID';
        if ($where != '') {
            $query .= ' WHERE ' . $where . ' AND ' . 'sh.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        } else {
            $query .= ' WHERE sh.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        }
        return $result = DB::select(DB::raw($query));
    }

    public static function searchParcel($shippingAddInternalID) {
        $query = 'SELECT p.ParcelID, p.ParcelName, pp.* FROM t_shipping_parcel pp 
            INNER JOIN m_parcel p WHERE pp.ShippingInternalID = ' . $shippingAddInternalID;
        $top = DB::select(DB::raw($query));
        return $top;
    }

    public static function getIdshipping($shippingID) {
        $internalID = ShippingAddHeader::where('ShippingID', '=', $shippingID)->where('CompanyInternalID', Auth::user()->Company->InternalID)->pluck('InternalID');
        return $internalID;
    }

    public static function getNextIDShipping($text) {
        $query = 'SELECT ShippingID From t_shipping_header Where ShippingID LIKE "' . $text . '%" AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" order by ShippingID desc';
        $shippingID = DB::select(DB::raw($query));

        if (count($shippingID) <= 0) {
            $shippingID = '';
        } else {
            $shippingID = $shippingID[0]->ShippingID;
        }

        if ($shippingID == '') {
            $shippingID = $text . '0001';
        } else {
            $textTamp = $shippingID;
            $shippingID = substr($textTamp, (strlen($textTamp) - 4), 4) + 1;
            $shippingID = str_pad($shippingID, 4, '0', STR_PAD_LEFT);
            $shippingID = $text . $shippingID;
        }
        return $shippingID;
    }

    public static function qtyInventoryWarehouse($inventory, $warehouse, $bulan, $tahun) {
        $result = ShippingAddDetail::join('t_shipping_header', 't_shipping_header.InternalID', '=', 't_shipping_detail.ShippingInternalID')
                ->whereRaw('YEAR(ShippingDate) = "' . $tahun . '" AND MONTH(ShippingDate) = "' . $bulan . '"')
                ->where('InventoryInternalID', $inventory)
                ->where('WarehouseInternalID', $warehouse)
                ->where('t_shipping_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                ->groupBy('t_shipping_detail.UomInternalID')
                ->select(DB::raw('sum(Qty) as sumqty, t_shipping_detail.UomInternalID'))
                ->get();
        $tampQty = 0;
        foreach ($result as $data) {
            $invenUom = InventoryUom::where('UomInternalID', $data->UomInternalID)->where('InventoryInternalID', $inventory)->first();
            $tampQty += $data->sumqty * $invenUom->Value;
        }
        return $tampQty;
    }

    public static function getShippingReceivable() {
        $query = 'SELECT th.*, th.ShippingID as ID, th.ShippingDate as Date, coa.ACC6Name as coa6 '
                . 'FROM t_shipping_header th INNER JOIN m_coa6 as coa on th.ACC6InternalID = coa.InternalID '
                . 'where th.isCash = 1 '
                . 'AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '" ';
        $shipping = DB::select(DB::raw($query));
        $tamp = array();
        foreach ($shipping as $value) {
            $data = date("Y-m-d", strtotime("+" . $value->LongTerm . " day", strtotime($value->Date)));
            array_push($tamp, $data);
        }
        array_multisort($tamp, $shipping);
        return $shipping;
    }

    public static function getShippingReceivableAging() {
        $query = 'SELECT DATEDIFF(DATE_ADD(th.ShippingDate,INTERVAL th.LongTerm DAY),Now()) as selisihHari '
                . 'FROM t_shipping_header th INNER JOIN m_coa6 as coa on th.ACC6InternalID = coa.InternalID '
                . 'where th.isCash = 1 '
                . 'AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '" ';
        $shipping = DB::select(DB::raw($query));
        return $shipping;
    }

    public static function getSumShipping($inventoryID, $shippingID, $InternalID) {
        $query = 'SELECT SUM(td.Qty) as total '
                . 'From t_shipping_detail td INNER JOIN t_shipping_header th on td.ShippingInternalID = th.InternalID '
                . 'Where td.InventoryInternalID = "' . $inventoryID . '" '
                . 'AND td.SalesOrderDetailInternalID = "' . $InternalID . '" '
                . 'AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" AND td.ShippingParcelInternalID = 0 ';
        $shippingID = DB::select(DB::raw($query));
        return $shippingID[0]->total;
    }

    public static function getSumDescriptionShipping($descriptionID) {
        $query = 'SELECT SUM(td.Qty) as total '
                . 'From t_shipping_detail_description td INNER JOIN t_shipping_header th on td.ShippingInternalID = th.InternalID '
                . 'Where td.SalesOrderDescriptionInternalID = "' . $descriptionID . '" '
                . 'AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        $shippingID = DB::select(DB::raw($query));
        return $shippingID[0]->total;
    }

    public static function getSumDescriptionShippingExcept($descriptionID, $except) {
        $query = 'SELECT SUM(td.Qty) as total '
                . 'From t_shipping_detail_description td INNER JOIN t_shipping_header th on td.ShippingInternalID = th.InternalID '
                . 'Where td.SalesOrderDescriptionInternalID = "' . $descriptionID . '" '
                . 'AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '"'
                . ' AND th.InternalID != "' . $except . '"';
        $shippingID = DB::select(DB::raw($query));
        return $shippingID[0]->total;
    }

    public static function getSumShippingExcept($inventoryID, $shippingID, $InternalID) {
        $query = 'SELECT SUM(td.Qty) as total '
                . 'From t_shipping_detail td INNER JOIN t_shipping_header th on td.ShippingInternalID = th.InternalID '
                . 'Where td.InventoryInternalID = "' . $inventoryID . '" '
                . 'AND th.InternalID != "' . $shippingID . '" '
                . 'AND td.SalesOrderDetailInternalID = "' . $InternalID . '" '
                . 'AND td.is_deleted = 0 '
                . 'AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" ';
        $shippingID = DB::select(DB::raw($query));
        return $shippingID[0]->total;
    }

    public static function getSumShippingParcel($parcelID, $shippingID, $InternalID) {
        $query = 'SELECT SUM(td.Qty) as total '
                . 'From t_shipping_parcel td INNER JOIN t_shipping_header th on td.ShippingInternalID = th.InternalID '
                . 'Where td.ParcelInternalID = "' . $parcelID . '" '
                . ' AND td.SalesOrderParcelDetailInternalID = "' . $InternalID . '" '
                . 'AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '" ';
        $shippingID = DB::select(DB::raw($query));
        return $shippingID[0]->total;
    }

    public static function getSumShippingExceptParcel($inventoryID, $shippingID, $InternalID) {
        $query = 'SELECT SUM(td.Qty) as total '
                . 'From t_shipping_parcel td INNER JOIN t_shipping_header th on td.ShippingInternalID = th.InternalID '
                . 'Where td.ParcelInternalID = "' . $inventoryID . '" '
                . 'AND th.InternalID != "' . $shippingID . '" '
                . 'AND td.SalesOrderParcelDetailInternalID = "' . $InternalID . '" '
                . 'AND td.is_deleted = 0 '
                . 'AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '" ';
        $shippingID = DB::select(DB::raw($query));
        return $shippingID[0]->total;
    }

    public static function getSlipInternalID($shippingID) {
        $journal = JournalHeader::where('TransactionID', $shippingID)->first();
        $detail = JournalDetail::where('JournalInternalID', $journal->InternalID)->where('JournalIndex', '2')->first();
        $slip = Slip::where('ACC1InternalID', $detail->ACC1InternalID)
                        ->where('ACC2InternalID', $detail->ACC2InternalID)
                        ->where('ACC3InternalID', $detail->ACC3InternalID)
                        ->where('ACC4InternalID', $detail->ACC4InternalID)
                        ->where('ACC5InternalID', $detail->ACC5InternalID)
                        ->where('ACC6InternalID', $detail->ACC6InternalID)->first();
        return $slip->InternalID;
    }

    public static function qtyInventory_lama($inventory, $bulan, $tahun) {
        $result = ShippingDetail::join('t_shipping_header', 't_shipping_header.InternalID', '=', 't_shipping_detail.ShippingInternalID')
                ->whereRaw('YEAR(ShippingDate) = "' . $tahun . '" AND MONTH(ShippingDate) = "' . $bulan . '"')
                ->where('InventoryInternalID', $inventory)
                ->where('t_shipping_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                ->sum('Qty');
        return $result;
    }

    public static function qtyInventory($inventory, $bulan, $tahun) {
        $result = ShippingAddDetail::join('t_shipping_header', 't_shipping_header.InternalID', '=', 't_shipping_detail.ShippingInternalID')
                ->whereRaw('YEAR(ShippingDate) = "' . $tahun . '" AND MONTH(ShippingDate) = "' . $bulan . '"')
                ->where('InventoryInternalID', $inventory)
                ->where('t_shipping_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                ->groupBy('t_shipping_detail.UomInternalID')
                ->select(DB::raw('sum(Qty) as sumqty, t_shipping_detail.UomInternalID'))
                ->get();
        $tampQty = 0;
        foreach ($result as $data) {
            $invenUom = InventoryUom::where('UomInternalID', $data->UomInternalID)->where('InventoryInternalID', $inventory)->first();
            $tampQty += $data->sumqty * $invenUom->Value;
        }
        return $tampQty;
    }

    public static function qtyInventorySuperAdmin($inventory, $company) {
        $result = ShippingDetail::join('t_shipping_header', 't_shipping_header.InternalID', '=', 't_shipping_detail.ShippingInternalID')
                ->where('InventoryInternalID', $inventory)
                ->where('t_shipping_header.CompanyInternalID', $company)
                ->sum('Qty');
        return $result;
    }

    public static function getYearMin() {
        $result = ShippingAddHeader::orderBy('ShippingDate')->select(DB::raw('YEAR(ShippingDate) as ShippingDates'))->where("CompanyInternalID", Auth::User()->CompanyInternalID)->first();
        if ($result == '') {
            return '0';
        }
        return $result->ShippingDates;
    }

    public static function getSumDiscountGlobalShipping($shippingID) {
        $query = 'SELECT SUM(th.DiscountGlobal) as total '
                . 'From t_shipping_header th '
                . 'Where th.SalesOrderInternalID = "' . $shippingID . '" '
                . 'AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" ';
        $shippingID = DB::select(DB::raw($query));
        return $shippingID[0]->total;
    }

    public function shippingDetail() {
        return $this->hasMany('ShippingAddDetail', 'ShippingInternalID', 'InternalID');
    }

    public function shippingDescription() {
        return $this->hasMany('ShippingAddDescription', 'ShippingInternalID', 'InternalID');
    }

    public function coa6() {
        return $this->belongsTo('Coa6', 'ACC6InternalID', 'InternalID');
    }

    public function warehouse() {
        return $this->belongsTo('Warehouse', 'WarehouseInternalID', 'InternalID');
    }

    public function salesOrder() {
        return $this->belongsTo('SalesOrderHeader', 'SalesOrderInternalID', 'InternalID');
    }

}
