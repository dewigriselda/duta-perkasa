<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class QuotationDescription extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_quotation_detail_description';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showQuotationDescription() {
        return QuotationDescription::all();
    }

    public function quotationHeader() {
        return $this->belongsTo('QuotationHeader', 'QuotationInternalID', 'InternalID');
    }

    public function quotationParcel() {
        return $this->belongsTo('QuotationParcel', 'QuotationParcelInternalID', 'InternalID');
    }

}
