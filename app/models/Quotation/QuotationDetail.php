<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class QuotationDetail extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_quotation_detail';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showQuotationDetail() {
        return QuotationDetail::all();
    }

    public function quotationHeader() {
        return $this->belongsTo('QuotationHeader', 'QuotationInternalID', 'InternalID');
    }

    public function inventory() {
        return $this->belongsTo('Inventory', 'InventoryInternalID', 'InternalID');
    }

    public function uom() {
        return $this->belongsTo('Uom', 'UomInternalID', 'InternalID');
    }

    public function quotationParcel() {
        return $this->belongsTo('QuotationParcel', 'QuotationParcelInternalID', 'InternalID');
    }

}
