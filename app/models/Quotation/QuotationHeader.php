<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class QuotationHeader extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_quotation_header';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showQuotationHeader() {
        return QuotationHeader::all();
    }

    public static function advancedSearch($coa6, $typePayment, $typeTax, $start, $end) {
        $where = '';
        if ($typePayment != '-1' && $typePayment != '') {
            $where .= 'isCash = "' . $typePayment . '" ';
        }
//        dd($coa6);
        if ($coa6 != '-1' && $coa6 != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'ACC6InternalID = ' . $coa6 . ' ';
        }
        if ($typeTax != '-1' && $typeTax != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'VAT = "' . $typeTax . '" ';
        }
        if ($start != '' && $end != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $start = explode('-', $start);
            $end = explode('-', $end);
            $startSearch = $start[2] . '-' . $start[1] . '-' . $start[0];
            $endSearch = $end[2] . '-' . $end[1] . '-' . $end[0];
            $where .= 'QuotationDate between "' . $startSearch . ' 00:00:00" AND "' . $endSearch . ' 23:59:59"';
        }

        $query = 'SELECT sh.*, c.CurrencyName, cc.ACC6Name, cc.ACC6ID '
                . 'FROM t_quotation_header sh '
                . 'INNER JOIN m_currency c on c.InternalID = sh.CurrencyInternalID '
                . 'INNER JOIN m_coa6 cc on cc.InternalID = sh.ACC6InternalID';
        if ($where != '') {
            $query .= ' WHERE ' . $where . ' AND ' . 'sh.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        } else {
            $query .= ' WHERE sh.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        }
//        dd($query);
        $result = DB::select(DB::raw($query));
//        dd($result);
        return $result;
    }

    public static function getIdquotation($quotationID) {
        $internalID = QuotationHeader::where('QuotationID', '=', $quotationID)->where('CompanyInternalID', Auth::user()->Company->InternalID)->pluck('InternalID');
        return $internalID;
    }

    public static function getNextIDQuotation($text) {
        $query = 'SELECT QuotationID From t_quotation_header Where QuotationID LIKE "' . $text . '%" AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" order by QuotationID desc';
        $quotationID = DB::select(DB::raw($query));

        if (count($quotationID) <= 0) {
            $quotationID = '';
        } else {
            $quotationID = $quotationID[0]->QuotationID;
        }

        if ($quotationID == '') {
            $quotationID = $text . '0001';
        } else {
            $textTamp = $quotationID;
            $quotationID = substr($textTamp, (strlen($textTamp) - 4), 4) + 1;
            $quotationID = str_pad($quotationID, 4, '0', STR_PAD_LEFT);
            $quotationID = $text . $quotationID;
        }
        return $quotationID;
    }

    public static function getTopTen() {
        $query = 'SELECT table2.* FROM ('
                . 'SELECT QuotationDate, GrandTotal*CurrencyRate as hasil From t_quotation_header '
                . 'WHERE CompanyInternalID = "' . Auth::user()->Company->InternalID . '" '
                . 'Order by QuotationDate desc Limit 0,10'
                . ') as table2 '
                . 'Order by table2.QuotationDate asc';
        $top = DB::select(DB::raw($query));
        return $top;
    }

    public static function getQuotation10($id) {
        $query = 'SELECT SUM(table2.hasil) as hasil'
                . ' FROM '
                . '(SELECT c.ACC6Name,th.GrandTotal*th.CurrencyRate as hasil '
                . ' From t_quotation_header th INNER JOIN m_coa6 as c on c.InternalID = th.ACC6InternalID '
                . ' Where c.InternalID = "' . $id . '" '
                . ' AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"'
                . ' Order by th.QuotationDate desc Limit 0,10) as table2';
        $quotation = DB::select(DB::raw($query));
        return $quotation;
    }

    public static function getQuotationReceivable() {
        $query = 'SELECT th.*, th.QuotationID as ID, th.QuotationDate as Date, coa.ACC6Name as coa6, cur.CurrencyName '
                . 'FROM t_quotation_header th INNER JOIN m_coa6 as coa on th.ACC6InternalID = coa.InternalID '
                . 'INNER JOIN m_currency as cur on cur.InternalID = th.CurrencyInternalID '
                . 'where th.isCash = 1 '
                . 'AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '" '
                . 'AND th.`GrandTotal`*th.`CurrencyRate` > (Select IFNULL(SUM(td.JournalCreditMU),0) From t_journal_detail td Where td.JournalTransactionID = th.`QuotationID`)';
        $quotation = DB::select(DB::raw($query));
        return $quotation;
    }

    public static function getQuotationReceivableAging() {
        $query = 'SELECT DATEDIFF(DATE_ADD(th.QuotationDate,INTERVAL th.LongTerm DAY),Now()) as selisihHari, th.GrandTotal*th.CurrencyRate as GrandTotal '
                . 'FROM t_quotation_header th INNER JOIN m_coa6 as coa on th.ACC6InternalID = coa.InternalID '
                . 'where th.isCash = 1 '
                . 'AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '" '
                . 'AND th.`GrandTotal`*th.`CurrencyRate` > (Select IFNULL(SUM(td.JournalCreditMU),0) From t_journal_detail td Where td.JournalTransactionID = th.`QuotationID`)';
        $quotation = DB::select(DB::raw($query));
        return $quotation;
    }

    public static function getSlipInternalID($quotationID) {
        $journal = JournalHeader::where('TransactionID', $quotationID)->first();
        $detail = JournalDetail::where('JournalInternalID', $journal->InternalID)->where('JournalIndex', '2')->first();
        $slip = Slip::where('ACC1InternalID', $detail->ACC1InternalID)
                        ->where('ACC2InternalID', $detail->ACC2InternalID)
                        ->where('ACC3InternalID', $detail->ACC3InternalID)
                        ->where('ACC4InternalID', $detail->ACC4InternalID)
                        ->where('ACC5InternalID', $detail->ACC5InternalID)
                        ->where('ACC6InternalID', $detail->ACC6InternalID)->first();
        return $slip->InternalID;
    }

    public static function isInvoice($internalID) {
        $quotation = SalesOrderHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)
                        ->where('InternalID', $internalID)->count();
        if ($quotation > 0) {
            return true;
        }
        return false;
    }

    public function quotationDetail() {
        return $this->hasMany('QuotationDetail', 'QuotationInternalID', 'InternalID');
    }

    public function quotationDescription() {
        return $this->hasMany('QuotationDescription', 'QuotationInternalID', 'InternalID');
    }

    public function coa6() {
        return $this->belongsTo('Coa6', 'ACC6InternalID', 'InternalID');
    }

    public function currency() {
        return $this->belongsTo('Currency', 'CurrencyInternalID', 'InternalID');
    }

    public function warehouse() {
        return $this->belongsTo('Warehouse', 'WarehouseInternalID', 'InternalID');
    }

    public function quotationSales() {
        return $this->hasMany('quotationSales', 'QuotationInternalID', 'InternalID');
    }

}
