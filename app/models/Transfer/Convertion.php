<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Convertion extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'h_convertion';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showConvertion() {
        return Convertion::all();
    }

    public function inventoryUom1() {
        return $this->belongsTo('InventoryUom', 'InventoryUomInternalID1', 'InternalID');
    }

    public function inventoryUom2() {
        return $this->belongsTo('InventoryUom', 'InventoryUomInternalID2', 'InternalID');
    }

    public function warehouse() {
        return $this->belongsTo('Warehouse', 'WarehouseInternalID', 'InternalID');
    }

}
