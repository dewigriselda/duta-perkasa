<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class TransferOrder extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_transfer_salesorder';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showTransferOrder() {
        return TransferOrder::all();
    }

    public function transferHeader() {
        return $this->belongsTo('TransferHeader', 'TransferInternalID', 'InternalID');
    }

    public function salesOrderHeader() {
        return $this->belongsTo('SalesOrderHeader', 'SalesOrderInternalID', 'InternalID');
    }

}
