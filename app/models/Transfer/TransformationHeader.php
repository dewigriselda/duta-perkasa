<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class TransformationHeader extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_transformation_header';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showTransformationHeader() {
        return TransformationHeader::all();
    }

    public static function getIdmemoIn($memoInID) {
        $internalID = TransformationHeader::where('TransformationID', '=', $memoInID)->where('CompanyInternalID', Auth::user()->Company->InternalID)->pluck('InternalID');
        return $internalID;
    }

    public static function getIdTransformation($transformationID) {
        $internalID = TransformationHeader::where('TransformationID', '=', $transformationID)->where('CompanyInternalID', Auth::user()->Company->InternalID)->pluck('InternalID');
        return $internalID;
    }

    public static function qtyInInventory($inventory, $bulan, $tahun) {
        $result = TransformationDetail::join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                ->whereRaw('YEAR(TransformationDate) = "' . $tahun . '" AND MONTH(TransformationDate) = "' . $bulan . '"')
                ->where('InventoryInternalID', $inventory)
                ->where('Type', 'in')
                ->where('t_transformation_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                ->groupBy('t_transformation_detail.UomInternalID')
                ->select(DB::raw('sum(Qty) as sumqty, t_transformation_detail.UomInternalID'))
                ->get();
        $tampQty = 0;
        foreach ($result as $data) {
            $invenUom = InventoryUom::where('UomInternalID', $data->UomInternalID)->where('InventoryInternalID', $inventory)->first();
            $tampQty += $data->sumqty * $invenUom->Value;
        }
        return $tampQty;
    }
    public static function qtyOutInventory($inventory, $bulan, $tahun) {
        $result = TransformationDetail::join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                ->whereRaw('YEAR(TransformationDate) = "' . $tahun . '" AND MONTH(TransformationDate) = "' . $bulan . '"')
                ->where('InventoryInternalID', $inventory)
                ->where('Type', 'out')
                ->where('t_transformation_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                ->groupBy('t_transformation_detail.UomInternalID')
                ->select(DB::raw('sum(Qty) as sumqty, t_transformation_detail.UomInternalID'))
                ->get();
        $tampQty = 0;
        foreach ($result as $data) {
            $invenUom = InventoryUom::where('UomInternalID', $data->UomInternalID)->where('InventoryInternalID', $inventory)->first();
            $tampQty += $data->sumqty * $invenUom->Value;
        }
        return $tampQty;
    }

    public static function valueInventory($inventory, $bulan, $tahun) {
        $result = TransformationDetail::join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.TransformationInternalID')
                ->whereRaw('YEAR(TransformationDate) = "' . $tahun . '" AND MONTH(TransformationDate) = "' . $bulan . '"')
                ->where('InventoryInternalID', $inventory)
                ->where('t_memoin_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                ->select(DB::raw('sum(SubTotal*CurrencyRate) as SubTotalCurrency'))
                ->pluck('SubTotalCurrency');
        return $result;
    }

    public static function qtyInInventoryWarehouse($inventory, $warehouse, $bulan, $tahun) {
        $result = TransformationDetail::join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                ->whereRaw('YEAR(TransformationDate) = "' . $tahun . '" AND MONTH(TransformationDate) = "' . $bulan . '"')
                ->where('InventoryInternalID', $inventory)
                ->where('WarehouseInternalID', $warehouse)
                ->where('Type', 'in')
                ->where('t_transformation_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                ->groupBy('t_transformation_detail.UomInternalID')
                ->select(DB::raw('sum(Qty) as sumqty, t_transformation_detail.UomInternalID'))
                ->get();
        $tampQty = 0;
        foreach ($result as $data) {
            $invenUom = InventoryUom::where('UomInternalID', $data->UomInternalID)->where('InventoryInternalID', $inventory)->first();
            $tampQty += $data->sumqty * $invenUom->Value;
        }
        return $tampQty;
    }
    public static function qtyOutInventoryWarehouse($inventory, $warehouse, $bulan, $tahun) {
        $result = TransformationDetail::join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                ->whereRaw('YEAR(TransformationDate) = "' . $tahun . '" AND MONTH(TransformationDate) = "' . $bulan . '"')
                ->where('InventoryInternalID', $inventory)
                ->where('WarehouseInternalID', $warehouse)
                ->where('Type', 'out')
                ->where('t_transformation_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                ->groupBy('t_transformation_detail.UomInternalID')
                ->select(DB::raw('sum(Qty) as sumqty, t_transformation_detail.UomInternalID'))
                ->get();
        $tampQty = 0;
        foreach ($result as $data) {
            $invenUom = InventoryUom::where('UomInternalID', $data->UomInternalID)->where('InventoryInternalID', $inventory)->first();
            $tampQty += $data->sumqty * $invenUom->Value;
        }
        return $tampQty;
    }

    public static function valueInventoryWarehouse($inventory, $warehouse, $bulan, $tahun) {
        $result = TransformationDetail::join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.TransformationInternalID')
                ->whereRaw('YEAR(TransformationDate) = "' . $tahun . '" AND MONTH(TransformationDate) = "' . $bulan . '"')
                ->where('InventoryInternalID', $inventory)
                ->where('WarehouseInternalID', $warehouse)
                ->where('t_memoin_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                ->select(DB::raw('sum(SubTotal*CurrencyRate) as SubTotalCurrency'))
                ->pluck('SubTotalCurrency');
        return $result;
    }
    
    public static function valueInInventoryWarehouse($inventory, $warehouse, $bulan, $tahun) {
        $result = TransformationDetail::join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                ->whereRaw('YEAR(TransformationDate) = "' . $tahun . '" AND MONTH(TransformationDate) = "' . $bulan . '"')
                ->where('InventoryInternalID', $inventory)
                ->where('WarehouseInternalID', $warehouse)
                ->where('Type', 'in')
                ->where('t_transformation_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                ->select(DB::raw('sum(SubTotal*CurrencyRate) as SubTotalCurrency'))
                ->pluck('SubTotalCurrency');
        return $result;
    }
    public static function valueOutInventoryWarehouse($inventory, $warehouse, $bulan, $tahun) {
        $result = TransformationDetail::join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                ->whereRaw('YEAR(TransformationDate) = "' . $tahun . '" AND MONTH(TransformationDate) = "' . $bulan . '"')
                ->where('InventoryInternalID', $inventory)
                ->where('WarehouseInternalID', $warehouse)
                ->where('Type', 'out')
                ->where('t_transformation_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                ->select(DB::raw('sum(SubTotal*CurrencyRate) as SubTotalCurrency'))
                ->pluck('SubTotalCurrency');
        return $result;
    }
    public static function valueInInventory($inventory, $bulan, $tahun) {
        $result = TransformationDetail::join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                ->whereRaw('YEAR(TransformationDate) = "' . $tahun . '" AND MONTH(TransformationDate) = "' . $bulan . '"')
                ->where('InventoryInternalID', $inventory)
                ->where('Type', 'in')
                ->where('t_transformation_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                ->select(DB::raw('sum(SubTotal*CurrencyRate) as SubTotalCurrency'))
                ->pluck('SubTotalCurrency');
        return $result;
    }
    public static function valueOutInventory($inventory, $bulan, $tahun) {
        $result = TransformationDetail::join('t_transformation_header', 't_transformation_header.InternalID', '=', 't_transformation_detail.TransformationInternalID')
                ->whereRaw('YEAR(TransformationDate) = "' . $tahun . '" AND MONTH(TransformationDate) = "' . $bulan . '"')
                ->where('InventoryInternalID', $inventory)
                ->where('Type', 'out')
                ->where('t_transformation_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                ->select(DB::raw('sum(SubTotal*CurrencyRate) as SubTotalCurrency'))
                ->pluck('SubTotalCurrency');
        return $result;
    }

    public static function qtyInventorySuperAdmin($inventory, $company) {
        $result = TransformationDetail::join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.TransformationInternalID')
                ->where('InventoryInternalID', $inventory)
                ->where('t_memoin_header.CompanyInternalID', $company)
                ->groupBy('t_memoin_detail.UomInternalID')
                ->select(DB::raw('sum(Qty) as sumqty, t_memoin_detail.UomInternalID'))
                ->get();
        $tampQty = 0;
        foreach ($result as $data) {
            $invenUom = InventoryUom::where('UomInternalID', $data->UomInternalID)->where('InventoryInternalID', $inventory)->first();
            $tampQty += $data->sumqty * $invenUom->Value;
        }
        return $tampQty;
    }

    public static function valueInventorySuperAdmin($inventory, $company) {
        $result = TransformationDetail::join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.TransformationInternalID')
                ->where('InventoryInternalID', $inventory)
                ->where('t_memoin_header.CompanyInternalID', $company)
                ->select(DB::raw('sum(SubTotal*CurrencyRate) as SubTotalCurrency'))
                ->pluck('SubTotalCurrency');
        return $result;
    }

//    public static function getNextIDTransformation($text) {
//        $query = 'SELECT TransformationID From t_memoin_header Where TransformationID LIKE "' . $text . '%" AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" order by TransformationID desc';
//        $memoInID = DB::select(DB::raw($query));
//
//        if (count($memoInID) <= 0) {
//            $memoInID = '';
//        } else {
//            $memoInID = $memoInID[0]->TransformationID;
//        }
//
//        if ($memoInID == '') {
//            $memoInID = $text . '0001';
//        } else {
//            $textTamp = $memoInID;
//            $memoInID = substr($textTamp, (strlen($textTamp) - 4), 4) + 1;
//            $memoInID = str_pad($memoInID, 4, '0', STR_PAD_LEFT);
//            $memoInID = $text . $memoInID;
//        }
//        return $memoInID;
//    }

    public static function getNextIDTransformation($text) {
        $query = 'SELECT TransformationID From t_transformation_header Where TransformationID LIKE "' . $text . '%" AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" order by TransformationID desc';
        $memoInID = DB::select(DB::raw($query));

        if (count($memoInID) <= 0) {
            $memoInID = '';
        } else {
            $memoInID = $memoInID[0]->TransformationID;
        }

        if ($memoInID == '') {
            $memoInID = $text . '0001';
        } else {
            $textTamp = $memoInID;
            $memoInID = substr($textTamp, (strlen($textTamp) - 4), 4) + 1;
            $memoInID = str_pad($memoInID, 4, '0', STR_PAD_LEFT);
            $memoInID = $text . $memoInID;
        }
        return $memoInID;
    }

    public function memoInDetail() {
        return $this->hasMany('TransformationDetail', 'TransformationInternalID', 'InternalID');
    }

    public function currency() {
        return $this->belongsTo('Currency', 'CurrencyInternalID', 'InternalID');
    }

    public function warehouse() {
        return $this->belongsTo('Warehouse', 'WarehouseInternalID', 'InternalID');
    }

}
