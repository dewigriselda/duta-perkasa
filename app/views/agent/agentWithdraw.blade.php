@extends('template.header-footer')

@section('title')
Company
@stop

@section('css')
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
@stop

@section('nav')

@stop

@section('content')
@if(isset($messages))
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Status withdraw has been updated.
</div>
@endif
@if($messages == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@endif

<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread" role="group">
                <a href="{{route('showUser')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showWithdrawAgent')}}" type="button" class="btn btn-sm btn-pure">Withdraw</a>
            </div>
        </div>
        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Withdraw</h4>
            </div>            
            <div class="tableadd">
                <table id="example" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Agent ID</th>
                            <th>Name</th>
                            <th>Withdraw ID</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Value</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $f = rand(0, 50);
                        foreach (Withdraw::join('m_user', 'm_user.InternalID', '=', 'AgentInternalID')
                                ->orderBy('h_withdraw.Status', 'asc')
                                ->select('*', 'h_withdraw.InternalID as wInternalID', 'h_withdraw.Status as wStatus')
                                ->get() as $data) {
                            $data->dtRecordformat = date("d-m-Y H:i:s", strtotime($data->dtRecord));
                            $data->dtModifformat = date("d-m-Y H:i:s", strtotime($data->dtModified));
                            $arrData = array($data);
                            $tamp = myEscapeStringData($arrData);
                            $tamp = myEncryptJavaScriptText($tamp, $f);
                            $status = 'Completed';
                            if ($data->wStatus == 0) {
                                $status = 'Pending';
                            }
                            ?>
                            <tr>
                                <td>{{$data->UserID}}</td>
                                <td>{{$data->UserName}}</td>
                                <td>{{$data->WithdrawID}}</td>
                                <td>{{date('d-m-Y', strtotime($data->WithdrawDate))}}</td>
                                <td>{{$status}}</td>
                                <td class="text-right">{{number_format($data->WithdrawNominal,'2','.',',')}}</td>
                                <td class="text-center">
                                    <button data-target="#m_withdrawCompleted" data-internal="{{$data->wInternalID}}"  
                                            data-toggle="modal" role="dialog"
                                            data-id="{{$data->WithdrawID}}" 
                                            class="btn btn-pure-xs btn-xs btn-completed" 
                                            @if($data->Status == 1) {{"disabled";}} @endif>
                                            <b> Complete </b>
                                    </button>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div><!---- end div tabwrap---->
        </div><!---- end div tabwrap---->
    </div><!--end primcontent-->
</div><!--end wrapjour--->


@stop

@section('modal')
<div class="modal fade" id="m_withdrawCompleted" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Complete Withdraw</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action">
                        <ul>
                            <input type="hidden" value="" id="idCompleted" name="InternalID">
                            <input type="hidden" value="completedWithdraw" id="jenisCompleted" name="jenis">
                            <p>Are you sure?</p>
                        </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-green">Yes</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
            </div>
            </form>      
        </div>
    </div>  
</div>
@stop
@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript">
$('#example').dataTable({
    columnDefs: [{
            targets: [0],
            orderData: [0, 1]
        }, {
            targets: [1],
            orderData: [1, 0]
        }, {
            targets: [2],
            orderData: [2, 0]
        }]
});
$(".btn-completed").click(function () {
    $('#idCompleted').val($(this).data('internal'));
});
</script>
@stop