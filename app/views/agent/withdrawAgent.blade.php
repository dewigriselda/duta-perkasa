@extends('template.header-footer-agent')

@section('title')
Company
@stop

@section('css')
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
@stop

@section('nav')

@stop

@section('content')
@if(isset($messages))
@if($messages == 'gagalInsert' || $messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesInsertWithdraw')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New withdraw has been inserted.
</div>
@endif
@if($messages == 'gagalWithdraw')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Withdraw must not greater than your saldo.
</div>
@endif
@endif

<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread" role="group">
                <a href="{{route('showHomeAgent')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showWithdrawAgent')}}" type="button" class="btn btn-sm btn-pure">Withdraw</a>
            </div>


            <button type="button" class="btn btn-sm btn-green btn-withdraw" 
                    data-target="#m_withdraw" data-toggle="modal" data-id="{{Auth::user()->InternalID}}" role="dialog" >Withdraw</button>
        </div>
        <div class="btnnest pull-right" style="margin-top: 10px;">
            <?php
            $harga = 0;
            $totalKomisi = 0;
            $totalWithdraw = 0;
            $total = 0;
            foreach (Payment::join("m_company", "m_company.InternalID", "=", "CompanyInternalID")->where("m_company.AgentInternalID", Auth::user()->InternalID)->get() as $dataWithdraw) {
                if ($dataWithdraw->PackageInternalID == 1) {
                    $harga = 350000;
                } else {
                    $harga = 1000000;
                }
                $totalKomisi += $harga * ($dataWithdraw->AgentCommission / 100);
            }
            foreach (Withdraw::where("AgentInternalID", Auth::user()->InternalID)->get() as $withdraw) {
                $totalWithdraw += $withdraw->WithdrawNominal;
            }
            $total = $totalKomisi - $totalWithdraw;
            ?>
            <span >Saldo : {{number_format($total,'2', '.', ',')}}</span>
        </div>
        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Withdraw</h4>
            </div>            
            <div class="tableadd">
                <table id="example" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Withdraw ID</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Value</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $f = rand(0, 50);
                        foreach (Withdraw::where("AgentInternalID", Auth::user()->InternalID)->orderBy('h_withdraw.Status', 'asc')->get() as $data) {
                            $data->dtRecordformat = date("d-m-Y H:i:s", strtotime($data->dtRecord));
                            $data->dtModifformat = date("d-m-Y H:i:s", strtotime($data->dtModified));
                            $arrData = array($data);
                            $tamp = myEscapeStringData($arrData);
                            $tamp = myEncryptJavaScriptText($tamp, $f);
                            $status = 'Completed';
                            if ($data->Status == 0) {
                                $status = 'Pending';
                            }
                            ?>
                            <tr>
                                <td>{{$data->WithdrawID}}</td>
                                <td>{{date('d-m-Y', strtotime($data->WithdrawDate))}}</td>
                                <td>{{$status}}</td>
                                <td class="text-right">{{number_format($data->WithdrawNominal,'2','.',',')}}</td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div><!---- end div tabwrap---->
        </div><!---- end div tabwrap---->
    </div><!--end primcontent-->
</div><!--end wrapjour--->


@stop

@section('modal')
<div class="modal fade bs-example-modal" id="m_withdraw" role="dialog">
    <div class="modal-dialog modal-mid ">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Withdraw</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="form-insert-withdraw">
                        <ul>
                            <input type="hidden" value="submitWithdraw" id="jenisUpdate" name="jenis">
                            <input type="hidden" value="{{Auth::user()->InternalID}}" name="AgentInternalID">
                            <li>
                                <label for="Saldo">Saldo : </label>
                                <label >Rp. </label>
                                <label id="saldoSisa"></label>
                            </li>
                            <li>
                                <label for="withdraw">Withdraw</label>
                            </li>
                            <li>
                                <input type="text" id="withdrawNominal" class="numaja price" name="WithdrawNominal" value="50,000" maxlength="1000" >
                            </li>
                            <li>
                                <small>Note : Minimum withdraw 50.000</small>
                            </li>
                        </ul>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green" >Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </div>
            </form>
        </div>
    </div>  
</div>
@stop
@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript">
var getSaldoWithdraw = "<?php echo Route("getSaldoWithdraw") ?>";
</script>
<script type="text/javascript" src="{{Asset('js/agent-js-commission/withdraw-agent.js')}}"></script>
@stop