@extends('template.header-footer')

@section('title')
COA Master
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}" />
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}" />
<link rel="stylesheet" href="{{Asset('css/jquery-ui.css')}}" />
@stop

@section('nav')

@stop

@section('content')

@if(myCheckIsEmpty('Coa1'))
<div class="alert alert-warning alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one COA Level 1 to insert COA Master.
</div>
@endif
@if(isset($messages))
@if($messages == 'gagalInsert' || $messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New account has been inserted.
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Account has been updated.
</div>
@endif
@if($messages == 'suksesDelete')
<div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Account has been deleted.
</div>
@endif
@if($messages == 'gagalDelete')
<div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Account has been registered in other tables.
</div>
@endif
@if($messages == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@endif

<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread hidden-xs" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showCoa')}}" type="button" class="btn btn-sm btn-pure">COA Master</a>
            </div>
            @if(checkModul('O01'))
            <a target="_blank" href="{{Route('exportCoa')}}">
                <button type="button" class="btn btn-sm btn-green btn-export" id="btn-export">Export Excel</button>    
            </a>
            @endif
            <button <?php if (myCheckIsEmpty('Coa1')) echo 'disabled' ?> type="button" class="btn btn-green btn-insert" id="btn-insert" data-target="#m_coa" data-toggle="modal" role="dialog">
                <span class="glyphicon glyphicon-plus"></span> New </button>
        </div>

        <div class="bread-xs visible-xs hidden-sm hidden-md hidden-lg">
            <p class="text-center"><a href="{{route('showDashboard')}}">{{Config::get('companyHeader.header_company');}}</a> / <a>{{ucfirst($toogle)}}</a> / <a href="{{route('showCoa')}}">COA Master</a></p>
        </div>

        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">COA Master</h4>
            </div>

            <div class="tableadd">

                <table id="example" class="display table-rwd table-coa" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Account ID</th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Stock</th>
                            <th>Initial Balance</th>
                            <th>Record</th>
                            <th>Modified</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $f = rand(0, 50);
                        foreach (Coa::showCoa() as $data) {
                            $data->dtRecordformat = date("d-m-Y H:i:s", strtotime($data->dtRecord));
                            $data->dtModifformat = date("d-m-Y H:i:s", strtotime($data->dtModified));
                            $data->Remark = str_replace("\r\n", " ", $data->Remark);
                            $arrData = array($data);
                            $tamp = myEscapeStringData($arrData);
                            $tamp = myEncryptJavaScriptText($tamp, $f);
                            ?>
                            <tr>
                                <td>{{Coa::formatCoa($data->ACC1ID,$data->ACC2ID,$data->ACC3ID,$data->ACC4ID,$data->ACC5ID,$data->ACC6ID,'0')}}</td>
                                <td>{{$data->COAName}}</td>
                                @if($data->Flag == "0")
                                <td>Balance</td>
                                @else
                                <td>Profit and Loss</td>
                                @endif
                                <td class="right">{{$data->Persediaan}}</td>
                                <td class="right">{{number_format($data->InitialBalance,2,'.',',')}}</td>
                                <td>{{$data->UserRecord." ".date( "d-m-Y H:i:s", strtotime($data->dtRecord))}}</td>
                                @if($data->UserModified == "0")
                                <td>-</td>
                                @else
                                <td>{{$data->UserModified." ".date( "d-m-Y H:i:s", strtotime($data->dtModified))}}</td>
                                @endif
                                <td class="text-center"><button id="" data-target="#m_coaUpdate" data-all='{{$tamp}}'
                                                                data-level="0" data-toggle="modal" role="dialog"
                                                                class="btn btn-pure-xs btn-xs btn-edit">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </button>
                                    <button data-target="#m_coaDelete" data-internal="{{$data->InternalID}}"  data-toggle="modal" role="dialog"
                                            data-level="0" data-accountNumber="{{$data->m1id.".".$data->m2id.".".$data->m3id.".".$data->m4id.".".$data->m5id.".".$data->m6id}}"
                                            class="btn btn-pure-xs btn-xs btn-delete" data-name='{{$data->COAName}}' >
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>
                                 <button data-target="#m_coaReport" data-internal="{{$data->InternalID}}"  data-toggle="modal" role="dialog"
                                            data-level="0" data-accountNumber="{{$data->m1id.".".$data->m2id.".".$data->m3id.".".$data->m4id.".".$data->m5id.".".$data->m6id}}"
                                            class="btn btn-pure-xs btn-xs btn-file" data-name='{{$data->COAName}}' >
                                        <span class="glyphicon glyphicon-file"></span>
                                    </button>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>

            </div><!---end tableadd--->
        </div><!---- end div tabwrap---->

    </div><!---end primcontent--->
</div><!---end wrapjour--->

@stop

@section('modal')
<div class="modal fade bs-example-modal-lg " id="m_coa" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="" method="post" id="form-insert" class="action formCoaInsert">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Insert COA Master</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form coawd">
                        <div class="row">
                            <div class="col-sm-6">
                                <input type='hidden' name='jenis' value='insertCoa'>

                                <div class="margbot10">
                                    <label for="npwp">COA Detail *</label> 
                                </div>

                                <div class="margbot10 chosenmodlg">
                                    <select class="chosen-select choosen-modal" id="coa1" name="coa1">
                                        @foreach(Coa1::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('ACC1ID')->get() as $coa1)
                                        <option value="{{$coa1->ACC1ID.'---;---'.$coa1->ACC1Name.'---;---'.$coa1->InternalID}}">
                                            {{$coa1->ACC1ID." "}} {{" ".$coa1->ACC1Name}}
                                        </option>
                                        @endforeach
                                    </select><br>
                                    <select class="chosen-select choosen-modal" id="coa2" name="coa2">
                                        @foreach(Coa2::where('CompanyInternalID', Auth::user()->Company->InternalID)->orWhere('CompanyInternalID', NULL)->orderBy('ACC2ID')->get() as $coa2)
                                        @if($coa2->InternalID == 0)
                                        <option value="{{'0---;---Empty---;---0'}}" id="coa2Empty">
                                            Empty coa level 2
                                        </option>
                                        @else
                                        <option style='display: none'
                                                value="{{$coa2->ACC2ID.'---;---'.$coa2->ACC2Name.'---;---'.$coa2->InternalID}}" id="{{$coa2->ACC2ID}}">
                                            {{$coa2->ACC2ID." "}} {{" ".$coa2->ACC2Name}}
                                        </option>
                                        @endif
                                        @endforeach
                                    </select><br>
                                    <select class="chosen-select choosen-modal" id="coa3" name="coa3">
                                        @foreach(Coa3::where('CompanyInternalID', Auth::user()->Company->InternalID)->orWhere('CompanyInternalID', NULL)->orderBy('ACC3ID')->get() as $coa3)
                                        @if($coa3->InternalID == '0')
                                        <option value="{{'0---;---Empty---;---0'}}"  id="coa3Empty">
                                            Empty coa level 3
                                        </option>
                                        @else
                                        <option  style='display: none'
                                                 value="{{$coa3->ACC3ID.'---;---'.$coa3->ACC3Name.'---;---'.$coa3->InternalID}}" id="{{$coa3->ACC3ID}}">
                                            {{$coa3->ACC3ID." "}} {{" ".$coa3->ACC3Name}}
                                        </option>
                                        @endif
                                        @endforeach
                                    </select><br>
                                    <select class="chosen-select choosen-modal" id="coa4" name="coa4">
                                        @foreach(Coa4::where('CompanyInternalID', Auth::user()->Company->InternalID)->orWhere('CompanyInternalID', NULL)->orderBy('ACC4ID')->get() as $coa4)
                                        @if($coa4->InternalID == '0')
                                        <option value="{{'0---;---Empty---;---0'}}"  id="coa4Empty">
                                            Empty coa level 4
                                        </option>
                                        @else
                                        <option  style='display: none'
                                                 value="{{$coa4->ACC4ID.'---;---'.$coa4->ACC4Name.'---;---'.$coa4->InternalID}}" id="{{$coa4->ACC4ID}}">
                                            {{$coa4->ACC4ID." "}} {{" ".$coa4->ACC4Name}}
                                        </option>
                                        @endif
                                        @endforeach
                                    </select><br>
                                    <select class="chosen-select choosen-modal" id="coa5" name="coa5">
                                        @foreach(Coa5::where('CompanyInternalID', Auth::user()->Company->InternalID)->orWhere('CompanyInternalID', NULL)->orderBy('ACC5ID')->get() as $coa5)
                                        @if($coa5->InternalID == '0')
                                        <option value="{{'0---;---Empty---;---0'}}"  id="coa5Empty">
                                            Empty coa level 5
                                        </option>
                                        @else
                                        <option value="{{$coa5->ACC5ID.'---;---'.$coa5->ACC5Name.'---;---'.$coa5->InternalID}}">
                                            {{$coa5->ACC5ID." "}} {{" ".$coa5->ACC5Name}}
                                        </option>
                                        @endif
                                        @endforeach
                                    </select><br>
                                    <select class="chosen-select choosen-modal" id="coa6" name="coa6">
                                        @foreach(Coa6::where('CompanyInternalID', Auth::user()->Company->InternalID)->orWhere('CompanyInternalID', NULL)->orderBy('ACC6ID')->get() as $coa6)
                                        @if($coa6->InternalID == '0')
                                        <option value="{{'0---;---Empty---;---0'}}"  id="coa6Empty">
                                            Empty coa level 6
                                        </option>
                                        @else
                                        <option value="{{$coa6->ACC6ID.'---;---'.$coa6->ACC6Name.'---;---'.$coa6->InternalID}}">
                                            {{$coa6->ACC6ID." "}} {{" ".$coa6->ACC6Name}}
                                        </option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="Type">Type</label> *
                                </div>
                                <div class="margbot10">
                                    <div class="radio-inline">
                                        <input type="radio" class="radio-tipe " id="tipeNeraca" name="Type" value="0" checked="checked"><label class="margr10" for="tipeNeraca">Balance</label>
                                    </div>
                                    <div class="radio-inline nomargl">
                                        <input type="radio" class="radio-tipe" id="tipeLabarugi" name="Type" value="1"><label for="tipeLabarugi">Profit and Loss</label>
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="CoaName">Name *</label> 
                                </div>
                                <div class="margbot10">
                                    <input class="noSpecialCharacter" type="text" name="CoaName" id="name" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="Header1">Header 1 *</label> 
                                </div>
                                <div class="margbot10">
                                    <input type="text" class="header1" name="Header1" id="header1" maxlength="500" data-validation="required">
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="margbot10">
                                    <label for="Header2">Header 2 *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" class="header2" name="Header2" id="header2" maxlength="500" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="Header3">Header 3 *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" class="header3" name="Header3" id="header3" maxlength="500" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="HeaderCashFlow1">Header Cash Flow 1 *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" class="headerCashFlow1" name="HeaderCashFlow1" id="hcf1" maxlength="500" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="HeaderCashFlow2">Header Cash Flow 2 *</label> 
                                </div>
                                <div class="margbot10">
                                    <input type="text" class="headerCashFlow2" name="HeaderCashFlow2" id="hcf2" maxlength="500" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="HeaderCashFlow3">Header Cash Flow 3 *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" class="headerCashFlow3" name="HeaderCashFlow3" id="hcf3" maxlength="500" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="persediaan">Stock *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="checkbox" name="Stock" id="persediaan" value="1"> <label for="persediaan">Chart of Account Stock.</label>
                                </div>
                                <div class="margbot10">
                                    <label for="balance">Initial Balance *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Balance" id="balance" class='numajaDesimal' data-validation='required'>
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;"  name="remark" id="remark" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <p> * Required</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>


<div class="modal fade bs-example-modal-lg" id="m_coaUpdate" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="" method="post" class="action formCoaUpdate" id="form-update">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update COA</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form coawd">
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="margbot10">
                                    <input type="hidden" value="" id="idUpdate" name="InternalID">
                                    <input type="hidden" value="updateCoa" id="jenisUpdate" name="jenis">
                                </div>
                                <div class="margbot10">
                                    <label for="ACCName">Name *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="noSpecialCharacter" type="text" name="CoaName" id="nameUpdate" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="Header1">Header 1 *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" class="header1update" name="Header1" id="header1Update" maxlength="500" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="Header2">Header 2 *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" class="header2update" name="Header2" id="header2Update" maxlength="500" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="Header3">Header 3 *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" class="header3update" name="Header3" id="header3Update" maxlength="500" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="Headercashflow1">Header Cash Flow 1 *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" class="headerCashFlow1update" name="HeaderCashFlow1" id="hcf1Update" maxlength="500" data-validation="required">
                                </div>
                            </div>
                            
                            <div class="col-sm-6">
                                <div class="margbot10">
                                    <label for="Headercashflow2">Header Cash Flow 2 *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" class="headerCashFlow2update" name="HeaderCashFlow2" id="hcf2Update" maxlength="500" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="Headercashflow3">Header Cash Flow 3 *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" class="headerCashFlow3update" name="HeaderCashFlow3" id="hcf3Update" maxlength="500" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="persediaan">Stock *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="checkbox" name="Stock" id="persediaanUpdate" value="1"> <label for="persediaanUpdate">Chart of Account Stock.</label>
                                </div>
                                <div class="margbot10">
                                    <label for="balance">Initial Balance *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Balance" id="balanceUpdate" class='numajaDesimal' data-validation='required'>
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;"  name="remark" id="remarkUpdate" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <small>Created by <span id="createdDetail"></span></small><br>
                                    <small>Modified by <span id="modifiedDetail"></span></small>
                                </div>
                                <div class="required">
                                    * Required
                                </div>
                            </div>
                        </div>
                        
                    </div>           
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Update</button>
                    <button type="reset" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade nopaddr" id="m_coaDelete" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <form action="" method="post" class="action">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Delete COA</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">

                            <input type="hidden" value="" id="idDelete" name="InternalID">
                            <input type="hidden" value="" id="AccID" name="AccID">
                            <input type="hidden" value="deleteCoa" id="jenisDelete" name="jenis">
                            <p>Are you sure want to delete <span id="deleteName"></span>?</p>
                            
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Yes</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade" id="m_coaReport" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Summary Report</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="" target="_blank">
                        <ul>
                            <input type="hidden" value="" id="idReport" name="InternalID">
                            <input type='hidden' name='jenis' id="jenisReport" value='coaReport'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateReport" data-validation="required">
                            </li>
                            <div class="margbot10">
                                <label for="downloadType">Type *</label>
                            </div>
                            <div class="margbot10">
                                <select class="input-theme" name="downloadType">
                                    <option value="view">View</option>
                                    <option value="pdf">PDF</option>
                                    <option value="excel">Excel</option>
                                </select>
                            </div>
                        </ul>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>      
        </div>
    </div>  
</div>
@stop
<?php
$c = myEncryptJavaScript(Coa::coaAccID(), $f);
?>
@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript">
var a = '<?php echo $c; ?>';
var b = <?php echo $f; ?>;
var countCOA = '<?php echo myCheckIsEmpty('Coa1'); ?>';
        var availableTags = [<?php foreach (Coa::select('Header1')->where('CompanyInternalID', Auth::user()->Company->InternalID)->distinct()->get() as $auto) { ?>"<?php echo $auto->Header1 ?>",<?php } ?>];
        var availableTags2 = [<?php foreach (Coa::select('Header2')->where('CompanyInternalID', Auth::user()->Company->InternalID)->distinct()->get() as $auto) { ?>"<?php echo $auto->Header2 ?>",<?php } ?>];
        var availableTags3 = [<?php foreach (Coa::select('Header3')->where('CompanyInternalID', Auth::user()->Company->InternalID)->distinct()->get() as $auto) { ?>"<?php echo $auto->Header3 ?>",<?php } ?>];
        var availableTags4 = [<?php foreach (Coa::select('HeaderCashFlow1')->where('CompanyInternalID', Auth::user()->Company->InternalID)->distinct()->get() as $auto) { ?>"<?php echo $auto->HeaderCashFlow1 ?>",<?php } ?>];
        var availableTags5 = [<?php foreach (Coa::select('HeaderCashFlow2')->where('CompanyInternalID', Auth::user()->Company->InternalID)->distinct()->get() as $auto) { ?>"<?php echo $auto->HeaderCashFlow2 ?>",<?php } ?>];
        var availableTags6 = [<?php foreach (Coa::select('HeaderCashFlow3')->where('CompanyInternalID', Auth::user()->Company->InternalID)->distinct()->get() as $auto) { ?>"<?php echo $auto->HeaderCashFlow3 ?>",<?php } ?>];</script>
<script type="text/javascript" src="{{Asset('js/entry-js-coa/coa.js')}}"></script>
@stop