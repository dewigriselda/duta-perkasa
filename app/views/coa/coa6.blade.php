@extends('template.header-footer')

@section('title')
Coa level 6
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('./css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
@stop

@section('nav')

@stop

@section('content')


@if(isset($messages))
@if($messages == 'gagalInsert' || $messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New account has been inserted.
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Account has been updated.
</div>
@endif
@if($messages == 'suksesDelete')
<div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Account has been deleted.
</div>
@endif
@if($messages == 'gagalDelete')
<div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Account has been registered in other tables.
</div>
@endif
@if($messages == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@endif


<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread hidden-xs" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showCoa6')}}" type="button" class="btn btn-sm btn-pure">COA Level 6</a>
            </div>
            @if(checkModul('O01'))
            <a target="_blank" href="{{Route('exportCoa6')}}">
                <button type="button" class="btn btn-sm btn-green btn-export" id="btn-export">Export Excel</button>    
            </a>
            <button type="button" class="btn btn-green btn-export" data-target="#importCoa6" data-toggle="modal" role="dialog">
                Import Excel</button>
            @endif
            <button type="button" class="btn btn-green btn-insert" data-target="#m_coa" data-toggle="modal" role="dialog">
                <span class="glyphicon glyphicon-plus"></span> New</button>
        </div>

        <div class="bread-xs visible-xs hidden-sm hidden-md hidden-lg">
            <p class="text-center"><a href="{{route('showDashboard')}}">{{Config::get('companyHeader.header_company');}}</a> / <a>{{ucfirst($toogle)}}</a> / <a href="{{route('showCoa6')}}">COA Level 6</a></p>
        </div>

        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">COA Level 6</h4>
            </div>            
            <div class="tableadd">
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs no-border" role="tablist">
                        <li role="presentation" class="customer active"><a href="#customer" aria-controls="customer" role="tab" data-toggle="tab">Customer</a></li>
                        <li role="presentation" class="supplier"><a href="#supplier" aria-controls="profile" role="tab" data-toggle="tab">Supplier</a></li>  
                        <li role="presentation" class="industry"><a href="#industry" aria-controls="profile" role="tab" data-toggle="tab">Industry</a></li>  
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content tab-content-pad">
                        <div role="tabpanel" class="tab-pane active" id="customer">
                            <table id="exampleCustomer" class="display table-rwd table-coa6-customer" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Customer ID</th>
                                        <th>Customer Type</th>
                                        <th>Name</th>
                                        <th>Address</th>
                                        <th>City</th>
                                        <th>Postal Code</th>
                                        <th>TaxID</th>
                                        <th>Phone</th>
                                        <th>Credit Limit</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="supplier">
                            <table id="exampleSupplier" class="display table-coa6-supplier" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Supplier ID</th>
                                        <th>Supplier Type</th>
                                        <th>Name</th>
                                        <th>Address</th>
                                        <th>City</th>
                                        <th>Postal Code</th>
                                        <th>TaxID</th>
                                        <th>Phone</th>
                                        <th>Credit Limit</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>

                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="industry">
                            <table id="exampleIndustry" class="display table-coa6-industry" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Industry ID</th>
                                        <th>Name</th>
                                        <th>Address</th>
                                        <th>City</th>
                                        <th>Postal Code</th>
                                        <th>TaxID</th>
                                        <th>Phone</th>
                                        <th>Credit Limit</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>

                            </table>
                        </div>
                    </div><!--end tab-content-->
                </div><!--end tabpanel-->
            </div><!--end tableadd-->
        </div><!---- end div tabwrap---->
    </div><!--end primcontent-->
</div><!--end wrapjour--->


@stop

@section('modal')
<div class="modal fade bs-example-modal-lg" id="m_coa" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="" method="post" class="action" id="form-insert">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Insert coa level 6</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form coawd">

                        <div class="row">
                            <div class="col-md-5">
                                <div class="margbot10">
                                    <input type='hidden' name='jenis' value='insertCoa'>
                                </div>
                                <div class="margbot10">
                                    <label for="AccID">Type *</label>
                                </div>
                                <div class="margbot10">
                                    <div class="radio-inline margr10">
                                        <input type="radio" class="radio-tipe" id="tipeCustomer" name="Type" value="c" checked="checked"><label for="tipeCustomer">Customer</label>
                                    </div>
                                    <div class="radio-inline nomargl">
                                        <input type="radio" class="radio-tipe" id="tipeSupplier" name="Type" value="s"><label for="tipeSupplier">Supplier</label>
                                    </div>
                                    <div class="radio-inline nomargl">
                                        <input type="radio" class="radio-tipe" id="tipeIndustry" name="Type" value="i"><label for="tipeIndustry">Industry</label>
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="AccID">ID *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="noSpecialCharacter" type="text" name="AccID" id="accID" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="ACCName">Name *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="noSpecialCharacter" type="text" name="AccName" id="name" maxlength="200" data-validation="required">
                                </div>
                                <div class="typecustomer">
                                    <div class="margbot10">
                                        <label for="customerType">Customer Type</label>
                                    </div>
                                    <div class="margbot10">

                                        <div class="radio-inline margr10">
                                            <input type="radio" class="radio-tipe" id="tipeSeller" name="userType" value="sr" checked="checked"><label for="seller">Seller</label>
                                        </div>
                                        <div class="radio-inline nomargl">
                                            <input type="radio" class="radio-tipe" id="tipeEnduser" name="userType" value="er"><label for="endUser">End User</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="typesupplier">
                                    <div class="margbot10">
                                        <label for="supplierType">Supplier Type</label>
                                    </div>
                                    <div class="margbot10">

                                        <div class="radio-inline margr10">
                                            <input type="radio" class="radio-tipe" id="tipeImport" name="userType2" value="im" checked="checked"><label for="import">Import</label>
                                        </div>
                                        <div class="radio-inline nomargl">
                                            <input type="radio" class="radio-tipe" id="tipeLocal" name="userType2" value="lo"><label for="local">Local</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="taxID">TaxID</label>
                                </div>
                                <div class="margbot10">
                                    <div id="npwpin">
                                        <input type="hidden" name="taxID" value="" id="taxID">
                                        <input class="persen11 autoTab numaja" type="text" name="taxID1" id="taxID1" maxlength="2"
                                               data-validation="length" data-validation-length="min2" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID2" id="taxID2" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID3" id="taxID3" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen8 autoTab numaja" type="text" name="taxID4" id="taxID4" maxlength="1"
                                               data-validation="length" data-validation-length="min1" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> - 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID5" id="taxID5" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID6" id="taxID6" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information.">
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="address">Address *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;" name="Address" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <label for="Block">Block</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Block" id="block" maxlength="200">
                                </div>
                                <div class="margbot10">
                                    <label for="AddressNumber">Address Number</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="AddressNumber" id="addressNumber" maxlength="200">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="margbot10">
                                    <label for="rt">RT</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="RT" id="rt" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="rw">RW</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="RW" id="rw" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="district">District</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="District" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="subdistrict">Sub District</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Subdistrict" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="city">City</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="City" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="province">Province</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Province" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="postalcode">Postal Code</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="PostalCode" maxlength="200" >
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="margbot10">
                                    <label for="origin">Email</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Email" maxlength="200">
                                </div>
                                <div class="margbot10">
                                    <label for="phone">Phone *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Phone" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="fax">Fax *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Fax" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="creditlimit">Credit limit</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" class="numajaDesimal" id="uangCredit" name="CreditLimit" maxlength="">
                                </div>
<!--                                <div class="margbot10">
                                    <label for="contactperscon">Contact person</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="ContactPerson" maxlength="200">
                                </div>-->
                                <div class="custmanager">
                                    <div class="margbot10">
                                        <label for="remark">Customer Manager *</label>
                                    </div>
                                    <div class="margbot10">
                                        <select name="customerManager" id="customerManager" class="chosen-select choosen-modal">
                                            @foreach(SalesMan::where('Status',1)->get() as $salesman)
                                            <option value="{{$salesman->InternalID}}">{{$salesman->SalesManName}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="margbot10">
                                        <label for="remark">Sales Assistant</label>
                                    </div>
                                    <div class="margbot10">
                                        <select name="salesAssistant" id="salesAssistant" class="chosen-select choosen-modal">
                                            @foreach(SalesMan::where('Status',1)->get() as $salesman)
                                            <option value="{{$salesman->InternalID}}">{{$salesman->SalesManName}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;"  name="remark" id="" maxlength="1000"></textarea>
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>


<div class="modal fade bs-example-modal-lg" id="m_coaUpdate" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="" method="post" class="action" id="form-update">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update coa level 6</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form coawd">

                        <div class="row">
                            <div class="col-md-5">
                                <div class="margbot10">
                                    <input type="hidden" value="" id="idUpdate" name="InternalID">
                                    <input type="hidden" value="updateCoa" id="jenisUpdate" name="jenis">
                                </div>
                                <div class="margbot10">
                                    <label for="ACCName">Name *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="noSpecialCharacter" type="text" name="AccName" id="nameUpdate" maxlength="200" data-validation="required">
                                </div>
                                <div class="typecustomer">
                                    <div class="margbot10">
                                        <label for="customerType">Customer Type</label>
                                    </div>
                                    <div class="margbot10">
                                        <input type="hidden" name="typeCust" id="typeCust">
                                        <div class="radio-inline margr10">
                                            <input type="radio" class="radio-tipe" id="updatetipeSeller" name="userType" value="sr" checked="checked"><label for="seller">Seller</label>
                                        </div>
                                        <div class="radio-inline nomargl">
                                            <input type="radio" class="radio-tipe" id="updatetipeEnduser" name="userType" value="er"><label for="endUser">End User</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="typesupplier">
                                    <div class="margbot10">
                                        <label for="supplierType">Supplier Type</label>
                                    </div>
                                    <div class="margbot10">
                                        <input type="hidden" name="typeSup" id="typeSup">
                                        <div class="radio-inline margr10">
                                            <input type="radio" class="radio-tipe" id="updatetipeImport" name="userType2" value="im" checked="checked"><label for="import">Import</label>
                                        </div>
                                        <div class="radio-inline nomargl">
                                            <input type="radio" class="radio-tipe" id="updatetipeLocal" name="userType2" value="lo"><label for="local">Local</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="taxID">TaxID</label>
                                </div>
                                <div class="margbot10">
                                    <div id="npwpin">
                                        <input type="hidden" name="taxID" value="" id="taxIDupdate">
                                        <input class="persen11 autoTab numaja" type="text" name="taxID1" id="taxID1update" maxlength="2"
                                               data-validation="length" data-validation-length="min2" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID2" id="taxID2update" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID3" id="taxID3update" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen8 autoTab numaja" type="text" name="taxID4" id="taxID4update" maxlength="1"
                                               data-validation="length" data-validation-length="min1" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> - 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID5" id="taxID5update" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID6" id="taxID6update" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information.">
                                    </div>
                                </div>
<!--                                <div class="margbot10">
                                    <label for="origin">Origin</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Origin" id="originUpdate" maxlength="200">
                                </div>-->
                                <div class="margbot10">
                                    <label for="address">Address *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;"  name="Address" id="addressUpdate" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <label for="block">Block</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" id="blockUpdate" name="Block" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="addressnumber">Address Number</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" id="addressNumberUpdate" name="AddressNumber" maxlength="200" >
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="margbot10">
                                    <label for="rt">RT</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" id="rtUpdate" name="RT" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="rw">RW</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" id="rwUpdate" name="RW" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="district">District</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" id="districtUpdate" name="District" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="subdistrict">Subdistrict</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" id="subdistrictUpdate" name="Subdistrict" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="city">City</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" id="cityUpdate" name="City" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="province">Province</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" id="provinceUpdate" name="Province" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="postalCode">Postal Code</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" id="postalCodeUpdate" name="PostalCode" maxlength="200" >
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="margbot10">
                                    <label for="phone">Email</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Email" id="emailUpdate" maxlength="200">
                                </div>
                                <div class="margbot10">
                                    <label for="phone">Phone *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Phone" id="phoneUpdate" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="fax">Fax *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Fax" id="faxUpdate" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="creditlimit">Credit limit</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" class="numajaDesimal" name="CreditLimit" id="creditLimitUpdate" maxlength="200">
                                </div>
<!--                                <div class="margbot10">
                                    <label for="contactperscon">Contact person</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="ContactPerson" id="contactPersonUpdate" maxlength="200">
                                </div>-->
                                <div class="custmanager">
                                    <div class="margbot10">
                                        <label for="remark">Customer Manager *</label>
                                    </div>
                                    <div class="margbot10">
                                        <select name="customerManager" id="customerManagerUpdate" class="chosen-select choosen-modal">
                                            @foreach(SalesMan::where('Status',1)->get() as $salesman)
                                            <option value="{{$salesman->InternalID}}">{{$salesman->SalesManName}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="margbot10">
                                        <label for="remark">Sales Assistant</label>
                                    </div>
                                    <div class="margbot10">
                                        <select name="salesAssistant" id="salesAssistantUpdate" class="chosen-select choosen-modal">
                                            @foreach(SalesMan::where('Status',1)->get() as $salesman)
                                            <option value="{{$salesman->InternalID}}">{{$salesman->SalesManName}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;"  name="remark" id="remarkUpdate" maxlength="1000"></textarea>
                                </div>
                                <div class="margbot10">
                                    <small>Created by <span id="createdDetail"></span></small><br>
                                    <small>Modified by <span id="modifiedDetail"></span></small>
                                </div>
                                <div class="required margbot10">
                                    * Required
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Update</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade bs-example-modal-lg" id="m_coaDelete" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Delete coa level 6</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action">
                        <ul>
                            <input type="hidden" value="" id="idDelete" name="InternalID">
                            <input type="hidden" value="" id="AccID" name="AccID">
                            <input type="hidden" value="deleteCoa" id="jenisDelete" name="jenis">
                            <p>Are you sure want to delete <span id="deleteName"></span>?</p>
                        </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-green">Yes</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
            </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade" id="importCoa6" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Import COA Level 6</h4>
            </div>
            <form action="" method="post" enctype="multipart/form-data" class="action" id="form-so">
                {{'';$hitung = 0;}}
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="importCOA6" id="jenisCoa" name="jenis">
                            <li>
                                <label for="import">IMPORT COA Level 6</label> *
                            </li>
                            <input class="input-theme margbot10" type="file" id="importcoa" name="importcoa">
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btn-add-so" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-coa/coa6.js')}}"></script>
<script type="text/javascript">
var COA6CustomerDataBackup = '<?php echo Route('CustomerDataBackup') ?>';
var COA6SupplierDataBackup = '<?php echo Route('SupplierDataBackup') ?>';
var COA6IndustryDataBackup = '<?php echo Route('IndustryDataBackup') ?>';
$(document).ready(function () {
    $(".chosen-select").chosen({disable_search_threshold: 10});
    $(".typecustomer").show();
    $(".typesupplier").hide();
    $(".custmanager").show();
    $(".btn-insert").click(function () {
        var idAkhir = "<?php echo Coa6::maxID('C') ?>";
        $('#accID').val("C" + idAkhir);
        $('#tipeSupplier').removeAttr('checked');
        $('#tipeIndustry').removeAttr('checked');
        $('#tipeCustomer').attr('checked', 'checked');
        $(".typecustomer").show();
        $(".typesupplier").hide();
        $(".custmanager").show();
    });
    $("#tipeCustomer,#tipeSupplier,#tipeIndustry").change(function () {
        var idAkhirCustomer = "<?php echo Coa6::maxID('C') ?>";
        var idAkhirSupplier = "<?php echo Coa6::maxID('S') ?>";
        var idAkhirIndustry = "<?php echo Coa6::maxID('I') ?>";
        autoAccID(idAkhirCustomer, idAkhirSupplier, idAkhirIndustry)
    });

    $(".customer").click(function () {
        $(".typecustomer").show();
        $(".typesupplier").hide();
        $(".custmanager").show();
    });
    $(".supplier").click(function () {
        $(".typecustomer").hide();
        $(".typesupplier").show();
        $(".custmanager").hide();
    });
    $(".industry").click(function () {
        $(".typecustomer").hide();
        $(".typesupplier").hide();
        $(".custmanager").hide();
    });
});

function autoAccID(customer, supplier, industry) {
//customer dipilih
    if ($('#tipeCustomer').is(':checked') == true) {
        $('#tipeSupplier').removeAttr('checked');
        $('#tipeIndustry').removeAttr('checked');
        $('#tipeCustomer').attr('checked', 'checked');
        var idAkhir = customer;
        $('#accID').val("C" + idAkhir);
        $(".typecustomer").show();
        $(".typesupplier").hide();
        $(".custmanager").show();
    }
    //supplier dipilih
    else if ($('#tipeSupplier').is(':checked') == true) {
        $('#tipeCustomer').removeAttr('checked');
        $('#tipeIndustry').removeAttr('checked');
        $('#tipeSupplier').attr('checked', 'checked');
        var idAkhir = supplier;
        $('#accID').val("S" + idAkhir);
        $(".typecustomer").hide();
        $(".typesupplier").show();
        $(".custmanager").hide();
    }
    //industry dipilih
    else {
        $('#tipeCustomer').removeAttr('checked');
        $('#tipeSupplier').removeAttr('checked');
        $('#tipeIndustry').attr('checked', 'checked');
        var idAkhir = industry;
        $('#accID').val("I" + idAkhir);
        $(".typecustomer").hide();
        $(".typesupplier").hide();
        $(".custmanager").hide();
    }
}

</script>


@stop