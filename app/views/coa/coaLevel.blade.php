@extends('template.header-footer')

@section('title')
COA Level 1 - 4
@stop

@section('css')
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
@stop

@section('nav')

@stop
@section('content')
@if(isset($messages))
@if($messages == 'gagalInsert' || $messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New account has been inserted.
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Account has been updated.
</div>
@endif
@if($messages == 'suksesDelete')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Account has been deleted.
</div>
@endif
@if($messages == 'gagalDelete')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Account is a parent or has been registered in other tables.
</div>
@endif
@if($messages == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@endif
@if(Session::get('messages') == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('coaLevel')}}" type="button" class="btn btn-sm btn-pure">COA Level</a>
            </div>
            @if(checkModul('O01'))
            <a target="_blank" href="{{Route('exportCoaLevel')}}">
                <button type="button" class="btn btn-sm btn-green btn-export" id="btn-export">Export Excel</button>    
            </a>
            @endif
            <button type="button" class="btn btn-green btn-insert" data-target="#m_coa" data-toggle="modal" role="dialog">
                <span class="glyphicon glyphicon-plus"></span> New</button>

        </div>
        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">COA Level 1 - 4</h4>
            </div> 
            <div class="tableadd">
                <div class="wraphead">
                    <div class="wrap-cont head-cont no-marg-right">
                        <div class="head border-right treelist wd8 ">
                            <h5>Account Name</h5>       
                        </div>
                        <div class="head border-right level wd2 ">
                            <h5>Account ID</h5>
                        </div>
                        <div class="head border-right border-right account wd1">
                            <h5>Level</h5>
                        </div>
                        <div class="head  no-padd account wd1">
                            <h5>Actions</h5>
                        </div>
                    </div>
                </div>
                <div class="wrapbody">
                    <?php
                    $array = array();
                    $f = rand(0, 50);
                    ?>
                    @foreach(Coa1::orderBy('ACC1ID','asc')->where('CompanyInternalID',Auth::user()->Company->InternalID)->get() as $data)
                    {{'';$idClass = myEncryptNumeric($data->ACC1ID)}}
                    <div class="wrap-cont list-data border-bot coaID{{$idClass}}" >
                        <div class="wd12 no-padd list-height">
                            <div class="wd8 border-left list-height">
                                <p class="parent1">
                                    @if(count(Coa2::coa2inCoa1($data->ACC1ID))>0)
                                    <span class="glyphicon glyphicon-plus" id="coaID{{$idClass}}"></span> 
                                    @else
                                    <span class="glyphicon" style="width: 14px" id="coaID{{$idClass}}"></span> 
                                    @endif
                                    {{$data->ACC1Name}}</p>
                            </div>
                            <div class="wd2 list-height">
                                <p class="parent1">{{$data->ACC1ID}}</p>
                                <?php array_push($array, $idClass) ?>
                            </div>
                            <div class="wd1 list-height right">
                                <p class="parent1">1</p>
                            </div>
                            <div class="wd1 list-height text-center border-right no-padd">
                                <p class="parent1">
                                    <?php
                                    $data->dtRecordformat = date("d-m-Y H:i:s", strtotime($data->dtRecord));
                                    $data->dtModifformat = date("d-m-Y H:i:s", strtotime($data->dtModified));
                                    $data->Remark = str_replace("\r\n", " ", $data->Remark);
                                    $arrData = array($data);
                                    $tamp = myEscapeStringData($arrData);
                                    $tamp = myEncryptJavaScriptText($tamp, $f);
                                    ?>
                                    <button id="btn-{{$data->ACC1ID}}" data-target="#m_coaUpdate" data-all='{{$tamp}}'
                                            data-level="1" data-toggle="modal" role="dialog"
                                            class="btn btn-xs btn-edit btn-pure-xs btn-custom">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </button>
                                    <button data-target="#m_coaDelete" data-internal="{{$data->InternalID}}" data-toggle="modal" role="dialog"
                                            data-id="{{$data->ACC1ID}}" data-name='{{$data->ACC1Name}}' data-level="1" class="btn btn-pure-xs btn-xs btn-delete btn-custom">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>
                                </p>
                            </div>
                        </div>
                    </div>

                    @foreach (Coa2::coa2inCoa1($data->ACC1ID) as $data2)
                    {{'';$idClass2 = myEncryptNumeric($data2->ACC2ID)}}
                    <div class="wrap-cont list-data-child border-bot coaID{{$idClass.'-up'}} coaID{{$idClass.'-hide'}} coaID{{$idClass2}} ">
                        <div class="wd12 no-padd list-height ">
                            <div class="wd8 list-height ">
                                <p class="parent1 child-padd-left">
                                    @if(count(Coa3::coa3inCoa2($data2->ACC2ID))>0)
                                    <span class="glyphicon glyphicon-plus coaID{{$idClass.'-glip'}}" id="coaID{{$idClass2}}"></span> 
                                    @else
                                    <span class="glyphicon" style="width: 14px" id="coaID{{$idClass}}"></span> 
                                    @endif
                                    {{$data2->ACC2Name}}</p>
                            </div>
                            <div class="wd2 list-height">
                                <p class="parent1">{{$data2->ACC2ID}}</p>
                                <?php array_push($array, $idClass2) ?>
                            </div>
                            <div class="wd1 list-height right">
                                <p class="parent1">2</p>
                            </div>
                            <div class="wd1 text-center list-height no-padd">
                                <p class="parent1">
                                    <?php
                                    $data2->dtRecordformat = date("d-m-Y H:i:s", strtotime($data2->dtRecord));
                                    $data2->dtModifformat = date("d-m-Y H:i:s", strtotime($data2->dtModified));
                                    $data2->Remark = str_replace("\r\n", " ", $data2->Remark);
                                    $tamp = array();
                                    foreach ($data2 as $key => $value) {
                                        $tamp[$key] = '"' . $key . '":"' . $value . '"';
                                    };
                                    $tamp = myEscapeStringDataLevel($tamp);
                                    $tamp = myEncryptJavaScriptText($tamp, $f);
                                    ?>
                                    <button id="btn-{{$data2->ACC2ID}}" data-all='{{$tamp}}' data-target="#m_coaUpdate" 
                                            data-level="2" data-toggle="modal" role="dialog"
                                            class="btn btn-xs btn-pure-xs btn-edit btn-custom">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </button>
                                    <button data-target="#m_coaDelete" data-internal="{{$data2->InternalID}}"  data-toggle="modal" role="dialog"
                                            data-id="{{$data2->ACC2ID}}" data-name="{{$data2->ACC2Name}}" data-level="2" class="btn btn-pure-xs btn-xs btn-delete btn-custom">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>
                                </p>
                            </div>
                        </div>
                    </div>

                    @foreach (Coa3::coa3inCoa2($data2->ACC2ID) as $data3)
                    {{'';$idClass3 = myEncryptNumeric($data3->ACC3ID)}}
                    <div class="wrap-cont list-data-child2 border-bot coaID{{$idClass.'-up'}} coaID{{$idClass2.'-up'}} coaID{{$idClass2.'-hide'}} coaID{{$idClass3}} ">
                        <div class="wd12 no-padd list-height ">
                            <div class="wd8 list-height ">
                                <p class="parent1 child-padd-left2">
                                    @if(count(Coa4::coa4inCoa3($data3->ACC3ID))>0)
                                    <span class="glyphicon glyphicon-plus coaID{{$idClass.'-glip'}} coaID{{$idClass2.'-glip'}}" id="coaID{{$idClass3}}"></span> 
                                    @else
                                    <span class="glyphicon" style="width: 14px" id="coaID{{$idClass}}"></span> 
                                    @endif
                                    {{$data3->ACC3Name}}</p>
                            </div>
                            <div class="wd2 list-height">
                                <p class="parent1">{{$data3->ACC3ID}}</p>
                                <?php array_push($array, $idClass3) ?>
                            </div>
                            <div class="wd1 list-height right">
                                <p class="parent1">3</p>
                            </div>                    
                            <div class="wd1 text-center list-height no-padd">                        
                                <p class="parent1">      
                                    <?php
                                    $data3->dtRecordformat = date("d-m-Y H:i:s", strtotime($data3->dtRecord));
                                    $data3->dtModifformat = date("d-m-Y H:i:s", strtotime($data3->dtModified));
                                    $data3->Remark = str_replace("\r\n", " ", $data3->Remark);
                                    $tamp = array();
                                    foreach ($data3 as $key => $value) {
                                        $tamp[$key] = '"' . $key . '":"' . $value . '"';
                                    };
                                    $tamp = myEscapeStringDataLevel($tamp);
                                    $tamp = myEncryptJavaScriptText($tamp, $f);
                                    ?>
                                    <button id="btn-{{$data3->ACC3ID}}" data-target="#m_coaUpdate" data-all='{{$tamp}}'
                                            data-level="3" data-toggle="modal" role="dialog"
                                            class="btn btn-xs btn-pure-xs btn-edit btn-custom">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </button>
                                    <button data-target="#m_coaDelete" data-internal="{{$data3->InternalID}}"  data-toggle="modal" role="dialog"
                                            data-id="{{$data3->ACC3ID}}" data-name="{{$data3->ACC3Name}}" data-level="3" class="btn btn-pure-xs btn-xs btn-delete btn-custom">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>
                                </p>
                            </div>
                        </div>
                    </div>

                    @foreach (Coa4::coa4inCoa3($data3->ACC3ID) as $data4)
                    {{'';$idClass4 = myEncryptNumeric($data4->ACC4ID)}}
                    <div class="wrap-cont list-data-child3 border-bot coaID{{$idClass.'-up'}} coaID{{$idClass2.'-up'}} coaID{{$idClass3.'-up'}} coaID{{$idClass3.'-hide'}} coaID{{$idClass4}}">
                        <div class="wd12 no-padd list-height ">
                            <div class="wd8 list-height ">
                                <p class="parent1 child-padd-left3">{{$data4->ACC4Name}}</p>
                            </div>
                            <div class="wd2 list-height">
                                <p class="parent1">{{$data4->ACC4ID}}</p>
                                <?php array_push($array, $idClass4) ?>
                            </div>
                            <div class="wd1 list-height right">
                                <p class="parent1">4</p>
                            </div>
                            <div class="wd1 text-center list-height no-padd">                        
                                <p class="parent1">
                                    <?php
                                    $data4->dtRecordformat = date("d-m-Y H:i:s", strtotime($data4->dtRecord));
                                    $data4->dtModifformat = date("d-m-Y H:i:s", strtotime($data4->dtModified));
                                    $data4->Remark = str_replace("\r\n", " ", $data4->Remark);
                                    $tamp = array();
                                    foreach ($data4 as $key => $value) {
                                        $tamp[$key] = '"' . $key . '":"' . $value . '"';
                                    };
                                    $tamp = myEscapeStringDataLevel($tamp);
                                    $tamp = myEncryptJavaScriptText($tamp, $f);
                                    ?>
                                    <button id="btn-{{$data4->ACC4ID}}" data-target="#m_coaUpdate" data-all='{{$tamp}}'
                                            data-level="4" data-toggle="modal" role="dialog"
                                            class="btn btn-xs btn-pure-xs btn-edit btn-custom">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </button>
                                    <button data-target="#m_coaDelete" data-internal="{{$data4->InternalID}}"  data-toggle="modal" role="dialog"
                                            data-id="{{$data4->ACC4ID}}" data-name="{{$data4->ACC4Name}}" data-level="4" class="btn btn-pure-xs btn-xs btn-delete btn-custom">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>
                                </p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endforeach
                    @endforeach
                    @endforeach
                    @if(Coa1::where('CompanyInternalID',Auth::user()->CompanyInternalID)->count() <= 0)
                    <span style="font-size: 12px"><center>No data available in table</center></span>
                    <hr style="height:1px;width: 98%; margin-left: 1%; color:#cccccc;background-color:#cccccc;" />
                    @endif
                </div>
            </div><!---end tableadd-->
        </div><!---- end div tabwrap---->
    </div><!---end primcontent-->
</div><!---end wrapjour-->
@stop

@section('modal')
<div class="modal fade" id="m_coa" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insert COA Level</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="form-insert">
                        <ul>
                            <li>
                                <label for="level">Choose a Level</label> *
                            </li>
                            <li>
                                <select name="jenis" id="selectLevel">
                                    <option value="insertCoa1">level 1</option>
                                    <option value="insertCoa2">level 2</option>
                                    <option value="insertCoa3">level 3</option>
                                    <option value="insertCoa4">level 4</option>
                                </select>
                            </li>   
                            <li>
                                <label for="parentid">Parent ID</label> *
                            </li>
                            <li>
                                <select class="parentCOA" id="parentCoa1" name="parentid" disabled='disabled'>
                                    <option>This level can't have parent</option>
                                </select>
                                <select class="chosen-select choosen-modal parentCOA none" id="parentCoa2" style="" name="parentid2">
                                    @foreach(Coa1::idCoa1() as $parent)
                                    <option value="{{$parent->ACC1ID}}">
                                        {{$parent->ACC1ID." "}} {{" ".$parent->ACC1Name}}
                                    </option>
                                    @endforeach
                                </select>
                                <select class="chosen-select choosen-modal parentCOA none" id="parentCoa3" style="" name="parentid3">
                                    @foreach(Coa2::idCoa2() as $parent)
                                    <option value="{{$parent->ACC2ID}}">
                                        {{$parent->ACC2ID." "}} {{" ".$parent->ACC2Name}}
                                    </option>
                                    @endforeach
                                </select>
                                <select class="chosen-select choosen-modal parentCOA none" id="parentCoa4" style="" name="parentid4">
                                    @foreach(Coa3::idCoa3() as $parent)
                                    <option value="{{$parent->ACC3ID}}">
                                        {{$parent->ACC3ID." "}} {{" ".$parent->ACC3Name}}
                                    </option>
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label for="AccID">Account ID</label> *
                            </li>
                            <li>
                                <input class="noSpecialCharacter" type="text" name="AccID" id="accID" maxlength="200" data-validation="required">
                            </li>
                            <li>
                                <label for="ACCName">Name</label> *
                            </li>
                            <li>
                                <input class="noSpecialCharacter" type="text" name="AccName" id="name" maxlength="200" data-validation="required">
                            </li>
                            <li>
                                <label for="remark">Remarks</label> *
                            </li>
                            <li>
                                <textarea style="resize:none;"  name="remark" id="remark" maxlength="1000" data-validation="required"></textarea>
                            </li>
                        </ul>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade" id="m_coaUpdate" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Update COA Level</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="form-update">
                        <ul>
                            <input type="hidden" value="" id="idUpdate" name="InternalID">
                            <input type="hidden" value="" id="jenisUpdate" name="jenis">
                            <li>
                                <label for="ACCName">Name</label> *
                            </li>
                            <li>
                                <input class="noSpecialCharacter" type="text" name="AccName" id="nameUpdate" maxlength="200" data-validation="required">
                            </li>
                            <li>
                                <label for="remark">Remarks</label> *
                            </li>
                            <li>
                                <textarea style="resize:none;"  name="remark" id="remarkUpdate" maxlength="1000" data-validation="required"></textarea>
                            </li>
                            <li>
                                <small>Created by <span id="createdDetail"></span></small><br>
                                <small>Modified by <span id="modifiedDetail"></span></small>
                            </li>
                        </ul>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-green">Update</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade" id="m_coaDelete" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Delete COA Level</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action">
                        <ul>
                            <input type="hidden" value="" id="idDelete" name="InternalID">
                            <input type="hidden" value="" id="AccID" name="AccID">
                            <input type="hidden" value="" id="jenisDelete" name="jenis">
                            <p>Are you sure want to delete <span id="deleteName"></span>?</p>
                        </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-green">Yes</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
            </div>
            </form>      
        </div>
    </div>  
</div>
@stop
<?php
$c1 = myEncryptJavaScript(Coa1::select('ACC1ID as accID')->where('CompanyInternalID', Auth::user()->Company->InternalID)->get(), $f);
$c2 = myEncryptJavaScript(Coa2::select('ACC2ID as accID')->where('CompanyInternalID', Auth::user()->Company->InternalID)->get(), $f);
$c3 = myEncryptJavaScript(Coa3::select('ACC3ID as accID')->where('CompanyInternalID', Auth::user()->Company->InternalID)->get(), $f);
$c4 = myEncryptJavaScript(Coa4::select('ACC4ID as accID')->where('CompanyInternalID', Auth::user()->Company->InternalID)->get(), $f);
?>
@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script src="{{Asset('js/chosen.jquery.js')}}" type="text/javascript"></script>
<script src="{{Asset('js/slide.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript">
var array = <?php echo '["' . implode('", "', $array) . '"]'; ?>;
var c1 = '<?php echo $c1; ?>';
var c2 = '<?php echo $c2; ?>';
var c3 = '<?php echo $c3; ?>';
var c4 = '<?php echo $c4; ?>';
var b = <?php echo $f; ?>;
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-coa/coaLevel.js')}}"></script>
@stop