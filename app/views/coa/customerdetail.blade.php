@extends('template.header-footer')

@section('title')
Customer Detail
@stop

@section('css')
<link rel="stylesheet" href="{{Asset('./css/chosen.css')}}">
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
@stop

@section('nav')

@stop

@section('content')

@if(isset($messages))
@if($messages == 'gagalInsert' || $messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New customer detail has been inserted.
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Customer detail has been updated.
</div>
@endif
@if($messages == 'suksesDelete')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Customer detail has been deleted.
</div>
@endif
@if($messages == 'gagalDelete')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Customer detail has been registered in table journal.
</div>
@endif
@if($messages == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@endif


<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group hidden-xs bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showCoa6')}}" type="button" class="btn btn-sm btn-pure">COA Level 6</a>
            </div>
            @if(checkModul('O01'))
            <a target="_blank" href="{{Route('exportDetail',Coa6::find($id)->ACC6ID)}}">
                <button type="button" class="btn btn-sm btn-green btn-export" id="btn-export">Export Excel</button>  
            </a>
            @endif
            <button type="button" class="btn btn-green btn-insert" data-target="#m_detail" data-toggle="modal" role="dialog">
                <span class="glyphicon glyphicon-plus"></span> New</button>
        </div>

        <div class="bread-xs visible-xs hidden-sm hidden-md hidden-lg">
            <p class="text-center"><a href="{{route('showDashboard')}}">{{Config::get('companyHeader.header_company');}}</a> / <a>{{ucfirst($toogle)}}</a> / <a href="{{route('showDepartment')}}">Department</a></p>
        </div>

        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                {{'';$customer = Coa6::find($id)->ACC6Name}}
                <h4 class="headtitle">Customer Detail {{$customer}}</h4>
            </div>

            <div class="tableadd">
                <table id="example" class="display table-rwd table-department" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>City</th>
                            <th>Province</th>
                            <th>Area</th>
                            <th>Phone</th>
                            <th>Tax</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
//                        $f = rand(0, 50);
                        foreach (CustomerDetail::where('ACC6InternalID', $id)->get() as $data) {
                            $data->dtRecordformat = date("d-m-Y H:i:s", strtotime($data->dtRecord));
                            $data->dtModifformat = date("d-m-Y H:i:s", strtotime($data->dtModified));
                            $data->Remark = str_replace("\r\n", " ", $data->Remark);
                            $arrData = array($data);
                            $tamp = myEscapeStringData($arrData);
//                            $tamp = myEncryptJavaScriptText($tamp, $f);
                            ?>
                            <tr>
                                <td>{{$data->CustomerDetailID}}</td>
                                <td>{{$data->DetailName}}</td>
                                <td>{{$data->Address}}</td>
                                <td>{{$data->City}}</td>
                                <td>{{$data->Province}}</td>
                                <td>{{$data->area->AreaName}}</td>
                                <td>{{$data->Phone}}</td>
                                <!--<td>{{($data->Default == 1 ? "Default" : "Not Default")}}</td>-->
                                <td>{{$data->TaxID}}</td>
                                <td class="text-center">
                                    <button id="btn-{{$data->CustomerDetailID}}" data-target="#m_detailUpdate" data-all='{{$tamp}}'
                                            data-toggle="modal" role="dialog" onclick='updateDetail(this)'
                                            class="btn btn-pure-xs btn-xs btn-edit">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </button>
                                    <button data-target="#m_detailDelete" data-internal="{{$data->InternalID}}"  data-toggle="modal" role="dialog" onclick='deleteDetail(this)'
                                            data-id="{{$data->CustomerDetailID}}" data-name='{{$data->DetailName}}' class="btn btn-pure-xs btn-xs btn-delete">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div><!---end tableadd-->
        </div><!---- end div tabwrap---->
    </div><!---end primcontent-->
</div><!---end wrapjour-->

@stop

@section('modal')
<div class="modal fade bs-example-modal-lg" id="m_detail" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="" method="post" class="action" id="form-insert">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Insert Customer Detail</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form coawd">

                        <div class="row">
                            <div class="col-md-5">
                                <input type='hidden' name='jenis' value='insertDetail'>
                                <input type='hidden' name='id' value='{{$id}}'>
                                <!--                                <div class="margbot10">
                                                                    <label for="AccID">ID *</label>
                                                                </div>
                                                                <div class="margbot10">
                                                                    <input class="noSpecialCharacter" type="text" name="DetailID" id="accID" maxlength="200" data-validation="required">
                                                                </div>-->
                                <div class="margbot10">
                                    <label for="ACCName">Name *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="noSpecialCharacter" type="text" name="DetailName" id="name" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="address">Address</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;" name="Address" maxlength="1000"></textarea>
                                </div>
                                <div class="margbot10" id='newaddress_text'>
                                    <label for="address">NPWP Address</label>
                                </div>
                                <div class="margbot10" id='newaddress_isi'>
                                    <textarea style="resize:none;" name="NpwpAddress" maxlength="1000"></textarea>
                                </div>
                                <div class="margbot10">
                                    <label for="taxID">TaxID</label>
                                </div>
                                <div class="margbot10">
                                    <div id="npwpin">
                                        <input type="hidden" name="taxID" value="" id="taxID">
                                        <input class="persen11 autoTab numaja" type="text" name="taxID1" id="taxID1" maxlength="2"
                                               data-validation="length" data-validation-length="min2" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID2" id="taxID2" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID3" id="taxID3" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen8 autoTab numaja" type="text" name="taxID4" id="taxID4" maxlength="1"
                                               data-validation="length" data-validation-length="min1" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> - 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID5" id="taxID5" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID6" id="taxID6" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information.">
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="phone">Email</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Email" maxlength="200">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="margbot10">
                                    <label for="Block">Block</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Block" id="block" maxlength="200">
                                </div>
                                <div class="margbot10">
                                    <label for="AddressNumber">Address Number</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="AddressNumber" id="addressNumber" maxlength="200">
                                </div>
                                <div class="margbot10">
                                    <label for="rt">RT</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="RT" id="rt" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="rw">RW</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="RW" id="rw" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="district">District</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="District" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="subdistrict">Sub District</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Subdistrict" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="city">City</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="City" maxlength="200" >
                                </div>
                            </div>

                            <div class="col-md-4" >
                                <div class="margbot10" id="area_text">
                                    <label for="customertype" >Area</label>
                                </div>
                                <div class="margbot10" id="area_isi">
                                    <select class="chosen-select choosen-modal" id="area" style="" name="Area">
                                        @foreach(Area::all() as $coa)
                                        <option id="coa{{$coa->InternalID}}" value="{{$coa->InternalID}}">
                                            {{$coa->AreaName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="province">Province</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Province" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="postalcode">Postal Code</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="PostalCode" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="phone">Phone</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Phone" maxlength="200">
                                </div>
                                <div class="margbot10">
                                    <label for="fax">Fax</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Fax" maxlength="200">
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;"  name="remark" id="" maxlength="1000"></textarea>
                                </div>
<!--                                <div class="margbot10 centralizationTax">
                                    <input type="checkbox" id="centralizationTax" name="centralizationTax" value="1" />&nbsp;<span>Centralization Tax</span>
                                </div>-->
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green" id="btn-submit">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade bs-example-modal-lg" id="m_detailUpdate" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="" method="post" class="action" id="form-update">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Customer Detail</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form coawd">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="margbot10">
                                    <input type="hidden" value="" id="idUpdate" name="InternalID">
                                    <input type="hidden" value="updateDetail" id="jenisUpdate" name="jenis">
                                </div>
                                <div class="margbot10">
                                    <label for="ACCName">Name *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="noSpecialCharacter" type="text" name="DetailName" id="nameUpdate" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="address">Address</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;"  name="Address" id="addressUpdate" maxlength="1000"></textarea>
                                </div>
                                <div class="margbot10" id='newaddress_update_text'>
                                    <label for="address">NPWP Address</label>
                                </div>
                                <div class="margbot10" id='newaddress_update_isi'>
                                    <textarea style="resize:none;"  name="NpwpAddress" id="newAddressUpdate" maxlength="1000"></textarea>
                                </div>
                                <div class="margbot10">
                                    <label for="taxID">TaxID</label>
                                </div>
                                <div class="margbot10">
                                    <div id="npwpin">
                                        <input type="hidden" name="taxID" value="" id="taxIDupdate">
                                        <input class="persen11 autoTab numaja" type="text" name="taxID1" id="taxID1update" maxlength="2"
                                               data-validation="length" data-validation-length="min2" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID2" id="taxID2update" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID3" id="taxID3update" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen8 autoTab numaja" type="text" name="taxID4" id="taxID4update" maxlength="1"
                                               data-validation="length" data-validation-length="min1" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> - 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID5" id="taxID5update" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID6" id="taxID6update" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information.">
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="phone">Email</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Email" id="emailUpdate" maxlength="200">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="margbot10">
                                    <label for="block">Block</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" id="blockUpdate" name="Block" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="addressnumber">Address Number</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" id="addressNumberUpdate" name="AddressNumber" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="rt">RT</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" id="rtUpdate" name="RT" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="rw">RW</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" id="rwUpdate" name="RW" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="district">District</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" id="districtUpdate" name="District" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="subdistrict">Subdistrict</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" id="subdistrictUpdate" name="Subdistrict" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="city">City</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" id="cityUpdate" name="City" maxlength="200" >
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="margbot10" id="area_update_text">
                                    <label for="customertype" >Area</label>
                                </div>
                                <div class="margbot10" id="area_update_isi">
                                    <select class="chosen-select choosen-modal" id="areaUpdate" style="" name="Area">
                                        @foreach(Area::all() as $coa)
                                        <option id="coa{{$coa->InternalID}}" value="{{$coa->InternalID}}">
                                            {{$coa->AreaName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="province">Province</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" id="provinceUpdate" name="Province" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="postalCode">Postal Code</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" id="postalCodeUpdate" name="PostalCode" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="phone">Phone</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Phone" id="phoneUpdate" maxlength="200">
                                </div>
                                <div class="margbot10">
                                    <label for="fax">Fax</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Fax" id="faxUpdate" maxlength="200">
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;"  name="remark" id="remarkUpdate" maxlength="1000"></textarea>
                                </div>
<!--                                <div class="margbot10 centralizationTaxUpdate">
                                    <input type="checkbox" id="centralizationTaxUpdate" name="centralizationTax" value="1" />&nbsp;<span>Centralization Tax</span>
                                </div>-->
                                <div class="margbot10">
                                    <small>Created by <span id="createdDetail"></span></small><br>
                                    <small>Modified by <span id="modifiedDetail"></span></small>
                                </div>
                                <div class="required margbot10">
                                    * Required
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Update</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade bs-example-modal-lg" id="m_detailDelete" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <form action="" method="post" class="action">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Delete Customer Detail</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="" id="idDelete" name="InternalID">
                            <input type="hidden" value="deleteDetail" id="jenisDelete" name="jenis">
                            <p>Are you sure want to delete <span id="deleteName"></span>?</p>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Yes</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
                </div>
            </form>      
        </div>
    </div>  
</div>
@stop
<?php
//$s = myEncryptJavaScript(Department::select('DepartmentID')->where('CompanyInternalID', Auth::user()->Company->InternalID)->get(), $f);
?>
@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript">
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-coa/customerdetail.js')}}"></script>
@stop
