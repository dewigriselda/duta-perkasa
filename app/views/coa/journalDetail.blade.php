@extends('template.header-footer')

@section('title')
Journal
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosenCustom.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
@stop

@section('nav')

@stop

@section('content')
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showJournal')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Journal</a>
                <a href="{{route('journalDetail',$header->JournalID)}}" type="button" class="btn btn-sm btn-pure">Detail {{$header->JournalID}}</a>
            </div>
            <div class="btn-group">
                <button type="button" class="btn btn-green btn-sm dropdown-toggle  margr5" data-toggle="dropdown" aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New 
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{Route('journalNew','Cash In')}}">Cash In</a></li>
                    <li><a href="{{Route('journalNew','Cash Out')}}">Cash Out</a></li>
                    <li><a href="{{Route('journalNew','Bank In')}}">Bank In</a></li>
                    <li><a href="{{Route('journalNew','Bank Out')}}">Bank Out</a></li>
                    <li><a href="{{Route('journalNew','Piutang Giro')}}">Piutang Giro</a></li>
                    <li><a href="{{Route('journalNew','Hutang Giro')}}">Hutang Giro</a></li>
                    <li><a href="{{Route('journalNew/Memorial')}}">Memorial</a></li>
                </ul>
            </div>
            <button id="search-button" class="btn btn-green btn-sm margr5"><span class="glyphicon glyphicon-search"></span> Search </button>
            @if($header->TransactionID != NULL)
            <button id="btn-{{$header->JournalID}}-update"
                    class="btn btn-green btn-sm margr5" disabled>
                <span class="glyphicon glyphicon-edit"></span> Edit
            </button>
            @else
            <a href="{{Route('journalUpdate',$header->JournalID)}}">
                <button id="btn-{{$header->JournalID}}-update"
                        class="btn btn-green btn-sm margr5">
                    <span class="glyphicon glyphicon-edit"></span> Edit
                </button>
            </a>
            @endif
            @if(checkModul('O04'))
            <a href="{{Route('journalPrint',$header->JournalID)}}" target='_blank' style="margin-right: 0px !important;">
                <button id="btn-{{$header->JournalID}}-print"
                        class="btn btn-green btn-sm ">
                    <span class="glyphicon glyphicon-print"></span> Print
                </button>
            </a>
            <a href="{{Route("getPayable")}}" target="_blank" type="button" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-file"></span> Report Payable </a>
            <a data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rSummary" href="#" type="button" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-file"></span> Report Receivable </a>
            @endif
        </div>
        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Search</h4>
            </div>
            <div class="tableadd">     
                <ul class="searchmenu">
                    <form method="GET" action="{{Route('showJournal')}}">
                        <li><label for="slipnumber">Slip</label>
                            <br>
                            <select class="chosen-select choosen-modal" id="slipID" style="" name="slipID">
                                <option value="-1">All</option>
                                @foreach(Slip::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $slip)
                                <option value="{{$slip->InternalID}}">
                                    {{$slip->SlipID.' '.$slip->SlipName}}
                                </option>
                                @endforeach
                            </select>
                        </li>
                        <li><label for="typeTransaction">Transaction Type</label>
                            <br><select name="typeTransaction" >
                                <option value="-1">All</option>
                                <option value="Cash In">Cash In</option>
                                <option value="Cash Out">Cash Out</option>  
                                <option value="Bank In">Bank In</option>
                                <option value="Bank Out">Bank Out</option>
                                <option value="Piutang Giro">Piutang Giro</option>
                                <option value="Hutang Giro">Hutang Giro</option>
                                <option value="Memorial">Memorial</option>
                            </select>
                        </li>
                        <li><label for="date">Start Date</label>
                            <br><input id="startDate" name="startDate" type="text" autocomplete="off">
                        </li>
                        <li><label for="date">End Date</label>
                            <br><input id="endDate" name="endDate" type="text" autocomplete="off">
                        </li>
                        <li><br>
                            <button type="submit" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-search"></span> Search </button>
                        </li>
                        <li><br>
                            <button id="cancel" type="reset"   class="btn btn-pure btn-sm"><span class="glyphicon glyphicon-remove"></span> Cancel </button>
                        </li>
                    </form>
                </ul>
            </div><!---- end div tableadd---->
        </div><!---- end div tabwrap---->

        <div class="tabwrap">
            <div class="tabhead">
                <h4 class="headtitle">{{$header->JournalType.' '.$header->JournalID}}</h4>
            </div>
            <div class="tableadd"> 
                <div class="headinv new">
                    <ul class="pull-left">
                        <li>
                            <label for="type">Date</label>
                            <span>{{date( "d-m-Y", strtotime($header->JournalDate))}}</span>
                        </li>
                        <li>
                            <label for="journal_from">From</label>
                            <span>{{$header->JournalFrom}}</span>
                        </li>
                        <li>
                            <label for="transaction">Transaction</label>
                            @if($header->TransactionID == NULL)
                            <span>{{"No Transaction"}}</span>
                            @else
                            <span>{{$header->TransactionID}}</span>
                            @endif
                        </li>
                        <li>
                            <label for="journal_notes">Notes</label>
                            <span>{{$header->Notes}}</span>
                        </li>
                        <!--                        <li>
                                                    <label for="journal_from">Status</label>
                                                    @if($header->Lock == 1)
                                                    <span>Lock</span>
                                                    @elseif($header->Check == 1)
                                                    <span>Lock and Check</span>
                                                    @else
                                                    <span>Not lock</span>
                                                    @endif
                                                </li>-->
                    </ul>
                    <ul class="pull-right">
                        @if($header->JournalType != 'Memorial')
                        <li>
                            <label for="slip">Slip Number</label>
                            <span><?php
                                $slip = JournalHeader::find($header->InternalID)->slip;
                                echo $slip->SlipID . ' ' . $slip->SlipName
                                ?></span>
                        </li>
                        @endif
                        <li>
                            <label for="department">Department</label>
                            <span><?php
                                $department = JournalHeader::find($header->InternalID)->department;
                                echo $department->DepartmentName
                                ?></span>
                        </li>
                        <li>
                            <label for="coa5">COA 5</label>
                            <span><?php
                                $coa5 = JournalHeader::find($header->InternalID)->coa5;
                                echo $coa5->ACC5ID . ' ' . $coa5->ACC5Name
                                ?></span>
                        </li>
                        <li>
                            <label for="journal_remark">Remark</label>
                            <span>{{$header->Remark}}</span>
                        </li>
                    </ul> 
                </div>
                <div class="padrl10">
                    <table class="table master-data " id="table-jurnal" >
                        <thead>
                            <tr>
                                <th>Account</th>
                                <th>Debet MU</th>
                                <th>Credit MU</th>
                                <th>Currency</th>
                                <th>Rate</th>
                                <th>Debet</th>
                                <th>Credit</th>
                                <th>Notes</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($detail) > 0)
                            <?php
                            $totalDebet = 0;
                            $totalKredit = 0;
                            $currencyName = '';
                            ?>
                            @foreach($detail as $data)
                            <tr>
                                <td class="left">{{Coa::formatCoa($data->ACC1InternalID, $data->ACC2InternalID, $data->ACC3InternalID, $data->ACC4InternalID, $data->ACC5InternalID, $data->ACC6InternalID, 1).' '.$data->COAName}}</td>
                                <td class="right">{{number_format($data->JournalDebetMU,'2','.',',')}}</td>
                                <td class="right">{{number_format($data->JournalCreditMU,'2','.',',')}}</td>
                                <td class="left">{{'';$currency = Currency::find($data->CurrencyInternalID); $currencyName = $currency->CurrencyName; echo $currencyName}}</td>
                                <td class="right">{{number_format($data->CurrencyRate,'2','.',',')}}</td>
                                <td class="right">{{number_format($data->CurrencyRate*$data->JournalDebetMU,'2','.',',');$totalDebet += $data->CurrencyRate*$data->JournalDebetMU}}</td>
                                <td class="right">{{number_format($data->CurrencyRate*$data->JournalCreditMU,'2','.',',');$totalKredit += $data->CurrencyRate*$data->JournalCreditMU}}</td>
                                <td class="left">{{$data->JournalNotes}}</td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8">There is no account registered in this journal.</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                    @if(count($detail) > 0)
                    <div class='pull-right'>
                        <h5><b>Total Debet : {{number_format($totalDebet,'2','.',',')}}</b></h5>
                        <h5><b>Total Credit : {{number_format($totalKredit,'2','.',',')}}</b></h5>
                    </div>
                    @endif
                </div><!---- end div padrl10---->         
            </div><!---- end div tableadd---->   
        </div><!---- end div tabwrap---->                 
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->


<div class="modal fade" id="r_summary" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Report Receivable</h4>
            </div>
            <form action="" method="post" class="action" id="" target="_blank">
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='report_receivable'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="coa6">Customer</label> *
                            </li>
                            <li >
                                <select class="chosen-select choosen-modal" id="coa6" name="coa6InternalID" data-validation="required">
                                    <option value="-1">All supplier and customer</option>
                                    @foreach(Coa6::where("CompanyInternalID", Auth::user()->Company->InternalID)->get() as $data)
                                    <option value="{{$data->InternalID}}">{{$data->ACC6ID}} - {{$data->ACC6Name}}</option>
                                    @endforeach
                                </select>
                            </li>
                        </ul>
                    </div>
                    <div class="required">
                        * Required
                    </div>
                </div>
                <div class="modal-footer">
                    <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                    <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>


@stop


@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script><script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNewJournal.jquery.js')}}" type="text/javascript"></script><script>
    var journalDataBackup = '<?php echo Route('journalDataBackup',Input::get('slipID').'---;---'.Input::get('typeTransaction').'---;---'.Input::get('startDate').'---;---'.Input::get('endDate')) ?>';
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-coa/journal.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
@stop