@extends('template.header-footer')

@section('title')
Journal
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosenCustom.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
@stop

@section('nav')
@stop

@section('content')
@if(myCheckIsEmpty('Coa;Currency;Department;Coa5;Default'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one master COA, Currency, Department, Default COA, and COA Level 5 to insert Journal.
</div>
@endif
@if(isset($messages))
@if($messages == 'suksesDelete')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Journal has been void.
</div>
@endif
@if($messages == 'gagalDelete')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Journal has been registered in other tables.
</div>
@endif
@if($messages == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest"> 
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showJournal')}}" type="button" class="btn btn-sm btn-pure">Journal</a>
            </div>
            <div class="btn-group">
                <button <?php if (myCheckIsEmpty('Coa;Currency;Department;Coa5;Default')) echo 'disabled'; ?> type="button" class="btn btn-green btn-sm dropdown-toggle  margr5" data-toggle="dropdown" aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New 
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{Route('journalNew','Cash In')}}">Cash In</a></li>
                    <li><a href="{{Route('journalNew','Cash Out')}}">Cash Out</a></li>
                    <li><a href="{{Route('journalNew','Bank In')}}">Bank In</a></li>
                    <li><a href="{{Route('journalNew','Bank Out')}}">Bank Out</a></li>
                    <li><a href="{{Route('journalNew','Piutang Giro')}}">Piutang Giro</a></li>
                    <li><a href="{{Route('journalNew','Hutang Giro')}}">Hutang Giro</a></li>
                    <li><a href="{{Route('journalNew/Memorial')}}">Memorial</a></li>
                </ul>
            </div>
            <button id="search-button" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-search"></span> Search </button>
            @if(checkModul('O04'))
            <a href="{{Route("getPayable")}}" target="_blank" type="button" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-file"></span> Report Payable </a>
            <a data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rSummary" href="#" type="button" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-file"></span> Report Receivable </a>
            @endif
        </div>
        <div class="btnnest pull-right">
            <input type="checkbox" id="showVoid" class="checkbox checkbox-inline" value="1" style="position: relative;top:-3px;left:-2px;">Show Void Data
        </div>
        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Search</h4>
            </div>
            <div class="tableadd">     
                <ul class="searchmenu">
                    <form method="GET" action="">
                        <li><label for="slipnumber">Slip</label>
                            <br>
                            <select class="chosen-select choosen-modal" id="slipID" style="" name="slipID">
                                <option value="-1">All</option>
                                @foreach(Slip::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $slip)
                                <option value="{{$slip->InternalID}}">
                                    {{$slip->SlipID.' '.$slip->SlipName}}
                                </option>
                                @endforeach
                            </select>
                        </li>
                        <li><label for="typeTransaction">Transaction Type</label>
                            <br><select name="typeTransaction" >
                                <option value="-1">All</option>
                                <option value="Cash In">Cash In</option>
                                <option value="Cash Out">Cash Out</option>  
                                <option value="Bank In">Bank In</option>
                                <option value="Bank Out">Bank Out</option>
                                <option value="Piutang Giro">Piutang Giro</option>
                                <option value="Hutang Giro">Hutang Giro</option>
                                <option value="Memorial">Memorial</option>
                            </select>
                        </li>
                        <li><label for="date">Start Date</label>
                            <br><input id="startDate" name="startDate" type="text" autocomplete="off">
                        </li>
                        <li><label for="date">End Date</label>
                            <br><input id="endDate" name="endDate" type="text" autocomplete="off">
                        </li>
                        <li><br>
                            <button type="submit" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-search"></span> Search </button>
                        </li>
                        <li><br>
                            <button id="cancel" type="reset"  class="btn btn-pure btn-sm"><span class="glyphicon glyphicon-remove"></span> Cancel </button>
                        </li>
                    </form>
                </ul>
            </div><!---- end div tableadd---->
        </div><!---- end div tabwrap---->
    </div><!---- end div wrapjour---->
</div><!---- end div wrapcontent---->
<div class="leftrow pull-left">
</div>
<div class="righttrow pull-right">
</div>
<div class="wrapjour">
    <div class="primcontent">
        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Journal</h4>
            </div>
            <div class="tableadd">
                <table id="example" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Journal ID</th>
                            <th>Type</th>
                            <th>Date</th>
                            <th>Department</th>
                            <th>Slip</th>
                            <th>COA 5</th>
                            <th>Transaction</th>
                            <th style="min-width: 65px">Actions</th>
                        </tr>
                    </thead>
                </table>
            </div><!---end of tableadd--->
        </div><!---- end div tabwrap---->
    </div><!---- end div wrapjour---->
</div><!---- end div wrapcontent---->
@stop

@section('modal')
<div class="modal fade" id="m_journalDelete" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Void Journal</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action">
                        <ul>
                            <input type="hidden" value="" id="idDelete" name="InternalID">
                            <input type="hidden" value="deleteJournal" id="jenisDelete" name="jenis">
                            <p>Are you sure want to void <span id="deleteName"></span>?</p>
                        </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-green">Yes</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
            </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade" id="r_summary" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Report Receivable</h4>
            </div>
            <form action="" method="post" class="action" id="" target="_blank">
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='report_receivable'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="coa6">Customer</label> *
                            </li>
                            <li >
                                <select class="chosen-select choosen-modal" id="coa6" name="coa6InternalID" data-validation="required">
                                    <option value="-1">All supplier and customer</option>
                                    @foreach(Coa6::where("CompanyInternalID", Auth::user()->Company->InternalID)->get() as $data)
                                    <option value="{{$data->InternalID}}">{{$data->ACC6ID}} - {{$data->ACC6Name}}</option>
                                    @endforeach
                                </select>
                            </li>
                        </ul>
                    </div>
                    <div class="required">
                        * Required
                    </div>
                </div>
                <div class="modal-footer">
                    <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                    <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script><script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script>
var journalDataBackup = '<?php echo Route('journalDataBackup', Input::get('slipID') . '---;---' . Input::get('typeTransaction') . '---;---' . Input::get('startDate') . '---;---' . Input::get('endDate')) ?>';
var journalDataBackupVoid = '<?php echo Route('journalDataBackupVoid', Input::get('slipID') . '---;---' . Input::get('typeTransaction') . '---;---' . Input::get('startDate') . '---;---' . Input::get('endDate')) ?>';

var table = $('#example').dataTable({
    "draw": 10,
    "processing": true,
    "serverSide": true,
    "ajax": journalDataBackup,
    "destroy": true
});

$("#showVoid").on("change", function () {
    if ($('#showVoid:checkbox:checked').length > 0) {
        table.fnClearTable(0);
        table = $('#example').dataTable({
            "draw": 10,
            "processing": true,
            "serverSide": true,
            "ajax": journalDataBackupVoid,
            "destroy": true
        });
    } else {
        table.fnClearTable(0);
        table = $('#example').dataTable({
            "draw": 10,
            "processing": true,
            "serverSide": true,
            "ajax": journalDataBackup,
            "destroy": true
        });
    }
});
</script>

<script src="{{Asset('js/chosenNewJournal.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-coa/journal.js')}}"></script>
@stop