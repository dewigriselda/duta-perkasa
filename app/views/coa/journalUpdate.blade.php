@extends('template.header-footer')

@section('title')
Journal
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<style>
    .help-block {
        margin-left: 30% !important;
    }
</style>
@stop

@section('nav')

@stop

@section('content')
@if(isset($messages))
@if($messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Journal has been updated.
</div>
@endif
@endif

<?php
$jenisTransaksi = 'Purchase';
$player = 'Supplier';
$jenisAccount = 'Payable';
$jenisDebetCredit = 'Debet';
$jenisDebetCreditRetur = 'Credit';
$defaultHutang = Default_s::find(5)->DefaultID;
$defaultAccount = Default_s::getInternalCoa($defaultHutang);
$defaultRetur = Default_s::find(5)->DefaultID;
$defaultAccountRetur = Default_s::getInternalCoa($defaultRetur);
if ($header->JournalType == 'Cash In' || $header->JournalType == 'Bank In') {
    $jenisTransaksi = 'Sales';
    $player = 'Customer';
    $jenisAccount = 'Receivable';
    $jenisDebetCredit = 'Credit';
    $jenisDebetCreditRetur = 'Debet';
    $defaultPiutang = Default_s::find(2)->DefaultID;
    $defaultAccount = Default_s::getInternalCoa($defaultPiutang);
    $defaultRetur = Default_s::find(2)->DefaultID;
    $defaultAccountRetur = Default_s::getInternalCoa($defaultRetur);
}
$coaSalesPurchase = Coa::getInternalID($defaultAccount->ACC1InternalID, $defaultAccount->ACC2InternalID, $defaultAccount->ACC3InternalID, $defaultAccount->ACC4InternalID, $defaultAccount->ACC5InternalID, $defaultAccount->ACC6InternalID);
$account = Coa::formatCoa($defaultAccount->ACC1InternalID, $defaultAccount->ACC2InternalID, $defaultAccount->ACC3InternalID, $defaultAccount->ACC4InternalID, $defaultAccount->ACC5InternalID, $defaultAccount->ACC6InternalID, '1') . ' ' . Coa::find($coaSalesPurchase)->COAName;
$coaRetur = Coa::getInternalID($defaultAccountRetur->ACC1InternalID, $defaultAccountRetur->ACC2InternalID, $defaultAccountRetur->ACC3InternalID, $defaultAccountRetur->ACC4InternalID, $defaultAccountRetur->ACC5InternalID, $defaultAccountRetur->ACC6InternalID);
$accountRetur = Coa::formatCoa($defaultAccountRetur->ACC1InternalID, $defaultAccountRetur->ACC2InternalID, $defaultAccountRetur->ACC3InternalID, $defaultAccountRetur->ACC4InternalID, $defaultAccountRetur->ACC5InternalID, $defaultAccountRetur->ACC6InternalID, '1') . ' ' . Coa::find($coaRetur)->COAName;
?>
<div class="wrapjour">
    <div class="primcontent mw1088">
        <div class="btnnest"> 
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showJournal')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Journal</a>
                <a href="{{route('journalUpdate',$header->JournalID)}}" type="button" class="btn btn-sm btn-pure">Update {{$header->JournalID}}</a>
            </div>
            <div class="btn-group">
                <button type="button" class="btn btn-green btn-sm dropdown-toggle  margr5" data-toggle="dropdown" aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New 
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{Route('journalNew','Cash In')}}">Cash In</a></li>
                    <li><a href="{{Route('journalNew','Cash Out')}}">Cash Out</a></li>
                    <li><a href="{{Route('journalNew','Bank In')}}">Bank In</a></li>
                    <li><a href="{{Route('journalNew','Bank Out')}}">Bank Out</a></li>
                    <li><a href="{{Route('journalNew','Piutang Giro')}}">Piutang Giro</a></li>
                    <li><a href="{{Route('journalNew','Hutang Giro')}}">Hutang Giro</a></li>
                    <li><a href="{{Route('journalNew/Memorial')}}">Memorial</a></li>
                </ul>
            </div>
            <button id="search-button" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-search"></span> Search </button>
        </div>
        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Search</h4>
            </div>
            <div class="tableadd">     
                <ul class="searchmenu">
                    <form method="GET" action="{{Route('showJournal')}}">
                        <li><label for="slipnumber">Slip</label>
                            <br>
                            <select class="chosen-select choosen-modal" id="slipID" style="" name="slipID">
                                <option value="-1">All</option>
                                @foreach(Slip::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $slip)
                                <option value="{{$slip->InternalID}}">
                                    {{$slip->SlipID.' '.$slip->SlipName}}
                                </option>
                                @endforeach
                            </select>
                        </li>
                        <li><label for="typeTransaction">Transaction Type</label>
                            <br><select name="typeTransaction" id="transui" >
                                <option value="-1">All</option>
                                <option value="Cash In">Cash In</option>
                                <option value="Cash Out">Cash Out</option>  
                                <option value="Bank In">Bank In</option>
                                <option value="Bank Out">Bank Out</option>
                                <option value="Piutang Giro">Piutang Giro</option>
                                <option value="Hutang Giro">Hutang Giro</option>
                                <option value="Memorial">Memorial</option>
                            </select>
                        </li>
                        <li><label for="date">Start Date</label>
                            <br><input id="startDate" name="startDate" type="text" autocomplete="off">
                        </li>
                        <li><label for="date">End Date</label>
                            <br><input id="endDate" name="endDate" type="text" autocomplete="off">
                        </li>
                        <li><br>
                            <button type="submit" class="btn btn-green btn-sm"> <span class="glyphicon glyphicon-search"></span> Search</button>
                        </li>
                        <li><br>
                            <button id="cancel" type="reset"  class="btn btn-pure btn-sm"><span class="glyphicon glyphicon-remove"></span> Cancel </button>
                        </li>
                    </form>
                </ul>
            </div><!---- end div tableadd---->
        </div><!---- end div tabwrap---->
        <form method="POST" action="">
            <input type='hidden' name='JournalInternalID' value='{{$header->InternalID}}'>
            <div class="tabwrap">
                <div class="tabhead">
                    <h4 class="headtitle">{{$header->JournalType.' '.$header->JournalID}}</h4>
                </div>
                <div class="tableadd"> 
                    <div class="headinv new">
                        <ul class="pull-left">
                            <li>
                                <label for="from">From *</label>
                                <input type="text" name="from" id="from" data-validation="required" value="{{$header->JournalFrom}}">
                            </li>
                            <li>
                                <label for="notes">Notes *</label>
                                <textarea name="journalNotes" id="journalNotes" data-validation="required" value="">{{$header->Notes}}</textarea>
                            </li>
                        </ul>
                        <ul class="pull-right">
                            <input type='hidden' id='slip' name='slip' value='{{$header->SlipInternalID.'---;---'}}'>
                            <li>
                                <label for="department">Department *</label>
                                <select class="chosen-select" id="department" style="" name="department">
                                    @foreach(Department::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $department)
                                    @if($department->InternalID == $header->DepartmentInternalID)
                                    <option selected="selected" value="{{$department->InternalID}}">
                                        {{$department->DepartmentName}}
                                    </option>
                                    @else
                                    <option value="{{$department->InternalID}}">
                                        {{$department->DepartmentName}}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label for="coa5">COA 5*</label>
                                <select class="chosen-select" id="coa5" style="" name="coa5">
                                    @foreach(Coa5::where('InternalID','!=','0')->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $coa5)
                                    @if($coa5->InternalID == $header->ACC5InternalID)
                                    <option selected="selected" value="{{$coa5->InternalID}}">
                                        {{$coa5->ACC5ID.' '.$coa5->ACC5Name}}
                                    </option>
                                    @else
                                    <option value="{{$coa5->InternalID}}">
                                        {{$coa5->ACC5ID.' '.$coa5->ACC5Name}}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label for="remark">Remark *</label>
                                <textarea name="remark" id="remark" data-validation="required" value="">{{$header->Remark}}</textarea>
                            </li>
                            <li>
                                <div class="required">
                                    * Required
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="tableadd journupdate">
                        <table class="table master-data" id="table-jurnal">
                            <thead>
                                <tr>
                                    <th width="20%">Account</th>
                                    <th width="10%">Debet MU</th>
                                    <th width="10%">Credit MU</th>
                                    <th width="15%">Currency</th>
                                    <th width="5%">Rate</th>
                                    <th width="10%">Debet</th>
                                    <th width="10%">Credit</th>
                                    <th width="15%">Notes</th>
                                    <th width="5%">Action</th>
                                </tr>
                            </thead>
                            <tbody class="inputbody">
                                <!--Awal kalo misal kosong atau cma berisi journal transaksi-->
                                <?php
                                $barisTerakhir = 0;
                                $barisNormal = JournalDetail::where('JournalTransactionID', NULL)
                                                ->where('JournalIndex', '<', '1000')->where('JournalInternalID', $header->InternalID)->count();
                                ?>
                                @if(count($detail) <= 0 || $barisNormal == 0)
                                <tr id="row0">
                            <input type="hidden" class="hiddenTransactionID" value="-1" name="JournalTransactionID[]">
                            <td class="appd">
                                <select class="chosen-select choosen-modal left" id="coa-0" name="coa[]">
                                    @foreach(Coa::getCoaNotSlip() as $coa)
                                    <option id="coa{{$coa->InternalID}}" value="{{$coa->InternalID}}">
                                        {{Coa::formatCoa($coa->ACC1InternalID,$coa->ACC2InternalID,$coa->ACC3InternalID,$coa->ACC4InternalID,$coa->ACC5InternalID,$coa->ACC6InternalID,'1')}} 
                                        {{" ".$coa->COAName}}
                                    </option>
                                    @endforeach
                                </select>
                            </td>
                            <td class="dcmu">
                                <input type="text" class="maxWidth debetJournal right" name="Debet_value[]" maxlength="200" id="debet-0" value="0">
                            </td>
                            <td class="dcmu">
                                <input type="text" class="maxWidth kreditJournal right" name="Kredit_value[]" maxlength="200" id="kredit-0" value="0">
                            </td>
                            <td>
                                <!--cek kalau2 bukan kosong tp cma ad journal transaksi berarti kan sudah ada currency dan rate-->
                                <select class="chosen-select choosen-modal currency" id="currency" name="currency">
                                    {{'';$default = 0;}}
                                    @foreach(Currency::orderBy('Default','desc')->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $cur)
                                    @if(count($detail) > 0)
                                    @if($detail[0]->CurrencyInternalID == $cur->InternalID)
                                    <option selected="selected" id="cur{{$cur->InternalID}}" value="{{$cur->InternalID.'---;---'.$cur->CurrencyName.'---;---'.$cur->Rate.'---;---'.$cur->Default}}">
                                        {{$cur->CurrencyName;}}
                                    </option>
                                    @else
                                    <option id="cur{{$cur->InternalID}}" value="{{$cur->InternalID.'---;---'.$cur->CurrencyName.'---;---'.$cur->Rate.'---;---'.$cur->Default}}">
                                        {{$cur->CurrencyName;}}
                                    </option>
                                    @endif
                                    @else
                                    <option id="cur{{$cur->InternalID}}" value="{{$cur->InternalID.'---;---'.$cur->CurrencyName.'---;---'.$cur->Rate.'---;---'.$cur->Default}}">
                                        {{$cur->CurrencyName;}}
                                        @if($default == 0)
                                        {{'';$default=$cur->Rate}}
                                        @endif
                                    </option>
                                    @endif
                                    @endforeach
                                </select>
                            </td>
                            <td class="ratetd">
                                @if(count($detail) > 0)
                                {{'';$default = $detail[0]->CurrencyRate}}
                                @endif
                                <input type="text" class="maxWidth rate right" name="rate" maxlength="" id="rate" value="{{number_format($default,'2','.',',')}}">
                            </td>
                            <td id="debet-0-hitung" class="right">
                                0
                            </td>
                            <td id="kredit-0-hitung" class="right">
                                0
                            </td>
                            <td>
                                <textarea name="notes[]" id="notes" style="resize:none"></textarea>
                            </td>
                            <td>
                                -
                            </td>
                            </tr>
                            {{'';$barisTerakhir++;}}
                            @endif
                            <!--munculin semua detail yang ada di database-->
                            @if(count($detail) > 0)
                            @foreach($detail as $data)
                            @if($data->JournalIndex != '1000' && $data->JournalTransactionID == '')
                            <tr id="row{{$barisTerakhir}}">
                                <td class="appd">
                                    <input type="hidden" class="hiddenTransactionID" value="-1" name="JournalTransactionID[]">
                                    <select class="chosen-select choosen-modal left" id="coa-{{$barisTerakhir}}" name="coa[]">
                                        @foreach(Coa::getCoaNotSlip() as $coa)
                                        @if($coa->InternalID == Coa::getInternalID($data->ACC1InternalID,$data->ACC2InternalID,$data->ACC3InternalID,$data->ACC4InternalID,$data->ACC5InternalID,$data->ACC6InternalID))
                                        <option selected="selected" id="coa{{$coa->InternalID}}" value="{{$coa->InternalID}}">
                                            {{Coa::formatCoa($coa->ACC1InternalID,$coa->ACC2InternalID,$coa->ACC3InternalID,$coa->ACC4InternalID,$coa->ACC5InternalID,$coa->ACC6InternalID,'1')}} 
                                            {{" ".$coa->COAName}}
                                        </option>
                                        @else
                                        <option id="coa{{$coa->InternalID}}" value="{{$coa->InternalID}}">
                                            {{Coa::formatCoa($coa->ACC1InternalID,$coa->ACC2InternalID,$coa->ACC3InternalID,$coa->ACC4InternalID,$coa->ACC5InternalID,$coa->ACC6InternalID,'1')}} 
                                            {{" ".$coa->COAName}}
                                        </option>
                                        @endif
                                        @endforeach
                                    </select>
                                </td>
                                <td class="dcmu">
                                    <input type="text" class="maxWidth debetJournal right" name="Debet_value[]" maxlength="200" id="debet-{{$barisTerakhir}}" value="{{number_format($data->JournalDebetMU,'2','.',',')}}">
                                </td>
                                <td class="dcmu">
                                    <input type="text" class="maxWidth kreditJournal right" name="Kredit_value[]" maxlength="200" id="kredit-{{$barisTerakhir}}" value="{{number_format($data->JournalCreditMU,'2','.',',')}}">
                                </td>
                                @if($barisTerakhir == 0)
                                <td>
                                    <select class="chosen-select choosen-modal currency" id="currency" name="currency">
                                        @foreach(Currency::orderBy('Default','desc')->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $cur)
                                        @if($cur->InternalID == $data->CurrencyInternalID)
                                        <option selected="selected" id="cur{{$cur->InternalID}}" value="{{$cur->InternalID.'---;---'.$cur->CurrencyName.'---;---'.$cur->Rate.'---;---'.$cur->Default}}">
                                            {{$cur->CurrencyName;}}
                                        </option>
                                        @else
                                        <option id="cur{{$cur->InternalID}}" value="{{$cur->InternalID.'---;---'.$cur->CurrencyName.'---;---'.$cur->Rate.'---;---'.$cur->Default}}">
                                            {{$cur->CurrencyName;}}
                                        </option>
                                        @endif
                                        @endforeach
                                    </select>
                                </td>
                                @else
                                <td class='autoCurrency left'>
                                    {{Currency::find($data->CurrencyInternalID)->CurrencyName}}
                                    @endif
                                </td>
                                @if($barisTerakhir == 0)
                                <td class="ratetd">
                                    <input type="text" class="maxWidth rate right" name="rate" maxlength="" id="rate" value="{{number_format($data->CurrencyRate,'2','.',',')}}">
                                </td>
                                @else
                                <td class="autoRate right">
                                    {{number_format($data->CurrencyRate,'2','.',',')}}
                                </td>
                                @endif
                                </td>
                                <td id="debet-{{$barisTerakhir}}-hitung" class="right">
                                    {{number_format($data->JournalDebet,'2','.',',')}}
                                </td>
                                <td id="kredit-{{$barisTerakhir}}-hitung" class="right">
                                    {{number_format($data->JournalCredit,'2','.',',')}}
                                </td>
                                <td>
                                    <textarea rows="1.5" name="notes[]" id="notes-{{$barisTerakhir}}" style="resize:none">{{$data->JournalNotes}}</textarea>
                                </td>
                                <td>
                                    @if($barisTerakhir == 0)
                                    -
                                    @else
                                    <button class="btn btn-pure-xs btn-xs btn-deleteRow" type="button" data="row{{$barisTerakhir}}"><span class="glyphicon glyphicon-trash"></span></button>
                                    @endif
                                </td>
                            </tr>
                            {{'';$barisNormal++;}}
                            @elseif($data->JournalTransactionID != '')
                            <tr id="row{{$barisTerakhir}}">
                            <input type="hidden" class="hiddenTransactionID" value="{{$data->JournalTransactionID}}" name="JournalTransactionID[]">
                            <td class="appd">
                                <input type="hidden" value="{{Coa::getInternalID($data->ACC1InternalID,$data->ACC2InternalID,$data->ACC3InternalID,$data->ACC4InternalID,$data->ACC5InternalID,$data->ACC6InternalID)}}" name="coa[]">
                                {{'';$coa = Coa::find(Coa::getInternalID($data->ACC1InternalID,$data->ACC2InternalID,$data->ACC3InternalID,$data->ACC4InternalID,$data->ACC5InternalID,$data->ACC6InternalID))}}
                                {{Coa::formatCoa($coa->ACC1InternalID,$coa->ACC2InternalID,$coa->ACC3InternalID,$coa->ACC4InternalID,$coa->ACC5InternalID,$coa->ACC6InternalID,'1')}} 
                                {{" ".$coa->COAName}}
                            </td>
                            <td class="dcmu right">
                                {{number_format($data->JournalDebetMU,'2','.',',')}}
                                <input type="hidden" class="maxWidth debetJournal right" name="Debet_value[]" maxlength="200" id="debet-{{$barisTerakhir}}" value="{{number_format($data->JournalDebetMU,'2','.',',')}}">
                            </td>
                            <td class="dcmu right">
                                {{number_format($data->JournalCreditMU,'2','.',',')}}
                                <input type="hidden" class="maxWidth kreditJournal right" name="Kredit_value[]" maxlength="200" id="kredit-{{$barisTerakhir}}" value="{{number_format($data->JournalCreditMU,'2','.',',')}}">
                            </td>
                            <td class='autoCurrency left'>
                                {{Currency::find($data->CurrencyInternalID)->CurrencyName}}
                            </td>
                            <td class="autoRate right">
                                {{number_format($data->CurrencyRate,'2','.',',')}}
                            </td>
                            </td>
                            <td id="debet-{{$barisTerakhir}}-hitung" class="right">
                                {{number_format($data->JournalDebet,'2','.',',')}}
                            </td>
                            <td id="kredit-{{$barisTerakhir}}-hitung" class="right">
                                {{number_format($data->JournalCredit,'2','.',',')}}
                            </td>
                            <td>
                                <textarea rows="1.5" name="notes[]" id="notes-{{$barisTerakhir}}" style="resize:none">{{$data->JournalNotes}}</textarea>
                            </td>
                            <td>
                                <button class="btn btn-pure-xs btn-xs btn-deleteRow" type="button" data="row{{$barisTerakhir}}"><span class="glyphicon glyphicon-trash"></span></button>
                            </td>
                            </tr>
                            @endif
                            {{'';$barisTerakhir++;}}
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div><!---- end div tableadd----> 
                    <h5 class="right margr10 h5total"><b id="totalDebet">Total Debet : 0</b></h5>
                    <h5 class="right margr10 h5total"><b id="totalKredit">Total Credit : 0</b></h5>
                    <button type="button" class="btn btn-green btn-sm margl10 margr5" id="btn-addRow"><span class="glyphicon glyphicon-plus"></span> Add row</button>
                    @if ($tipe != 'Memo')
                    <button type="button" class="btn btn-green btn-sm " data-toggle="modal" data-target="#multiselect" ><span class="glyphicon glyphicon-copy"></span> Account {{$jenisAccount}}</button>
                    @endif
                </div><!---- end div tableadd---->   
            </div><!---- end div tabwrap---->  
            <div class="btnnest pull-right">
                <button class="btn btn-green btn-sm btn-save" id="btn-save"> Save </button>
            </div> 
        </form>
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop

@section('modal')
<div class="modal fade bs-example-modal-lg" id="multiselect" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg " style="width:1100px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Account {{$jenisAccount}}</h4>
            </div>
            <div class="modal-body modal-receive">
                <button type="button" class="btn btn-pure check-all" id="checkAll" style="position: absolute; left: 15x;top: 50px;height: 56px; z-index: 9999; border-bottom-left-radius: 0;border-bottom-right-radius: 0; border-top-right-radius: 0; border-top-left-radius: 10px; border-color: #dddddd;"><span class="glyphicon glyphicon-check"></span></button>
                <table id="example" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr style="border-top: none !important">
                            <th width="3%" style="visibility: hidden;"></th>
                            <th width="12%">{{$jenisTransaksi}} ID</th>
                            <th width="15%">{{$player}}</th>
                            <th width="12%">Date</th>
                            <th width="12%">Due Date</th>
                            <th>Down Payment</th>
                            <th>Total</th>
                            <th>Balance</th>
                            <th>Currency</th>
                            <th>Rate</th>
                            <th>Value</th>
                        </tr>
                    </thead>
                    <tbody id="dataaccount">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-green" id="btn-insertAccount">Insert</button>  
                <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script><script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNewJournal.jquery.js')}}" type="text/javascript"></script>
<script>
var textSelect = '<select class="chosen-select choosen-modal" id="" style="" name="coa[]">' +
<?php foreach (Coa::getCoaNotSlip() as $coa) { ?> '<option id="coa<?php echo $coa->InternalID ?>" value="<?php echo $coa->InternalID ?>">' +
            '<?php echo Coa::formatCoa($coa->ACC1InternalID, $coa->ACC2InternalID, $coa->ACC3InternalID, $coa->ACC4InternalID, $coa->ACC5InternalID, $coa->ACC6InternalID, '1') . " " . $coa->COAName ?>' + '</option>' +<?php } ?>
'</select>';
var dataSlip = '';
var baris = '<?php echo $barisTerakhir; ?>';
var jenisTransaksi = '<?php echo $jenisTransaksi; ?>';
var jenisDebetCredit = '<?php echo $jenisDebetCredit; ?>';
var account = '<?php echo $account; ?>';
var accountInternalID = '<?php echo $coaSalesPurchase; ?>';
var accountR = '<?php echo $accountRetur; ?>';
var accountRInternalID = '<?php echo $coaRetur; ?>';
<?php
foreach (Slip::coaID() as $value) {
    $slipID = $value->InternalID;
    $coaID = $value->COAID;
    $tamp = explode('-', $coaID);
    $coaID = Coa::formatCoa($tamp[0], $tamp[1], $tamp[2], $tamp[3], $tamp[4], $tamp[5], 1);
    $coaName = $value->COAName;
    ?>
    dataSlip += '[' + '<?php echo $slipID ?>' + '] : &"' + '<?php echo $coaID ?>' + '&",&"' + '<?php echo $coaName ?>' + '&"---;---';
<?php } ?>
<?php if ($tipe == 'Memo') { ?>
    var tipeJournal = 'Memo';
<?php } else { ?>
    var tipeJournal = 'InOut';
<?php } ?>
</script>
<script>
    var getDataAccount = '<?php echo Route('getDataAccount') ?>';
    var jenis = '<?php echo $header->JournalType; ?>';
    var journalDataBackup = '<?php echo Route('journalDataBackup', Input::get('slipID') . '---;---' . Input::get('typeTransaction') . '---;---' . Input::get('startDate') . '---;---' . Input::get('endDate')) ?>';
</script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-coa/journal.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-coa/journalUpdate.js')}}"></script>
@stop