@extends('template.header-footer')

@section('title')
Top Up New
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<style>
    .help-block {
        margin-left: 30% !important;
    }
    label{
        width:115px !important;
    }
</style>
@stop

@section('nav')

@stop

@section('content')
@if(myCheckIsEmpty('Currency'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one master Inventory, Currency and Warehouse to insert Top Up.
</div>
@endif
@if(isset($messages))
@if($messages == 'gagalInsert')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Top Up has been updated.
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest"> 
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showTopUp')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Top Up</a>
                <a href="{{route('topUpNew')}}" type="button" class="btn btn-sm btn-pure">New Top Up</a>
            </div>
            <div class="btn-group margr5">
                <a href="{{Route('topUpNew')}}">
                    <button <?php if (myCheckIsEmpty('Slip;Currency')) echo 'disabled'; ?> type="button" class="btn btn-green btn-sm dropdown-toggle  " aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
                </a>
            </div>
            <button id="search-button" <?php if (myCheckIsEmpty('PurchaseOrder')) echo 'disabled'; ?>  class="btn btn-green btn-sm margr5"><span class="glyphicon glyphicon-search"></span> Search </button>
        </div>
        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Search</h4>
            </div>
            <div class="tableadd">     
                <ul class="searchmenu">
                    <form method="GET" action="">
                        <li><label for="typePayment">Payment Type</label>
                            <br>
                            <select name="typePayment" style="width: 100px">
                                <option value="-1">All</option>
                                <!--                                <option value="0">Cash</option>
                                                                <option value="1">Credit</option>  -->
                                <option value="0">CBD</option>  
                                <option value="1">Deposit</option>  
                                <option value="2">Down Payment</option>  
                            </select>
                        </li>
                        <li><label for="typeTax">Tax Type</label>
                            <br>
                            <select name="typeTax" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Non Tax</option>
                                <option value="1">Tax</option>  
                            </select>
                        </li>
                        <li><label for="date">Start Date</label>
                            <br><input id="startDate" name="startDate" type="text" autocomplete="off">
                        </li>
                        <li><label for="date">End Date</label>
                            <br><input id="endDate" name="endDate" type="text" autocomplete="off">
                        </li>
                        <li><br>
                            <button type="submit" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-search"></span> Search </button>
                        </li>
                        <li><br>
                            <button id="cancel" type="reset"  class="btn btn-pure btn-sm"><span class="glyphicon glyphicon-remove"></span> Cancel </button>
                        </li>
                    </form>
                </ul>
            </div><!---- end div tableadd---->
        </div><!---- end div tabwrap---->
        <form method="POST" action="">
            <div class="tabwrap">
                <div class="tabhead">
                    <h4 class="headtitle">Update Top Up <span id="topupID">{{$id}}</span></h4>
                </div>
                <div class="tableadd"> 
                    <div class="headinv new">
                        <input type="hidden" id="InternalID" name="InternalID" value="{{$internalid}}">
                        <input type="hidden" name="type" value="{{$topup->Type}}">
                        <input type="hidden" name="grandTotalValue" value="0" id="grandTotalValue">
                        <input type="hidden" name="grandTotalCek" value="0" id="grandTotalCek">
                        <ul class="col-md-6" style="background-color: none">
                            <li>
                                <label for="payment">Type *</label>
                                @if($topup->Type == "customer")
                                Customer
                                @else
                                Supplier
                                @endif
                            </li>
                            <li id="inputsupplier" class="supplier">
                                <label for="coa6">Supplier *</label>
                                <input type='hidden' name="supplierID" id="supplierID" value="{{$topup->ACC6InternalID}}">
                                {{Coa6::find($topup->ACC6InternalID)->ACC6Name}}
                            </li>
                            <li id="inputcustomer" class="customer">
                                <label for="coa6">Customer *</label>
                                <input type='hidden' name="customerID" id="customerID" value="{{$topup->ACC6InternalID}}">
                                {{Coa6::find($topup->ACC6InternalID)->ACC6Name}}
                            </li>
                            <li>
                                <label for="currency">Payment Type *</label>
                                @if($topup->PaymentType == 0)
                                CBD
                                @elseif($topup->PaymentType == 1)
                                Deposit
                                @else
                                Down Payment
                                @endif
                                <input type="hidden" id="paymentType" name="paymentType" value="{{$topup->PaymentType}}">
                            </li>
                            <li class="salesorder customer">
                                <label for="customerID">Sales Order *</label>
                                <select id="salesOrderID" style="height:30px" style="" name="salesOrderID">

                                </select>
                            </li>
                            <!--                            <li class="salesorder customer">
                                                            <label for="customerID">Sales Order *</label>
                                                            @if(SalesOrderHeader::where('InternalID',$topup->SalesOrderInternalID)->count() >0)
                                                            {{SalesOrderHeader::find($topup->SalesOrderInternalID)->SalesOrderID}}
                                                            <input type='hidden' name="salesOrderID" id="salesOrderID" value="{{$topup->SalesOrderInternalID}}">
                                                            @endif
                                                        </li>-->
                            <li class="purchaseorder supplier">
                                <label for="supplierID">Purchase Order *</label>
                                <select id="purchaseOrderID" style="height:30px" style="" name="purchaseOrderID">

                                </select>
                            </li>
                            <!--                            <li class="purchaseorder supplier">
                                                            <label for="supplierID">Purchase Order *</label>
                                                            @if(PurchaseOrderHeader::where('InternalID',$topup->PurchaseOrderInternalID)->count() >0)
                                                            {{PurchaseOrderHeader::find($topup->PurchaseOrderInternalID)->PurchaseOrderID}}
                                                            <input type='hidden' name="purchaseOrderID" id="purchaseOrderID" value="{{$topup->PurchaseSOrderInternalID}}">
                                                            @endif
                                                        </li>-->
                            <li>
                                <label for="currency">Amount</label>
                                <label id="amount" style="background-color: none;text-align: left">-</label>
                            </li>
                            <li>
                                <label for="currency">Top Up Amount *</label>
                                <input type="hidden" class="maxWidth price numajaDesimal" maxlength="" name="grandTotal" id="grandTotal" value="">
                                <input type="text" class="maxWidth price numajaDesimal" maxlength="" id="downpayment"  name="downpayment"  data-validation="required" value="{{number_format($topup->GrandTotalTopUp,"2",".",",")}}">
<!--                                <div style="display:none;">
                                    <input style="width:15px; height: 15px;" type="checkbox" name="vat" id="vat" value="1" checked="checked"> Tax 10%
                                </div>-->
                            </li>
                            <li>
                                <label for="selisih">Selisih Bayar</label>
                                <input type="text" class="maxWidth price numajaDesimalMinus" maxlength="" id="selisihbayar"  name="selisihbayar"  data-validation="required" value="{{$topup->SelisihBayar}}">
                            </li>
                            <li>
                                <label for="selisih">Tax</label>
                                <input type="checkbox" id="usetax" value="1" name="vat" @if($topup->VAT == 1) checked @endif> Tax 10%
                            </li>
                        </ul>
                        <ul class="col-md-6">
                            <li>
                                <label for="date">Date *</label>
                                <input id="date" name="date" type="text" autocomplete="off" data-validation="required" value="">
                            </li>
                            <li class="">
                                <label for="customerID">Tax Number </label>
                                <input type="hidden" name="TaxNumber" value="" id="numberTax">
                                <input type="text" class="numaja autoTab2" style="width: 40px;" maxlength="3" id="numberTax1" > .
                                <input type="text" class="numaja autoTab2" style="width: 40px;" maxlength="3" id="numberTax2" > -
                                <input type="text" class="numaja autoTab2" style="width: 30px;" maxlength="2" id="numberTax3" > .
                                <input type="text" class="numaja autoTab2" style="width: 75px;" maxlength="8" id="numberTax4" >
                            </li>
                            <li>
                                <label for="account">To Account *</label>
                                <select class="chosen-select choosen-modal" name="account" id="account" data-validation="required" style="" name="account">
                                    @foreach(Slip::where('CompanyInternalID', Auth::user()->Company->InternalID)
                                    ->whereIn('Flag',array(0,1))
                                    ->get() as $slip)
                                    <option @if($topup->SlipInternalID == $slip->InternalID)selected @endif value="{{$slip->InternalID}}">
                                             {{$slip->SlipID.' '.$slip->SlipName}}
                                </option>
                                @endforeach
                            </select>
                        </li>        
                        <li>
                            <label for="currency">Currency *</label>
                            <select class="chosen-select choosen-modal currency" id="currencyHeader" name="currency">
                                {{'';$default = 0;}}
                                @foreach(Currency::orderBy('Default','desc')->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $cur)
                                <option @if($topup->CurrencyInternalID == $cur->InternalID)selected @endif id="cur{{$cur->InternalID}}" value="{{$cur->InternalID.'---;---'.$cur->CurrencyName.'---;---'.$cur->Rate.'---;---'.$cur->Default}}">
                                         {{$cur->CurrencyName;}}
                                         @if($default == 0)
                                         {{'';$default=$cur->Rate}}
                                         @endif
                            </option>
                            @endforeach
                        </select>
                    </li>
                    <li>
                        <label for="rate">Rate *</label>
                        <input type="text" class="maxWidth rate numajaDesimal" name="rate" maxlength="" id="rate" value="{{$topup->CurrencyRate}}" data-validation="required">
                    </li>
                    <li>
                        <label for="remark">Remark</label>
                        <textarea name="remark" id="remark">{{$topup->Remark}}</textarea>
                    </li>
                    <li>
                        <div class="required">
                            * Required
                        </div>
                    </li>
                </ul>
            </div> 
        </div><!---- end div tableadd---->   
    </div><!---- end div tabwrap---->  
    <div class="btnnest pull-right">
        <button class="btn btn-green btn-sm btn-save" id="btn-save" <?php if (myCheckIsEmpty('Inventory;Warehouse;Currency')) echo 'disabled'; ?>> Save </button>
    </div>
</form>
</div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNewJournal.jquery.js')}}" type="text/javascript"></script>
<script>
var textSelect = '';
var tanggalHariIni = '<?php echo date("d-m-Y", strtotime($topup->TopUpDate)) ?>';
var taxx = '<?php echo $topup->Tax ?>';
var getSO = "<?php echo route("getSO"); ?>";
var getPO = "<?php echo route("getPO"); ?>";
var checkPO = "<?php echo route("checkPOSupplierUpdate"); ?>"
var checkSO = "<?php echo route("checkSOCustomerUpdate"); ?>"
var getSOAmount = "<?php echo route("getSOAmountExcept"); ?>";
var getSOAmountDp = "<?php echo route("getSOAmountDPExcept"); ?>"
var getPOAmount = "<?php echo route("getPOAmountExcept"); ?>";
var getPOAmountDp = "<?php echo route("getPOAmountDPExcept"); ?>"
var tipe = "<?php echo $topup->Type; ?>"
var salesorder = "<?php echo $topup->SalesOrderInternalID; ?>"
var purchaseorder = "<?php echo $topup->PurchaseOrderInternalID; ?>"
</script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script>
var topUpDataBackup = '<?php echo Route('topUpDataBackup') ?>';
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-coa/topUp.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-coa/topUpUpdate.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script>
$.validate();
//$("#date").change(function () {
//    var cariText = '';
//    if ($('#customerTopup').is(':checked') == true) {
//        $('#topupID').load('<?php echo Route('formatCariIDCustomerTopUp') ?>', {
//            "date": $("#date").val()
//        });
//    } else {
//        $('#topupID').load('<?php echo Route('formatCariIDSupplierTopUp') ?>', {
//            "date": $("#date").val()
//        });
//    }
//});
</script>
@stop