@extends('template.header-footer')

@section('title')
Dashboard
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosenCustom.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<link rel="stylesheet" href="{{Asset('morris.css')}}">
@stop

@section('content')


@if(isset($messages))
@if($messages == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@endif
@if(Session::get('messages') == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif



<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest hidden-xs"> 
            <div class="btn-group bread nomarg" role="group">
                <a href="{{Route('showDashboard')}}"  class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a class="btn btn-sm btn-pure">{{ucfirst($toogle)}}</a>
            </div>
        </div>

        <div class="bread-xs visible-xs hidden-sm hidden-md hidden-lg">
            <p class="text-center"><a href="{{route('showDashboard')}}">{{Config::get('companyHeader.header_company');}}</a> / <a>{{ucfirst($toogle)}}</a></p>
        </div>
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->

<div class="wrapjour">
    <div class="primcontentnopadd">
        <div class="row">
            <div  class="col-sm-12 col-md-6">
                <div>
                    <div class="tabwrap">
                        <div class="tabhead">
                            <h4 class="headtitle">User Profile from {{Auth::user()->UserID}}</h4>
                        </div>
                        <div class="tableadd overhide"> 
                            <form action="" method="post" class="actionprofile" id="form-insert" enctype="multipart/form-data">
                                <div class="col-md-3">
                                    <div class="photo">
                                        @if(Auth::user()->Picture == NULL)
                                        <img src="{{Asset('/img/profile.png')}}" width="160" height="180">
                                        @else
                                        <img src="{{Asset(Auth::user()->Picture)}}" width="160" height="180">
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-8 col-md-offset-1">
                                    <ul>
                                        <li>
                                            <input type='hidden' name='jenis' value='updateProfile'>
                                            <label for="UserID">User ID </label> 
                                            <span>{{Auth::user()->UserID}}</span>
                                        </li>
                                        <li>
                                            <label for="Name">Name </label> 
                                            <span>{{Auth::user()->UserName}}</span>
                                        </li>
                                        <li>
                                            <label for="LastLogin">Last Login </label> 
                                            @if(Session::get('LastLogin') == NULL)
                                            <span>New User</span>
                                            @else
                                            <span>{{date('d M Y H:i:s', strtotime(Session::get('LastLogin')))}}</span>
                                            @endif
                                        </li>
                                        <li>
                                            <label for="CompanyID">Company ID </label> 
                                            <span>{{Auth::user()->Company->CompanyID}}</span>
                                        </li>
                                        <li>
                                            <label for="Company">Company Name</label> 
                                            <span>{{Auth::user()->Company->CompanyName}}</span>
                                        </li>
                                        <li>
                                            <label for="Expired">Expired Date</label> 
                                            <span>{{date("d M Y", strtotime(Auth::user()->Company->ExpiredDate))}}</span>
                                        </li>
                                        <li>
                                            <div style="width: 100%">
                                                <label for="Expired" style="width: 45%;float:left">Memory Company</label> 
                                                <span class="headinv" id="donut" style="width: 50%;height: 150px;margin: 0;float: left"></span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </form>
                        </div><!---- end div tableadd---->   
                    </div><!---- end div tabwrap---->                       
                </div><!---- end div leftrow----> 
                <!--                <div>
                                    <div class="tabwrap">
                                        <div class="tabhead">
                                            <h4 class="headtitle">Memory Company</h4>
                                        </div>
                                        <div class="tableadd overauto">
                                            <div class="headinv new overauto" id="donut2">
                                            </div>-- end div new--
                                        </div>-- end div tableadd--   
                                    </div>-- end div tabwrap--                       
                                </div>-- end div rigthrow--  -->
            </div>
            <div class="overauto col-sm-12 col-md-6">
                <div>
                    <div class="tabwrap">
                        <div class="tabhead">
                            <h4 class="headtitle">Warning Stock</h4>
                        </div>
                        <div class="tableadd overauto">
                            <div class="headinv new overauto">
                                <table id="tableWStock" class="table table-fixed">
                                    <thead>
                                        <tr>
                                            <th>Inventory ID</th>
                                            <th>Inventory Name</th>
                                            <th>Stock</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div><!---- end div new---->
                        </div><!---- end div tableadd---->   
                    </div><!---- end div tabwrap---->                       
                </div><!---- end div rigthrow---->  
            </div>
        </div>
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<!--<script type="text/javascript" src="{{Asset('js/entry-js-penjualan/sales.js')}}"></script>-->
<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.2/raphael-min.js"></script>
<script src="{{Asset('morris.js')}}" type="text/javascript"></script>
<script>
//morris js............................................................
var exeMorrisDonut = function () {
    Morris.Donut({
        element: 'donut',
        data: [
            {value: <?php echo countMemory(); ?>, label: 'Usage', formatted: '<?php echo number_format(countMemory(), '2', '.', ','); ?> Mb'},
            {value: <?php echo Auth::user()->Company->Package->Memory - countMemory(); ?>, label: 'Available', formatted: '<?php echo number_format((Auth::user()->Company->Package->Memory - countMemory()), '2', '.', ','); ?> Mb'}
        ],
        formatter: function (x, data) {
            return data.formatted;
        },
        resize: true
    });
};

exeMorrisDonut();

var wStock = "<?php echo route('wStock'); ?>";
</script>
<script src="{{Asset('js/dashboard.js')}}" type="text/javascript"></script>

@stop