<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <div>
            <h2>Your Password</h2>

            <p>Dear, {{$name}}</p>
            <p>Your new password has been reset, this is your new password:</p>
                <p>User ID : {{$userID}}</p>
                <p>New Password : {{$password}}</p>
                
            <br>
            <p>Best Regards, </p>
            <p>Salmon Accounting</p>
        </div>
    </body>
</html>