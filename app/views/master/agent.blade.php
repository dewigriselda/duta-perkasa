@extends('template.header-footer')

@section('title')
Agent
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
@stop

@section('nav')

@stop

@section('content')
@if(isset($messages))
@if($messages == 'gagalInsert' || $messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New Agent has been inserted.
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Agent has been updated.
</div>
@endif
@if($messages == 'suksesDelete')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Agent has been deleted.
</div>
@endif
@if($messages == 'gagalDelete')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Agent has been create transaction.
</div>
@endif
@if($messages == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@endif
@if(Session::get('messages') == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread" role="group">
                <a href="{{route('showUser')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showAgent')}}" type="button" class="btn btn-sm btn-pure">Agent</a>
            </div>
            @if(checkModul('O01') || Auth::user()->CompanyInternalID == -1)
            <a target="_blank" href="{{Route('exportAgent')}}">
                <button type="button" class="btn btn-sm btn-green btn-export" id="btn-export">Export Excel</button>    
            </a>
            @endif
            <button type="button" class="btn btn-green btn-insert" data-target="#m_agent" data-toggle="modal" role="dialog">
                <span class="glyphicon glyphicon-plus"></span> New</button>
        </div>
        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Agent</h4>
            </div>
            <div class="tableadd">
                <table id="example" class="display table-rwd table-agent" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Agent ID</th>
                            <th>Name</th>
                            <th>Company</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Account Bank</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $userData = User::where('CompanyInternalID', -2)
                                ->get();
                        $f = rand(0, 50);
                        foreach ($userData as $data) {
                            $data->dtRecordformat = date("d-m-Y H:i:s", strtotime($data->dtRecord));
                            $data->dtModifformat = date("d-m-Y H:i:s", strtotime($data->dtModified));
                            $data->Remark = str_replace("\r\n", " ", $data->Remark);
                            $company = User::find($data->InternalID)->Company;
                            $arrData = array($data);
                            $tamp = myEscapeStringData($arrData);
                            $tamp = myEncryptJavaScriptText($tamp, $f);
                            ?>
                            <tr>
                                <td>{{$data->UserID}}</td>
                                <td>{{$data->UserName}}</td>
                                <td>{{$company->CompanyName}}</td>
                                <td>{{$data->Email}}</td>
                                <td>{{$data->Phone}}</td>
                                <td>{{$data->BankAccount. " " . $data->AccountNumber. " - ". $data->AccountName }}</td>
                                <td>
                                    @if($data->Status == 0)
                                    Not Active
                                    @else
                                    Active
                                    @endif
                                </td>
                                <td class="text-center">
                                    <button id="btn-{{$data->UserID}}" data-target="#m_agentUpdate" data-all='{{$tamp}}'
                                            data-toggle="modal" role="dialog" class="btn btn-pure-xs btn-xs btn-edit">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </button>
                                    <button data-target="#m_agentActive" data-internal="{{$data->InternalID}}"  data-toggle="modal" role="dialog"
                                            data-id="{{$data->UserID}}" class="btn btn-pure-xs btn-xs btn-active" @if($data->Status == 1) {{"disabled";}} @endif>
                                            <b> Active </b>
                                    </button>
                                    <button data-target="#m_agentNonActive" data-internal="{{$data->InternalID}}"  data-toggle="modal" role="dialog"
                                            data-id="{{$data->UserID}}" class="btn btn-pure-xs btn-xs btn-non-active" @if($data->Status == 0) {{"disabled";}} @endif>
                                            <b> Non Active </b>
                                    </button>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>    
            </div><!---end tableadd-->
        </div><!---- end div tabwrap---->
    </div><!---end primcontent-->
</div><!---end wrapjour-->
@stop

@section('modal')
<div class="modal fade bs-example-modal-lg " id="m_agent" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action" id="form-insert">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Insert Agent</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="margbot10">
                                    <input type='hidden' name='jenis' value='insertAgent'>
                                </div>
                                <div class="margbot10">
                                    <label for="AgentID">Agent ID *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="noSpecialCharacter input-theme" type="text" name="AgentID" id="agentID" maxlength="16" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="Name">Name *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" type="text" name="Name" id="name" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="Email">Email *</label> 
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" type="text" name="Email" id="email" maxlength="200" data-validation="email">
                                </div>
                                <div class="margbot10">
                                    <label for="Phone">Phone *</label> 
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" type="text" name="Phone" id="phone" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="BankAccount">Bank Account *</label> 
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="BankAccount" id="bankAccount" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="AccountNumber">Account Number *</label> 
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" type="text" name="AccountNumber" id="accountNumber" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="AccountName">Account Name *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" type="text" name="AccountName" id="accountName" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="Password">Password *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" type="password" name="Password_confirmation" id="password" maxlength="16" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="cPassword">Confirm Password *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" type="password" name="Password" id="cpassword" maxlength="16" data-validation="confirmation">
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea class="area-theme" style="resize:none;" name="remark" id="remark" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade bs-example-modal-lg" id="m_agentUpdate" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action" id="form-update">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Agent</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="margbot10">
                                    <input type="hidden" value="" id="idUpdate" name="InternalID">
                                    <input type="hidden" value="updateAgent" id="jenisUpdate" name="jenis">
                                </div>
                                <div class="margbot10">
                                    <label for="Name">Name *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" type="text" name="Name" id="nameUpdate" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="Email">Email *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" type="text" name="Email" id="emailUpdate" maxlength="200" data-validation="email">
                                </div>
                                <div class="margbot10">
                                    <label for="Phone">Phone *</label> 
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" type="text" name="Phone" id="phoneUpdate" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="BankAccount">Bank Account *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" type="text" name="BankAccount" id="bankAccountUpdate" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="AccountNumber">Account Number *</label> 
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" type="text" name="AccountNumber" id="accountNumberUpdate" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="AccountName">Account Name *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" type="text" name="AccountName" id="accountNameUpdate" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea class="area-theme" style="resize:none;"  name="remark" id="remarkUpdate" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <small>Created by <span id="createdDetail"></span></small><br>
                                    <small>Modified by <span id="modifiedDetail"></span></small>
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Update</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade" id="m_agentActive" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Active Agent</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="" id="idActive" name="InternalID">
                            <input type="hidden" value="activeAgent" id="jenisActive" name="jenis">
                            <p>Are you sure?</p>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Yes</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade" id="m_agentNonActive" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Non Active Agent</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="" id="idNonActive" name="InternalID">
                            <input type="hidden" value="nonActiveAgent" id="jenisNonActive" name="jenis">
                            <p>Are you sure?</p>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Yes</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
                </div>
            </form>      
        </div>
    </div>  
</div>
@stop
<?php
$u = myEncryptJavaScript(User::select('UserID')->get(), $f);
$e = myEncryptJavaScript(User::select('Email')->get(), $f);
?>
@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript">
var a = '<?php echo $u; ?>';
var c = '<?php echo $e; ?>';
var b = <?php echo $f; ?>
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-master/agent.js')}}"></script>
@stop
