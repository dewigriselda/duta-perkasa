@extends('template.header-footer')

@section('title')
Inventory Price
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<style>
    .help-block {
        margin-left: 30% !important;
    }
    .headinv .chosen-single{
        width: 200px !important;
    }
    .headinv .chosen-drop{
        width: 200px !important;
    }
    .li_detail{
        clear: both;
        padding-top: 10px;
    }
    .label_detail{
        float: left;
    }
    .div_detail{
        display: inline-block;
    }

    .listinput-group label{
        display: block;
        width: 93px;
        text-align: right;
    }

    #salesIDText{
        position: relative;
        top: -3px;
    }

    #btn-info-salesID{
        position: relative;
        top: -2px;
    }

    .span-info-tooltip-wrapper{
        display: inline-block;
    }
</style>
@stop

@section('nav')

@stop

@section('content')
@if(isset($messages))
@if($messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Inventory price has been updated.
</div>
@endif
@if($messages == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showInventoryUom')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Inventory Uom</a>
                <a href="{{route('showInventoryPrice')}}" type="button" class="btn btn-sm btn-pure">Inventory Price</a>
            </div>
            @if(checkModul('O01'))
            <a target="_blank" href="{{Route('exportInventoryUom')}}">
                <button type="button" class="btn btn-sm btn-green btn-export" id="btn-export">Export Excel</button>    
            </a>
            @endif
            <a href="{{Route('showInventoryPrice')}}">
                <button type="button" class="btn btn-sm btn-green" id="btn-price">Update Price</button>    
            </a>
        </div>
        <form method="POST" action="" id='form-inventoryPrice'>
            <input type='hidden' name='jenis' value='updateInventoryPrice'>
            <div class="tabwrap">
                <div class="tabhead">
                    <h4 class="headtitle">Inventory Price List</h4>
                </div>
                <div class="tableadd"> 
                    <div class="tableadd journupdate">
                        <table class="table master-data" id="table-inventory">
                            <thead>
                                <tr>
                                    <th width="15%">Inventory ID</th>
                                    <th width="10%">Name</th>
                                    <th width="5%">UOM</th>
                                    <th width="14%">Harga Beli</th>
                                    @foreach( DefaultPrice::where("CompanyInternalID", Auth::user()->Company->InternalID)->orderBy("InternalID", "ASC")->get() as $data)
                                    <th width="14%">{{$data->PriceName}}</th>
                                    @endforeach
                                </tr>
                            </thead>
                        </table>
                    </div><!---- end div tableadd---->      
                </div><!---- end div tableadd---->   
            </div><!---- end div tabwrap---->  
            <div class="btnnest pull-right">
                <button class="btn btn-green btn-sm btn-save" id="btn-save"> Save </button>
            </div>
        </form>
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNewJournal.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/tooltip.info.js')}}"></script>
<script>
var inventoryPriceDataBackup = '<?php echo Route('inventoryPriceDataBackup') ?>';
$(document).ready(function () {
    $('#table-inventory').dataTable({
            "draw": 10,
            "processing": true,
            "serverSide": true,
            "ajax": inventoryPriceDataBackup
    });
});
</script>
@stop