@extends('template.header-footer') <!--Load template atau view dasarnya pada view ini-->

@section('title')
Inventory Type
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
@stop

@section('nav')@stop

@section('content')
@if(isset($messages))
@if($messages == 'gagalInsert' || $messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif

@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New inventory type has been inserted.
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Inventory type has been updated.
</div>
@endif
@if($messages == 'suksesDelete')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Inventory type has been deleted.
</div>
@endif
@if($messages == 'gagalDelete')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Inventory type has been registered in table inventory.
</div>
@endif
@if($messages == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@endif


<div class="wrapjour">
    <div class="primcontent">

        <div class="btnnest">
            <div class="btn-group hidden-xs bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showInventoryType')}}" type="button" class="btn btn-sm btn-pure">Inventory Type</a>
            </div>
            @if(checkModul('O01'))
            <a target="_blank" href="{{Route('exportInventoryType')}}">
                <button type="button" class="btn btn-sm btn-green btn-export" id="btn-export">Export Excel</button>  
            </a>
            @endif
            <button type="button" class="btn btn-green btn-insert" data-target="#m_inventoryType" data-toggle="modal" role="dialog">
                <span class="glyphicon glyphicon-plus"></span> New</button>
        </div>

        <div class="bread-xs visible-xs hidden-sm hidden-md hidden-lg">
            <p class="text-center"><a href="{{route('showDashboard')}}">{{Config::get('companyHeader.header_company');}}</a> / <a>{{ucfirst($toogle)}}</a> / <a href="{{route('showInventoryType')}}">Inventory Type</a></p>
        </div>

        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Type Inventory</h4>
            </div>

            <div class="tableadd">
                <table id="example" class="display table-rwd table-inventory-type" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Inventory Type ID</th>
                            <th>Name</th>
                            <th>Account ID</th>
                            <th>Account Name</th>
                            <th>Record</th>
                            <th>Modified</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $f = rand(0, 50);
                        foreach (InventoryType::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                            $data->dtRecordformat = date("d-m-Y H:i:s", strtotime($data->dtRecord));
                            $data->dtModifformat = date("d-m-Y H:i:s", strtotime($data->dtModified));
                            $data->Remark = str_replace("\r\n", " ", $data->Remark);
                            $coa = InventoryType::coaName($data->InternalID);
                            $data->COAName = $coa[0]->COAName;
                            $data->coaID = $coa[0]->COAID;
                            $arrData = array($data);
                            $tamp = myEscapeStringData($arrData);
                            $tamp = myEncryptJavaScriptText($tamp, $f);
                            ?>
                            <tr>
                                <td>{{$data->InventoryTypeID}}</td>
                                <td>{{$data->InventoryTypeName}}</td>
                                <td>{{Coa::FormatCoa($data->ACC1InternalID, $data->ACC2InternalID, $data->ACC3InternalID, $data->ACC4InternalID, $data->ACC5InternalID, $data->ACC6InternalID, '1')}}</td>
                                <td>{{$data->COAName}}</td>
                                <td>{{$data->UserRecord." ".date( "d-m-Y H:i:s", strtotime($data->dtRecord))}}</td>
                                @if($data->UserModified == "0")
                                <td>-</td>
                                @else
                                <td>{{$data->UserModified." ".date( "d-m-Y H:i:s", strtotime($data->dtModified))}}</td>
                                @endif
                                <td class="text-center"><button id="btn-{{$data->InventoryTypeID}}" data-target="#m_inventoryTypeUpdate" data-all='{{$tamp}}'
                                                                data-toggle="modal" role="dialog"
                                                                class="btn btn-pure-xs btn-xs btn-edit">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </button>
                                    <button data-target="#m_inventoryTypeDelete" data-internal="{{$data->InternalID}}"  data-toggle="modal" role="dialog"
                                            data-id="{{$data->InventoryTypeID}}" data-name='{{$data->InventoryTypeName}}'  class="btn btn-pure-xs btn-xs btn-delete">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div><!---end tableadd-->
        </div><!---- end div tabwrap---->
    </div><!---end primcontent-->
</div><!---end wrapjour-->
@stop

@section('modal')

<div class="modal fade" id="m_inventoryType" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <form action="" method="post" class="action" id="form-insert">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Insert Inventory Type</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="margbot10">
                                    <input type='hidden' name='jenis' value='insertInventoryType'>
                                </div>
                                <div class="margbot10">
                                    <label for="InventoryTypeID">Inventory Type ID *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="noSpecialCharacter" autofocus="" type="text" name="InventoryTypeID" id="inventoryTypeID" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="InventoryTypeName">Name *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="InventoryTypeName" id="name" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="coa">Account *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal" id="coa" style="" name="coa">
                                        @foreach(Coa::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('ACC1InternalID')->orderBy('InternalID')->get() as $coa)
                                        <option class="coaInsert" id="coainsert{{$coa->InternalID.'------'.$coa->Persediaan}}" value="{{$coa->InternalID}}">
                                            {{Coa::FormatCoa($coa->ACC1InternalID, $coa->ACC2InternalID, $coa->ACC3InternalID, $coa->ACC4InternalID, $coa->ACC5InternalID, $coa->ACC6InternalID, '1')}}
                                            {{" ". $coa->COAName }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="Type">Type *</label>
                                </div>
                                <div class="margbot10">
                                    <div class="radio-inline">
                                        <input type="radio" class="radio-tipe radioInsert" id="tipeInventory" name="Type" value="1" checked="checked"><label for="tipeInventory">Inventory</label>
                                    </div>
                                    <div class="radio-inline">
                                        <input type="radio" class="radio-tipe radioInsert" id="tipeCost" name="Type" value="2"><label for="tipeCost">Cost</label>
                                    </div>
                                    <div class="radio-inline">
                                        <input type="radio" class="radio-tipe radioInsert" id="tipeService" name="Type" value="3"><label for="tipeService">Service</label>
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;"  name="remark" id="remark" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>


<div class="modal fade nopaddr" id="m_inventoryTypeUpdate" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <form action="" method="post" class="action" id="form-update">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Inventory Type</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="margbot10">
                                    <input type="hidden" value="" id="idUpdate" name="InternalID">
                                    <input type="hidden" value="updateInventoryType" id="jenisUpdate" name="jenis">
                                </div>
                                <div class="margbot10">
                                    <label for="InventoryTypeName">Name *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="InventoryTypeName" autofocus="" id="nameUpdate" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="Type">Type *</label>
                                </div>
                                <div class="margbot10">
                                    <div class="radio-inline">
                                        <input type="radio" class="radio-tipe radioUpdate" id="tipeInventoryUpdate" name="Type" value="1"><label for="tipeInventoryUpdate">Inventory</label>
                                    </div>
                                    <div class="radio-inline">
                                        <input type="radio" class="radio-tipe radioUpdate" id="tipeCostUpdate" name="Type" value="2"><label for="tipeCostUpdate">Cost</label>
                                    </div>
                                    <div class="radio-inline">
                                        <input type="radio" class="radio-tipe radioUpdate" id="tipeServiceUpdate" name="Type" value="3"><label for="tipeServiceUpdate">Service</label>
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="coaUpdate">Account *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal" id="coaUpdate" style="" name="coa">
                                        @foreach(Coa::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('ACC1InternalID')->orderBy('InternalID')->get() as $coa)
                                        <option class="coaUpdate" id="coa{{$coa->InternalID.'------'.$coa->Persediaan}}" value="{{$coa->InternalID}}">
                                            {{Coa::FormatCoa($coa->ACC1InternalID, $coa->ACC2InternalID, $coa->ACC3InternalID, $coa->ACC4InternalID, $coa->ACC5InternalID, $coa->ACC6InternalID, '1')}}
                                            {{" ". $coa->COAName }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;" name="remark" id="remarkUpdate" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <small>Created by <span id="createdDetail"></span></small><br>
                                    <small>Modified by <span id="modifiedDetail"></span></small>
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Update</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade nopaddr" id="m_inventoryTypeDelete" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <form action="" method="post" class="action">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Delete Inventory Type</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="" id="idDelete" name="InternalID">
                            <input type="hidden" value="deleteInventoryType" id="jenisDelete" name="jenis">
                            <p>Are you sure want to delete <span id="deleteName"></span>?</p>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Yes</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
                </div>
            </form>      
        </div>
    </div>  
</div>
@stop
<?php
$i = myEncryptJavaScript(InventoryType::select('InventoryTypeID')->where('CompanyInternalID', Auth::user()->Company->InternalID)->get(), $f);
?>

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/chosenNoHide.jquery.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript">
var a = '<?php echo $i; ?>';
var b = <?php echo $f; ?>;
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-master/inventoryType.js')}}"></script>
@stop