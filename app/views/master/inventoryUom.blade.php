@extends('template.header-footer')

@section('title')
Inventory Uom
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
@stop

@section('nav')

@stop

@section('content')
@if(myCheckIsEmpty('Inventory;Uom'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one Inventory and Uom to insert Inventory Uom.
</div>
@endif
@if(isset($messages))
@if($messages == 'gagalInsert' || $messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New inventory uom has been inserted.
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Inventory uom has been updated.
</div>
@endif
@if($messages == 'suksesDelete')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Inventory uom has been deleted.
</div>
@endif
@if($messages == 'gagalDelete')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Inventory uom has been registered in other tables.
</div>
@endif
@if($messages == 'sudahAda')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Inventory uom is already registered before.
</div>
@endif
@if($messages == 'gagalDefault')
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Inventory uom is uom default, can't be made into non default.
</div>
@endif
@if($messages == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@endif
@if(isset($errmessages))
@if($errmessages != 0)
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Inventory {{Inventory::find($inventoryerr)->InventoryName}} with UOM {{Uom::find($uomerr)->UomID}} is Exist
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showInventoryUom')}}" type="button" class="btn btn-sm btn-pure">Inventory Uom</a>
            </div>
            @if(checkModul('O01'))
            <a target="_blank" href="{{Route('exportInventoryUom')}}">
                <button type="button" class="btn btn-sm btn-green btn-export" id="btn-export">Export Excel</button>    
            </a>
            @endif
            <a href="{{Route('showInventoryPrice')}}" {{(myCheckIsEmpty('Inventory;Uom')) ? "disabled" : ""}}>
                <button type="button" class="btn btn-sm btn-green" id="btn-price">Update Price</button>    
            </a>
<!--            <a href="{{Route('showInventoryMarketPrice')}}" {{(myCheckIsEmpty('Inventory;Uom')) ? "disabled" : ""}}>
                <button type="button" class="btn btn-sm btn-green" id="btn-price">Update Market Price</button>    
            </a>-->
            <button type="button" class="btn btn-green btn-insert margr5" {{(myCheckIsEmpty('Inventory;Uom')) ? "disabled" : ""}} data-target="#m_priceinventoryUom" data-toggle="modal" role="dialog">
               Import Price</button>
            <button type="button" class="btn btn-green btn-insert margr5" {{(myCheckIsEmpty('Inventory;Uom')) ? "disabled" : ""}} data-target="#m_inventoryUom" data-toggle="modal" role="dialog">
                <span class="glyphicon glyphicon-plus"></span> New</button>
            <a href="{{Route('reportHPP')}}" class="nomarg" target="_blank">
                <button type="button" class="btn btn-sm btn-green" id="btn-hpp">Report HPP</button>    
            </a>
        </div>
        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Inventory Uom</h4>
            </div>

            <div class="tableadd">
                <table id="example" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Inventory ID</th>
                            <th>Name</th>
                            <th>Uom</th>
                            <th>Default</th>
                            <th>Value</th>
                            <th>Harga Beli</th>
                            @foreach( DefaultPrice::where("CompanyInternalID", Auth::user()->Company->InternalID)->orderBy("InternalID", "ASC")->get() as $data)
                            <th>{{$data->PriceName}}</th>
                            @endforeach
                            <th>Actions</th>
                        </tr>
                    </thead>
                </table>

            </div><!---end tableadd-->
        </div><!---- end div tabwrap---->
    </div><!---end primcontent-->
</div><!---end wrapjour-->


@stop

@section('modal')
<div class="modal fade bs-example-modal-lg" id="m_inventoryUom" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insert Inventory Uom</h4>
            </div>
            <form action="" method="post" class="action" id="form-insert">
                <div class="modal-body">
                    <div class="coa-form">
                        <div class="row">
                            <div class="col-xs-12">
                                <input type='hidden' name='jenis' value='insertInventoryUom'>
                                <div class="margbot10">
                                    <label for="Default">Default</label> *
                                </div>
                                <div class="margbot10">
                                    <div class="radio-inline">
                                        <input type="radio" autofocus="" class="radio-tipe" id="tipeNoDefault" name="Default" value="0" checked="checked">Not default
                                    </div>
                                    <div class="radio-inline">
                                        <input type="radio" class="radio-tipe" id="tipeDefault" name="Default" value="1">Default
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="Inventory">Inventory *</label>
                                </div>
                                <input class="input-theme margbot10" type="text" id="searchInventory" title="Type Inventory Name or ID then 'Enter'" placeholder="Type Inventory Name or ID then 'Enter'">
                                <div class="margbot10" id="selectInventory">

                                </div>
                                <div class="margbot10">
                                    <label for="Uom">Uom</label> *
                                </div>
                                <div class="margbot10">
                                    <select id="uomInternalID" class="input-theme" name="UomInternalID">
                                        @foreach(Uom::where("CompanyInternalID", Auth::user()->Company->InternalID)->get() as $data);
                                        <option value="{{$data->InternalID}}">{{$data->UomID}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="Price">Harga Beli</label> 
                                </div>
                                <div class="margbot10">
                                    <input type="text" class="numajaDesimal price" value="0" placeholder="Purchase Price" name="purchasePrice">
                                </div>
                                @foreach( DefaultPrice::where("CompanyInternalID", Auth::user()->Company->InternalID)->orderBy("InternalID", "ASC")->get() as $data)
                                <div class="margbot10">
                                    <label for="Price">{{$data->PriceName}}</label> 
                                </div>
                                <div class="margbot10">
                                    <input type="text" class="numajaDesimal price" value="0" placeholder="Price for {{$data->PriceName}}" name="{{$data->PriceID}}" id="{{lcfirst($data->PriceID)}}">
                                </div>
                                @endforeach
                                <div class="margbot10">
                                    <label for="Value">Value</label> *
                                </div>
                                <div class="margbot10">
                                    <input type="text" class="numaja" id="value" name="Value" maxlength="" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks</label> *
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;"  name="remark" id="remark" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="col-xs-12">
                                    <input type="hidden" id="divError">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="required">
                        * Required
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green" id="btn-submit">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade bs-example-modal-lg" id="m_priceinventoryUom" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Import Price</h4>
            </div>
            <form action="" method="post" class="action" id="form-insert" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="coa-form">
                        <div class="row">
                            <div class="col-xs-12">
                                <input type='hidden' name='jenis' value='importUpdatePrice'>
                                <div class="margbot10">
                                    <label for="Value">Import File</label> *
                                </div>
                                <div class="margbot10">
                                    <input type="file" class="numaja" name="file" maxlength="" data-validation="required">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="required">
                        * Required
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green" id="btn-submit">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>


<div class="modal fade" id="m_inventoryUomUpdate" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Update Inventory Uom</h4>
            </div>
            <form action="" method="post" class="action" id="form-update">
                <div class="modal-body">
                    <div class="coa-form">
                        <div class="row">
                            <div class="col-xs-12">
                                <input type="hidden" value="" id="idUpdate" name="InternalID">
                                <input type="hidden" value="updateInventoryUom" id="jenisUpdate" name="jenis">
                                <div class="margbot10">
                                    <label for="Default">Default</label> *
                                </div>
                                <div class="margbot10">
                                    <div class="radio-inline">
                                        <input type="radio" class="radio-tipe" id="tipeNoDefaultUpdate" name="Default" value="0" checked="checked">Not default
                                    </div>
                                    <div class="radio-inline">
                                        <input type="radio" class="radio-tipe" id="tipeDefaultUpdate" name="Default" value="1">Default
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="Inventory">Inventory</label> *
                                </div>
                                <div class="margbot10">
                                    <span id="updateName"></span>
                                </div>
                                <div class="margbot10">
                                    <label for="Price">Harga Beli</label> 
                                </div>
                                <div class="margbot10">
                                    <input type="text" class="numajaDesimal price" value="0" placeholder="Purchase Price" name="purchasePriceUpdate" id="purchasePriceUpdate">
                                </div>
                                @foreach( DefaultPrice::where("CompanyInternalID", Auth::user()->Company->InternalID)->orderBy("InternalID", "ASC")->get() as $data)
                                <div class="margbot10">
                                    <label for="Price">{{$data->PriceName}}</label> 
                                </div>
                                <div class="margbot10">
                                    <input type="text" class="numajaDesimal price" value="0" placeholder="Price for {{$data->PriceName}}" name="{{$data->PriceID}}" id="{{lcfirst($data->PriceID)}}Update">
                                </div>
                                @endforeach
                                <div class="margbot10">
                                    <label for="Value">Value</label> *
                                </div>
                                <div class="margbot10">
                                    <input type="text" class="numaja" id="valueUpdate" name="Value" maxlength="" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks</label> *
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;" name="remark" id="remarkUpdate" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <small>Created by <span id="createdDetail"></span></small><br>
                                    <small>Modified by <span id="modifiedDetail"></span></small>
                                </div>
                                <div class="col-xs-12">
                                    <input type="hidden" id="divErrorUpdate">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="required">
                        * Required
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green" id="btn-update">Update</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade" id="m_inventoryUomDelete" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Delete Inventory Uom</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="margbot10">
                                    <input type="hidden" value="" id="idDelete" name="InternalID">
                                    <input type="hidden" value="deleteInventoryUom" id="jenisDelete" name="jenis">
                                    <p>Are you sure want to delete <span id="deleteName"></span>?</p>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-green">Yes</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
            </div>
            </form>      
        </div>
    </div>  
</div>
@stop
@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/chosenNoHide.jquery.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script>
var inventoryUomDataBackup = '<?php echo Route('inventoryUomDataBackup') ?>';
var checkInvUom = '<?php echo Route('checkInvUom') ?>';
var checkInvUomUpdate = '<?php echo Route('checkInvUomUpdate') ?>';
var getResultSearch = '<?php echo Route('getResultSearchInventory') ?>';
var cekgantivalue = '<?php echo Route('cekGantiValue') ?>';
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-master/inventoryUom.js')}}"></script>
@stop
