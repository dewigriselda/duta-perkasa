@extends('template.header-footer')

@section('title')
Inventory
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/master/inventoryUpdate.css')}}">
@stop

@section('nav')

@stop

@section('content')
@if(Session::get("messages") == "suksesPushData")
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Success push to online.
</div>
@endif
@if(Session::get("messages") == "gagalPushData")
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Please check your connection.
</div>
@endif
@if(myCheckIsEmpty('InventoryType;Variety;Brand;Uom'))
<!--<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one Inventory Type, Variety, Brand and Uom to insert Inventory.
</div>-->
@endif
@if(isset($messages))
@if($messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($errors))
    <ul style="margin-left: 2%">
        @foreach($errors->all('<li>:message</li>') as $messages)
        {{$messages}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Inventory has been updated.
</div>
@endif
@if($messages == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">

        <div class="btnnest option-slip">
            <div class="btn-group bread hidden-xs" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showInventory')}}" type="button" class="btn btn-sm btn-pure">Inventory</a>
            </div>
            @if(checkModul('O01'))
            <a target="_blank" href="{{Route('exportInventory')}}">
                <button type="button" class="btn btn-sm btn-green btn-export" id="btn-export">Export Excel</button>  
            </a>
            @endif
            <a href="{{Route("showInventoryNew")}}" type="button" <?php if (myCheckIsEmpty('InventoryType')) echo 'disabled' ?>  class="btn btn-green btn-insert">
                <span class="glyphicon glyphicon-plus"></span> New</a>
            @if(checkModul('O04'))
            <div class="btn-group">
                <button type="button" class="btn btn-green dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="glyphicon glyphicon-file"></span> Report <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a data-target="#r_detail" data-toggle="modal" role="dialog" href="#">Detail Report</a></li>
                    <li><a data-target="#r_summary" data-toggle="modal" role="dialog" href="#">Summary Report</a></li>
                    <li><a data-target="#r_bufferStock" data-toggle="modal" role="dialog" href="#">Buffer Stock Report</a></li>  
                </ul>
            </div>
            @endif
        </div>

        <div class="bread-xs visible-xs hidden-sm hidden-md hidden-lg">
            <p class="text-center"><a href="{{route('showDashboard')}}">{{Config::get('companyHeader.header_company');}}</a> / <a>{{ucfirst($toogle)}}</a> / <a href="{{route('showInventory')}}">Inventory</a></p>
        </div>

        <form method="POST" id="form-update" enctype="multipart/form-data">
            <input type="hidden" name="jenis" value="updateInventory">
            <input type="hidden" name="InventoryID"  value="{{$inventory->InventoryID}}">
            <input type="hidden" id="id-update" value="{{$inventory->InternalID}}">
            <div class="tabwrap">
                <div class="tabhead">
                    <h4 class="headtitle">Update Inventory</h4>
                </div>
                <div class="tableadd" style="padding-top: 40px;">

                    <div class="margbot20 ">
                        <div class="clearfix">
                            <div class="margbot10 col-xs-2 text-right">
                                <label for="InventoryName">Name</label> *
                            </div>
                            <div class="margbot10 col-xs-8">
                                <input class="input-theme" type="text" name="InventoryName" id="name" value="{{$inventory->InventoryName}}" maxlength="200" data-validation="required">
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="margbot10 col-xs-2 text-right">
                            <label for="InventoryTextPrint">Text Print</label> *
                        </div>
                        <div class="margbot10 col-xs-8">
                            <input class="input-theme" type="text" name="TextPrint" id="textPrint" maxlength="200" value="{{$inventory->TextPrint}}" data-validation="required">
                        </div>
                    </div>
                    <hr>
                    <div class="margbot20">
                        <ul class="nav nav-tabs " id="tab-inventory" role="tablist">
                            <li role="presentation" class="active"><a href="#basic" aria-controls="home" role="tab" data-toggle="tab">Basic</a></li>
                            <li role="presentation"><a href="#detail" aria-controls="profile" role="tab" data-toggle="tab">Detail</a></li>
                        </ul>
                    </div>
                    <br>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="basic">
                            <div class="col-sm-5 left-inventory">

                                <div class="margbot10">
                                    <label for="InventoryType">Type</label> *
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal" id="type" style="" name="Type">
                                        @foreach(InventoryType::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('InternalID')->get() as $InventoryType)
                                        <option {{($inventory->InventoryTypeInternalID == $InventoryType->InternalID ? "selected":"" )}} value="{{$InventoryType->InternalID}}">
                                            {{$InventoryType->InventoryTypeName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="InventoryVariety">Variety</label> *
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal" id="varietyInternalID" style="" name="VarietyInternalID">
                                        @foreach(Variety::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('InternalID')->get() as $data)
                                        <option {{($inventory->VarietyInternalID == $data->InternalID ? "selected":"" )}} value="{{$data->InternalID}}">
                                            {{$data->Group->GroupName}} - {{$data->VarietyName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="InventoryBrand">Brand</label> *
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal" id="brandInternalID" style="" name="BrandInternalID">
                                        @foreach(Brand::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('InternalID')->get() as $data)
                                        <option {{($inventory->BrandInternalID == $data->InternalID ? "selected":"" )}} value="{{$data->InternalID}}">
                                            {{$data->BrandName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label >Type Detail 1</label> *
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal input-theme" name="TypeDetailInternalID" id="typeDetailInternalID">
                                        @foreach(TypeDetail::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('InternalID')->get() as $data)
                                        <option value="{{$data->InternalID}}" {{($data->InternalID == $inventory->TypeDetailInternalID ? 'selected':'')}}>
                                            {{$data->TypeDetailName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label >Type Detail 2</label>
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" type="text" name="TypeDetail2" id="typeDetail2" value="{{$inventory->TypeDetail2}}" maxlength="200">
                                </div>
                                <div class="margbot10">
                                    <label for="bufferStock">Buffer Stock</label> *
                                </div>
                                <div class="margbot10">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <span for="bufferStock">Max</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" class="input-theme numaja price text-right" name="MaxStock" id="max" value="{{number_format($inventory->MaxStock, 2, ".", ",")}}" data-validation="required">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <span for="bufferStock">Min</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" class="input-theme numaja price text-right" name="MinStock" id="min" value="{{number_format($inventory->MinStock, 2, ".", ",")}}" data-validation="required">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-7  right-inventory">
                                
                                <div class="margbot10">
                                    <label for="BarcodeCode">Barcode</label> *
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" type="text" name="BarcodeCode" id="barcodeCode" value="{{$inventory->BarcodeCode}}" maxlength="200" data-validation="required">
                                </div>
                                
                                <div class="margbot10">
                                    <label for="Commission">Commission (%)</label> *
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme numajaDesimal" type="text" name="Commission" id="commission" value="{{$inventory->Commission}}" maxlength="5" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks</label> *
                                </div>
                                <div class="margbot10">
                                    <textarea class="area-theme" style="resize:none;"  name="Remark" id="remark" maxlength="1000" data-validation="required">{{$inventory->Remark}}</textarea>
                                </div>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane" id="detail">
                            <div class="col-sm-5 left-inventory">
                                <div class="margbot10">
                                    <label>Ordering Name</label>
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" type="text" name="OrderingName" id="orderingName" value="{{$inventory->OrderingName}}" maxlength="200">
                                </div>
                                <div class="margbot10">
                                    <label>Alias</label>
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" type="text" name="Alias" id="alias" value="{{$inventory->Alias}}" maxlength="200">
                                </div>
                                <div class="margbot10">
                                    <label>Similarity</label>
                                </div>
                                <div class="margbot10">
                                    <a data-target="#add_inventory" data-toggle="modal" role="dialog" href="#">
                                        <button type="button" class="btn btn-green btn-sm" id="btn-addRow-inventory"><span class="glyphicon glyphicon-plus"></span> Add Inventory </button>
                                    </a>

                                    <div class="modal fade" id="add_inventory" role="dialog">
                                        <div class="modal-dialog modal-mid">
                                            <div class="modal-content modal-mid">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" >Add Inventory</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="coa-form">
                                                        <table class="table master-data" id="table-inventory" style="table-layout:fixed;">
                                                            <thead>
                                                                <tr>
                                                                    <th style="width: 75%;">Inventory</th>
                                                                    <th style="width: 25%;">Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr id="row0" style="background-color: #e5e5e5 !important">
                                                                    <td class="chosen-transaction" style="border-color: #d8d8d8 !important">
                                                                        <input class="input-theme margbot10" type="text" id="searchInventory" title="Type Inventory Name or ID then 'Enter'" placeholder="Type Inventory Name or ID then 'Enter'">
                                                                        <div id="selectInventory">

                                                                        </div>
                                                                    </td>
                                                                    <td style="border-color: #d8d8d8 !important">
                                                                        <button type="button" class="btn btn-green btn-sm" disabled="" id="btn-addRow"><span class="glyphicon glyphicon-plus"></span></button>
                                                                    </td>
                                                                </tr>
                                                                {{'';$baris = 0;}}
                                                                @foreach(InventorySimilarity::where("InventoryInternalID", $inventory->InternalID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->get() as $data)
                                                                <tr id="row{{$baris}}">
                                                                    <td class="chosen-uom">
                                                                        <input type="hidden" class="inventory" style="width: 100px" id="inventory-{{$baris}} baris" style="" name="inventory[]" value="{{$data->Inventory->InternalID}}">
                                                                        {{$data->Inventory->InventoryID}} {{$data->Inventory->InventoryName}}
                                                                    </td>
                                                                    <td><button class="btn btn-pure-xs btn-xs btn-deleteRow" type="button" data="row{{$baris}}"><span class="glyphicon glyphicon-trash"></span></button></td>
                                                                </tr>
                                                                {{'';$baris++;}}
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>  
                                    </div>
                                </div>

                                <div class="margbot10">
                                    <label for="Dimension">Packaging Dimension</label>
                                </div>
                                <div class="margbot10">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-md-12 margbot10">
                                                    <span for="Width">Width</span>
                                                </div>
                                                <div class="col-md-12">
                                                    <input type="text" class="input-theme numajaDesimal text-right" value="{{number_format($inventory->Width, 2, ".", ",")}}" name="Width">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-md-12 margbot10">
                                                    <span for="Height">Height</span>
                                                </div>
                                                <div class="col-md-12">
                                                    <input type="text" class="input-theme numajaDesimal text-right" value="{{number_format($inventory->Height, 2, ".", ",")}}" name="Height">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-md-12 margbot10">
                                                    <span for="Length">Length</span>
                                                </div>
                                                <div class="col-md-12">
                                                    <input type="text" class="input-theme numajaDesimal text-right" value="{{number_format($inventory->Length, 2, ".", ",")}}" name="Length">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="Weigth">Weigth</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" class="input-theme numajaDesimal text-right" name="Weigth" id="weigth" value="{{$inventory->Weight}}">
                                </div>
                                <div class="margbot10">
                                    <label >Finishing</label>
                                </div>
                                <div class="margbot10">
                                    <select class="input-theme chosen-select" name="FinishingInternalID" id="finishingInternalID" type="text">
                                        @foreach(Finishing::where("CompanyInternalID", Auth::user()->Company->InternalID)->get() as $data)
                                        <option {{($inventory->FinishingInternalID == $data->InternalID ? "selected":"" )}} value="{{$data->InternalID}}">
                                            {{$data->FinishingID}} - {{$data->FinishingName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label >Material</label>
                                </div>
                                <div class="margbot10">
                                    <select class="input-theme chosen-select" name="MaterialInternalID" id="materialInternalID" type="text">
                                        @foreach(Material::where("CompanyInternalID", Auth::user()->Company->InternalID)->get() as $data)
                                        <option {{($inventory->MaterialInternalID == $data->InternalID ? "selected":"" )}} value="{{$data->InternalID}}">
                                            {{$data->MaterialID}} - {{$data->MaterialName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-7  right-inventory">
                                <div class="margbot10">
                                    <label>Price Range</label>
                                </div>
                                <div class="margbot10">
                                    <table class="table  table-striped table-responsive">
                                        <thead class="thead-boots">
                                            <tr>
                                                <th width="65%" colspan="2" class="text-center">Qty</th>
                                                <th  width="35%">Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td width="20%"  class="text-right">Big :</td>
                                                <td width="45%">
                                                    <input type="text" class="input-theme text-right" name="BigQty" value="{{number_format($inventory->BigQty, 2, ".", ",")}}">
                                                </td>
                                                <td width="35%"><input name="BigValue" class="input-theme numaja price text-right" id="bigQty" type="text" value="{{number_format($inventory->BigValue, 2, ".", ",")}}"/></td>
                                            </tr>
                                            <tr>
                                                <td width="20%"  class="text-right">Medium :</td>
                                                <td width="45%">
                                                    <input type="text" class="input-theme text-right" name="MediumQty" value="{{number_format($inventory->MediumQty, 2, ".", ",")}}">
                                                </td>
                                                <td width="35%"><input name="MediumValue" class="input-theme numaja price text-right" id="mediumValue" type="text" value="{{number_format($inventory->MediumValue, 2, ".", ",")}}"/></td>
                                            </tr>
                                            <tr>
                                                <td width="20%"  class="text-right">Small :</td>
                                                <td width="45%">
                                                    <input type="text" class="input-theme text-right" name="SmallQty" value="{{number_format($inventory->SmallQty, 2, ".", ",")}}" data-validation="required">
                                                </td>
                                                <td id="default-input" width="35%"><input name="SmallValue" class="input-theme numaja price text-right" id="smallValue" type="text" data-validation="required" value="{{number_format($inventory->SmallValue, 2, ".", ",")}}"/></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="margbot10">
                                    <label >Origin</label>
                                </div>
                                <div class="margbot10">
                                    <select class="input-theme chosen-select" name="OriginInternalID" type="text">
                                        @foreach(Origin::where("CompanyInternalID", Auth::user()->Company->InternalID)->get() as $data)
                                        <option {{($inventory->OriginInternalID == $data->InternalID ? "selected":"" )}} value="{{$data->InternalID}}">
                                            {{$data->OriginID}} - {{$data->OriginName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label >Link</label>
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" id="link" name="Link" value="{{$inventory->Link}}" type="text" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label >Location</label>
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" id="location" name="Location" value="{{$inventory->Location}}" type="text" maxlength="200">
                                </div>
                                <div class="margbot10">
                                    <label>Picture</label>
                                </div>
                                <div class="margbot10">
                                    @if($inventory->Picture != '')
                                    <img src="{{Asset($inventory->Picture)}}" class="margbot10" width="100px" height="100px">
                                    @endif
                                    <input type="file" name="Picture" id="picture">
                                </div>
                                <div class="margbot10">
                                    <label >Reorder Time (Days)</label>
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme numaja text-right" name="ReorderTime" value="{{$inventory->ReorderTime}}" id="reorderTime" type="text" maxlength="200">
                                </div>
                                <div class="margbot10">
                                    <label >Availability</label>
                                </div>
                                <div class="margbot10">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <input name="Availability" type="radio" {{($inventory->Availability == 1 ? "checked" : "")}} id="input-available" checked="" value="1">
                                            <label for="input-available">Available</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input name="Availability" type="radio" {{($inventory->Availability == 0 ? "checked" : "")}} id="input-notavailable" value="0">
                                            <label for="input-notavailable">Not Available</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!---end tableadd-->
            </div><!---- end div tabwrap---->
            <div class="clearfix">
                <div class="btnnest pull-right">
                    <button type="submit" class="btn btn-green btn-sm btn-save">Save</button>
                </div>
            </div>
        </form>  
    </div><!---end primcontent-->
</div><!---end wrapjour-->

@stop

@section('modal')
<div class="modal fade" id="r_detail" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action" id=""  target="_blank">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detail Report</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="margbot10">
                                    <input type='hidden' name='jenis' value='detailInventory'>
                                </div>
                                <div class="margbot10">
                                    <label for="warehouse">Warehouse *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal" id="warehouse" style="" name="warehouse">
                                        <option value="-1"> All </option>
                                        @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $warehouse)
                                        <option value="{{$warehouse->InternalID}}">
                                            {{$warehouse->WarehouseName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="sDate">Start Date *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="sDate" id="startDate" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="eDate">End Date *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="eDate" id="endDate" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="no">No. *</label>
                                </div>
                                <div class="margbot10">
                                    <select name="ValueTake" class="input-theme">
                                        <?php
                                        $pembagi = 1000;
                                        $countInventory = Inventory::where("CompanyInternalID", Auth::user()->Company->InternalID)->count();
                                        $loop = intval($countInventory / $pembagi);
                                        for ($i = 1; $i <= $loop; $i++) {
                                            ?>
                                            <option value="<?php echo ($pembagi * ($i - 1)) ?>"> <?php echo ($pembagi * ($i - 1)) + 1 . '-' . ($pembagi * $i) ?></option>
                                            <?php
                                        }
                                        if ($countInventory != $loop * $pembagi) {
                                            ?>
                                            <option value="<?php echo ($pembagi * $loop) ?>"> <?php echo ($loop * $pembagi) + 1 . '-' . ($countInventory) ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                    <button type="submit" id="btn-detail-report" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade" id="r_summary" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action" id="" target="_blank">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Summary Report</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="margbot10">
                                    <input type='hidden' name='jenis' value='summaryInventory'>
                                </div>
                                <div class="margbot10">
                                    <label for="warehouse">Warehouse *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal" id="warehouse" style="" name="warehouse">
                                        <option value="-1"> All </option>
                                        @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $warehouse)
                                        <option value="{{$warehouse->InternalID}}">
                                            {{$warehouse->WarehouseName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="date">Date *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="date" id="date" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="no">No. *</label>
                                </div>
                                <div class="margbot10">
                                    <select name="ValueTake" class="input-theme">
                                        <?php
                                        $pembagi = 1000;
                                        $countInventory = Inventory::where("CompanyInternalID", Auth::user()->Company->InternalID)->count();
                                        $loop = intval($countInventory / $pembagi);
                                        for ($i = 1; $i <= $loop; $i++) {
                                            ?>
                                            <option value="<?php echo ($pembagi * ($i - 1)) ?>"> <?php echo ($pembagi * ($i - 1)) + 1 . '-' . ($pembagi * $i) ?></option>
                                            <?php
                                        }
                                        if ($countInventory != $loop * $pembagi) {
                                            ?>
                                            <option value="<?php echo ($pembagi * $loop) ?>"> <?php echo ($loop * $pembagi) + 1 . '-' . ($countInventory) ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                    <button type="submit" id="btn-summary-report" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade" id="r_stock" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action" id="" target="_blank">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Stock Report</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <div class="row">
                            <div class="col-xs-12">
                                <input type="hidden" value="" id="idStock" name="InternalID">
                                <div class="margbot10">
                                    <input type='hidden' name='jenis' value='stockInventory'>
                                </div>
                                <div class="margbot10">
                                    <label for="warehouse">Warehouse *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal" id="warehouse" style="" name="warehouse">
                                        <option value="-1"> All </option>
                                        @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $warehouse)
                                        <option value="{{$warehouse->InternalID}}">
                                            {{$warehouse->WarehouseName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="sDate">Start Date *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="sDate" id="startDateStock" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="eDate">End Date *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="eDate" id="endDateStock" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="no">No. *</label>
                                </div>
                                <div class="margbot10">
                                    <select name="ValueTake" class="input-theme">
                                        <?php
                                        $pembagi = 1000;
                                        $countInventory = Inventory::where("CompanyInternalID", Auth::user()->Company->InternalID)->count();
                                        $loop = intval($countInventory / $pembagi);
                                        for ($i = 1; $i <= $loop; $i++) {
                                            ?>
                                            <option value="<?php echo ($pembagi * ($i - 1)) ?>"> <?php echo ($pembagi * ($i - 1)) + 1 . '-' . ($pembagi * $i) ?></option>
                                            <?php
                                        }
                                        if ($countInventory != $loop * $pembagi) {
                                            ?>
                                            <option value="<?php echo ($pembagi * $loop) ?>"> <?php echo ($loop * $pembagi) + 1 . '-' . ($countInventory) ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                    <button type="submit" id="btn-stock-report" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>
<div class="modal fade" id="r_bufferStock" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action" id="" target="_blank">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Report Buffer Stock</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="margbot10">
                                    <input type='hidden' name='jenis' value='reportStock'>
                                </div>
                                <div class="margbot10">
                                    <label for="warehouse">Warehouse *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal" id="warehouse" style="" name="warehouse">
                                        <option value="-1"> All </option>
                                        @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $warehouse)
                                        <option value="{{$warehouse->InternalID}}">
                                            {{$warehouse->WarehouseName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="date">Date *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="date" id="date1" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="no">No. *</label>
                                </div>
                                <div class="margbot10">
                                    <select name="ValueTake" class="input-theme">
                                        <?php
                                        $pembagi = 1000;
                                        $countInventory = Inventory::where("CompanyInternalID", Auth::user()->Company->InternalID)->count();
                                        $loop = intval($countInventory / $pembagi);
                                        for ($i = 1; $i <= $loop; $i++) {
                                            ?>
                                            <option value="<?php echo ($pembagi * ($i - 1)) ?>"> <?php echo ($pembagi * ($i - 1)) + 1 . '-' . ($pembagi * $i) ?></option>
                                            <?php
                                        }
                                        if ($countInventory != $loop * $pembagi) {
                                            ?>
                                            <option value="<?php echo ($pembagi * $loop) ?>"> <?php echo ($loop * $pembagi) + 1 . '-' . ($countInventory) ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                    <button type="submit" id="btn-buffer-stock-report" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>
@stop
@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/chosenNoHide.jquery.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script>
var getNameInventory = "<?php echo Route("getNameInventory") ?>";
var getSearchResultInventorySimilarity = "<?php echo Route("getSearchResultInventorySimilarity") ?>";
var checkBarcodeCodeInventory = "<?php echo Route("checkBarcodeCodeInventory") ?>";
var getPrintText = "<?php echo Route("getPrintText") ?>";
var baris = <?php echo $baris; ?>;
var inventoryDataBackup = '<?php echo Route('inventoryDataBackup') ?>';
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-master/inventoryUpdate.js')}}"></script>
@stop
