@extends('template.header-footer')

@section('title')
Notes
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
@stop

@section('nav')

@stop

@section('content')
@if(isset($messages))
@if($messages == 'gagalInsert' || $messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New notes has been inserted.
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Notes has been updated.
</div>
@endif
@if($messages == 'suksesDelete')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Notes has been deleted.
</div>
@endif
@if($messages == 'gagalDelete')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Notes has been create transaction.
</div>
@endif
@if($messages == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@endif
@if(Session::get('messages') == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread hidden-xs" role="group">
                <a href="{{route('showDashboard')}}" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showNotes')}}" class="btn btn-sm btn-pure">Notes</a>
            </div>
            @if(checkModul('O01') || Auth::user()->CompanyInternalID == -1)
            <a target="_blank" href="{{Route('exportNotes')}}">
                <button type="button" class="btn btn-sm btn-green btn-export" id="btn-export">Export Excel</button>    
            </a>
            @endif
            <button type="button" class="btn btn-green btn-insert" data-target="#m_notes" data-toggle="modal" role="dialog">
                <span class="glyphicon glyphicon-plus"></span> New</button>
        </div>

        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Notes</h4>
            </div>
            <div class="tableadd">
                <table id="example" class="display table-rwd table-user" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>To</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
//                        $notes = Notes::where('CompanyInternalID', Auth::user()->Company->InternalID)
//                                ->get();
//                        $f = rand(0, 50);
//                        foreach ($notes as $data) {
//                            $data->dtRecordformat = date("d-m-Y H:i:s", strtotime($data->dtRecord));
//                            $data->dtModifformat = date("d-m-Y H:i:s", strtotime($data->dtModified));
//                            $data->Remark = str_replace("\r\n", " ", $data->Remark);
//                            $company = Notes::find($data->InternalID)->Company;
//                            $arrData = array($data);
//                            $tamp = myEscapeStringData($arrData);
//                            $tamp = myEncryptJavaScriptText($tamp, $f);
//                            
                        ?>

<?php // }  ?>
                    </tbody>
                </table>    
            </div><!---end tableadd-->
        </div><!---- end div tabwrap---->
    </div><!---end primcontent-->
</div><!---end wrapjour-->
@stop

@section('modal')
<div class="modal fade bs-example-modal-lg " id="m_notes" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action formNotesInsert" id="form-insert">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Insert Notes</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="margbot10">
                                    <input type='hidden' name='jenis' value='insertNotes'>
                                </div>
                                <div class="margbot10">
                                    <label for="UserID">Notes ID *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="noSpecialCharacter" autofocus="" type="text" name="NotesID" id="notesID" maxlength="16" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="Name">To *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="To" id="to" maxlength="200" class="to" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Description *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;" name="Description" id="description" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;" name="Remark" id="remark" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade" id="m_notesUpdate" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action formNotesInsert" id="form-update">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Notes</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="margbot10">
                                    <input type="hidden" value="" id="idUpdate" name="InternalID">
                                    <input type="hidden" value="updateNotes" id="jenisUpdate" name="jenis">
                                </div>
                                <div class="margbot10">
                                    <label for="Name">To *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="To" class="to"  autofocus="" id="toUpdate" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Description *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;" name="Description" id="descriptionUpdate" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;"  name="Remark" id="remarkUpdate" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <small>Created by <span id="createdDetail"></span></small><br>
                                    <small>Modified by <span id="modifiedDetail"></span></small>
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Update</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade" id="m_notesDone" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <form action="" method="post" class="action">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Finishing Notes</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="" id="idDone" name="InternalID">
                            <input type="hidden" value="doneNotes" id="jenisActive" name="jenis">
                            <p>Are you sure?</p>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Yes</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade" id="m_notesDelete" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Delete Notes</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action">
                        <ul>
                            <input type="hidden" value="" id="idDelete" name="InternalID">
                            <input type="hidden" value="deleteNotes" id="jenisDelete" name="jenis">
                            <p>Are you sure want to delete <span id="deleteName"></span>?</p>
                        </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-green">Yes</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
            </div>
            </form>      
        </div>
    </div>  
</div>
@stop
@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript">
var availableTags = [<?php foreach (Notes::select('To')->where('CompanyInternalID', Auth::user()->Company->InternalID)->distinct()->get() as $auto) { ?>"<?php echo $auto->To ?>",<?php } ?>];</script>
<script>
            var notesDataBackup = '<?php echo Route('NotesDataBackup') ?>'
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-master/notes.js')}}"></script>
@stop
