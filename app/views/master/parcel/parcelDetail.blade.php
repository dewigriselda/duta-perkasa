@extends('template.header-footer')

@section('title')
Parcel
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosenCustom.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
@stop

@section('nav')

@stop

@section('content')
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest"> 
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showParcel')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Parcel</a>
                <a href="{{route('parcelDetail',$header->ParcelID)}}" type="button" class="btn btn-sm btn-pure">Detail {{$header->ParcelID}}</a>
            </div>
            <div class="btn-group margr5">
                <a >
                    <button type="button" class="btn btn-green btn-sm dropdown-toggle" id="btn-new" aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
                </a>
            </div>
            <a href="{{Route('parcelUpdate',$header->ParcelID)}}">
                <button id="btn-{{$header->ParcelID}}-update"
                        class="btn btn-green btn-sm ">
                    <span class="glyphicon glyphicon-edit"></span> Edit
                </button>
            </a>
            @if(checkModul('O04'))
            <a href="{{Route('parcelPrint',$header->ParcelID)}}" target='_blank' style="margin-right: 0px !important;">
                <button id="btn-{{$header->ParcelID}}-print"
                        class="btn btn-green btn-sm ">
                    <span class="glyphicon glyphicon-print"></span> Print
                </button>
            </a>
            @endif
        </div>
        <div class="tabwrap">
            <div class="tabhead">
                <h4 class="headtitle">{{'Parcel '.$header->ParcelID}}</h4>
            </div>
            <div class="tableadd"> 
                <div class="headinv new">
                    <ul class="pull-left">
                        <li>
                            <label for="ParcelID">Parcel ID</label>
                            <span>{{$header->ParcelID}}</span>
                        </li>
                        <li>
                            <label for="ParcelName">Name</label>
                            <span>{{$header->ParcelName}}</span>
                        </li>
                        <li>
                            <label for="TextPrint">Text Print</label>
                            <span>{{$header->TextPrint}}</span>
                        </li>

                    </ul>
                    <ul class="pull-right">
                        <li>
                            <label for="Commission">Commission (%)</label>
                            <span>{{$header->Commission}}</span>
                        </li>
                        <li>
                            <label for="">Price</label>
                            <span>{{number_format($header->GrandTotal,2,'.',',')}}</span>
                        </li>
                        <li>
                            <label for="">Remark</label>
                            <span>{{$header->Remark}}</span>
                        </li>
                    </ul>
                </div>
                <div class="padrl10">
                    <table class="table master-data " id="table-parcel" >
                        <thead>
                            <tr>
                                <th>Inventory</th>
                                <th>Uom</th>
                                <th>Qty</th>
                                <th>Price</th>
                                <th>Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($detail) > 0)
                            <?php
                            $total = 0;
                            ?>
                            @foreach($detail as $data)
                            <tr>
                                <td class="left">{{'';$inventory = Inventory::find($data->InventoryInternalID); echo $inventory->InventoryID.' '.$inventory->InventoryName}}</td>
                                <td class="left">{{$data->Uom->UomID}}</td>
                                <td class="right">{{number_format($data->Qty,'0','.',',')}}</td>
                                <td class="right">{{number_format($data->Price,'2','.',',')}}</td>
                                <td class="right">{{number_format($data->SubTotal,'2','.',',');$total += $data->SubTotal}}</td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="4">There is no inventory registered in this parcel.</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                    @if(count($detail) > 0)

                    <table class="pull-right"> 
                        <tr>
                            <td><h5 class="right margr10 h5total"><b>Grand Total</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="grandTotal">{{number_format($total,'2','.',',')}}</b></h5></td>
                        </tr>
                    </table>
                    @endif
                </div><!---- end div padrl10---->         
            </div><!---- end div tableadd---->   
        </div><!---- end div tabwrap---->                 
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop

@section('js')
<script>
    var parcelNew = "<?php echo Route('parcelNew') ?>";
</script>
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script><script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-master/parcel/parcel.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
@stop