@extends('template.header-footer')

@section('title')
Parcel
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<style>
    .help-block {
        margin-left: 30% !important;
    }
</style>
@stop

@section('nav')

@stop

@section('content')
@if(myCheckIsEmpty('Inventory;Warehouse;Currency'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one master Inventory, Currency and Warehouse to insert Parcel.
</div>
@endif
@if(isset($messages))
@if($messages == 'gagalInsert')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New parcel has been inserted.
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest"> 
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showParcel')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Parcel</a>
                <a href="{{route('parcelNew')}}" type="button" class="btn btn-sm btn-pure">New Parcel</a>
            </div>
            <div class="btn-group margr5">
                <a >
                    <button <?php if (myCheckIsEmpty('Inventory;Warehouse;Currency')) echo 'disabled'; ?> type="button" id="btn-new" class="btn btn-green btn-sm dropdown-toggle  " aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
                </a>
            </div>
        </div>
        <form method="POST" action="" id="form-insert">
            <div class="tabwrap">
                <div class="tabhead">
                    <h4 class="headtitle">New Parcel</h4>
                </div>
                <div class="tableadd"> 
                    <div class="headinv new">
                        <ul class="pull-left">
                            <li>
                                <label for="ParcelID">Parcel ID *</label>
                                <input class="noSpecialCharacter" autofocus="" id="parcelID" name="ParcelID" type="text" autocomplete="off" data-validation="required">
                            </li>
                            <li>
                                <label for="ParcelName">Parcel Name *</label>
                                <input class="noSpecialCharacter" id="parcelName" name="ParcelName" type="text" autocomplete="off" data-validation="required">
                            </li>
                            <li>
                                <label for="ParcelName">Text Print *</label>
                                <input class="noSpecialCharacter" id="textPrint" name="TextPrint" type="text" autocomplete="off" data-validation="required">
                            </li>
                            <li>
                                <label for="Price">Price *</label>
                                <input class="noSpecialCharacter numajaDesimal" id="price" name="Price" type="text" autocomplete="off" value="" >
                            </li>
                        </ul>
                        <ul class="pull-right">
                            <li>
                                <label for="barcodeCode">Barcode</label>
                                <input type="text" name="BarcodeCode" id="barcodeCode1" >
                            </li>
                            <li>
                                <label for="Commission">Commission (%) *</label>
                                <input class="numajaDesimal" type="text" name="Commission" id="commission" maxlength="5" data-validation="required">
                            </li>
                            <li>
                                <label for="remark">Remark *</label>
                                <textarea name="Remark" id="remark" data-validation="required"></textarea>
                            </li>
                            <li>
                                <div class="required">
                                    * Required
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="tableadd journupdate">
                        <table class="table master-data" id="table-parcel">
                            <thead>
                                <tr>
                                    <th width="15%">Inventory</th>
                                    <th width="10%">Uom</th>
                                    <th width="10%">Qty</th>
                                    <th width="10%">Price</th>
                                    <th width="10%">Subtotal</th>
                                    <th width="5%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="row0" style="background-color: #e5e5e5 !important">
                                    <td class="" style="border-color: #d8d8d8 !important">
                                        <input class="input-theme margbot10" type="text" id="searchInventory" title="Type Inventory Name or ID then 'Enter'" placeholder="Type Inventory Name or ID then 'Enter'">
                                        <div id="selectInventory">

                                        </div>
                                    </td>
                                    <td style="border-color: #d8d8d8 !important">
                                        <select id="uom-0" class="input-theme uom">

                                        </select>
                                    </td>
                                    <td class="text-right" style="border-color: #d8d8d8 !important">
                                        <input type="text" class="maxWidth qty right numaja" maxlength="11" min="1" id="price-0-qty" value="1">
                                    </td>
                                    <td class="text-right" style="border-color: #d8d8d8 !important">
                                        <input type="text" class="maxWidth price right numajaDesimal" maxlength="" id="price-0" value="0">
                                    </td>
                                    <td id="price-0-qty-hitung" class="right" style="border-color: #d8d8d8 !important">
                                        0.00
                                    </td>
                                    <td style="border-color: #d8d8d8 !important">
                                        <button type="button" class="btn btn-green btn-sm" disabled="" id="btn-addRow"><span class="glyphicon glyphicon-plus"></span></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <input type="hidden" name="grandTotalValue" value="0" id="grandTotalValue">
                        <table class="pull-right"> 
                            <tr>
                                <td><h5 class="right margr10 h5total"><b>Grand Total</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b id="grandTotal"></b></h5></td>
                            </tr>
                        </table>
                    </div><!---- end div tableadd---->      
                </div><!---- end div tableadd---->   
            </div><!---- end div tabwrap---->  
            <div class="btnnest pull-right">
                <button class="btn btn-green btn-sm btn-save" type="button" id="btn-save" <?php if (myCheckIsEmpty('Inventory;Warehouse;Currency')) echo 'disabled'; ?>> Save </button>
            </div>
        </form>
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNewJournal.jquery.js')}}" type="text/javascript"></script>
<script>
var getUomThisInventoryParcel = "<?php echo Route("getUomThisInventoryParcel") ?>";
var getHPPValueInventoryParcel = "<?php echo Route("getHPPValueInventoryParcel") ?>";
var getSearchResultInventoryParcel = "<?php echo Route("getSearchResultInventoryParcel") ?>";
var checkParcelID = "<?php echo Route("checkParcelID") ?>";
var checkBarcodeCode = "<?php echo Route("checkBarcodeCode") ?>";
var getNextParcelID = "<?php echo Route("getNextParcelID") ?>";
var textSelect = '';
var tanggalHariIni = '<?php echo date('d') . '-' . date('m') . '-' . date('Y') ?>';
var parcelNew = "<?php echo Route('parcelNew') ?>";
var parcelDataBackup = '<?php echo Route('parcelDataBackup') ?>';
</script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-master/parcel/parcel.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-master/parcel/parcelNew.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
@stop