@extends('template.header-footer')

@section('title')
Parcel
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<style>
    .help-block {
        margin-left: 30% !important;
    }
</style>
@stop

@section('nav')

@stop

@section('content')
@if(isset($messages))
@if($messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Parcel has been updated.
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest"> 
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showParcel')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Parcel</a>
                <a href="{{route('parcelUpdate',$header->ParcelID)}}" type="button" class="btn btn-sm btn-pure">Update {{$header->ParcelID}}</a>
            </div>
            <div class="btn-group margr5">
                <a href="{{Route('parcelNew')}}">
                    <button type="button" class="btn btn-green btn-sm dropdown-toggle " aria-expanded="false"><span class="glyphicon glyphicon-plus"></span>New </button>
                </a>
            </div>
        </div>
        <form method="POST" action="" id="form-update">
            <input type='hidden' name='ParcelInternalID' value='{{$header->InternalID}}'>
            <input type='hidden' id="date" value='{{date('d-m-Y', strtotime($header->InternalID))}}'>
            <div class="tabwrap">
                <div class="tabhead">
                    <h4 class="headtitle">Parcel <span id="parcelID">{{$header->ParcelID}}</span></h4>
                </div>
                <div class="tableadd"> 
                    <div class="headinv new">
                        <ul class="pull-left">
                            <li>
                                <label for="ParcelName">Parcel Name *</label>
                                <input class="noSpecialCharacter" autofocus="" id="parcelName" name="ParcelName" type="text" autocomplete="off" value="{{$header->ParcelName}}" data-validation="required">
                            </li>
                            <li>
                                <label for="ParcelName">Text Print *</label>
                                <input class="noSpecialCharacter" autofocus="" id="textPrint" name="TextPrint" type="text" autocomplete="off" value="{{$header->TextPrint}}" data-validation="required">
                            </li>
                            <li>
                                <label for="BarcodeCode">Barcode Code </label>
                                <input class="noSpecialCharacter" id="barcodeCode1" name="BarcodeCode" type="text" autocomplete="off" value="{{$header->BarcodeCode}}" >
                            </li>
                            <li>
                                <label for="Price">Price *</label>
                                <input class="noSpecialCharacter numajaDesimal text-right" id="price" name="Price" type="text" autocomplete="off" data-validation="required" value="{{number_format($header->GrandTotal, 2, '.', ',')}}">
                            </li>
                        </ul>
                        <ul class="pull-right">
                            <li>
                                <label for="Commission">Commission (%) *</label>
                                <input class="numajaDesimal" type="text" name="Commission" id="commission" maxlength="5" value="{{$header->Commission}}" data-validation="required">
                            </li>
                            <li>
                                <label for="remark">Remark *</label>
                                <textarea name="Remark" id="remark" data-validation="required">{{$header->Remark}}</textarea>
                            </li>
                            <li>
                                <div class="required">
                                    * Required
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="tableadd journupdate">
                        <table class="table master-data" id="table-parcel">
                            <thead>
                                <tr>
                                    <th width="15%">Inventory</th>
                                    <th width="10%">Uom</th>
                                    <th width="10%">Qty</th>
                                    <th width="10%">Price</th>
                                    <th width="10%">Subtotal</th>
                                    <th width="5%">Action</th>
                                </tr>
                            </thead>
                            <?php
                            $inventoryInternalID = Inventory::select('m_inventory.*')->distinct()->join('m_inventory_uom', 'm_inventory_uom.InventoryInternalID', '=', 'm_inventory.InternalID')
                                            ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)->first()->InternalID;
                            ?>
                            <input type="hidden" name="HidInternalIDFirst" id="hidInternalIDFirst" value="{{$inventoryInternalID}}">
                            <tbody>
                                <?php $barisTerakhir = 0; ?>
                                @if(count($detail) > 0)
                                <tr id="row0" style="background-color: #e5e5e5 !important">
                                    <td class="" style="border-color: #d8d8d8 !important">
                                        <input class="input-theme margbot10" type="text" id="searchInventory" title="Type Inventory Name or ID then 'Enter'" placeholder="Type Inventory Name or ID then 'Enter'">
                                        <div id="selectInventory">

                                        </div>
                                    </td>
                                    <td style="border-color: #d8d8d8 !important">
                                        <select id="uom-0" class="input-theme uom">

                                        </select>
                                    </td>
                                    <td class="text-right" style="border-color: #d8d8d8 !important">
                                        <input type="text" class="maxWidth qty right numaja" maxlength="11" min="1" id="price-0-qty" value="1">
                                    </td>
                                    <td class="text-right" style="border-color: #d8d8d8 !important">
                                        <input type="text" class="maxWidth price right numajaDesimal" maxlength="" id="price-0" value="0">
                                    </td>
                                    <td id="price-0-qty-hitung" class="right" style="border-color: #d8d8d8 !important">
                                        0.00
                                    </td>
                                    <td style="border-color: #d8d8d8 !important">
                                        <button type="button" class="btn btn-green btn-sm" disabled="" id="btn-addRow"><span class="glyphicon glyphicon-plus"></span></button>
                                    </td>
                                </tr>
                                {{'';$barisTerakhir++;}}
                                @foreach($detail as $data)
                                <tr id="row{{$barisTerakhir}}">
                                    <td class="chosen-uom">
                                        <?php $inventory = Inventory::find($data->InventoryInternalID); ?>
                                        <input type="hidden" class="inventory" id="inventory-{{$barisTerakhir}}" style="" name="inventory[]" value="{{$inventory->InternalID}}">
                                        {{$inventory->InventoryID}} 
                                        {{" ".$inventory->InventoryName}}
                                    </td>
                                    <td>
                                        <select id="uom-{{$barisTerakhir}}" name="uom[]" class="input-theme uom">
                                            @foreach (InventoryUom::where("InventoryInternalID", $data->InventoryInternalID)->get() as $uom)
                                            <option value="{{$uom->UomInternalID}}" {{ ($uom->UomInternalID == $data->UomInternalID) ? "selected" : ""}}>{{ $uom->Uom->UomID; }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth qty right numaja" name="qty[]" maxlength="11" min="1" id="price-{{$barisTerakhir}}-qty" value="{{number_format($data->Qty,'0','.',',')}}">
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth price right numajaDesimal" name="price[]" maxlength="" id="price-{{$barisTerakhir}}" value="{{number_format($data->Price,'2','.',',')}}">
                                    </td>
                                    <td id="price-{{$barisTerakhir}}-qty-hitung" class="right subtotal">
                                        {{number_format($data->SubTotal,'2','.',',')}}
                                    </td>
                                    <td>
                                        @if($barisTerakhir == 0)
                                        -
                                        @else
                                        <button class="btn btn-pure-xs btn-xs btn-deleteRow" type="button" data="row{{$barisTerakhir}}"><span class="glyphicon glyphicon-trash"></span></button>
                                        @endif
                                    </td>
                                </tr>
                                {{'';$barisTerakhir++;}}
                                @endforeach
                                @else
                                <tr id="row0" style="background-color: #e5e5e5 !important">
                                    <td class="" style="border-color: #d8d8d8 !important">
                                        <input class="input-theme margbot10" type="text" id="searchInventory" title="Type Inventory Name or ID then 'Enter'" placeholder="Type Inventory Name or ID then 'Enter'">
                                        <div id="selectInventory">

                                        </div>
                                    </td>
                                    <td style="border-color: #d8d8d8 !important">
                                        <select id="uom-0" class="input-theme uom">

                                        </select>
                                    </td>
                                    <td class="text-right" style="border-color: #d8d8d8 !important">
                                        <input type="text" class="maxWidth qty right numaja" maxlength="11" min="1" id="price-0-qty" value="1">
                                    </td>
                                    <td class="text-right" style="border-color: #d8d8d8 !important">
                                        <input type="text" class="maxWidth price right numajaDesimal" maxlength="" id="price-0" value="0">
                                    </td>
                                    <td id="price-0-qty-hitung" class="right" style="border-color: #d8d8d8 !important">
                                        0.00
                                    </td>
                                    <td style="border-color: #d8d8d8 !important">
                                        <button type="button" class="btn btn-green btn-sm" disabled="" id="btn-addRow"><span class="glyphicon glyphicon-plus"></span></button>
                                    </td>
                                </tr>
                                {{'';$barisTerakhir++;}}
                                @endif
                            </tbody>
                        </table>
                        <input type="hidden" name="grandTotalValue" value="0" id="grandTotalValue">

                        <table class="pull-right"> 
                            <tr>
                                <td><h5 class="right margr10 h5total"><b>Grand Total</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b id="grandTotal"></b></h5></td>
                            </tr>
                        </table>
                    </div><!---- end div tableadd---->      
                </div><!---- end div tableadd---->   
            </div><!---- end div tabwrap---->  
            <div class="btnnest pull-right">
                <button type="button" class="btn btn-green btn-sm btn-save" id="btn-save"> Save </button>
            </div>
        </form>
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNewJournal.jquery.js')}}" type="text/javascript"></script>
<script>
var checkParcelIDUpdate = "<?php echo Route("checkParcelIDUpdate") ?>";
var getHPPValueInventoryParcel = "<?php echo Route("getHPPValueInventoryParcel") ?>";
var getUomThisInventoryParcel = "<?php echo Route("getUomThisInventoryParcel") ?>";
var getSearchResultInventoryParcel = "<?php echo Route("getSearchResultInventoryParcel") ?>";
var checkBarcodeCode = "<?php echo Route("checkBarcodeCode") ?>";
var tanggalHariIni = '<?php echo date('d') . '-' . date('m') . '-' . date('Y') ?>';
var textSelect = "";
var baris = '<?php echo $barisTerakhir; ?>';
var barcodeLama = '<?php echo $header->BarcodeCode; ?>';
var parcelDataBackup = '<?php echo Route('parcelDataBackup') ?>';
</script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-master/parcel/parcel.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-master/parcel/parcelUpdate.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script>
$.validate();
</script>
@stop