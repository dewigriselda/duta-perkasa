@extends('template.header-footer')

@section('title')
User
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
@stop

@section('nav')

@stop

@section('content')
@if(isset($messages))
@if($messages == 'gagalInsert' || $messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New user has been inserted.
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> User has been updated.
</div>
@endif
@if($messages == 'suksesDelete')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> User has been deleted.
</div>
@endif
@if($messages == 'gagalDelete')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> User has been create transaction.
</div>
@endif
@if($messages == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@endif
@if(Session::get('messages') == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread hidden-xs" role="group">
                @if(Auth::User()->CompanyInternalID == -1)
                <a href="{{route('showUser')}}" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                @else
                <a href="{{route('showDashboard')}}" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                @endif
                <a class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showUser')}}" class="btn btn-sm btn-pure">User</a>
            </div>
            @if(checkModul('O01') || Auth::user()->CompanyInternalID == -1)
            <a target="_blank" href="{{Route('exportUser')}}">
                <button type="button" class="btn btn-sm btn-green btn-export" id="btn-export">Export Excel</button>    
            </a>
            @endif
            <button type="button" class="btn btn-green btn-insert margr5" data-target="#m_user" data-toggle="modal" role="dialog">
                <span class="glyphicon glyphicon-plus"></span> New</button>
            @if(checkModul('O04'))
            <button type="button" class="btn btn-green nomarg" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rSlipMU">
                <span class="glyphicon glyphicon-file"></span> Commission Report</button>
            @endif
        </div>

        <div class="bread-xs visible-xs hidden-sm hidden-md hidden-lg">
            <p class="text-center">
                @if(Auth::User()->CompanyInternalID == -1)
                <a href="{{route('showUser')}}">{{Config::get('companyHeader.header_company');}}</a> /
                @else
                <a href="{{route('showDashboard')}}">{{Config::get('companyHeader.header_company');}}</a> /
                @endif
                <a>{{ucfirst($toogle)}}</a> /
                <a href="{{route('showUser')}}">User</a>
            </p>
        </div>

        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">User</h4>
            </div>
            <div class="tableadd">
                <table id="example" class="display table-rwd table-user" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>User ID</th>
                            <th>Name</th>
                            <th>Company</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Warehouse</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $userData = User::where('CompanyInternalID', Auth::user()->Company->InternalID)
                                ->where('InternalID', '<>', Auth::user()->InternalID)
                                ->get();
                        if (Auth::user()->Company->InternalID == '-1') {
                            $userData = User::where('InternalID', '<>', Auth::user()->InternalID)
                                            ->where('CompanyInternalID', '<>', -2)->get();
                        }
                        $f = rand(0, 50);
                        foreach ($userData as $data) {
                            $data->dtRecordformat = date("d-m-Y H:i:s", strtotime($data->dtRecord));
                            $data->dtModifformat = date("d-m-Y H:i:s", strtotime($data->dtModified));
                            $data->Remark = str_replace("\r\n", " ", $data->Remark);
                            $company = User::find($data->InternalID)->Company;
                            $arrData = array($data);
                            $tamp = myEscapeStringData($arrData);
                            $tamp = myEncryptJavaScriptText($tamp, $f);
                            ?>
                            <tr>
                                <td>{{$data->UserID}}</td>
                                <td>{{$data->UserName}}</td>
                                <td>{{$company->CompanyName}}</td>
                                <td>{{$data->Email}}</td>
                                <td>{{$data->Phone}}</td>
                                <td>{{Warehouse::find($data->WarehouseInternalID)->WarehouseName}}</td>
                                <td>
                                    @if($data->Status == 0)
                                    Not Active
                                    @else
                                    Active
                                    @endif
                                </td>
                                <td class="text-center">
                                    <button id="btn-{{$data->UserID}}" data-target="#m_userUpdate" data-all='{{$tamp}}'
                                            data-toggle="modal" role="dialog" class="btn btn-pure-xs btn-xs btn-edit">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </button>
                                    <button data-target="#m_userActive" data-internal="{{$data->InternalID}}"  data-toggle="modal" role="dialog"
                                            data-id="{{$data->UserID}}" class="btn btn-pure-xs btn-xs btn-active" @if($data->Status == 1) {{"disabled";}} @endif>
                                            <b> Active </b>
                                    </button>
                                    <button data-target="#m_userNonActive" data-internal="{{$data->InternalID}}"  data-toggle="modal" role="dialog"
                                            data-id="{{$data->UserID}}" class="btn btn-pure-xs btn-xs btn-non-active" @if($data->Status == 0) {{"disabled";}} @endif>
                                            <b> Non Active </b>
                                    </button>
                                    @if((checkModul('SM05') && User::checkAdmin()))
                                    <a href="{{Route('showUserMatrix',$data->UserID)}}">
                                        <button class="btn btn-pure-xs btn-xs">
                                            <b> User Matrix </b>
                                        </button>
                                    </a>
                                    @endif
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>    
            </div><!---end tableadd-->
        </div><!---- end div tabwrap---->
    </div><!---end primcontent-->
</div><!---end wrapjour-->
@stop

@section('modal')
<div class="modal fade bs-example-modal-lg " id="m_user" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action" id="form-insert">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Insert User</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="margbot10">
                                    <input type='hidden' name='jenis' value='insertUser'>
                                </div>
                                <div class="margbot10">
                                    <label for="UserID">User ID *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="noSpecialCharacter" type="text" name="UserID" id="userID" maxlength="16" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="Name">Name *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Name" id="name" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="Email">Email *</label> 
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Email" id="email" maxlength="200" data-validation="email">
                                </div>
                                <div class="margbot10">
                                    <label for="Phone">Phone *</label> 
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Phone" id="phone" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="warehouse">Warehouse *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal currency" id="warehouse" name="Warehouse" data-validation="required">
                                        @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $war)
                                        <option id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                            {{$war->WarehouseName;}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="Password">Password *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="password" name="Password_confirmation" id="password" maxlength="16" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="cPassword">Confirm Password *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="password" name="Password" id="cpassword" maxlength="16" data-validation="confirmation">
                                </div>
                                @if(Auth::user()->Company->InternalID == "-1")
                                <div class="margbot10">
                                    <label for="company">Company *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal" id="company" style="" name="company">
                                        @foreach(Company::all() as $company)
                                        <option value="{{$company->InternalID}}">
                                            {{$company->CompanyName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                @endif
                                <div class="margbot10">
                                    <label for="remark">Remarks *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;" name="remark" id="remark" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <input type="checkbox" name="see" value="1">Complete
                                </div>
                                <div class="margbot10">
                                    <input type="checkbox" name="approval" value="1">Approval
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade" id="m_userUpdate" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action" id="form-update">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update User</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="margbot10">
                                    <input type="hidden" value="" id="idUpdate" name="InternalID">
                                    <input type="hidden" value="updateUser" id="jenisUpdate" name="jenis">
                                </div>
                                <div class="margbot10">
                                    <label for="Name">Name *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Name" id="nameUpdate" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="Email">Email *</label> 
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Email" id="emailUpdate" maxlength="200" data-validation="email">
                                </div>
                                <div class="margbot10">
                                    <label for="Phone">Phone *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Phone" id="phoneUpdate" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="warehouse">Warehouse *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal currency" id="warehouseUpdate" name="Warehouse" data-validation="required">
                                        @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $war)
                                        <option id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                            {{$war->WarehouseName;}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                @if(Auth::user()->Company->InternalID == "-1")
                                <div class="margbot10">
                                    <label for="company">Company *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal" id="companyUpdate" style="" name="company">
                                        @foreach(Company::all() as $com)
                                        <option id="com{{$com->InternalID}}" value="{{$com->InternalID}}">
                                            {{$com->CompanyName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                @endif
                                <div class="margbot10">
                                    <label for="remark">Remarks *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;"  name="remark" id="remarkUpdate" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <input type="checkbox" id="seeUpdate" name="see" value="1">Complete
                                </div>
                                <div class="margbot10">
                                    <input type="checkbox" id="approvalUpdate" name="approval" value="1">Approval
                                </div>
                                <div class="margbot10">
                                    <small>Created by <span id="createdDetail"></span></small><br>
                                    <small>Modified by <span id="modifiedDetail"></span></small>
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Update</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade" id="m_userActive" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <form action="" method="post" class="action">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Active User</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="" id="idActive" name="InternalID">
                            <input type="hidden" value="activeUser" id="jenisActive" name="jenis">
                            <p>Are you sure?</p>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Yes</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade" id="m_userNonActive" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <form action="" method="post" class="action">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Non Active User</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="" id="idNonActive" name="InternalID">
                            <input type="hidden" value="nonActiveUser" id="jenisNonActive" name="jenis">
                            <p>Are you sure?</p>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Yes</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade bs-example-modal-lg" id="r_summary" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action" id="" target="_blank">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="titleReport">Commission Report</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="margbot10">
                                    <input type='hidden' name='jenis' id="jenisReport" value='commissionReport'>
                                </div>
                                <div class="margbot10">
                                    <label for="user">User *</label>
                                    <select class="input-theme" id="user" style="" name="user">
                                        @foreach(User::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $user)
                                        <option value="{{$user->InternalID}}">
                                            {{$user->UserID .' '. $user->UserName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="sDate">Start Date *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="sDate" id="startDateReport" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="eDate">End Date *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="eDate" id="endDateReport" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                    <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>
@stop
<?php
$u = myEncryptJavaScript(User::select('UserID')->get(), $f);
$e = myEncryptJavaScript(User::select('Email')->get(), $f);
?>
@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript">
var a = '<?php echo $u; ?>';
var c = '<?php echo $e; ?>';
var b = <?php echo $f; ?>
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-master/user.js')}}"></script>
@stop
