@extends('template.header-footer') <!--Load template atau view dasarnya pada view ini-->

@section('title')
Variety
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
@stop

@section('nav')
@stop

@section('content')
@if(myCheckIsEmpty('Group'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one Group to insert Variety.
</div>
@endif
@if(isset($messages))
@if($messages == 'gagalInsert' || $messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif

@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New variety has been inserted.
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Variety has been updated.
</div>
@endif
@if($messages == 'suksesDelete')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Variety has been deleted.
</div>
@endif
@if($messages == 'gagalDelete')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Variety has been registered in table inventory.
</div>
@endif
@if($messages == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">

        <div class="btnnest">
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showVariety')}}" type="button" class="btn btn-sm btn-pure">Variety</a>
            </div>
            @if(checkModul('O01'))
            <a target="_blank" href="{{Route('exportVariety')}}">
                <button type="button" class="btn btn-sm btn-green btn-export" id="btn-export">Export Excel</button>  
            </a>
            @endif
            <button type="button" class="btn btn-green btn-insert" {{ (myCheckIsEmpty('Group')) ? "disabled" : ""}} data-target="#m_variety" data-toggle="modal" role="dialog">
                <span class="glyphicon glyphicon-plus"></span> New</button>
        </div>

        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Variety</h4>
            </div>

            <div class="tableadd">
                <table id="example" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Variety ID</th>
                            <th>Name</th>
                            <th>Group Name</th>
                            <th>Remark</th>
                            <th>Record</th>
                            <th>Modified</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $f = rand(0, 50);
                        foreach (Variety::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                            $data->dtRecordformat = date("d-m-Y H:i:s", strtotime($data->dtRecord));
                            $data->dtModifformat = date("d-m-Y H:i:s", strtotime($data->dtModified));
                            $arrData = array($data);
                            $tamp = myEscapeStringData($arrData);
                            $tamp = myEncryptJavaScriptText($tamp, $f);
                            ?>
                            <tr>
                                <td>{{$data->VarietyID}}</td>
                                <td>{{$data->VarietyName}}</td>
                                <td>{{$data->Group->GroupName}}</td>
                                <td>{{$data->Remark}}</td>
                                <td>{{$data->UserRecord." ".date( "d-m-Y H:i:s", strtotime($data->dtRecord))}}</td>
                                @if($data->UserModified == "0")
                                <td>-</td>
                                @else
                                <td>{{$data->UserModified." ".date( "d-m-Y H:i:s", strtotime($data->dtModified))}}</td>
                                @endif
                                <td class="text-center"><button id="btn-{{$data->VarietyID}}" data-target="#m_varietyUpdate" data-all='{{$tamp}}'
                                                                data-toggle="modal" role="dialog"
                                                                class="btn btn-pure-xs btn-xs btn-edit">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </button>
                                    <button data-target="#m_varietyDelete" data-internal="{{$data->InternalID}}"  data-toggle="modal" role="dialog"
                                            data-id="{{$data->VarietyID}}" data-name='{{$data->VarietyName}}'  class="btn btn-pure-xs btn-xs btn-delete">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div><!---end tableadd-->
        </div><!---- end div tabwrap---->
    </div><!---end primcontent-->
</div><!---end wrapjour-->
@stop

@section('modal')

<div class="modal fade" id="m_variety" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insert Variety</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="form-insert">
                        <ul>
                            <input type='hidden' name='jenis' value='insertVariety'>
                            <li>
                                <label for="VarietyID">Variety ID</label> *
                            </li>
                            <li>
                                <input class="noSpecialCharacter" autofocus="" type="text" name="VarietyID" id="varietyID" maxlength="200" data-validation="required">
                            </li>
                            <li>
                                <label for="VarietyID">Name</label> *
                            </li>
                            <li>
                                <input type="text" name="VarietyName" id="varietyName" maxlength="200" data-validation="required">
                            </li>
                            <li>
                                <label for="InventoryGroup">Group</label> *
                            </li>
                            <li>
                                <select class="chosen-select choosen-modal" id="groupInternalID" style="" name="GroupInternalID">
                                    @foreach(Group::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('InternalID')->get() as $data)
                                    <option value="{{$data->InternalID}}">
                                        {{$data->GroupName}}
                                    </option>
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label for="remark">Remarks</label> *
                            </li>
                            <li>
                                <textarea style="resize:none;"  name="remark" id="remark" maxlength="1000" data-validation="required"></textarea>
                            </li>
                        </ul>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>      
        </div>
    </div>  
</div>


<div class="modal fade" id="m_varietyUpdate" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Update Variety</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="form-update">
                        <ul>
                            <input type="hidden" value="" id="idUpdate" name="InternalID">
                            <input type="hidden" value="updateVariety" id="jenisUpdate" name="jenis">
                            <li>
                                <label for="VarietyID">Name</label> *
                            </li>
                            <li>
                                <input type="text" name="VarietyName" autofocus="" id="varietyNameUpdate" maxlength="200" data-validation="required">
                            </li>
                            <li>
                                <label for="InventoryGroup">Group</label> *
                            </li>
                            <li>
                                <select class="chosen-select choosen-modal" id="groupInternalIDUpdate" style="" name="GroupInternalID">
                                    @foreach(Group::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('InternalID')->get() as $data)
                                    <option value="{{$data->InternalID}}">
                                        {{$data->GroupName}}
                                    </option>
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label for="remark">Remarks</label> *
                            </li>
                            <li>
                                <textarea style="resize:none;" name="remark" id="remarkUpdate" maxlength="1000" data-validation="required"></textarea>
                            </li>
                            <li>
                                <small>Created by <span id="createdDetail"></span></small><br>
                                <small>Modified by <span id="modifiedDetail"></span></small>
                            </li>
                        </ul>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-green">Update</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade" id="m_varietyDelete" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Delete Variety</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action">
                        <ul>
                            <input type="hidden" value="" id="idDelete" name="InternalID">
                            <input type="hidden" value="deleteVariety" id="jenisDelete" name="jenis">
                            <p>Are you sure want to delete <span id="deleteName"></span>?</p>
                        </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-green">Yes</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
            </div>
            </form>      
        </div>
    </div>  
</div>
@stop
<?php
$i = myEncryptJavaScript(Variety::select('VarietyID')->where('CompanyInternalID', Auth::user()->Company->InternalID)->get(), $f);
?>

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/chosenNoHide.jquery.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript">
var a = '<?php echo $i; ?>';
var b = <?php echo $f; ?>;
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-master/variety.js')}}"></script>
@stop