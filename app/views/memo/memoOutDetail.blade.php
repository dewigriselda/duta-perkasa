@extends('template.header-footer')

@section('title')
MemoOut
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosenCustom.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
@stop

@section('nav')

@stop

@section('content')
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showMemoOut')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Memo Out</a>
                <a href="{{route('memoOutDetail',$header->MemoOutID)}}" type="button" class="btn btn-sm btn-pure">Detail {{$header->MemoOutID}}</a>
            </div>
            <div class="btn-group margr5">
                <a href="{{Route('memoOutNew')}}">
                    <button type="button" class="btn btn-green btn-sm dropdown-toggle" aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
                </a>
            </div>
            <a href="{{Route('memoOutUpdate',$header->MemoOutID)}}">
                <button id="btn-{{$header->MemoOutID}}-update"
                        class="btn btn-green btn-sm ">
                    <span class="glyphicon glyphicon-edit"></span> Edit
                </button>
            </a>
            @if(checkModul('O04'))
            <a href="{{Route('memoOutPrint',$header->MemoOutID)}}" target='_blank' style="margin-right: 0px !important;">
                <button id="btn-{{$header->MemoOutID}}-print"
                        class="btn btn-green btn-sm ">
                    <span class="glyphicon glyphicon-print"></span> Print
                </button>
            </a>
            @endif
        </div>
        <div class="tabwrap">
            <div class="tabhead">
                <h4 class="headtitle">{{'MemoOut '.$header->MemoOutID}}</h4>
            </div>
            <div class="tableadd"> 
                <div class="headinv new">
                    <ul class="pull-left">
                        <li>
                            <label for="date">Date</label>
                            <span>{{date( "d-m-Y", strtotime($header->MemoOutDate))}}</span>
                        </li>
                        <li>
                            <label for="currency">Currency</label>
                            <span>{{'';$currency = Currency::find($header->CurrencyInternalID); $currencyName = $currency->CurrencyName; echo $currencyName}}</span>
                        </li>
                        <li>
                            <label for="rate">Rate</label>
                            <span>{{number_format($header->CurrencyRate,'2','.',',')}}</span>
                        </li>
                    </ul>
                    <ul class="pull-right">
                        <li>
                            <label for="">Warehouse</label>
                            <span>{{$header->Warehouse->WarehouseName}}</span>
                        </li>
                        <li>
                            <label for="">Remark</label>
                            <span>{{$header->Remark}}</span>
                        </li>
                    </ul>
                </div>
                <div class="padrl10">
                    <table class="table master-data " id="table-memoOut" >
                        <thead>
                            <tr>
                                <th>Inventory</th>
                                <th>Uom</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($detail) > 0)
                            <?php
                            $total = 0;
                            ?>
                            @foreach($detail as $data)
                            <tr>
                                <td class="left">{{'';$inventory = Inventory::find($data->InventoryInternalID); echo $inventory->InventoryID.' '.$inventory->InventoryName}}</td>
                                <td class="right">{{$data->Uom->UomID}}</td>
                                <td class="right">{{number_format($data->Qty,'0','.',',')}}</td>
                                <td class="right">{{number_format($data->Price,'2','.',',')}}</td>
                                <td class="right">{{number_format($data->SubTotal,'2','.',',');$total += $data->SubTotal}}</td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="4">There is no inventory registered in this memoOut.</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                    @if(count($detail) > 0)

                    <table class="pull-right"> 
                        <tr>
                            <td><h5 class="right margr10 h5total"><b>Grand Total</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="grandTotal">{{number_format($total,'2','.',',')}}</b></h5></td>
                        </tr>
                    </table>
                    @endif
                </div><!---- end div padrl10---->         
            </div><!---- end div tableadd---->   
        </div><!---- end div tabwrap---->                 
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop

@section('modal')
<div class="modal fade" id="r_summary" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Summary Report</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="" target="_blank">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='summaryMemoOut'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateReport" data-validation="required">
                            </li>
                        </ul>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>      
        </div>
    </div>  
</div>
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script><script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script>
    var memoOutDataBackup = '<?php echo Route('memoOutDataBackup') ?>';
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-memo/memoOut.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
@stop