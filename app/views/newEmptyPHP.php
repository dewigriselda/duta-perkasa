<?php

class ReferralController extends BaseController {
    /*
      |--------------------------------------------------------------------------
      | Default Home Controller
      |--------------------------------------------------------------------------
      |
      | You may wish to use controllers instead of, or in addition to, Closure
      | based routes. That's great! Here is an example controller method to
      | get you started. To route to this controller, just add the route:
      |
      |	Route::get('/', 'HomeController@showWelcome');
      |
     */

    public function showReferral() {
        if ($_SERVER["REQUEST_METHOD"] === "POST") {
            if (Input::get("jenis") == "insertReferral") {
                return $this->insertReferral();
            } else if (Input::get("jenis") == "updateReferral") {
                return $this->updateReferral();
            } else if (Input::get("jenis") == "deleteReferral") {
                return $this->deleteReferral();
            } else if (Input::get("jenis") == "nactiveReferral") {
                return $this->nactiveReferral();
            } else if (Input::get("jenis") == "activeReferral") {
                return $this->activeReferral();
            }
        }
        return View::make('referral')->withPage('referral')->with('parentPage', 'master');
    }

    public function showReferralDetail($id) {
        if ($_SERVER["REQUEST_METHOD"] === "POST") {
            if (Input::get("jenis") == "updateChild") {
                echo ("masuk1");
                return $this->addChildReferral();
            }
        }
        $header = Referral::where('InternalID', $id)->first();
        return View::make('referral-detail')->withHeader($header)->withPage('referralchild')->with('parentPage', 'master')->with('kode', $id);
    }

    public function addChildReferral() {
        echo ("masuk2");
        foreach (Input::get('Check') as $a) {
            echo ("masuk3");
            $referralchild = new ReferralChild;
            $referralchild->ReferralInternalID = Input::get("referralIDchild");
            $referralchild->ChildInternalID = $a;
            $referralchild->save();
        }
        exit();
        return View::make('referral-detail')->withHeader($header)->withPage('referralchild')->with('parentPage', 'master')->with('kode', $id)->withMessages('gagalInsert')->withErrors($validator->messages());
    }

    public function insertReferral() {
        $data = Input::all();
        $rule = array(
            "ReferralID" => "required|max:200",
            "ReferralName" => "required|max:200",
            "ReferralPhone" => "required|max:200",
            "ReferralAddress" => "required|max:200",
            "ReferralCity" => "required|max:200",
            "ReferralEmail" => "required|max:200"
        );

        $validator = Validator::make($data, $rule);

        if ($validator->fails()) {
            return View::make("referral")
                            ->withMessages('gagalInsert')
                            ->withErrors($validator->messages())
                            ->withPage('referral')->with('parentPage', 'master');
        } else {
            //input valid
            $referral = new Referral();
            $referral->ReferralID = Input::get("ReferralID");
            $referral->ReferralName = Input::get("ReferralName");
            $referral->ReferralPhone = Input::get("ReferralPhone");
            $referral->ReferralAddress = Input::get("ReferralAddress");
            $referral->ReferralCity = Input::get("ReferralCity");
            $referral->ReferralEmail = Input::get("ReferralEmail");

            $referral->save();

            return View::make("referral")
                            ->withMessages('suksesInsert')
                            ->withReferral($referral->ReferralID)
                            ->withPage('referral')->with('parentPage', 'master');
        }
    }

    public function updateReferral() {
        //rule
        $rule = array(
            "ReferralName" => "required|max:200",
            "ReferralPhone" => "required|max:200",
            "ReferralAddress" => "required|max:200",
            "ReferralCity" => "required|max:200",
            "ReferralEmail" => "required|max:200"
        );

        $data = Input::all();
        $validator = Validator::make($data, $rule);

        if ($validator->fails()) {
            return View::make("referral")
                            ->withMessages('gagalUpdate')
                            ->withErrors($validator->messages())
                            ->withPage('referral')->with('parentPage', 'master');
        } else {
            //input valid

            $referral = Referral::find(Input::get("InternalID"));
            $referral->ReferralName = Input::get("ReferralName");
            $referral->ReferralPhone = Input::get("ReferralPhone");
            $referral->ReferralAddress = Input::get("ReferralAddress");
            $referral->ReferralCity = Input::get("ReferralCity");
            $referral->ReferralEmail = Input::get("ReferralEmail");
            $referral->save();

            return View::make("referral")
                            ->withMessages('suksesUpdate')
                            ->withReferral($referral->ReferralID)
                            ->withPage('referral')->with('parentPage', 'master');
        }
    }

    public function deleteReferral() {
        $referral = Referral::find(Input::get("InternalID"))->delete();
        return View::make("referral")
                        ->withMessages('suksesDelete')
                        ->withPage('referral')->with('parentPage', 'master');
    }

    public function cetakReferral($id) {
        $utama = Referral::where('ReferralID', $id)->first();
        $anakLangsung = Company::where('ReferralInternalID', $utama->InternalID)->get();
        $anakReferal = ReferralChild::where('ReferralInternalID', ($utama->InternalID))->get();
        $html = '
            <html>
                <head>
                    <style>
                        table{
                            border-spacing: 0;
                        }      
                        th{
                            padding: 5px;
                             border-spacing: 0;
                            border-bottom: 2px solid #ddd;
                            text-align: center;
                        }
                        td{
                            padding: 10px;
                        }
                    .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 6px; content: "Halaman " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box; position: relative;">
                    <div style="height: 85px;border-bottom:1px solid #ddd;margin-bottom:20px;">
                        <div style="width: 420px; float: left;">
                            <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->company->CompanyName . '</b>
                        </div> 
                        <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Alamat</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->company->Address . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Nomor Telepon</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Laporan Referral</h5>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                            <table>
                            <tr>
                              <td width="275px">
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">ID Referrer</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $id . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Nama Referrer</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $utama->ReferralName . '</td>
                                 </tr>
                                </table>
                              </td>
                            </tr>
                            </table>
                        </div>    
                            <table width="100%"  style="margin-top: 5px; clear: both;  top: 80px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="50%">Nama Company</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Price</th> 
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Perolehan</th> 
                                        </tr>
                                    </thead>
                                    <tbody>';
        if (count($anakLangsung) > 0) {
            foreach ($anakLangsung as $companyLangsung) {
                $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;"> ' . $companyLangsung->CompanyName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($companyLangsung->Price) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format(($companyLangsung->Price) * 0.2) . '</td>
                            </tr>';
            }
        } else {

            $html.= '<tr>
                            <td colspan="4" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">Tidak ada company terdaftar.</td>
                        </tr>';
        }
        $html.="</tbody></table>";
        if (count($anakReferal) > 0) {
            foreach ($anakReferal as $anakReferalObject) {
                $html.='<div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                            <table>
                            <tr>
                              <td width="275px">
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">ID Referrer</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $anakReferalObject->ReferralID . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Nama Referrer</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $anakReferalObject->ReferralName . '</td>
                                 </tr>
                                </table>
                              </td>
                            </tr>
                            </table>
                        </div>   
                            <table width="100%"  style="margin-top: 5px; clear: both;  top: 80px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="50%">Nama Company</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Price</th> 
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Perolehan</th> 
                                        </tr>
                                    </thead>
                             </table>';
            }
        } else {
            $html.="Tidak ada lanjutan";
        }
        return PDF ::load($html, 'A5', 'portrait')->show();
    }

    public function exportExcelReferral() {
        //sampai sini
        Excel::create('Master_Referral', function($excel) {
            $excel->sheet('Master_Referral', function($sheet) {
                $sheet->mergeCells('B1:I1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Master Referral");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "ID Referral");
                $sheet->setCellValueByColumnAndRow(3, 2, "Nama");
                $sheet->setCellValueByColumnAndRow(4, 2, "Telepon");
                $sheet->setCellValueByColumnAndRow(5, 2, "Alamat");
                $sheet->setCellValueByColumnAndRow(6, 2, "Kota");
                $sheet->setCellValueByColumnAndRow(7, 2, "Email");
                $sheet->setCellValueByColumnAndRow(8, 2, "Pemakaian");
                $row = 3;
                foreach (Referral::orderBy('ReferralID', 'asc')->get() as $data) {
                    $counts = Company::where('ReferralInternalID', '=', $data->InternalID)->count();

                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, "`" . $data->ReferralID);
                    $sheet->setCellValueByColumnAndRow(3, $row, $data->ReferralName);
                    $sheet->setCellValueByColumnAndRow(4, $row, $data->ReferralPhone);
                    $sheet->setCellValueByColumnAndRow(5, $row, $data->ReferralAddress);
                    $sheet->setCellValueByColumnAndRow(6, $row, $data->ReferralCity);
                    $sheet->setCellValueByColumnAndRow(7, $row, $data->ReferralEmail);
                    $sheet->setCellValueByColumnAndRow(8, $row, $counts);
                    $row++;
                }
                $row--;
                $sheet->setBorder('B2:I' . $row, 'thin');
                $sheet->cells('B2:I2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:I' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
            });
        })->export('xls');
    }

//====================================AJAX=============================================
    public function getReferralDetailUpdate() {
        return Referral::find(Input::get("id"));
    }

    public function getReferralDetailForSales() {
        return Referral::find(Input::get("id"));
    }

    public function checkReferralID() {
        if (Input::get("id") == '') {
            return 1;
        } else {
            $referral = Referral::where('ReferralID', Input::get("id"))->count();
            if ($referral == 0) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    //===================================/AJAX=============================================
}
