@extends('template.header-footer')

@section('title')
Mrv 
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<style>
    .help-block {
        margin-left: 30% !important;
    }
    .headinv .chosen-single{
        width: 200px !important;
    }
    .headinv .chosen-drop{
        width: 200px !important;
    }
</style>
@stop

@section('nav')

@stop

@section('content')
@if(isset($messages))
@if($messages == 'gagalInsert')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New mrv  has been inserted.
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest"> 
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showMrv')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Mrv</a>
                <a href="{{route('mrvNew', $header->PurchaseOrderID)}}" type="button" class="btn btn-sm btn-pure">New Mrv from {{$header->PurchaseOrderID}}</a>
            </div>
            <div class="btn-group margr5">
                <button type="button" class="btn btn-green btn-sm dropdown-toggle" data-target="#insert" data-toggle="modal" role="dialog"
                        aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
            </div>
            <button  <?php if (myCheckIsEmpty('Mrv')) echo 'disabled'; ?> id="search-button" class="btn btn-green btn-sm margr5"><span class="glyphicon glyphicon-search"></span> Search </button>
            @if(checkModul('O04'))
            <button type="button" class="btn btn-green margr5" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rSummary">
                <span class="glyphicon glyphicon-file"></span> Summary Report</button>
            <button type="button" class="btn btn-green" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rDetail">
                <span class="glyphicon glyphicon-file"></span> Detail Report</button>
            @endif
        </div>
        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Search</h4>
            </div>
            <div class="tableadd">     
                <ul class="searchmenu">
                    <form method="GET" action="">
                        <!--                        <li><label for="typePayment">Payment Type</label>
                                                    <br>
                                                    <select name="typePayment" style="width: 100px">
                                                        <option value="-1">All</option>
                                                        <option value="0">Cash</option>
                                                        <option value="1">Credit</option>  
                                                        <option value="2">CBD</option>  
                                                        <option value="3">Deposit</option>  
                                                        <option value="4">Down Payment</option>  
                                                    </select>
                                                </li>-->
                        <!--                        <li><label for="typeTax">Tax Type</label>
                                                    <br>
                                                    <select name="typeTax" style="width: 100px">
                                                        <option value="-1">All</option>
                                                        <option value="0">Non Tax</option>
                                                        <option value="1">Tax</option>  
                                                    </select>
                                                </li>-->
                        <li><label for="typeTax">Warehouse</label>
                            <br>
                            <select name="warehouseatas" style="width: 100px">
                                <option value="-1">All</option>
                                @foreach(Warehouse::where("Type",Auth::user()->WarehouseCheck)->get() as $w)
                                <option value="{{$w->InternalID}}">{{$w->WarehouseName}}</option>
                                @endforeach
                            </select>
                        </li>
                        <li><label for="date">Start Date</label>
                            <br><input id="startDate" name="startDate" type="text" autocomplete="off">
                        </li>
                        <li><label for="date">End Date</label>
                            <br><input id="endDate" name="endDate" type="text" autocomplete="off">
                        </li>
                        <li><br>
                            <button type="submit" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-search"></span> Search </button>
                        </li>
                        <li><br>
                            <button id="cancel" type="reset"  class="btn btn-pure btn-sm"><span class="glyphicon glyphicon-remove"></span> Cancel </button>
                        </li>
                    </form>
                </ul>
            </div><!---- end div tableadd---->
        </div><!---- end div tabwrap---->
        <form method="POST" action="" id='form-insertMrv'>
            <input type='hidden' name='PurchaseOrderInternalID' value='{{$header->InternalID}}'>
            <div class="tabwrap">
                <div class="tabhead">
                    <h4 class="headtitle"> Mrv <span id="mrvID">{{MrvHeader::getNextIDMrv($mrv)}}</span></h4>
                </div>
                <div class="tableadd"> 
                    <div class="headinv new">
                        <ul @if(checkModul('O05')) class="pull-left" style="width: 540px;" @else class="pull-left"  @endif>
                             <li>
                                <label for="purchaseOrderId">Order ID</label>
                                <span for="purchaseOrderId">{{$header->PurchaseOrderID}}</span>
                            </li>
                            <li>
                                <label for="date">Date *</label>
                                <input id="date" name="date" type="text" style="width: 200px;" autocomplete="off" data-validation="required">
                            </li>
                            <li>
                                <label for="supplier" style="float: left">Supplier</label>
                                <span style=""><?php
                                    //$header->coa6->ACC6ID . ' ' . $header->coa6->ACC6Name;  //cara baru
                                    $coa6 = PurchaseOrderHeader::find($header->InternalID)->coa6;
                                    echo $coa6->ACC6ID . ' ' . $coa6->ACC6Name;
                                    ?></span>
                            </li>
                            <!--                            <li>
                                                            <label for="longTerm">Payment</label>
                                                            @if($header->isCash == 0)
                                                            <span>{{'Cash'}}</span>
                                                            @else
                                                            <span>{{'Credit'}}</span>
                                                            @endif
                                                        </li>-->
                            @if($header->isCash != 0)
                            <!--                            <li>
                                                            <label for="longTerm">Due Date</label>
                                                            <span>{{date( "d-m-Y", strtotime("+".$header->LongTerm." day",strtotime($header->PurchaseOrderDate)))}}</span>
                                                        </li>-->
                            @else
                            <!--                            <li>
                                                            <label for="slip">Slip number *</label>
                                                            <select class="chosen-select" id="slip" style="" name="slip">
                                                                @foreach(Slip::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $slip)
                                                                <option value="{{$slip->InternalID}}">
                                                                    {{$slip->SlipID.' '.$slip->SlipName}}
                                                                </option>
                                                                @endforeach
                                                            </select>
                                                        </li>-->
                            @endif
                            <li>
                                <label for="date">SJ ID</label>
                                <input id="sjid" name="sjid" type="text" style="width: 200px;" autocomplete="off">
                            </li>
                        </ul>
                        <ul @if(checkModul('O05')) class="pull-left" style="width: 540px;" @else class="pull-right"  @endif>
                             <li>
                                <label for="warehouse">Warehouse *</label>
                                <select class="chosen-select choosen-modal currency" id="warehouseHeader" name="warehouse">
                                    @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->where("Type",Auth::user()->WarehouseCheck)->get() as $war)
                                    <option {{'';if($war->InternalID == $header->WarehouseInternalID) echo 'selected';}} id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                        {{$war->WarehouseName;}}
                                    </option>
                                    @endforeach
                                </select>
                            </li>
                            <!--                            <li>
                                                            <label for="currency">Currency</label>
                                                            <span>{{'';$currency = Currency::find($header->CurrencyInternalID); $currencyName = $currency->CurrencyName; echo $currencyName}}</span>
                                                        </li>
                                                        <li>
                                                            <label for="rate">Rate</label>
                                                            <span>{{number_format($header->CurrencyRate,'2','.',',')}}</span>
                                                        </li>
                                                        <li>
                                                            <label for="VAT">VAT</label>
                                                            @if($header->VAT == 0)
                                                            <span>{{'Non Tax'}}</span>
                                                            <input type="hidden" id="taxMrv" value="0">
                                                            @else
                                                            <span>{{'Tax'}}</span>
                                                            <input type="hidden" id="taxMrv" value="1">
                                                            @endif
                                                        </li>-->
                            <li>
                                <label for="remark">Remark</label>
                                <textarea name="remark" id="remark" style="width: 200px;" >{{$header->Remark}}</textarea>
                            </li>
                            @if(!checkModul('O05'))
                            <li>
                                <div class="required">
                                    * Required
                                </div>
                            </li>
                            @endif
                        </ul>
                        @if(checkModul('O05'))
                        <!--                        <ul class="pull-left" style="width: 360px;">
                                                    <li>
                                                        <label for="transactiontype">Transaction</label>
                                                        <select class="chosen-select choosen-modal currency" id="transactionType" name="TransactionType">
                                                            <option value="1"> For who is not collect PPN </option>
                                                            <option value="2"> For Chamberlain </option>
                                                            <option value="3"> Except Chamberlain </option>
                                                            <option value="4"> DPP other value </option>
                                                            <option value="6"> Other handover, include handover to foreigner tourist in the event of VAT refund </option>
                                                            <option value="7"> Handover PPN is not collect</option>
                                                            <option value="8"> Handover PPN Freed</option>
                                                            <option value="9"> Handover Assets (Pasal 16D UU PPN)</option>
                                                        </select>
                                                    </li>
                                                    <li>
                                                        <label for="replacement">Replacement</label>
                                                        <input style="width:15px; height: 15px;" type="checkbox" name="Replacement" id="replacement" value="1"> Tax Replacement
                                                    </li>
                                                    <li>
                                                        <label for="numbertax">Tax Number</label>
                                                        <input type="hidden" name="TaxNumber" value="" id="numberTax">
                                                        <input type="text" class="numaja autoTab" style="width: 40px;" maxlength="3" id="numberTax1" > .
                                                        <input type="text" class="numaja autoTab" style="width: 40px;" maxlength="3" id="numberTax2" > -
                                                        <input type="text" class="numaja autoTab" style="width: 30px;" maxlength="2" id="numberTax3" > .
                                                        <input type="text" class="numaja autoTab" style="width: 75px;" maxlength="8" id="numberTax4" >
                                                    </li>
                                                    <li>
                                                        <label for="taxmonth">Tax Month</label>
                                                        <select class="chosen-select choosen-modal currency" id="taxMonth" name="TaxMonth">
                                                            @for($aa = 1; $aa<=12; $aa++)
                                                            <option value="{{$aa}}" >{{date('F',strtotime('2015-'.$aa.'-01'))}}</option>
                                                            @endfor
                                                        </select>
                                                    </li>
                                                    <li>
                                                        <label for="taxyear">Tax Year</label>
                                                        {{''; $year = date('Y'); $mulai = 2012;}}
                                                        <select class="chosen-select choosen-modal currency" id="taxYear" name="TaxYear">
                                                            @while($mulai <= $year)
                                                            <option value="{{$year}}">{{$year}}</option>
                                                            {{'';$year--;}}
                                                            @endwhile
                                                        </select>
                                                    </li>
                                                    <li>
                                                        <div class="required">
                                                            * Required
                                                        </div>
                                                    </li>
                                                </ul>-->
                        @endif
                    </div>
                    <div class="tableadd journupdate">
                        <table class="table master-data" id="table-mrv">
                            <thead>
                                <tr>
                                    <th width="15%">Inventory</th>
                                    <th width="6%">Uom</th>
                                    <th width="6%">Qty Order</th>
<!--                                    <th width="8%">Price</th>
                                    <th width="8%">Disc (%)</th>
                                    <th width="8%">Disc 2 (%)</th>
                                    <th width="10%">Disc</th>
                                    <th width="10%">Subtotal</th>-->
                                    <th width="10%">Max Qty Mrv</th>
                                    <th width="10%">Qty</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $barisTerakhir = 0; ?>
                                @if(count($detail) > 0)
                                <?php
                                $total = 0;
                                $totalVAT = 0;
                                ?>
                                @foreach($detail as $data)
                                <?php
                                $sumMrv = MrvHeader::getSum($data->InventoryInternalID, $header->InternalID, $data->InternalID);
                                if ($sumMrv == '') {
                                    $sumMrv = '0.00';
                                }
                                ?>
                                <tr id="row{{$barisTerakhir}}">
                                    <td class="left">{{'';$inventory = Inventory::find($data->InventoryInternalID); echo $inventory->InventoryID.' '.$inventory->InventoryName}}</td>
                                    <td class="left">{{$data->Uom->UomID}}</td>
                                    <td class="right" id="qtyMrv-{{$barisTerakhir}}">{{number_format($data->Qty,'0','.',',')}}</td>
                                    <!--<td class="right">{{number_format($data->Price,'2','.',',')}}</td>
                                    <td class="right">{{$data->Discount.''}}</td>
                                    <td class="right">{{$data->Discount1.''}}</td>
                                    <td class="right">{{number_format($data->DiscountNominal,'2','.',',')}}</td>
                                    {{'';$totalVAT += $data->VAT}}
                                    <td class="right" id="subtotalMrv-{{$barisTerakhir}}">{{number_format($data->SubTotal,'2','.',',');$total += $data->SubTotal}}</td>
                                    --><td class="right">{{number_format(($data->Qty-$sumMrv),'0','.',',')}}</td>
                                    <td class="text-right">
                                        <input type="hidden" class="maxWidth right" name="InternalDetail[]" value="{{$data->InternalID}}">
                                        <input type="hidden" class="maxWidth right" name="inventory[]" value="{{$data->InventoryInternalID}}">
                                        <input type="hidden" class="maxWidth right" name="uom[]" value="{{$data->UomInternalID}}">
                                        <input type="hidden" class="maxWidth right" name="qty[]" value="{{$data->Qty}}">
                                        <input type="hidden" class="maxWidth right" name="price[]" value="{{$data->Price}}">
                                        <input type="hidden" class="maxWidth right" name="discount[]" value="{{$data->Discount}}">
                                        <input type="hidden" class="maxWidth right" name="discount1[]" value="{{$data->Discount1}}">
                                        <input type="hidden" class="maxWidth right" name="discountNominal[]" value="{{$data->DiscountNominal}}">
                                        <input type="text" class="maxWidth right Mrv numaja quantityMrv" id="quantityMrv-{{$barisTerakhir}}" name="Mrv[]" min="0" max="{{$data->Qty-$sumMrv}}" value="{{number_format(($data->Qty-$sumMrv),'0','.',',')}}">
                                    </td>
                                </tr>
                                {{'';$barisTerakhir++;}}
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="5">There is no inventory registered in this mrv.</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        <input type="hidden" name="grandTotalValue" value="0" id="grandTotalValue">
                        <!--<table class="pull-right"> 
                            <tr>
                                <td><h5 class="right margr10 h5total"><b>Total</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b id="total"></b></h5></td>
                            </tr>
                            <tr >
                            <input type="hidden" id="discountGlobalMax" value="{{($header->DiscountGlobal - MrvHeader::getSumDiscountGlobalMrv($header->InternalID))}}">
                            <td><h5 class="right margr10 h5total"><b>Discount (Max {{number_format(($header->DiscountGlobal - MrvHeader::getSumDiscountGlobalMrv($header->InternalID)),'0','.',',')}})</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total new" style="width: 150px;"><input type="text" style="width: 120px;" class="maxWidth right numajaDesimal discountGlobal" name="DiscountGlobal" maxlength="" id="discountGlobal" value="0"></h5></td>
                            </tr>
                            <tr >
                                <td><h5 class="right margr10 h5total"><b>Down Payment</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                                <td><h5 class="right margr10 h5total new" style="width: 150px;"><input type="text" style="width: 120px;" class="maxWidth right numajaDesimal downPayment" name="DownPayment" maxlength="" id="dp" value="0"></h5></td>
                            </tr>
                            <tr>
                                <td><h5 class="right margr10 h5total"><b>Grand Total</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b id="grandTotal"></b></h5></td>
                            </tr>

                            <tr>
                                <td><h5 class="right margr10 h5total hidevat"><b>Tax</b></h5></td>
                                <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b id="tax"></b></h5></td>
                            </tr>
                            <tr>
                                <td><h5 class="right margr10 h5total hidevat"><b>Grand Total(tax)</b></h5></td>
                                <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b id="grandTotalAfterTax"></b></h5></td>
                            </tr>
                        </table>-->
                    </div><!---- end div tableadd---->      
                </div><!---- end div tableadd---->   
            </div><!---- end div tabwrap---->  
            <div class="btnnest pull-right">
                <button class="btn btn-green btn-sm btn-save" id="btn-save"> Save </button>
            </div>
        </form>
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop

@section('modal')
<div class="modal fade" id="insert" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insert Mrv </h4>
            </div>
            <form action="" method="post" class="action">
                {{'';$hitung = 0;}}
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="insertMrv" id="jenis" name="jenis">
                            <li>
                                <label for="mrv">Purchase Order ID</label> *
                            </li>
                            <input class="input-theme margbot10" type="text" id="searchPurchaseOrder" title="Type Purchase Order Name or ID then 'Enter'" placeholder="Type Mrv Name or ID then 'Enter'">
                            <li id="selectPurchaseOrder">

                            </li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btn-add-po" @if($hitung == 0){{'disabled'}}@endif class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>   
        </div>
    </div>  
</div>

<div class="modal fade" id="r_summary" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Summary Mrv</h4>
            </div>
            <form action="" method="post" class="action" id="" target="_blank">
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='summaryMrv'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateReport" data-validation="required">
                            </li>
                        </ul>
                    </div>
                    <div class="required">
                        * Required
                    </div>
                </div>
                <div class="modal-footer">
                    <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                    <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNewJournal.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script>
$("form").bind("submit", function (event) {
    setTimeout(function () {
        if ($(".form-error")[0]) {
            $("#btn-save").prop("disabled", false);
        } else {
            $("#btn-save").prop("disabled", true);
        }
    }, 100);

});
var getResultSearchPO = "<?php echo Route("getResultSearchPO") ?>";
var tanggalHariIni = '<?php echo date('d') . '-' . date('m') . '-' . date('Y') ?>';
var a = '<?php echo Route('formatCariIDMrv') ?>';
</script>
<script>
    var mrvDataBackup = '<?php echo Route('mrvDataBackup', Input::get('warehouseatas') . '---;---' . Input::get('startDate') . '---;---' . Input::get('endDate')) ?>';
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-pembelian-add/mrv.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-pembelian-add/mrvNew.js')}}"></script>
@stop