@extends('template.header-footer')

@section('title')
Purchase 
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
@if(checkModul('O05'))
<style>
    .li_detail{
        clear: both;
        padding-top: 10px;
    }
    .label_detail{
        float: left;
    }
    .div_detail{
        float: left; width: 65%
    }
</style>
<?php $element = 'div'; ?>
@endif
<style>
    .li_detail{
    }
    .label_detail{
    }
    .div_detail{
    }
</style>
<?php $element = 'span'; ?>
@stop

@section('nav')

@stop

@section('content')
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest"> 
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showPurchase')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Purchase</a>
                <a href="{{route('purchaseDetail', $header->PurchaseID)}}" type="button" class="btn btn-sm btn-pure">Detail {{$header->PurchaseID}}</a>
            </div>
            <div class="btn-group margr5">
                <button type="button" class="btn btn-green btn-sm dropdown-toggle" data-target="#insert" data-toggle="modal" role="dialog"
                        aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
            </div>
            <button  <?php if (myCheckIsEmpty('Purchase')) echo 'disabled'; ?> id="search-button" class="btn btn-green btn-sm margr5"><span class="glyphicon glyphicon-search"></span> Search </button>
            @if(checkModul('O04'))
            <button type="button" class="btn btn-green margr5" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rSummary">
                <span class="glyphicon glyphicon-file"></span> Summary Report</button>
            <button type="button" class="btn btn-green margr5" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rDetail">
                <span class="glyphicon glyphicon-file"></span> Detail Report</button>
            <button type="button" class="btn btn-green margr5" data-target="#r_purchase" data-toggle="modal" role="dialog" id="btn-rPurchase">
                <span class="glyphicon glyphicon-file"></span> Purchase Report</button>
            @endif
            @if(!PurchaseHeader::isReturn($header->PurchaseID))
            @if((JournalDetail::where('JournalTransactionID', $header->PurchaseID)->sum('JournalCreditMU') == 0 && $header->isCash != 0) || $header->isCash == 0)
            <a href="{{Route('purchaseUpdate',$header->PurchaseID)}}">
                <button id="btn-{{$header->PurchaseID}}-update"
                        class="btn btn-green btn-sm margr5">
                    <span class="glyphicon glyphicon-edit"></span> Edit
                </button>
            </a>
            @else
            <button disabled class="btn btn-green btn-sm margr5"><span class="glyphicon glyphicon-edit"></span> Edit</button>
            @endif
            @else
            <button disabled class="btn btn-green btn-sm margr5"><span class="glyphicon glyphicon-edit"></span> Edit</button>
            @endif
            @if(checkModul('O04'))
            <a href="{{Route('purchasePrint',$header->PurchaseID)}}" target='_blank' style="margin-right: 0px !important;">
                <button id="btn-{{$header->PurchaseID}}-print"
                        class="btn btn-green btn-sm ">
                    <span class="glyphicon glyphicon-print"></span> Print
                </button>
            </a>
<!--            <a href="{{Route('purchasePrintSJ',$header->PurchaseID)}}" target='_blank' style="margin-right: 0px !important;">
                <button id="btn-{{$header->PurchaseID}}-printSJ"
                        class="btn btn-green btn-sm ">
                    <span class="glyphicon glyphicon-print"></span> Print SJ
                </button>
            </a>-->
            @endif
        </div>
        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Search</h4>
            </div>
            <div class="tableadd">     
                <ul class="searchmenu">
                    <form method="GET" action="{{Route('showPurchase')}}">
                        <li><label for="typePayment">Payment Type</label>
                            <br>
                            <select name="typePayment" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Cash</option>
                                <option value="1">Credit</option>  
                                <option value="2">CBD</option>  
                                <option value="3">Deposit</option>  
                                <option value="4">Down Payment</option>  
                            </select>
                        </li>
                        <li><label for="typeTax">Tax Type</label>
                            <br>
                            <select name="typeTax" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Non Tax</option>
                                <option value="1">Tax</option>  
                            </select>
                        </li>
                        <li><label for="date">Start Date</label>
                            <br><input id="startDate" name="startDate" type="text" autocomplete="off">
                        </li>
                        <li><label for="date">End Date</label>
                            <br><input id="endDate" name="endDate" type="text" autocomplete="off">
                        </li>
                        <li><br>
                            <button type="submit" class="btn btn-green btn-sm">Search <span class="glyphicon glyphicon-search"></span></button>
                        </li>
                        <li><br>
                            <button id="cancel" type="reset"  class="btn btn-pure btn-sm">Cancel <span class="glyphicon glyphicon-remove"></span></button>
                        </li>
                    </form>
                </ul>
            </div><!---- end div tableadd---->
        </div><!---- end div tabwrap---->

        <div class="tabwrap">
            <div class="tabhead">
                <h4 class="headtitle">{{'Purchase  '.$header->PurchaseID}}</h4>
            </div>
            <div class="tableadd"> 
                <div class="headinv new">
                    <ul @if(checkModul('O05')) class="pull-left" style="width: 360px;" @else class="pull-left"  @endif>
                         <li>
                            <label for="orderID" class="label_detail">MRV ID</label>
                            <{{$element}} class="div_detail">{{$header->MrvHeader->MrvID}}</{{$element}}>
                        </li>
                        <li class="li_detail">
                            <label for="date" style="float: left;">Date</label>
                            <{{$element}} class="div_detail">{{date( "d-m-Y", strtotime($header->PurchaseDate))}}</{{$element}}>
                        </li>
                        <li class="li_detail">
                            <label for="supplier" style="float: left;">Supplier</label>
                            <{{$element}} class="div_detail">
                            <?php
                            $coa6 = PurchaseHeader::find($header->InternalID)->coa6;
                            echo $coa6->ACC6ID . ' ' . $coa6->ACC6Name
                            ?>
                            </{{$element}}>
                        </li>
                        <li class="li_detail">
                            <label for="longTerm" style="float: left;">Payment</label>
                            @if($header->isCash == 0)
                            <{{$element}} class="div_detail">{{'Cash'}}</{{$element}}>
                            @elseif($header->isCash == 1)
                            <{{$element}} class="div_detail">{{'Credit'}}</{{$element}}>
                            @elseif($header->isCash == 2)
                            <{{$element}} class="div_detail">{{'CBD'}}</{{$element}}>
                            @elseif($header->isCash == 3)
                            <{{$element}} class="div_detail">{{'Deposit'}}</{{$element}}>
                            @elseif($header->isCash == 4)
                            <{{$element}} class="div_detail">{{'Down Payment'}}</{{$element}}>
                            @endif
                        </li>
                        @if($header->isCash == 4)
<!--                        <li class="li_detail">
                            <label for="longTerm" style="float: left;">Down Payment</label>
                            <{{$element}} class="div_detail">{{$header->DownPayment}}</{{$element}}>
                        </li>-->
                        @endif
                        @if($header->isCash != 0 && $header->isCash != 2 && $header->isCash != 3)
                        <li class="li_detail">
                            <label for="longTerm" style="float: left;">Due Date</label>
                            <{{$element}} class="div_detail">{{date( "d-m-Y", strtotime("+".$header->LongTerm." day",strtotime($header->PurchaseDate)))}}</{{$element}}>
                        </li>
                        @endif
                    </ul>
                    <ul @if(checkModul('O05')) class="pull-left" style="width: 360px;" @else class="pull-right"  @endif>
                         <li>
                            <label for="payment" style="float: left;">Status</label>
                            <?php
                            $tampPay = 'Completed';
                            foreach (PurchaseHeader::getPurchasePayable() as $pay) {
                                if ($pay->ID == $header->PurchaseID) {
                                    $tampPay = 'Uncompleted';
                                }
                            }
                            ?>
                            <{{$element}} class="div_detail">{{$tampPay}}</{{$element}}>
                        </li>
                        <li class="li_detail">
                            <label for="warehouse" style="float: left;">Warehouse</label>
                            <{{$element}} class="div_detail">{{$header->Warehouse->WarehouseName}}</{{$element}}>
                        </li>
                        <li class="li_detail">
                            <label for="currency" style="float: left;">Currency</label>
                            <{{$element}} class="div_detail">{{$header->Currency->CurrencyName}}</{{$element}}>
                        </li>
                        <li class="li_detail">
                            <label for="rate" style="float: left;">Rate</label>
                            <{{$element}} class="div_detail">{{number_format($header->CurrencyRate,'2','.',',')}}</{{$element}}>
                        </li>
                        <li class="li_detail">
                            <label for="VAT" style="float: left;">VAT</label>
                            @if($header->VAT == 0)
                            <{{$element}} class="div_detail">{{'Non Tax'}}</{{$element}}>
                            @else
                            <{{$element}} class="div_detail">{{'Tax'}}</{{$element}}>
                            @endif
                        </li>
                        <li class="li_detail">
                            <label for="" style="float: left;">Remark</label>
                            <{{$element}} class="div_detail">{{$header->Remark}}</{{$element}}>
                        </li>
                    </ul>

                    @if(checkModul('O05'))
                    <ul class="pull-left" style="width: 360px;">
                        <li>
                            <label for="transactionType" style="float: left;">Transaction</label>
                            @if($header->TransactionType == 1)
                            <div style="float: left; width: 65%">For who is not collect PPN</div>
                            @elseif($header->TransactionType == 2)
                            <div style="float: left; width: 65%">For Chamberlain</div>
                            @elseif($header->TransactionType == 3)
                            <div style="float: left; width: 65%">Except Chamberlain</div>
                            @elseif($header->TransactionType == 4)
                            <div style="float: left; width: 65%">DPP other value</div>
                            @elseif($header->TransactionType == 6)
                            <div style="float: left; width: 65%">Other handover, include handover to foreigner tourist in the event of VAT refund</div>
                            @elseif($header->TransactionType == 7)
                            <div style="float: left; width: 65%">Handover PPN is not collect</div>
                            @elseif($header->TransactionType == 8)
                            <div style="float: left; width: 65%">Handover PPN Freed</div>
                            @elseif($header->TransactionType == 9)
                            <div style="float: left; width: 65%">Handover Assets (Pasal 16D UU PPN)</div>
                            @endif
                        </li>
                        <li class="li_detail">
                            <label for="replacement" style="float: left;">Replacement</label>
                            @if($header->Replacement == 1)
                            <div style="float: left; width: 65%">Tax Replacement</div>
                            @else
                            <div style="float: left; width: 65%">Non Tax Replacement</div>
                            @endif
                        </li>
                        <li class="li_detail">
                            <label for="Taxnumber" style="float: left;">Tax Number</label>
                            <div style="float: left; width: 65%">{{$header->TaxNumber}}</div>
                        </li>
                        <li class="li_detail">
                            <label for="Taxmonth" style="float: left;">Tax Month</label>
                            <div style="float: left; width: 65%">{{date('F',strtotime('2015-'.$header->TaxMonth.'-01'))}}</div>
                        </li>
                        <li class="li_detail">
                            <label for="TaxYear" style="float: left;">Tax Year</label>
                            <div style="float: left; width: 65%">{{$header->TaxYear}}</div>
                        </li>
                    </ul>
                    @endif
                </div>
                <div class="padrl10">
                    <label>Inventory Detail</label>
                    <table class="table master-data " id="table-purchase" >
                        <thead>
                            <tr>
                                <th>Inventory</th>
                                <th>Uom</th>
                                <th>Qty</th>
                                <th>Price</th>
                                <th>Disc (%)</th>
                                <th>Disc 2 (%)</th>
                                <th>Disc</th>
                                <th>Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($detail) > 0)
                            <?php
                            $total = 0;
                            $totalVAT = 0;
                            ?>
                            @foreach($detail as $data)
                            <tr>
                                <td class="left">{{'';$inventory = Inventory::find($data->InventoryInternalID); echo $inventory->InventoryID.' '.$inventory->InventoryName}}</td>
                                <td class="left">{{$data->Uom->UomID}}</td>
                                <td class="right">{{number_format($data->Qty,'0','.',',')}}</td>
                                <td class="right">{{number_format($data->Price,'2','.',',')}}</td>
                                <td class="right">{{$data->Discount.''}}</td>
                                <td class="right">{{$data->Discount1.''}}</td>
                                <td class="right">{{number_format($data->DiscountNominal,'2','.',',')}}</td>
                                {{'';$totalVAT += $data->VAT}}
                                <td class="right">{{number_format(ceil($data->SubTotal),'0','.',',');$total += $data->SubTotal}}</td>
                            </tr>
                            @endforeach
                            @if($totalVAT != 0)
                            {{'';//$totalVAT = $totalVAT - $header->DiscountGlobal*0.1;
                            $totalVAT = ($header->GrandTotal/11 );}}
                            @endif
                            @else
                            <tr>
                                <td colspan="6">There is no inventory registered in this purchase .</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                    <?php $order = PurchaseOrderHeader::find(MrvHeader::find($header->MrvInternalID)->PurchaseOrderInternalID); ?>
                    @if($order->Import == 1)
                    <label>Cost Detail</label>
                    <table class="table master-data " id="table-cost" >
                        <thead>
                            <tr>
                                <th>Cost</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count(PurchaseCostDetail::where('PurchaseInternalID',$header->InternalID)->get()) > 0)
                            @foreach(PurchaseCostDetail::where('PurchaseInternalID',$header->InternalID)->get() as $data)
                            <tr>
                                <td class="left">{{'';$cost = Cost::find($data->CostInternalID); echo $cost->CostName}}</td>
                                <td class="right">{{number_format($data->TotalCost,'2','.',',')}}</td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="2">There is no cost registered in this purchase order.</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                    @endif
                    <table class="pull-left"> 
                        <tr>
                            <td></td>
                        </tr>
                        <tr>
                            <td><p>Created by {{User::where('UserID', $header->UserRecord)->first()->UserName.' '.date('d-m-Y H:i:s', strtotime($header->dtRecord))}}</p></td>
                        </tr>
                        <tr>
                            @if($header->UserModified != '0')
                            <td><p>Modified by {{User::where('UserID', $header->UserModified)->first()->UserName.' '.date('d-m-Y H:i:s', strtotime($header->dtModified))}}</p></td>
                            @else
                            <td><p>Modified by -</p></td>
                            @endif
                        </tr>
                    </table>
                    @if(count($detail) > 0)


                    <table class="pull-right"> 
                        <tr>
                            <td><h5 class="right margr10 h5total"><b>Total</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="total">{{number_format(ceil($total),'0','.',',')}}</b></h5></td>
                        </tr>
                        <tr >
                            <td><h5 class="right margr10 h5total"><b>Discount</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total new"><b>{{number_format($header->DiscountGlobal, '0', '.',',')}}</b></h5></td>
                        </tr>

                        <tr >
                            <td><h5 class="right margr10 h5total"><b>Down Payment</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total new"><b>{{number_format($header->DownPayment, '0', '.',',')}}</b></h5></td>
                        </tr>

                        <tr>
                            <td><h5 class="right margr10 h5total"><b>Grand Total</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="grandTotal">{{number_format(ceil($total-$header->DiscountGlobal-$header->DownPayment),'0','.',',')}}</b></h5></td>
                        </tr>

                        <tr>
                            <td><h5 class="right margr10 h5total hidevat"><b>Tax</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                            @if($totalVAT == 0)
                            <td><h5 class="right margr10 h5total"><b id="tax">{{number_format(0,'0','.',',')}}</b></h5></td>
                            @else
                            <td><h5 class="right margr10 h5total"><b id="tax">{{number_format(floor(ceil($total-$header->DiscountGlobal-$header->DownPayment)/10),'0','.',',')}}</b></h5></td>
                            @endif
                        </tr>
                        <tr>
                            <td><h5 class="right margr10 h5total hidevat"><b>Grand Total(tax)</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="grandTotalAfterTax">{{number_format(ceil($header->GrandTotal),'0','.',',')}}</b></h5></td>
                        </tr>
                    </table>
                    @endif
                </div><!---- end div padrl10---->         
            </div><!---- end div tableadd---->   
        </div><!---- end div tabwrap---->                 
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->

@stop


@section('modal')
<div class="modal fade" id="insert" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insert Purchase</h4>
            </div>
            <form action="" method="post" class="action">
                {{'';$hitung = 0;}}
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="insertPurchase" id="jenis" name="jenis">
                            <li>
                                <label for="purchase">MRV ID</label> *
                            </li>
                            <input class="input-theme margbot10" type="text" id="searchPurchaseOrder" title="Type MRV ID or Supplier Name then 'Enter'" placeholder="Type Purchase Name or ID then 'Enter'">
                            <li id="selectPurchaseOrder">

                            </li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btn-add-po" @if($hitung == 0){{'disabled'}}@endif class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>    
        </div>
    </div>  
</div>

<div class="modal fade" id="r_summary" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Summary Purchase</h4>
            </div>
            <form action="" method="post" class="action" id="" target="_blank">
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='summaryPurchase'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateReport" data-validation="required">
                            </li>
                        </ul>
                    </div>
                    <div class="required">
                        * Required
                    </div>
                </div>
                <div class="modal-footer">
                    <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                    <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade" id="r_purchase" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Purchase Report</h4>
            </div>
            <form action="" method="post" class="action" id="" target="_blank">
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='purchaseReport'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDatePurchaseReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDatePurchaseReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">Supplier</label> *
                            </li>
                            <li>
                                <select class='chosen-select' name='supplier'>
                                    <option value='-1'>All</option>
                                    @foreach(Coa6::where('CompanyInternalID',Auth::user()->CompanyInternalID)->where('Type','s')->get() as $c)
                                    <option value='{{$c->InternalID}}'>{{$c->ACC6Name}}</option>
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label for="eDate"></label>
                                <input type="checkbox" value="hide" name="hide">Hide Column
                            </li>
                        </ul>
                    </div>
                    <div class="required">
                        * Required
                    </div>
                </div>
                <div class="modal-footer">
                    <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                    <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script><script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script>
var purchaseDataBackup = '<?php echo Route('purchaseDataBackup', Input::get('typePayment') . '---;---' . Input::get('typeTax') . '---;---' . Input::get('startDate') . '---;---' . Input::get('endDate')) ?>';
</script>
<script>
    var getResultSearchPO = "<?php echo Route("getResultSearchMRV") ?>";
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-pembelian-add/purchaseAdd.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
@stop