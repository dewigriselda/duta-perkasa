@extends('template.header-footer')

@section('title')
Purchase Order
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<style>
    .help-block {
        margin-left: 30% !important;
    }
</style>
@stop

@section('nav')

@stop

@section('content')
@if(myCheckIsEmpty('Supplier;Warehouse;Currency;Inventory'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one master Supplier, Currency, Warehouse, and Inventory to insert purchase order.
</div>
@endif
@if(isset($messages))
@if($messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Purchase Order has been updated.
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest"> 
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showPurchaseOrder')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Purchase Order</a>
                <a href="{{route('purchaseOrderUpdate', $header->PurchaseOrderID)}}" type="button" class="btn btn-sm btn-pure ">Update {{$header->PurchaseOrderID}}</a>
            </div>
            <div class="btn-group margr5">
                @if (myCheckIsEmpty('Supplier;Warehouse;Currency;Inventory'))
                <button type="button" disabled class="btn btn-green btn-sm dropdown-toggle " aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
                <button type="button" disabled class="btn btn-green btn-sm dropdown-toggle " aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> Repeat Order </button>
                @else
                <a href="{{Route('purchaseOrderNew')}}">
                    <button type="button"  class="btn btn-green btn-smdropdown-toggle " aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>    
                </a>
                @endif
            </div>
            <button id="search-button" <?php if (myCheckIsEmpty('PurchaseOrder')) echo 'disabled'; ?> class="btn btn-green btn-sm margr5"> <span class="glyphicon glyphicon-search"></span> Search </button>
            @if(checkModul('O04'))
            <button type="button" class="btn btn-green margr5" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rSummary">
                <span class="glyphicon glyphicon-file"></span> Summary Report</button>
            <button type="button" class="btn btn-green" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rDetail">
                <span class="glyphicon glyphicon-file"></span> Detail Report</button>
            @endif
            <a href="{{Route('outstandingPO')}}" target='_blank' style="margin-right: 0px !important;">
                <button class="btn btn-green btn-sm ">
                    <span class="glyphicon glyphicon-file"></span> Outstanding PO
                </button>
            </a>
        </div>
        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Search</h4>
            </div>
            <div class="tableadd">     
                <ul class="searchmenu">
                    <form method="GET" action="{{Route('showPurchaseOrder')}}">
                        <li><label for="typePayment">Payment Type</label>
                            <br>
                            <select name="typePayment" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Cash</option>
                                <option value="1">Credit</option> 
                                <option value="2">CBD</option>  
                                <option value="3">Deposit</option>  
                                <option value="4">Down Payment</option>  
                            </select>
                        </li>
                        <li><label for="typeTax">Tax Type</label>
                            <br>
                            <select name="typeTax" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Non Tax</option>
                                <option value="1">Tax</option>  
                            </select>
                        </li>
                        <li><label for="date">Start Date</label>
                            <br><input id="startDate" name="startDate" type="text" autocomplete="off">
                        </li>
                        <li><label for="date">End Date</label>
                            <br><input id="endDate" name="endDate" type="text" autocomplete="off">
                        </li>
                        <li><br>
                            <button type="submit" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-search"></span> Search </button>
                        </li>
                        <li><br>
                            <button id="cancel" type="reset"  class="btn btn-pure btn-sm"><span class="glyphicon glyphicon-remove"></span>  Cancel </button>
                        </li>
                    </form>
                </ul>
            </div><!---- end div tableadd---->
        </div><!---- end div tabwrap---->
        <form method="POST" action="" enctype="multipart/form-data">
            <input type='hidden' name='PurchaseOrderID' value='{{$header->InternalID}}'>
            <div class="tabwrap">
                <div class="tabhead">
                    <h4 class="headtitle">Purchase Order <span id="purchaseOrderID">{{$header->PurchaseOrderID}}</span></h4>
                </div>
                <div class="tableadd"> 
                    <div class="headinv new">
                        <ul class="pull-left">
                            <li>
                                <label>Type</label>
                                <input type="radio" name="jenispo" id="checkpolocal" value="local" @if($header->Import == 0) checked @endif > Local
                                       <input type="radio" name="jenispo" id="checkpoimport" value="import" @if($header->Import == 1) checked @endif > Import
                            </li>
                            <li>
                                <label for="coa6">Supplier *</label>
                                <select class="chosen-select" id="coa6" style="" name="coa6">
                                    @foreach(Coa6::where("Type", "s")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $coa6)
                                    @if($coa6->InternalID == $header->ACC6InternalID)
                                    <option selected="selected" value="{{$coa6->InternalID}}">
                                        {{$coa6->ACC6ID.' '.$coa6->ACC6Name}}
                                    </option>
                                    @else
                                    <option value="{{$coa6->InternalID}}">
                                        {{$coa6->ACC6ID.' '.$coa6->ACC6Name}}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>
                            </li>     
                            <!--                            <li>
                                                            <label for="payment">Payment *</label>
                                                            @if($header->isCash == 0)  
                                                            <input type="radio" class="radio-tipe" id="paymentCash" name="isCash" value="0" checked="checked">Cash
                                                            <input type="radio" class="radio-tipe" id="paymentCredit" name="isCash" value="1">Credit      
                                                            @else           
                                                            <input type="radio" class="radio-tipe" id="paymentCash" name="isCash" value="0">Cash
                                                            <input type="radio" class="radio-tipe" id="paymentCredit" name="isCash" value="1" checked="checked">Credit      
                                                            @endif               
                                                        </li>  -->
                            <li>
                                <label for="payment">Payment *</label>
                                <select name="isCash" id="payment">
                                    <option value="0" @if($header->isCash == 0)selected @endif >Cash</option>
                                    <option value="1" @if($header->isCash == 1)selected @endif >Credit</option>
                                    <option value="2" @if($header->isCash == 2)selected @endif >CBD</option>
                                    <option value="3" @if($header->isCash == 3)selected @endif >Deposit</option>
                                    <option value="4" @if($header->isCash == 4)selected @endif >Down Payment</option>
                                </select>
                            </li>
                             <li class="downpayment">
                                <label for="longTerm">Down Payment Type</label>
                                <select name="jenisDP" id="jenisdp">
                                    <option value="0" @if($header->DownPayment > 0)selected @endif >Percentage</option>
                                    <option value="1" @if($header->DPValue > 0)selected @endif >Nominal</option>
                                </select>
                            </li>
                            <li class="downpayment persen">
                                <label for="longTerm">Down Payment (%)*</label>
                                <input type="text" name="downPayment" id="" class="numajaDesimal price2" value="{{number_format($header->DownPayment,'2','.',',')}}">
                            </li>
                            <li class="downpayment dpnominal">
                                <label for="longTerm">Down Payment *</label>
                                <input type="text" name="downPaymentValue" id="downPayment" class="numajaDesimal price2" value="{{number_format($header->DPValue,'2','.',',')}}">
                            </li>
                            <li>
                                <label for="longTerm">Long Term (day) *</label>
                                @if($header->isCash == 0 || $header->isCash == 2 || $header->isCash == 3)
                                <input type="text" name="longTerm" id="longTerm" value="0" disabled>
                                @else
                                <input type="text" class="numaja" name="longTerm" id="longTerm"  value='{{$header->LongTerm}}'>
                                @endif
                            </li>  
                            <!--                            <li>
                                                            <label for="warehouse">Warehouse *</label>
                                                            <select class="chosen-select choosen-modal warehouse" id="warehouseHeader" name="warehouse">
                                                                @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $warehouse)
                                                                @if($warehouse->InternalID == $header->WarehouseInternalID)
                                                                <option selected="selected" id="warehouse{{$warehouse->InternalID}}" value="{{$warehouse->InternalID}}">
                                                                    {{$warehouse->WarehouseName;}}
                                                                </option>
                                                                @else
                                                                <option id="warehouse{{$warehouse->InternalID}}" value="{{$warehouse->InternalID}}">
                                                                    {{$warehouse->WarehouseName;}}
                                                                </option>
                                                                @endif
                                                                @endforeach
                                                            </select>
                                                        </li>-->
                            <li>
                                <label for="warehouse">Import Order</label>
                                <input type="file" name="importorder">
                            </li>
                        </ul>
                        <ul class="pull-right">
                            <li>
                                <label for="longTerm">Attn</label>
                                <input type="text" name="attn" id="attn" value="{{$header->Attn}}">
                            </li>
                            <li>
                                <label for="longTerm">Kirim Via</label>
                                <input type="text" name="kirimvia" id="kirimvia" value="{{$header->KirimVia}}">
                            </li>
                            <li>
                                <label for="longTerm">Request By</label>
                                <input type="text" name="requestby" id="requestby" value="{{$header->RequestBy}}">
                            </li>
                            <li>
                                <label for="currency">Currency *</label>
                                <select class="chosen-select choosen-modal currency" id="currencyHeader" name="currency">
                                    @foreach(Currency::orderBy('Default','desc')->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $cur)
                                    @if($cur->InternalID == $header->CurrencyInternalID)
                                    <option selected="selected" id="cur{{$cur->InternalID}}" value="{{$cur->InternalID.'---;---'.$cur->CurrencyName.'---;---'.$cur->Rate.'---;---'.$cur->Default}}">
                                        {{$cur->CurrencyName;}}
                                    </option>
                                    @else
                                    <option id="cur{{$cur->InternalID}}" value="{{$cur->InternalID.'---;---'.$cur->CurrencyName.'---;---'.$cur->Rate.'---;---'.$cur->Default}}">
                                        {{$cur->CurrencyName;}}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label for="rate">Rate *</label>
                                <input type="text" class="maxWidth rate numajaDesimal" name="rate" maxlength="" id="rate" value="{{$header->CurrencyRate}}" data-validation="required">
                            </li>
                            <li>
                                <label for="vat">VAT *</label>
                                @if($header->VAT == 0) 
                                <input style="width:15px; height: 15px;" type="checkbox" name="vat" id="vat" value="1"> Tax 10%
                                @else
                                <input style="width:15px; height: 15px;" type="checkbox" checked name="vat" id="vat" value="1"> Tax 10%
                                @endif
                            </li>
                            <li>
                                <label for="remark">Remark</label>
                                <textarea name="remark" id="remark">{{$header->Remark}}</textarea>
                            </li>
                            <li>
                                <div class="required">
                                    * Required
                                </div>
                            </li>
                        </ul>
                    </div>
                    <?php $arrInv = array(); ?>
                    <div class="tableadd journupdate">
                        <label>Inventory Detail</label>
                        <table class="table master-data" id="table-purchaseOrder" style="table-layout:fixed;">
                            <thead>
                                <tr>
                                    <th style="width: 25%;">Inventory</th>
                                    <th style="width: 10%;">Uom</th>
                                    <th style="width: 10%;">Qty</th>
                                    <th style="width: 10%;">Price</th>
                                    <th style="width: 10%;">Disc (%)</th>
                                    <th style="width: 10%;">Disc 2 (%)</th>
                                    <th style="width: 10%;">Disc</th>
                                    <th style="width: 10%;">Subtotal</th>
                                    <th style="width: 20%;">Action</th>
                                </tr>
                            </thead>
                            <?php
                            $inventoryInternalID = Inventory::select('m_inventory.*')->distinct()->join('m_inventory_uom', 'm_inventory_uom.InventoryInternalID', '=', 'm_inventory.InternalID')
                                            ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)->first()->InternalID;
                            ?>
                            <input type="hidden" name="HidInternalIDFirst" id="hidInternalIDFirst" value="{{$inventoryInternalID}}">
                            <tbody>
                                <?php $barisTerakhir = 0; ?>
                                @if(count($detail) > 0)
                                <tr id="row0" style="background-color: #e5e5e5 !important">
                                    <td class="chosen-uom" style="border-color: #d8d8d8 !important">
                                        <input class="input-theme margbot10" type="text" id="searchInventory" tabindex="-1" title="Type Inventory Name or ID then 'Enter'" placeholder="Type Inventory Name or ID then 'Enter'">
                                        <div id="selectInventory">

                                        </div>
                                    </td>
                                    <td style="border-color: #d8d8d8 !important">
                                        <select id="uom-0" class="input-theme uom">

                                        </select>
                                    </td>
                                    <td class="text-right" style="border-color: #d8d8d8 !important">
                                        <input type="text" class="maxWidth qty right numaja input-theme" maxlength="11" min="1" id="price-0-qty" value="1">
                                    </td>
                                    <td class="text-right" style="border-color: #d8d8d8 !important">
                                        <input type="text" class="maxWidth price right numajaDesimal input-theme" maxlength="" id="price-0" value="0">
                                    </td>
                                    <td class="text-right" style="border-color: #d8d8d8 !important">
                                        <input type="text" class="maxWidth discount right numajaDesimal input-theme" id="price-0-discount" value="0.00">
                                    </td>
                                    <td class="text-right" style="border-color: #d8d8d8 !important">
                                        <input type="text" class="maxWidth discount1 right numajaDesimal input-theme" id="price-0-discount1" value="0.00">
                                    </td>
                                    <td class="text-right" style="border-color: #d8d8d8 !important">
                                        <input type="text" class="maxWidth discountNominal right numajaDesimal input-theme" id="price-0-discountNominal" value="0.00">
                                    </td>
                                    <td id="price-0-qty-hitung" class="right" style="border-color: #d8d8d8 !important">
                                        0.00
                                    </td>
                                    <td style="border-color: #d8d8d8 !important">
                                        <button type="button" class="btn btn-green btn-sm" disabled="" id="btn-addRow"><span class="glyphicon glyphicon-plus"></span></button>
                                    </td>
                                </tr>
                                {{'';$barisTerakhir++;}}
                                @foreach($detail as $data)
                                <tr id="row{{$barisTerakhir}}">
                                    <td class="chosen-transaction">
                                        <?php $inventory = Inventory::find($data->InventoryInternalID); ?>
                                        <?php $arrInv[$barisTerakhir] = $inventory->InternalID . "---;---inventory" ?>
                                        <input type="hidden" class="inventory" id="inventory-{{$barisTerakhir}}" style="" name="inventory[]" value="{{$inventory->InternalID}}---;---inventory">
                                        {{$inventory->InventoryID}} 
                                        {{" ".$inventory->InventoryName}}
                                    </td>
                                    <td>
                                        <select id="uom-{{$barisTerakhir}}" name="uom[]" class="input-theme uom">
                                            @foreach (InventoryUom::where("InventoryInternalID", $data->InventoryInternalID)->get() as $uom)
                                            <option value="{{$uom->UomInternalID}}" {{ ($uom->UomInternalID == $data->UomInternalID) ? "selected" : ""}}>{{ $uom->Uom->UomID; }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth qty right numaja input-theme" name="qty[]" maxlength="11" min="1" id="price-{{$barisTerakhir}}-qty" value="{{number_format($data->Qty,'0','.',',')}}">
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth price right numajaDesimal input-theme" name="price[]" maxlength="" id="price-{{$barisTerakhir}}" value="{{number_format($data->Price,'2','.',',')}}">
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth discount right numajaDesimal input-theme" name="discount[]" id="price-{{$barisTerakhir}}-discount" value="{{$data->Discount}}">
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth discount1 right numajaDesimal input-theme" name="discount1[]" id="price-{{$barisTerakhir}}-discount1" value="{{$data->Discount1}}">
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth discountNominal right numajaDesimal input-theme" name="discountNominal[]" id="price-{{$barisTerakhir}}-discountNominal" value="{{number_format($data->DiscountNominal,'2','.',',')}}">
                                    </td>
                                    <td id="price-{{$barisTerakhir}}-qty-hitung" class="right subtotal">
                                        0.00
                                    </td>
                                    <td>
                                        @if($barisTerakhir == 0)
                                        -
                                        @else
                                        <button class="btn btn-pure-xs btn-xs btn-deleteRow" type="button" data="row{{$barisTerakhir}}" text="rowSpec{{$barisTerakhir}}" barang="inventory-{{$barisTerakhir}}"><span class="glyphicon glyphicon-trash"></span></button>
                                        @endif
                                    </td>
                                </tr>
                                <tr id='rowSpec{{$barisTerakhir}}'  style="display:none">
                                    <td colspan='9' class='rowSpec{{$barisTerakhir}}'>
                                        <textarea onkeyup="textAreaAdjust(this)" name="spesifikasi[]" style="overflow:hidden; width: 100%;" class="input-theme rowSpec{{$barisTerakhir}}">{{$data->Remark}}</textarea>
                                    </td>
                                </tr>
                                {{'';$barisTerakhir++;}}
                                @endforeach
                                @else
                                <tr id="row0" style="background-color: #e5e5e5 !important">
                                    <td class="chosen-uom" style="border-color: #d8d8d8 !important">
                                        <input class="input-theme margbot10" type="text" id="searchInventory" title="Type Inventory Name or ID then 'Enter'" placeholder="Type Inventory Name or ID then 'Enter'">
                                        <div id="selectInventory">

                                        </div>
                                    </td>
                                    <td style="border-color: #d8d8d8 !important">
                                        <select id="uom-0" class="input-theme uom">

                                        </select>
                                    </td>
                                    <td class="text-right" style="border-color: #d8d8d8 !important">
                                        <input type="text" class="maxWidth qty right numaja input-theme" maxlength="11" min="1" id="price-0-qty" value="1">
                                    </td>
                                    <td class="text-right" style="border-color: #d8d8d8 !important">
                                        <input type="text" class="maxWidth price right numajaDesimal input-theme" maxlength="" id="price-0" value="0">
                                    </td>
                                    <td class="text-right" style="border-color: #d8d8d8 !important">
                                        <input type="text" class="maxWidth discount right numajaDesimal input-theme" id="price-0-discount" value="0.00">
                                    </td>
                                    <td class="text-right" style="border-color: #d8d8d8 !important">
                                        <input type="text" class="maxWidth discount1 right numajaDesimal input-theme" id="price-0-discount1" value="0.00">
                                    </td>
                                    <td class="text-right" style="border-color: #d8d8d8 !important">
                                        <input type="text" class="maxWidth discountNominal right numajaDesimal input-theme" id="price-0-discountNominal" value="0.00">
                                    </td>
                                    <td id="price-0-qty-hitung" class="right" style="border-color: #d8d8d8 !important">
                                        0.00
                                    </td>
                                    <td style="border-color: #d8d8d8 !important">
                                        <button type="button" class="btn btn-green btn-sm" disabled="" id="btn-addRow"><span class="glyphicon glyphicon-plus"></span></button>
                                    </td>
                                </tr>
                                {{'';$barisTerakhir++;}}
                                @endif
                            </tbody>
                        </table>
                        <?php $barisTerakhir2 = 0; ?>
                        @if($header->Import == "aa" && $header->Import != 0)
                        <label>Cost Detail</label>
                        <table class="table master-data" id="table-cost" style="table-layout:fixed;">
                            <thead>
                                <tr>
                                    <th style="width: 50%;">Cost</th>
                                    <th style="width: 30%;">Total</th>
                                    <th style="width: 20%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="row0" style="background-color: #e5e5e5 !important">
                                    <td class="chosen-uom" style="border-color: #d8d8d8 !important">
                                        <select id="cost-0" class="chosen-select">
                                            @foreach(Cost::all() as $c)
                                            <option value="{{$c->InternalID}}">{{$c->CostName}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="text-right" style="border-color: #d8d8d8 !important">
                                        <input type="text" class="maxWidth price2 right numajaDesimal input-theme" maxlength="" id="totalcost-0" value="0">
                                    </td>
                                    <td style="border-color: #d8d8d8 !important">
                                        <button type="button" class="btn btn-green btn-sm" id="btn-addRow2"><span class="glyphicon glyphicon-plus"></span></button>
                                    </td>
                                </tr>
                                @if(count(PurchaseOrderCostDetail::where('PurchaseOrderInternalID',$header->InternalID)->get()) > 0)
                                {{'';$barisTerakhir2++;}}
                                @foreach(PurchaseOrderCostDetail::where('PurchaseOrderInternalID',$header->InternalID)->get() as $data)
                                <tr id="rowcost{{$barisTerakhir2}}">
                                    <td class="chosen-transaction">
                                        <?php $cost = Cost::find($data->CostInternalID); ?>
                                        <input type="hidden" class="cost" id="cost-{{$barisTerakhir2}}" style="" name="cost[]" value="{{$cost->InternalID}}">
                                        {{$cost->CostName}}
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth right price2 numajaDesimal input-theme" name="totalcost[]" id="totalcost-{{$barisTerakhir2}}" value="{{number_format($data->TotalCost,'2','.',',')}}">
                                    </td>
                                    <td>
                                        @if($barisTerakhir == 0)
                                        -
                                        @else
                                        <button class="btn btn-pure-xs btn-xs btn-deleteRow2" type="button" data="rowcost{{$barisTerakhir2}}"><span class="glyphicon glyphicon-trash"></span></button>
                                        @endif
                                    </td>
                                </tr>
                                {{'';$barisTerakhir2++;}}
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                        @endif
                        <input type="hidden" name="grandTotalValue" value="0" id="grandTotalValue">
                        <table class="pull-left"> 
                            <tr>
                                <td></td>
                            </tr>
                            <tr>
                                <td><br></td>
                            </tr>
                            <tr>
                                <td><p>Created by {{User::where('UserID', $header->UserRecord)->first()->UserName.' '.date('d-m-Y H:i:s', strtotime($header->dtRecord))}}</p></td>
                            </tr>
                            <tr>
                                @if($header->UserModified != '0')
                                <td><p>Modified by {{User::where('UserID', $header->UserModified)->first()->UserName.' '.date('d-m-Y H:i:s', strtotime($header->dtModified))}}</p></td>
                                @else
                                <td><p>Modified by -</p></td>
                                @endif
                            </tr>
                        </table>
                        <table class="pull-right"> 
                            <tr>
                                <td><h5 class="right margr10 h5total"><b>Total</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b id="total"></b></h5></td>
                            </tr>
                            <tr >
                                <td><h5 class="right margr10 h5total"><b>Discount</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                                <td><h5 class="right margr10 h5total new" style="width: 150px;"><input type="text" style="width: 120px;" class="maxWidth right numaja discountGlobal" name="DiscountGlobal" maxlength="" id="discountGlobal" value="{{number_format($header->DiscountGlobal, '2', '.', ',')}}"></h5></td>
                            </tr>
                            <tr>
                                <td><h5 class="right margr10 h5total"><b>Grand Total</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b id="grandTotal"></b></h5></td>
                            </tr>

                            <tr>
                                <td><h5 class="right margr10 h5total hidevat"><b>Tax</b></h5></td>
                                <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b id="tax"></b></h5></td>
                            </tr>
                            <tr>
                                <td><h5 class="right margr10 h5total hidevat"><b>Grand Total(tax)</b></h5></td>
                                <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b id="grandTotalAfterTax"></b></h5></td>
                            </tr>
                        </table>
                    </div><!---- end div tableadd---->      
                </div><!---- end div tableadd---->   
            </div><!---- end div tabwrap---->  
            <div class="btnnest pull-right">
                <button class="btn btn-green btn-sm btn-save" id="btn-save"> Save </button>
            </div>
        </form>
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop

@section('modal')
<div class="modal fade" id="r_summary" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Summary Purchase Order</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="" target="_blank">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='summaryPurchaseOrder'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">Supplier</label> *
                            </li>
                            <li>
                                <select class='chosen-select' name='supplier'>
                                    <option value='-1'>All</option>
                                    @foreach(Coa6::where('CompanyInternalID',Auth::user()->CompanyInternalID)->where('Type','s')->get() as $c)
                                    <option value='{{$c->InternalID}}'>{{$c->ACC6Name}}</option>
                                    @endforeach
                                </select>
                            </li>
                        </ul>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>      
        </div>
    </div>  
</div>
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNewJournal.jquery.js')}}" type="text/javascript"></script>
<script>
                                            var getUomThisInventory = "<?php echo Route("getUomThisInventory") ?>";
                                            var getPriceRangeThisInventoryPO = "<?php echo Route("getPriceRangeThisInventoryPO") ?>";
                                            var getSearchResultInventoryForPO = "<?php echo Route("getSearchResultInventoryForPO") ?>";
                                            var textSelect = '';
                                            var baris = '<?php echo $barisTerakhir; ?>';
                                            var baris2 = '<?php echo $barisTerakhir2; ?>';
                                            var counter = '<?php echo $barisTerakhir; ?>';
                                            var arrInven = '<?php echo json_encode($arrInv) ?>';
</script>
<script>
    var purchaseOrderDataBackup = '<?php echo Route('purchaseOrderDataBackup', Input::get('typePayment') . '---;---' . Input::get('typeTax') . '---;---' . Input::get('startDate') . '---;---' . Input::get('endDate')) ?>';
</script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-pembelian-add/purchaseOrder.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-pembelian-add/purchaseOrderUpdate.js')}}"></script>
@stop