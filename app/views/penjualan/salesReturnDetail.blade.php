@extends('template.header-footer')

@section('title')
Sales Return
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
@stop

@section('nav')

@stop

@section('content')
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showSalesReturn')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Sales Return</a>
                <a href="{{route('salesReturnDetail',$header->SalesReturnID)}}" type="button" class="btn btn-sm btn-pure">Detail {{$header->SalesReturnID}}</a>
            </div>
            <div class="btn-group margr5">
                <button type="button" class="btn btn-green btn-sm dropdown-toggle" data-target="#insertReturn" data-toggle="modal" role="dialog"
                        aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
            </div>
            <button id="search-button"  <?php if (myCheckIsEmpty('SalesReturn')) echo 'disabled'; ?>  class="btn btn-green btn-sm margr5"><span class="glyphicon glyphicon-search"></span> Search </button>
            @if(checkModul('O04'))
            <button type="button" class="btn btn-green margr5" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rSummary">
                <span class="glyphicon glyphicon-file"></span> Summary Report</button>
            <button type="button" class="btn btn-green margr5" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rDetail">
                <span class="glyphicon glyphicon-file"></span> Detail Report</button>
            @endif
            <a href="{{Route('salesReturnUpdate',$header->SalesReturnID)}}">
                <button id="btn-{{$header->SalesReturnID}}-update"
                        class="btn btn-green btn-sm margr5">
                    <span class="glyphicon glyphicon-edit"></span> Edit
                </button>
            </a>
            @if(checkModul('O04'))
            <div class="btn-group">
                                <a href="{{Route('salesReturnPrint',$header->SalesReturnID)}}" id="btn-{{$header->SalesReturnID}}-print" target='_blank' style="margin-right: 0px !important;">
                                    <button type="button" class="btn btn-green">
                                        <span class="glyphicon glyphicon-print"></span>
                                        Print
                                    </button>
                                </a>
<!--                <a href="#" data-target="#r_print" data-internal="{{$header->SalesReturnID }}"  data-toggle="modal" role="dialog"
                   onclick="printAttach(this)" data-id="{{$header->SalesReturnID}}" data-name="{{$header->SalesReturnID}}">
                    <button type="button" class="btn btn-green">
                        <span class="glyphicon glyphicon-print"></span> Print
                    </button>
                </a>-->
                <!--                <a href="{{Route('salesReturnInternalPrint',$header->SalesReturnID)}}" id="btn-{{$header->SalesReturnID}}-print" target='_blank' style="margin-right: 0px !important;">
                                    <button type="button" class="btn btn-green">
                                        <span class="glyphicon glyphicon-print"></span> Internal
                                    </button>
                                </a>-->
            </div>

            @endif
        </div>
        @include('template.searchComponentTransactionModule')

        <div class="tabwrap">
            <div class="tabhead">
                <h4 class="headtitle">{{'Sales Return '.$header->SalesReturnID}}</h4>
            </div>
            <div class="tableadd">
                <div class="headinv new">
                    <ul class="pull-left">
                        <li>
                            <label for="date">Date</label>
                            <span>{{date( "d-m-Y", strtotime($header->SalesReturnDate))}}</span>
                        </li>
                        <li>
                            <label for="customer">Customer</label>
                            <span><?php
                                $coa6 = SalesReturnHeader::find($header->InternalID)->coa6;
                                echo $coa6->ACC6ID . ' ' . $coa6->ACC6Name
                                ?></span>
                        </li>
                        <li>
                            <label for="longTerm">Payment</label>
                            @if($header->isCash == 0)
                            <span>{{'Cash'}}</span>
                            @elseif($header->isCash == 1)
                            <span>{{'Credit'}}</span>
                            @elseif($header->isCash == 2)
                            <span>{{'CBD'}}</span>
                            @elseif($header->isCash == 3)
                            <span>{{'Deposit'}}</span>
                            @elseif($header->isCash == 4)
                            <span>{{'Down Payment'}}</span>
                            @endif
                        </li>
                        <li>
                            <label for="warehouse">Ware house</label>
                            <span>{{$header->Warehouse->WarehouseName}}</span>
                        </li>
                    </ul>
                    <ul class="pull-right">
                        <li>
                            <label for="currency">Currency</label>
                            <span>{{$header->Currency->CurrencyName}}</span>
                        </li>
                        <li>
                            <label for="rate">Rate</label>
                            <span>{{number_format($header->CurrencyRate,'2','.',',')}}</span>
                        </li>
                        <!--                        <li>
                                                    <label for="VAT">VAT</label>
                                                    @if($header->VAT == 0)
                                                    <span>{{'Non Tax'}}</span>
                                                    @else
                                                    <span>{{'Tax'}}</span>
                                                    @endif
                                                </li>-->
                        <li>
                            <label for="">Remark</label>
                            <span>{{$header->Remark}}</span>
                        </li>
                    </ul>
                </div>
                <div class="padrl10">
                    <table class="table master-data" id="table-quotation-description" style="table-layout:fixed;">
                        <thead>
                            <tr>
                                <th style="width: 25%;">Inventory</th>
                                <th style="width: 10%;">Uom</th>
                                <th style="width: 10%;">Qty</th>
                                <th style="width: 15%;">Price</th>
                                <th style="width: 10%;">Disc (%)</th>
                                <th style="width: 10%;">Disc</th>
                                <th style="width: 15%;">Subtotal</th>
                                <th style="width: 14%;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $total = 0;
                            $totalVAT = 0;
                            $barisDesc = 1;
                            ?>
                            @foreach(SalesReturnDescription::where('SalesReturnInternalID',$header->InternalID)->get() as $description)
                            <tr style='background: #F1FFDB;' id='rowDescription{{$barisDesc}}'>
                                <td class='chosen-uom'>
                                    {{$description->InventoryText}}
                                </td>
                                <td>
                                    {{$description->UomText}}
                                </td>
                                <td class='text-right'>
                                    {{$description->Qty}}
                                </td>
                                <td class='text-right'>
                                    {{number_format($description->Price,'2','.',',')}}
                                </td>
                                <td class='text-right'>
                                    {{$description->Discount}}
                                </td>
                                <td class='text-right'>
                                    {{number_format($description->DiscountNominal,'2','.',',')}}
                                </td>
                                <td class="right subtotalDescription" id="priceDescription-{{$barisDesc}}-qty-hitung">{{number_format(ceil($description->SubTotal),'0','.',',')}}</td>
                                <td>
                                    <button class="btn btn-pure-xs btn-xs btn-toggle-detail margr5" type="button" data-toshow="show" data="rowDescription{{$barisDesc}}"><span class="glyphicon glyphicon-eye-open"></span></button>
                                    <!--<button class="btn btn-pure-xs btn-xs btn-toggle-spec" type="button" data-toshow="show" data="rowSpec{{$barisDesc}}"><span class="glyphicon glyphicon-comment"></span></button>-->
                                </td>
                            </tr>
<!--                            <tr id='rowSpec{{$barisDesc}}'>
                                <td colspan='8' class='rowSpec{{$barisDesc}}' style="text-align: left">
                                    {{nl2br($description->Spesifikasi)}}
                                </td>
                            </tr>-->
                            <?php
                            $barisDetail = 1;
                            ?>
                            @foreach(SalesReturnDetail::where('DescriptionInternalID',$description->InternalID)->get() as $detail)
                            <!--untuk non-parcel-->
                            @if($detail->SalesReturnParcelInternalID == 0)
                            <tr id='row{{$barisDetail}}' class='rowDescription{{$barisDesc}}'>
                                <td class='chosen-uom'>
                                    <input type="hidden" class="inventory" style="width: 100px" id="inventory-{{$barisDetail}}" name="inventory{{$barisDesc}}[]" value="{{$detail->InventoryInternalID}}---;---inventory">{{Inventory::find($detail->InventoryInternalID)->InventoryID.' '.Inventory::find($detail->InventoryInternalID)->InventoryName}}
                                </td>
                                <td>
                                    {{Uom::find($detail->UomInternalID)->UomID}}
                                </td>
                                <td class='text-right'>
                                    {{number_format($detail->Qty,'0','.',',')}}
                                </td>
                                <td class='text-right'>
                                    {{number_format($detail->Price,'2','.',',')}}
                                </td>
                                <td class='text-right'>
                                    {{$detail->Discount}}
                                </td>
                                <td class='text-right'>
                                    {{number_format($detail->DiscountNominal,'2','.',',')}}
                                </td>
                                <td class="right subtotal" id="price-{{$barisDetail}}-qty-hitung">{{number_format(ceil($detail->SubTotal),'0','.',',')}}</td>
                                <td>
                                    -
                                </td>
                                {{'';//$totalVAT += $detail->VAT}}
                            </tr>
                            <?php
                            $barisDetail++;
                            $total += $detail->SubTotal;
                            $totalVAT += ceil($detail->SubTotal) * 0.1;
                            ?>
                            @endif
                            @endforeach
                            <!--untuk parcel-->
                            @foreach(SalesReturnParcel::where('DescriptionInternalID',$description->InternalID)->get() as $parcel)
                            <tr id='row{{$barisDetail}}' class='rowDescription{{$barisDesc}}'>
                                <td class='chosen-uom'>
                                    {{Parcel::find($parcel->ParcelInternalID)->ParcelID.' '.Parcel::find($parcel->ParcelInternalID)->ParcelName}}
                                </td>
                                <td>
                                    -
                                </td>
                                <td class='text-right'>
                                    {{number_format($parcel->Qty,'0','.',',')}}
                                </td>
                                <td>
                                    {{number_format($parcel->Price,'2','.',',')}}
                                </td>
                                <td class='text-right'>
                                    {{$parcel->Discount}}
                                </td>
                                <td class='text-right'>
                                    {{number_format($parcel->DiscountNominal,'2','.',',')}}
                                </td>
                                <td class="right subtotal" id="price-{{$barisDetail}}-qty-hitung">{{number_format($parcel->SubTotal,'2','.',',')}}</td>
                                <td>
                                    -
                                </td>
                                {{'';//$totalVAT += $parcel->VAT}}
                            </tr>
                            <?php
                            $barisDetail++;
                            $total += $parcel->SubTotal;
                            ?>
                            @endforeach
                            <?php $barisDesc++; ?>
                            @endforeach
                            @if($totalVAT != 0)
                            {{'';$totalVAT = $totalVAT - $header->DiscountGlobal*0.1;}}
                            @endif
                        </tbody>
                    </table>
                    <table class="pull-left">
                        <tr>
                            <td></td>
                        </tr>
                        <tr>
                            <td><p>Created by {{User::where('UserID', $header->UserRecord)->first()->UserName.' '.date('d-m-Y H:i:s', strtotime($header->dtRecord))}}</p></td>
                        </tr>
                        <tr>
                            @if($header->UserModified != '0')
                            <td><p>Modified by {{User::where('UserID', $header->UserModified)->first()->UserName.' '.date('d-m-Y H:i:s', strtotime($header->dtModified))}}</p></td>
                            @else
                            <td><p>Modified by -</p></td>
                            @endif
                        </tr>
                    </table>
                    @if(count($detail) > 0)

                    <table class="pull-right">
                        <tr>
                            <td><h5 class="right margr10 h5total"><b>Total</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="total">{{number_format(ceil($total),'0','.',',')}}</b></h5></td>
                        </tr>
                        <tr >
                            <td><h5 class="right margr10 h5total"><b>Discount</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total new"><b>{{number_format($header->DiscountGlobal, '0', '.',',')}}</b></h5></td>
                        </tr>
                        <tr>
                            <td><h5 class="right margr10 h5total"><b>Grand Total</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="grandTotal">{{number_format(ceil($total-$header->DiscountGlobal),'0','.',',')}}</b></h5></td>
                        </tr>

                        <tr>
                            <td><h5 class="right margr10 h5total hidevat"><b>Tax</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="tax">{{number_format(floor($totalVAT),'0','.',',')}}</b></h5></td>
                        </tr>
                        <tr>
                            <td><h5 class="right margr10 h5total hidevat"><b>Grand Total(tax)</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="grandTotalAfterTax">{{number_format(ceil($header->GrandTotal),'0','.',',')}}</b></h5></td>
                        </tr>
                    </table>

                    @endif
                </div><!---- end div padrl10---->
            </div><!---- end div tableadd---->
        </div><!---- end div tabwrap---->
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->

@stop


@section('modal')
<div class="modal fade" id="insertReturn" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insert Sales Return</h4>
            </div>
            <form action="" method="post" class="action" id="form-sr">
                {{'';$hitung = 0;}}
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="insertSalesReturn" id="jenisReturn" name="jenis">
                            <li>
                                <label for="sales">Sales ID</label> *
                            </li>
                            <input class="input-theme margbot10" type="text" id="searchSalesReturn" title="Type Sales Name or ID then 'Enter'" placeholder="Type Sales Name or ID then 'Enter'">
                            <li id="selectSalesReturn">

                            </li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btn-add-sr" @if($hitung==0){{'disabled'}}@endif class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="r_summary" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Summary Report</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="" target="_blank">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='summarySales'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateReport" data-validation="required">
                            </li>
                        </ul>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="r_print" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Tax</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="" target="_blank">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='printSalesReturn'>
                            <input type="hidden" name="internalID" id="internalID" />
                            <li>
                                <label for="type">Type Tax</label> *
                            </li>
                            <li>
                                <select class="form-control" name="type">
                                    <option value="include">Include</option>
                                    <option value="exclude">Exclude</option>
                                </select>
                            </li>
                        </ul>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script><script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script>
                       var getResultSearchSR = "<?php echo Route("getResultSearchSR") ?>";
</script><script>
    var salesReturnDataBackup = '<?php echo Route('salesReturnDataBackup', Input::get('coa6') . '---;---' . Input::get('typePayment') . '---;---' . Input::get('typeTax') . '---;---' . Input::get('startDate') . '---;---' . Input::get('endDate')) ?>';
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-penjualan/salesReturn.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
@stop
