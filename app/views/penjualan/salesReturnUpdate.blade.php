@extends('template.header-footer')

@section('title')
Sales Return
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<style>
    .help-block {
        margin-left: 30% !important;
    }
</style>
@stop

@section('nav')

@stop

@section('content')
@if(isset($messages))
@if($messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Sales return has been updated.
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showSalesReturn')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Sales Return</a>
                <a href="{{route('salesReturnUpdate',$header->SalesReturnID)}}" type="button" class="btn btn-sm btn-pure">Update {{$header->SalesReturnID}}</a>
            </div>
            <div class="btn-group margr5">
                <button type="button" class="btn btn-green btn-sm dropdown-toggle" data-target="#insertReturn" data-toggle="modal" role="dialog"
                        aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
            </div>
            <button id="search-button"  <?php if (myCheckIsEmpty('SalesReturn')) echo 'disabled'; ?> class="btn btn-green btn-sm"><span class="glyphicon glyphicon-search margr5"></span> Search </button>
            @if(checkModul('O04'))
            <button type="button" class="btn btn-green margr5" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rSummary">
                <span class="glyphicon glyphicon-file"></span> Summary Report</button>
            <button type="button" class="btn btn-green" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rDetail">
                <span class="glyphicon glyphicon-file"></span> Detail Report</button>
            @endif
            <!--            <a href="#" data-target="#r_print" data-internal="{{$header->SalesReturnID }}"  data-toggle="modal" role="dialog"
                           onclick="printAttach(this)" data-id="{{$header->SalesReturnID}}" data-name="{{$header->SalesReturnID}}">
                            <button type="button" class="btn btn-green">
                                <span class="glyphicon glyphicon-print"></span> Print
                            </button>
                        </a>-->
            <a href="{{Route('salesReturnPrint',$header->SalesReturnID)}}" id="btn-{{$header->SalesReturnID}}-print" target='_blank' style="margin-right: 0px !important;">
                <button type="button" class="btn btn-green">
                    <span class="glyphicon glyphicon-print"></span>
                    Print
                </button>
            </a>
        </div>
        @include('template.searchComponentTransactionModule')
        <form method="POST" action="" id='form-updateSales'>
            <input type='hidden' name='SalesReturnInternalID' value='{{$header->InternalID}}'>
            <div class="tabwrap">
                <div class="tabhead">
                    @if($header->isCash == 0)
                    <h4 class="headtitle">Sales Return <span id="salesID">{{$header->SalesReturnID}}</span></h4>
                    @else
                    <h4 class="headtitle">Sales Return <span id="salesID">{{$header->SalesReturnID}}</span></h4>
                    @endif
                </div>
                <div class="tableadd">
                    <div class="headinv new">
                        <ul class="pull-left">
                            <li>
                                <label for="date">Date</label>
                                <span>{{date( "d-m-Y", strtotime($header->SalesReturnDate))}}</span>
                            </li>
                            <li>
                                <label for="customer">Customer</label>
                                <span><?php
                                    $coa6 = SalesReturnHeader::find($header->InternalID)->coa6;
                                    echo $coa6->ACC6ID . ' ' . $coa6->ACC6Name
                                    ?></span>
                            </li>
                            <li>
                                <label for="longTerm">Payment</label>
                                @if($header->isCash == 0)
                                <span>{{'Cash'}}</span>
                                @elseif($header->isCash == 1)
                                <span>{{'Credit'}}</span>
                                @elseif($header->isCash == 2)
                                <span>{{'CBD'}}</span>
                                @elseif($header->isCash == 3)
                                <span>{{'Deposit'}}</span>
                                @elseif($header->isCash == 4)
                                <span>{{'Down Payment'}}</span>
                                @endif
                            </li>
                            <li>
                                <label for="warehouse">Warehouse *</label>
                                <select class="chosen-select choosen-modal warehouse" id="warehouseHeader" name="warehouse">
                                    @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->where("Type",Auth::user()->WarehouseCheck)->get() as $war)
                                    @if($war->InternalID == $header->WarehouseInternalID)
                                    <option selected="selected" id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                        {{$war->WarehouseName;}}
                                    </option>
                                    @else
                                    <option id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                        {{$war->WarehouseName;}}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>
                            </li>
                        </ul>
                        <ul class="pull-right">
                            <li>
                                <label for="currency">Currency</label>
                                <span>{{'';$currency = Currency::find($header->CurrencyInternalID); $currencyName = $currency->CurrencyName; echo $currencyName}}</span>
                            </li>
                            <li>
                                <label for="rate">Rate</label>
                                <span>{{number_format($header->CurrencyRate,'2','.',',')}}</span>
                            </li>
                            <!--                            <li>
                                                            <label for="VAT">VAT</label>
                                                            @if($header->VAT == 0)
                                                            <span>{{'Non Tax'}}</span>
                                                            <input type="hidden" id="taxSales" value="0">
                                                            @else
                                                            <span>{{'Tax'}}</span>
                                                            <input type="hidden" id="taxSales" value="1">
                                                            @endif
                                                        </li>-->
                            <li>
                                <label for="remark">Remark *</label>
                                <textarea name="remark" id="remark" data-validation="required">{{$header->Remark}}</textarea>
                            </li>
                        </ul>
                    </div>

                    <table class="table master-data" id="table-salesorder-description" style="table-layout:fixed;">
                        <thead>
                            <tr>
                                <th style="width: 15%;">Inventory</th>
                                <th style="width: 5%;">Uom</th>
                                <th style="width: 5%;">Qty</th>
                                <th style="width: 10%;">Price</th>
                                <th style="width: 5%;">Disc (%)</th>
                                <th style="width: 10%;">Disc</th>
                                <th style="width: 10%;">Subtotal</th>
                                <th style="width: 5%;">Max Qty Return</th>
                                <th style="width: 8%;">Return</th>
                                <th style="width: 10%;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 0;
                            $barisDesc = 1;
                            $barisDetail = 1;
                            ?>
                            @foreach(SalesAddDescription::where('SalesInternalID',$headersales->InternalID)->get() as $description)
                            <?php
                            $sumDescriptionReturn = SalesReturnHeader::getSumReturnDescriptionExcept($description->InternalID, $header->InternalID);
                            if ($sumDescriptionReturn == '') {
                                $sumDescriptionReturn = '0';
                            }
                            $qtyDescription = SalesReturnDescription::where('SalesDescriptionInternalID', $description->InternalID)
                                            ->where('SalesReturnInternalID', $header->InternalID)->pluck('Qty');
                            ?>
                            <tr style='background: #F1FFDB;' id='rowDescription{{$barisDesc}}'>
                                <td class='chosen-uom'>
                                    {{$description->InventoryText}}
                                </td>
                                <td>
                                    {{$description->UomText}}
                                </td>
                                <td class='text-right'>
                                    {{number_format($description->Qty,'0','.',',')}}
                                </td>
                                <td class='text-right'>
                                    {{number_format($description->Price,'2','.',',')}}
                                </td>
                                <td class='text-right'>
                                    {{number_format($description->Discount,'0','.',',')}}
                                </td>
                                <td class='text-right'>
                                    {{number_format($description->DiscountNominal,'2','.',',')}}
                                </td>
                                <td class='text-right'>
                                    {{number_format(ceil($description->SubTotal),'2','.',',')}}
                                </td>
                                <td class='text-right'>
                                    {{number_format($description->Qty - $sumDescriptionReturn,'0','.',',')}}
                                </td>
                                <td class='text-right'>
                                    <input type="text" min="0" max="{{$description->Qty - $sumDescriptionReturn}}" class="maxWidth addSales qtyDescription right input-theme" name="qtyDescription[]" id="priceDescription-{{$barisDesc}}-qty" value='{{number_format($qtyDescription,'0','.',',')}}'>
                                </td>
                                <td>
                                    <input type="hidden" name="InternalDescription[]" value="{{$description->InternalID}}">
                                    <input type="hidden" name="max[]" value="{{$description->Qty - $sumDescriptionReturn}}">
                                    <button class="btn btn-pure-xs btn-xs btn-toggle-detail margr5" type="button" data-toshow="show" data="rowDescription{{$barisDesc}}"><span class="glyphicon glyphicon-eye-open"></span></button>
                                    <!--<button class="btn btn-pure-xs btn-xs btn-toggle-spec" type="button" data-toshow="show" data="rowSpec{{$barisDesc}}"><span class="glyphicon glyphicon-comment"></span></button>-->
                                </td>
                            </tr>
                            <tr id='rowSpec{{$barisDesc}}' style="display: none">
                                <td colspan='10' class='rowSpec{{$barisDesc}}' style="text-align: left">
                                    {{nl2br($description->Spesifikasi)}}
                                </td>
                            </tr>
                            @foreach(SalesAddDetail::where('DescriptionInternalID',$description->InternalID)->get() as $detail)
                            <!--untuk non-parcel-->
                            @if($detail->SalesParcelInternalID == 0)
                            <?php
                            $sumSalesReturn = SalesReturnHeader::getSumReturnExcept($detail->InventoryInternalID, $header->SalesReturnID, $detail->InternalID);
                            if ($sumSalesReturn == '') {
                                $sumSalesReturn = '0';
                            }
                            $qtyDetail = SalesReturnDetail::where('SalesDetailInternalID', $detail->InternalID)
                                            ->where('SalesReturnInternalID', $header->InternalID)->pluck('Qty');
                            ?>
                            <tr id='row{{$barisDetail}}' class='rowDescription{{$barisDesc}}'>
                                <td class='chosen-uom'>
                                    {{Inventory::find($detail->InventoryInternalID)->InventoryID.' '.Inventory::find($detail->InventoryInternalID)->InventoryName}}
                                </td>
                                <td>
                                    {{Uom::find($detail->UomInternalID)->UomID.' '.Uom::find($detail->UomInternalID)->UomName}}
                                </td>
                                <td id="qtySales-{{$barisDetail}}" class='text-right'>
                                    {{$detail->Qty}}
                                </td>
                                <td class='text-right'>
                                    {{number_format($detail->Price,'2','.',',')}}
                                </td>
                                <td class='text-right'>
                                    {{$detail->Discount}}
                                </td>
                                <td class='text-right'>
                                    {{number_format($detail->DiscountNominal,'2','.',',')}}
                                </td>
                                <td id="subtotalSales-{{$barisDetail}}" class='text-right'>
                                    {{number_format(ceil($detail->SubTotal),'2','.',',')}}
                                </td>
                                <td class='text-right'>
                                    {{number_format($detail->Qty -$sumSalesReturn,'2','.',',')}}
                                </td>
                                <td class='text-right'>
                                    <input type="hidden" name="InternalDetail{{$barisDesc}}[]" value="{{$detail->InternalID}}">
                                    <input type="hidden" name="tipe{{$barisDesc}}[]" value="inventory">
                                    <input type="text" class="maxWidth addSales quantitySales right input-theme" name="qty{{$barisDesc}}[]" min="0" max="{{$detail->Qty-$sumSalesReturn}}" value="{{number_format($qtyDetail,'0','.',',')}}" id="price-{{$barisDetail}}-qty">
                                </td>
                                <td>
                                    -
                                </td>
                            </tr>
                            <?php $barisDetail++; ?>
                            @endif <!--tutup if non parcel -->
                            @endforeach<!--tutup foreach quotation detail-->
                            <!--untuk parcel-->
                            @foreach(SalesAddParcel::where('DescriptionInternalID',$description->InternalID)->get() as $parcel)
                            <?php
                            $sumSalesReturnParcel = SalesReturnHeader::getSumReturnExceptParcel($parcel->ParcelInternalID, $header->SalesReturnID, $parcel->InternalID);
                            if ($sumSalesReturnParcel == '') {
                                $sumSalesReturnParcel = '0';
                            }
                            $qtyDetail = SalesReturnParcel::where('SalesParcelDetailInternalID', $parcel->InternalID)
                                            ->where('SalesReturnInternalID', $header->InternalID)->pluck('Qty');
                            ?>
                            <tr id='row{{$barisDetail}}' class='rowDescription{{$barisDesc}}'>
                                <td class='chosen-uom'>
                                    {{Parcel::find($parcel->ParcelInternalID)->ParcelID.' '.Parcel::find($parcel->ParcelInternalID)->ParcelName}}
                                </td>
                                <td>
                                    -
                                </td>
                                <td id="qtySales-{{$barisDetail}}">
                                    {{$parcel->Qty}}
                                </td>
                                <td>
                                    {{$parcel->Price}}
                                </td>
                                <td>
                                    {{$parcel->Discount}}
                                </td>
                                <td>
                                    {{$parcel->DiscountNominal}}
                                </td>
                                <td id="subtotalSales-{{$barisDetail}}">
                                    {{$parcel->SubTotal}}
                                </td>
                                <td>
                                    {{$parcel->Qty -$sumSalesReturnParcel}}
                                </td>
                                <td class='text-right'>
                                    <input type="hidden" name="InternalDetail{{$barisDesc}}[]" value="{{$parcel->InternalID}}">
                                    <input type="hidden" name="tipe{{$barisDesc}}[]" value="parcel">
                                    <input type="text" class="maxWidth quantitySales addSales right input-theme" name="qty{{$barisDesc}}[]" maxlength="{{$parcel->Qty -$sumSalesReturnParcel}}" min="0" value="{{number_format($qtyDetail,'0','.',',')}}" id="price-{{$barisDetail}}-qty"></td>
                                <td>
                                    -
                                </td>
                            </tr>
                            <?php $barisDetail++; ?>
                            @endforeach<!--tutup foreach quotation parcel-->
                            <?php $barisDesc++; ?>
                            @endforeach <!--tutup foreach quotation description-->
                            <?php $i++; ?>
                        </tbody>
                    </table>
                    <input type="hidden" name="grandTotalValue" value="0" id="grandTotalValue">

                    <table class="pull-left">
                        <tr>
                            <td></td>
                        </tr>
                        <tr>
                            <td><p>Created by {{User::where('UserID', $header->UserRecord)->first()->UserName.' '.date('d-m-Y H:i:s', strtotime($header->dtRecord))}}</p></td>
                        </tr>
                        <tr>
                            @if($header->UserModified != '0')
                            <td><p>Modified by {{User::where('UserID', $header->UserModified)->first()->UserName.' '.date('d-m-Y H:i:s', strtotime($header->dtModified))}}</p></td>
                            @else
                            <td><p>Modified by -</p></td>
                            @endif
                        </tr>
                    </table>
                    <table class="pull-right">
                        <tr>
                            <td><h5 class="right margr10 h5total"><b>Total</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="total"></b></h5></td>
                        </tr>
                        <tr >
                        <input type="hidden" id="discountGlobalMax" value="{{($headersales->DiscountGlobal - SalesReturnHeader::getSumDiscountGlobalReturnExcept($headersales->SalesID, $header->SalesReturnID))}}">
                        <td><h5 class="right margr10 h5total"><b>Discount (Max {{number_format(($headersales->DiscountGlobal - SalesReturnHeader::getSumDiscountGlobalReturnExcept($headersales->SalesID, $header->SalesReturnID)),'0','.',',')}})</b></h5></td>
                        <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                        <td><h5 class="right margr10 h5total new" style="width: 150px;"><input type="text" style="width: 120px;" class="maxWidth right numaja discountGlobal" name="DiscountGlobal" maxlength="" id="discountGlobal" value="{{number_format($header->DiscountGlobal, 2,'.',',')}}"></h5></td>
                        </tr>
                        <tr>
                            <td><h5 class="right margr10 h5total"><b>Grand Total</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="grandTotal"></b></h5></td>
                        </tr>

                        <tr>
                            <td><h5 class="right margr10 h5total hidevat"><b>Tax</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="tax"></b></h5></td>
                        </tr>
                        <tr>
                            <td><h5 class="right margr10 h5total hidevat"><b>Grand Total(tax)</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="grandTotalAfterTax"></b></h5></td>
                        </tr>
                    </table>
                </div><!---- end div tableadd---->
            </div><!---- end div tabwrap---->
            <div class="btnnest pull-right">
                <button class="btn btn-green btn-sm btn-save" id="btn-save"> Save </button>
            </div>
        </form>
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop

@section('modal')
<div class="modal fade" id="insertReturn" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insert Sales Return</h4>
            </div>
            <form action="" method="post" class="action" id="form-sr">
                {{'';$hitung = 0;}}
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="insertSalesReturn" id="jenisReturn" name="jenis">
                            <li>
                                <label for="sales">Sales ID</label> *
                            </li>
                            <input class="input-theme margbot10" type="text" id="searchSalesReturn" title="Type Sales Name or ID then 'Enter'" placeholder="Type Sales Name or ID then 'Enter'">
                            <li id="selectSalesReturn">

                            </li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btn-add-sr" @if($hitung==0){{'disabled'}}@endif class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="r_summary" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Summary Report</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="" target="_blank">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='summarySales'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateReport" data-validation="required">
                            </li>
                        </ul>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="r_print" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Tax</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="" target="_blank">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='printSalesReturn'>
                            <input type="hidden" name="internalID" id="internalID" />
                            <li>
                                <label for="type">Type Tax</label> *
                            </li>
                            <li>
                                <select class="form-control" name="type">
                                    <option value="include">Include</option>
                                    <option value="exclude">Exclude</option>
                                </select>
                            </li>
                        </ul>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNewJournal.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script>
var getResultSearchSR = "<?php echo Route("getResultSearchSR") ?>";
</script><script>
    var salesReturnDataBackup = '<?php echo Route('salesReturnDataBackup', Input::get('coa6') . '---;---' . Input::get('typePayment') . '---;---' . Input::get('typeTax') . '---;---' . Input::get('startDate') . '---;---' . Input::get('endDate')) ?>';
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-penjualan/salesReturn.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-penjualan/salesReturnUpdate.js')}}"></script>
<script>
</script>
@stop
