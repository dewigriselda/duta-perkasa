@extends('template.header-footer')

@section('title')
Packing
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<style>
    .help-block {
        margin-left: 30% !important;
    }
    .headinv .chosen-single{
        width: 200px !important;
    }
    .headinv .chosen-drop{
        width: 200px !important;
    }
</style>
@stop

@section('nav')

@stop

@section('content')
@if(myCheckIsEmpty('SalesOrder;Default;Slip;DepartmentDefault'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one SalesOrder, Default COA, and Slip to insert Packing.
</div>
@endif
@if(isset($messages))
@if($messages == 'gagalInsert')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New shipping has been inserted.
</div>
<input type="hidden" name="print" id="print" value="{{$print}}">
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <!--<a href="{{route('showPackingList')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Packing List</a>-->
                <a href="{{route('showShipping')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Shipping</a>
                <a data-target="#insertPacking" data-toggle="modal" type="button" class="btn btn-sm btn-pure">Packing List New</a>
            </div>
            <div class="btn-group margr5">
<!--                <button type="button" <?php if (myCheckIsEmpty('SalesOrder;Default;Slip;DepartmentDefault')) echo 'disabled'; ?> class="btn btn-green btn-sm dropdown-toggle" data-target="#insertPacking" data-toggle="modal" role="dialog"
                        aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>-->
                <button type="button" <?php if (myCheckIsEmpty('SalesOrder;Default;Slip;DepartmentDefault')) echo 'disabled'; ?> class="btn btn-green btn-sm dropdown-toggle" data-target="#insertPacking" data-toggle="modal" role="dialog"
                        aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
            </div>
            <!--<button id="search-button" <?php if (myCheckIsEmpty('Packing')) echo 'disabled'; ?>  class="btn btn-green btn-sm margr5"><span class="glyphicon glyphicon-search"></span> Search </button>-->
            @if(checkModul('O04'))
            <!--            <button type="button" class="btn btn-green margr5" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rSummary">
                            <span class="glyphicon glyphicon-file"></span> Summary Report</button>
                        <button type="button" class="btn btn-green" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rDetail">
                            <span class="glyphicon glyphicon-file"></span> Detail Report</button>-->
            @endif
        </div>
        <!-- <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Search</h4>
            </div>
            <div class="tableadd">
                <ul class="searchmenu">
                    <form method="GET" action="">
                        <li>
                            <label for="coa6">Customer</label>
                            <br>
                            <select class="chosen-select" id="coa6" style="" name="coa6">
                                <option value="-1">All Customer</option>
                                @foreach(Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $coa6)
                                <option value="{{$coa6->InternalID}}">
                                    {{$coa6->ACC6ID.' '.$coa6->ACC6Name}}
                                </option>
                                @endforeach
                            </select>
                        </li>
                        <li><label for="typePayment">Payment Type</label>
                            <br>
                            <select name="typePayment" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Cash</option>
                                <option value="1">Credit</option>
                            </select>
                        </li>
                        <li><label for="typeTax">Tax Type</label>
                            <br>
                            <select name="typeTax" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Non Tax</option>
                                <option value="1">Tax</option>
                            </select>
                        </li>
                        <li><label for="date">Start Date</label>
                            <br><input id="startDate" name="startDate" type="text" autocomplete="off">
                        </li>
                        <li><label for="date">End Date</label>
                            <br><input id="endDate" name="endDate" type="text" autocomplete="off">
                        </li>
                        <li><br>
                            <button type="submit" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-search"></span> Search </button>
                        </li>
                        <li><br>
                            <button id="cancel" type="reset"  class="btn btn-pure btn-sm"><span class="glyphicon glyphicon-remove"></span> Cancel </button>
                        </li>
                    </form>
                </ul>
            </div><!---- end div tableadd---->
        <!--</div><!---- end div tabwrap---->
        @include('template.searchComponentTransactionModule')

        <form method="POST" action="" id='form-insertPacking'>
            <input type="hidden" id="jumlahbarisdetail" name="jumlahbarisdetail" value=0>
            <div class="tabwrap">
                <div class="tabhead">
                    <h4 class="headtitle">Packing List <span id="packingID">{{Packing::getNextIDPacking($packing)}}</span></h4>
                </div>
                <div class="tableadd">
                    <div class="headinv new">
                        <ul @if(checkModul('O05')) class="pull-left" style="width: 360px;" @else class="pull-left"  @endif>
                             <li>
                                <label for="shipping" class="margr10" style="float: left;">Sales Order ID</label>
                                <span>{{$header->SalesOrderID}}</span>
                                <input type="hidden" value="{{$header->InternalID}}" name="SalesOrderInternalID">
                            </li>
                            <li>
                                <label for="date">Date *</label>
                                <input id="date" name="date" type="text" style="width: 200px;" autocomplete="off" data-validation="required">
                            </li>
                            <li>
                                <label for="customer">Customer</label>
                                <span><?php
                                    $coa6 = SalesOrderHeader::find($header->InternalID)->coa6;
                                    echo $coa6->ACC6ID . ' ' . $coa6->ACC6Name
                                    ?></span>
                            </li>
                        </ul>
                        <ul class="pull-left" style="width: 360px;">
                            <li>
                                <label for="vehicle">Number of Vehicle</label>
                                <input id="vehicle" name="vehicle" type="text" style="width: 200px;">
                            </li>
                            <li>
                                <label for="driver">Driver Name</label>
                                <input id="driver" name="driver" type="text" style="width: 200px;">
                            </li>
                        </ul>
                        <ul class="pull-right">
                            <li>
                                <label for="vehicle">PO Customer</label>
                                <span>{{SalesOrderHeader::find($header->InternalID)->POCustomer}}</span>
                            </li>
                            <li>
                                <label for="remark">Remark</label>
                                <textarea name="remark" id="remark" style="width: 200px;" >{{$header->Remark}}</textarea>
                            </li>
                            <li>
                                <div class="required">
                                    * Required
                                </div>
                            </li>
                        </ul>
                    </div>
                    <label>Sales Order Detail</label>
                    <table class="table master-data" id="table--description" style="table-layout:fixed;">
                        <thead>
                            <tr>
                                <th style="width: 40%;">Description</th>
                                <th style="width: 40%;">Inventory</th>
                                <!--<th style="width: 30%;">Uom</th>-->
                                <th style="width: 20%;">Sales Order Qty</th>
                                <th style="width: 20%;">Max Qty</th>
                                <th style="width: 20%;">Sisa</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 0;
                            $barisDesc = 1;
                            $barisDetail = 1;
                            ?>
                            @foreach($descriptionn as $description)
                            @foreach(SalesOrderDetail::where('DescriptionInternalID',$description->InternalID)->get() as $detail)
                            <!--untuk non-parcel-->
                            @if($detail->SalesOrderParcelInternalID == 0)
                            <tr>
                                <td>
                                    {{$description->InventoryText}}
                                </td>
                                <td>
                                    {{Inventory::find($detail->InventoryInternalID)->InventoryID.' '.Inventory::find($detail->InventoryInternalID)->InventoryName}}
                                </td>
<!--                                <td>
                                    {{Uom::find($detail->UomInternalID)->UomID.' '.Uom::find($detail->UomInternalID)->UomName}}
                                </td>-->
                                <td>
                                    {{$detail->Qty}}
                                </td>
                                <td>
                                    {{$detail->Qty - ShippingAddDetail::where('SalesOrderDetailInternalID',$detail->InternalID)->sum('Qty')}}
                                </td>
                                <td id='sisa{{$description->InternalID}}_{{$detail->InventoryInternalID}}'>
                                    {{$detail->Qty - ShippingAddDetail::where('SalesOrderDetailInternalID',$detail->InternalID)->sum('Qty')}}
                                </td>
                            </tr>
                            <?php $barisDetail++; ?>
                            @endif <!--tutup if non parcel -->
                            @endforeach<!--tutup foreach quotation detail-->
                            <!--untuk parcel-->
                            @foreach(SalesOrderParcel::where('DescriptionInternalID',$description->InternalID)->get() as $parcel)
                            <tr>
                                <td>
                                    {{$description->InventoryText}}
                                </td>
                                <td class='chosen-uom'>
                                    {{Parcel::find($parcel->ParcelInternalID)->ParcelID.' '.Parcel::find($parcel->ParcelInternalID)->ParcelName}}
                                </td>
<!--                                <td>
                                    -
                                </td>-->
                                <td>
                                    {{$parcel->Qty}}
                                </td>
                                <td>
                                    {{$parcel->Qty - ShippingAddParcel::where('SalesOrderParcelDetailInternalID',$parcel->InternalID)->sum('Qty')}}
                                </td>
                                <td id='sisaparcel{{$detail->InventoryInternalID}}'>
                                    {{$parcel->Qty- ShippingAddParcel::where('SalesOrderParcelDetailInternalID',$parcel->InternalID)->sum('Qty')}}
                                </td>
                            </tr>
                            <?php $barisDetail++; ?>
                            @endforeach<!--tutup foreach quotation parcel-->
                            <?php $barisDesc++; ?>
                            @endforeach <!--tutup foreach quotation description-->
                            <?php $i++; ?>
                        </tbody>
                    </table>
                    <label>Detail</label>
                    <table class="table master-data  table-striped" id="table-packing-header" style="table-layout:fixed;">
                        <thead  class="default-thead">
                            <tr>
                                <th style="width: 25%;">Warehouse</th>
                                <th style="width: 25%;">Inventory</th>
                                        <!--<th style="width: 8%;">Uom</th>-->
                                <th style="width: 7%;">Stock</th>
                                <th style="width: 8%;">Qty</th>
                                <th class="text-center" style="width: 5%;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr id="row0">
                                <td class="chosen-transaction">
                                    <select id='warehouse-0' class="chosen-select warehouse">
                                        @foreach(Warehouse::all() as $w)
                                        <option value='{{$w->InternalID}}'>{{$w->WarehouseName}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td class="chosen-transaction">
<!--                                    <input class="input-theme margbot10" type="text" id="searchInventory" tabindex="-1" title="Type Inventory Name or ID then 'Enter'" placeholder="Type Inventory Name or ID then 'Enter'">
                                    <div id="selectInventory">

                                    </div>-->
                                    <select id='inventory-0' class="chosen-select inventory" data-toggle="popover" data-placement="bottom" data-html="true">
                                        @foreach(SalesOrderDetail::where('SalesOrderInternalID',$header->InternalID)->get() as $w)
                                        @if($w->SalesOrderParcelInternalID == 0)
                                        @if($detail->Qty - ShippingAddDetail::where('SalesOrderDetailInternalID',$w->InternalID)->sum('Qty') > 0)
                                        <option id="inventory{{$w->DescriptionInternalID}}_{{$w->InventoryInternalID}}" value='{{$w->InventoryInternalID}}---;---inventory---;---{{$w->InternalID}}---;---{{$w->DescriptionInternalID}}'>{{SalesOrderDescription::find($w->DescriptionInternalID)->InventoryText.' - '.$w->inventory->InventoryName}}</option>
                                        @endif
                                        @endif
                                        @endforeach
                                        @foreach(SalesOrderParcel::where('SalesOrderInternalID',$header->InternalID)->get() as $w)
                                        @if($parcel->Qty - ShippingAddParcel::where('SalesOrderParcelDetailInternalID',$w->InternalID)->sum('Qty') > 0)
                                        <option id="parcel{{$w->ParcelInternalID}}" value='{{$w->ParcelInternalID}}---;---parcel---;---{{$w->InternalID}}---;---{{$w->DescriptionInternalID}}'>{{SalesOrderDescription::find($w->DescriptionInternalID)->InventoryText.' - '.$w->parcel->ParcelName}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </td>
<!--                                <td>
                                    <select id="uom-0" class="input-theme uom">

                                    </select>
                                </td>-->
                                <td class="text-right">
                                    <span class="maxWidth stock" id="price-0-stock" data-toggle="popover" data-placement="bottom" data-html="true">-</span>
                                </td>
                                <td class="text-right">
                                    <input type="text" class="maxWidth qty right numaja input-theme" maxlength="11" min="1" id="price-0-qty" value="1">
                                </td>
                                <td class="text-center">
                                    <button type="button" class="btn btn-green btn-sm" id="btn-addRow"><span class="glyphicon glyphicon-plus"></span></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div><!---- end div tabwrap---->
                <div class="btnnest pull-right">
                    <button class="btn btn-green btn-sm btn-save" id="btn-save"> Save & Print </button>
                </div>
            </div><!---- end div primcontent--->
        </form>
    </div><!---- end div wrapjour---->
    @stop

    @section('modal')
    <div class="modal fade" id="insertPacking" role="dialog">
        <div class="modal-dialog modal-mid">
            <div class="modal-content modal-mid">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Insert Packing List</h4>
                </div>
                <form action="" method="post" class="action" id="form-so">
                    {{'';$hitung = 0;}}
                    <div class="modal-body">
                        <div class="coa-form">
                            <ul>
                                <input type="hidden" value="insertPacking" id="jenisShipping" name="jenis">
                                <li>
                                    <label for="sales">Sales Order ID</label> *
                                </li>
                                <input class="input-theme margbot10" type="text" id="searchSalesOrder2" title="Type Sales Order Name or ID then 'Enter'" placeholder="Type Sales Order Name or ID then 'Enter'">
                                <li id="selectSalesOrder2">

                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="btn-add-so2" @if($hitung==0){{'disabled'}}@endif class="btn btn-green">Submit</button>
                        <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="r_summary" role="dialog">
        <div class="modal-dialog modal-mid">
            <div class="modal-content modal-mid">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="titleReport">Summary Report</h4>
                </div>
                <form action="" method="post" class="action" id="" target="_blank">
                    <div class="modal-body">
                        <div class="coa-form">
                            <ul>
                                <input type='hidden' name='jenis' id="jenisReport" value='summarySalesOrder'>
                                <li>
                                    <label for="sDate">Start Date</label> *
                                </li>
                                <li>
                                    <input type="text" name="sDate" id="startDateReport" data-validation="required">
                                </li>
                                <li>
                                    <label for="eDate">End Date</label> *
                                </li>
                                <li>
                                    <input type="text" name="eDate" id="endDateReport" data-validation="required">
                                </li>
                            </ul>
                        </div>
                        <div class="required">
                            * Required
                        </div>
                    </div>
                    <div class="modal-footer">
                        <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                        <button id="btn-report-transaction" type="submit" class="btn btn-green">Submit</button>
                        <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @stop

    @section('js')
    <script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
    <script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
    <script src="{{Asset('js/chosenNewJournal.jquery.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
    <script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
    <script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
    <script>
$("form").bind("submit", function (event) {
    setTimeout(function () {
        if ($(".form-error")[0]) {
            $("#btn-save").prop("disabled", false);
        } else {
            $("#btn-save").prop("disabled", true);
        }
    }, 100);

});
var getResultSearchSO2 = "<?php echo Route("getResultSearchShippingSO2") ?>";
var baris = 1;
var tanggalHariIni = '<?php echo date('d') . '-' . date('m') . '-' . date('Y') ?>';
var getSearchResultInventoryForSO = "<?php echo Route("getSearchResultInventoryForPacking") ?>";
var hitungStockInventoryWarehouse = "<?php echo Route("hitungStockInventoryWarehouse") ?>";
    </script>
    <script>
        var packingDataBackup = '<?php echo Route('packingDataBackup', Input::get('coa6') . '---;---' . Input::get('typePayment') . '---;---' . Input::get('typeTax') . '---;---' . Input::get('startDate') . '---;---' . Input::get('endDate')) ?>';
    </script>
    <script type="text/javascript" src="{{Asset('js/entry-js-penjualan-add/packing.js')}}"></script>
    <script type="text/javascript" src="{{Asset('js/entry-js-penjualan-add/packingNew.js')}}"></script>
    <script type="text/javascript" src="{{Asset('js/tooltip.info.js')}}"></script>
    <!--<script type="text/javascript" src="{{Asset('js/entry-js-penjualan-add/shippingAddNewDescription.js')}}"></script>-->
    @stop
