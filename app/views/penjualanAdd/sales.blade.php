@extends('template.header-footer')

@section('title')
Penjualan
@stop

@section('title-view')
Penjualan
@stop

@section('main-section')
<div class="white-pane__bordered margbot20">
    <div class="search-wrapper left-icon">
        <span class="glyphicon glyphicon-search"></span>
        <form method='get' id='form-sales'>
            <input type="search" name="search" class="input-stretch" id="input-search-sales" placeholder="Pencarian Berdasarkan Order Penjualan atau Nama Pelanggan">
        </form>
    </div>
</div>

<section>

    <!--    <div id="loading-search" class="spinner none-usual">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
            <h4 class="text-center cgrey fw100">Searching...</h4>
        </div>

    <div class="row">
        <div class="col-sm-16 col-sm-offset-4 margtop30" id="hint-search">
            <h4 class="text-center cgrey fw100">Search sales first to display data.</h4>
            <hr>
        </div>
    </div>-->
    @if(isset($messages))
    @if($messages == 'tidakadaInput')
    <div class="row">
        <div class="col-sm-16 col-sm-offset-4 margtop30" id="hint-search">
            <h4 class="text-center cgrey fw100">Cari Order Penjualan atau Nama Pelanggan untuk Menampilkan Data.</h4>
            <hr>
        </div>
    </div>
    @endif

    @if($messages == 'tidakadaData')
    <div class="row">
        <div class="col-sm-16 col-sm-offset-4 margtop30" id="hint-search">
            <h4 class="text-center cgrey fw100"> Tidak ada data dengan nama {{$sales}}.</h4>
            <hr>
        </div>
    </div>
    @endif
    <p class="text-center margbot20">
        <a href="{{Route('showNewSales')}}" class="button btnprim">Tambah data penjualan</a>
    </p>
    @if($messages == 'adaData')
    <?php $i = 0; ?>
    <div class=" margbot60">
        @foreach(SalesOrderHeader::join('m_coa6','t_salesorder_header.ACC6InternalID','=','m_coa6.InternalID')
        ->where('t_salesorder_header.SalesOrderID', 'like', '%' . str_replace(" ","%",$sales) . '%')
        ->orwhere('m_coa6.ACC6Name', 'like', '%' . str_replace(" ","%",$sales) . '%')
        ->orwhere('m_coa6.ACC6ID', 'like', '%' . str_replace(" ","%",$sales) . '%')
        ->orderBy("t_salesorder_header.SalesOrderID", "asc")
        ->take(100)
        ->select(DB::raw("t_salesorder_header.SalesOrderID as SalesOrderID, t_salesorder_header.SalesOrderDate as SalesOrderDate, t_salesorder_header.GrandTotal as GrandTotal,
        t_salesorder_header.InternalID as SalesOrderInternalID, ACC6InternalID, t_salesorder_header.dtRecord"))
        ->get() as $data)
        <?php if ($i % 3 == 0) { ?>
            <div class="row margbot10">
            <?php } $i++;?>
            <div class="col-sm-8 margbot20">
                <div class="white-pane__bordered">
                    <div class="dropdown pull-right">
                        <button class="button btntrans btn-mini no-shadow dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <span class="glyphicon glyphicon-option-vertical"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="{{Route('showDetailSales',$data->SalesOrderInternalID)}}" class="fw500 text-uppercase"><b>Lihat</b></a></li>
                            <li role="separator" class="divider"></li>
                            <?php
                                $arr = DB::select("select s.Status from t_sales_header s, t_salesorder_header so where s.SalesOrderInternalID = so.InternalID and so.InternalID = ?",array($data->SalesOrderInternalID));
                                if(count($arr) > 0)
                                {
                                    $status = $arr[0]->Status;
                                    if($status == "0")
                                    {
                                        echo '<li><a href="'.Route('showUpdateSales',$data->SalesOrderInternalID).'" class="fw500 text-uppercase"><b>Ubah</b></a></li>';
                                    }
                                }
                            ?>
                        </ul>
                    </div>

                    <p class="fw400 margbot5">{{$data->SalesOrderID}} | Bpk/Ibu {{Coa6::find($data->ACC6InternalID)->ACC6Name}}</p>
                    <p class="fw300 margbot20 cgrey"><small>{{date('d M Y',strtotime($data->SalesOrderDate))}} {{date('H:i:s',strtotime($data->dtRecord))}}</small></p>
                    <h4 class="fw300 nomarg">Rp. {{number_format($data->GrandTotal, 2, ".", ",")}},-</h4>
                </div>
            </div>
                <?php if ($i % 3 == 0) { ?>
        </div>
        <?php } ?>
        @endforeach
        @endif
        @endif
</section>
@stop

@section('js-content')
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script src="{{Asset('js/sales.js')}}"></script>
@stop