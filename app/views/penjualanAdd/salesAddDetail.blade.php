@extends('template.header-footer')

@section('title')
Sales
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
@if(checkModul('O05'))
<style>
    .li_detail{
        clear: both;
        padding-top: 10px;
    }
    .label_detail{
        float: left;
    }
    .div_detail{
        float: left; width: 65%
    }
</style>
<?php $element = 'div'; ?>
@endif
<style>
    .li_detail{
    }
    .label_detail{
    }
    .div_detail{
    }
</style>
<?php $element = 'span'; ?>
@stop

@section('nav')

@stop

@section('content')
@if(myCheckIsEmpty('SalesOrder;Default;Slip;DepartmentDefault'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one Sales Order, Default COA, and Slip to insert sales.
</div>
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showSales')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Sales</a>
                <a href="{{route('salesDetail',$header->SalesID)}}" type="button" class="btn btn-sm btn-pure">Detail {{$header->SalesID}}</a>
            </div>
            <div class="btn-group margr5">
                <button type="button" <?php if (myCheckIsEmpty('SalesOrder;Default;Slip;DepartmentDefault')) echo 'disabled'; ?> class="btn btn-green btn-sm dropdown-toggle" data-target="#insertSales" data-toggle="modal" role="dialog"
                        aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
            </div>
            <!--            <div class="btn-group margr5">
                            <a href="{{Route('salesCashNew')}}">
                                <button type="button" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-plus"></span> New Cash </button>
                            </a>
                        </div>-->
            <button id="search-button" <?php if (myCheckIsEmpty('Sales')) echo 'disabled'; ?>  class="btn btn-green btn-sm margr5"><span class="glyphicon glyphicon-search"></span> Search </button>
            @if(!SalesHeader::isReturn($header->SalesID))
            @if((JournalDetail::where('JournalTransactionID', $header->SalesID)->sum('JournalCreditMU') == 0 && $header->isCash != 0) || $header->isCash == 0)
            <a href="{{Route('salesUpdate',$header->SalesID)}}">
                <button id="btn-{{$header->SalesID}}-update"
                        class="btn btn-green btn-sm margr5">
                    <span class="glyphicon glyphicon-edit"></span> Edit
                </button>
            </a>
            @else
            <button disabled class="btn btn-green btn-sm margr5"><span class="glyphicon glyphicon-edit"></span> Edit</button>
            @endif
            @else
            <button disabled class="btn btn-green btn-sm margr5"><span class="glyphicon glyphicon-edit"></span> Edit</button>
            @endif
            @if(checkModul('O04'))
            <div class="btn-group">
                <button type="button" class="btn btn-green dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="glyphicon glyphicon-file"></span> Report & Print <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a data-target="#r_summary" data-toggle="modal" role="dialog" href="#" id="btn-rDetail">Detail Report</a></li>
                    <li><a data-target="#r_summary" data-toggle="modal" role="dialog" href="#" id="btn-rSummary">Summary Report</a></li>
                    <li><a data-target="#r_sales" data-toggle="modal" role="dialog" href="#" id="btn-rSales">Sales Report</a></li>
                    <li><a target="_blank" href="{{Route("uncomplateSalesOrder")}}">Uncomplate Sales Order Report</a></li>
                    <li>
                                                <a href="{{Route('salesPrint',$header->SalesID)}}" id="btn-{{$header->SalesID}}-print" target='_blank' style="margin-right: 0px !important;"> Print</a>
<!--                        <a href="#" data-target="#r_print" data-internal="{{$header->SalesID }}"  data-toggle="modal" role="dialog"
                           onclick="printAttach(this)" data-id="{{$header->SalesID}}" data-name="{{$header->SalesID}}">
                            Print
                        </a>-->
                    </li>
                    <!--<li><a href="{{Route('salesOrderPrintSJ',$header->SalesOrderID)}}" id="btn-{{$header->SalesOrderID}}-print" target='_blank' style="margin-right: 0px !important;">Print SJ</a></li>-->
                    <!--<li><a href="{{Route('salesInternalPrint',$header->SalesID)}}" id="btn-{{$header->SalesID}}-print" target='_blank' style="margin-right: 0px !important;">Internal</a></li>-->
                </ul>
            </div>


            <!--            <button type="button" class="btn btn-green margr5" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rSummary">
                            <span class="glyphicon glyphicon-file"></span> Summary Report</button>
                        <button type="button" class="btn btn-green margr5" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rDetail">
                            <span class="glyphicon glyphicon-file"></span> Detail Report</button>-->


            <!--             <a href="{{Route('salesPrint',$header->SalesID)}}" id="btn-{{$header->SalesID}}-print" target='_blank' style="margin-right: 0px !important;">
                            <button type="buttRoute('salesPrint',$header->SalesID)}}on" class="btn btn-green">
                                Print
                            </button>
                        </a>-->
            <!--            <a href="#" data-target="#r_print" data-internal="{{$header->SalesID }}"  data-toggle="modal" role="dialog"
                           onclick="printAttach(this)" data-id="{{$header->SalesID}}" data-name="{{$header->SalesID}}">
                            <button type="button" class="btn btn-green">
                                <span class="glyphicon glyphicon-print"></span> Print
                            </button>
                        </a>-->
            <!--            <a href="{{Route('salesInternalPrint',$header->SalesID)}}" id="btn-{{$header->SalesID}}-print" target='_blank' style="margin-right: 0px !important;">
                            <button type="button" class="btn btn-green">
                                <span class="glyphicon glyphicon-print"></span> Internal
                            </button>
                        </a>-->
            @endif
            <button type="button" class="btn btn-green" data-target="#r_tax" data-toggle="modal" role="dialog" id="btn-rDetail">
                <span class="glyphicon glyphicon-file"></span> Tax Report</button>
        </div>
        <!-- <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Search</h4>
            </div>
            <div class="tableadd">
                <ul class="searchmenu">
                    <form method="GET" action="{{Route('showSales')}}">
                        <li>
                            <label for="coa6">Customer</label>
                            <br>
                            <select class="chosen-select" id="coa6" style="" name="coa6">
                                <option value="-1">All Customer</option>
                                @foreach(Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $coa6)
                                <option value="{{$coa6->InternalID}}">
                                    {{$coa6->ACC6ID.' '.$coa6->ACC6Name}}
                                </option>
                                @endforeach
                            </select>
                        </li>
                        <li><label for="typePayment">Payment Type</label>
                            <br>
                            <select name="typePayment" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Cash</option>
                                <option value="1">Credit</option>
                            </select>
                        </li>
                        <li><label for="typeTax">Tax Type</label>
                            <br>
                            <select name="typeTax" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Non Tax</option>
                                <option value="1">Tax</option>
                            </select>
                        </li>
                        <li><label for="date">Start Date</label>
                            <br><input id="startDate" name="startDate" type="text" autocomplete="off">
                        </li>
                        <li><label for="date">End Date</label>
                            <br><input id="endDate" name="endDate" type="text" autocomplete="off">
                        </li>
                        <li><br>
                            <button type="submit" class="btn btn-green btn-sm">Search <span class="glyphicon glyphicon-search"></span></button>
                        </li>
                        <li><br>
                            <button id="cancel" type="reset"  class="btn btn-pure btn-sm">Cancel <span class="glyphicon glyphicon-remove"></span></button>
                        </li>
                    </form>
                </ul>
            </div><!---- end div tableadd---->
        <!--</div><!---- end div tabwrap---->
        @include('template.searchComponentTransactionModule')

        <div class="tabwrap">
            <div class="tabhead">
                <h4 class="headtitle">{{'Sales '.$header->SalesID}}</h4>
            </div>
            <div class="tableadd">
                <div class="headinv new">
                    <ul @if(checkModul('O05')) class="pull-left" style="width: 360px;" @else class="pull-left" @endif>
                         <li>
                            <label for="orderID" style="float: left;">Order ID</label>
                            <{{$element}} class="div_detail">{{$header->SalesOrder->SalesOrderID}}</{{$element}}>
                        </li>
                        <li class="li_detail">
                            <label for="date" style="float: left;">Date</label>
                            <{{$element}} class="div_detail">{{date( "d-m-Y", strtotime($header->SalesDate))}}</{{$element}}>
                        </li>
                        <li class="li_detail">
                            <label for="customer" style="float: left;">Customer</label>
                            <{{$element}} class="div_detail"><?php
                            $coa6 = SalesAddHeader::find($header->InternalID)->coa6;
                            echo $coa6->ACC6ID . ' ' . $coa6->ACC6Name
                            ?>
                            </{{$element}}>
                        </li>
                        <li class="li_detail">
                            <label for="longTerm" style="float: left;">Payment</label>
                            @if($header->isCash == 0)
                            <{{$element}} class="div_detail">{{'Cash'}}</{{$element}}>
                            @elseif($header->isCash == 1)
                            <{{$element}} class="div_detail">{{'Credit'}}</{{$element}}>
                            @elseif($header->isCash == 2)
                            <{{$element}} class="div_detail">{{'CBD'}}</{{$element}}>
                            @elseif($header->isCash == 3)
                            <{{$element}} class="div_detail">{{'Deposit'}}</{{$element}}>
                            @elseif($header->isCash == 4)
                            <{{$element}} class="div_detail">{{'Down Payment'}}</{{$element}}>
                            @endif
                        </li>
                        @if($header->isCash != 0)
                        <li class="li_detail">
                            <label for="longTerm" style="float: left;">Due Date</label>
                            <{{$element}} class="div_detail">{{date( "d-m-Y", strtotime("+".$header->LongTerm." day",strtotime($header->SalesDate)))}}</{{$element}}>
                        </li>
                        @endif
                        <li class="li_detail">
                            <label for="Vehicle" style="float: left;">PO Customer</label>
                            <div style="float: left; width: 65%">{{SalesOrderHeader::find($header->SalesOrderInternalID)->POCustomer}}</div>
                        </li>
                    </ul>
                    <ul @if(checkModul('O05')) class="pull-left" style="width: 360px;" @else class="pull-right" @endif>
                         <li>
                            <label for="payment" style="float: left;">Status</label>
                            <?php
                            $tampReceiv = 'Completed';
                            foreach (SalesHeader::getSalesReceivable() as $receiv) {
                                if ($receiv->ID == $header->SalesID) {
                                    $tampReceiv = 'Uncompleted';
                                }
                            }
                            ?>
                            <{{$element}} class="div_detail">{{$tampReceiv}}</{{$element}}>
                        </li>
                        <li class="li_detail">
                            <label for="warehouse" style="float: left;">Warehouse</label>
                            <{{$element}} class="div_detail">{{$header->Warehouse->WarehouseName}}</{{$element}}>
                        </li>
                        <li class="li_detail">
                            <label for="currency" style="float: left;">Currency</label>
                            <{{$element}} class="div_detail">{{$header->Currency->CurrencyName}}</{{$element}}>
                        </li>
                        <li class="li_detail">
                            <label for="rate" style="float: left;">Rate</label>
                            <{{$element}} class="div_detail">{{number_format($header->CurrencyRate,'2','.',',')}}</{{$element}}>
                        </li>
                        <!--                        <li class="li_detail">
                                                    <label for="VAT" style="float: left;">VAT</label>
                                                    @if($header->VAT == 0)
                                                    <{{$element}} class="div_detail">{{'Non Tax'}}</{{$element}}>
                                                    @else
                                                    <{{$element}} class="div_detail">{{'Tax'}}</{{$element}}>
                                                    @endif
                                                </li>-->
                        <li class="li_detail">
                            <label for="" style="float: left;">Remark</label>
                            <{{$element}} class="div_detail">{{$header->Remark}}</{{$element}}>
                        </li>
                    </ul>
                    @if(checkModul('O05'))
                    <ul class="pull-left" style="width: 360px;">
                        <li>
                            <label for="transactionType" style="float: left;">Transaction</label>
                            @if($header->TransactionType == 1)
                            <div style="float: left; width: 65%">For who is not collect PPN</div>
                            @elseif($header->TransactionType == 2)
                            <div style="float: left; width: 65%">For Chamberlain</div>
                            @elseif($header->TransactionType == 3)
                            <div style="float: left; width: 65%">Except Chamberlain</div>
                            @elseif($header->TransactionType == 4)
                            <div style="float: left; width: 65%">DPP other value</div>
                            @elseif($header->TransactionType == 6)
                            <div style="float: left; width: 65%">Other handover, include handover to foreigner tourist in the event of VAT refund</div>
                            @elseif($header->TransactionType == 7)
                            <div style="float: left; width: 65%">Handover PPN is not collect</div>
                            @elseif($header->TransactionType == 8)
                            <div style="float: left; width: 65%">Handover PPN Freed</div>
                            @elseif($header->TransactionType == 9)
                            <div style="float: left; width: 65%">Handover Assets (Pasal 16D UU PPN)</div>
                            @endif
                        </li>
                        <li class="li_detail">
                            <label for="replacement" style="float: left;">Replacement</label>
                            @if($header->Replacement == 1)
                            <div style="float: left; width: 65%">Tax Replacement</div>
                            @else
                            <div style="float: left; width: 65%">Non Tax Replacement</div>
                            @endif
                        </li>
                        <li class="li_detail">
                            <label for="Taxnumber" style="float: left;">Tax Number</label>
                            <div style="float: left; width: 65%">{{$header->TaxNumber}}</div>
                        </li>
                        <li class="li_detail">
                            <label for="Taxmonth" style="float: left;">Tax Month</label>
                            <div style="float: left; width: 65%">{{date('F',strtotime('2015-'.$header->TaxMonth.'-01'))}}</div>
                        </li>
                        <li class="li_detail">
                            <label for="TaxYear" style="float: left;">Tax Year</label>
                            <div style="float: left; width: 65%">{{$header->TaxYear}}</div>
                        </li>
                    </ul>
                    @endif
                </div>
                <div class="padrl10">
                    <table class="table master-data" id="table-quotation-description" style="table-layout:fixed;">
                        <thead>
                            <tr>
                                <th style="width: 25%;">Inventory</th>
                                <th style="width: 10%;">Uom</th>
                                <th style="width: 10%;">Qty</th>
                                <th style="width: 15%;">Price</th>
                                <th style="width: 10%;">Disc (%)</th>
                                <th style="width: 10%;">Disc</th>
                                <th style="width: 15%;">Subtotal</th>
                                <th style="width: 14%;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $total = 0;
                            $totalVAT = 0;
                            $barisDesc = 1;
                            ?>
                            @foreach(SalesAddDescription::where('SalesInternalID',$header->InternalID)->get() as $description)
                            <tr style='background: #F1FFDB;' id='rowDescription{{$barisDesc}}'>
                                <td class='chosen-uom'>
                                    {{$description->InventoryText}}
                                </td>
                                <td>
                                    {{$description->UomText}}
                                </td>
                                <td class='text-right'>
                                    {{$description->Qty}}
                                </td>
                                <td class='text-right'>
                                    {{number_format($description->Price,'2','.',',')}}
                                </td>
                                <td class='text-right'>
                                    {{$description->Discount}}
                                </td>
                                <td class='text-right'>
                                    {{number_format($description->DiscountNominal,'2','.',',')}}
                                </td>
                                <td class="right subtotalDescription" id="priceDescription-{{$barisDesc}}-qty-hitung">{{number_format(ceil($description->SubTotal),'0','.',',')}}</td>
                                <td>
                                    <button class="btn btn-pure-xs btn-xs btn-toggle-detail margr5" type="button" data-toshow="show" data="rowDescription{{$barisDesc}}"><span class="glyphicon glyphicon-eye-open"></span></button>
                                    <!--<button class="btn btn-pure-xs btn-xs btn-toggle-spec" type="button" data-toshow="show" data="rowSpec{{$barisDesc}}"><span class="glyphicon glyphicon-comment"></span></button>-->
                                </td>
                            </tr>
<!--                            <tr id='rowSpec{{$barisDesc}}'>
                                <td colspan='8' class='rowSpec{{$barisDesc}}' style="text-align: left">
                                    {{nl2br($description->Spesifikasi)}}
                                </td>-->
                            </tr>
                            <?php
                            $barisDetail = 1;
                            $total += $description->SubTotal;
                            $totalVAT += ceil($description->SubTotal) / 10;
                            ?>
                            @foreach(SalesAddDetail::where('DescriptionInternalID',$description->InternalID)->get() as $detail)
                            <!--untuk non-parcel-->
                            @if($detail->SalesParcelInternalID == 0)
                            <tr id='row{{$barisDetail}}' class='rowDescription{{$barisDesc}}'>
                                <td class='chosen-uom'>
                                    <input type="hidden" class="inventory" style="width: 100px" id="inventory-{{$barisDetail}}" name="inventory{{$barisDesc}}[]" value="{{$detail->InventoryInternalID}}---;---inventory">{{Inventory::find($detail->InventoryInternalID)->InventoryID.' '.Inventory::find($detail->InventoryInternalID)->InventoryName}}
                                </td>
                                <td>
                                    {{Uom::find($detail->UomInternalID)->UomID}}
                                </td>
                                <td class='text-right'>
                                    {{number_format($detail->Qty,'0','.',',')}}
                                </td>
                                <td class='text-right'>
                                    {{number_format($detail->Price,'2','.',',')}}
                                </td>
                                <td class='text-right'>
                                    {{$detail->Discount}}
                                </td>
                                <td class='text-right'>
                                    {{number_format($detail->DiscountNominal,'2','.',',')}}
                                </td>
                                <td class="right subtotal" id="price-{{$barisDetail}}-qty-hitung">{{number_format(ceil($detail->SubTotal),'0','.',',')}}</td>
                                <td>
                                    -
                                </td>
                                {{'';//$totalVAT += $detail->VAT}}
                            </tr>
                            <?php
                            $barisDetail++;
//                            $total += $detail->SubTotal;
                            ?>
                            @endif
                            @endforeach
                            <!--untuk parcel-->
                            @foreach(SalesAddParcel::where('DescriptionInternalID',$description->InternalID)->get() as $parcel)
                            <tr id='row{{$barisDetail}}' class='rowDescription{{$barisDesc}}'>
                                <td class='chosen-uom'>
                                    {{Parcel::find($parcel->ParcelInternalID)->ParcelID.' '.Parcel::find($parcel->ParcelInternalID)->ParcelName}}
                                </td>
                                <td>
                                    -
                                </td>
                                <td class='text-right'>
                                    {{number_format($parcel->Qty,'0','.',',')}}
                                </td>
                                <td class='text-right'>
                                    {{number_format($parcel->Price,'2','.',',')}}
                                </td>
                                <td class='text-right'>
                                    {{$parcel->Discount}}
                                </td>
                                <td class='text-right'>
                                    {{number_format($parcel->DiscountNominal,'2','.',',')}}
                                </td>
                                <td class="right subtotal" id="price-{{$barisDetail}}-qty-hitung">{{number_format($parcel->SubTotal,'2','.',',')}}</td>
                                <td>
                                    -
                                </td>
                                {{'';//$totalVAT += $parcel->VAT}}
                            </tr>
                            <?php
                            $barisDetail++;
//                            $total += $parcel->SubTotal;
                            ?>
                            @endforeach
                            <?php $barisDesc++; ?>
                            @endforeach
                            @if($totalVAT != 0)
                            {{'';$totalVAT = $totalVAT - $header->DiscountGlobal*0.1;}}
                            @endif

                        </tbody>
                    </table>

                    <table class="pull-left">
                        <tr>
                            <td></td>
                        </tr>
                        <tr>
                            <td><p>Created by {{User::where('UserID', $header->UserRecord)->first()->UserName.' '.date('d-m-Y H:i:s', strtotime($header->dtRecord))}}</p></td>
                        </tr>
                        <tr>
                            @if($header->UserModified != '0')
                            <td><p>Modified by {{User::where('UserID', $header->UserModified)->first()->UserName.' '.date('d-m-Y H:i:s', strtotime($header->dtModified))}}</p></td>
                            @else
                            <td><p>Modified by -</p></td>
                            @endif
                        </tr>
                    </table>
                    @if(count($detail) > 0)


                    <table class="pull-right">
                        <tr>
                            <td><h5 class="right margr10 h5total"><b>Total</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="total">{{number_format(ceil($total),'0','.',',')}}</b></h5></td>
                        </tr>
                        <tr >
                            <td><h5 class="right margr10 h5total"><b>Discount</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total new"><b>{{number_format($header->DiscountGlobal, '0', '.',',')}}</b></h5></td>
                        </tr>

                        <tr >
                            <td><h5 class="right margr10 h5total"><b>Down Payment</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total new"><b>{{number_format($header->DownPayment, '0', '.',',')}}</b></h5></td>
                        </tr>

                        <tr>
                            <td><h5 class="right margr10 h5total"><b>Grand Total</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="grandTotal">{{number_format(ceil($total-$header->DiscountGlobal-$header->DownPayment),'0','.',',')}}</b></h5></td>
                        </tr>

                        <tr>
                            <td><h5 class="right margr10 h5total hidevat"><b>Tax</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                            @if($totalVAT == 0)
                            <td><h5 class="right margr10 h5total hidevat"><b id="tax">{{number_format(0,'0','.',',')}}</b></h5></td>
                            @else
                            <td><h5 class="right margr10 h5total hidevat"><b id="tax">{{number_format(floor($totalVAT-$header->DownPayment*0.1),'0','.',',')}}</b></h5></td>
                            @endif
                        </tr>
                        <tr>
                            <td><h5 class="right margr10 h5total hidevat"><b>Grand Total(tax)</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b id="grandTotalAfterTax">{{number_format(ceil($header->GrandTotal),'0','.',',')}}</b></h5></td>
                        </tr>
                    </table>

                    @endif
                </div><!---- end div padrl10---->
            </div><!---- end div tableadd---->
        </div><!---- end div tabwrap---->
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->

@stop


@section('modal')
<div class="modal fade" id="insertSales" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insert Sales</h4>
            </div>
            <form action="" method="post" class="action" id="form-so">
                {{'';$hitung = 0;}}
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="insertSales" id="jenisSales" name="jenis">
                            <li>
                                <label for="sales">Sales Order ID</label> *
                            </li>
                            <input class="input-theme margbot10" type="text" id="searchSalesOrder" title="Type Sales Order Name or ID then 'Enter'" placeholder="Type Sales Order Name or ID then 'Enter'">
                            <li id="selectSalesOrder">

                            </li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btn-add-so" @if($hitung==0){{'disabled'}}@endif class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="r_summary" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Summary Report</h4>
            </div>
            <form action="" method="post" class="action" id="" target="_blank">
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='summarySales'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateReport" data-validation="required">
                            </li>
                        </ul>
                    </div>
                    <div class="required">
                        * Required
                    </div>
                </div>
                <div class="modal-footer">
                    <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                    <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="r_tax" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Tax Report</h4>
            </div>
            <form action="" method="post" class="action" id="" target="_blank">
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='taxSales'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateTaxReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateTaxReport" data-validation="required">
                            </li>
                        </ul>
                    </div>
                    <div class="required">
                        * Required
                    </div>
                </div>
                <div class="modal-footer">
                    <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                    <button id="btn-report-transaction" type="submit" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="r_print" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Tax</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="" target="_blank">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='printSales'>
                            <input type="hidden" name="internalID" id="internalID" />
                            <li>
                                <label for="type">Type Tax</label> *
                            </li>
                            <li>
                                <select class="form-control" name="type">
                                    <option value="include">Include</option>
                                    <option value="exclude">Exclude</option>
                                </select>
                            </li>
                        </ul>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="r_sales" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Sales Report</h4>
            </div>
            <form action="" method="post" class="action" id="" target="_blank">
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='salesReport'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateSalesReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateSalesReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate"></label>
                                <input type="checkbox" value="hide" name="hide">Hide Column
                            </li>
                        </ul>
                    </div>
                    <div class="required">
                        * Required
                    </div>
                </div>
                <div class="modal-footer">
                    <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                    <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script><script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script>
                               var getResultSearchSO = "<?php echo Route("getResultSearchSO") ?>";
                               $(document).ready(function () {
                                   var ppn = '<?php echo $header->VAT ?>'
                                   if (ppn == 1) {
                                       $('.hidevat').show();
                                   } else {
                                       $('.hidevat').hide();
                                   }
                               });
</script>
<script>
    var salesDataBackup = '<?php echo Route('salesDataBackup', Input::get('coa6') . '---;---' . Input::get('typePayment') . '---;---' . Input::get('typeTax') . '---;---' . Input::get('startDate') . '---;---' . Input::get('endDate')) ?>';
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-penjualan-add/salesAdd.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
@stop
