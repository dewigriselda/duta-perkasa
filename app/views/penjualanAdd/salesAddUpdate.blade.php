@extends('template.header-footer')

@section('title')
Sales
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<style>
    .help-block {
        margin-left: 30% !important;
    }
    .headinv .chosen-single{
        width: 200px !important;
    }
    .headinv .chosen-drop{
        width: 200px !important;
    }
</style>
@stop

@section('nav')

@stop

@section('content')
@if(myCheckIsEmpty('SalesOrder;Default;Slip;DepartmentDefault'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one Sales Order, Default COA, and Slip to insert sales.
</div>
@endif
@if(isset($messages))
@if($messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Sales has been updated.
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showSales')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Sales</a>
                <a href="{{route('salesUpdate',$header->SalesID)}}" type="button" class="btn btn-sm btn-pure">Update {{$header->SalesID}}</a>
            </div>
            <div class="btn-group margr5">
                <button type="button" <?php if (myCheckIsEmpty('SalesOrder;Default;Slip;DepartmentDefault')) echo 'disabled'; ?> class="btn btn-green btn-sm dropdown-toggle" data-target="#insertSales" data-toggle="modal" role="dialog"
                        aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
            </div>
            <!--            <div class="btn-group margr5">
                            <a href="{{Route('salesCashNew')}}">
                                <button type="button" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-plus"></span> New Cash </button>
                            </a>
                        </div>-->
            <button id="search-button" <?php if (myCheckIsEmpty('Sales')) echo 'disabled'; ?>  class="btn btn-green btn-sm margr5"><span class="glyphicon glyphicon-search"></span> Search </button>
            @if(checkModul('O04'))
            <button type="button" class="btn btn-green margr5" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rSummary">
                <span class="glyphicon glyphicon-file"></span> Summary Report</button>
            <button type="button" class="btn btn-green" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rDetail">
                <span class="glyphicon glyphicon-file"></span> Detail Report</button>
            @endif
            <button type="button" class="btn btn-green margr5" data-target="#r_sales" data-toggle="modal" role="dialog" id="btn-rSales">
                <span class="glyphicon glyphicon-file"></span> Sales Report</button>
            <button type="button" class="btn btn-green" data-target="#r_tax" data-toggle="modal" role="dialog" id="btn-rDetail">
                <span class="glyphicon glyphicon-file"></span> Tax Report</button>
                <a href="{{Route('salesPrint',$header->SalesID)}}" id="btn-{{$header->SalesID}}-print" target='_blank' style="margin-right: 0px !important;"><button type="button" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-print"></span>Print</button></a>
<!--            <button type="button" class="btn btn-green btn-sm" data-target="#r_print" data-internal="{{$header->SalesID }}"  data-toggle="modal" role="dialog"
                    onclick="printAttach(this)" data-id="{{$header->SalesID}}" data-name="{{$header->SalesID}}">
                Print
            </button>-->
        </div>
        <!-- <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Search</h4>
            </div>
            <div class="tableadd">
                <ul class="searchmenu">
                    <form method="GET" action="{{Route('showSales')}}">
                        <li>
                            <label for="coa6">Customer</label>
                            <br>
                            <select class="chosen-select" id="coa6" style="" name="coa6">
                                <option value="-1">All Customer</option>
                                @foreach(Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $coa6)
                                <option value="{{$coa6->InternalID}}">
                                    {{$coa6->ACC6ID.' '.$coa6->ACC6Name}}
                                </option>
                                @endforeach
                            </select>
                        </li>
                        <li><label for="typePayment">Payment Type</label>
                            <br>
                            <select name="typePayment" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Cash</option>
                                <option value="1">Credit</option>
                            </select>
                        </li>
                        <li><label for="typeTax">Tax Type</label>
                            <br>
                            <select name="typeTax" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Non Tax</option>
                                <option value="1">Tax</option>
                            </select>
                        </li>
                        <li><label for="date">Start Date</label>
                            <br><input id="startDate" name="startDate" type="text" autocomplete="off">
                        </li>
                        <li><label for="date">End Date</label>
                            <br><input id="endDate" name="endDate" type="text" autocomplete="off">
                        </li>
                        <li><br>
                            <button type="submit" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-search"></span> Search </button>
                        </li>
                        <li><br>
                            <button id="cancel" type="reset"  class="btn btn-pure btn-sm"><span class="glyphicon glyphicon-remove"></span> Cancel </button>
                        </li>
                    </form>
                </ul>
            </div><!---- end div tableadd---->
        <!--</div><!---- end div tabwrap---->
        @include('template.searchComponentTransactionModule')
        <form method="POST" action="" id='form-updateSalesAdd'>
            <input type='hidden' name='SalesInternalID' value='{{$header->InternalID}}'>
            <input type='hidden' name='SalesOrderInternalID' id="salesOrderInternalID" value='{{$header->SalesOrderInternalID}}'>
            <div class="tabwrap">
                <div class="tabhead">
                    <h4 class="headtitle">Sales <span id="salesID">{{$header->SalesID}}</span></h4>
                </div>
                <div class="tableadd">
                    <div class="headinv new">
                        <ul @if(checkModul('O05')) class="pull-left" style="width: 360px;" @else class="pull-left"  @endif>
                             <li>
                                <label for="date">Date</label>
                                <span>{{date( "d-m-Y", strtotime($header->SalesDate))}}</span>
                            </li>
                            <li>
                                <label for="customer">Customer</label>
                                <span><?php
                                    $coa6 = SalesHeader::find($header->InternalID)->coa6;
                                    echo $coa6->ACC6ID . ' ' . $coa6->ACC6Name
                                    ?></span>
                            </li>

                            <li>
                                <label for="longTerm">Payment</label>
                                @if($header->isCash == 0)
                                <span>{{'Cash'}}</span>
                                @elseif($header->isCash == 1)
                                <span>{{'Credit'}}</span>
                                @elseif($header->isCash == 2)
                                <span>{{'CBD'}}</span>
                                @elseif($header->isCash == 3)
                                <span>{{'Deposit'}}</span>
                                @else
                                <span>{{'Down Payment'}}</span>
                                @endif
                            </li>
                            @if($header->isCash != 0)
                            <li>
                                <label for="longTerm">Due Date</label>
                                <span>{{date( "d-m-Y", strtotime("+".$header->LongTerm." day",strtotime($header->SalesDate)))}}</span>
                            </li>
                            @else
                            <li>
                                <label for="slip">Slip number *</label>
                                <?php
                                $slipInternal = '';
                                $journal = JournalHeader::where('TransactionID', '=', $header->SalesID)->where('CompanyInternalID', Auth::user()->Company->InternalID)->get();
                                if (count($journal) > 0) {
                                    $slipInternal = $journal[0]->SlipInternalID;
                                } else {
                                    $slipInternal = '';
                                }
                                if ($header->isCash == 0) {
                                    $slipDefault = '';
                                } else {
                                    $slipDefault = 'Disabled = "true"';
                                }
                                ?>
                                <select class="chosen-select" id="slip" style="" name="slip"  {{$slipDefault}}>
                                    @foreach(Slip::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $slip)
                                    @if($slip->InternalID == $slipInternal)
                                    <option selected="selected" value="{{$slip->InternalID}}">
                                        {{$slip->SlipID.' '.$slip->SlipName}}
                                    </option>
                                    @else
                                    <option value="{{$slip->InternalID}}">
                                        {{$slip->SlipID.' '.$slip->SlipName}}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>
                            </li>
                            @endif
                            <li>
                                <label for="purchasingCommission">Purchasing Commission *</label>
                                <input type="text" name="purchasingCommission" style="width: 200px;" value="{{number_format($header->PurchasingCommission,'2','.',',')}}" id="purchasingCommission" class="nominal numajaDesimal">
                            </li>
                        </ul>
                        <ul @if(checkModul('O05')) class="pull-left" style="width: 360px;" @else class="pull-right"  @endif>
                             <li>
                                <label for="salesOrder">PO Customer</label>
                                <span>{{SalesOrderHeader::find($header->SalesOrderInternalID)->POCustomer}}</span>
                            </li>
                            <li>
                                <label for="warehouse">Warehouse *</label>
                                <select class="chosen-select choosen-modal warehouse" id="warehouseHeader" name="warehouse">
                                    @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->where("Type",Auth::user()->WarehouseCheck)->get() as $war)
                                    @if($war->InternalID == $header->WarehouseInternalID)
                                    <option selected="selected" id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                        {{$war->WarehouseName;}}
                                    </option>
                                    @else
                                    <option id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                        {{$war->WarehouseName;}}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label for="currency">Currency</label>
                                <span>{{'';$currency = Currency::find($header->CurrencyInternalID); $currencyName = $currency->CurrencyName; echo $currencyName}}</span>
                            </li>
                            <li>
                                <label for="rate">Rate</label>
                                <span>{{number_format($header->CurrencyRate,'2','.',',')}}</span>
                            </li>
                            <!--                            <li>
                                                            <label for="VAT">VAT</label>
                                                            @if($header->VAT == 0)
                                                            <span>{{'Non Tax'}}</span>
                                                            <input type="hidden" id="taxSales" value="0">
                                                            @else
                                                            <span>{{'Tax'}}</span>
                                                            <input type="hidden" id="taxSales" value="1">
                                                            @endif
                                                        </li>-->
                            <li>
                                <label for="remark">Remark</label>
                                <textarea name="remark" class="new-textarea-small" id="remark" style="width: 200px">{{$header->Remark}}</textarea>
                            </li>
                            @if(!checkModul('O05'))
                            <li>
                                <div class="required">
                                    * Required
                                </div>
                            </li>
                            @endif
                        </ul>
                        @if(checkModul('O05'))
                        <ul class="pull-left" style="width: 360px;">
                            <li>
                                <label for="transactiontype">Transaction</label>
                                <select class="chosen-select choosen-modal currency" id="transactionType" name="TransactionType">
                                    <option value="1" {{($header->TransactionType == 1 ? 'selected' : '')}}> For who is not collect PPN </option>
                                    <option value="2" {{($header->TransactionType == 2 ? 'selected' : '')}}> For Chamberlain </option>
                                    <option value="3" {{($header->TransactionType == 3 ? 'selected' : '')}}> Except Chamberlain </option>
                                    <option value="4" {{($header->TransactionType == 4 ? 'selected' : '')}}> DPP other value </option>
                                    <option value="6" {{($header->TransactionType == 6 ? 'selected' : '')}}> Other handover, include handover to foreigner tourist in the event of VAT refund </option>
                                    <option value="7" {{($header->TransactionType == 7 ? 'selected' : '')}}> Handover PPN is not collect</option>
                                    <option value="8" {{($header->TransactionType == 8 ? 'selected' : '')}}> Handover PPN Freed</option>
                                    <option value="9" {{($header->TransactionType == 9 ? 'selected' : '')}}> Handover Assets (Pasal 16D UU PPN)</option>
                                </select>
                            </li>
                            <li>
                                <label for="replacement">Replacement</label>
                                @if($header->Replacement == 1)
                                <input style="width:15px; height: 15px;" checked="true" type="checkbox" name="Replacement" id="replacement" value="1"> Tax Replacement
                                @else
                                <input style="width:15px; height: 15px;" type="checkbox" name="Replacement" id="replacement" value="1"> Tax Replacement
                                @endif
                            </li>
                            <li>
                                <label for="numbertax">Tax Number</label>
                                <input type="hidden" name="TaxNumber" value="{{$header->TaxNumber}}" id="numberTax">
                                <?php $taxNumberSplit = explode('-', $header->TaxNumber); ?>
                                <?php $taxNumber1 = explode('.', $taxNumberSplit[0]); ?>
                                <?php $taxNumber2 = explode('.', $taxNumberSplit[1]); ?>
                                <input type="text" class="numaja autoTab" style="width: 40px;" maxlength="3" id="numberTax1"  value="{{$taxNumber1[0]}}"> .
                                <input type="text" class="numaja autoTab" style="width: 40px;" maxlength="3" id="numberTax2"  value="{{$taxNumber1[1]}}"> -
                                <input type="text" class="numaja autoTab" style="width: 30px;" maxlength="2" id="numberTax3"  value="{{$taxNumber2[0]}}"> .
                                <input type="text" class="numaja autoTab" style="width: 75px;" maxlength="8" id="numberTax4"  value="{{$taxNumber2[1]}}">
                            </li>
                            <li>
                                <label for="taxmonth">Tax Month</label>
                                <select class="chosen-select choosen-modal currency" id="taxMonth" name="TaxMonth">
                                    @for($aa = 1; $aa<=12; $aa++)
                                    <option value="{{$aa}}" {{($header->TaxMonth == $aa ? 'selected' : '')}}>{{date('F',strtotime('2015-'.$aa.'-01'))}}</option>
                                    @endfor
                                </select>
                            </li>
                            <li>
                                <label for="taxyear">Tax Year</label>
                                {{''; $year = date('Y'); $mulai = 2012;}}
                                <select class="chosen-select choosen-modal currency" id="taxYear" name="TaxYear">
                                    @while($mulai <= $year)
                                    @if( $header->TaxYear == $mulai )
                                    <option selected="true" value="{{$mulai}}">{{$mulai}}</option>
                                    @else
                                    <option value="{{$mulai}}">{{$mulai}}</option>
                                    @endif
                                    {{'';$mulai++;}}
                                    @endwhile
                                </select>
                            </li>
                            <li>
                                <div class="required">
                                    * Required
                                </div>
                            </li>
                        </ul>
                        @endif
                    </div>
                    <table class="table master-data" id="table-salesorder-description" style="table-layout:fixed;">
                        <thead>
                            <tr>
                                <th style="width: 15%;">Inventory</th>
                                <th style="width: 5%;">Uom</th>
                                <th style="width: 5%;">Stock</th>
                                <th style="width: 5%;">Qty Order</th>
                                <th style="width: 10%;">Price</th>
                                <th style="width: 5%;">Disc (%)</th>
                                <th style="width: 10%;">Disc</th>
                                <th style="width: 10%;">Subtotal</th>
                                <th style="width: 5%;">Max Qty Sales</th>
                                <th style="width: 8%;">Qty</th>
                                <th style="width: 10%;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 0;
                            $barisDesc = 1;
                            $barisDetail = 1;
                            ?>
                            @foreach(SalesOrderDescription::where('SalesOrderInternalID',$headerorder->InternalID)->get() as $description)
                            <?php
                            $sumDescription = SalesAddHeader::getSumDescriptionSalesExcept($description->InternalID, $header->InternalID);
                            if ($sumDescription == '') {
                                $sumDescription = '0';
                            }
                            $sumDescriptionReturn = SalesReturnHeader::getSumReturnDescription($description->InternalID, $description->SalesOrderInternalID);
                            if ($sumDescriptionReturn == '') {
                                $sumDescriptionReturn = '0';
                            }
                            $qtyDescription = SalesAddDescription::where('SalesOrderDescriptionInternalID', $description->InternalID)
                                            ->where('SalesInternalID', $header->InternalID)->pluck('Qty');
                            ?>
                            <tr style='background: #F1FFDB;' id='rowDescription{{$barisDesc}}'>
                                <td class='chosen-uom'>
                                    {{$description->InventoryText}}
                                </td>
                                <td>
                                    {{$description->UomText}}
                                </td>
                                <td class="text-right">
                                    -
                                </td>
                                <td id="qtyDescription-{{$barisDesc}}" class="text-right">
                                    {{number_format($description->Qty,'0','.',',')}}
                                </td>
                                <td class="text-right">
                                    {{number_format($description->Price,'2','.',',')}}
                                </td>
                                <td class="text-right">
                                    {{number_format($description->Discount,'0','.',',')}}
                                </td>
                                <td class="text-right">
                                    {{number_format($description->DiscountNominal,'2','.',',')}}
                                </td>
                                <td id="subtotalDescription-{{$barisDesc}}" class="text-right">
                                    {{number_format(ceil($description->SubTotal),'2','.',',')}}
                                </td>
                                <td class="text-right">
                                    {{number_format($description->Qty - $sumDescription+$sumDescriptionReturn,'0','.',',')}}
                                </td>
                                <td class='text-right'>
                                    <input type="text" min="0" max="{{$description->Qty - $sumDescription+$sumDescriptionReturn}}" class="maxWidth qtyDescription right input-theme" name="qtyDescription[]" id="priceDescription-{{$barisDesc}}-qty" value='{{number_format($qtyDescription,'0','.',',')}}'>
                                </td>
                                <td>
                                    <input type="hidden" name="InternalDescription[]" value="{{$description->InternalID}}">
                                    <input type="hidden" name="max[]" value="{{$description->Qty - $sumDescription + $sumDescriptionReturn}}">
                                    <button class="btn btn-pure-xs btn-xs btn-toggle-detail margr5" type="button" data-toshow="show" data="rowDescription{{$barisDesc}}"><span class="glyphicon glyphicon-eye-open"></span></button>
                                    <!--<button class="btn btn-pure-xs btn-xs btn-toggle-spec" type="button" data-toshow="show" data="rowSpec{{$barisDesc}}"><span class="glyphicon glyphicon-comment"></span></button>-->
                                </td>
                            </tr>
                            <tr id='rowSpec{{$barisDesc}}' style="display:none">
                                <td colspan='10' class='rowSpec{{$barisDesc}}' style="text-align: left">
                                    {{nl2br($description->Spesifikasi)}}
                                </td>
                            </tr>
                            @foreach(SalesOrderDetail::where('DescriptionInternalID',$description->InternalID)->get() as $detail)
                            <!--untuk non-parcel-->
                            @if($detail->SalesOrderParcelInternalID == 0)
                            <?php
                            $sumSales = SalesAddHeader::getSumSalesExcept($detail->InventoryInternalID, $header->InternalID, $detail->InternalID);
                            if ($sumSales == '') {
                                $sumSales = '0';
                            }
                            $sumSalesReturn = SalesReturnHeader::getSumReturnOrder($detail->InventoryInternalID, $header->SalesOrderInternalID, $detail->InternalID);
                            if ($sumSalesReturn == '') {
                                $sumSalesReturn = '0';
                            }
                            $qtyDetail = SalesAddDetail::where('SalesOrderDetailInternalID', $detail->InternalID)
                                            ->where('SalesInternalID', $header->InternalID)->pluck('Qty');
                            ?>
                            <tr id='row{{$barisDetail}}' class='rowDescription{{$barisDesc}}'>
                                <td class='chosen-uom'>
                                    {{Inventory::find($detail->InventoryInternalID)->InventoryID.' '.Inventory::find($detail->InventoryInternalID)->InventoryName}}
                                </td>
                                <td>
                                    {{Uom::find($detail->UomInternalID)->UomID.' '.Uom::find($detail->UomInternalID)->UomName}}
                                </td>
                                <td class="right currentStock" id="currentStock-{{$barisDetail}}">
                                    {{getEndStockInventoryWithWarehouse($detail->InventoryInternalID,Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->first()->InternalID)}}
                                </td>
                                <td id="qtySales-{{$barisDetail}}" class="text-right">
                                    {{$detail->Qty}}
                                    <input type="hidden" name="stockcurrent" id="stockCurrent-{{$barisDetail}}" value="{{getEndStockInventoryWithWarehouse_angka($detail->InventoryInternalID,Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->first()->InternalID)}}">
                                </td>
                                <td class="text-right">
                                    {{number_format($detail->Price,'2','.',',')}}
                                </td>
                                <td class="text-right">
                                    {{$detail->Discount}}
                                </td>
                                <td class="text-right">
                                    {{number_format($detail->DiscountNominal,'2','.',',')}}
                                </td>
                                <td id="subtotalSales-{{$barisDetail}}" class="text-right">
                                    {{number_format(ceil($detail->SubTotal),'2','.',',')}}
                                </td>
                                <td class="text-right">
                                    {{$detail->Qty - $sumSales+$sumSalesReturn}}
                                </td>
                                <td class='text-right'>
                                    <input type="hidden" name="InternalDetail{{$barisDesc}}[]" value="{{$detail->InternalID}}">
                                    <input type="hidden" name="tipe{{$barisDesc}}[]" value="inventory">
                                    <input type="text" class="maxWidth addSales quantitySales right input-theme" name="qty{{$barisDesc}}[]" min="0" max="{{$detail->Qty-$sumSales+$sumSalesReturn}}" value="{{number_format($qtyDetail,'0','.',',')}}" id="price-{{$barisDetail}}-qty">
                                </td>
                                <td>
                                    -
                                </td>
                            </tr>
                            <?php $barisDetail++; ?>
                            @endif <!--tutup if non parcel -->
                            @endforeach<!--tutup foreach quotation detail-->
                            <!--untuk parcel-->
                            @foreach(SalesOrderParcel::where('DescriptionInternalID',$description->InternalID)->get() as $parcel)
                            <?php
                            $sumSalesParcel = SalesAddHeader::getSumSalesExceptParcel($parcel->ParcelInternalID, $header->InternalID, $parcel->InternalID);
                            if ($sumSalesParcel == '') {
                                $sumSalesParcel = '0';
                            }
                            $sumSalesReturnParcel = SalesReturnHeader::getSumReturnOrderParcel($parcel->ParcelInternalID, $header->SalesOrderInternalID, $parcel->InternalID);
                            if ($sumSalesReturnParcel == '') {
                                $sumSalesReturnParcel = '0';
                            }
                            $qtyDetail = SalesAddParcel::where('SalesOrderParcelDetailInternalID', $parcel->InternalID)
                                            ->where('SalesInternalID', $header->InternalID)->pluck('Qty');
                            ?>
                            <tr id='row{{$barisDetail}}' class='rowDescription{{$barisDesc}}'>
                                <td class='chosen-uom'>
                                    {{Parcel::find($parcel->ParcelInternalID)->ParcelID.' '.Parcel::find($parcel->ParcelInternalID)->ParcelName}}
                                </td>
                                <td>
                                    -
                                </td>
                                <td>
                                    -
                                </td>
                                <td id="qtySales-{{$barisDetail}}">
                                    {{$parcel->Qty}}
                                </td>
                                <td>
                                    {{$parcel->Price}}
                                </td>
                                <td>
                                    {{$parcel->Discount}}
                                </td>
                                <td>
                                    {{$parcel->DiscountNominal}}
                                </td>
                                <td id="subtotalSales-{{$barisDetail}}">
                                    {{$parcel->SubTotal}}
                                </td>
                                <td>
                                    {{$parcel->Qty - $sumSalesParcel+$sumSalesReturnParcel}}
                                </td>
                                <td class='text-right'>
                                    <input type="hidden" name="InternalDetail{{$barisDesc}}[]" value="{{$parcel->InternalID}}">
                                    <input type="hidden" name="tipe{{$barisDesc}}[]" value="parcel">
                                    <input type="text" class="maxWidth quantitySales addSales right input-theme" name="qty{{$barisDesc}}[]" maxlength="{{$parcel->Qty - $sumSalesParcel+$sumSalesReturnParcel}}" min="0" value="{{number_format($qtyDetail,'0','.',',')}}" id="price-{{$barisDetail}}-qty"></td>
                                <td>
                                    -
                                </td>
                            </tr>
                            <?php $barisDetail++; ?>
                            @endforeach<!--tutup foreach quotation parcel-->
                            <?php $barisDesc++; ?>
                            @endforeach <!--tutup foreach quotation description-->
                            <?php $i++; ?>
                        </tbody>
                    </table>
                    <input type="hidden" name="grandTotalValue" value="0" id="grandTotalValue">

                    <table class="pull-left">
                        <tr>
                            <td></td>
                        </tr>
                        <tr>
                            <td><p>Created by {{User::where('UserID', $header->UserRecord)->first()->UserName.' '.date('d-m-Y H:i:s', strtotime($header->dtRecord))}}</p></td>
                        </tr>
                        <tr>
                            @if($header->UserModified != '0')
                            <td><p>Modified by {{User::where('UserID', $header->UserModified)->first()->UserName.' '.date('d-m-Y H:i:s', strtotime($header->dtModified))}}</p></td>
                            @else
                            <td><p>Modified by -</p></td>
                            @endif
                        </tr>
                    </table>
                    <table class="pull-right">
                        <tr>
                            <td><h5 class="right margr10 h5total"><b>Total</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="total"></b></h5></td>
                        </tr>
                        <tr >
                        <input type="hidden" id="discountGlobalMax" value="{{($headerorder->DiscountGlobal - SalesAddHeader::getSumDiscountGlobalExcept($header->InternalID, $header->SalesOrderInternalID))}}">
                        <td><h5 class="right margr10 h5total"><b>Discount (Max {{number_format(($headerorder->DiscountGlobal - SalesAddHeader::getSumDiscountGlobalExcept($header->InternalID, $header->SalesOrderInternalID)),'0','.',',')}})</b></h5></td>
                        <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                        <td><h5 class="right margr10 h5total new" style="width: 150px;"><input type="text" style="width: 120px;" class="maxWidth right numaja discountGlobal" name="DiscountGlobal" maxlength="" id="discountGlobal" value="{{number_format($header->DiscountGlobal, 2,'.',',')}}"></h5></td>
                        </tr>
                        <tr <?php if ($header->isCash != 4) echo 'style="display:none"'; ?>>
                            <td><h5 class="right margr10 h5total"><b>Down Payment ({{number_format($header->salesorder->DownPayment,'0','.',',')}}%)</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total new" style="width: 150px;"><input type="text" style="width: 120px;" class="maxWidth right numajaDesimal downPayment margbot10" name="DownPayment" maxlength="" id="dp" value="{{number_format($header->DownPayment, '2', '.', ',')}}"><br /><span class="label label-default" id="nominalDP">{{number_format($downpayment,'2','.',',')}}</span></h5></td>
                        </tr>
                        <tr>
                            <td><h5 class="right margr10 h5total"><b>Grand Total</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="grandTotal"></b></h5></td>
                        </tr>

                        <tr>
                            <td><h5 class="right margr10 h5total hidevat"><b>Tax</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b id="tax"></b></h5></td>
                        </tr>
                        <tr>
                            <td><h5 class="right margr10 h5total hidevat"><b>Grand Total(tax)</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b id="grandTotalAfterTax"></b></h5></td>
                        </tr>
                    </table>
                </div><!---- end div tableadd---->
            </div><!---- end div tabwrap---->
            <div class="btnnest pull-right">
                <button class="btn btn-green btn-sm btn-save" id="btn-save"> Save </button>
            </div>
        </form>
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop

@section('modal')
<div class="modal fade" id="insertSales" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insert Sales</h4>
            </div>
            <form action="" method="post" class="action" id="form-so">
                {{'';$hitung = 0;}}
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="insertSales" id="jenisSales" name="jenis">
                            <li>
                                <label for="sales">Sales Order ID</label> *
                            </li>
                            <input class="input-theme margbot10" type="text" id="searchSalesOrder" title="Type Sales Order Name or ID then 'Enter'" placeholder="Type Sales Order Name or ID then 'Enter'">
                            <li id="selectSalesOrder">

                            </li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btn-add-so" @if($hitung==0){{'disabled'}}@endif class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="r_summary" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Summary Report</h4>
            </div>
            <form action="" method="post" class="action" id="" target="_blank">
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='summarySales'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateReport" data-validation="required">
                            </li>
                        </ul>
                    </div>
                    <div class="required">
                        * Required
                    </div>
                </div>
                <div class="modal-footer">
                    <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                    <button id="btn-report-transaction" type="submit" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="r_tax" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Tax Report</h4>
            </div>
            <form action="" method="post" class="action" id="" target="_blank">
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='taxSales'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateTaxReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateTaxReport" data-validation="required">
                            </li>
                        </ul>
                    </div>
                    <div class="required">
                        * Required
                    </div>
                </div>
                <div class="modal-footer">
                    <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                    <button id="btn-report-transaction" type="submit" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="r_print" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Tax</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="" target="_blank">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='printSales'>
                            <input type="hidden" name="internalID" id="internalID" />
                            <li>
                                <label for="type">Type Tax</label> *
                            </li>
                            <li>
                                <select class="form-control" name="type">
                                    <option value="include">Include</option>
                                    <option value="exclude">Exclude</option>
                                </select>
                            </li>
                        </ul>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="r_sales" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Sales Report</h4>
            </div>
            <form action="" method="post" class="action" id="" target="_blank">
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='salesReport'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateSalesReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateSalesReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate"></label>
                                <input type="checkbox" value="hide" name="hide">Hide Column
                            </li>
                        </ul>
                    </div>
                    <div class="required">
                        * Required
                    </div>
                </div>
                <div class="modal-footer">
                    <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                    <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNewJournal.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script>
                        var getResultSearchSO = "<?php echo Route("getResultSearchSO") ?>";
                        var currentStockShipping = '<?php echo route('currentStockShipping') ?>';
</script>
<script>
    var salesDataBackup = '<?php echo Route('salesDataBackup', Input::get('coa6') . '---;---' . Input::get('typePayment') . '---;---' . Input::get('typeTax') . '---;---' . Input::get('startDate') . '---;---' . Input::get('endDate')) ?>';
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-penjualan-add/salesAdd.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-penjualan-add/salesAddUpdate.js')}}"></script>
@stop
