@extends('template.header-footer')

@section('title')
Sales Order
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosenCustom.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<style>
    .help-block {
        margin-left: 30% !important;
    }
    .headinv .chosen-single{
        width: 200px !important;
    }
    .headinv .chosen-select{
        width: 200px !important;
    }
    .headinv .chosen-drop{
        width: 200px !important;
    }
    .input{
        width: 200px !important;
    }
</style>
@stop

@section('nav')

@stop

@section('content')
@if(myCheckIsEmpty('Customer;Warehouse;Currency;Inventory'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one master Customer, Currency, Warehouse, and Inventory to insert sales.
</div>
@endif

@if(Session::get('messages') == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New sales has been inserted.
</div>
@endif

@if(isset($messages))
@if($messages == 'gagalInsert')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New sales has been inserted.
</div>
<!--<input type="hidden" name="print" id="print" value="{{'';//$print}}">-->
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showSales')}}" type="button" class="btn btn-sm btn-pure">Sales</a>
            </div>
            <div class="btn-group margr5">
                <button type="button" <?php if (myCheckIsEmpty('SalesOrder;Default;Slip;DepartmentDefault')) echo 'disabled'; ?> class="btn btn-green btn-sm dropdown-toggle" data-target="#insertSales" data-toggle="modal" role="dialog"
                        aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
            </div>
            <div class="btn-group margr5">
                <a href="{{Route('salesCashNew')}}">
                    <button type="button" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-plus"></span> New Cash </button>
                </a>
            </div>
            <button id="search-button"  <?php if (myCheckIsEmpty('Sales')) echo 'disabled'; ?> class="btn btn-green btn-sm margr5"><span class="glyphicon glyphicon-search"></span> Search </button>
            @if(checkModul('O04'))
            <button type="button" class="btn btn-green margr5" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rSummary">
                <span class="glyphicon glyphicon-file"></span> Summary Report</button>
            <button type="button" class="btn btn-green" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rDetail">
                <span class="glyphicon glyphicon-file"></span> Detail Report</button>
            @endif
            <button type="button" class="btn btn-green" data-target="#r_tax" data-toggle="modal" role="dialog" id="btn-rDetail">
                <span class="glyphicon glyphicon-file"></span> Tax Report</button>
        </div>
        <!-- <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Search</h4>
            </div>
            <div class="tableadd">
                <ul class="searchmenu">
                    <form method="GET" action="{{Route('showSalesOrder')}}">
                        <li><label for="typePayment">Payment Type</label>
                            <br>
                            <select name="typePayment" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Cash</option>
                                <option value="1">Credit</option>
                            </select>
                        </li>
                        <li><label for="typeTax">Tax Type</label>
                            <br>
                            <select name="typeTax" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Non Tax</option>
                                <option value="1">Tax</option>
                            </select>
                        </li>
                        <li><label for="date">Start Date</label>
                            <br><input id="startDate" name="startDate" type="text" autocomplete="off">
                        </li>
                        <li><label for="date">End Date</label>
                            <br><input id="endDate" name="endDate" type="text" autocomplete="off">
                        </li>
                        <li><br>
                            <button type="submit" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-search"></span> Search </button>
                        </li>
                        <li><br>
                            <button id="cancel" type="reset"  class="btn btn-pure btn-sm"><span class="glyphicon glyphicon-remove"></span> Cancel </button>
                        </li>
                    </form>
                </ul>
            </div><!---- end div tableadd---->
        <!--</div><!---- end div tabwrap---->
        @include('template.searchComponentTransactionModule')

        <form method="POST" action="">
            <div class="tabwrap">
                <div class="tabhead">
                    <h4 class="headtitle">Sales <span id="salesOrderID">{{SalesOrderHeader::getNextIDSalesOrder($sales)}}</span></h4>
                </div>

                <div class="alert alert-warning alert-dismissible none" id="alert_piutang" role="alert" style="width: 98%; margin: 1%">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Information!</strong> This customer's credit limit is <strong id="credit_limit"></strong>. Remaining debt is <strong id="hutang"></strong>.
                </div>

                <div class="tableadd">
                    <div class="headinv new">
                        <ul @if(checkModul('O05')) class="pull-left" style="width: 360px;" @else class="pull-left"  @endif>
                            <li>
                                <label for="date">Date *</label>
                                <input id="date" name="date" type="text" autocomplete="off" class="input" data-validation="required">
                            </li>
                            <li>
                                <label for="coa6">Customer *</label>
                                <select class="chosen-select" id="coa6" style="" name="coa6">
                                    @foreach(Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $coa6)
                                    <option value="{{$coa6->InternalID}}">
                                        {{$coa6->ACC6ID.' '.$coa6->ACC6Name}}
                                    </option>
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label for="payment">Payment *</label>
                                <input type="radio" class="radio-tipe" id="paymentCash" name="isCash" value="0" checked="checked">Cash
                                <input type="radio" class="radio-tipe" id="paymentCredit" name="isCash" value="1">Credit
                            </li>
                            <li class="credit">
                                <label for="longTerm">Long Term (day) *</label>
                                <input type="text" name="longTerm" value="0" id="longTerm" disabled="true">
                            </li>
                            <li class="cash">
                                <label for="slip">Slip number *</label>
                                <select class="chosen-select" id="slip" style="" name="slip">
                                    @foreach(Slip::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $slip)
                                    <option value="{{$slip->InternalID}}">
                                        {{$slip->SlipID.' '.$slip->SlipName}}
                                    </option>
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label for="purchasingCommission">Purchasing Commission *</label>
                                <input type="text" name="purchasingCommission" style="width: 200px;" value="0" id="purchasingCommission" class="nominal numajaDesimal" data-validation="required">
                            </li>
                            <li>
                                <label for="warehouse">Warehouse *</label>
                                <select class="chosen-select choosen-modal currency" id="warehouseHeader" name="warehouse">
                                    @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $war)
                                    <option id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                        {{$war->WarehouseName;}}
                                    </option>
                                    @endforeach
                                </select>
                            </li>
                        </ul>
                        <ul @if(checkModul('O05')) class="pull-left" style="width: 360px;" @else class="pull-right" @endif>
                             <li>
                                <label for="currency">Currency *</label>
                                <select class="chosen-select choosen-modal currency" id="currencyHeader" name="currency">
                                    {{'';$default = 0;}}
                                    @foreach(Currency::orderBy('Default','desc')->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $cur)
                                    <option id="cur{{$cur->InternalID}}" value="{{$cur->InternalID.'---;---'.$cur->CurrencyName.'---;---'.$cur->Rate.'---;---'.$cur->Default}}">
                                        {{$cur->CurrencyName;}}
                                        @if($default == 0)
                                        {{'';$default=$cur->Rate}}
                                        @endif
                                    </option>
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label for="rate">Rate *</label>
                                <input type="text" class="maxWidth input rate numajaDesimal" name="rate" maxlength="" id="rate" value="{{$default}}" data-validation="required">
                            </li>
                            <li>
                                <label for="vat">VAT *</label>
                                <input style="width:15px; height: 15px;" type="checkbox" name="vat" id="vat" value="1"> Tax 10%
                            </li>
                            <li>
                                <label for="Sales Man">Sales Man *</label>
                                <select class="chosen-select choosen-modal" name="salesman" id="salesManInternalID">
                                    <option value="0">No Sales</option>
                                    @foreach(SalesMan::where("CompanyInternalID", Auth::user()->Company->InternalID)->get() as $data)
                                    <option value="{{$data->InternalID}}">{{$data->SalesManName}}</option>
                                    @endforeach
                                </select>
                            </li>
                           <li>
                                <label for="remark">Remark *</label>
                                <textarea name="remark" id="remark" class="input" data-validation="required"></textarea>
                            </li>
                            @if(!checkModul('O05'))
                            <li>
                                <div class="required">
                                    * Required
                                </div>
                            </li>
                            @endif
                        </ul>

                        @if(checkModul('O05'))
                        <ul class="pull-left" style="width: 360px;">
                            <li>
                                <label for="transactiontype">Transaction</label>
                                <select class="chosen-select choosen-modal currency" id="transactionType" name="TransactionType">
                                    <option value="1"> For who is not collect PPN </option>
                                    <option value="2"> For Chamberlain </option>
                                    <option value="3"> Except Chamberlain </option>
                                    <option value="4"> DPP other value </option>
                                    <option value="6"> Other handover, include handover to foreigner tourist in the event of VAT refund </option>
                                    <option value="7"> Handover PPN is not collect</option>
                                    <option value="8"> Handover PPN Freed</option>
                                    <option value="9"> Handover Assets (Pasal 16D UU PPN)</option>
                                </select>
                            </li>
                            <li>
                                <label for="replacement">Replacement</label>
                                <input style="width:15px; height: 15px;" type="checkbox" name="Replacement" id="replacement" value="1"> Tax Replacement
                            </li>
                            <li>
                                <label for="numbertax">Tax Number</label>
                                <input type="hidden" name="TaxNumber" value="" id="numberTax">
                                <input type="text" class="numaja autoTab" style="width: 40px;" maxlength="3" id="numberTax1" > .
                                <input type="text" class="numaja autoTab" style="width: 40px;" maxlength="3" id="numberTax2" > -
                                <input type="text" class="numaja autoTab" style="width: 30px;" maxlength="2" id="numberTax3" > .
                                <input type="text" class="numaja autoTab" style="width: 75px;" maxlength="8" id="numberTax4" >
                            </li>
                            <li>
                                <label for="taxmonth">Tax Month</label>
                                <select class="chosen-select choosen-modal currency" id="taxMonth" name="TaxMonth">
                                    @for($aa = 1; $aa<=12; $aa++)
                                    <option value="{{$aa}}" >{{date('F',strtotime('2015-'.$aa.'-01'))}}</option>
                                    @endfor
                                </select>
                            </li>
                            <li>
                                <label for="taxyear">Tax Year</label>
                                {{''; $year = date('Y'); $mulai = 2012;}}
                                <select class="chosen-select choosen-modal currency" id="taxYear" name="TaxYear">
                                    @while($mulai <= $year)
                                    <option value="{{$year}}">{{$year}}</option>
                                    {{'';$year--;}}
                                    @endwhile
                                </select>
                            </li>
                            <li>
                                <div class="required">
                                    * Required
                                </div>
                            </li>
                        </ul>
                        @endif
                    </div>
                    <div class="tableadd journupdate">
                        <table class="table master-data" id="table-salesOrder" style="table-layout:fixed;">
                            <thead>
                                <tr>
                                    <th style="width: 25%;">Inventory</th>
                                    <th style="width: 10%;">Uom</th>
                                    <th style="width: 10%;">Qty</th>
                                    <th style="width: 15%;">Price</th>
                                    <th style="width: 10%;">Disc (%)</th>
                                    <th style="width: 10%;">Disc</th>
                                    <th style="width: 15%;">Subtotal</th>
                                    <th style="width: 5%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="row0" style="background-color: rgba(215, 244, 119, 0.29) !important">
                                    <td class="chosen-transaction" style="border-color: #d8d8d8 !important">
                                        <input class="input-theme margbot10" type="text" id="searchInventory" title="Type Inventory Name or ID then 'Enter'" placeholder="Type Inventory Name or ID then 'Enter'">
                                        <div id="selectInventory">

                                        </div>
                                    </td>
                                    <td style="border-color: #d8d8d8 !important">
                                        <select id="uom-0" class="input-theme uom">

                                        </select>
                                    </td>
                                    <td class="text-right" style="border-color: #d8d8d8 !important">
                                        <input type="text" class="maxWidth qty numaja right input-theme" maxlength="11" min="1" id="price-0-qty" value="1">
                                    </td>
                                    <td class="text-right" style="border-color: #d8d8d8 !important">
                                        <input type="text" class="maxWidth price right numajaDesimal input-theme" maxlength="" id="price-0" value="0.00">
                                    </td>
                                    <td class="text-right" style="border-color: #d8d8d8 !important">
                                        <input type="text" {{(checkMatrix("ST13") ) ? "" : "readonly" }} class="maxWidth discount right numajaDesimal input-theme" id="price-0-discount" value="0.00">
                                    </td>
                                    <td class="text-right" style="border-color: #d8d8d8 !important">
                                        <input type="text" {{(checkMatrix("ST13") ) ? "" : "readonly" }} class="maxWidth discountNominal right numajaDesimal input-theme" id="price-0-discountNominal" value="0.00">
                                    </td>
                                    <td id="price-0-qty-hitung" class="right" style="border-color: #d8d8d8 !important">
                                        0.00
                                    </td>
                                    <td style="border-color: #d8d8d8 !important">
                                        <button type="button" class="btn btn-green btn-sm" id="btn-addRow"><span class="glyphicon glyphicon-plus"></span></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <input type="hidden" name="grandTotalValue" value="0" id="grandTotalValue">
                        <table class="pull-right">
                            <tr>
                                <td><h5 class="right margr10 h5total"><b>Total</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b id="total"></b></h5></td>
                            </tr>
                            <tr >
                                <td><h5 class="right margr10 h5total"><b>Discount</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                                <td><h5 class="right margr10 h5total new" style="width: 150px;"><input type="text" {{(checkMatrix("ST13") ) ? "" : "readonly" }} style="width: 120px;" class="maxWidth right numajaDesimal discountGlobal" name="DiscountGlobal" maxlength="" id="discountGlobal" value="0.00"></h5></td>
                            </tr>
                            <tr>
                                <td><h5 class="right margr10 h5total"><b>Grand Total</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b id="grandTotal"></b></h5></td>
                            </tr>

                            <tr>
                                <td><h5 class="right margr10 h5total hidevat"><b>Tax</b></h5></td>
                                <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b id="tax"></b></h5></td>
                            </tr>
                            <tr>
                                <td><h5 class="right margr10 h5total hidevat"><b>Grand Total(tax)</b></h5></td>
                                <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b id="grandTotalAfterTax"></b></h5></td>
                            </tr>
                        </table>
                    </div><!---- end div tableadd---->
                </div><!---- end div tableadd---->
            </div><!---- end div tabwrap---->
            <div class="btnnest pull-right">
                @if (myCheckIsEmpty('Customer;Warehouse;Currency;Inventory'))
                <button class="btn btn-green btn-sm btn-save" type="button"> Save </button>
                @else
                <button class="btn btn-green btn-sm btn-save" id="btn-save"> Save </button>
                @endif
            </div>
        </form>
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop

@section('modal')
<div class="modal fade" id="insertSales" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insert Sales</h4>
            </div>
            <form action="" method="post" class="action" id="form-so">
                {{'';$hitung = 0;}}
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="insertSales" id="jenisSales" name="jenis">
                            <li>
                                <label for="sales">Sales Order ID</label> *
                            </li>
                            <input class="input-theme margbot10" type="text" id="searchSalesOrder" title="Type Sales Order Name or ID then 'Enter'" placeholder="Type Sales Order Name or ID then 'Enter'">
                            <li id="selectSalesOrder">

                            </li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btn-add-so" @if($hitung==0){{'disabled'}}@endif class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="r_summary" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Summary Report</h4>
            </div>
            <form action="" method="post" class="action" id="" target="_blank">
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='summarySales'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateReport" data-validation="required">
                            </li>
                        </ul>
                    </div>
                    <div class="required">
                        * Required
                    </div>
                </div>
                <div class="modal-footer">
                    <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                    <button id="btn-report-transaction" type="submit" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="r_tax" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Tax Report</h4>
            </div>
            <form action="" method="post" class="action" id="" target="_blank">
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='taxSales'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateTaxReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateTaxReport" data-validation="required">
                            </li>
                        </ul>
                    </div>
                    <div class="required">
                        * Required
                    </div>
                </div>
                <div class="modal-footer">
                    <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                    <button id="btn-report-transaction" type="submit" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
@if(checkMatrix("ST13"))
<script>
var statDiscount = "";
</script>
@else
<script>
    var statDiscount = "readonly";
</script>
@endif
<script>
    var getUomThisInventory = "<?php echo Route("getUomThisInventory") ?>";
    var getPriceRangeThisInventorySO = "<?php echo Route("getPriceRangeThisInventorySO") ?>";
    var getPriceThisParcel = "<?php echo Route("getPriceThisParcel") ?>";
    var checkRecieveable = '<?php echo Route('checkRecieveable') ?>';
    var getSearchResultInventoryForSO = "<?php echo Route("getSearchResultInventoryForSO") ?>";
    var getStockInventorySO = "<?php echo Route("getStockInventorySO") ?>";
    var getPriceRangeSO = "<?php echo Route("getPriceRangeSO") ?>";
    var textSelect = '';
    var savePrint = '<?php echo Route('savePrintSO'); ?>';
</script>
<script>
    var tanggalHariIni = '<?php echo date('d') . '-' . date('m') . '-' . date('Y') ?>';
    var cariS = '<?php echo Route('formatCariIDSales') ?>';
    var getResultSearchSO = "<?php echo Route("getResultSearchSO") ?>";
    var salesDataBackup = '<?php echo Route('salesDataBackup', Input::get('typePayment') . '---;---' . Input::get('typeTax') . '---;---' . Input::get('startDate') . '---;---' . Input::get('endDate')) ?>';
</script><script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-penjualan-add/salesCashNew.js')}}"></script>
@stop
