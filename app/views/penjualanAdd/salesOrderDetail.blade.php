@extends('template.header-footer')

@section('title')
Sales Order
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosenSearch.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosenCustom.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
@stop

@section('nav')

@stop

@section('content')
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showSalesOrder')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Sales Order</a>
                <a href="{{route('salesOrderDetail',$header->SalesOrderID)}}" type="button" class="btn btn-sm btn-pure">Detail {{$header->SalesOrderID}}</a>
            </div>
            <div class="btn-group margr5">
                <a href="{{Route('salesOrderNew')}}">
                    <button type="button" class="btn btn-green btn-sm dropdown-toggle" aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
                </a>
            </div>
            <div class="btn-group margr5">
                <button type="button" <?php if (myCheckIsEmpty('Quotation;Default')) echo 'disabled'; ?> class="btn btn-green btn-sm dropdown-toggle" data-target="#insertQuotation" data-toggle="modal" role="dialog"
                        aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New from Quotation </button>
            </div>
            <button id="search-button"  <?php if (myCheckIsEmpty('SalesOrder')) echo 'disabled'; ?> class="btn btn-green btn-sm margr5"><span class="glyphicon glyphicon-search"></span> Search </button>
            @if(checkModul('O04'))
            <div class="btn-group">
                <button type="button" class="btn btn-green dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="glyphicon glyphicon-file"></span> Report & Print <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a data-target="#r_summary" data-toggle="modal" role="dialog" href="#" id="btn-rDetail">Detail Report</a></li>
                    <li><a data-target="#r_summary" data-toggle="modal" role="dialog" href="#" id="btn-rSummary">Summary Report</a></li>
                    <li><a target="_blank" href="{{Route("uncomplateSalesOrder")}}">Uncomplate Sales Order Report</a></li>
                    <li>
                                                <a href="{{Route('salesOrderPrint',$header->SalesOrderID)}}" id="btn-{{$header->SalesOrderID}}-print" target='_blank' style="margin-right: 0px !important;"> Print</a>
<!--                        <a href="#" data-target="#r_print" data-internal="{{$header->SalesOrderID }}"  data-toggle="modal" role="dialog"
                           onclick="printAttach(this)" data-id="{{$header->SalesOrderID}}" data-name="{{$header->SalesOrderID}}">
                            Print
                        </a>-->
                    </li>
                    <!--<li><a href="{{Route('salesOrderPrintSJ',$header->SalesOrderID)}}" id="btn-{{$header->SalesOrderID}}-print" target='_blank' style="margin-right: 0px !important;">Print SJ</a></li>-->
                    <!--<li><a href="{{Route('salesOrderInternalPrint',$header->SalesOrderID)}}" id="btn-{{$header->SalesOrderID}}-print" target='_blank' style="margin-right: 0px !important;">Internal</a></li>-->
                </ul>
            </div>
            @endif
            @if(!SalesOrderHeader::isInvoice($header->InternalID) && !SalesOrderHeader::isShipping($header->InternalID) && $header->Closed == 0)
            <a href="{{Route('salesOrderUpdate',$header->SalesOrderID)}}">
                <button id="btn-{{$header->SalesOrderID}}-update"
                        class="btn btn-green btn-sm margr5">
                    <span class="glyphicon glyphicon-edit"></span> Edit
                </button>
            </a>
            @else
            <button disabled class="btn btn-green btn-sm margr5"><span class="glyphicon glyphicon-edit"></span> Edit</button>
            @endif
            @if (checkShippingAdd($header->InternalID) && $header->Closed == 0 && $header->Status == 1)
            @if ((checkAmountSales($header->InternalID) <= 0 && $header->isCash == 2) || ($header->isCash == 4 && checkDpShipping($header->InternalID)) || ($header->isCash != 2 && $header->isCash != 4))
            <a href="{{Route('shippingNew',$header->SalesOrderID)}}" id="btn-{{$header->SalesOrderID}}-print" target='_blank' style="margin-right: 0px !important;">
                <button type="button" class="btn btn-green">
                    <span class="glyphicon glyphicon-plus"></span> Create Shipping </span>
                </button>
            </a>
            @endif
            @endif
            @if (checkSalesAdd($header->InternalID) && $header->Closed == 0&& $header->Status == 1)
            @if ((checkAmountSales($header->InternalID) <= 0 && $header->isCash == 2) || ($header->isCash == 4 && checkDpShipping($header->InternalID)) || ($header->isCash != 2 && $header->isCash != 4))
            <a href="{{Route('salesNew',$header->SalesOrderID)}}" id="btn-{{$header->SalesOrderID}}-print" target='_blank' style="margin-right: 0px !important;">
                <button type="button" class="btn btn-green">
                    <span class="glyphicon glyphicon-plus"></span> Create Sales </span>
                </button>
            </a>
            @endif
            @endif
            @if( $header->Closed == 0&& $header->Status == 1)
            <a href="{{asset('/')}}transferNew?jenis=insertTransfer&SalesOrder={{$header->SalesOrderID}}" id="btn-{{$header->SalesOrderID}}-print" target='_blank' style="margin-right: 0px !important;">
                <button type="button" class="btn btn-green">
                    <span class="glyphicon glyphicon-plus"></span> Create Transfer </span>
                </button>
            </a>
            @endif
            @if(checkModul('O04'))
            <div class="btn-group">
                <!--                <button type="button" class="btn btn-green dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="glyphicon  glyphicon-print"></span> Print <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{Route('salesOrderPrint',$header->SalesOrderID)}}" id="btn-{{$header->SalesOrderID}}-print" target='_blank' style="margin-right: 0px !important;">
                                            Print
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{Route('salesOrderPrintStruk',$header->SalesOrderID)}}" id="btn-{{$header->SalesOrderID}}-print" target='_blank' style="margin-right: 0px !important;">
                                            Print Struk
                                        </a>
                                    </li>
                                </ul>-->


            </div>
            @endif
        </div>
        <!-- <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Search</h4>
            </div>
            <div class="tableadd">
                <ul class="searchmenu">
                    <form method="GET" action="{{Route('showSalesOrder')}}">
                        <li>
                            <label for="coa6">Customer</label>
                            <br>
                            <select class="chosen-select" id="coa6" style="" name="coa6">
                                <option value="-1">All Customer</option>
                                @foreach(Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $coa6)
                                <option value="{{$coa6->InternalID}}">
                                    {{$coa6->ACC6ID.' '.$coa6->ACC6Name}}
                                </option>
                                @endforeach
                            </select>
                        </li>
                        <li><label for="typePayment">Payment Type</label>
                            <br>
                            <select name="typePayment" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Cash</option>
                                <option value="1">Credit</option>
                            </select>
                        </li>
                        <li><label for="typeTax">Tax Type</label>
                            <br>
                            <select name="typeTax" style="width: 70px">
                                <option value="-1">All</option>
                                <option value="0">Non Tax</option>
                                <option value="1">Tax</option>
                            </select>
                        </li>
                        <li><label for="date">Start Date</label>
                            <br><input id="startDate" name="startDate" type="text" autocomplete="off">
                        </li>
                        <li><label for="date">End Date</label>
                            <br><input id="endDate" name="endDate" type="text" autocomplete="off">
                        </li>
                        <li><br>
                            <button type="submit" class="btn btn-green btn-sm">Search <span class="glyphicon glyphicon-search"></span></button>
                        </li>
                        <li><br>
                            <button id="cancel" type="reset"  class="btn btn-pure btn-sm">Cancel <span class="glyphicon glyphicon-remove"></span></button>
                        </li>
                    </form>
                </ul>
            </div><!---- end div tableadd---->
        <!--</div><!---- end div tabwrap---->
        @include('template.searchComponentTransactionModule')

        <div class="tabwrap">
            <div class="tabhead">
                <h4 class="headtitle">{{'Sales Order '.$header->SalesOrderID}}</h4>
            </div>
            <div class="tableadd">
                <div class="headinv new">
                    <ul class="pull-left">
                        @if($id != NULL)
                        <li>
                            <label for="quotationid" >Quotation ID</label>
                            <span id="quotationIDText">{{substr(str_replace(',',' ',$id),12)}}
                                @if(strlen($id)>12)
                                ...
                                @endif
                            </span>
                            <button type="button" data-toggle="popover" data-content="{{str_replace(',',' ',$id)}}" data-placement="bottom" id="btn-info-purchaseID" class="btn btn-xs btn-pure"><span class="glyphicon glyphicon-question-sign"></span></button>
                        </li>
                        @endif
                        <li>
                            <label for="date">Date</label>
                            <span>{{date( "d-m-Y", strtotime($header->SalesOrderDate))}}</span>
                        </li>
                        <li>
                            <label for="customer">Customer</label>
                            <span><?php
                                $coa6 = SalesOrderHeader::find($header->InternalID)->coa6;
                                echo $coa6->ACC6ID . ' ' . $coa6->ACC6Name
                                ?></span>
                        </li>
                        <li>
                            <label for="payment">Payment</label>
                            @if($header->isCash == 0)
                            <span>{{'Cash'}}</span>
                            @elseif($header->isCash == 1)
                            <span>{{'Credit'}}</span>
                            @elseif($header->isCash == 2)
                            <span>{{'CBD'}}</span
                            @elseif($header->isCash == 3)
                            <span>{{'Deposit'}}</span>
                            @else
                            <span>{{'Down Payment'}}</span>
                            @endif
                        </li>
                        @if($header->isCash == 4)
                        <li>
                            <label for="longTerm">Down Payment</label>
                            @if($header->DownPayment > 0)
                            <span>{{number_format($header->DownPayment,'2','.',',')}}</span>
                            @else
                            <span>{{number_format($header->DPValue,'2','.',',')}}</span>
                            @endif
                        </li>
                        @endif
                        @if($header->isCash != 0 && $header->isCash != 2 && $header->isCash != 3)
                        <li>
                            <label for="longTerm">Due Date</label>
                            <span>{{date( "d-m-Y", strtotime("+".$header->LongTerm." day",strtotime($header->SalesOrderDate)))}}</span>
                        </li>
                        @endif
                        <li>
                            <label for='pickupType'>Pickup Type</label>
                            <span>
                                @if ($header->PickupType == 0)
                                Customer Pickup
                                @else
                                Shipping
                                @endif
                            </span>
                        </li>
                        <li>
                            <label for="currency">Salesman</label>
                            <span>
                                {{SalesMan::find($header->SalesManInternalID)->SalesManName}}
                            </span>
                        </li>
                        <li>
                            <label for="rate">Purchasing Commission</label>
                            <span>{{number_format($header->PurchaseCommission,'2','.',',')}}</span>
                        </li>
                    </ul>
                    <ul class="pull-right">
                        <li>
                            <label for="currency">Currency</label>
                            <span>{{$header->Currency->CurrencyName}}</span>
                        </li>
                        <li>
                            <label for="rate">Rate</label>
                            <span>{{number_format($header->CurrencyRate,'2','.',',')}}</span>
                        </li>
                        <!--                        <li>
                                                    <label for="VAT">VAT</label>
                                                    @if($header->VAT == 0)
                                                    <span>{{'Non Tax'}}</span>
                                                    @else
                                                    <span>{{'Tax'}}</span>
                                                    @endif
                                                </li>-->
                        <li>
                            <label for="">Remark</label>
                            <span>{{$header->Remark}}</span>
                        </li>
                        <li>
                            <label for="">PO Customer</label>
                            <span>{{$header->POCustomer}}</span>
                        </li>
                        <li>
                            <label for="">Delivery Terms</label>
                            <span>{{$header->DeliveryTerms}}</span>
                        </li>
                        <li>
                            <label for="">Delivery Time</label>
                            <span>{{$header->DeliveryTime}}</span>
                        </li>
                    </ul>
                </div>
                <div class="padrl10">
                    <table class="table master-data" id="table-quotation-description" style="table-layout:fixed;">
                        <thead>
                            <tr>
                                <th style="width: 25%;">Inventory</th>
                                <th style="width: 10%;">Uom</th>
                                <th style="width: 10%;">Qty</th>
                                <th style="width: 15%;">Price</th>
                                <th style="width: 10%;">Disc (%)</th>
                                <th style="width: 10%;">Disc</th>
                                <th style="width: 15%;">Subtotal</th>
                                <th style="width: 14%;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $total = 0;
                            $totalVAT = 0;
                            $barisDesc = 1;
                            ?>
                            @foreach(SalesOrderDescription::where('SalesOrderInternalID',$header->InternalID)->get() as $description)
                            <tr style='background: #F1FFDB;' id='rowDescription{{$barisDesc}}'>
                                <td class='chosen-uom'>
                                    {{$description->InventoryText}}
                                </td>
                                <td>
                                    {{$description->UomText}}
                                </td>
                                <td class='text-right'>
                                    {{$description->Qty}}
                                </td>
                                <td class='text-right'>
                                    {{number_format($description->Price,'2','.',',')}}
                                </td>
                                <td class='text-right'>
                                    {{$description->Discount}}
                                </td>
                                <td class='text-right'>
                                    {{number_format($description->DiscountNominal,'2','.',',')}}
                                </td>
                                <td class="right subtotalDescription" id="priceDescription-{{$barisDesc}}-qty-hitung">{{number_format(ceil($description->SubTotal),'0','.',',')}}</td>
                                <td>
                                    <button class="btn btn-pure-xs btn-xs btn-toggle-detail margr5" type="button" data-toshow="show" data="rowDescription{{$barisDesc}}"><span class="glyphicon glyphicon-eye-open"></span></button>
                                    <!--<button class="btn btn-pure-xs btn-xs btn-toggle-spec" type="button" data-toshow="show" data="rowSpec{{$barisDesc}}"><span class="glyphicon glyphicon-comment"></span></button>-->
                                </td>
                            </tr>
<!--                            <tr id='rowSpec{{$barisDesc}}'>
                                <td colspan='8' class='rowSpec{{$barisDesc}}' style="text-align: left">
                                    {{nl2br($description->Spesifikasi)}}
                                </td>
                            </tr>-->
                            <?php
                            $barisDetail = 1;
                            $total += $description->SubTotal;
                            $totalVAT += ceil($description->SubTotal) / 10;
                            ?>
                            @foreach(SalesOrderDetail::where('DescriptionInternalID',$description->InternalID)->get() as $detail)
                            <!--untuk non-parcel-->
                            @if($detail->SalesOrderParcelInternalID == 0)
                            <tr id='row{{$barisDetail}}' class='rowDescription{{$barisDesc}}'>
                                <td class='chosen-uom'>
                                    <input type="hidden" class="inventory" style="width: 100px" id="inventory-{{$barisDetail}}" name="inventory{{$barisDesc}}[]" value="{{$detail->InventoryInternalID}}---;---inventory">{{Inventory::find($detail->InventoryInternalID)->InventoryID.' '.Inventory::find($detail->InventoryInternalID)->InventoryName}}
                                </td>
                                <td>
                                    {{Uom::find($detail->UomInternalID)->UomID}}
                                </td>
                                <td class='text-right'>
                                    {{number_format($detail->Qty,'0','.',',')}}
                                </td>
                                <td class='text-right'>
                                    {{number_format($detail->Price,'2','.',',')}}
                                </td>
                                <td class='text-right'>
                                    {{$detail->Discount}}
                                </td>
                                <td class='text-right'>
                                    {{number_format($detail->DiscountNominal,'2','.',',')}}
                                </td>
                                <td class="right subtotal" id="price-{{$barisDetail}}-qty-hitung">{{number_format(ceil($detail->SubTotal),'0','.',',')}}</td>
                                <td>
                                    -
                                </td>
                                {{'';//$totalVAT += $detail->VAT}}
                            </tr>
                            <?php
                            $barisDetail++;
//                            $total += $detail->SubTotal;
                            ?>
                            @endif
                            @endforeach
                            <!--untuk parcel-->
                            @foreach(SalesOrderParcel::where('DescriptionInternalID',$description->InternalID)->get() as $parcel)
                            <tr id='row{{$barisDetail}}' class='rowDescription{{$barisDesc}}'>
                                <td class='chosen-uom'>
                                    {{Parcel::find($parcel->ParcelInternalID)->ParcelID.' '.Parcel::find($parcel->ParcelInternalID)->ParcelName}}
                                </td>
                                <td>
                                    -
                                </td>
                                <td class='text-right'>
                                    {{number_format($parcel->Qty,'0','.',',')}}
                                </td>
                                <td>
                                    {{number_format($parcel->Price,'2','.',',')}}
                                </td>
                                <td class='text-right'>
                                    {{$parcel->Discount}}
                                </td>
                                <td class='text-right'>
                                    {{number_format($parcel->DiscountNominal,'2','.',',')}}
                                </td>
                                <td class="right subtotal" id="price-{{$barisDetail}}-qty-hitung">{{number_format($parcel->SubTotal,'0','.',',')}}</td>
                                <td>
                                    -
                                </td>
                                {{'';//$totalVAT += $parcel->VAT}}
                            </tr>
                            <?php
                            $barisDetail++;
//                            $total += $parcel->SubTotal;
                            ?>
                            @endforeach
                            <?php $barisDesc++; ?>
                            @endforeach
                            @if($totalVAT != 0)
                            {{'';$totalVAT = $totalVAT - $header->DiscountGlobal*0.1;}}
                            @endif

                        </tbody>
                    </table>

                    <table class="pull-left">
                        <tr>
                            <td></td>
                        </tr>
                        <tr>
                            <td><p>Created by {{User::where('UserID', $header->UserRecord)->first()->UserName.' '.date('d-m-Y H:i:s', strtotime($header->dtRecord))}}</p></td>
                        </tr>
                        <tr>
                            @if($header->UserModified != '0')
                            <td><p>Modified by {{User::where('UserID', $header->UserModified)->first()->UserName.' '.date('d-m-Y H:i:s', strtotime($header->dtModified))}}</p></td>
                            @else
                            <td><p>Modified by -</p></td>
                            @endif
                        </tr>
                    </table>
                    @if(count($detail) > 0)

                    <table class="pull-right">
                        <tr>
                            <td><h5 class="right margr10 h5total"><b>Total</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="total">{{number_format(ceil($total),'0','.',',')}}</b></h5></td>
                        </tr>
                        <tr >
                            <td><h5 class="right margr10 h5total"><b>Discount</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total new"><b>{{number_format($header->DiscountGlobal, '0', '.',',')}}</b></h5></td>
                        </tr>
                        <tr>
                            <td><h5 class="right margr10 h5total"><b>Grand Total</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="grandTotal">{{number_format(ceil($total-$header->DiscountGlobal),'0','.',',')}}</b></h5></td>
                        </tr>

                        <tr>
                            <td><h5 class="right margr10 h5total hidevat"><b>Tax</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b id="tax">{{number_format(floor($totalVAT),'0','.',',')}}</b></h5></td>
                        </tr>
                        <tr>
                            <td><h5 class="right margr10 h5total hidevat"><b>Grand Total(tax)</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b id="grandTotalAfterTax">{{number_format(ceil($header->GrandTotal),'0','.',',')}}</b></h5></td>
                        </tr>
                    </table>

                    @endif
                </div><!---- end div padrl10---->
            </div><!---- end div tableadd---->
        </div><!---- end div tabwrap---->
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->

@stop

@section('modal')
<div class="modal fade modal-quotation" id="insertQuotation" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insert Quotation</h4>
            </div>
            <form action="{{Route("salesOrderNew")}}" method="GET" class="action">
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="insertQuotation" id="jenisQuotation" name="jenis">
                            <li>
                                <label for="customerSales">Customer</label> *
                            </li>
                            <li>
                                <select class="chosen-select choosen-modal input-theme" id="customerSales" style="" name="customerSales">
                                    @foreach(QuotationHeader::where("VAT","!=",Auth::user()->SeeNPPN)->distinct()->select('CustomerName')->groupBy('CustomerName')->get() as $cus)
                                    <option value="{{$cus->CustomerName}}">
                                        {{$cus->CustomerName}}
                                    </option>
                                    @endforeach
                                    @foreach(Coa6::where('CompanyInternalID', Auth::user()->Company->InternalID)->where('Type', 'C')->get() as $coa6)
                                    <option value="{{$coa6->InternalID}}">
                                        {{$coa6->ACC6ID.' | '.$coa6->ACC6Name}}
                                    </option>
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label for="sales">Quotation ID</label> *
                            </li>
                            <li>
                                <select multiple class="chosen-select choosen-modal input-theme input-stretch" id="quotation" style="" name="Quotation[]">
                                    {{'';$hitung=0;}}
                                    @foreach(QuotationHeader::where("VAT","!=",Auth::user()->SeeNPPN)->where('CompanyInternalID', Auth::user()->Company->InternalID)->OrderBy('QuotationDate', 'desc')->get() as $quotation)
                                    @if(checkQuotation($quotation->InternalID))
                                    @if($quotation->TypeCustomer == 0)
                                    <option class="optionQuotation" id="cust{{$quotation->ACC6InternalID}}" value="{{$quotation->QuotationID}}">
                                        @else
                                    <option class="optionQuotation" id="cust{{$quotation->CustomerName}}" value="{{$quotation->QuotationID}}">
                                        @endif
                                        {{$quotation->QuotationID.' | '.date( "d-m-Y", strtotime($quotation->QuotationDate))}}
                                    </option>
                                    {{'';$hitung=1;}}
                                    @endif
                                    @endforeach
                                </select>
                            </li>
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="btn-submit-quotation" @if($hitung==0){{'disabled'}}@endif class="btn btn-green">Submit</button>
                        <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="r_summary" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Summary Report</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="" target="_blank">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='summarySalesOrder'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateReport" data-validation="required">
                            </li>
                        </ul>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="r_print" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Tax</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="" target="_blank">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='printSalesOrder'>
                            <input type="hidden" name="internalID" id="internalID" />
                            <li>
                                <label for="type">Type Tax</label> *
                            </li>
                            <li>
                                <select class="form-control" name="type">
                                    <option value="include">Include</option>
                                    <option value="exclude">Exclude</option>
                                </select>
                            </li>
                        </ul>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script><script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script>
                               var salesOrderDataBackup = '<?php echo Route('salesOrderDataBackup', Input::get('coa6') . '---;---' . Input::get('typePayment') . '---;---' . Input::get('typeTax') . '---;---' . Input::get('startDate') . '---;---' . Input::get('endDate')) ?>';
                               $(function () {
                                   $('[data-toggle="popover"]').popover();
                               });
                               $(document).ready(function () {
                                   var ppn = '<?php echo $header->VAT ?>'
                                   if (ppn == 1) {
                                       $('.hidevat').show();
                                   } else {
                                       $('.hidevat').hide();
                                   }
                               });
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-penjualan-add/salesOrder.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/tooltip.info.js')}}"></script>
@stop
