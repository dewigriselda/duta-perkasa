@extends('template.header-footer')

@section('title')
Sales Order
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosenSearch.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<style>
    .help-block {
        margin-left: 30% !important;
    }
</style>
@stop

@section('nav')

@stop

@section('content')
@if(myCheckIsEmpty('Customer;Warehouse;Currency;Inventory'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one master Customer, Currency, Warehouse, and Inventory to insert sales order.
</div>
@endif
@if(isset($messages))
@if($messages == 'gagalInsert')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New sales order has been inserted.
</div>

<input type="hidden" name="print" id="print" value="{{$print}}">
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showSalesOrder')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Sales Order</a>
                <a href="{{route('salesOrderNew')}}" type="button" class="btn btn-sm btn-pure">New Sales Order</a>
            </div>
            <div class="btn-group margr5">
                @if (myCheckIsEmpty('Customer;Warehouse;Currency;Inventory'))
                <button type="button" class="btn btn-green btn-sm dropdown-toggle  " aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
                @else
                <a href="{{Route('salesOrderNew')}}">
                    <button type="button" class="btn btn-green btn-sm dropdown-toggle  " aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
                </a>
                @endif
            </div>
            <div class="btn-group margr5">
                <button type="button" <?php if (myCheckIsEmpty('Quotation;Default')) echo 'disabled'; ?> class="btn btn-green btn-sm dropdown-toggle" data-target="#insertQuotation" data-toggle="modal" role="dialog"
                        aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New from Quotation </button>
            </div>
            <button id="search-button" <?php if (myCheckIsEmpty('SalesOrder')) echo 'disabled'; ?> class="btn btn-green btn-sm margr5"><span class="glyphicon glyphicon-search"></span> Search </button>
            @if(checkModul('O04'))
            <div class="btn-group">
                <button type="button" class="btn btn-green dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="glyphicon glyphicon-file"></span> Report <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a data-target="#r_summary" data-toggle="modal" role="dialog" href="#" id="btn-rDetail">Detail Report</a></li>
                    <li><a data-target="#r_summary" data-toggle="modal" role="dialog" href="#" id="btn-rSummary">Summary Report</a></li>
                    <li><a target="_blank" href="{{Route("uncomplateSalesOrder")}}">Uncomplate Sales Order Report</a></li>
                </ul>
            </div>
            @endif
        </div>
        <!-- <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Search</h4>
            </div>
            <div class="tableadd">
                <ul class="searchmenu">
                    <form method="GET" action="{{Route('showSalesOrder')}}">
                        <li>
                            <label for="coa6">Customer *</label>
                            <br>
                            <select class="chosen-select" id="coa6" style="" name="coa6">
                                <option value="-1">All Customer</option>
                                @foreach(Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $coa6)
                                <option value="{{$coa6->InternalID}}">
                                    {{$coa6->ACC6ID.' '.$coa6->ACC6Name}}
                                </option>
                                @endforeach
                            </select>
                        </li>
                        <li><label for="typePayment">Payment Type</label>
                            <br>
                            <select name="typePayment" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Cash</option>
                                <option value="1">Credit</option>
                            </select>
                        </li>
                        <li><label for="typeTax">Tax Type</label>
                            <br>
                            <select name="typeTax" style="width: 70px">
                                <option value="-1">All</option>
                                <option value="0">Non Tax</option>
                                <option value="1">Tax</option>
                            </select>
                        </li>
                        <li><label for="date">Start Date</label>
                            <br><input id="startDate" name="startDate" type="text" autocomplete="off">
                        </li>
                        <li><label for="date">End Date</label>
                            <br><input id="endDate" name="endDate" type="text" autocomplete="off">
                        </li>
                        <li><br>
                            <button type="submit" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-search"></span> Search </button>
                        </li>
                        <li><br>
                            <button id="cancel" type="reset"  class="btn btn-pure btn-sm"><span class="glyphicon glyphicon-remove"></span> Cancel </button>
                        </li>
                    </form>
                </ul>
            </div><!---- end div tableadd---->
        <!--</div><!---- end div tabwrap---->
        @include('template.searchComponentTransactionModule')

        <form id="form-insert" method="POST" action="">
            <div class="tabwrap">
                <div class="tabhead">
                    <h4 class="headtitle">Sales Order <span id="salesOrderID">{{SalesOrderHeader::getNextIDSalesOrder($salesorder)}}</span></h4>
                </div>
                <div class="tableadd">
                    <div class="headinv new">
                        <ul class="pull-left">
                            <li>
                                <label for="date">Date *</label>
                                <input id="date" name="date" type="text" autocomplete="off" data-validation="required">
                            </li>
                            <li>

                                <label for="coa6">Customer *</label>
                                @if(isset($customer) && $customer != "0")
                                @if($type == "lama")
                                {{'';
                                    $coa6 = Coa6::where('Type','c')->where("InternalID", "=",$customer)->orWhere("ACC6Name", "=",$customer)->where('Type','c')->where('InternalID','!=',"0")->first();
//                                    $coa6 = Coa6::find($customer);
                                }}
                                <select class="chosen-select" id="coa6" style="" disabled>
                                    <option value="{{$coa6->InternalID}}">
                                        {{$coa6->ACC6ID.' '.$coa6->ACC6Name}}
                                    </option>
                                </select>
                                <input type="hidden" name="coa6" id="coa6_input" value="{{$coa6->InternalID}}">
                                @else
                                <button type="button" class="btn btn-green btn-insert" data-target="#m_coa" data-toggle="modal" role="dialog">
                                    <span class="glyphicon glyphicon-plus"></span> New</button>
                                @endif
                                @else
                                <select class="chosen-select" id="coa6_input" style="" name="coa6" {{(isset($customer) && $customer != 0 ) ? "disabled" : ""}}>
                                    @foreach(Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $coa6)
                                    <option value="{{$coa6->InternalID}}">
                                        {{$coa6->ACC6ID.' '.$coa6->ACC6Name}}
                                    </option>
                                    @endforeach
                                </select>
                                @endif
                            </li>
                            <li id='customermanagerdiv'>

                            </li>
                            <li>
                                <label for="">Sales Assistant</label>
                                <span>{{Auth::user()->UserName}}</span>
                            </li>
                            <!--                            <li>
                                                            <label for="payment">Payment *</label>
                                                            @if(isset($quotation))
                                                            <input type="radio" class="radio-tipe" id="paymentCash" name="isCash" value="0" @if($quotation->isCash==0)checked @endif)>Cash
                                                                   <input type="radio" class="radio-tipe" id="paymentCredit" name="isCash" value="1" @if($quotation->isCash==1)checked @endif>Credit
                                                                   @else
                                                                   <input type="radio" class="radio-tipe" id="paymentCash" name="isCash" checked value="0">Cash
                                                            <input type="radio" class="radio-tipe" id="paymentCredit" name="isCash" value="1">Credit
                                                            @endif
                                                        </li>-->
                            <li>
                                <label for="payment">Payment *</label>
                                <select name="isCash" id="payment">
                                    <option value="0" selected>Cash</option>
                                    <option value="1">Credit</option>
                                    <option value="2">CBD</option>
                                    <option value="3">Deposit</option>
                                    <option value="4">Down Payment</option>
                                </select>
                            </li>
                            <li class="downpayment">
                                <label for="longTerm">Down Payment Type</label>
                                <select name="jenisDP" id="jenisdp">
                                    <option value="0" selected>Percentage</option>
                                    <option value="1">Nominal</option>
                                </select>
                            </li>
                            <li class="downpayment persen">
                                <label for="longTerm">Down Payment (%)*</label>
                                <input type="text" name="downPayment" value="0" id="" class="numajaDesimal price2">
                            </li>
                            <li class="downpayment dpnominal">
                                <label for="longTerm">Down Payment *</label>
                                <input type="text" name="downPaymentValue" value="0" id="downPayment" class="numajaDesimal price2">
                            </li>
                            <li>
                                <label for="longTerm">Long Term (day) *</label>
                                @if(isset($quotation) && $quotation->isCash==1)
                                <input type="text" class="numaja" name="longTerm" value="{{$quotation->LongTerm}}" id="longTerm">
                                @else
                                <input type="text" class="numaja" name="longTerm" value="0" id="longTerm"  disabled="true">
                                @endif
                            </li>
                            <li>
                                <label for="payment">Pickup Type *</label>
                                <input checked type="radio" class="radio-tipe" id="pickupShipping" name="isShipping" value="1">Shipping
                                <input type="radio" class="radio-tipe" id="pickupCustomer" name="isShipping" value="0">Customer PickUp
                            </li>
                            <!--                            <li>
                                                            <label for="warehouse">Warehouse *</label>
                                                            <select class="chosen-select choosen-modal currency" id="warehouseHeader" name="warehouse">
                                                                @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $war)
                                                                <option id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                                                    {{$war->WarehouseName;}}
                                                                </option>
                                                                @endforeach
                                                            </select>
                                                        </li>-->
                            <!--                            <li>
                                                            <label for="salesman">Salesman *</label>
                                                            <select class="chosen-select" id="salesman" style="" name="salesman" required>
                                                                @foreach(SalesMan::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $salesman)
                                                                <option value="{{$salesman->InternalID}}">
                                                                    {{$salesman->SalesManName}}
                                                                </option>
                                                                @endforeach
                                                            </select>
                                                        </li>-->
                        </ul>
                        <ul class="pull-right">
                            <li>
                                <label for="purchasingCommission">Purchasing Commission *</label>
                                <input type="text" name="purchasingCommission"style="" value="0" id="purchasingCommission" class="nominal numajaDesimal" data-validation="required">
                            </li>
                            <li>
                                <label for="currency">Currency *</label>
                                <select class="chosen-select choosen-modal currency" id="currencyHeader" name="currency">
                                    {{'';$default = 0;}}
                                    @foreach(Currency::orderBy('Default','desc')->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $cur)
                                    <option id="cur{{$cur->InternalID}}" value="{{$cur->InternalID.'---;---'.$cur->CurrencyName.'---;---'.$cur->Rate.'---;---'.$cur->Default}}">
                                        {{$cur->CurrencyName;}}
                                        @if($default == 0)
                                        {{'';$default=$cur->Rate}}
                                        @endif
                                    </option>
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label for="rate">Rate *</label>
                                <input type="text" class="maxWidth rate numajaDesimal" name="rate" maxlength="" id="rate" value="{{$default}}" data-validation="required">
                            </li>

                            <li id="type">
                                <label for="type">Price Type *</label>
                                <select name="typeTax" id="typeTax">
                                    <option value="Exclude">Exclude</option>
                                    <option value="Include">Include</option>
                                </select>
                            </li>
                            <li>
                                <label for="remark">Remark</label>
                                @if(isset($quotation))
                                {{''; $idquot = QuotationHeader::where('QuotationID',$arrquotation[0])->first()->InternalID}}
                                <textarea name="remark" id="remark">{{$quotation->Remark}}</textarea>
                                @else
                                <textarea name="remark" id="remark"></textarea>
                                @endif
                            </li>
                            <li>
                                <label for="longTerm">PO Customer *</label>
                                <input type="text" name="POCustomer" data-validation="required">
                            </li>
                            <li>
                                <label for="longTerm">Delivery Terms</label>
                                @if(isset($quotation))
                                <input type="text" name="DeliveryTerms" value="{{$quotation->DeliveryTerms}}">
                                @else
                                <input type="text" name="DeliveryTerms">
                                @endif
                            </li>
                            <li>
                                <label for="longTerm">Delivery Time</label>
                                @if(isset($quotation))
                                <input type="text" name="DeliveryTime" value="{{$quotation->DeliveryTime}}">
                                @else
                                <input type="text" name="DeliveryTime">
                                @endif
                            </li>
                            <li>
                                <div class="required">
                                    * Required
                                </div>
                            </li>
                            <li style="visibility: hidden">
                                <label for="vat">VAT *</label>
                                @if(isset($quotation))
                                {{'';$quot = 'yes'}}
                                @if($quotation->VAT == 1)
                                <input checked style="width:15px; height: 15px;" type="checkbox" name="vat" id="vat" value="1"> Tax 10%
                                @else
                                <input style="width:15px; height: 15px;" type="checkbox" name="vat" id="vat" value="1"> Tax 10%
                                @endif
                                @else
                                <input @if(Auth::user()->SeeNPPN == 0)checked @endif style="width:15px; height: 15px;" type="checkbox" name="vat" id="vat" value="1"> Tax 10%
                                        @endif
                                        <!--<input style="width:15px; height: 15px;" type="checkbox" name="vat" id="vat" value="1"> Tax 10%-->
                            </li>
                        </ul>
                    </div>
                    <div class="tableadd journupdate">
                        <label>Description</label>
                        <table class="table master-data table-striped" id="table-salesorder-description-header" style="table-layout:fixed;">
                            <thead class="default-thead">
                                <tr>
                                    <th style="width: 25%;">Inventory</th>
                                    <th style="width: 10%;">Uom</th>
                                    <th style="width: 10%;">Qty</th>
                                    <th style="width: 15%;">Price</th>
                                    <th style="width: 10%;">Disc (%)</th>
                                    <th style="width: 10%;">Disc</th>
                                    <th style="width: 15%;">Subtotal</th>
                                    <th class="text-center" style="width: 5%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="row0">
                                    <td class="chosen-transaction">
                                        <input class="input-theme inventorydesc" type="text" id="InventoryDescription-0" placeholder="Inventory Name">
                                    </td>
                                    <td>
                                        <input type="text" class="input-theme uomDescription" id="uomDescription-0" placeholder="uom">
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth qtyDescription right numaja input-theme" maxlength="11" min="1" id="priceDescription-0-qty" value="1">
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth priceDescription right numajaDesimal" maxlength="" id="priceDescription-0" value="0">
                                    </td>
                                    <td class="text-right">
                                        <input disabled type="text" class="maxWidth discountDescription right numajaDesimal input-theme" id="priceDescription-0-discount" value="0.00">
                                    </td>
                                    <td class="text-right">
                                        <input disabled type="text" class="maxWidth discountNominalDescription right numajaDesimal input-theme" id="priceDescription-0-discountNominal" value="0.00">
                                    </td>
                                    <td id="priceDescription-0-qty-hitung" class="right">
                                        0.00
                                    </td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-green btn-sm" id="btn-addRow-description"><span class="glyphicon glyphicon-plus"></span></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <label>Detail</label>
                        <table class="table master-data  table-striped" id="table-salesorder-header" style="table-layout:fixed;">
                            <thead  class="default-thead">
                                <tr>
                                    <th style="width: 25%;">Inventory</th>
                                    <th style="width: 8%;">Uom</th>
                                    <th style="width: 7%;">Stock</th>
                                    <th style="width: 10%;">Description</th>
                                    <th style="width: 8%;">Qty</th>
                                    <th style="width: 10%;">Original Price</th>
                                    <th style="width: 15%;">Price</th>
                                    <th style="width: 7%;">Disc (%)</th>
                                    <th style="width: 10%;">Disc</th>
                                    <th style="width: 10%;">Subtotal</th>
                                    <th class="text-center" style="width: 5%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="row0">
                                    <td class="chosen-transaction">
                                        <input class="input-theme margbot10" type="text" id="searchInventory" tabindex="-1" title="Type Inventory Name or ID then 'Enter'" placeholder="Type Inventory Name or ID then 'Enter'">
                                        <div id="selectInventory">

                                        </div>
                                    </td>
                                    <td>
                                        <select id="uom-0" class="input-theme uom">

                                        </select>
                                    </td>
                                    <td class="text-right">
                                        <span class="maxWidth stock" id="price-0-stock" data-toggle="popover" data-placement="bottom" data-html="true">-</span>
                                    </td>
                                    <td>
                                        <select id="description-0" class="input-theme description">
                                            <?php $count = 1; ?>
                                            @if(isset($arrquotation) && $arrquotation != 0)
                                            @foreach($arrquotation as $QuotationID)
                                            <?php $QuotationInternalID = QuotationHeader::where('QuotationID', $QuotationID)->first()->InternalID; ?>
                                            @foreach(QuotationDescription::where('QuotationInternalID',$QuotationInternalID)->get() as $description)
                                            <option value='rowDescription{{$count}}'>{{$description->InventoryText}}</option>
                                            <?php $count++; ?>
                                            @endforeach
                                            @endforeach
                                            @endif
                                        </select>
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth qty right numaja input-theme" maxlength="11" min="1" id="price-0-qty" value="1">
                                    </td>
                                    <td class="text-right" id="oriprice-0">
                                        0.00
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth price right numajaDesimal input-theme" maxlength="" id="price-0" value="0">
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth discount right numajaDesimalMinus input-theme" id="price-0-discount" value="0.00">
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth discountNominal right numajaDesimalMinus input-theme" id="price-0-discountNominal" value="0.00">
                                    </td>
                                    <td id="price-0-qty-hitung" class="right">
                                        0.00
                                    </td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-green btn-sm" disabled="" id="btn-addRow"><span class="glyphicon glyphicon-plus"></span></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <!--table bawah yang berisi lengkap-->
                        <table class="table master-data" id="table-salesorder-description" style="table-layout:fixed;">
                            <thead>
                                <tr>
                                    <th style="width: 25%;">Inventory</th>
                                    <th style="width: 10%;">Uom</th>
                                    <th style="width: 10%;">Stock</th>
                                    <th style="width: 10%;">Qty</th>
                                    <th style="width: 15%;">Price</th>
                                    <th style="width: 10%;">Disc (%)</th>
                                    <th style="width: 10%;">Disc</th>
                                    <th style="width: 15%;">Subtotal</th>
                                    <th style="width: 14%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $arrData = "";
                                $descriptionn = array();
                                $arrCount = array();
                                $arrInv = array();
                                $i = 0;
                                $barisDesc = 1;
                                $barisDetail = 1;
                                ?>
                                @if(isset($arrquotation) && $arrquotation != 0)
                                @foreach($arrquotation as $QuotationID)
                            <input type="hidden" name="idQuotation[]" value="{{$arrquotation[$i]}}">
                            {{''; $arrData.=$QuotationInternalID.','}}
                            <?php $QuotationInternalID = QuotationHeader::where('QuotationID', $QuotationID)->first()->InternalID; ?>
                            @foreach(QuotationDescription::where('QuotationInternalID',$QuotationInternalID)->get() as $description)
                            <?php array_push($descriptionn, "~" . $barisDesc); ?>
                            <tr style='background: #F1FFDB;' id='rowDescription{{$barisDesc}}' data-parent="rowDescription{{$barisDesc}}">
                                <td class='chosen-uom'>
                                    <input type="hidden" class="inventoryDescription" style="width: 100px" id="inventoryDescription-{{$barisDesc}}" name="inventoryDescription[]" value="{{$description->InventoryText}}">{{$description->InventoryText}}
                                </td>
                                <td>
                                    <input type="text" class="uomDescription input-theme" name="uomDescription[]" id="uomDescription-{{$barisDesc}}" value='{{$description->UomText}}'>
                                </td>
                                <td class='text-right'>
                                    -
                                </td>
                                <td class='text-right'>
                                    <input type="text" class="maxWidth qtyDescription right input-theme" name="qtyDescription[]" id="priceDescription-{{$barisDesc}}-qty" value='{{$description->Qty}}'>
                                </td>
                                <td>
                                    <input type="text" class="maxWidth priceDescription right numajaDesimalMinus input-theme" name="priceDescription[]" maxlength="" value="{{number_format($description->Price,'2','.',',')}}" id="priceDescription-{{$barisDesc}}">
                                </td>
                                <td class='text-right'>
                                    <input disabled type="text" class="maxWidth discountDescription right numajaDesimalMinus input-theme" name="discountDescription[]" id="priceDescription-{{$barisDesc}}-discount" value="{{number_format($description->Discount,'2','.',',')}}">
                                </td>
                                <td class='text-right'>
                                    <input disabled type="text" class="maxWidth discountNominalDescription right numajaDesimalMinus input-theme" name="discountNominalDescription[]" id="priceDescription-{{$barisDesc}}-discountNominal" value="{{number_format($description->DiscountNominal,'2','.',',')}}">
                                </td>
                                <td class="right subtotalDescription" id="priceDescription-{{$barisDesc}}-qty-hitung">{{number_format(ceil($description->SubTotal),'2','.',',')}}</td>
                                <td>
                                    <button class="btn btn-pure-xs btn-xs btn-deleteRow-Description margr5" type="button" data="rowDescription{{$barisDesc}}" text="rowSpec{{$barisDesc}}"><span class="glyphicon glyphicon-trash"></span></button>
                                    <button class="btn btn-pure-xs btn-xs btn-toggle-detail margr5" type="button" data-toshow="show" data="rowDescription{{$barisDesc}}"><span class="glyphicon glyphicon-eye-open"></span></button>
                                    <button class="btn btn-pure-xs btn-xs btn-toggle-spec" type="button" data-toshow="show" data="rowSpec{{$barisDesc}}"><span class="glyphicon glyphicon-comment"></span></button>
                                </td>
                            </tr>
                            <tr id='rowSpec{{$barisDesc}}' style="display:none">
                                <td colspan='9' class='rowSpec{{$barisDesc}}'>
                                    <textarea onkeyup="textAreaAdjust(this)" name="spesifikasi[]" style="overflow:hidden; width: 100%;" class="input-theme rowSpec{{$barisDesc}}">{{$description->Spesifikasi}}</textarea>
                                </td>
                            </tr>
                            @foreach(QuotationDetail::where('DescriptionInternalID',$description->InternalID)->get() as $detail)
                            <!--untuk non-parcel-->
                            @if($detail->QuotationParcelInternalID == 0)
                            <?php
                            $arrCount["rowDescription" . $barisDesc] = $barisDetail;
                            $arrInv["rowDescription" . $barisDesc][$arrCount["rowDescription" . $barisDesc]] = $detail->InventoryInternalID . "---;---inventory"
                            ?>
                            <tr id='row{{$barisDetail}}' class='rowDescription{{$barisDesc}}' data-parent="rowDescription{{$barisDesc}}">
                                <td class='chosen-uom'>
                                    <input type="hidden" class="inventory" style="width: 100px" id="inventory-{{$barisDetail}}" name="inventory{{$barisDesc}}[]" value="{{$detail->InventoryInternalID}}---;---inventory">{{Inventory::find($detail->InventoryInternalID)->InventoryID.' '.Inventory::find($detail->InventoryInternalID)->InventoryName}}
                                </td>
                                <td>
                                    <select id="uom-{{$barisDetail}}" name="uom{{$barisDesc}}[]" class="input-theme uom">
                                        @foreach (InventoryUom::where("InventoryInternalID", $detail->InventoryInternalID)->get() as $uom)
                                        <option value="{{$uom->UomInternalID}}" {{ ($uom->UomInternalID == $detail->UomInternalID) ? "selected" : ""}}>{{ $uom->Uom->UomID; }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td class='text-right'>
                                    <?php
                                    $tamp = "";
                                    $tamp = getStockInventorySOHelper($detail->InventoryInternalID);
                                    $result = explode('---;---', $tamp);
                                    ?>
                                    <span class="maxWidth stock right input-theme" id="price-{{$barisDesc}}-stock">{{$result[0]}}</span>
                                </td>
                                <td class='text-right'>
                                    <input type="text" class="maxWidth qty right input-theme" name="qty{{$barisDesc}}[]" maxlength="11" min="1" value="{{number_format($detail->Qty,'0','.',',')}}" id="price-{{$barisDetail}}-qty">
                                </td>
                                <td>
                                    <input type="text" class="maxWidth price right numajaDesimal input-theme" name="price{{$barisDesc}}[]" maxlength="" value="{{number_format($detail->Price,'2','.',',')}}" id="price-{{$barisDetail}}">
                                </td>
                                <td class='text-right'>
                                    <input type="text" class="maxWidth discount right input-theme numajaDesimalMinus" name="discount{{$barisDesc}}[]" min="0" max="100" id="price-{{$barisDetail}}-discount" value="{{$detail->Discount}}">
                                </td>
                                <td class='text-right'>
                                    <input type="text" class="maxWidth discountNominal right numajaDesimalMinus input-theme" name="discountNominal{{$barisDesc}}[]" id="price-{{$barisDetail}}-discountNominal" value="{{number_format($detail->DiscountNominal,'2','.',',')}}">
                                </td>
                                <td class="right subtotal" id="price-{{$barisDetail}}-qty-hitung">{{number_format(ceil($detail->SubTotal),'2','.',',')}}</td>
                                <td>
                                    <button class="btn btn-pure-xs btn-xs btn-deleteRow" type="button" data="row{{$barisDetail}}" barang="inventory-{{$barisDetail}}"><span class="glyphicon glyphicon-trash"></span></button>
                                </td>
                            </tr>
                            <?php $barisDetail++; ?>
                            @endif <!--tutup if non parcel -->
                            @endforeach<!--tutup foreach quotation detail-->
                            <!--untuk parcel-->
                            @foreach(QuotationParcel::where('DescriptionInternalID',$description->InternalID)->get() as $parcel)
                            <tr id='row{{$barisDetail}}' class='rowDescription{{$barisDesc}}' data-parent="rowDescription{{$barisDesc}}">
                                <?php
                                $arrCount["rowDescription" . $barisDesc] = $barisDetail;
                                $arrInv["rowDescription" . $barisDesc][$arrCount["rowDescription" . $barisDesc]] = $parcel->ParcelInternalID . "---;---parcel"
                                ?>
                                <td class='chosen-uom'>
                                    <input type="hidden" class="inventory" style="width: 100px" id="inventory-{{$barisDetail}}" name="inventory{{$barisDesc}}[]" value="{{$parcel->ParcelInternalID}}---;---parcel">{{Parcel::find($parcel->ParcelInternalID)->ParcelID.' '.Parcel::find($parcel->ParcelInternalID)->ParcelName}}
                                </td>
                                <td>
                                    <select id="uom-{{$barisDetail}}" readonly name="uom[]" class="input-theme uom">
                                        <option value="0" >-</option>
                                    </select>
                                </td>
                                <td class='text-right'>
                                    -
                                </td>
                                <td class='text-right'>
                                    <input type="text" class="maxWidth qty right input-theme" name="qty{{$barisDesc}}[]" maxlength="11" min="1" value="{{number_format($parcel->Qty,'0','.',',')}}" id="price-{{$barisDetail}}-qty"></td>
                                <td>
                                    <input type="text" class="maxWidth price right numajaDesimal input-theme" name="price{{$barisDesc}}[]" maxlength="" value="{{number_format($parcel->Price,'2','.',',')}}" id="price-{{$barisDetail}}">
                                </td>
                                <td class='text-right'>
                                    <input type="text" class="maxWidth discount right input-theme numajaDesimalMinus" name="discount{{$barisDesc}}[]" min="0" max="100" id="price-{{$barisDetail}}-discount" value="{{$parcel->Discount}}">
                                </td>
                                <td class='text-right'>
                                    <input type="text" class="maxWidth discountNominal right numajaDesimalMinus input-theme" name="discountNominal{{$barisDesc}}[]" id="price-{{$barisDetail}}-discountNominal" value="{{number_format($parcel->DiscountNominal,'2','.',',')}}">
                                </td>
                                <td class="right subtotal" id="price-{{$barisDetail}}-qty-hitung">{{number_format($parcel->SubTotal,'2','.',',')}}</td>
                                <td>
                                    <button class="btn btn-pure-xs btn-xs btn-deleteRow" type="button" data="row{{$barisDetail}}" barang="inventory-{{$barisDetail}}"><span class="glyphicon glyphicon-trash"></span></button>
                                </td>
                            </tr>
                            <?php $barisDetail++; ?>
                            @endforeach<!--tutup foreach quotation parcel-->
                            <?php $barisDesc++; ?>
                            @endforeach <!--tutup foreach quotation description-->
                            <?php $i++; ?>
                            @endforeach <!--tutup foreach $arrquotation-->
                            @endif <!--tutup if ada quotation -->
                            </tbody>
                            @if(!isset($arrquotation) || $arrquotation == 0)
                            <tfoot id="no-in-description">
                                <tr style="background: #fafafa;" >
                                    <td colspan="9" class="text-center">no data available.</td>
                                </tr>
                            </tfoot>
                            @endif
                        </table>
                    </div><!---- end div tableadd---->
                    <input type="hidden" name="grandTotalValue" value="0" id="grandTotalValue">
                    <table class="pull-right">
                        <tr>
                            <td><h5 class="right margr10 h5total"><b>Total</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="total"></b></h5></td>
                        </tr>
                        <tr >
                            <td><h5 class="right margr10 h5total"><b>Discount</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            @if(isset($quotation))
                            <td><h5 class="right margr10 h5total new" style="width: 150px;"><input type="text" style="width: 120px;" class="maxWidth right numaja discountGlobal" name="DiscountGlobal" maxlength="" id="discountGlobal" value="{{number_format($quotation->DiscountGlobal,2,".",",")}}"></h5></td>
                            @else
                            <td><h5 class="right margr10 h5total new" style="width: 150px;"><input type="text" style="width: 120px;" class="maxWidth right numaja discountGlobal" name="DiscountGlobal" maxlength="" id="discountGlobal" value=""></h5></td>
                            @endif
                        </tr>
                        <tr>
                            <td><h5 class="right margr10 h5total"><b>Grand Total</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="grandTotal"></b></h5></td>
                        </tr>

                        <tr>
                            <td><h5 class="right margr10 h5total hidevat"><b>Tax</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b id="tax"></b></h5></td>
                        </tr>
                        <tr>
                            <td><h5 class="right margr10 h5total hidevat"><b>Grand Total(tax)</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b id="grandTotalAfterTax"></b></h5></td>
                        </tr>
                    </table>
                </div><!---- end div tableadd---->
            </div><!---- end div tabwrap---->

            <div class="btnnest pull-right">
                @if (myCheckIsEmpty('Customer;Warehouse;Currency;Inventory'))
                <button class="btn btn-green btn-sm btn-save" type="button"> Save & Print </button>
                @else
                <button class="btn btn-green btn-sm btn-save" id="btn-save"> Save & Print </button>
                @endif
            </div>
        </form>
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop

@section('modal')
<!--untuk customer-->
<div class="modal fade bs-example-modal-lg" id="m_coa" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="" method="post" class="action" id="form-insert">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Insert coa level 6</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form coawd">

                        <div class="row">
                            <div class="col-md-5">
                                <div class="margbot10">
                                    <input type='hidden' name='jenis' value='insertCustomer'>
                                    @if(isset($customer) && $customer != "0")
                                    <input type='hidden' name='customerSales2' value='{{$customer}}'>
                                    @endif
                                    @if(isset($inputquot) && count($inputquot) != "0")
                                    <input type='hidden' name='Quotation2[]' value='<?php print_r($inputquot) ?>'>
                                    @endif
                                </div>
                                <div class="margbot10" style="display:none">
                                    <label for="AccID">Type *</label                                >
                                </div>
                                <div class="margbot10" style="display:none">
                                    <div class="radio-inline margr10">
                                        <input type="radio" class="radio-tipe" id="tipeCustomer" name="Type" value="c" checked="checked"><label for="tipeCustomer">Customer</label>
                                    </div>
                                    <div class="radio-inline nomargl">
                                        <input type="radio" class="radio-tipe" id="tipeSupplier" name="Type" value="s"><label for="tipeSupplier">Supplier</label>
                                    </div>
                                    <div class="radio-inline nomargl">
                                        <input type="radio" class="radio-tipe" id="tipeIndustry" name="Type"        value="i"><label for="tipeIndustry">Industry</label>
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="AccID">ID *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="noSpecialCh    aracter" type="text" name="AccID" id="accID" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="ACCName">Name *</label>
                                </div>
                                <div class="margbot10">
                                    @if(isset($customer) && $customer != "0" && isset($quotation) && $type == "baru")
                                    <input class="noSpecialCharacter" type="text" name="AccName" id="name" maxlength="200" data-validation="required" value="{{$quotation->CustomerName}}">
                                    @else
                                    <input class="noSpecialCharacter" type="text" name="AccName" id="name" maxlength="200" data-validation="required">
                                    @endif
                                </div>
                                <div class="typecustomer">
                                    <div class="margbot10">
                                        <label for="customerType">Customer Type</label>
                                    </div>
                                    <div class="margbot10">

                                        <div class="radio-inline margr10">
                                            <input type="radio" class="radio-tipe" id="tipeSeller" name="userType" value="sr" checked="checked"><label for="seller">Seller</label>
                                        </div>
                                        <div class="radio-inline nomargl">
                                            <input type="radio" class="radio-tipe" id="tipeEnduser" name="userType" value="er"><label for="endUser">End User</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="taxID">TaxID</label>
                                </div>
                                <div class="margbot10">
                                    <div id="npwpin">
                                        <input type="hidden" name="taxID" value="" id="taxID">
                                        <input class="persen11 autoTab numaja" type="text" name="taxID1" id="taxID1" maxlength="2"
                                               data-validation="length" data-validation-length="min2"
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> .
                                        <input class="persen15 autoTab numaja" type="text" name="taxID2" id="taxID2" maxlength="3"
                                               data-validation="length" data-validation-length="min3"
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> .
                                        <input class="persen15 autoTab numaja" type="text" name="taxID3" id="taxID3" maxlength="3"
                                               data-validation="length" data-validation-length="min3"
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> .
                                        <input class="persen8 autoTab numaja" type="text" name="taxID4" id="taxID4" maxlength="1"
                                               data-validation="length" data-validation-length="min1"
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> -
                                        <input class="persen15 autoTab numaja" type="text" name="taxID5" id="taxID5" maxlength="3"
                                               data-validation="length" data-validation-length="min3"
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> .
                                        <input class="persen15 autoTab numaja" type="text" name="taxID6" id="taxID6" maxlength="3"
                                               data-validation="length" data-validation-length="min3"
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information.">
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="address">Address *</label>
                                </div>
                                <div class="margbot10">
                                    @if(isset($customer) && $customer != "0" && isset($quotation) && $type == "baru" && $quotation->CustomerAddress != null && $quotation->CustomerAddress != '')
                                    <textarea style="resize:none;" name="Address" maxlength="1000" data-validation="required">{{$quotation->CustomerAddress}}</textarea>
                                    @else
                                    <textarea style="resize:none;" name="Address" maxlength="1000" data-validation="required"></textarea>
                                    @endif
                                </div>
                                <div class="margbot10">
                                    <label for="Block">Block</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Block" id="block" maxlength="200">
                                </div>
                                <div class="margbot10">
                                    <label for="AddressNumber">Address Number</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="AddressNumber" id="addressNumber" maxlength="200">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="margbot10">
                                    <label for="rt">RT</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="RT" id="rt" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="rw">RW</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="RW" id="rw" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="district">District</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="District" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="subdistrict">Sub District</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Subdistrict" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="city">City</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="City" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="province">Province</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Province" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="postalcode">Postal Code</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="PostalCode" maxlength="200" >
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="margbot10">
                                    <label for="origin">Email</label>
                                </div>
                                <div class="margbot10">
                                    @if(isset($customer) && $customer != "0" && isset($quotation) && $type == "baru")
                                    <input type="text" name="Email" maxlength="200" value="{{$quotation->CustomerEmail}}">
                                    @else
                                    <input type="text" name="Email" maxlength="200">
                                    @endif
                                </div>
                                <div class="margbot10">
                                    <label for="phone">Phone *</label>
                                </div>
                                <div class="margbot10">
                                    @if(isset($customer) && $customer != "0" && isset($quotation)&& $type == "baru")
                                    <input type="text" name="Phone" maxlength="200" data-validation="required" value="{{$quotation->CustomerPhone}}">
                                    @else
                                    <input type="text" name="Phone" maxlength="200" data-validation="required">
                                    @endif
                                </div>
                                <div class="margbot10">
                                    <label for="fax">Fax *</label>
                                </div>
                                <div class="margbot10">
                                    @if(isset($customer) && $customer != "0" && isset($quotation)&& $type == "baru")
                                    <input type="text" name="Fax" maxlength="200" data-validation="required" value="{{$quotation->CustomerFax}}">
                                    @else
                                    <input type="text" name="Fax" maxlength="200" data-validation="required">
                                    @endif
                                </div>
                                <div class="margbot10">
                                    <label for="creditlimit">Credit limit</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" class="numajaDesimal" id="uangCredit" name="CreditLimit" maxlength="">
                                </div>
                                <!--                                <div class="margbot10">
                                                                    <label for="contactperscon">Contact person</label>
                                                                </div>
                                                                <div class="margbot10">
                                                                    <input type="text" name="ContactPerson" maxlength="200">
                                                                </div>-->
                                <div class="custmanager">
                                    <div class="margbot10">
                                        <label for="remark">Customer Manager *</label>
                                    </div>
                                    <div class="margbot10">
                                        <select name="customerManager" id="customerManager" class="chosen-select choosen-modal">
                                            @foreach(SalesMan::all() as $salesman)
                                            <option value="{{$salesman->InternalID}}">{{$salesman->SalesManName}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="margbot10">
                                        <label for="remark">Sales Assistant</label>
                                    </div>
                                    <div class="margbot10">
                                        <select name="salesAssistant" id="salesAssistant" class="chosen-select choosen-modal">
                                            @foreach(SalesMan::all() as $salesman)
                                            <option value="{{$salesman->InternalID}}">{{$salesman->SalesManName}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;"  name="remark" id="" maxlength="1000"></textarea>
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade modal-quotation" id="insertQuotation" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insert Quotation</h4>
            </div>
            <form action="{{Route("salesOrderNew")}}" method="GET" class="action">
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="insertQuotation" id="jenisQuotation" name="jenis">
                            <li>
                                <label for="customerSales">Customer</label> *
                            </li>
                            <li>
                                <select class="chosen-select choosen-modal input-theme" id="customerSales" style="" name="customerSales">
                                    @foreach(QuotationHeader::where("VAT","!=",Auth::user()->SeeNPPN)->distinct()->select('CustomerName')->groupBy('CustomerName')->get() as $cus)
                                    <option value="{{$cus->CustomerName}}">
                                        {{$cus->CustomerName}}
                                    </option>
                                    @endforeach
                                    @foreach(Coa6::where('CompanyInternalID', Auth::user()->Company->InternalID)->where('Type', 'C')->get() as $coa6)
                                    <option value="{{$coa6->InternalID}}">
                                        {{$coa6->ACC6ID.' | '.$coa6->ACC6Name}}
                                    </option>
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label for="sales">Quotation ID</label> *
                            </li>
                            <li>
                                <select multiple class="chosen-select choosen-modal input-theme input-stretch" id="quotation" style="" name="Quotation[]">
                                    {{'';$hitung=0;}}
                                    @foreach(QuotationHeader::where("VAT","!=",Auth::user()->SeeNPPN)->where('CompanyInternalID', Auth::user()->Company->InternalID)->OrderBy('QuotationDate', 'desc')->get() as $quotation)
                                    @if(checkQuotation($quotation->InternalID))
                                    @if($quotation->TypeCustomer == 0)
                                    <option class="optionQuotation" id="cust{{$quotation->ACC6InternalID}}" value="{{$quotation->QuotationID}}">
                                        @else
                                    <option class="optionQuotation" id="cust{{$quotation->CustomerName}}" value="{{$quotation->QuotationID}}">
                                        @endif
                                        {{$quotation->QuotationID.' | '.date( "d-m-Y", strtotime($quotation->QuotationDate))}}
                                    </option>
                                    {{'';$hitung=1;}}
                                    @endif
                                    @endforeach
                                </select>
                            </li>
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="btn-submit-quotation" @if($hitung==0){{'disabled'}}@endif class="btn btn-green">Submit</button>
                        <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                    </div>
            </form>
        </div>
    </div>
</div>
</div>

<div class="modal fade" id="r_summary" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Summary Report</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="" target="_blank">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='summarySalesOrder'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateReport" data-validation="required">
                            </li>
                        </ul>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>

<?php
if (!isset($quot))
    $quot = 'no';
if (!isset($idquot))
    $idquot = 'no';
?>
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosen.jquery.js')}}" type="text/javascript"></script>
<script>
                                        $("form").bind("submit", function (event) {
                                            setTimeout(function () {
                                                if ($(".form-error")[0]) {
                                                    $("#btn-save").prop("disabled", false);
                                                } else {
                                                    $("#btn-save").prop("disabled", true);
                                                }
                                            }, 100);

                                        });
                                        var quot = "<?php echo $quot ?>";
                                        var idquot = "<?php echo $idquot ?>";
                                        var getUomThisInventory = "<?php echo Route("getUomThisInventory") ?>";
                                        var getCustomerManager = "<?php echo Route("getCustomerManager") ?>";
                                        var getPriceDefault = "<?php echo Route("getPriceDefault") ?>";
                                        var getPriceRangeThisInventorySO = "<?php echo Route("getPriceRangeThisInventorySO") ?>";
                                        var getPriceThisParcel = "<?php echo Route("getPriceThisParcel") ?>";
                                        var checkRecieveable = '<?php echo Route('checkRecieveable') ?>';
                                        var getSearchResultInventoryForSO = "<?php echo Route("getSearchResultInventoryForSO") ?>";
                                        var getStockInventorySO = "<?php echo Route("getStockInventorySO") ?>";
                                        var getPriceRangeSO = "<?php echo Route("getPriceRangeSO") ?>";
                                        var textSelect = '';
                                        var tanggalHariIni = '<?php echo date('d') . '-' . date('m') . '-' . date('Y') ?>';
                                        var cariS = '<?php echo Route('formatCariIDSalesOrder') ?>';
                                        var savePrint = '<?php echo Route('savePrintSO'); ?>';

                                        $(document).ready(function () {
                                            $(".btn-insert").click(function () {
                                                var idAkhir = "<?php echo Coa6::maxID('C') ?>";
                                                $('#accID').val("C" + idAkhir);
                                            });
                                        });
</script>
@if(isset($barisDesc))
<script>
    var bariss = <?php echo $barisDesc; ?>;
    var baris = <?php echo $barisDetail; ?>;
    var description = '<?php echo json_encode($descriptionn); ?>';
    var arrCount = '<?php echo json_encode($arrCount) ?>';
    var arrInven = '<?php echo json_encode($arrInv) ?>';
</script>
@else
<script>
    var baris = 1;
    var bariss = 1;
    var description = [];
    var arrCount = [];
    var arrInven = [];
</script>
@endif
<script>
    var salesOrderDataBackup = '<?php echo Route('salesOrderDataBackup', Input::get('coa6') . '---;---' . Input::get('typePayment') . '---;---' . Input::get('typeTax') . '---;---' . Input::get('startDate') . '---;---' . Input::get('endDate')) ?>';
</script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script src="{{Asset('js/chosenNewJournal.jquery.js')}}" type="text/javascript"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-penjualan-add/salesOrder.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-penjualan-add/salesOrderNew.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-penjualan-add/salesOrderNewDescription.js')}}"></script>
@stop
