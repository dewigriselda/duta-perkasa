@extends('template.header-footer')

@section('title')
Sales Order
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosenSearch.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<style>
    .redClass{
        background-color: lightsalmon !important;
    }
</style>
@stop

@section('nav')
@stop

@section('content')
@if(myCheckIsEmpty('Customer;Warehouse;Currency;Inventory'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one master Customer, Currency, Warehouse, and Inventory to insert sales order.
</div>
@endif
@if(isset($messages))
@if($messages == 'suksesDelete')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Sales Order has been deleted.
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Sales Order has been updated.
</div>
@endif
@if($messages == 'gagalDelete')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Sales Order has been registered in table sales.
</div>
@endif
@if($messages == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showSalesOrder')}}" type="button" class="btn btn-sm btn-pure">Sales Order</a>
            </div>
            <div class="btn-group margr5">
                @if (myCheckIsEmpty('Customer;Warehouse;Currency;Inventory'))
                <button type="button" class="btn btn-green btn-sm dropdown-toggle  " aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
                @else
                <a href="{{Route('salesOrderNew')}}">
                    <button type="button" class="btn btn-green btn-sm dropdown-toggle  " aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
                </a>
                @endif
            </div>
            <div class="btn-group margr5">
                <button type="button" <?php if (myCheckIsEmpty('Quotation;Default')) echo 'disabled'; ?> class="btn btn-green btn-sm dropdown-toggle" data-target="#insertQuotation" data-toggle="modal" role="dialog"
                        aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New from Quotation </button>
            </div>
            <button id="search-button" class="btn btn-green btn-sm margr5"  <?php if (myCheckIsEmpty('SalesOrder')) echo 'disabled'; ?> ><span class="glyphicon glyphicon-search"></span> Search </button>
            @if(checkModul('O04'))
            <div class="btn-group">
                <button type="button" class="btn btn-green dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="glyphicon glyphicon-file"></span> Report <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a data-target="#r_summary" data-toggle="modal" role="dialog" href="#" id="btn-rDetail">Detail Report</a></li>
                    <li><a data-target="#r_summary" data-toggle="modal" role="dialog" href="#" id="btn-rSummary">Summary Report</a></li>
                    <li><a target="_blank" href="{{Route("uncomplateSalesOrder")}}">Uncomplate Sales Order Report</a></li>
                </ul>
            </div>
            @endif
        </div>
        <!-- <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Search</h4>
            </div>
            <div class="tableadd">
                <ul class="searchmenu">
                    <form method="GET" action="">
                        <li>
                            <label for="coa6">Customer</label>
                            <br>
                            <select class="chosen-select" id="coa6" style="" name="coa6">
                                <option value="-1">All Customer</option>
                                @foreach(Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $coa6)
                                <option value="{{$coa6->InternalID}}">
                                    {{$coa6->ACC6ID.' '.$coa6->ACC6Name}}
                                </option>
                                @endforeach
                            </select>
                        </li>
                        <li><label for="typePayment">Payment Type</label>
                            <br>
                            <select name="typePayment" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Cash</option>
                                <option value="1">Credit</option>
                            </select>
                        </li>
                        <li><label for="typeTax">Tax Type</label>
                            <br>
                            <select name="typeTax" style="width: 70px">
                                <option value="-1">All</option>
                                <option value="0">Non Tax</option>
                                <option value="1">Tax</option>
                            </select>
                        </li>
                        <li><label for="date">Start Date</label>
                            <br><input id="startDate" name="startDate" type="text" autocomplete="off">
                        </li>
                        <li><label for="date">End Date</label>
                            <br><input id="endDate" name="endDate" type="text" autocomplete="off">
                        </li>
                        <li><br>
                            <button type="submit" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-search"></span> Search </button>
                        </li>
                        <li><br>
                            <button id="cancel" type="reset"  class="btn btn-pure btn-sm"><span class="glyphicon glyphicon-remove"></span> Cancel </button>
                        </li>
                    </form>
                </ul>
            </div><!---- end div tableadd---->
        <!--</div><!---- end div tabwrap---->
        @include('template.searchComponentTransactionModule')
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
<div class="leftrow pull-left">
</div>
<div class="righttrow pull-right">
</div>
<div class="wrapjour">
    <div class="primcontent">
        <div class="tabwrap">
            <div class="tabhead">
                <h4 class="headtitle">Sales Order</h4>
            </div>
            <div class="tableadd">
                <table id="example" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Internal ID</th>
                            <th>Sales Order ID</th>
                            <th>Payment Type</th>
                            <th>Date</th>
<!--                            <th>Currency</th>
                            <th>Rate</th>-->
                            <th>Customer</th>
                            @if(checkSeeNPPN())
                            <!--<th>Tax Type</th>-->
                            @endif
                            <th>Grand Total</th>
                            <th>Commission</th>
                            <th>Approval</th>
                            <th>Status</th>
                            <th>Print</th>
                            <th style="min-width: 100px">Actions</th>
                        </tr>
                    </thead>
                </table>
            </div><!---end of tableadd--->
        </div><!---- end div tabwrap---->
    </div><!---- end div wrapjour---->
</div><!---- end div wrapcontent---->
@stop

@section('modal')
<div class="modal fade modal-quotation" id="insertQuotation" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insert Quotation</h4>
            </div>
            <form action="{{Route("salesOrderNew")}}" method="GET" class="action">
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="insertQuotation" id="jenisQuotation" name="jenis">
                            <li>
                                <label for="customerSales">Customer</label> *
                            </li>
                            <li>
                                <select class="chosen-select choosen-modal input-theme" id="customerSales" style="" name="customerSales">
                                    @foreach(QuotationHeader::where("VAT","!=",Auth::user()->SeeNPPN)->distinct()->select('CustomerName')->groupBy('CustomerName')->get() as $cus)
                                    <option value="{{$cus->CustomerName}}">
                                        {{$cus->CustomerName}}
                                    </option>
                                    @endforeach
                                    @foreach(Coa6::where('CompanyInternalID', Auth::user()->Company->InternalID)->where('Type', 'C')->get() as $coa6)
                                    <option value="{{$coa6->InternalID}}">
                                        {{$coa6->ACC6ID.' | '.$coa6->ACC6Name}}
                                    </option>
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label for="sales">Quotation ID</label> *
                            </li>
                            <li>
                                <select multiple class="chosen-select choosen-modal input-theme input-stretch" id="quotation" style="" name="Quotation[]">
                                    {{'';$hitung=0;}}
                                    @foreach(QuotationHeader::where("VAT","!=",Auth::user()->SeeNPPN)->where('CompanyInternalID', Auth::user()->Company->InternalID)->OrderBy('QuotationDate', 'desc')->get() as $quotation)
                                    @if(checkQuotation($quotation->InternalID))
                                    @if($quotation->TypeCustomer == 0)
                                    <option class="optionQuotation" id="cust{{$quotation->ACC6InternalID}}" value="{{$quotation->QuotationID}}">
                                        @else
                                    <option class="optionQuotation" id="cust{{$quotation->CustomerName}}" value="{{$quotation->QuotationID}}">
                                        @endif
                                        {{$quotation->QuotationID.' | '.date( "d-m-Y", strtotime($quotation->QuotationDate))}}
                                    </option>
                                    {{'';$hitung=1;}}
                                    @endif
                                    @endforeach
                                </select>
                            </li>
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="btn-submit-quotation" @if($hitung==0){{'disabled'}}@endif class="btn btn-green">Submit</button>
                        <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="m_salesOrderDelete" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Delete Sales Order</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action">
                        <ul>
                            <input type="hidden" value="" id="idDelete" name="InternalID">
                            <input type="hidden" value="deleteSalesOrder" id="jenisDelete" name="jenis">
                            <p>Are you sure want to delete <span id="deleteName"></span>?</p>
                        </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-green">Yes</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="m_salesOrderClose" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Close Sales Order</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action">
                        <ul>
                            <input type="hidden" value="" id="idClose" name="InternalID">
                            <input type="hidden" value="closeSalesOrder" id="jenisClose" name="jenis">
                            <p>Are you sure want to close <span id="closeName"></span>?</p>
                        </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-green">Yes</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="m_salesOrderApprove" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Approve Sales Order</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action">
                        <ul>
                            <input type="hidden" value="" id="idApprove" name="InternalID">
                            <input type="hidden" value="approveSalesOrder" id="jenisApprove" name="jenis">
                            <p>Are you sure want to approve <span id="approveName"></span>?</p>
                        </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-green">Yes</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="r_summary" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Summary Report</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="" target="_blank">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='summarySalesOrder'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateReport" data-validation="required">
                            </li>
                        </ul>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="r_print" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Tax</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="" target="_blank">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='printSalesOrder'>
                            <input type="hidden" name="internalID" id="internalID" />
                            <li>
                                <label for="type">Type Tax</label> *
                            </li>
                            <li>
                                <select class="form-control" name="type">
                                    <option value="include">Include</option>
                                    <option value="exclude">Exclude</option>
                                </select>
                            </li>
                        </ul>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<!--<script src="{{Asset('js/chosenNewJournal.jquery.js')}}" type="text/javascript"></script>-->
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<!--<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>-->
<script>
var salesOrderDataBackup =
        '<?php
if (Input::get('typePayment') == '') {
    echo Route('salesOrderDataBackup', '-1---;----1---;----1---;---' . $start . '---;---' . $end);
} else {
    echo Route('salesOrderDataBackup', Input::get('coa6') . '---;---' . Input::get('typePayment') . '---;---' . Input::get('typeTax') . '---;---' . Input::get('startDate') . '---;---' . Input::get('endDate'));
}
?>';
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-penjualan-add/salesOrder.js')}}"></script>
@stop
