@extends('template.header-footer')

@section('title')
Sales Order
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosenSearch.css')}}">
<!--<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">-->
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<style>
    .help-block {
        margin-left: 30% !important;
    }
</style>
@stop

@section('nav')

@stop

@section('content')
@if(isset($messages))
@if($messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Sales order has been updated.
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showSalesOrder')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Sales Order</a>
                <a href="{{route('salesOrderUpdate',$header->SalesOrderID)}}" type="button" class="btn btn-sm btn-pure">Update {{$header->SalesOrderID}}</a>
            </div>
            <div class="btn-group margr5">
                <a href="{{Route('salesOrderNew')}}">
                    <button type="button" class="btn btn-green btn-sm dropdown-toggle " aria-expanded="false"><span class="glyphicon glyphicon-plus"></span>New </button>
                </a>
            </div>
            <div class="btn-group margr5">
                <button type="button" <?php if (myCheckIsEmpty('Quotation;Default')) echo 'disabled'; ?> class="btn btn-green btn-sm dropdown-toggle" data-target="#insertQuotation" data-toggle="modal" role="dialog"
                        aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New from Quotation </button>
            </div>
            <button id="search-button" <?php if (myCheckIsEmpty('SalesOrder')) echo 'disabled'; ?>  class="btn btn-green btn-sm margr5"><span class="glyphicon glyphicon-search"></span> Search</button>
            @if(checkModul('O04'))
            <div class="btn-group">
                <button type="button" class="btn btn-green dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="glyphicon glyphicon-file"></span> Report & Print <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a data-target="#r_summary" data-toggle="modal" role="dialog" href="#" id="btn-rDetail">Detail Report</a></li>
                    <li><a data-target="#r_summary" data-toggle="modal" role="dialog" href="#" id="btn-rSummary">Summary Report</a></li>
                    <li><a target="_blank" href="{{Route("uncomplateSalesOrder")}}">Uncomplate Sales Order Report</a></li>
                    <li>
                        <a href="{{Route('salesOrderPrint',$header->SalesOrderID)}}" id="btn-{{$header->SalesOrderID}}-print" target='_blank' style="margin-right: 0px !important;"> Print</a>
<!--                        <a href="#" data-target="#r_print" data-internal="{{$header->SalesOrderID }}"  data-toggle="modal" role="dialog"
                           onclick="printAttach(this)" data-id="{{$header->SalesOrderID}}" data-name="{{$header->SalesOrderID}}">
                            Print
                        </a>-->
                    </li>
                    <!--<li><a href="{{Route('salesOrderPrintSJ',$header->SalesOrderID)}}" id="btn-{{$header->SalesOrderID}}-print" target='_blank' style="margin-right: 0px !important;">Print SJ</a></li>-->
                    <!--<li><a href="{{Route('salesOrderInternalPrint',$header->SalesOrderID)}}" id="btn-{{$header->SalesOrderID}}-print" target='_blank' style="margin-right: 0px !important;">Internal</a></li>-->
                </ul>
            </div>
            @endif
        </div>
        <!-- <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Search</h4>
            </div>
            <div class="tableadd">
                <ul class="searchmenu">
                    <form method="GET" action="{{Route('showSalesOrder')}}">
                        <li>
                            <label for="coa6">Customer *</label>
                            <br>
                            <select class="chosen-select" id="coa6" style="" name="coa6">
                                <option value="-1">All Customer</option>
                                @foreach(Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $coa6)
                                <option value="{{$coa6->InternalID}}">
                                    {{$coa6->ACC6ID.' '.$coa6->ACC6Name}}
                                </option>
                                @endforeach
                            </select>
                        </li>
                        <li><label for="typePayment">Payment Type</label>
                            <br>
                            <select name="typePayment" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Cash</option>
                                <option value="1">Credit</option>
                            </select>
                        </li>
                        <li><label for="typeTax">Tax Type</label>
                            <br>
                            <select name="typeTax" style="width: 70px">
                                <option value="-1">All</option>
                                <option value="0">Non Tax</option>
                                <option value="1">Tax</option>
                            </select>
                        </li>
                        <li><label for="date">Start Date</label>
                            <br><input id="startDate" name="startDate" type="text" autocomplete="off">
                        </li>
                        <li><label for="date">End Date</label>
                            <br><input id="endDate" name="endDate" type="text" autocomplete="off">
                        </li>
                        <li><br>
                            <button type="submit" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-search"></span> Search </button>
                        </li>
                        <li><br>
                            <button id="cancel" type="reset"  class="btn btn-pure btn-sm"><span class="glyphicon glyphicon-remove"></span> Cancel </button>
                        </li>
                    </form>
                </ul>
            </div><!---- end div tableadd---->
        <!--</div><!---- end div tabwrap---->
        @include('template.searchComponentTransactionModule')

        <form method="POST" action="">
            <input type='hidden' name='SalesOrderInternalID' id='SalesOrderInternalID' value='{{$header->InternalID}}'>
            <div class="tabwrap">
                <div class="tabhead">
                    @if($header->isCash == 0)
                    <h4 class="headtitle">Sales Order <span id="salesOrderID">{{$header->SalesOrderID}}</span></h4>
                    @else
                    <h4 class="headtitle">Sales Order <span id="salesOrderID">{{$header->SalesOrderID}}</span></h4>
                    @endif
                </div>
                <div class="tableadd">
                    <div class="headinv new">
                        <ul class="pull-left">
                            <li>
                                <label for="date">Date </label>
                                <span>{{date('d M Y', strtotime($header->SalesOrderDate))}}</span>
                                <input type="hidden" id="headerDate" value="{{date('d-m-Y', strtotime($header->SalesOrderDate))}}">
                            </li>
                            <li>
                                <label for="coa6">Customer *</label>
                                <select class="chosen-select" id="coa6_input" style="" name="coa6">
                                    @foreach(Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $coa6)
                                    @if($coa6->InternalID == $header->ACC6InternalID)
                                    <option selected="selected" value="{{$coa6->InternalID}}">
                                        {{$coa6->ACC6ID.' '.$coa6->ACC6Name}}
                                    </option>
                                    @else
                                    <option value="{{$coa6->InternalID}}">
                                        {{$coa6->ACC6ID.' '.$coa6->ACC6Name}}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>
                            </li>
                            <li id='customermanagerdiv'>

                            </li>
                            <li>
                                <label for="">Sales Assistant</label>
                                <span>{{Auth::user()->UserName}}</span>
                            </li>
                            <!--                            <li>
                                                            <label for="payment">Payment *</label>
                                                            @if($header->isCash == 0)
                                                            <input type="radio" class="radio-tipe" id="paymentCash" name="isCash" value="0" checked="checked">Cash
                                                            <input type="radio" class="radio-tipe" id="paymentCredit" name="isCash" value="1">Credit
                                                            @else
                                                            <input type="radio" class="radio-tipe" id="paymentCash" name="isCash" value="0">Cash
                                                            <input type="radio" class="radio-tipe" id="paymentCredit" name="isCash" value="1" checked="checked">Credit
                                                            @endif
                                                        </li>-->
                            <li>
                                <label for="payment">Payment *</label>
                                <select name="isCash" id="payment">
                                    <option value="0" @if($header->isCash == 0)selected @endif >Cash</option>
                                    <option value="1" @if($header->isCash == 1)selected @endif >Credit</option>
                                    <option value="2" @if($header->isCash == 2)selected @endif >CBD</option>
                                    <option value="3" @if($header->isCash == 3)selected @endif >Deposit</option>
                                    <option value="4" @if($header->isCash == 4)selected @endif >Down Payment</option>
                                </select>
                            </li>
                            <li class="downpayment">
                                <label for="longTerm">Down Payment Type</label>
                                <select name="jenisDP" id="jenisdp">
                                    <option value="0" @if($header->DownPayment > 0)selected @endif >Percentage</option>
                                    <option value="1" @if($header->DPValue > 0)selected @endif >Nominal</option>
                                </select>
                            </li>
                            <li class="downpayment persen">
                                <label for="longTerm">Down Payment (%)*</label>
                                <input type="text" name="downPayment" id="" class="numajaDesimal price2" value="{{number_format($header->DownPayment,'2','.',',')}}">
                            </li>
                            <li class="downpayment dpnominal">
                                <label for="longTerm">Down Payment *</label>
                                <input type="text" name="downPaymentValue" id="downPayment" class="numajaDesimal price2" value="{{number_format($header->DPValue,'2','.',',')}}">
                            </li>
                            <li>
                                <label for="longTerm">Long Term (day) *</label>
                                @if($header->isCash == 0 || $header->isCash == 2 || $header->isCash == 3)
                                <input type="text" class="numaja" name="longTerm" id="longTerm" value="0" disabled>
                                @else
                                <input type="text" class="numaja" name="longTerm" id="longTerm"  value='{{$header->LongTerm}}'>
                                @endif
                            </li>
                            <li>
                                <label for="payment">Pickup Type *</label>
                                @if($header->PickupType == 1)
                                <input checked type="radio" class="radio-tipe" id="pickupShipping" name="isShipping" value="1">Shipping
                                <input type="radio" class="radio-tipe" id="pickupCustomer" name="isShipping" value="0">Customer PickUp
                                @else
                                <input type="radio" class="radio-tipe" id="pickupShipping" name="isShipping" value="1">Shipping
                                <input checked type="radio" class="radio-tipe" id="pickupCustomer" name="isShipping" value="0">Customer PickUp
                                @endif
                            </li>
                            <!--                            <li>
                                                            <label for="warehouse">Warehouse *</label>
                                                            <select class="chosen-select choosen-modal warehouse" id="warehouseHeader" name="warehouse">
                                                                @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $war)
                                                                @if($war->InternalID == $header->WarehouseInternalID)
                                                                <option selected="selected" id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                                                    {{$war->WarehouseName;}}
                                                                </option>
                                                                @else
                                                                <option id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                                                    {{$war->WarehouseName;}}
                                                                </option>
                                                                @endif
                                                                @endforeach
                                                            </select>
                                                        </li>-->
                            <!--                            <li>
                                                            <label for="coa6">Salesman *</label>
                                                            <select class="chosen-select" id="salesman" style="" name="salesman" required>
                                                                @foreach(SalesMan::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $salesman)
                                                                @if($salesman->InternalID == $header->SalesManInternalID)
                                                                <option value="{{$salesman->InternalID}}" selected="selected">
                                                                    {{$salesman->SalesManName}}
                                                                </option>
                                                                @else
                                                                <option value="{{$salesman->InternalID}}">
                                                                    {{$salesman->SalesManName}}
                                                                </option>
                                                                @endif
                                                                @endforeach
                                                            </select>
                                                        </li>-->
                            <li>
                                <label for="purchasingCommission">Purchasing Commission *</label>
                                <input type="text" name="purchasingCommission" style="width: 200px;" value="{{number_format($header->PurchasingCommission,'2','.',',')}}" id="purchasingCommission" class="nominal numajaDesimal" data-validation="required">
                            </li>
                        </ul>
                        <ul class="pull-right">
                            <li>
                                <label for="currency">Currency *</label>
                                <select class="chosen-select choosen-modal currency" id="currencyHeader" name="currency">
                                    @foreach(Currency::orderBy('Default','desc')->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $cur)
                                    @if($cur->InternalID == $header->CurrencyInternalID)
                                    <option selected="selected" id="cur{{$cur->InternalID}}" value="{{$cur->InternalID.'---;---'.$cur->CurrencyName.'---;---'.$cur->Rate.'---;---'.$cur->Default}}">
                                        {{$cur->CurrencyName;}}
                                    </option>
                                    @else
                                    <option id="cur{{$cur->InternalID}}" value="{{$cur->InternalID.'---;---'.$cur->CurrencyName.'---;---'.$cur->Rate.'---;---'.$cur->Default}}">
                                        {{$cur->CurrencyName;}}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label for="rate">Rate *</label>
                                <input type="text" class="maxWidth rate numajaDesimal" name="rate" maxlength="" id="rate" value="{{$header->CurrencyRate}}" data-validation="required">
                            </li>
                            <li id="type">
                                <label for="type">Price Type *</label>
                                <select name="typeTax" id="typeTax">
                                    <option value="Exclude">Exclude</option>
                                    <option value="Include">Include</option>
                                </select>
                            </li>
                            <li>
                                <label for="remark">Remark</label>
                                <textarea name="remark" id="remark">{{$header->Remark}}</textarea>
                            </li>
                            <li>
                                <label for="longTerm">PO Customer *</label>
                                <input type="text" name="POCustomer" data-validation="required" value="{{$header->POCustomer}}">
                            </li>
                            <li>
                                <label for="longTerm">Delivery Terms</label>
                                <input type="text" name="DeliveryTerms" value="{{$header->DeliveryTerms}}">
                            </li>
                            <li>
                                <label for="longTerm">Delivery Time</label>
                                <input type="text" name="DeliveryTime" value="{{$header->DeliveryTime}}">
                            </li>
                            <li>
                                <div class="required">
                                    * Required
                                </div>
                            </li>
                            <li style="visibility: hidden">
                                <label for="vat">VAT *</label>
                                @if($header->VAT == 0)
                                <input style="width:15px; height: 15px;" type="checkbox" name="vat" id="vat" value="1"> Tax 10%
                                @else
                                <input style="width:15px; height: 15px;" type="checkbox" checked name="vat" id="vat" value="1"> Tax 10%
                                @endif
                            </li>
                        </ul>
                    </div>
                    <div class="tableadd journupdate">
                        <label>Description</label>
                        <table class="table master-data table-striped" id="table-salesorder-description-header" style="table-layout:fixed;">
                            <thead class="default-thead">
                                <tr>
                                    <th style="width: 25%;">Inventory</th>
                                    <th style="width: 10%;">Uom</th>
                                    <th style="width: 10%;">Qty</th>
                                    <th style="width: 15%;">Price</th>
                                    <th style="width: 10%;">Disc (%)</th>
                                    <th style="width: 10%;">Disc</th>
                                    <th style="width: 15%;">Subtotal</th>
                                    <th class="text-center" style="width: 5%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="row0">
                                    <td class="chosen-transaction">
                                        <input class="input-theme" type="text" id="InventoryDescription-0" placeholder="Inventory Name">
                                    </td>
                                    <td>
                                        <input type="text" class="input-theme uomDescription" id="uomDescription-0" placeholder="uom">
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth qtyDescription right numaja input-theme" maxlength="11" min="1" id="priceDescription-0-qty" value="1">
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth priceDescription right numajaDesimal" maxlength="" id="priceDescription-0" value="0">
                                    </td>
                                    <td class="text-right">
                                        <input disabled type="text" class="maxWidth discountDescription right numajaDesimal input-theme" id="priceDescription-0-discount" value="0.00">
                                    </td>
                                    <td class="text-right">
                                        <input disabled type="text" class="maxWidth discountNominalDescription right numajaDesimal input-theme" id="priceDescription-0-discountNominal" value="0.00">
                                    </td>
                                    <td id="priceDescription-0-qty-hitung" class="right">
                                        0.00
                                    </td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-green btn-sm" id="btn-addRow-description"><span class="glyphicon glyphicon-plus"></span></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <label>Detail</label>
                        <table class="table master-data  table-striped" id="table-salesorder-header" style="table-layout:fixed;">
                            <thead  class="default-thead">
                                <tr>
                                    <th style="width: 25%;">Inventory</th>
                                    <th style="width: 8%;">Uom</th>
                                    <th style="width: 7%;">Stock</th>
                                    <th style="width: 10%;">Description</th>
                                    <th style="width: 8%;">Qty</th>
                                    <th style="width: 10%;">Original Price</th>
                                    <th style="width: 15%;">Price</th>
                                    <th style="width: 7%;">Disc (%)</th>
                                    <th style="width: 10%;">Disc</th>
                                    <th style="width: 10%;">Subtotal</th>
                                    <th class="text-center" style="width: 5%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="row0">
                                    <td class="chosen-transaction">
                                        <input class="input-theme margbot10" type="text" id="searchInventory" tabindex="-1" title="Type Inventory Name or ID then 'Enter'" placeholder="Type Inventory Name or ID then 'Enter'">
                                        <div id="selectInventory">

                                        </div>
                                    </td>
                                    <td>
                                        <select id="uom-0" class="input-theme uom">

                                        </select>
                                    </td>
                                    <td class="text-right">
                                        <span class="maxWidth stock" id="price-0-stock" data-toggle="popover" data-placement="bottom" data-html="true">-</span>
                                    </td>
                                    <td>
                                        <select id="description-0" class="input-theme description">
                                            <?php $count = 1; ?>
                                            @foreach(SalesOrderDescription::where('SalesOrderInternalID',$header->InternalID)->get() as $description)
                                            <option value='rowDescription{{$count}}'>{{$description->InventoryText}}</option>
                                            <?php $count++; ?>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth qty right numaja input-theme" maxlength="11" min="1" id="price-0-qty" value="1">
                                    </td>
                                    <td class="text-right" id="oriprice-0">
                                        0.00
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth price right numajaDesimal input-theme" maxlength="" id="price-0" value="0">
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth discount right numajaDesimalMinus input-theme" id="price-0-discount" value="0.00">
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth discountNominal right numajaDesimalMinus input-theme" id="price-0-discountNominal" value="0.00">
                                    </td>
                                    <td id="price-0-qty-hitung" class="right">
                                        0.00
                                    </td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-green btn-sm" disabled="" id="btn-addRow"><span class="glyphicon glyphicon-plus"></span></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <!--table bawah yang berisi lengkap-->
                        <table class="table master-data" id="table-salesorder-description" style="table-layout:fixed;">
                            <thead>
                                <tr>
                                    <th style="width: 25%;">Inventory</th>
                                    <th style="width: 10%;">Uom</th>
                                    <th style="width: 10%;">Stock</th>
                                    <th style="width: 10%;">Qty</th>
                                    <th style="width: 15%;">Price</th>
                                    <th style="width: 10%;">Disc (%)</th>
                                    <th style="width: 10%;">Disc</th>
                                    <th style="width: 15%;">Subtotal</th>
                                    <th style="width: 14%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
                                $barisDesc = 1;
                                $barisDetail = 1;
                                $totalVAT = 0;
                                $descriptionn = array();
                                $arrCount = array();
                                $arrInv = array();
                                ?>
                                @foreach(SalesOrderDescription::where('SalesOrderInternalID',$header->InternalID)->get() as $description)
                                <?php array_push($descriptionn, "~" . $barisDesc); ?>
                                <tr style='background: #F1FFDB;' id='rowDescription{{$barisDesc}}'>
                                    <td class='chosen-uom'>
                                        <input type="hidden" class="inventoryDescription" style="width: 100px" id="inventoryDescription-{{$barisDesc}}" name="inventoryDescription[]" value="{{$description->InventoryText}}">{{$description->InventoryText}}
                                    </td>
                                    <td>
                                        <input type="text" class="uomDescription input-theme" name="uomDescription[]" id="uomDescription-{{$barisDesc}}" value='{{$description->UomText}}'>
                                    </td>
                                    <td class='text-right'>
                                        -
                                    </td>
                                    <td class='text-right'>
                                        <input type="text" class="maxWidth qtyDescription right input-theme" name="qtyDescription[]" id="priceDescription-{{$barisDesc}}-qty" value='{{$description->Qty}}'>
                                    </td>
                                    <td>
                                        <input type="text" class="maxWidth priceDescription right numajaDesimal input-theme" name="priceDescription[]" maxlength="" value="{{number_format($description->Price,'2','.',',')}}" id="priceDescription-{{$barisDesc}}">
                                    </td>
                                    <td class='text-right'>
                                        <input disabled type="text" class="maxWidth discountDescription right input-theme numajaDesimal" name="discountDescription[]" min="0" max="100" id="priceDescription-{{$barisDesc}}-discount" value="{{$description->Discount}}">
                                    </td>
                                    <td class='text-right'>
                                        <input disabled type="text" class="maxWidth discountNominalDescription right numajaDesimal input-theme" name="discountNominalDescription[]" id="priceDescription-{{$barisDesc}}-discountNominal" value="{{number_format($description->DiscountNominal,'2','.',',')}}">
                                    </td>
                                    <td class="right subtotalDescription" id="priceDescription-{{$barisDesc}}-qty-hitung">{{number_format(ceil($description->SubTotal),'2','.',',')}}</td>
                                    <td>
                                        <button class="btn btn-pure-xs btn-xs btn-deleteRow-Description margr5" type="button" data="rowDescription{{$barisDesc}}" text="rowSpec{{$barisDesc}}"><span class="glyphicon glyphicon-trash"></span></button>
                                        <button class="btn btn-pure-xs btn-xs btn-toggle-detail margr5" type="button" data-toshow="show" data="rowDescription{{$barisDesc}}"><span class="glyphicon glyphicon-eye-open"></span></button>
                                        <!--<button class="btn btn-pure-xs btn-xs btn-toggle-spec" type="button" data-toshow="show" data="rowSpec{{$barisDesc}}"><span class="glyphicon glyphicon-comment"></span></button>-->
                                    </td>
                                </tr>
                                <tr id='rowSpec{{$barisDesc}}' style="display: none">
                                    <td colspan='9' class='rowSpec{{$barisDesc}}'>
                                        <textarea onkeyup="textAreaAdjust(this)" name="spesifikasi[]" style="overflow:hidden; width: 100%;" class="input-theme rowSpec{{$barisDesc}}">{{$description->Spesifikasi}}</textarea>
                                    </td>
                                </tr>
                                @foreach(SalesOrderDetail::where('DescriptionInternalID',$description->InternalID)->get() as $detail)
                                <!--untuk non-parcel-->
                                @if($detail->SalesOrderParcelInternalID == 0)
                                <?php
                                $arrCount["rowDescription" . $barisDesc] = $barisDetail;
                                $arrInv["rowDescription" . $barisDesc][$arrCount["rowDescription" . $barisDesc]] = $detail->InventoryInternalID . "---;---inventory"
                                ?>
                                <tr id='row{{$barisDetail}}' class='rowDescription{{$barisDesc}}' data-parent='rowDescription{{$barisDesc}}'>
                                    <td class='chosen-uom'>
                                        <input type="hidden" class="inventory" style="width: 100px" id="inventory-{{$barisDetail}}" name="inventory{{$barisDesc}}[]" value="{{$detail->InventoryInternalID}}---;---inventory">{{Inventory::find($detail->InventoryInternalID)->InventoryID.' '.Inventory::find($detail->InventoryInternalID)->InventoryName}}
                                    </td>
                                    <td>
                                        <select id="uom-{{$barisDetail}}" name="uom{{$barisDesc}}[]" class="input-theme uom">
                                            @foreach (InventoryUom::where("InventoryInternalID", $detail->InventoryInternalID)->get() as $uom)
                                            <option value="{{$uom->UomInternalID}}" {{ ($uom->UomInternalID == $detail->UomInternalID) ? "selected" : ""}}>{{ $uom->Uom->UomID; }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class='text-right'>
                                        <?php
                                        $tamp = "";
                                        $tamp = getStockInventorySOHelper($detail->InventoryInternalID);
                                        $result = explode('---;---', $tamp);
                                        ?>
                                        <span class="maxWidth stock right input-theme" id="price-{{$barisDesc}}-stock">{{$result[0]}}</span>
                                    </td>
                                    <td class='text-right'>
                                        <input type="text" class="maxWidth qty right input-theme" name="qty{{$barisDesc}}[]" maxlength="11" min="1" value="{{number_format($detail->Qty,'0','.',',')}}" id="price-{{$barisDetail}}-qty">
                                    </td>
                                    <td>
                                        <input type="text" class="maxWidth price right numajaDesimal input-theme" name="price{{$barisDesc}}[]" maxlength="" value="{{number_format($detail->Price,'2','.',',')}}" id="price-{{$barisDetail}}">
                                    </td>
                                    <td class='text-right'>
                                        <input type="text" class="maxWidth discount right input-theme numajaDesimalMinus" name="discount{{$barisDesc}}[]" min="0" max="100" id="price-{{$barisDetail}}-discount" value="{{$detail->Discount}}">
                                    </td>
                                    <td class='text-right'>
                                        <input type="text" class="maxWidth discountNominal right numajaDesimalMinus input-theme" name="discountNominal{{$barisDesc}}[]" id="price-{{$barisDetail}}-discountNominal" value="{{number_format($detail->DiscountNominal,'2','.',',')}}">
                                    </td>
                                    <td class="right subtotal" id="price-{{$barisDetail}}-qty-hitung">{{number_format(ceil($detail->SubTotal),'0','.',',')}}</td>
                                    <td>
                                        <button class="btn btn-pure-xs btn-xs btn-deleteRow" type="button" data="row{{$barisDetail}}" barang="inventory-{{$barisDetail}}"><span class="glyphicon glyphicon-trash"></span></button>
                                    </td>
                                </tr>
                                <?php $barisDetail++; ?>
                                {{'';$totalVAT += $detail->VAT}}
                                @endif <!--tutup if non parcel -->
                                @endforeach<!--tutup foreach quotation detail-->
                                <!--untuk parcel-->
                                @foreach(SalesOrderParcel::where('DescriptionInternalID',$description->InternalID)->get() as $parcel)
                                <tr id='row{{$barisDetail}}' class='rowDescription{{$barisDesc}}' data-parent='rowDescription{{$barisDesc}}'>
                                    <?php
                                    $arrCount["rowDescription" . $barisDesc] = $barisDetail;
                                    $arrInv["rowDescription" . $barisDesc][$arrCount["rowDescription" . $barisDesc]] = $parcel->ParcelInternalID . "---;---parcel"
                                    ?>
                                    <td class='chosen-uom'>
                                        <input type="hidden" class="inventory" style="width: 100px" id="inventory-{{$barisDetail}}" name="inventory{{$barisDesc}}[]" value="{{$parcel->ParcelInternalID}}---;---parcel">{{Parcel::find($parcel->ParcelInternalID)->ParcelID.' '.Parcel::find($parcel->ParcelInternalID)->ParcelName}}
                                    </td>
                                    <td>
                                        <select id="uom-{{$barisDetail}}" readonly name="uom[]" class="input-theme uom">
                                            <option value="0" >-</option>
                                        </select>
                                    </td>
                                    <td class='text-right'>
                                        -
                                    </td>
                                    <td class='text-right'>
                                        <input type="text" class="maxWidth qty right input-theme" name="qty{{$barisDesc}}[]" maxlength="11" min="1" value="{{number_format($parcel->Qty,'0','.',',')}}" id="price-{{$barisDetail}}-qty"></td>
                                    <td>
                                        <input type="text" class="maxWidth price right numajaDesimal input-theme" name="price{{$barisDesc}}[]" maxlength="" value="{{number_format($parcel->Price,'2','.',',')}}" id="price-{{$barisDetail}}">
                                    </td>
                                    <td class='text-right'>
                                        <input type="text" class="maxWidth discount right input-theme numajaDesimalMinus" name="discount{{$barisDesc}}[]" min="0" max="100" id="price-{{$barisDetail}}-discount" value="{{$parcel->Discount}}">
                                    </td>
                                    <td class='text-right'>
                                        <input type="text" class="maxWidth discountNominal right numajaDesimalMinus input-theme" name="discountNominal{{$barisDesc}}[]" id="price-{{$barisDetail}}-discountNominal" value="{{number_format($parcel->DiscountNominal,'2','.',',')}}">
                                    </td>
                                    <td class="right subtotal" id="price-{{$barisDetail}}-qty-hitung">{{number_format(ceil($parcel->SubTotal),'2','.',',')}}</td>
                                    <td>
                                        <button class="btn btn-pure-xs btn-xs btn-deleteRow" type="button" data="row{{$barisDetail}}" barang="inventory-{{$barisDetail}}"><span class="glyphicon glyphicon-trash"></span></button>
                                    </td>
                                </tr>
                                <?php $barisDetail++; ?>
                                {{'';$totalVAT += $parcel->VAT}}
                                @endforeach<!--tutup foreach quotation parcel-->
                                <?php $barisDesc++; ?>
                                @endforeach <!--tutup foreach quotation description-->
                                <?php $i++; ?>
                                @if($totalVAT != 0)
                                {{'';$totalVAT = $totalVAT - $header->DiscountGlobal*0.1;}}
                                @endif
                            </tbody>
                        </table>
                    </div><!---- end div tableadd---->
                    <input type="hidden" name="grandTotalValue" value="0" id="grandTotalValue">

                    <table class="pull-left">
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td><br></td>
                        </tr>
                        <tr>
                            <td><p>Created by {{User::where('UserID', $header->UserRecord)->first()->UserName.' '.date('d-m-Y H:i:s', strtotime($header->dtRecord))}}</p></td>
                        </tr>
                        <tr>
                            @if($header->UserModified != '0')
                            <td><p>Modified by {{User::where('UserID', $header->UserModified)->first()->UserName.' '.date('d-m-Y H:i:s', strtotime($header->dtModified))}}</p></td>
                            @else
                            <td><p>Modified by -</p></td>
                            @endif
                        </tr>
                    </table>
                    <table class="pull-right">
                        <tr>
                            <td><h5 class="right margr10 h5total"><b>Total</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="total"></b></h5></td>
                        </tr>
                        <tr >
                            <td><h5 class="right margr10 h5total"><b>Discount</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total new" style="width: 150px;"><input type="text" style="width: 120px;" class="maxWidth right numaja discountGlobal" name="DiscountGlobal" maxlength="" id="discountGlobal" value="{{number_format($header->DiscountGlobal, '2', '.', ',')}}"></h5></td>
                        </tr>
                        <tr>
                            <td><h5 class="right margr10 h5total"><b>Grand Total</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="grandTotal"></b></h5></td>
                        </tr>

                        <tr>
                            <td><h5 class="right margr10 h5total hidevat"><b>Tax</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b id="tax"></b></h5></td>
                        </tr>
                        <tr>
                            <td><h5 class="right margr10 h5total hidevat"><b>Grand Total(tax)</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b id="grandTotalAfterTax"></b></h5></td>
                        </tr>
                    </table>
                </div><!---- end div tableadd---->
            </div><!---- end div tabwrap---->
            <div class="btnnest pull-right">
                <button class="btn btn-green btn-sm btn-save" id="btn-save"> Save </button>
            </div>
        </form>
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop

@section('modal')
<div class="modal fade modal-quotation" id="insertQuotation" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insert Quotation</h4>
            </div>
            <form action="{{Route("salesOrderNew")}}" method="GET" class="action">
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="insertQuotation" id="jenisQuotation" name="jenis">
                            <li>
                                <label for="customerSales">Customer</label> *
                            </li>
                            <li>
                                <select class="chosen-select choosen-modal input-theme" id="customerSales" style="" name="customerSales">
                                    @foreach(QuotationHeader::where("VAT","!=",Auth::user()->SeeNPPN)->distinct()->select('CustomerName')->groupBy('CustomerName')->get() as $cus)
                                    <option value="{{$cus->CustomerName}}">
                                        {{$cus->CustomerName}}
                                    </option>
                                    @endforeach
                                    @foreach(Coa6::where('CompanyInternalID', Auth::user()->Company->InternalID)->where('Type', 'C')->get() as $coa6)
                                    <option value="{{$coa6->InternalID}}">
                                        {{$coa6->ACC6ID.' | '.$coa6->ACC6Name}}
                                    </option>
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label for="sales">Quotation ID</label> *
                            </li>
                            <li>
                                <select multiple class="chosen-select choosen-modal input-theme input-stretch" id="quotation" style="" name="Quotation[]">
                                    {{'';$hitung=0;}}
                                    @foreach(QuotationHeader::where("VAT","!=",Auth::user()->SeeNPPN)->where('CompanyInternalID', Auth::user()->Company->InternalID)->OrderBy('QuotationDate', 'desc')->get() as $quotation)
                                    @if(checkQuotation($quotation->InternalID))
                                    @if($quotation->TypeCustomer == 0)
                                    <option class="optionQuotation" id="cust{{$quotation->ACC6InternalID}}" value="{{$quotation->QuotationID}}">
                                        @else
                                    <option class="optionQuotation" id="cust{{$quotation->CustomerName}}" value="{{$quotation->QuotationID}}">
                                        @endif
                                        {{$quotation->QuotationID.' | '.date( "d-m-Y", strtotime($quotation->QuotationDate))}}
                                    </option>
                                    {{'';$hitung=1;}}
                                    @endif
                                    @endforeach
                                </select>
                            </li>
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="btn-submit-quotation" @if($hitung==0){{'disabled'}}@endif class="btn btn-green">Submit</button>
                        <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="r_summary" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Summary Report</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="" target="_blank">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='summarySalesOrder'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateReport" data-validation="required">
                            </li>
                        </ul>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="r_print" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Tax</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="" target="_blank">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='printSalesOrder'>
                            <input type="hidden" name="internalID" id="internalID" />
                            <li>
                                <label for="type">Type Tax</label> *
                            </li>
                            <li>
                                <select class="form-control" name="type">
                                    <option value="include">Include</option>
                                    <option value="exclude">Exclude</option>
                                </select>
                            </li>
                        </ul>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosen.jquery.js')}}" type="text/javascript"></script>
<script>
                                            var getUomThisInventory = "<?php echo Route("getUomThisInventory") ?>";
                                            var getCustomerManager = "<?php echo Route("getCustomerManager") ?>";
                                            var getPriceDefault = "<?php echo Route("getPriceDefault") ?>";
                                            var getPriceThisParcel = "<?php echo Route("getPriceThisParcel") ?>";
                                            var getPriceRangeThisInventorySO = "<?php echo Route("getPriceRangeThisInventorySO") ?>";
                                            var getSearchResultInventoryForSO = "<?php echo Route("getSearchResultInventoryForSO") ?>";
                                            var getStockInventorySO = "<?php echo Route("getStockInventorySO") ?>";
                                            var getPriceRangeSO = "<?php echo Route("getPriceRangeSO") ?>";
                                            var textSelect = '';
                                            var bariss = '<?php echo $barisDesc; ?>';
                                            var baris = '<?php echo $barisDetail; ?>';
                                            var description = '<?php echo json_encode($descriptionn); ?>';
                                            var arrCount = '<?php echo json_encode($arrCount) ?>';
                                            var arrInven = '<?php echo json_encode($arrInv) ?>';
</script><script>
    var salesOrderDataBackup = '<?php echo Route('salesOrderDataBackup', Input::get('coa6') . '---;---' . Input::get('typePayment') . '---;---' . Input::get('typeTax') . '---;---' . Input::get('startDate') . '---;---' . Input::get('endDate')) ?>';
</script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script src="{{Asset('js/chosenNewJournal.jquery.js')}}" type="text/javascript"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-penjualan-add/salesOrder.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-penjualan-add/salesOrderUpdate.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-penjualan-add/salesOrderUpdateDescription.js')}}"></script>
@stop
