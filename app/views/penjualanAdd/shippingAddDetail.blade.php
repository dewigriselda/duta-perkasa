@extends('template.header-footer')

@section('title')
Shipping
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
@if(checkModul('O05'))
<style>
    .li_detail{
        clear: both;
        padding-top: 10px;
    }
    .label_detail{
        float: left;
    }
    .div_detail{
        float: left; width: 65%
    }
</style>
<?php $element = 'div'; ?>
@endif
<style>
    .li_detail{
    }
    .label_detail{
    }
    .div_detail{
    }
</style>
<?php $element = 'span'; ?>
@stop

@section('nav')

@stop

@section('content')
@if(myCheckIsEmpty('SalesOrder;Default;Slip;DepartmentDefault'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one Sales Order, Default COA, and Slip to insert shipping.
</div>
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showShipping')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Shipping</a>
                <a href="{{route('shippingDetail',$header->ShippingID)}}" type="button" class="btn btn-sm btn-pure">Detail {{$header->ShippingID}}</a>
            </div>
            <div class="btn-group margr5">
                <button type="button" <?php if (myCheckIsEmpty('SalesOrder;Default;Slip;DepartmentDefault')) echo 'disabled'; ?> class="btn btn-green btn-sm dropdown-toggle" data-target="#insertShipping" data-toggle="modal" role="dialog"
                        aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
            </div>
            <div class="btn-group margr5">
                <button type="button" <?php if (myCheckIsEmpty('SalesOrder;Default;Slip;DepartmentDefault')) echo 'disabled'; ?> class="btn btn-green btn-sm dropdown-toggle" data-target="#insertPacking" data-toggle="modal" role="dialog"
                        aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New Packing </button>
            </div>
            <button id="search-button" <?php if (myCheckIsEmpty('Shipping')) echo 'disabled'; ?>  class="btn btn-green btn-sm margr5"><span class="glyphicon glyphicon-search"></span> Search </button>
            @if(checkModul('O04'))
            <button type="button" class="btn btn-green margr5" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rSummary">
                <span class="glyphicon glyphicon-file"></span> Summary Report</button>
            <button type="button" class="btn btn-green margr5" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rDetail">
                <span class="glyphicon glyphicon-file"></span> Detail Report</button>
            @endif
            <a href="{{Route('shippingUpdate',$header->ShippingID)}}">
                <button id="btn-{{$header->ShippingID}}-update"
                        class="btn btn-green btn-sm margr5">
                    <span class="glyphicon glyphicon-edit"></span> Edit
                </button>
            </a>
            @if(checkModul('O04'))
            <div class="btn-group">
                <a href="{{Route('shippingPrint',$header->ShippingID)}}" class="btn btn-green btn-sm" id="btn-{{$header->ShippingID}}-print" target='_blank' style="margin-right: 0px !important;">
                    <span class="glyphicon glyphicon-print"></span>
                    Print
                </a>
<!--                <a href="{{Route('shippingInternalPrint',$header->ShippingID)}}" class="btn btn-green btn-sm" id="btn-{{$header->ShippingID}}-print" target='_blank' style="margin-right: 0px !important;">
                    <span class="glyphicon glyphicon-print"></span>
                    Internal
                </a>-->
            </div>
            @endif
            {{'';$so = SalesOrderHeader::find($header->SalesOrderInternalID)}}
            @if (checkSalesAdd($so->InternalID) && $so->Closed == 0)
            <a href="{{Route('salesNew',$so->SalesOrderID)}}" id="btn-{{$so->SalesOrderID}}-print" target='_blank' style="margin-right: 0px !important;">
                <button type="button" class="btn btn-green">
                    <span class="glyphicon glyphicon-plus"></span> Create Sales </span>
                </button>
            </a>
            @endif
        </div>
        <!-- <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Search</h4>
            </div>
            <div class="tableadd">
                <ul class="searchmenu">
                    <form method="GET" action="{{Route('showShipping')}}">
                         <li>
                            <label for="coa6">Customer</label>
                            <br>
                            <select class="chosen-select" id="coa6" style="" name="coa6">
                                <option value="-1">All Customer</option>
                                @foreach(Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $coa6)
                                <option value="{{$coa6->InternalID}}">
                                    {{$coa6->ACC6ID.' '.$coa6->ACC6Name}}
                                </option>
                                @endforeach
                            </select>
                        </li>
                        <li><label for="typePayment">Payment Type</label>
                            <br>
                            <select name="typePayment" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Cash</option>
                                <option value="1">Credit</option>
                            </select>
                        </li>
                        <li><label for="typeTax">Tax Type</label>
                            <br>
                            <select name="typeTax" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Non Tax</option>
                                <option value="1">Tax</option>
                            </select>
                        </li>
                        <li><label for="date">Start Date</label>
                            <br><input id="startDate" name="startDate" type="text" autocomplete="off">
                        </li>
                        <li><label for="date">End Date</label>
                            <br><input id="endDate" name="endDate" type="text" autocomplete="off">
                        </li>
                        <li><br>
                            <button type="submit" class="btn btn-green btn-sm">Search <span class="glyphicon glyphicon-search"></span></button>
                        </li>
                        <li><br>
                            <button id="cancel" type="reset"  class="btn btn-pure btn-sm">Cancel <span class="glyphicon glyphicon-remove"></span></button>
                        </li>
                    </form>
                </ul>
            </div><!---- end div tableadd---->
        <!--</div><!---- end div tabwrap---->
        @include('template.searchComponentTransactionModule')

        <div class="tabwrap">
            <div class="tabhead">
                <h4 class="headtitle">{{'Shipping '.$header->ShippingID}}</h4>
            </div>
            <div class="tableadd">
                <div class="headinv new">
                    <ul @if(checkModul('O05')) class="pull-left" style="width: 540px;" @else class="pull-left" @endif>
                         <li>
                            <label for="orderID" style="float: left;">Order ID</label>
                            <{{$element}} class="div_detail">{{$header->SalesOrder->SalesOrderID}}</{{$element}}>
                        </li>
                        <li class="li_detail">
                            <label for="date" style="float: left;">Date</label>
                            <{{$element}} class="div_detail">{{date( "d-m-Y", strtotime($header->ShippingDate))}}</{{$element}}>
                        </li>
                        <li class="li_detail">
                            <label for="customer" style="float: left;">Customer</label>
                            <{{$element}} class="div_detail"><?php
                            $coa6 = ShippingAddHeader::find($header->InternalID)->coa6;
                            echo $coa6->ACC6ID . ' ' . $coa6->ACC6Name
                            ?>
                            </{{$element}}>
                        </li>
<!--                        <li class="li_detail">
                            <label for="longTerm" style="float: left;">Payment</label>
                            @if($header->isCash == 0)
                            <{{$element}} class="div_detail">{{'Cash'}}</{{$element}}>
                            @else
                            <{{$element}} class="div_detail">{{'Credit'}}</{{$element}}>
                            @endif
                        </li>-->
                        @if($header->isCash != 0)
<!--                        <li class="li_detail">
                            <label for="longTerm" style="float: left;">Due Date</label>
                            <{{$element}} class="div_detail">{{date( "d-m-Y", strtotime("+".$header->LongTerm." day",strtotime($header->ShippingDate)))}}</{{$element}}>
                        </li>-->
                        @endif
                        <li class="li_detail">
                            <label for="warehouse" style="float: left;">Warehouse</label>
                            <{{$element}} class="div_detail">{{$header->Warehouse->WarehouseName}}</{{$element}}>
                        </li>
                    </ul>
<!--                    <ul @if(checkModul('O05')) class="pull-left" style="width: 360px;" @else class="pull-right" @endif>
                         <li>
                            <label for="payment" style="float: left;">Status</label>
                            <?php
                            $tampReceiv = 'Completed';
                            foreach (ShippingAddHeader::getShippingReceivable() as $receiv) {
                                if ($receiv->ID == $header->ShippingID) {
                                    $tampReceiv = 'Uncompleted';
                                }
                            }
                            ?>
                            <{{$element}} class="div_detail">{{$tampReceiv}}</{{$element}}>
                        </li>
                        
                        <li class="li_detail">
                            <label for="VAT" style="float: left;">VAT</label>
                            @if($header->VAT == 0)
                            <{{$element}} class="div_detail">{{'Non Tax'}}</{{$element}}>
                            @else
                            <{{$element}} class="div_detail">{{'Tax'}}</{{$element}}>
                            @endif
                        </li>
                       
                    </ul>-->
                    @if(checkModul('O05'))
                    <ul class="pull-left" style="width: 540px;">
                        <!--                        <li>
                                                    <label for="transactionType" style="float: left;">Transaction</label>
                                                    @if($header->TransactionType == 1)
                                                    <div style="float: left; width: 65%">For who is not collect PPN</div>
                                                    @elseif($header->TransactionType == 2)
                                                    <div style="float: left; width: 65%">For Chamberlain</div>
                                                    @elseif($header->TransactionType == 3)
                                                    <div style="float: left; width: 65%">Except Chamberlain</div>
                                                    @elseif($header->TransactionType == 4)
                                                    <div style="float: left; width: 65%">DPP other value</div>
                                                    @elseif($header->TransactionType == 6)
                                                    <div style="float: left; width: 65%">Other handover, include handover to foreigner tourist in the event of VAT refund</div>
                                                    @elseif($header->TransactionType == 7)
                                                    <div style="float: left; width: 65%">Handover PPN is not collect</div>
                                                    @elseif($header->TransactionType == 8)
                                                    <div style="float: left; width: 65%">Handover PPN Freed</div>
                                                    @elseif($header->TransactionType == 9)
                                                    <div style="float: left; width: 65%">Handover Assets (Pasal 16D UU PPN)</div>
                                                    @endif
                                                </li>
                                                <li class="li_detail">
                                                    <label for="replacement" style="float: left;">Replacement</label>
                                                    @if($header->Replacement == 1)
                                                    <div style="float: left; width: 65%">Tax Replacement</div>
                                                    @else
                                                    <div style="float: left; width: 65%">Non Tax Replacement</div>
                                                    @endif
                                                </li>
                                                <li class="li_detail">
                                                    <label for="Taxnumber" style="float: left;">Tax Number</label>
                                                    <div style="float: left; width: 65%">{{$header->TaxNumber}}</div>
                                                </li>
                                                <li class="li_detail">
                                                    <label for="Taxmonth" style="float: left;">Tax Month</label>
                                                    <div style="float: left; width: 65%">{{date('F',strtotime('2015-'.$header->TaxMonth.'-01'))}}</div>
                                                </li>
                                                <li class="li_detail">
                                                    <label for="TaxYear" style="float: left;">Tax Year</label>
                                                    <div style="float: left; width: 65%">{{$header->TaxYear}}</div>
                                                </li>-->
                        <li class="li_detail">
                            <label for="Vehicle" style="float: left;">PO Customer</label>
                            <div style="float: left; width: 65%">{{SalesOrderHeader::find($header->SalesOrderInternalID)->POCustomer}}</div>
                        </li>
                        <li class="li_detail">
                            <label for="Vehicle" style="float: left;">Number of Vehicle</label>
                            <div style="float: left; width: 65%">{{$header->NumberVehicle}}</div>
                        </li>
                        <li class="li_detail">
                            <label for="TaxYear" style="float: left;">Driver Name</label>
                            <div style="float: left; width: 65%">{{$header->DriverName}}</div>
                        </li>
                         <li class="li_detail">
                            <label for="" style="float: left;">Remark</label>
                            <{{$element}} class="div_detail">{{$header->Remark}}</{{$element}}>
                        </li>
                    </ul>
                    @endif
                </div>
                <div class="padrl10">
                    <table class="table master-data" id="table-salesorder-description" style="table-layout:fixed;">
                        <thead>
                            <tr>
                                <th style="width: 25%;">Inventory</th>
                                <th style="width: 10%;">Uom</th>
                                <th style="width: 10%;">Qty</th>
                                <th style="width: 14%;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $barisDesc = 1;
                            ?>
                            @foreach(ShippingAddDescription::where('ShippingInternalID',$header->InternalID)->get() as $description)
                            <tr style='background: #F1FFDB;' id='rowDescription{{$barisDesc}}'>
                                <td class='chosen-uom'>
                                    {{$description->InventoryText}}
                                </td>
                                <td>
                                    {{$description->UomText}}
                                </td>
                                <td class='text-right'>
                                    {{$description->Qty}}
                                </td>
                                <td>
                                    <button class="btn btn-pure-xs btn-xs btn-toggle-detail margr5" type="button" data-toshow="show" data="rowDescription{{$barisDesc}}"><span class="glyphicon glyphicon-eye-open"></span></button>
                                    <button class="btn btn-pure-xs btn-xs btn-toggle-spec" type="button" data-toshow="show" data="rowSpec{{$barisDesc}}"><span class="glyphicon glyphicon-comment"></span></button>
                                </td>
                            </tr>
<!--                            <tr id='rowSpec{{$barisDesc}}'>
                                <td colspan='4' class='rowSpec{{$barisDesc}}' style="text-align: left">
                                    {{nl2br($description->Spesifikasi)}}
                                </td>
                            </tr>-->
                            <?php
                            $barisDetail = 1;
                            ?>
                            @foreach(ShippingAddDetail::where('DescriptionInternalID',$description->InternalID)->get() as $detail)
                            <!--untuk non-parcel-->
                            @if($detail->ShippingParcelInternalID == 0)
                            <tr id='row{{$barisDetail}}' class='rowDescription{{$barisDesc}}'>
                                <td class='chosen-uom'>
                                    <input type="hidden" class="inventory" style="width: 100px" id="inventory-{{$barisDetail}}" name="inventory{{$barisDesc}}[]" value="{{$detail->InventoryInternalID}}---;---inventory">{{Inventory::find($detail->InventoryInternalID)->InventoryID.' '.Inventory::find($detail->InventoryInternalID)->InventoryName}}
                                </td>
                                <td>
                                    {{Uom::find($detail->UomInternalID)->UomID}}
                                </td>
                                <td class='text-right'>
                                    {{number_format($detail->Qty,'0','.',',')}}
                                </td>
                                <td>
                                    -
                                </td>
                            </tr>
                            <?php
                            $barisDetail++;
                            ?>
                            @endif
                            @endforeach
                            <!--untuk parcel-->
                            @foreach(ShippingAddParcel::where('DescriptionInternalID',$description->InternalID)->get() as $parcel)
                            <tr id='row{{$barisDetail}}' class='rowDescription{{$barisDesc}}'>
                                <td class='chosen-uom'>
                                    {{Parcel::find($parcel->ParcelInternalID)->ParcelID.' '.Parcel::find($parcel->ParcelInternalID)->ParcelName}}
                                </td>
                                <td>
                                    -
                                </td>
                                <td class='text-right'>
                                    {{number_format($parcel->Qty,'0','.',',')}}
                                </td>
                                <td>
                                    -
                                </td>
                            </tr>
                            <?php
                            $barisDetail++;
                            ?>
                            @endforeach
                            <?php $barisDesc++; ?>
                            @endforeach
                        </tbody>
                    </table>

                    <table class="pull-left">
                        <tr>
                            <td></td>
                        </tr>
                        <tr>
                            <td><p>Created by {{User::where('UserID', $header->UserRecord)->first()->UserName.' '.date('d-m-Y H:i:s', strtotime($header->dtRecord))}}</p></td>
                        </tr>
                        <tr>
                            @if($header->UserModified != '0')
                            <td><p>Modified by {{User::where('UserID', $header->UserModified)->first()->UserName.' '.date('d-m-Y H:i:s', strtotime($header->dtModified))}}</p></td>
                            @else
                            <td><p>Modified by -</p></td>
                            @endif
                        </tr>
                    </table>
                </div><!---- end div padrl10---->
            </div><!---- end div tableadd---->
        </div><!---- end div tabwrap---->
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->

@stop


@section('modal')
<div class="modal fade" id="insertPacking" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insert Packing List</h4>
            </div>
            <form action="" method="post" class="action" id="form-so">
                {{'';$hitung = 0;}}
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="insertPacking" id="jenisShipping" name="jenis">
                            <li>
                                <label for="sales">Sales Order ID</label> *
                            </li>
                            <input class="input-theme margbot10" type="text" id="searchSalesOrder2" title="Type Sales Order Name or ID then 'Enter'" placeholder="Type Sales Order Name or ID then 'Enter'">
                            <li id="selectSalesOrder2">

                            </li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btn-add-so2" @if($hitung==0){{'disabled'}}@endif class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="insertShipping" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insert Shipping</h4>
            </div>
            <form action="" method="post" class="action" id="form-so">
                {{'';$hitung = 0;}}
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="insertShipping" id="jenisShipping" name="jenis">
                            <li>
                                <label for="sales">Sales Order ID</label> *
                            </li>
                            <input class="input-theme margbot10" type="text" id="searchSalesOrder" title="Type Sales Order Name or ID then 'Enter'" placeholder="Type Sales Order Name or ID then 'Enter'">
                            <li id="selectSalesOrder">

                            </li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btn-add-so" @if($hitung==0){{'disabled'}}@endif class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="r_summary" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Summary Report</h4>
            </div>
            <form action="" method="post" class="action" id="" target="_blank">
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='summaryShipping'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateReport" data-validation="required">
                            </li>
                        </ul>
                    </div>
                    <div class="required">
                        * Required
                    </div>
                </div>
                <div class="modal-footer">
                    <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                    <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script><script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script>
var getResultSearchSO = "<?php echo Route("getResultSearchShippingSO") ?>";
var getResultSearchSO2 = "<?php echo Route("getResultSearchShippingSO2") ?>";</script>
<script>
    var shippingDataBackup = '<?php echo Route('shippingDataBackup', Input::get('coa6') . '---;---' . Input::get('typePayment') . '---;---' . Input::get('typeTax') . '---;---' . Input::get('startDate') . '---;---' . Input::get('endDate')) ?>';
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-penjualan-add/shippingAdd.js')}}"></script>
<!--<script type="text/javascript" src="{{Asset('js/entry-js-penjualan-add/packing.js')}}"></script>-->
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
@stop
