@extends('template.header-footer')

@section('title')
Shipping
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<style>
    .help-block {
        margin-left: 30% !important;
    }
    .headinv .chosen-single{
        width: 200px !important;
    }
    .headinv .chosen-drop{
        width: 200px !important;
    }
</style>
@stop

@section('nav')

@stop

@section('content')
@if(myCheckIsEmpty('SalesOrder;Default;Slip;DepartmentDefault'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one Sales Order, Default COA, and Slip to insert shipping.
</div>
@endif
@if(isset($messages))
@if($messages == 'gagalInsert')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New shipping has been inserted.
</div>
<input type="hidden" name="print" id="print" value="{{$print}}">
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showShipping')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Shipping</a>
                <a href="{{route('shippingNew',$header->SalesOrderID)}}" type="button" class="btn btn-sm btn-pure">New Shipping from {{$header->SalesOrderID}}</a>
            </div>
            <div class="btn-group margr5">
                <button type="button" <?php if (myCheckIsEmpty('SalesOrder;Default;Slip;DepartmentDefault')) echo 'disabled'; ?> class="btn btn-green btn-sm dropdown-toggle" data-target="#insertShipping" data-toggle="modal" role="dialog"
                        aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
            </div>
            <div class="btn-group margr5">
                <button type="button" <?php if (myCheckIsEmpty('SalesOrder;Default;Slip;DepartmentDefault')) echo 'disabled'; ?> class="btn btn-green btn-sm dropdown-toggle" data-target="#insertPacking" data-toggle="modal" role="dialog"
                        aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New Packing </button>
            </div>
            <button id="search-button" <?php if (myCheckIsEmpty('Shipping')) echo 'disabled'; ?>  class="btn btn-green btn-sm margr5"><span class="glyphicon glyphicon-search"></span> Search </button>
            @if(checkModul('O04'))
            <button type="button" class="btn btn-green margr5" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rSummary">
                <span class="glyphicon glyphicon-file"></span> Summary Report</button>
            <button type="button" class="btn btn-green" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rDetail">
                <span class="glyphicon glyphicon-file"></span> Detail Report</button>
            @endif
        </div>
        <!-- <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Search</h4>
            </div>
            <div class="tableadd">
                <ul class="searchmenu">
                    <form method="GET" action="">
                        <li>
                            <label for="coa6">Customer</label>
                            <br>
                            <select class="chosen-select" id="coa6" style="" name="coa6">
                                <option value="-1">All Customer</option>
                                @foreach(Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $coa6)
                                <option value="{{$coa6->InternalID}}">
                                    {{$coa6->ACC6ID.' '.$coa6->ACC6Name}}
                                </option>
                                @endforeach
                            </select>
                        </li>
                        <li><label for="typePayment">Payment Type</label>
                            <br>
                            <select name="typePayment" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Cash</option>
                                <option value="1">Credit</option>
                            </select>
                        </li>
                        <li><label for="typeTax">Tax Type</label>
                            <br>
                            <select name="typeTax" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Non Tax</option>
                                <option value="1">Tax</option>
                            </select>
                        </li>
                        <li><label for="date">Start Date</label>
                            <br><input id="startDate" name="startDate" type="text" autocomplete="off">
                        </li>
                        <li><label for="date">End Date</label>
                            <br><input id="endDate" name="endDate" type="text" autocomplete="off">
                        </li>
                        <li><br>
                            <button type="submit" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-search"></span> Search </button>
                        </li>
                        <li><br>
                            <button id="cancel" type="reset"  class="btn btn-pure btn-sm"><span class="glyphicon glyphicon-remove"></span> Cancel </button>
                        </li>
                    </form>
                </ul>
            </div><!---- end div tableadd---->
        <!--</div><!---- end div tabwrap---->
        @include('template.searchComponentTransactionModule')

        <form method="POST" action="" id='form-insertShippingAdd' class="formshipping">
            <input type='hidden' name='SalesOrderInternalID' id="salesOrderInternalID" value='{{$header->InternalID}}'>
            <div class="tabwrap">
                <div class="tabhead">
                    <h4 class="headtitle">Shipping <span id="shippingID">{{Salesheader::getNextIDShipping($shipping)}}</span></h4>
                </div>
                <div class="tableadd">
                    <div class="headinv new">
                        <ul @if(checkModul('O05')) class="pull-left" style="width: 540px;" @else class="pull-left"  @endif>
                             <li>
                                <label for="salesOrder">Order ID</label>
                                <span>{{$header->SalesOrderID}}</span>
                            </li>
                            <li>
                                <label for="date">Date *</label>
                                <input id="date" name="date" type="text" style="width: 200px;" autocomplete="off" data-validation="required">
                            </li>
                            <li>
                                <label for="customer">Customer</label>
                                <span><?php
                                    $coa6 = SalesOrderHeader::find($header->InternalID)->coa6;
                                    echo $coa6->ACC6ID . ' ' . $coa6->ACC6Name
                                    ?></span>
                            </li>
                            <li>
                                <label for="longTerm">Payment</label>
                                @if($header->isCash == 0)
                                <span>{{'Cash'}}</span>
                                @elseif($header->isCash == 1)
                                <span>{{'Credit'}}</span>
                                @elseif($header->isCash == 2)
                                <span>{{'CBD'}}</span>
                                @elseif($header->isCash == 3)
                                <span>{{'Deposit'}}</span>
                                @elseif($header->isCash == 4)
                                <span>{{'Down Payment'}}</span>
                                @endif
                            </li>
                            @if($header->isCash == 0)
                            <!--                            <li>
                                                            <label for="slip">Slip number *</label>
                                                            <select class="chosen-select" id="slip" style="" name="slip">
                                                                @foreach(Slip::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $slip)
                                                                <option value="{{$slip->InternalID}}">
                                                                    {{$slip->SlipID.' '.$slip->SlipName}}
                                                                </option>
                                                                @endforeach
                                                            </select>
                                                        </li>-->
                            @else
                            <!--                            <li>
                                                            <label for="longTerm">Due Date</label>
                                                            <span>{{date( "d-m-Y", strtotime("+".$header->LongTerm." day",strtotime($header->SalesOrderDate)))}}</span>
                                                        </li>-->
                            @endif
                            <li>
                                <label for="warehouse">Warehouse *</label>
                                <select class="chosen-select choosen-modal currency" id="warehouseHeader" name="warehouse">
                                    @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->where("Type",Auth::user()->WarehouseCheck)->get() as $war)
                                    <option {{'';if($war->InternalID == $header->WarehouseInternalID) echo 'selected';}} id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                        {{$war->WarehouseName;}}
                                    </option>
                                    @endforeach
                                </select>
                            </li>
                        </ul>
                        <!--                        <ul @if(checkModul('O05')) class="pull-left" style="width: 360px;" @else class="pull-right" @endif>
                                                     
                                                    <li>
                                                        <label for="currency">Currency</label>
                                                        <span>{{'';$currency = Currency::find($header->CurrencyInternalID); $currencyName = $currency->CurrencyName; echo $currencyName}}</span>
                                                    </li>
                                                    <li>
                                                        <label for="rate">Rate</label>
                                                        <span>{{number_format($header->CurrencyRate,'2','.',',')}}</span>
                                                    </li>
                                                    <li>
                                                        <label for="VAT">VAT</label>
                                                        @if($header->VAT == 0)
                                                        <span>{{'Non Tax'}}</span>
                                                        <input type="hidden" id="taxShipping" value="0">
                                                        @else
                                                        <span>{{'Tax'}}</span>
                                                        <input type="hidden" id="taxShipping" value="1">
                                                        @endif
                                                    </li>
                                                    
                                                    @if(!checkModul('O05'))
                                                    <li>
                                                        <div class="required">
                                                            * Required
                                                        </div>
                                                    </li>
                                                    @endif
                                                </ul>-->

                        @if(checkModul('O05'))
                        <ul class="pull-left" style="width: 540px;">
                            <!--                            <li>
                                                            <label for="transactiontype">Transaction</label>
                                                            <select class="chosen-select choosen-modal currency" id="transactionType" name="TransactionType">
                                                                <option value="1"> For who is not collect PPN </option>
                                                                <option value="2"> For Chamberlain </option>
                                                                <option value="3"> Except Chamberlain </option>
                                                                <option value="4"> DPP other value </option>
                                                                <option value="6"> Other handover, include handover to foreigner tourist in the event of VAT refund </option>
                                                                <option value="7"> Handover PPN is not collect</option>
                                                                <option value="8"> Handover PPN Freed</option>
                                                                <option value="9"> Handover Assets (Pasal 16D UU PPN)</option>
                                                            </select>
                                                        </li>
                                                        <li>
                                                            <label for="replacement">Replacement</label>
                                                            <input style="width:15px; height: 15px;" type="checkbox" name="Replacement" id="replacement" value="1"> Tax Replacement
                                                        </li>
                                                        <li>
                                                            <label for="numbertax">Tax Number</label>
                                                            <input type="hidden" name="TaxNumber" value="" id="numberTax">
                                                            <input type="text" class="numaja autoTab" style="width: 40px;" maxlength="3" id="numberTax1" > .
                                                            <input type="text" class="numaja autoTab" style="width: 40px;" maxlength="3" id="numberTax2" > -
                                                            <input type="text" class="numaja autoTab" style="width: 30px;" maxlength="2" id="numberTax3" > .
                                                            <input type="text" class="numaja autoTab" style="width: 75px;" maxlength="8" id="numberTax4" >
                                                        </li>
                                                        <li>
                                                            <label for="taxmonth">Tax Month</label>
                                                            <select class="chosen-select choosen-modal currency" id="taxMonth" name="TaxMonth">
                                                                @for($aa = 1; $aa<=12; $aa++)
                                                                <option value="{{$aa}}" >{{date('F',strtotime('2015-'.$aa.'-01'))}}</option>
                                                                @endfor
                                                            </select>
                                                        </li>
                                                        <li>
                                                            <label for="taxyear">Tax Year</label>
                                                            {{''; $year = date('Y'); $mulai = 2012;}}
                                                            <select class="chosen-select choosen-modal currency" id="taxYear" name="TaxYear">
                                                                @while($mulai <= $year)
                                                                <option value="{{$year}}">{{$year}}</option>
                                                                {{'';$year--;}}
                                                                @endwhile
                                                            </select>
                                                        </li>-->
                            <li>
                                <label for="vehicle">PO Customer</label>
                                <span>{{SalesOrderHeader::find($header->InternalID)->POCustomer}}</span>
                            </li>
                            <li>
                                <label for="vehicle">Number of Vehicle</label>
                                <input id="vehicle" name="vehicle" class='vehicle' type="text" style="width: 200px;">
                            </li>
                            <li>
                                <label for="driver">Driver Name</label>
                                <input id="driver" name="driver" class="driver" type="text" style="width: 200px;">
                            </li>
                            <li>
                                <label for="remark">Remark</label>
                                <textarea name="remark" id="remark" style="width: 200px;" >{{$header->Remark}}</textarea>
                            </li>
                            <li>
                                <div class="required">
                                    * Required
                                </div>
                            </li>
                        </ul>
                        @endif
                    </div>
                    <table class="table master-data" id="table-salesorder-description" style="table-layout:fixed;">
                        <thead>
                            <tr>
                                <th style="width: 25%;">Inventory</th>
                                <th style="width: 10%;">Uom</th>
                                <th style="width: 10%;">Stock</th>
                                <th style="width: 10%;">Qty Order</th>
                                <th style="width: 10%;">Max Qty Shipping</th>
                                <th style="width: 10%;">Qty</th>
                                <th style="width: 14%;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 0;
                            $barisDesc = 1;
                            $barisDetail = 1;
                            ?>
                            @foreach(SalesOrderDescription::where('SalesOrderInternalID',$header->InternalID)->get() as $description)
                            <?php
                            $sumDescription = ShippingAddHeader::getSumDescriptionShipping($description->InternalID);
                            if ($sumDescription == '') {
                                $sumDescription = '0';
                            }
                            ?>
                            <tr style='background: #F1FFDB;' id='rowDescription{{$barisDesc}}'>
                                <td class='chosen-uom'>
                                    {{$description->InventoryText}}
                                </td>
                                <td>
                                    {{$description->UomText}}
                                </td>
                                <td class="text-right">
                                    -
                                </td>
                                <td class="text-right">
                                    {{number_format($description->Qty,'0','.',',')}}
                                </td>
                                <td class="text-right">
                                    {{number_format($description->Qty - $sumDescription,'0','.',',')}}
                                </td>
                                <td class='text-right'>
                                    <input type="text" min="0" max="{{$description->Qty - $sumDescription}}" class="maxWidth qtyDescription right input-theme" name="qtyDescription[]" id="priceDescription-{{$barisDesc}}-qty" value='{{number_format($description->Qty - $sumDescription,'0','.',',')}}'>
                                </td>
                                <td>
                                    <input type="hidden" name="InternalDescription[]" value="{{$description->InternalID}}">
                                    <input type="hidden" name="max[]" value="{{$description->Qty - $sumDescription}}">

                                    <button class="btn btn-pure-xs btn-xs btn-toggle-detail margr5" type="button" data-toshow="show" data="rowDescription{{$barisDesc}}"><span class="glyphicon glyphicon-eye-open"></span></button>
                                    <!--<button class="btn btn-pure-xs btn-xs btn-toggle-spec" type="button" data-toshow="show" data="rowSpec{{$barisDesc}}"><span class="glyphicon glyphicon-comment"></span></button>-->
                                </td>
                            </tr>
                            <tr id='rowSpec{{$barisDesc}}' style="display:none">
                                <td colspan='7' class='rowSpec{{$barisDesc}}' style="text-align: left">
                                    {{nl2br($description->Spesifikasi)}}
                                </td>
                            </tr>
                            @foreach(SalesOrderDetail::where('DescriptionInternalID',$description->InternalID)->get() as $detail)
                            <!--untuk non-parcel-->
                            @if($detail->SalesOrderParcelInternalID == 0)
                            <?php
                            $sumShipping = ShippingAddHeader::getSumShipping($detail->InventoryInternalID, $header->InternalID, $detail->InternalID);
                            if ($sumShipping == '') {
                                $sumShipping = '0';
                            }
                            ?>
                            <tr id='row{{$barisDetail}}' class='rowDescription{{$barisDesc}}'>
                                <td class='chosen-uom'>
                                    {{Inventory::find($detail->InventoryInternalID)->InventoryID.' '.Inventory::find($detail->InventoryInternalID)->InventoryName}}
                                </td>
                                <td>
                                    {{Uom::find($detail->UomInternalID)->UomID.' '.Uom::find($detail->UomInternalID)->UomName}}
                                </td>
                                <td class="right currentStock" id="currentStock-{{$barisDetail}}">
                                    {{getEndStockInventoryWithWarehouse($detail->InventoryInternalID,Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->first()->InternalID)}}
                                </td>
                                <td class="text-right">
                                    <input type="hidden" name="stockcurrent" id="stockCurrent-{{$barisDetail}}" value="{{getEndStockInventoryWithWarehouse_angka($detail->InventoryInternalID,Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->first()->InternalID)}}">
                                    {{$detail->Qty}}
                                </td>
                                <td class="text-right">
                                    {{$detail->Qty - $sumShipping}}
                                </td>
                                <td class='text-right'>
                                    <input type="hidden" name="InternalDetail{{$barisDesc}}[]" value="{{$detail->InternalID}}">
                                    <input type="hidden" name="tipe{{$barisDesc}}[]" value="inventory">
                                    <input type="text" class="maxWidth addShipping qty right input-theme" name="qty{{$barisDesc}}[]" min="0" max="{{$detail->Qty-$sumShipping}}" value="{{number_format($detail->Qty - $sumShipping,'0','.',',')}}" id="price-{{$barisDetail}}-qty">
                                </td>
                                <td>
                                    -
                                </td>
                            </tr>
                            <?php $barisDetail++; ?>
                            @endif <!--tutup if non parcel -->
                            @endforeach<!--tutup foreach quotation detail-->
                            <!--untuk parcel-->
                            @foreach(SalesOrderParcel::where('DescriptionInternalID',$description->InternalID)->get() as $parcel)
                            <?php
                            $sumShippingParcel = ShippingAddHeader::getSumShippingParcel($parcel->ParcelInternalID, $header->InternalID, $parcel->InternalID);
                            if ($sumShippingParcel == '') {
                                $sumShippingParcel = '0';
                            }
                            ?>
                            <tr id='row{{$barisDetail}}' class='rowDescription{{$barisDesc}}'>
                                <td class='chosen-uom'>
                                    {{Parcel::find($parcel->ParcelInternalID)->ParcelID.' '.Parcel::find($parcel->ParcelInternalID)->ParcelName}}
                                </td>
                                <td>
                                    -
                                </td>
                                <td>
                                    -
                                </td>
                                <td>
                                    {{$parcel->Qty}}
                                </td>
                                <td>
                                    {{$parcel->Qty - $sumShippingParcel}}
                                </td>
                                <td class='text-right'>
                                    <input type="hidden" name="InternalDetail{{$barisDesc}}[]" value="{{$parcel->InternalID}}">
                                    <input type="hidden" name="tipe{{$barisDesc}}[]" value="parcel">
                                    <input type="text" class="maxWidth qty addShipping right input-theme" name="qty{{$barisDesc}}[]" maxlength="{{$parcel->Qty - $sumShippingParcel}}" min="0" value="{{number_format($parcel->Qty - $sumShippingParcel,'0','.',',')}}" id="price-{{$barisDetail}}-qty"></td>
                                <td>
                                    -
                                </td>
                            </tr>
                            <?php $barisDetail++; ?>
                            @endforeach<!--tutup foreach quotation parcel-->
                            <?php $barisDesc++; ?>
                            @endforeach <!--tutup foreach quotation description-->
                            <?php $i++; ?>
                        </tbody>
                    </table>
                </div><!---- end div tabwrap---->
                <div class="btnnest pull-right">
                    <button class="btn btn-green btn-sm btn-save" id="btn-save"> Save & Print </button>
                </div>
        </form>
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop

@section('modal')
<div class="modal fade" id="insertPacking" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insert Packing List</h4>
            </div>
            <form action="" method="post" class="action" id="form-so">
                {{'';$hitung = 0;}}
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="insertPacking" id="jenisShipping" name="jenis">
                            <li>
                                <label for="sales">Sales Order ID</label> *
                            </li>
                            <input class="input-theme margbot10" type="text" id="searchSalesOrder2" title="Type Sales Order Name or ID then 'Enter'" placeholder="Type Sales Order Name or ID then 'Enter'">
                            <li id="selectSalesOrder2">

                            </li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btn-add-so2" @if($hitung==0){{'disabled'}}@endif class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="insertShipping" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insert Shipping</h4>
            </div>
            <form action="" method="post" class="action" id="form-so">
                {{'';$hitung = 0;}}
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="insertShipping" id="jenisShipping" name="jenis">
                            <li>
                                <label for="sales">Sales Order ID</label> *
                            </li>
                            <input class="input-theme margbot10" type="text" id="searchSalesOrder" title="Type Sales Order Name or ID then 'Enter'" placeholder="Type Sales Order Name or ID then 'Enter'">
                            <li id="selectSalesOrder">

                            </li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btn-add-so" @if($hitung==0){{'disabled'}}@endif class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="r_summary" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Summary Report</h4>
            </div>
            <form action="" method="post" class="action" id="" target="_blank">
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='summaryShipping'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateReport" data-validation="required">
                            </li>
                        </ul>
                    </div>
                    <div class="required">
                        * Required
                    </div>
                </div>
                <div class="modal-footer">
                    <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                    <button id="btn-report-transaction" type="submit" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNewJournal.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script>
//$("form").bind("submit", function (event) {
//    setTimeout(function () {
//        if ($(".form-error")[0]) {
//            $("#btn-save").prop("disabled", false);
//        } else {
//            $("#btn-save").prop("disabled", true);
//        }
//    }, 100);
//
//});
var tanggalHariIni = '<?php echo date('d') . '-' . date('m') . '-' . date('Y') ?>';
var cariS = '<?php echo Route('formatCariIDSales') ?>';
var getResultSearchSO = "<?php echo Route("getResultSearchShippingSO") ?>";
var getResultSearchSO2 = "<?php echo Route("getResultSearchShippingSO2") ?>";
var currentStockShipping = '<?php echo route('currentStockShipping') ?>';
</script>
<script>
    var availableTags = [<?php foreach (ShippingAddHeader::select('NumberVehicle')->where('CompanyInternalID', Auth::user()->Company->InternalID)->distinct()->get() as $auto) { ?>"<?php echo $auto->NumberVehicle ?>",<?php } ?>];
            var availableTags2 = [<?php foreach (ShippingAddHeader::select('DriverName')->where('CompanyInternalID', Auth::user()->Company->InternalID)->distinct()->get() as $auto) { ?>"<?php echo $auto->DriverName ?>",<?php } ?>];
    var shippingDataBackup = '<?php echo Route('shippingDataBackup', Input::get('coa6') . '---;---' . Input::get('typePayment') . '---;---' . Input::get('typeTax') . '---;---' . Input::get('startDate') . '---;---' . Input::get('endDate')) ?>';
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-penjualan-add/shippingAdd.js')}}"></script>
<!--<script type="text/javascript" src="{{Asset('js/entry-js-penjualan-add/packing.js')}}"></script>-->
<script type="text/javascript" src="{{Asset('js/entry-js-penjualan-add/shippingAddNew.js')}}"></script>
<!--<script type="text/javascript" src="{{Asset('js/entry-js-penjualan-add/shippingAddNewDescription.js')}}"></script>-->
@stop
