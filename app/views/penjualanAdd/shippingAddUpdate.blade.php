@extends('template.header-footer')

@section('title')
Shipping
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<style>
    .help-block {
        margin-left: 30% !important;
    }
    .headinv .chosen-single{
        width: 200px !important;
    }
    .headinv .chosen-drop{
        width: 200px !important;
    }
</style>
@stop

@section('nav')

@stop

@section('content')
@if(myCheckIsEmpty('SalesOrder;Default;Slip;DepartmentDefault'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one Sales Order, Default COA, and Slip to insert shipping.
</div>
@endif
@if(isset($messages))
@if($messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Shipping has been updated.
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showShipping')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Shipping</a>
                <a href="{{route('shippingUpdate',$header->ShippingID)}}" type="button" class="btn btn-sm btn-pure">Update {{$header->ShippingID}}</a>
            </div>
            <div class="btn-group margr5">
                <button type="button" <?php if (myCheckIsEmpty('SalesOrder;Default;Slip;DepartmentDefault')) echo 'disabled'; ?> class="btn btn-green btn-sm dropdown-toggle" data-target="#insertShipping" data-toggle="modal" role="dialog"
                        aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
            </div>
            <div class="btn-group margr5">
                <button type="button" <?php if (myCheckIsEmpty('SalesOrder;Default;Slip;DepartmentDefault')) echo 'disabled'; ?> class="btn btn-green btn-sm dropdown-toggle" data-target="#insertPacking" data-toggle="modal" role="dialog"
                        aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New Packing </button>
            </div>
            <button id="search-button" <?php if (myCheckIsEmpty('Shipping')) echo 'disabled'; ?>  class="btn btn-green btn-sm margr5"><span class="glyphicon glyphicon-search"></span> Search </button>
            @if(checkModul('O04'))
            <button type="button" class="btn btn-green margr5" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rSummary">
                <span class="glyphicon glyphicon-file"></span> Summary Report</button>
            <button type="button" class="btn btn-green" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rDetail">
                <span class="glyphicon glyphicon-file"></span> Detail Report</button>
            @endif
            <a href="{{Route('shippingPrint',$header->ShippingID)}}" class="btn btn-green btn-sm" id="btn-{{$header->ShippingID}}-print" target='_blank' style="margin-right: 0px !important;">
                <span class="glyphicon glyphicon-print"></span>
                Print
            </a>
        </div>
        <!-- <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Search</h4>
            </div>
            <div class="tableadd">
                <ul class="searchmenu">
                    <form method="GET" action="{{Route('showShipping')}}">
                         <li>
                            <label for="coa6">Customer</label>
                            <br>
                            <select class="chosen-select" id="coa6" style="" name="coa6">
                                <option value="-1">All Customer</option>
                                @foreach(Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $coa6)
                                <option value="{{$coa6->InternalID}}">
                                    {{$coa6->ACC6ID.' '.$coa6->ACC6Name}}
                                </option>
                                @endforeach
                            </select>
                        </li>
                        <li><label for="typePayment">Payment Type</label>
                            <br>
                            <select name="typePayment" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Cash</option>
                                <option value="1">Credit</option>
                            </select>
                        </li>
                        <li><label for="typeTax">Tax Type</label>
                            <br>
                            <select name="typeTax" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Non Tax</option>
                                <option value="1">Tax</option>
                            </select>
                        </li>
                        <li><label for="date">Start Date</label>
                            <br><input id="startDate" name="startDate" type="text" autocomplete="off">
                        </li>
                        <li><label for="date">End Date</label>
                            <br><input id="endDate" name="endDate" type="text" autocomplete="off">
                        </li>
                        <li><br>
                            <button type="submit" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-search"></span> Search </button>
                        </li>
                        <li><br>
                            <button id="cancel" type="reset"  class="btn btn-pure btn-sm"><span class="glyphicon glyphicon-remove"></span> Cancel </button>
                        </li>
                    </form>
                </ul>
            </div><!---- end div tableadd---->
        <!--</div><!---- end div tabwrap---->
        @include('template.searchComponentTransactionModule')

        <form method="POST" action="" id='form-updateShippingAdd'>
            <input type='hidden' name='ShippingInternalID' id="shippingInternalID" value='{{$header->InternalID}}'>
            <input type='hidden' name='SalesOrderInternalID' id="salesOrderInternalID" value='{{$header->SalesOrderInternalID}}'>
            <div class="tabwrap">
                <div class="tabhead">
                    <h4 class="headtitle">Shipping <span id="shippingID">{{$header->ShippingID}}</span></h4>
                </div>
                <div class="tableadd">
                    <div class="headinv new">
                        <ul @if(checkModul('O05')) class="pull-left" style="width: 540px;" @else class="pull-left"  @endif>
                            <li>
                                <label for="salesOrder">Order ID</label>
                                <span>{{$header->salesOrder->SalesOrderID}}</span>
                            </li>
                            <li>
                                <label for="date">Date</label>
                                <span>{{date( "d-m-Y", strtotime($header->ShippingDate))}}</span>
                            </li>
                            <li>
                                <label for="customer">Customer</label>
                                <span><?php
                                    $coa6 = ShippingAddHeader::find($header->InternalID)->coa6;
                                    echo $coa6->ACC6ID . ' ' . $coa6->ACC6Name
                                    ?></span>
                            </li>

                            <li>
                                <label for="longTerm">Payment</label>
                                @if($header->isCash == 0)
                                <span>{{'Cash'}}</span>
                                @elseif($header->isCash == 1)
                                <span>{{'Credit'}}</span>
                                @elseif($header->isCash == 2)
                                <span>{{'CBD'}}</span>
                                @elseif($header->isCash == 3)
                                <span>{{'Deposit'}}</span>
                                @elseif($header->isCash == 4)
                                <span>{{'Down Payment'}}</span>
                                @endif
                            </li>
                            @if($header->isCash != 0)
<!--                            <li>
                                <label for="longTerm">Due Date</label>
                                <span>{{date( "d-m-Y", strtotime("+".$header->LongTerm." day",strtotime($header->ShippingDate)))}}</span>
                            </li>-->
                            @else
<!--                            <li>
                                <label for="slip">Slip number *</label>
                                <?php
                                $slipInternal = '';
                                if ($header->isCash == 0) {
                                    $slipDefault = '';
                                } else {
                                    $slipDefault = 'Disabled = "true"';
                                }
                                ?>
                                <select class="chosen-select" id="slip" style="" name="slip"  {{$slipDefault}}>
                                    @foreach(Slip::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $slip)
                                    @if($slip->InternalID == $slipInternal)
                                    <option selected="selected" value="{{$slip->InternalID}}">
                                        {{$slip->SlipID.' '.$slip->SlipName}}
                                    </option>
                                    @else
                                    <option value="{{$slip->InternalID}}">
                                        {{$slip->SlipID.' '.$slip->SlipName}}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>
                            </li>-->
                            @endif
                            <li>
                                <label for="warehouse">Warehouse *</label>
                                <select class="chosen-select choosen-modal warehouse" id="warehouseHeader" name="warehouse">
                                    @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->where("Type",Auth::user()->WarehouseCheck)->get() as $war)
                                    @if($war->InternalID == $header->WarehouseInternalID)
                                    <option selected="selected" id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                        {{$war->WarehouseName;}}
                                    </option>
                                    @else
                                    <option id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                        {{$war->WarehouseName;}}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>
                            </li>
                        </ul>
                        <!--                        <ul @if(checkModul('O05')) class="pull-left" style="width: 360px;" @else class="pull-right"  @endif>
                                                     
                                                    <li>
                                                        <label for="VAT">VAT</label>
                                                        @if($header->VAT == 0)
                                                        <span>{{'Non Tax'}}</span>
                                                        <input type="hidden" id="taxShipping" value="0">
                                                        @else
                                                        <span>{{'Tax'}}</span>
                                                        <input type="hidden" id="taxShipping" value="1">
                                                        @endif
                                                    </li>
                                                   
                                                    @if(!checkModul('O05'))
                                                    <li>
                                                        <div class="required">
                                                            * Required
                                                        </div>
                                                    </li>
                                                    @endif
                                                </ul>-->
                        @if(checkModul('O05'))
                        <ul class="pull-left" style="width: 540px;">
                            <!--                            <li>
                                                            <label for="transactiontype">Transaction</label>
                                                            <select class="chosen-select choosen-modal currency" id="transactionType" name="TransactionType">
                                                                <option value="1" {{($header->TransactionType == 1 ? 'selected' : '')}}> For who is not collect PPN </option>
                                                                <option value="2" {{($header->TransactionType == 2 ? 'selected' : '')}}> For Chamberlain </option>
                                                                <option value="3" {{($header->TransactionType == 3 ? 'selected' : '')}}> Except Chamberlain </option>
                                                                <option value="4" {{($header->TransactionType == 4 ? 'selected' : '')}}> DPP other value </option>
                                                                <option value="6" {{($header->TransactionType == 6 ? 'selected' : '')}}> Other handover, include handover to foreigner tourist in the event of VAT refund </option>
                                                                <option value="7" {{($header->TransactionType == 7 ? 'selected' : '')}}> Handover PPN is not collect</option>
                                                                <option value="8" {{($header->TransactionType == 8 ? 'selected' : '')}}> Handover PPN Freed</option>
                                                                <option value="9" {{($header->TransactionType == 9 ? 'selected' : '')}}> Handover Assets (Pasal 16D UU PPN)</option>
                                                            </select>
                                                        </li>
                                                        <li>
                                                            <label for="replacement">Replacement</label>
                                                            @if($header->Replacement == 1)
                                                            <input style="width:15px; height: 15px;" checked="true" type="checkbox" name="Replacement" id="replacement" value="1"> Tax Replacement
                                                            @else
                                                            <input style="width:15px; height: 15px;" type="checkbox" name="Replacement" id="replacement" value="1"> Tax Replacement
                                                            @endif
                                                        </li>
                                                        <li>
                                                            <label for="numbertax">Tax Number</label>
                                                            <input type="hidden" name="TaxNumber" value="{{$header->TaxNumber}}" id="numberTax">
                            <?php $taxNumberSplit = explode('-', $header->TaxNumber); ?>
                            <?php $taxNumber1 = explode('.', $taxNumberSplit[0]); ?>
                            <?php $taxNumber2 = explode('.', $taxNumberSplit[1]); ?>
                                                            <input type="text" class="numaja autoTab" style="width: 40px;" maxlength="3" id="numberTax1"  value="{{$taxNumber1[0]}}"> .
                                                            <input type="text" class="numaja autoTab" style="width: 40px;" maxlength="3" id="numberTax2"  value="{{$taxNumber1[1]}}"> -
                                                            <input type="text" class="numaja autoTab" style="width: 30px;" maxlength="2" id="numberTax3"  value="{{$taxNumber2[0]}}"> .
                                                            <input type="text" class="numaja autoTab" style="width: 75px;" maxlength="8" id="numberTax4"  value="{{$taxNumber2[1]}}">
                                                        </li>
                                                        <li>
                                                            <label for="taxmonth">Tax Month</label>
                                                            <select class="chosen-select choosen-modal currency" id="taxMonth" name="TaxMonth">
                                                                @for($aa = 1; $aa<=12; $aa++)
                                                                <option value="{{$aa}}" {{($header->TaxMonth == $aa ? 'selected' : '')}}>{{date('F',strtotime('2015-'.$aa.'-01'))}}</option>
                                                                @endfor
                                                            </select>
                                                        </li>
                                                        <li>
                                                            <label for="taxyear">Tax Year</label>
                                                            {{''; $year = date('Y'); $mulai = 2012;}}
                                                            <select class="chosen-select choosen-modal currency" id="taxYear" name="TaxYear">
                                                                @while($mulai <= $year)
                                                                @if( $header->TaxYear == $mulai )
                                                                <option selected="true" value="{{$mulai}}">{{$mulai}}</option>
                                                                @else
                                                                <option value="{{$mulai}}">{{$mulai}}</option>
                                                                @endif
                                                                {{'';$mulai++;}}
                                                                @endwhile
                                                            </select>
                                                        </li>-->
                            <li>
                                <label for="vehicle">PO Customer</label>
                                <span>{{SalesOrderHeader::find($header->SalesOrderInternalID)->POCustomer}}</span>
                            </li>
                            <li>
                                <label for="vehicle">Number of Vehicle</label>
                                <input id="vehicle" name="vehicle" class="vehicle" type="text" style="width: 200px;" value="{{$header->NumberVehicle}}">
                            </li>
                            <li>
                                <label for="driver">Driver Name</label>
                                <input id="driver" name="driver" class="driver" type="text" style="width: 200px;" value="{{$header->DriverName}}">
                            </li>
                             <li>
                                <label for="remark">Remark</label>
                                <textarea name="remark" class="new-textarea-small" id="remark" style="width: 200px">{{$header->Remark}}</textarea>
                            </li>
                            <li>
                                <div class="required">
                                    * Required
                                </div>
                            </li>
                        </ul>
                        @endif
                    </div>
                    <table class="table master-data" id="table-salesorder-description" style="table-layout:fixed;">
                        <thead>
                            <tr>
                                <th style="width: 25%;">Inventory</th>
                                <th style="width: 10%;">Uom</th>
                                <th style="width: 10%;">Stock</th>
                                <th style="width: 10%;">Qty Order</th>
                                <th style="width: 10%;">Max Qty Shipping</th>
                                <th style="width: 10%;">Qty</th>
                                <th style="width: 14%;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 0;
                            $barisDesc = 1;
                            $barisDetail = 1;
                            ?>
                            @foreach(SalesOrderDescription::where('SalesOrderInternalID',$headerorder->InternalID)->get() as $description)
                            <?php
                            $sumDescription = ShippingAddHeader::getSumDescriptionShippingExcept($description->InternalID, $header->InternalID);
                            if ($sumDescription == '') {
                                $sumDescription = '0';
                            }
                            $qtyDescription = ShippingAddDescription::where('SalesOrderDescriptionInternalID', $description->InternalID)
                                            ->where('ShippingInternalID', $header->InternalID)->pluck('Qty');
                            ?>
                            <tr style='background: #F1FFDB;' id='rowDescription{{$barisDesc}}'>
                                <td class='chosen-uom'>
                                    {{$description->InventoryText}}
                                </td>
                                <td>
                                    {{$description->UomText}}
                                </td>
                                <td class="text-right">
                                    -
                                </td>
                                <td class="text-right">
                                    {{number_format($description->Qty,'0','.',',')}}
                                </td>
                                <td class="text-right">
                                    {{number_format($description->Qty - $sumDescription,'0','.',',')}}
                                </td>
                                <td class='text-right'>
                                    <input type="text" min="0" max="{{$description->Qty - $sumDescription}}" class="maxWidth qtyDescription right input-theme" name="qtyDescription[]" id="priceDescription-{{$barisDesc}}-qty" value='{{number_format($qtyDescription,'0','.',',')}}'>
                                </td>
                                <td>
                                    <input type="hidden" name="InternalDescription[]" value="{{$description->InternalID}}">
                                    <input type="hidden" name="max[]" value="{{$description->Qty - $sumDescription}}">

                                    <button class="btn btn-pure-xs btn-xs btn-toggle-detail margr5" type="button" data-toshow="show" data="rowDescription{{$barisDesc}}"><span class="glyphicon glyphicon-eye-open"></span></button>
                                    <!--<button class="btn btn-pure-xs btn-xs btn-toggle-spec" type="button" data-toshow="show" data="rowSpec{{$barisDesc}}"><span class="glyphicon glyphicon-comment"></span></button>-->
                                </td>
                            </tr>
                            <tr id='rowSpec{{$barisDesc}}' style="display:none">
                                <td colspan='6' class='rowSpec{{$barisDesc}}' style="text-align: left">
                                    {{nl2br($description->Spesifikasi)}}
                                </td>
                            </tr>
                            @foreach(SalesOrderDetail::where('DescriptionInternalID',$description->InternalID)->get() as $detail)
                            <!--untuk non-parcel-->
                            @if($detail->SalesOrderParcelInternalID == 0)
                            <?php
                            $sumShipping = ShippingAddHeader::getSumShippingExcept($detail->InventoryInternalID, $header->InternalID, $detail->InternalID);
                            if ($sumShipping == '') {
                                $sumShipping = '0';
                            }
                            $qtyDetail = ShippingAddDetail::where('SalesOrderDetailInternalID', $detail->InternalID)
                                            ->where('ShippingInternalID', $header->InternalID)->pluck('Qty');
                            ?>
                            <tr id='row{{$barisDetail}}' class='rowDescription{{$barisDesc}}'>
                                <td class='chosen-uom'>
                                    {{Inventory::find($detail->InventoryInternalID)->InventoryID.' '.Inventory::find($detail->InventoryInternalID)->InventoryName}}
                                </td>
                                <td>
                                    {{Uom::find($detail->UomInternalID)->UomID.' '.Uom::find($detail->UomInternalID)->UomName}}
                                </td>
                                <td class="right currentStock" id="currentStock-{{$barisDetail}}">
                                    {{getEndStockInventoryWithWarehouseWithoutShipping($detail->InventoryInternalID,Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->first()->InternalID,$header->InternalID)}}
                                </td>
                                <td class="text-right">
                                    <input type="hidden" name="stockcurrent" id="stockCurrent-{{$barisDetail}}" value="{{getEndStockInventoryWithWarehouseWithoutShipping_angka($detail->InventoryInternalID,Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->first()->InternalID,$header->InternalID)}}">
                                    {{$detail->Qty}}
                                </td>
                                <td class="text-right">
                                    {{$detail->Qty - $sumShipping}}
                                </td>
                                <td class='text-right'>
                                    <input type="hidden" name="InternalDetail{{$barisDesc}}[]" value="{{$detail->InternalID}}">
                                    <input type="hidden" name="tipe{{$barisDesc}}[]" value="inventory">
                                    <input type="text" class="maxWidth addShipping qty right input-theme" name="qty{{$barisDesc}}[]" min="0" max="{{$detail->Qty-$sumShipping}}" value="{{number_format($qtyDetail,'0','.',',')}}" id="price-{{$barisDetail}}-qty">
                                </td>
                                <td>
                                    -
                                </td>
                            </tr>
                            <?php $barisDetail++; ?>
                            @endif <!--tutup if non parcel -->
                            @endforeach<!--tutup foreach quotation detail-->
                            <!--untuk parcel-->
                            @foreach(SalesOrderParcel::where('DescriptionInternalID',$description->InternalID)->get() as $parcel)
                            <?php
                            $sumShippingParcel = ShippingAddHeader::getSumShippingExceptParcel($parcel->ParcelInternalID, $header->InternalID, $parcel->InternalID);
                            if ($sumShippingParcel == '') {
                                $sumShippingParcel = '0';
                            }
                            $qtyDetail = ShippingAddParcel::where('SalesOrderParcelDetailInternalID', $parcel->InternalID)
                                            ->where('ShippingInternalID', $header->InternalID)->pluck('Qty');
                            ?>
                            <tr id='row{{$barisDetail}}' class='rowDescription{{$barisDesc}}'>
                                <td class='chosen-uom'>
                                    {{Parcel::find($parcel->ParcelInternalID)->ParcelID.' '.Parcel::find($parcel->ParcelInternalID)->ParcelName}}
                                </td>
                                <td>
                                    -
                                </td>
                                <td>
                                    -
                                </td>
                                <td>
                                    {{$parcel->Qty}}
                                </td>
                                <td>
                                    {{$parcel->Qty - $sumShippingParcel}}
                                </td>
                                <td class='text-right'>
                                    <input type="hidden" name="InternalDetail{{$barisDesc}}[]" value="{{$parcel->InternalID}}">
                                    <input type="hidden" name="tipe{{$barisDesc}}[]" value="parcel">
                                    <input type="text" class="maxWidth qty addShipping right input-theme" name="qty{{$barisDesc}}[]" maxlength="{{$parcel->Qty - $sumShippingParcel}}" min="0" value="{{number_format($qtyDetail,'0','.',',')}}" id="price-{{$barisDetail}}-qty"></td>
                                <td>
                                    -
                                </td>
                            </tr>
                            <?php $barisDetail++; ?>
                            @endforeach<!--tutup foreach quotation parcel-->
                            <?php $barisDesc++; ?>
                            @endforeach <!--tutup foreach quotation description-->
                            <?php $i++; ?>
                        </tbody>
                    </table>
                </div><!---- end div tabwrap---->
            </div>
            <div class="btnnest pull-right">
                <button class="btn btn-green btn-sm btn-save" id="btn-save"> Save </button>
            </div>
        </form>
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop

@section('modal')
<div class="modal fade" id="insertPacking" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insert Packing List</h4>
            </div>
            <form action="" method="post" class="action" id="form-so">
                {{'';$hitung = 0;}}
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="insertPacking" id="jenisShipping" name="jenis">
                            <li>
                                <label for="sales">Sales Order ID</label> *
                            </li>
                            <input class="input-theme margbot10" type="text" id="searchSalesOrder2" title="Type Sales Order Name or ID then 'Enter'" placeholder="Type Sales Order Name or ID then 'Enter'">
                            <li id="selectSalesOrder2">

                            </li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btn-add-so2" @if($hitung==0){{'disabled'}}@endif class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="insertShipping" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insert Shipping</h4>
            </div>
            <form action="" method="post" class="action" id="form-so">
                {{'';$hitung = 0;}}
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="insertShipping" id="jenisShipping" name="jenis">
                            <li>
                                <label for="sales">Sales Order ID</label> *
                            </li>
                            <input class="input-theme margbot10" type="text" id="searchSalesOrder" title="Type Sales Order Name or ID then 'Enter'" placeholder="Type Sales Order Name or ID then 'Enter'">
                            <li id="selectSalesOrder">

                            </li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btn-add-so" @if($hitung==0){{'disabled'}}@endif class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="r_summary" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Summary Report</h4>
            </div>
            <form action="" method="post" class="action" id="" target="_blank">
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='summaryShipping'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateReport" data-validation="required">
                            </li>
                        </ul>
                    </div>
                    <div class="required">
                        * Required
                    </div>
                </div>
                <div class="modal-footer">
                    <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                    <button id="btn-report-transaction" type="submit" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNewJournal.jquery.js')}}" type="text/javascript"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script>
var getResultSearchSO = "<?php echo Route("getResultSearchShippingSO") ?>";
var getResultSearchSO2 = "<?php echo Route("getResultSearchShippingSO2") ?>";
var currentStockShipping = '<?php echo route('currentStockShippingUpdate') ?>';
</script>
<script>
    var availableTags = [<?php foreach (ShippingAddHeader::select('NumberVehicle')->where('CompanyInternalID', Auth::user()->Company->InternalID)->distinct()->get() as $auto) { ?>"<?php echo $auto->NumberVehicle ?>",<?php } ?>];
            var availableTags2 = [<?php foreach (ShippingAddHeader::select('DriverName')->where('CompanyInternalID', Auth::user()->Company->InternalID)->distinct()->get() as $auto) { ?>"<?php echo $auto->DriverName ?>",<?php } ?>];
    var shippingDataBackup = '<?php echo Route('shippingDataBackup', Input::get('coa6') . '---;---' . Input::get('typePayment') . '---;---' . Input::get('typeTax') . '---;---' . Input::get('startDate') . '---;---' . Input::get('endDate')) ?>';
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-penjualan-add/shippingAdd.js')}}"></script>
<!--<script type="text/javascript" src="{{Asset('js/entry-js-penjualan-add/packing.js')}}"></script>-->
<script type="text/javascript" src="{{Asset('js/entry-js-penjualan-add/shippingAddUpdate.js')}}"></script>
<!--<script type="text/javascript" src="{{Asset('js/entry-js-penjualan-add/shippingAddUpdateDescription.js')}}"></script>-->
@stop
