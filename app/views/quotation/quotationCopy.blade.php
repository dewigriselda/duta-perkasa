@extends('template.header-footer')

@section('title')
Quotation
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<style>
    .help-block {
        margin-left: 30% !important;
    }
</style>
@stop

@section('nav')

@stop

@section('content')
@if(isset($messages))
@if($messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Quotation has been updated.
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showQuotation')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Quotation</a>
                <a href="{{route('quotationUpdate',$header->QuotationID)}}" type="button" class="btn btn-sm btn-pure">Update {{$header->QuotationID}}</a>
            </div>
            <div class="btn-group margr5">
                <a href="{{Route('quotationNew')}}">
                    <button type="button" class="btn btn-green btn-sm dropdown-toggle " aria-expanded="false"><span class="glyphicon glyphicon-plus"></span>New </button>
                </a>
            </div>
            <button id="search-button" <?php if (myCheckIsEmpty('Quotation')) echo 'disabled'; ?>  class="btn btn-green btn-sm margr5"><span class="glyphicon glyphicon-search"></span> Search</button>
            @if(checkModul('O04'))
            <button type="button" class="btn btn-green margr5" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rSummary">
                <span class="glyphicon glyphicon-file"></span> Summary Report</button>
            <button type="button" class="btn btn-green" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rDetail">
                <span class="glyphicon glyphicon-file"></span> Detail Report</button>
            @endif
        </div>
        <!-- <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Search</h4>
            </div>
            <div class="tableadd">
                <ul class="searchmenu">
                    <form method="GET" action="{{Route('showQuotation')}}">
                        <li>
                            <label for="coa6">Customer *</label>
                            <br>
                            <select class="chosen-select" id="coa6" style="" name="coa6">
                                <option value="-1">All Customer</option>
                                @foreach(Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $coa6)
                                <option value="{{$coa6->InternalID}}">
                                    {{$coa6->ACC6ID.' '.$coa6->ACC6Name}}
                                </option>
                                @endforeach
                            </select>
                        </li>
                        <li><label for="typePayment">Payment Type</label>
                            <br>
                            <select name="typePayment" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Cash</option>
                                <option value="1">Credit</option>
                            </select>
                        </li>
                        <li><label for="typeTax">Tax Type</label>
                            <br>
                            <select name="typeTax" style="width: 70px">
                                <option value="-1">All</option>
                                <option value="0">Non Tax</option>
                                <option value="1">Tax</option>
                            </select>
                        </li>
                        <li><label for="date">Start Date</label>
                            <br><input id="startDate" name="startDate" type="text" autocomplete="off">
                        </li>
                        <li><label for="date">End Date</label>
                            <br><input id="endDate" name="endDate" type="text" autocomplete="off">
                        </li>
                        <li><br>
                            <button type="submit" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-search"></span> Search </button>
                        </li>
                        <li><br>
                            <button id="cancel" type="reset"  class="btn btn-pure btn-sm"><span class="glyphicon glyphicon-remove"></span> Cancel </button>
                        </li>
                    </form>
                </ul>
            </div><!---- end div tableadd---->
        <!--</div><!---- end div tabwrap---->
        @include('template.searchComponentTransactionModule')

        <form method="POST" action="">
            <input type='hidden' name='QuotationInternalID' id='QuotationInternalID' value='{{$header->InternalID}}'>
            <div class="tabwrap">
                <div class="tabhead">
                    <!--                    @if($header->isCash == 0)
                                        <h4 class="headtitle">Quotation <span id="quotationID">{{$header->QuotationID}}</span></h4>
                                        @else
                                        <h4 class="headtitle">Quotation <span id="quotationID">{{$header->QuotationID}}</span></h4>
                                        @endif-->
                    <h4 class="headtitle">Quotation <span id="quotationID">{{QuotationHeader::getNextIDQuotation($quotation)}}</span></h4>
                </div>
                <div class="tableadd">
                    <div class="headinv new">
                        <ul class="pull-left">
                            <li>
                                <label for="revisi">Replacement</label>
                                <span>{{$header->Parent}} - {{$header->QuotationID}}</span>
                            </li>
                            <li>
                                <label for="date">Date *</label>
                                <input id="date" name="date" type="text" autocomplete="off" data-validation="required">
                            </li>
                            <!--                            <li>
                                                            <label for="date">Date </label>
                                                            <span>{{date('d M Y', strtotime($header->QuotationDate))}}</span>
                                                            <input type="hidden" id="headerDate" value="{{date('d-m-Y', strtotime($header->QuotationDate))}}">
                                                        </li>-->
                            <li>
                                <label for="coa6">Customer *</label>
                                <select class="chosen-select" id="coa6_input" style="" name="coa6">
                                    @foreach(Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $coa6)
                                    @if($coa6->InternalID == $header->ACC6InternalID)
                                    <option selected="selected" value="{{$coa6->InternalID}}">
                                        {{$coa6->ACC6ID.' '.$coa6->ACC6Name}}
                                    </option>
                                    @else
                                    <option value="{{$coa6->InternalID}}">
                                        {{$coa6->ACC6ID.' '.$coa6->ACC6Name}}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>
                            </li>
                            <li id="customermanagerdiv">

                            </li>
                            <li id="customercpdiv">

                            </li>
                            <li class="hide">
                                <label for="payment">Payment *</label>
                                @if($header->isCash == 0)
                                <input type="radio" class="radio-tipe" id="paymentCash" name="isCash" value="0" checked="checked">Cash
                                <input type="radio" class="radio-tipe" id="paymentCredit" name="isCash" value="1">Credit
                                @else
                                <input type="radio" class="radio-tipe" id="paymentCash" name="isCash" value="0">Cash
                                <input type="radio" class="radio-tipe" id="paymentCredit" name="isCash" value="1" checked="checked">Credit
                                @endif
                            </li>
                            <li class="hide">
                                <label for="longTerm">Long Term *</label>
                                @if($header->isCash == 0)
                                <input type="text" name="longTerm" id="longTerm" value="0" disabled>
                                @else
                                <input type="text" name="longTerm" id="longTerm"  value='{{$header->LongTerm}}'>
                                @endif
                            </li>
                            <li style="display:none">
                                <label for="warehouse">Warehouse *</label>
                                <select class="chosen-select choosen-modal warehouse" id="warehouseHeader" name="warehouse">
                                    @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $war)
                                    @if($war->InternalID == $header->WarehouseInternalID)
                                    <option selected="selected" id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                        {{$war->WarehouseName;}}
                                    </option>
                                    @else
                                    <option id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                        {{$war->WarehouseName;}}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label for="currency">Currency *</label>
                                <select class="chosen-select choosen-modal currency" id="currencyHeader" name="currency">
                                    @foreach(Currency::orderBy('Default','desc')->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $cur)
                                    @if($cur->InternalID == $header->CurrencyInternalID)
                                    <option selected="selected" id="cur{{$cur->InternalID}}" value="{{$cur->InternalID.'---;---'.$cur->CurrencyName.'---;---'.$cur->Rate.'---;---'.$cur->Default}}">
                                        {{$cur->CurrencyName;}}
                                    </option>
                                    @else
                                    <option id="cur{{$cur->InternalID}}" value="{{$cur->InternalID.'---;---'.$cur->CurrencyName.'---;---'.$cur->Rate.'---;---'.$cur->Default}}">
                                        {{$cur->CurrencyName;}}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label for="rate">Rate *</label>
                                <input type="text" class="maxWidth rate numajaDesimal" name="rate" maxlength="" id="rate" value="{{$header->CurrencyRate}}" data-validation="required">
                            </li>
                            <li style="">
                                <label for="vat">VAT *</label>
                                @if($header->VAT == 0)
                                <input style="width:15px; height: 15px;" type="checkbox" name="vat" id="vat" value="1"> Tax 10%
                                @else
                                <input style="width:15px; height: 15px;" type="checkbox" checked name="vat" id="vat" value="1"> Tax 10%
                                @endif
                            </li>
                            <li>
                                <label for="refNumber">Ref Number</label>
                                <input type="text" name="RefNumber" value="{{$header->RefNumber}}">
                            </li>
                        </ul>
                        <ul class="pull-right">
                            <li>
                                <label for="remark">Remark *</label>
                                <textarea name="remark" id="remark" data-validation="required">{{$header->Remark}}</textarea>
                            </li>
                            <li>
                                <label for="longTerm">Delivery Terms</label>
                                <input type="text" name="DeliveryTerms" value='{{$header->DeliveryTerms}}'>
                            </li>
                            <li>
                                <label for="longTerm">Delivery Time</label>
                                <input type="text" name="DeliveryTime" value='{{$header->DeliveryTime}}'>
                            </li>
                            <li>
                                <label for="longTerm">Validity</label>
                                <input type="text" name="Validity" value='{{$header->Validity}}'>
                            </li>
                            <li>
                                <label for="termOfPayment">Term of Payment</label>
                                <input type="text" name="TermOfPayment" value="{{$header->TermOfPayment}}">
                            </li>
                            <li>
                                <div class="required">
                                    * Required
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="tableadd journupdate">
                        <label>Description</label>
                        <table class="table master-data table-striped" id="table-quotation-description-header" style="table-layout:fixed;">
                            <thead class="default-thead">
                                <tr>
                                    <th style="width: 25%;">Inventory</th>
                                    <th style="width: 10%;">Uom</th>
                                    <th style="width: 10%;">Qty</th>
                                    <th style="width: 15%;">Price</th>
                                    <th style="width: 10%;">Disc (%)</th>
                                    <th style="width: 10%;">Disc</th>
                                    <th style="width: 15%;">Subtotal</th>
                                    <th class="text-center" style="width: 5%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="row0">
                                    <td class="chosen-transaction">
                                        <input class="input-theme" type="text" id="InventoryDescription-0" placeholder="Inventory Name">
                                    </td>
                                    <td>
                                        <input type="text" class="input-theme uomDescription" id="uomDescription-0" placeholder="uom">
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth qtyDescription right numaja input-theme" maxlength="11" min="1" id="priceDescription-0-qty" value="1">
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth priceDescription right numajaDesimal" maxlength="" id="priceDescription-0" value="0">
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth discountDescription right numajaDesimal input-theme" id="priceDescription-0-discount" value="0.00">
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth discountNominalDescription right numajaDesimal input-theme" id="priceDescription-0-discountNominal" value="0.00">
                                    </td>
                                    <td id="priceDescription-0-qty-hitung" class="right">
                                        0.00
                                    </td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-green btn-sm" id="btn-addRow-description"><span class="glyphicon glyphicon-plus"></span></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <label>Detail</label>
                        <table class="table master-data  table-striped" id="table-quotation-header" style="table-layout:fixed;">
                            <thead  class="default-thead">
                                <tr>
                                    <th style="width: 25%;">Inventory</th>
                                    <th style="width: 10%;">Uom</th>
                                    <th style="width: 10%;">Stock</th>
                                    <th style="width: 10%;">Description</th>
                                    <th style="width: 10%;">Qty</th>
                                    <th style="width: 15%;">Price</th>
                                    <th style="width: 10%;">Disc (%)</th>
                                    <th style="width: 10%;">Disc</th>
                                    <th style="width: 15%;">Subtotal</th>
                                    <th class="text-center" style="width: 6%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="row0">
                                    <td class="chosen-transaction">
                                        <input class="input-theme margbot10" type="text" id="searchInventory" tabindex="-1" title="Type Inventory Name or ID then 'Enter'" placeholder="Type Inventory Name or ID then 'Enter'">
                                        <div id="selectInventory">

                                        </div>
                                    </td>
                                    <td>
                                        <select id="uom-0" class="input-theme uom">

                                        </select>
                                    </td>
                                    <td class="text-right">
                                        <span class="maxWidth stock" id="price-0-stock" data-toggle="popover" data-placement="bottom" data-html="true">-</span>
                                    </td>
                                    <td>
                                        <select id="description-0" class="input-theme description">
                                            <?php $count = 1; ?>
                                            @foreach(QuotationDescription::where('QuotationInternalID',$header->InternalID)->get() as $description)
                                            <option value='rowDescription{{$count}}'>{{$description->InventoryText}}</option>
                                            <?php $count++; ?>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth qty right numaja input-theme" maxlength="11" min="1" id="price-0-qty" value="1">
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth price right numajaDesimal input-theme" maxlength="" id="price-0" value="0">
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth discount right numajaDesimal input-theme" id="price-0-discount" value="0.00">
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth discountNominal right numajaDesimal input-theme" id="price-0-discountNominal" value="0.00">
                                    </td>
                                    <td id="price-0-qty-hitung" class="right">
                                        0.00
                                    </td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-green btn-sm" disabled="" id="btn-addRow"><span class="glyphicon glyphicon-plus"></span></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>

                        <table class="table master-data" id="table-quotation-description" style="table-layout:fixed;">
                            <thead>
                                <tr>
                                    <th style="width: 25%;">Inventory</th>
                                    <th style="width: 10%;">Uom</th>
                                    <th style="width: 10%;">Stock</th>
                                    <th style="width: 10%;">Qty</th>
                                    <th style="width: 15%;">Price</th>
                                    <th style="width: 10%;">Disc (%)</th>
                                    <th style="width: 10%;">Disc</th>
                                    <th style="width: 15%;">Subtotal</th>
                                    <th style="width: 14%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $descriptionn = array();
                                $barisDesc = 1;
                                $barisDetail = 1;
                                ?>
                                @foreach(QuotationDescription::where('QuotationInternalID',$header->InternalID)->get() as $description)
                                <?php array_push($descriptionn, "~" . $barisDesc); ?>
                                <tr style='background: #F1FFDB;' id='rowDescription{{$barisDesc}}'>
                                    <td class='chosen-uom'>
                                        <input type="hidden" class="inventoryDescription" style="width: 100px" id="inventoryDescription-{{$barisDesc}}" name="inventoryDescription[]" value="{{$description->InventoryText}}">{{$description->InventoryText}}
                                    </td>
                                    <td>
                                        <input type="text" class="uomDescription input-theme" name="uomDescription[]" id="uomDescription-{{$barisDesc}}" value='{{$description->UomText}}'>
                                    </td>
                                    <td class='text-right'>-</td>
                                    <td class='text-right'>
                                        <input type="text" class="maxWidth qtyDescription right input-theme" name="qtyDescription[]" id="priceDescription-{{$barisDesc}}-qty" value='{{$description->Qty}}'></input>
                                    </td>
                                    <td>
                                        <input type="text" class="maxWidth priceDescription right numajaDesimal input-theme" name="priceDescription[]" maxlength="" value="{{number_format($description->Price,'2','.',',')}}" id="priceDescription-{{$barisDesc}}">
                                    </td>
                                    <td class='text-right'>
                                        <input type="text" class="maxWidth discountDescription right input-theme numajaDesimal" name="discountDescription[]" min="0" max="100" id="priceDescription-{{$barisDesc}}-discount" value="{{$description->Discount}}">
                                    </td>
                                    <td class='text-right'>
                                        <input type="text" class="maxWidth discountNominalDescription right numajaDesimal input-theme" name="discountNominalDescription[]" id="priceDescription-{{$barisDesc}}-discountNominal" value="{{number_format($description->DiscountNominal,'2','.',',')}}">
                                    </td>
                                    <td class="right subtotalDescription" id="priceDescription-{{$barisDesc}}-qty-hitung">{{number_format($description->SubTotal,'2','.',',')}}</td>
                                    <td>
                                        <button class="btn btn-pure-xs btn-xs btn-deleteRow-Description margr5" type="button" data="rowDescription{{$barisDesc}}" text="rowSpec{{$barisDesc}}"><span class="glyphicon glyphicon-trash"></span></button>
                                        <button class="btn btn-pure-xs btn-xs btn-toggle-detail margr5" type="button" data-toshow="show" data="rowDescription{{$barisDesc}}"><span class="glyphicon glyphicon-eye-open"></span></button>
                                        <button class="btn btn-pure-xs btn-xs btn-toggle-spec" type="button" data-toshow="show" data="rowSpec{{$barisDesc}}"><span class="glyphicon glyphicon-comment"></span></button>
                                    </td>
                                </tr>
                                <tr id='rowSpec{{$barisDesc}}'>
                                    <td colspan='9' class='rowSpec{{$barisDesc}}'>
                                        <textarea onkeyup="textAreaAdjust(this)" name="spesifikasi[]" style="overflow:hidden; width: 100%;" class="input-theme rowSpec{{$barisDesc}}">{{$description->Spesifikasi}}</textarea>
                                    </td>
                                </tr>
                                @foreach(QuotationDetail::where('DescriptionInternalID',$description->InternalID)->get() as $detail)
                                <!--untuk non-parcel-->
                                @if($detail->QuotationParcelInternalID == 0)
                                <tr id='row{{$barisDetail}}' class='rowDescription{{$barisDesc}}'>
                                    <td class='chosen-uom'>
                                        <input type="hidden" class="inventory" style="width: 100px" id="inventory-{{$barisDetail}}" name="inventory{{$barisDesc}}[]" value="{{$detail->InventoryInternalID}}---;---inventory">{{Inventory::find($detail->InventoryInternalID)->InventoryID.' '.Inventory::find($detail->InventoryInternalID)->InventoryName}}
                                    </td>
                                    <td>
                                        <select id="uom-{{$barisDetail}}" name="uom{{$barisDesc}}[]" class="input-theme uom">
                                            @foreach (InventoryUom::where("InventoryInternalID", $detail->InventoryInternalID)->get() as $uom)
                                            <option value="{{$uom->UomInternalID}}" {{ ($uom->UomInternalID == $detail->UomInternalID) ? "selected" : ""}}>{{ $uom->Uom->UomID; }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class='text-right'>
                                        <?php
                                        $tamp = "";
                                        $tamp = getStockInventorySOHelper($detail->InventoryInternalID);
                                        $result = explode('---;---', $tamp);
                                        ?>
                                        <span class="maxWidth stock right input-theme" id="price-{{$barisDesc}}-stock">{{$result[0]}}</span>
                                    </td>
                                    <td class='text-right'>
                                        <input type="text" class="maxWidth qty right input-theme" name="qty{{$barisDesc}}[]" maxlength="11" min="1" value="{{number_format($detail->Qty,'0','.',',')}}" id="price-{{$barisDetail}}-qty"></td>
                                    <td>
                                        <input type="text" class="maxWidth price right numajaDesimal input-theme" name="price{{$barisDesc}}[]" maxlength="" value="{{number_format($detail->Price,'2','.',',')}}" id="price-{{$barisDetail}}">
                                    </td>
                                    <td class='text-right'>
                                        <input type="text" class="maxWidth discount right input-theme numajaDesimal" name="discount{{$barisDesc}}[]" min="0" max="100" id="price-{{$barisDetail}}-discount" value="{{$detail->Discount}}">
                                    </td>
                                    <td class='text-right'>
                                        <input type="text" class="maxWidth discountNominal right numajaDesimal input-theme" name="discountNominal{{$barisDesc}}[]" id="price-{{$barisDetail}}-discountNominal" value="{{number_format($detail->DiscountNominal,'2','.',',')}}">
                                    </td>
                                    <td class="right subtotal" id="price-{{$barisDetail}}-qty-hitung">{{number_format($detail->SubTotal,'2','.',',')}}</td>
                                    <td>
                                        <button class="btn btn-pure-xs btn-xs btn-deleteRow" type="button" data="row{{$barisDetail}}"><span class="glyphicon glyphicon-trash"></span></button>
                                    </td>
                                </tr>
                                <?php $barisDetail++; ?>
                                @endif
                                @endforeach
                                <!--untuk parcel-->
                                @foreach(QuotationParcel::where('DescriptionInternalID',$description->InternalID)->get() as $parcel)
                                <tr id='row{{$barisDetail}}' class='rowDescription{{$barisDesc}}'>
                                    <td class='chosen-uom'>
                                        <input type="hidden" class="inventory" style="width: 100px" id="inventory-{{$barisDetail}}" name="inventory{{$barisDesc}}[]" value="{{$parcel->ParcelInternalID}}---;---parcel">{{Parcel::find($parcel->ParcelInternalID)->ParcelID.' '.Parcel::find($parcel->ParcelInternalID)->ParcelName}}
                                    </td>
                                    <td>
                                        <select id="uom-{{$barisDetail}}" readonly name="uom[]" class="input-theme uom">
                                            <option value="0" >-</option>
                                        </select>
                                    </td>
                                    <td class='text-right'>
                                        -
                                    </td>
                                    <td class='text-right'>
                                        <input type="text" class="maxWidth qty right input-theme" name="qty{{$barisDesc}}[]" maxlength="11" min="1" value="{{number_format($parcel->Qty,'0','.',',')}}" id="price-{{$barisDetail}}-qty"></td>
                                    <td>
                                        <input type="text" class="maxWidth price right numajaDesimal input-theme" name="price{{$barisDesc}}[]" maxlength="" value="{{number_format($parcel->Price,'2','.',',')}}" id="price-{{$barisDetail}}">
                                    </td>
                                    <td class='text-right'>
                                        <input type="text" class="maxWidth discount right input-theme numajaDesimal" name="discount{{$barisDesc}}[]" min="0" max="100" id="price-{{$barisDetail}}-discount" value="{{$parcel->Discount}}">
                                    </td>
                                    <td class='text-right'>
                                        <input type="text" class="maxWidth discountNominal right numajaDesimal input-theme" name="discountNominal{{$barisDesc}}[]" id="price-{{$barisDetail}}-discountNominal" value="{{number_format($parcel->DiscountNominal,'2','.',',')}}">
                                    </td>
                                    <td class="right subtotal" id="price-{{$barisDetail}}-qty-hitung">{{number_format($parcel->SubTotal,'2','.',',')}}</td>
                                    <td>
                                        <button class="btn btn-pure-xs btn-xs btn-deleteRow" type="button" data="row{{$barisDetail}}"><span class="glyphicon glyphicon-trash"></span></button>
                                    </td>
                                </tr>
                                <?php $barisDetail++; ?>
                                @endforeach
                                <?php $barisDesc++; ?>
                                @endforeach
                            </tbody>
                        </table>
                    </div><!---- end div tableadd---->
                    <input type="hidden" name="grandTotalValue" value="0" id="grandTotalValue">
                    <table class="pull-left">
                        <tr>
                            <td></td>
                        </tr>
                        <tr>
                            <td><br></td>
                        </tr>
                        <tr>
                            <td><p>Created by {{User::where('UserID', $header->UserRecord)->first()->UserName.' '.date('d-m-Y H:i:s', strtotime($header->dtRecord))}}</p></td>
                        </tr>
                        <tr>
                            @if($header->UserModified != '0')
                            <td><p>Modified by {{User::where('UserID', $header->UserModified)->first()->UserName.' '.date('d-m-Y H:i:s', strtotime($header->dtModified))}}</p></td>
                            @else
                            <td><p>Modified by -</p></td>
                            @endif
                        </tr>
                    </table>
                    <table class="pull-right">
                        <tr>
                            <td><h5 class="right margr10 h5total"><b>Total</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="total"></b></h5></td>
                        </tr>
                        <tr >
                            <td><h5 class="right margr10 h5total"><b>Discount</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total new" style="width: 150px;"><input type="text" style="width: 120px;" class="maxWidth right numajaDesimal discountGlobal" name="DiscountGlobal" maxlength="" id="discountGlobal" value="{{number_format($header->DiscountGlobal, '2', '.', ',')}}"></h5></td>
                        </tr>
                        <tr>
                            <td><h5 class="right margr10 h5total"><b>Grand Total</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="grandTotal"></b></h5></td>
                        </tr>

                        <tr>
                            <td><h5 class="right margr10 h5total hidevat"><b>Tax</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="tax"></b></h5></td>
                        </tr>
                        <tr>
                            <td><h5 class="right margr10 h5total hidevat"><b>Grand Total(tax)</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="grandTotalAfterTax"></b></h5></td>
                        </tr>
                    </table>
                </div><!---- end div tableadd---->
            </div><!---- end div tabwrap---->
            <div class="btnnest pull-right">
                <button class="btn btn-green btn-sm btn-save" id="btn-save"> Save </button>
            </div>
        </form>
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop

@section('modal')
<div class="modal fade" id="r_summary" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Summary Report</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="" target="_blank">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='summaryQuotation'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateReport" data-validation="required">
                            </li>
                        </ul>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNewJournal.jquery.js')}}" type="text/javascript"></script>
<script>
                                            var getStockInventorySO = "<?php echo Route("getStockInventorySO") ?>";
                                            var getCustomerManager = "<?php echo Route("getCustomerManager") ?>";
                                            var getCustomerCP = "<?php echo Route("getCustomerCp") ?>";
                                            var getPriceThisParcel = "<?php echo Route("getPriceThisParcel") ?>";
                                            var getUomThisInventory = "<?php echo Route("getUomThisInventory") ?>";
                                            var getPriceRangeThisInventoryQuotation = "<?php echo Route("getPriceRangeThisInventoryQuotation") ?>";
                                            var getSearchResultInventoryForQuotation = "<?php echo Route("getSearchResultInventoryForQuotation") ?>";
                                            var textSelect = '';
//var baris = '<?php // echo $barisTerakhir;             ?>';
</script>
<script>
    var quotationDataBackup = '<?php echo Route('quotationDataBackup', Input::get('coa6') . '---;---' . Input::get('typePayment') . '---;---' . Input::get('typeTax') . '---;---' . Input::get('startDate') . '---;---' . Input::get('endDate')) ?>';
    var baris = '<?php echo $barisDesc; ?>';
    var description = '<?php echo json_encode($descriptionn); ?>';
    var copydate = '<?php echo date('d-m-Y', strtotime($header->QuotationDate)); ?>';
</script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-quotation/quotation.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-quotation/quotationUpdate.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-quotation/quotationUpdateDescription.js')}}"></script>
@stop
