@extends('template.header-footer')

@section('title')
Quotation
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosenCustom.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
@stop

@section('nav')

@stop

@section('content')
@if(isset($print))
<input type="hidden" name="print" id="print" value="{{$print}}">
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showQuotation')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Quotation</a>
                <a href="{{route('quotationDetail',$header->QuotationID)}}" type="button" class="btn btn-sm btn-pure">Detail {{$header->QuotationID}}</a>
            </div>
            <div class="btn-group margr5">
                <a href="{{Route('quotationNew')}}">
                    <button type="button" class="btn btn-green btn-sm dropdown-toggle" aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
                </a>
            </div>
            <button <?php if (myCheckIsEmpty('Quotation')) echo 'disabled'; ?> id="search-button" class="btn btn-green btn-sm margr5"><span class="glyphicon glyphicon-search"></span> Search </button>
            @if(checkModul('O04'))
            <button type="button" class="btn btn-green margr5" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rSummary">
                <span class="glyphicon glyphicon-file"></span> Summary Report</button>
            <button type="button" class="btn btn-green margr5" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rDetail">
                <span class="glyphicon glyphicon-file"></span> Detail Report</button>
            @endif
            <a href="{{Route('quotationUpdate',$header->QuotationID)}}">
                <button id="btn-{{$header->QuotationID}}-update"
                        class="btn btn-green btn-sm margr5">
                    <span class="glyphicon glyphicon-edit"></span> Edit
                </button>
            </a>
            @if(checkModul('O04'))
            <div class="btn-group">
                                <a href="{{Route('quotationPrint',$header->QuotationID)}}" id="btn-{{$header->QuotationID}}-print" target='_blank' style="margin-right: 0px !important;">
                                    <button type="button" class="btn btn-green">
                                        <span class="glyphicon glyphicon-print"></span> Print </span>
                                    </button>
                                </a>
<!--                <a href="#" data-target="#r_print" data-internal="{{$header->QuotationID }}"  data-toggle="modal" role="dialog"
                   onclick="printAttach(this)" data-id="{{$header->QuotationID}}" data-name="{{$header->QuotationID}}">
                    <button type="button" class="btn btn-green">
                        <span class="glyphicon glyphicon-print"></span> Print
                    </button>
                </a>-->
                <!--                <a href="{{Route('quotationInternalPrint',$header->QuotationID)}}" id="btn-{{$header->QuotationID}}-print" target='_blank' style="margin-right: 0px !important;">
                                    <button type="button" class="btn btn-green">
                                        <span class="glyphicon glyphicon-print"></span> Internal </span>
                                    </button>
                                </a>-->
                @if($header->TypeCustomer == 0)
                <a href="{{asset('/')}}salesOrderNew?jenis=insertQuotation&customerSales={{QuotationHeader::find($header->InternalID)->coa6->InternalID}}&Quotation%5B%5D={{$header->QuotationID}}" id="btn-{{$header->QuotationID}}-print" target='_blank' style="margin-right: 0px !important;">
                    <button type="button" class="btn btn-green">
                        <span class="glyphicon glyphicon-plus"></span> Create SO </span>
                    </button>
                </a>
                @else
                <a href="{{asset('/')}}salesOrderNew?jenis=insertQuotation&customerSales={{QuotationHeader::find($header->InternalID)->CustomerName}}&Quotation%5B%5D={{$header->QuotationID}}" id="btn-{{$header->QuotationID}}-print" target='_blank' style="margin-right: 0px !important;">
                    <button type="button" class="btn btn-green">
                        <span class="glyphicon glyphicon-plus"></span> Create SO </span>
                    </button>
                </a>
                @endif
            </div>

            @endif
        </div>
        <!-- <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Search</h4>
            </div>
            <div class="tableadd">
                <ul class="searchmenu">
                    <form method="GET" action="{{Route('showQuotation')}}">
                        <li>
                            <label for="coa6">Customer</label>
                            <br>
                            <select class="chosen-select" id="coa6" style="" name="coa6">
                                <option value="-1">All Customer</option>
                                @foreach(Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $coa6)
                                <option value="{{$coa6->InternalID}}">
                                    {{$coa6->ACC6ID.' '.$coa6->ACC6Name}}
                                </option>
                                @endforeach
                            </select>
                        </li>
                        <li><label for="typePayment">Payment Type</label>
                            <br>
                            <select name="typePayment" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Cash</option>
                                <option value="1">Credit</option>
                            </select>
                        </li>
                        <li><label for="typeTax">Tax Type</label>
                            <br>
                            <select name="typeTax" style="width: 70px">
                                <option value="-1">All</option>
                                <option value="0">Non Tax</option>
                                <option value="1">Tax</option>
                            </select>
                        </li>
                        <li><label for="date">Start Date</label>
                            <br><input id="startDate" name="startDate" type="text" autocomplete="off">
                        </li>
                        <li><label for="date">End Date</label>
                            <br><input id="endDate" name="endDate" type="text" autocomplete="off">
                        </li>
                        <li><br>
                            <button type="submit" class="btn btn-green btn-sm">Search <span class="glyphicon glyphicon-search"></span></button>
                        </li>
                        <li><br>
                            <button id="cancel" type="reset"  class="btn btn-pure btn-sm">Cancel <span class="glyphicon glyphicon-remove"></span></button>
                        </li>
                    </form>
                </ul>
            </div><!---- end div tableadd---->
        <!--</div><!---- end div tabwrap---->
        @include('template.searchComponentTransactionModule')

        <div class="tabwrap">
            <div class="tabhead">
                <h4 class="headtitle">{{'Quotation '.$header->QuotationID}}</h4>
            </div>
            <div class="tableadd">
                <div class="headinv new">
                    <ul class="pull-left">
                        @if($header->Parent != NULL)
                        <li>
                            <label for="revisi">Replacement</label>
                            <span>{{$header->Parent}}</span>
                        </li>
                        @endif
                        <li>
                            <label for="date">Date</label>
                            <span>{{date( "d-m-Y", strtotime($header->QuotationDate))}}</span>
                        </li>
                        @if($header->TypeCustomer == 0)
                        <li>
                            <label for="customer">Customer</label>
                            <span><?php
                                $coa6 = QuotationHeader::find($header->InternalID)->coa6;
                                echo $coa6->ACC6ID . ' ' . $coa6->ACC6Name
                                ?></span>
                        </li>
                        @else
                        <li>
                            <label for="customer">Customer</label>
                            <span>{{$header->CustomerName}}</span>
                        </li>
                        @endif
                        <li class="hide">
                            <label for="payment">Payment</label>
                            @if($header->isCash == 0)
                            <span>{{'Cash'}}</span>
                            @elseif($header->isCash == 1)
                            <span>{{'Credit'}}</span>
                            @elseif($header->isCash == 2)
                            <span>{{'CBD'}}</span
                            @elseif($header->isCash == 3)
                            <span>{{'Deposit'}}</span>
                            @else
                            <span>{{'Down Payment'}}</span>
                            @endif
                        </li>
                        @if($header->isCash != 0 && $header->isCash != 2 && $header->isCash != 3)
                        <li class="hide">
                            <label for="longTerm">Due Date</label>
                            <span>{{date( "d-m-Y", strtotime("+".$header->LongTerm." day",strtotime($header->QuotationDate)))}}</span>
                        </li>
                        @endif
                        <li class="hide">
                            <label for="warehouse">Warehouse</label>
                            <span>{{$header->Warehouse->WarehouseName}}</span>
                        </li>
                        <li>
                            <label for="currency">Currency</label>
                            <span>{{$header->Currency->CurrencyName}}</span>
                        </li>
                        <li>
                            <label for="currency">Salesman</label>
                            <span>
                                {{SalesMan::find($header->SalesManInternalID)->SalesManName}}
                            </span>
                        </li>
                        <li>
                            <label for="rate">Rate</label>
                            <span>{{number_format($header->CurrencyRate,'2','.',',')}}</span>
                        </li>
                        <!--                        <li>
                                                    <label for="VAT">VAT</label>
                                                    @if($header->VAT == 0)
                                                    <span>{{'Non Tax'}}</span>
                                                    @else
                                                    <span>{{'Tax'}}</span>
                                                    @endif
                                                </li>-->
                        <!--                        <li>
                                                    <label for="refNumber">Ref Number</label>
                                                    <span>{{$header->RefNumber}}</span>
                                                </li>-->
                    </ul>
                    <ul class="pull-right">
                        <li>
                            <label for="">Remark</label>
                            <span>{{$header->Remark}}</span>
                        </li>
                        <li>
                            <label for="">Delivery Terms</label>
                            <span>{{$header->DeliveryTerms}}</span>
                        </li>
                        <li>
                            <label for="">Delivery Time</label>
                            <span>{{$header->DeliveryTime}}</span>
                        </li>
                        <li>
                            <label for="">Validity</label>
                            <span>{{$header->Validity}}</span>
                        </li>
                        <li>
                            <label for="">Term of Payment</label>
                            <span>{{$header->TermOfPayment}}</span>
                        </li>
                    </ul>
                </div>
                <table class="table master-data" id="table-quotation-description" style="table-layout:fixed;">
                    <thead>
                        <tr>
                            <th style="width: 25%;">Inventory</th>
                            <th style="width: 10%;">Uom</th>
                            <th style="width: 10%;">Qty</th>
                            <th style="width: 15%;">Price</th>
                            <th style="width: 10%;">Disc (%)</th>
                            <th style="width: 10%;">Disc</th>
                            <th style="width: 15%;">Subtotal</th>
                            <th style="width: 14%;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $total = 0;
                        $barisDesc = 1;
                        $totalVAT = 0;
                        ?>
                        @foreach(QuotationDescription::where('QuotationInternalID',$header->InternalID)->get() as $description)
                        <tr style='background: #F1FFDB;' id='rowDescription{{$barisDesc}}'>
                            <td class='chosen-uom'>
                                {{$description->InventoryText}}
                            </td>
                            <td>
                                {{$description->UomText}}
                            </td>
                            <td class='text-right'>
                                {{$description->Qty}}
                            </td>
                            <td class='text-right'>
                                {{number_format($description->Price,'2','.',',')}}
                            </td>
                            <td class='text-right'>
                                {{$description->Discount}}
                            </td>
                            <td class='text-right'>
                                {{number_format($description->DiscountNominal,'2','.',',')}}
                            </td>
                            <td class="right subtotalDescription" id="priceDescription-{{$barisDesc}}-qty-hitung">{{number_format(ceil($description->SubTotal),'0','.',',')}}</td>
                            <td>
                                <button class="btn btn-pure-xs btn-xs btn-toggle-detail margr5" type="button" data-toshow="show" data="rowDescription{{$barisDesc}}"><span class="glyphicon glyphicon-eye-open"></span></button>
                                <!--<button class="btn btn-pure-xs btn-xs btn-toggle-spec" type="button" data-toshow="show" data="rowSpec{{$barisDesc}}"><span class="glyphicon glyphicon-comment"></span></button>-->
                            </td>
                        </tr>
<!--                        <tr id='rowSpec{{$barisDesc}}'>
                            <td colspan='8' class='rowSpec{{$barisDesc}}' style="text-align: left">
                                {{nl2br($description->Spesifikasi)}}
                            </td>
                        </tr>-->
                        <?php
                        $barisDetail = 1;
                        ?>
                        @foreach(QuotationDetail::where('DescriptionInternalID',$description->InternalID)->get() as $detail)
                        <!--untuk non-parcel-->
                        @if($detail->QuotationParcelInternalID == 0)
                        <tr id='row{{$barisDetail}}' class='rowDescription{{$barisDesc}}'>
                            <td class='chosen-uom'>
                                <input type="hidden" class="inventory" style="width: 100px" id="inventory-{{$barisDetail}}" name="inventory{{$barisDesc}}[]" value="{{$detail->InventoryInternalID}}---;---inventory">{{Inventory::find($detail->InventoryInternalID)->InventoryID.' '.Inventory::find($detail->InventoryInternalID)->InventoryName}}
                            </td>
                            <td>
                                {{Uom::find($detail->UomInternalID)->UomID}}
                            </td>
                            <td class='text-right'>
                                {{number_format($detail->Qty,'0','.',',')}}
                            </td>
                            <td class='text-right'>
                                {{number_format($detail->Price,'2','.',',')}}
                            </td>
                            <td class='text-right'>
                                {{$detail->Discount}}
                            </td>
                            <td class='text-right'>
                                {{number_format($detail->DiscountNominal,'2','.',',')}}
                            </td>
                            <td class="right subtotal" id="price-{{$barisDetail}}-qty-hitung">{{number_format(ceil($detail->SubTotal),'0','.',',')}}</td>
                            <td>
                                -
                            </td>
                            {{'';
//                                $totalVAT += $detail->VAT
$totalVAT += ceil($detail->SubTotal)/10;
                            }}
                        </tr>
                        <?php
                        $barisDetail++;
                        $total += $detail->SubTotal;
                        ?>
                        @endif
                        @endforeach
                        <!--untuk parcel-->
                        @foreach(QuotationParcel::where('DescriptionInternalID',$description->InternalID)->get() as $parcel)
                        <tr id='row{{$barisDetail}}' class='rowDescription{{$barisDesc}}'>
                            <td class='chosen-uom'>
                                {{Parcel::find($parcel->ParcelInternalID)->ParcelID.' '.Parcel::find($parcel->ParcelInternalID)->ParcelName}}
                            </td>
                            <td>
                                -
                            </td>
                            <td class='text-right'>
                                {{number_format($parcel->Qty,'0','.',',')}}
                            </td>
                            <td>
                                {{number_format($parcel->Price,'2','.',',')}}
                            </td>
                            <td class='text-right'>
                                {{$parcel->Discount}}
                            </td>
                            <td class='text-right'>
                                {{number_format($parcel->DiscountNominal,'2','.',',')}}
                            </td>
                            <td class="right subtotal" id="price-{{$barisDetail}}-qty-hitung">{{number_format($parcel->SubTotal,'2','.',',')}}</td>
                            <td>
                                -
                            </td>
                            {{'';$totalVAT += $parcel->VAT}}
                        </tr>
                        <?php
                        $barisDetail++;
                        $total += $parcel->SubTotal;
                        ?>
                        @endforeach
                        <?php $barisDesc++; ?>
                        @endforeach
                        @if($totalVAT != 0)
                        {{'';$totalVAT = $totalVAT - $header->DiscountGlobal*0.1;}}
                        @endif
                    </tbody>
                </table>
                <table class="pull-right">
                    <tr>
                        <td><h5 class="right margr10 h5total"><b>Total</b></h5></td>
                        <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                        <td><h5 class="right margr10 h5total"><b id="total">{{number_format(ceil($total),'0','.',',')}}</b></h5></td>
                    </tr>
                    <tr >
                        <td><h5 class="right margr10 h5total"><b>Discount</b></h5></td>
                        <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                        <td><h5 class="right margr10 h5total new"><b>{{number_format($header->DiscountGlobal, '0', '.',',')}}</b></h5></td>
                    </tr>
                    <tr>
                        <td><h5 class="right margr10 h5total"><b>Grand Total</b></h5></td>
                        <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                        <td><h5 class="right margr10 h5total"><b id="grandTotal">{{number_format(ceil($total-$header->DiscountGlobal),'0','.',',')}}</b></h5></td>
                    </tr>
                    <tr>
                        <td><h5 class="right margr10 h5total hidevat"><b>Tax</b></h5></td>
                        <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                        <td><h5 class="right margr10 h5total hidevat"><b id="tax">{{number_format(floor($totalVAT),'0','.',',')}}</b></h5></td>
                    </tr>
                    <tr>
                        <td><h5 class="right margr10 h5total hidevat"><b>Grand Total(tax)</b></h5></td>
                        <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                        <td><h5 class="right margr10 h5total hidevat"><b id="grandTotalAfterTax">{{number_format(ceil($header->GrandTotal),'0','.',',')}}</b></h5></td>
                    </tr>
                </table>
            </div><!---- end div tableadd---->
        </div><!---- end div tabwrap---->
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->

@stop

@section('modal')
<div class="modal fade" id="r_summary" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Summary Report</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="" target="_blank">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='summaryQuotation'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateReport" data-validation="required">
                            </li>
                        </ul>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="r_print" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Tax</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="" target="_blank">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='printQuotation'>
                            <input type="hidden" name="internalID" id="internalID" />
                            <li>
                                <label for="type">Type Tax</label> *
                            </li>
                            <li>
                                <select class="form-control" name="type">
                                    <option value="include">Include</option>
                                    <option value="exclude">Exclude</option>
                                </select>
                            </li>
                        </ul>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script><script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNewJournal.jquery.js')}}" type="text/javascript"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script>
                       var quotationDataBackup = '<?php echo Route('quotationDataBackup', Input::get('coa6') . '---;---' . Input::get('typePayment') . '---;---' . Input::get('typeTax') . '---;---' . Input::get('startDate') . '---;---' . Input::get('endDate')) ?>';
                       $(document).ready(function () {
                           var ppn = '<?php echo $header->VAT ?>'
                           if (ppn == 1) {
                               $('.hidevat').show();
                           } else {
                               $('.hidevat').hide();
                           }
                       });
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-quotation/quotation.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
@stop
