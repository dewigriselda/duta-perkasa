@extends('template.header-footer')

@section('title')
Quotation
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosenCustom.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<style>
    .help-block {
        margin-left: 30% !important;
    }
</style>
@stop

@section('nav')

@stop

@section('content')
@if(myCheckIsEmpty('Customer;Warehouse;Currency;Inventory'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one master Customer, Currency, Warehouse, and Inventory to insert quotation.
</div>
@endif
@if(isset($messages))
@if($messages == 'gagalInsert')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New quotation has been inserted.
</div>
<input type="hidden" name="print" id="print" value="{{$print}}">
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showQuotation')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Quotation</a>
                <a href="{{route('quotationNew')}}" type="button" class="btn btn-sm btn-pure">New Quotation</a>
            </div>
            <div class="btn-group margr5">
                <a href="{{Route('quotationNew')}}">
                    <button type="button" <?php if (myCheckIsEmpty('Customer;Warehouse;Currency;Inventory')) echo 'disabled'; ?> class="btn btn-green btn-sm dropdown-toggle  " aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
                </a>
            </div>
            <button id="search-button" <?php if (myCheckIsEmpty('Quotation')) echo 'disabled'; ?> class="btn btn-green btn-sm margr5"><span class="glyphicon glyphicon-search"></span> Search </button>
            @if(checkModul('O04'))
            <button type="button" class="btn btn-green margr5" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rSummary">
                <span class="glyphicon glyphicon-file"></span> Summary Report</button>
            <button type="button" class="btn btn-green" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rDetail">
                <span class="glyphicon glyphicon-file"></span> Detail Report</button>
            @endif
        </div>
        <!-- <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Search</h4>
            </div>
            <div class="tableadd">
                <ul class="searchmenu">
                    <form method="GET" action="{{Route('showQuotation')}}">
                        <li>
                            <label for="coa6">Customer *</label>
                            <br>
                            <select class="chosen-select" id="coa6" style="" name="coa6">
                                <option value="-1">All Customer</option>
                                @foreach(Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $coa6)
                                <option value="{{$coa6->InternalID}}">
                                    {{$coa6->ACC6ID.' '.$coa6->ACC6Name}}
                                </option>
                                @endforeach
                            </select>
                        </li>
                        <li><label for="typePayment">Payment Type</label>
                            <br>
                            <select name="typePayment" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Cash</option>
                                <option value="1">Credit</option>
                            </select>
                        </li>
                        <li><label for="typeTax">Tax Type</label>
                            <br>
                            <select name="typeTax" style="width: 70px">
                                <option value="-1">All</option>
                                <option value="0">Non Tax</option>
                                <option value="1">Tax</option>
                            </select>
                        </li>
                        <li><label for="date">Start Date</label>
                            <br><input id="startDate" name="startDate" type="text" autocomplete="off">
                        </li>
                        <li><label for="date">End Date</label>
                            <br><input id="endDate" name="endDate" type="text" autocomplete="off">
                        </li>
                        <li><br>
                            <button type="submit" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-search"></span> Search </button>
                        </li>
                        <li><br>
                            <button id="cancel" type="reset"  class="btn btn-pure btn-sm"><span class="glyphicon glyphicon-remove"></span> Cancel </button>
                        </li>
                    </form>
                </ul>
            </div><!---- end div tableadd---->
        <!--</div><!---- end div tabwrap---->
        @include('template.searchComponentTransactionModule')

        <form method="POST" action="" id="form-insert">
            <div class="tabwrap">
                <div class="tabhead">
                    <h4 class="headtitle">Quotation <span id="quotationID">{{QuotationHeader::getNextIDQuotation($quotation)}}</span></h4>
                </div>
                <div class="tableadd">
                    <div class="headinv new">
                        <ul class="pull-left">
                            <li>
                                <label for="date">Date *</label>
                                <input id="date" name="date" type="text" autocomplete="off" data-validation="required">
                            </li>
                            <li>
                                <label>Customer</label>
                                <input type="radio" name="jeniscustomer" id="checkcuslama" value="cuslama" checked> Existing Customer
                                <input type="radio" name="jeniscustomer" id="checkcusbaru" value="cusbaru"> New Customer
                            </li>
                            <div id="divcustomer">
                                <li>
                                    <label for="coa6">Name *</label>
                                    <select class="chosen-select" id="coa6_input" style="" name="coa6">
                                        @foreach(Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $coa6)
                                        <option value="{{$coa6->InternalID}}">
                                            {{$coa6->ACC6ID.' '.$coa6->ACC6Name}}
                                        </option>
                                        @endforeach
                                    </select>
                                </li>
                                <li id="customercpdiv">

                                </li>
                                <li id="customermanagerdiv">

                                </li>
                            </div>
                            <div id="divcustomer2" style="display: none">
                                <li>
                                    <label for="longTerm">Name *</label>
                                    <input type="text" name="name">
                                </li>
                                <li>
                                    <label for="longTerm">CP *</label>
                                    <input type="text" name="cp">
                                </li>
                                <li>
                                    <label for="longTerm">Phone *</label>
                                    <input type="text" name="phone">
                                </li>
                                <li>
                                    <label for="longTerm">Fax *</label>
                                    <input type="text" name="fax">
                                </li>
                                <li>
                                    <label for="longTerm">Email *</label>
                                    <input type="email" name="email">
                                </li>
                                <li>
                                    <label for="remark">Address</label>
                                    <textarea name="address" id="address"></textarea>
                                </li>
                                <li>
                                    <label for="longTerm">Customer Manager *</label>
                                    <select class="chosen-select" style="" name="salesman_baru">
                                        @foreach(SalesMan::where('CompanyInternalID', Auth::user()->Company->InternalID)->where('Status',1)->get() as $sm)
                                        <option value="{{$sm->InternalID}}">
                                            {{$sm->SalesManID.' '.$sm->SalesManName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </li>
                            </div>
                            <li>
                                <label for="">Sales Assistant</label>
                                <span>{{Auth::user()->UserName}}</span>
                            </li>
                            <li class="hide">
                                <label for="payment">Payment *</label>
                                <input type="radio" class="radio-tipe" id="paymentCash" name="isCash" value="0" checked="checked">Cash
                                <input type="radio" class="radio-tipe" id="paymentCredit" name="isCash" value="1">Credit
                            </li>
                            <!--                            <li>
                                <label for="payment">Payment *</label>
                                <select name="isCash" id="payment">
                                    <option value="0" selected>Cash</option>
                                    <option value="1">Credit</option>
                                    <option value="2">CBD</option>
                                    <option value="3">Deposit</option>
                                    <option value="4">Down Payment</option>
                                </select>
                            </li>
                            <li class="downpayment">
                                <label for="longTerm">Down Payment (%) *</label>
                                <input type="text" name="downPayment" value="0" id="downPayment" class="numajaDesimal">
                            </li>-->
                            <li class="hide">
                                <label for="longTerm">Long Term *</label>
                                <input type="text" name="longTerm" value="0" id="longTerm" disabled="true">
                            </li>
                            <li style="display:none">
                                <label for="warehouse">Warehouse *</label>
                                <select class="chosen-select choosen-modal currency" id="warehouseHeader" name="warehouse">
                                    @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $war)
                                    <option id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                        {{$war->WarehouseName;}}
                                    </option>
                                    @endforeach
                                </select>
                            </li>
                            <!--                            <li>
                                                            <label for="salesman">Salesman *</label>
                                                            <select class="chosen-select" id="salesman" style="" name="salesman" required>
                                                                @foreach(SalesMan::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $salesman)
                                                                <option value="{{$salesman->InternalID}}">
                                                                    {{$salesman->SalesManName}}
                                                                </option>
                                                                @endforeach
                                                            </select>
                                                        </li>-->
                            <li>
                                <label for="currency">Currency *</label>
                                <select class="chosen-select choosen-modal currency" id="currencyHeader" name="currency">
                                    {{'';$default = 0;}}
                                    @foreach(Currency::orderBy('Default','desc')->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $cur)
                                    <option id="cur{{$cur->InternalID}}" value="{{$cur->InternalID.'---;---'.$cur->CurrencyName.'---;---'.$cur->Rate.'---;---'.$cur->Default}}">
                                        {{$cur->CurrencyName;}}
                                        @if($default == 0)
                                        {{'';$default=$cur->Rate}}
                                        @endif
                                    </option>
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label for="rate">Rate *</label>
                                <input type="text" class="maxWidth rate numajaDesimal" name="rate" maxlength="" id="rate" value="{{$default}}" data-validation="required">
                            </li>
                            <!--                            <li>
                                                            <label for="refNumber">Ref Number</label>
                                                            <input type="text" name="RefNumber" value="">
                                                        </li>-->
                        </ul>
                        <ul class="pull-right">
                            <li id="type">
                                <label for="type">Price Type *</label>
                                <select name="typeTax" id="typeTax">
                                    <option value="Exclude">Exclude</option>
                                    <option value="Include">Include</option>
                                </select>
                            </li>
                            <li>
                                <label for="remark">Remark</label>
                                <textarea name="remark" id="remark"></textarea>
                            </li>
                            <li>
                                <label for="longTerm">Delivery Terms</label>
                                <input type="text" name="DeliveryTerms" value="Franco Surabaya">
                            </li>
                            <li>
                                <label for="longTerm">Delivery Time</label>
                                <input type="text" name="DeliveryTime">
                            </li>
                            <li>
                                <label for="validity">Validity</label>
                                <input type="text" name="Validity" value="3 days">
                            </li>
                            <li>
                                <label for="termOfPayment">Term of Payment</label>
                                <input type="text" name="TermOfPayment" value="Cash Before Delivery">
                            </li>
                            <li>
                                <div class="required">
                                    * Required
                                </div>
                            </li>
                            <li style="visibility: hidden">
                                <label for="vat">VAT *</label>
                                <input  @if(Auth::user()->SeeNPPN == 0)checked @endif style="width:15px; height: 15px;" type="checkbox" name="vat" id="vat" value="1"> Tax 10%
                            </li>
                        </ul>
                    </div>
                    <div class="tableadd journupdate">
                        <label>Description</label>
                        <table class="table master-data table-striped" id="table-quotation-description-header" style="table-layout:fixed;">
                            <thead class="default-thead">
                                <tr>
                                    <th style="width: 25%;">Inventory</th>
                                    <th style="width: 10%;">Uom</th>
                                    <th style="width: 10%;">Qty</th>
                                    <th style="width: 15%;">Price</th>
                                    <th style="width: 10%;">Disc (%)</th>
                                    <th style="width: 10%;">Disc</th>
                                    <th style="width: 15%;">Subtotal</th>
                                    <th class="text-center" style="width: 5%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="row0">
                                    <td class="chosen-transaction">
                                        <input class="input-theme" type="text" id="InventoryDescription-0" placeholder="Inventory Name">
                                    </td>
                                    <td>
                                        <input type="text" class="input-theme uomDescription" id="uomDescription-0" placeholder="uom">
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth qtyDescription right numaja input-theme" maxlength="11" min="1" id="priceDescription-0-qty" value="1">
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth priceDescription right numajaDesimal" maxlength="" id="priceDescription-0" value="0">
                                    </td>
                                    <td class="text-right">
                                        <input disabled type="text" class="maxWidth discountDescription right numajaDesimal input-theme" id="priceDescription-0-discount" value="0.00">
                                    </td>
                                    <td class="text-right">
                                        <input disabled type="text" class="maxWidth discountNominalDescription right numajaDesimal input-theme" id="priceDescription-0-discountNominal" value="0.00">
                                    </td>
                                    <td id="priceDescription-0-qty-hitung" class="right">
                                        0.00
                                    </td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-green btn-sm" id="btn-addRow-description"><span class="glyphicon glyphicon-plus"></span></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <label>Detail</label>
                        <table class="table master-data  table-striped" id="table-quotation-header" style="table-layout:fixed;">
                            <thead  class="default-thead">
                                <tr>
                                    <th style="width: 25%;">Inventory</th>
                                    <th style="width: 8%;">Uom</th>
                                    <th style="width: 7%;">Stock</th>
                                    <th style="width: 10%;">Description</th>
                                    <th style="width: 8%;">Qty</th>
                                    <th style="width: 10%;">Original Price</th>
                                    <th style="width: 15%;">Price</th>
                                    <th style="width: 7%;">Disc (%)</th>
                                    <th style="width: 10%;">Disc</th>
                                    <th style="width: 10%;">Subtotal</th>
                                    <th class="text-center" style="width: 5%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="row0">
                                    <td class="chosen-transaction">
                                        <input class="input-theme margbot10" type="text" id="searchInventory" tabindex="-1" title="Type Inventory Name or ID then 'Enter'" placeholder="Type Inventory Name or ID then 'Enter'">
                                        <div id="selectInventory">

                                        </div>
                                    </td>
                                    <td>
                                        <select id="uom-0" class="input-theme uom">

                                        </select>
                                    </td>
                                    <td class="text-right">
                                        <span class="maxWidth stock" id="price-0-stock" data-toggle="popover" data-placement="bottom" data-html="true">-</span>
                                    </td>
                                    <td>
                                        <select id="description-0" class="input-theme description">

                                        </select>
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth qty right numaja input-theme" maxlength="11" min="1" id="price-0-qty" value="1">
                                    </td>
                                    <td class="text-right" id="oriprice-0">
                                        0.00
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth price right numajaDesimal input-theme" maxlength="" id="price-0" value="0">
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth discount right numajaDesimalMinus input-theme" id="price-0-discount" value="0.00">
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth discountNominal right numajaDesimalMinus input-theme" id="price-0-discountNominal" value="0.00">
                                    </td>
                                    <td id="price-0-qty-hitung" class="right">
                                        0.00
                                    </td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-green btn-sm" disabled="" id="btn-addRow"><span class="glyphicon glyphicon-plus"></span></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <table class="table master-data" id="table-quotation-description" style="table-layout:fixed;">
                            <thead>
                                <tr>
                                    <th style="width: 25%;">Inventory</th>
                                    <th style="width: 10%;">Uom</th>
                                    <th style="width: 10%;">Stock</th>
                                    <th style="width: 10%;">Qty</th>
                                    <th style="width: 15%;">Price</th>
                                    <th style="width: 10%;">Disc (%)</th>
                                    <th style="width: 10%;">Disc</th>
                                    <th style="width: 15%;">Subtotal</th>
                                    <th style="width: 14%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot id="no-in-description">
                                <tr style="background: #fafafa;" >
                                    <td colspan="8" class="text-center">no data available.</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div><!---- end div tableadd---->
                    <input type="hidden" name="grandTotalValue" value="0" id="grandTotalValue">
                    <table class="pull-right">
                        <tr>
                            <td><h5 class="right margr10 h5total"><b>Total</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="total"></b></h5></td>
                        </tr>
                        <tr >
                            <td><h5 class="right margr10 h5total"><b>Discount</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total new" style="width: 150px;"><input type="text" style="width: 120px;" class="maxWidth right numaja discountGlobal" name="DiscountGlobal" maxlength="" id="discountGlobal" value="0"></h5></td>
                        </tr>
                        <tr>
                            <td><h5 class="right margr10 h5total"><b>Grand Total</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="grandTotal"></b></h5></td>
                        </tr>

                        <tr>
                            <td><h5 class="right margr10 h5total hidevat"><b>Tax</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="tax"></b></h5></td>
                        </tr>
                        <tr>
                            <td><h5 class="right margr10 h5total hidevat"><b>Grand Total(tax)</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="grandTotalAfterTax"></b></h5></td>
                        </tr>
                    </table>
                </div><!---- End div tab-content--->
            </div><!---- end div tableadd---->
    </div><!---- end div tabwrap---->
    <div class="btnnest pull-right">
        <button class="btn btn-green btn-sm btn-save" id="btn-save" <?php if (myCheckIsEmpty('Customer;Warehouse;Currency;Inventory;Supplier')) echo 'disabled'; ?>> Save and Print</button>
    </div>
</form>
</div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop

@section('modal')
<div class="modal fade" id="r_summary" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Summary Report</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="" target="_blank">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='summaryQuotation'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateReport" data-validation="required">
                            </li>
                        </ul>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNewJournal.jquery.js')}}" type="text/javascript"></script>
<script>
$("form").bind("submit", function (event) {
    setTimeout(function () {
        if ($(".form-error")[0]) {
            $("#btn-save").prop("disabled", false);
        } else {
            $("#btn-save").prop("disabled", true);
        }
    }, 100);

});
var getUomThisInventory = "<?php echo Route("getUomThisInventory") ?>";
var getCustomerManager = "<?php echo Route("getCustomerManager") ?>";
var getCustomerCP = "<?php echo Route("getCustomerCp") ?>";
var getPriceDefault = "<?php echo Route("getPriceDefault") ?>";
var getPriceRangeThisInventoryQuotation = "<?php echo Route("getPriceRangeThisInventoryQuotation") ?>";
var getPriceThisParcel = "<?php echo Route("getPriceThisParcel") ?>";
var getPriceRangeSO = "<?php echo Route("getPriceRangeSO") ?>";
var getStockInventorySO = "<?php echo Route("getStockInventorySO") ?>";
var getSearchResultInventoryForQuotation = "<?php echo Route("getSearchResultInventoryForQuotation") ?>";
var textSelect = '';
var tanggalHariIni = '<?php echo date('d') . '-' . date('m') . '-' . date('Y') ?>';
var cariS = '<?php echo Route('formatCariIDQuotation') ?>';
var baris = 1;
var description = [];
</script>
<script>
    var quotationDataBackup = '<?php echo Route('quotationDataBackup', Input::get('coa6') . '---;---' . Input::get('typePayment') . '---;---' . Input::get('typeTax') . '---;---' . Input::get('startDate') . '---;---' . Input::get('endDate')) ?>';
</script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-quotation/quotation.js')}}"></script>
<script type="text/javascript" src="{{ Asset('js/entry-js-quotation/quotationNew.js') }}"></script>
<script type="text/javascript" src="{{ Asset('js/entry-js-quotation/quotationNewDescription.js') }}"></script>
@stop
