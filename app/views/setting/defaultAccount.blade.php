@extends('template.header-footer')

@section('title')
Default Account
@stop

@section('css')
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<style>
    .help-block {
        margin-left: 22.5% !important;
    }
</style>
@stop

@section('nav')

@stop

@section('content')
@if(myCheckIsEmpty('Coa'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one master COA to insert default COA.
</div>
@endif
@if(Session::get('messages') == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@if(Session::get('messages') == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Default Account has been updated.
</div>
@endif
@if(isset($messages))
@if($messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontentnopadd">
        <div class="primcontent"> 
            <div class="btnnest hidden-xs"> 
                <div class="btn-group bread margr30min" role="group">
                    <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                    <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                    <a href="{{route('showDefaultAccount')}}" type="button" class="btn btn-sm btn-pure">Default Account</a>
                </div>
            </div>
            <div class="bread-xs visible-xs hidden-sm hidden-md hidden-lg">
                <p class="text-center"><a href="{{route('showDashboard')}}">{{Config::get('companyHeader.header_company');}}</a> / <a>{{ucfirst($toogle)}}</a> / <a href="{{route('showDefaultAccount')}}">Default Account</a></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form action="" method="post" class="actiondefault" id="form-insert" enctype="multipart/form-data">
                    <div class="tabwrap">
                        <div class="tabhead">
                            <h4 class="headtitle">Default Account</h4>
                        </div>
                        <div class="tableadd"> 
                            <div class="row">
                                <div class="col-md-10">
                                    <div>
                                        <input type='hidden' name='jenis' value='insertDefaultAccount'>
                                        {{'';$default_s = Default_s::where("CompanyInternalID", Auth::user()->Company->InternalID)->where('Type',0)->count();
                                    $coa1 = -1;$coa2 = -1;$coa3 = -1;$coa4 = -1;$coa5 = -1;$coa6 = -1;}}
                                        <div class="margbot10 row">
                                            <div class="col-md-4">
                                                <p class="text-left"><label for="Sales">Sales *</label></p> 
                                            </div>
                                            <div class="col-md-6">
                                                <select class="chosen-select" id="SalesCoa" name="salesCoa">
                                                    @if ($default_s > 0)
                                                    {{'';
                                                $coa1 = Default_s::getACCInternalID('Sales','1');
                                                $coa2 = Default_s::getACCInternalID('Sales','2');
                                                $coa3 = Default_s::getACCInternalID('Sales','3');
                                                $coa4 = Default_s::getACCInternalID('Sales','4');
                                                $coa5 = Default_s::getACCInternalID('Sales','5');
                                                $coa6 = Default_s::getACCInternalID('Sales','6');
                                                    }}
                                                    @endif
                                                    @foreach(Coa::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('ACC1InternalID')->orderBy('InternalID')->get() as $coa)
                                                    <option <?php echo Coa::cekCOA($coa->ACC1InternalID, $coa->ACC2InternalID, $coa->ACC3InternalID, $coa->ACC4InternalID, $coa->ACC5InternalID, $coa->ACC6InternalID, $coa1, $coa2, $coa3, $coa4, $coa5, $coa6) ?>
                                                        value="{{$coa->InternalID}}">
                                                        {{Coa::formatCoa($coa->ACC1InternalID,$coa->ACC2InternalID,$coa->ACC3InternalID,$coa->ACC4InternalID,$coa->ACC5InternalID,$coa->ACC6InternalID,'1')}} 
                                                        {{" ".$coa->COAName}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="margbot10 row">

                                            <div class="col-md-4">
                                                <p class=""><label for="Receiveable">Receivable *</label> </p>
                                            </div>
                                            <div class="col-md-6">
                                                <select class="chosen-select" id="ReceiveableCoa" style="" name="receiveableCoa">
                                                    @if ($default_s > 0) 
                                                    {{'';
                                                $coa1 = Default_s::getACCInternalID('Receivable','1');
                                                $coa2 = Default_s::getACCInternalID('Receivable','2');
                                                $coa3 = Default_s::getACCInternalID('Receivable','3');
                                                $coa4 = Default_s::getACCInternalID('Receivable','4');
                                                $coa5 = Default_s::getACCInternalID('Receivable','5');
                                                $coa6 = Default_s::getACCInternalID('Receivable','6');
                                                    }} 
                                                    @endif
                                                    @foreach(Coa::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('ACC1InternalID')->orderBy('InternalID')->get() as $coa)
                                                    <option <?php echo Coa::cekCOA($coa->ACC1InternalID, $coa->ACC2InternalID, $coa->ACC3InternalID, $coa->ACC4InternalID, $coa->ACC5InternalID, $coa->ACC6InternalID, $coa1, $coa2, $coa3, $coa4, $coa5, $coa6) ?>
                                                        value="{{$coa->InternalID}}">
                                                        {{Coa::formatCoa($coa->ACC1InternalID,$coa->ACC2InternalID,$coa->ACC3InternalID,$coa->ACC4InternalID,$coa->ACC5InternalID,$coa->ACC6InternalID,'1')}} 
                                                        {{" ".$coa->COAName}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>

                                        </div>
                                        <div class="margbot10 row">

                                            <div class="col-md-4">
                                                <p><label for="SalesTax">Sales Tax *</label></p> 
                                            </div>
                                            <div class="col-md-6">
                                                <select class="chosen-select" id="SalesTaxCoa" style="" name="salesTaxCoa">
                                                    @if ($default_s > 0) 
                                                    {{'';
                                                $coa1 = Default_s::getACCInternalID('Sales Tax','1');
                                                $coa2 = Default_s::getACCInternalID('Sales Tax','2');
                                                $coa3 = Default_s::getACCInternalID('Sales Tax','3');
                                                $coa4 = Default_s::getACCInternalID('Sales Tax','4');
                                                $coa5 = Default_s::getACCInternalID('Sales Tax','5');
                                                $coa6 = Default_s::getACCInternalID('Sales Tax','6');
                                                    }}
                                                    @endif
                                                    @foreach(Coa::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('ACC1InternalID')->orderBy('InternalID')->get() as $coa)
                                                    <option <?php echo Coa::cekCOA($coa->ACC1InternalID, $coa->ACC2InternalID, $coa->ACC3InternalID, $coa->ACC4InternalID, $coa->ACC5InternalID, $coa->ACC6InternalID, $coa1, $coa2, $coa3, $coa4, $coa5, $coa6) ?>
                                                        value="{{$coa->InternalID}}">
                                                        {{Coa::formatCoa($coa->ACC1InternalID,$coa->ACC2InternalID,$coa->ACC3InternalID,$coa->ACC4InternalID,$coa->ACC5InternalID,$coa->ACC6InternalID,'1')}} 
                                                        {{" ".$coa->COAName}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="margbot10 row">

                                            <div class="col-md-4">
                                                <p><label for="Payable">Payable *</label></p> 
                                            </div>
                                            <div class="col-md-6">
                                                <select class="chosen-select" id="PayableCoa" style="" name="payableCoa">
                                                    @if ($default_s > 0) 
                                                    {{'';
                                                $coa1 = Default_s::getACCInternalID('Payable','1');
                                                $coa2 = Default_s::getACCInternalID('Payable','2');
                                                $coa3 = Default_s::getACCInternalID('Payable','3');
                                                $coa4 = Default_s::getACCInternalID('Payable','4');
                                                $coa5 = Default_s::getACCInternalID('Payable','5');
                                                $coa6 = Default_s::getACCInternalID('Payable','6');
                                                    }}
                                                    @endif
                                                    @foreach(Coa::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('ACC1InternalID')->orderBy('InternalID')->get() as $coa)
                                                    <option <?php echo Coa::cekCOA($coa->ACC1InternalID, $coa->ACC2InternalID, $coa->ACC3InternalID, $coa->ACC4InternalID, $coa->ACC5InternalID, $coa->ACC6InternalID, $coa1, $coa2, $coa3, $coa4, $coa5, $coa6) ?>
                                                        value="{{$coa->InternalID}}">
                                                        {{Coa::formatCoa($coa->ACC1InternalID,$coa->ACC2InternalID,$coa->ACC3InternalID,$coa->ACC4InternalID,$coa->ACC5InternalID,$coa->ACC6InternalID,'1')}} 
                                                        {{" ".$coa->COAName}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="margbot10 row">
                                            <div class="col-md-4">
                                                <p><label for="PurchaseTax">Purchase Tax *</label></p> 
                                            </div>
                                            <div class="col-md-6">
                                                <select class="chosen-select" id="PurchaseTaxCoa" style="" name="purchaseTaxCoa">
                                                    @if ($default_s > 0) 
                                                    {{'';
                                                $coa1 = Default_s::getACCInternalID('Purchase Tax','1');
                                                $coa2 = Default_s::getACCInternalID('Purchase Tax','2');
                                                $coa3 = Default_s::getACCInternalID('Purchase Tax','3');
                                                $coa4 = Default_s::getACCInternalID('Purchase Tax','4');
                                                $coa5 = Default_s::getACCInternalID('Purchase Tax','5');
                                                $coa6 = Default_s::getACCInternalID('Purchase Tax','6');
                                                    }}
                                                    @endif
                                                    @foreach(Coa::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('ACC1InternalID')->orderBy('InternalID')->get() as $coa)
                                                    <option <?php echo Coa::cekCOA($coa->ACC1InternalID, $coa->ACC2InternalID, $coa->ACC3InternalID, $coa->ACC4InternalID, $coa->ACC5InternalID, $coa->ACC6InternalID, $coa1, $coa2, $coa3, $coa4, $coa5, $coa6) ?>
                                                        value="{{$coa->InternalID}}">
                                                        {{Coa::formatCoa($coa->ACC1InternalID,$coa->ACC2InternalID,$coa->ACC3InternalID,$coa->ACC4InternalID,$coa->ACC5InternalID,$coa->ACC6InternalID,'1')}} 
                                                        {{" ".$coa->COAName}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="margbot10 row">
                                            <div class="col-md-4">
                                                <p><label for="ClosedJournal">Closed Journal *</label></p>
                                            </div>
                                            <div class="col-md-6">
                                                <select class="chosen-select" id="ClosedJournalCoa" style="" name="closedJournalCoa">
                                                    @if ($default_s > 0) 
                                                    {{'';
                                                $coa1 = Default_s::getACCInternalID('Closed Journal','1');
                                                $coa2 = Default_s::getACCInternalID('Closed Journal','2');
                                                $coa3 = Default_s::getACCInternalID('Closed Journal','3');
                                                $coa4 = Default_s::getACCInternalID('Closed Journal','4');
                                                $coa5 = Default_s::getACCInternalID('Closed Journal','5');
                                                $coa6 = Default_s::getACCInternalID('Closed Journal','6');
                                                    }}
                                                    @endif
                                                    @foreach(Coa::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('ACC1InternalID')->orderBy('InternalID')->get() as $coa)
                                                    <option <?php echo Coa::cekCOA($coa->ACC1InternalID, $coa->ACC2InternalID, $coa->ACC3InternalID, $coa->ACC4InternalID, $coa->ACC5InternalID, $coa->ACC6InternalID, $coa1, $coa2, $coa3, $coa4, $coa5, $coa6) ?>
                                                        value="{{$coa->InternalID}}">
                                                        {{Coa::formatCoa($coa->ACC1InternalID,$coa->ACC2InternalID,$coa->ACC3InternalID,$coa->ACC4InternalID,$coa->ACC5InternalID,$coa->ACC6InternalID,'1')}} 
                                                        {{" ".$coa->COAName}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="margbot10 row">

                                            <div class="col-md-4">
                                                <p><label for="Mpl">Moving Profit and Loss *</label></p> 
                                            </div>
                                            <div class="col-md-6">
                                                <select class="chosen-select" id="MplCoa" style="" name="mplCoa">
                                                    @if ($default_s > 0) 
                                                    {{'';
                                                $coa1 = Default_s::getACCInternalID('Moving Profit and Loss','1');
                                                $coa2 = Default_s::getACCInternalID('Moving Profit and Loss','2');
                                                $coa3 = Default_s::getACCInternalID('Moving Profit and Loss','3');
                                                $coa4 = Default_s::getACCInternalID('Moving Profit and Loss','4');
                                                $coa5 = Default_s::getACCInternalID('Moving Profit and Loss','5');
                                                $coa6 = Default_s::getACCInternalID('Moving Profit and Loss','6');
                                                    }}
                                                    @endif
                                                    @foreach(Coa::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('ACC1InternalID')->orderBy('InternalID')->get() as $coa)
                                                    <option <?php echo Coa::cekCOA($coa->ACC1InternalID, $coa->ACC2InternalID, $coa->ACC3InternalID, $coa->ACC4InternalID, $coa->ACC5InternalID, $coa->ACC6InternalID, $coa1, $coa2, $coa3, $coa4, $coa5, $coa6) ?>
                                                        value="{{$coa->InternalID}}">
                                                        {{Coa::formatCoa($coa->ACC1InternalID,$coa->ACC2InternalID,$coa->ACC3InternalID,$coa->ACC4InternalID,$coa->ACC5InternalID,$coa->ACC6InternalID,'1')}} 
                                                        {{" ".$coa->COAName}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="margbot10 row">
                                            <div class="col-md-4">
                                                <p><label for="Cgs">Cost of Goods Sold *</label></p>
                                            </div>
                                            <div class="col-md-6">
                                                <select class="chosen-select" id="CgsCoa" style="" name="cgsCoa">
                                                    @if ($default_s > 0) 
                                                    {{'';
                                                $coa1 = Default_s::getACCInternalID('Cost of Goods Sold','1');
                                                $coa2 = Default_s::getACCInternalID('Cost of Goods Sold','2');
                                                $coa3 = Default_s::getACCInternalID('Cost of Goods Sold','3');
                                                $coa4 = Default_s::getACCInternalID('Cost of Goods Sold','4');
                                                $coa5 = Default_s::getACCInternalID('Cost of Goods Sold','5');
                                                $coa6 = Default_s::getACCInternalID('Cost of Goods Sold','6');
                                                    }}
                                                    @endif
                                                    @foreach(Coa::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('ACC1InternalID')->orderBy('InternalID')->get() as $coa)
                                                    <option <?php echo Coa::cekCOA($coa->ACC1InternalID, $coa->ACC2InternalID, $coa->ACC3InternalID, $coa->ACC4InternalID, $coa->ACC5InternalID, $coa->ACC6InternalID, $coa1, $coa2, $coa3, $coa4, $coa5, $coa6) ?>
                                                        value="{{$coa->InternalID}}">
                                                        {{Coa::formatCoa($coa->ACC1InternalID,$coa->ACC2InternalID,$coa->ACC3InternalID,$coa->ACC4InternalID,$coa->ACC5InternalID,$coa->ACC6InternalID,'1')}} 
                                                        {{" ".$coa->COAName}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="margbot10 row">
                                            <div class="col-md-4">
                                                <p><label for="Disc">Purchase Discount *</label></p>
                                            </div>
                                            <div class="col-md-6">
                                                <select class="chosen-select" id="DiscCoa" style="" name="discCoa">
                                                    @if ($default_s > 0) 
                                                    {{'';
                                                $coa1 = Default_s::getACCInternalID('Discount','1');
                                                $coa2 = Default_s::getACCInternalID('Discount','2');
                                                $coa3 = Default_s::getACCInternalID('Discount','3');
                                                $coa4 = Default_s::getACCInternalID('Discount','4');
                                                $coa5 = Default_s::getACCInternalID('Discount','5');
                                                $coa6 = Default_s::getACCInternalID('Discount','6');
                                                    }}
                                                    @endif
                                                    @foreach(Coa::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('ACC1InternalID')->orderBy('InternalID')->get() as $coa)
                                                    <option <?php echo Coa::cekCOA($coa->ACC1InternalID, $coa->ACC2InternalID, $coa->ACC3InternalID, $coa->ACC4InternalID, $coa->ACC5InternalID, $coa->ACC6InternalID, $coa1, $coa2, $coa3, $coa4, $coa5, $coa6) ?>
                                                        value="{{$coa->InternalID}}">
                                                        {{Coa::formatCoa($coa->ACC1InternalID,$coa->ACC2InternalID,$coa->ACC3InternalID,$coa->ACC4InternalID,$coa->ACC5InternalID,$coa->ACC6InternalID,'1')}} 
                                                        {{" ".$coa->COAName}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="margbot10 row">
                                            <div class="col-md-4">
                                                <p><label for="Disc">Sales Discount *</label></p>
                                            </div>
                                            <div class="col-md-6">
                                                <select class="chosen-select" id="DiscCoa" style="" name="discSalesCoa">
                                                    @if ($default_s > 0) 
                                                    {{'';
                                                $coa1 = Default_s::getACCInternalID('Discount Sales','1');
                                                $coa2 = Default_s::getACCInternalID('Discount Sales','2');
                                                $coa3 = Default_s::getACCInternalID('Discount Sales','3');
                                                $coa4 = Default_s::getACCInternalID('Discount Sales','4');
                                                $coa5 = Default_s::getACCInternalID('Discount Sales','5');
                                                $coa6 = Default_s::getACCInternalID('Discount Sales','6');
                                                    }}
                                                    @endif
                                                    @foreach(Coa::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('ACC1InternalID')->orderBy('InternalID')->get() as $coa)
                                                    <option <?php echo Coa::cekCOA($coa->ACC1InternalID, $coa->ACC2InternalID, $coa->ACC3InternalID, $coa->ACC4InternalID, $coa->ACC5InternalID, $coa->ACC6InternalID, $coa1, $coa2, $coa3, $coa4, $coa5, $coa6) ?>
                                                        value="{{$coa->InternalID}}">
                                                        {{Coa::formatCoa($coa->ACC1InternalID,$coa->ACC2InternalID,$coa->ACC3InternalID,$coa->ACC4InternalID,$coa->ACC5InternalID,$coa->ACC6InternalID,'1')}} 
                                                        {{" ".$coa->COAName}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="margbot10 row">
                                            <div class="col-md-4">
                                                <p><label for="DownSales">Down Payment Sales *</label></p>
                                            </div>
                                            <div class="col-md-6">
                                                <select class="chosen-select" id="DownPaymentSales" style="" name="dpCoaSales">
                                                    @if ($default_s > 0) 
                                                    {{'';
                                                $coa1 = Default_s::getACCInternalID('Down Payment Sales','1');
                                                $coa2 = Default_s::getACCInternalID('Down Payment Sales','2');
                                                $coa3 = Default_s::getACCInternalID('Down Payment Sales','3');
                                                $coa4 = Default_s::getACCInternalID('Down Payment Sales','4');
                                                $coa5 = Default_s::getACCInternalID('Down Payment Sales','5');
                                                $coa6 = Default_s::getACCInternalID('Down Payment Sales','6');
                                                    }}
                                                    @endif
                                                    @foreach(Coa::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('ACC1InternalID')->orderBy('InternalID')->get() as $coa)
                                                    <option <?php echo Coa::cekCOA($coa->ACC1InternalID, $coa->ACC2InternalID, $coa->ACC3InternalID, $coa->ACC4InternalID, $coa->ACC5InternalID, $coa->ACC6InternalID, $coa1, $coa2, $coa3, $coa4, $coa5, $coa6) ?>
                                                        value="{{$coa->InternalID}}">
                                                        {{Coa::formatCoa($coa->ACC1InternalID,$coa->ACC2InternalID,$coa->ACC3InternalID,$coa->ACC4InternalID,$coa->ACC5InternalID,$coa->ACC6InternalID,'1')}} 
                                                        {{" ".$coa->COAName}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="margbot10 row">
                                            <div class="col-md-4">
                                                <p class="margbot20"><label for="DownPurchase">Down Payment Purchase *</label></p>

                                                <div class="">
                                                    <p class="text-left">* Required</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <select class="chosen-select" id="DownPaymentPurchase" style="" name="dpCoaPurchase">
                                                    @if ($default_s > 0) 
                                                    {{'';
                                                $coa1 = Default_s::getACCInternalID('Down Payment Purchase','1');
                                                $coa2 = Default_s::getACCInternalID('Down Payment Purchase','2');
                                                $coa3 = Default_s::getACCInternalID('Down Payment Purchase','3');
                                                $coa4 = Default_s::getACCInternalID('Down Payment Purchase','4');
                                                $coa5 = Default_s::getACCInternalID('Down Payment Purchase','5');
                                                $coa6 = Default_s::getACCInternalID('Down Payment Purchase','6');
                                                    }}
                                                    @endif
                                                    @foreach(Coa::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('ACC1InternalID')->orderBy('InternalID')->get() as $coa)
                                                    <option <?php echo Coa::cekCOA($coa->ACC1InternalID, $coa->ACC2InternalID, $coa->ACC3InternalID, $coa->ACC4InternalID, $coa->ACC5InternalID, $coa->ACC6InternalID, $coa1, $coa2, $coa3, $coa4, $coa5, $coa6) ?>
                                                        value="{{$coa->InternalID}}">
                                                        {{Coa::formatCoa($coa->ACC1InternalID,$coa->ACC2InternalID,$coa->ACC3InternalID,$coa->ACC4InternalID,$coa->ACC5InternalID,$coa->ACC6InternalID,'1')}} 
                                                        {{" ".$coa->COAName}}
                                                    </option>
                                                    @endforeach
                                                </select>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!---- end div tableadd---->  
                    </div><!---- end div tabwrap----> 
                    <div class="btnnest pull-right">
                        <button <?php if (myCheckIsEmpty('Coa')) echo 'disabled'; ?> class="btn btn-green btn-sm btn-save" id="btn-save"> Save </button>
                    </div>
                </form>
            </div>   
        </div>
    </div>
</div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript">
var config = {
    '.chosen-select': {}
}
for (var selector in config) {
    $(selector).chosen(config[selector]);
}
$.validate({
    modules: 'security',
    form: '#form-insert'
});
</script>
@stop