<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Genesys Web Enterprise System</title>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400' rel='stylesheet' type='text/css' />
        <link rel="shortcut icon" href="{{Asset('fav.ico')}}" type="image/x-icon" />
        <link rel="stylesheet" href="{{Asset('lib/bootstrap/css/bootstrap.min.css')}}" />
        <link rel="stylesheet" href="{{Asset('css/jquery.mCustomScrollbar.css')}}" />
        <link rel="stylesheet" href="{{Asset('css/kustom.css')}}" />
        <script src="{{Asset('lib/bootstrap/js/jquery-1.11.1.min.js')}}"></script>
        <script src="{{Asset('lib/bootstrap/js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{Asset('js/jquery-ui.js')}}"></script>
        <script src="{{Asset('lib/bootstrap/js/jquery-ui.min.js')}}"></script>
        <script src="{{Asset('lib/bootstrap/js/jquery.easing.min.js')}}"></script>
        @yield('css')
    </head>
    <body>
        <div class="top-nav">
            <div class="{{Config::get('companyHeader.class');}}">
                <h4><span class="hidden-xs">Genesys Web Enterprise System</span></h4>
            </div> 
            <p id="logout" >Hello {{Auth::user()->UserName.' '.Auth::user()->Company->CompanyName;}} |
                @if(Auth::user()->CompanyInternalID != -1)
                <a href="{{Route('showContact')}}">Contact Us</a> | 
                @endif
                <a href="{{Route('logout')}}"><span class="glyphicon glyphicon-lock"></span> Logout</a></p>
            <!-- notification-->
            @if(Auth::user()->CompanyInternalID != -1)
            <div class="pull-right notify"> 
                <div class="btn-group hidden-xs">
                    <button type="button" class="btn btn-green btn-sm dropdown-toggle  margr5" data-toggle="dropdown" aria-expanded="false"><span class="glyphicon glyphicon-info-sign"></span> Notification
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a style="cursor: default;">{{'Memory '.number_format(countMemory(),'2','.',',').'Mb used from '.number_format(Auth::user()->Company->Package->Memory,'0','.',',').'Mb'}}</a></li>
                        <li><a style="cursor: default;">
                                @if(Auth::user()->Company->ExpiredDate == null)
                                {{"Finish your payment."}}
                                @else
                                {{'Expired '.date("d-m-Y", strtotime(Auth::user()->Company->ExpiredDate))}}
                                @endif
                            </a></li>
                    </ul>
                </div>       
                <a href="{{Route('showHelp')}}" class="btn btn-green btn-sm hidden-xs margr5 badge1" ><span class="glyphicon glyphicon-question-sign"></span> Help</a>
                <a href="{{Route('showChangeLog')}}" class="btn btn-green hidden-xs btn-sm margr5 badge1" ><span class="glyphicon glyphicon-list"></span> Change Log</a>
            </div>
            @endif
            <!-- notification-->
        </div>

        <div class="side-nav" style="padding-bottom: 90px;">

            <div class="visible-xs hidden-md hidden-sm hidden-lg">
                <p><span id="close-side-nav" class="hover-pointer glyphicon glyphicon-remove-circle"></span></p>
            </div>
            <ul class="side-menu">

                @if(Auth::user()->Company->InternalID != '-1')
                <a href="{{Route('showDashboard')}}">
                    <li class="<?php if ($aktif == 'dashboard') echo 'li-aktif' ?>" style="font-weight: 700;font-size: 15px; cursor: pointer; padding-left: 10px !important;"><p>Dashboard</p></li>
                </a>
                @foreach(Matrix::getUserMatrixType() as $dataMenu)
                <li class="{{Matrix::where('Type',$dataMenu->Type)->first()->TypeClass}}" style="font-weight: 700;font-size: 15px; cursor: pointer; padding-left: 10px !important;"><p>{{ucfirst($dataMenu->Type)}} <span class=" caret"></span></p></li>
                @if($dataMenu->Type == 'accounting')
                <li class="accdecend" style="font-weight: 700;font-size: 15px; cursor: pointer; padding-left: 15px !important;"><p><span class="glyphicon glyphicon-tasks"></span>Chart of account <span class=" caret"></span></p></li>
                @if(checkModul('AM01'))
                <ul class="accdecendchild">
                    @foreach(Matrix::getUserMatrixByChild('accdecend') as $dataMatrix)
                    <a href="{{Route($dataMatrix->MatrixRoute)}}">
                        <li class="<?php if ($aktif == $dataMatrix->MatrixActive) echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-{{$dataMatrix->MatrixIcon}}"></span>  {{$dataMatrix->MatrixMenu}}</p></li></a>
                    @endforeach
                </ul>
                @endif
                @endif
                @if($dataMenu->Type == 'master')
                <li class="invdecend" style="font-weight: 700;font-size: 15px; cursor: pointer; padding-left: 15px !important;"><p><span class="glyphicon glyphicon-tasks"></span>Inventory <span class=" caret"></span></p></li>
                <ul class="invdecendchild">
                    @foreach(Matrix::getUserMatrixByChild('invdecend') as $dataMatrix)
                    <a href="{{Route($dataMatrix->MatrixRoute)}}">
                        <li class="<?php if ($aktif == $dataMatrix->MatrixActive) echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-{{$dataMatrix->MatrixIcon}}"></span>  {{$dataMatrix->MatrixMenu}}</p></li></a>
                    @endforeach
                </ul>
                @endif
                @if($dataMenu->Type == 'transaction')
                @if(count(Matrix::getUserMatrixByChild('salesdecend')) >0)
                <li class="salesdecend" style="font-weight: 700;font-size: 15px; cursor: pointer; padding-left: 15px !important;"><p><span class="glyphicon glyphicon-tasks"></span>Sales <span class=" caret"></span></p></li>
                <ul class="salesdecendchild">
                    @foreach(Matrix::getUserMatrixByChild('salesdecend') as $dataMatrix)
                    <a href="{{Route($dataMatrix->MatrixRoute)}}">
                        <li class="<?php if ($aktif == $dataMatrix->MatrixActive) echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-{{$dataMatrix->MatrixIcon}}"></span>  {{$dataMatrix->MatrixMenu}}</p></li></a>
                    @endforeach
                </ul>
                @endif
                @if(count(Matrix::getUserMatrixByChild('purchasedecend')) > 0)
                <li class="purchasedecend" style="font-weight: 700;font-size: 15px; cursor: pointer; padding-left: 15px !important;"><p><span class="glyphicon glyphicon-tasks"></span>Purchase <span class=" caret"></span></p></li>
                <ul class="purchasedecendchild">
                    @foreach(Matrix::getUserMatrixByChild('purchasedecend') as $dataMatrix)
                    <a href="{{Route($dataMatrix->MatrixRoute)}}">
                        <li class="<?php if ($aktif == $dataMatrix->MatrixActive) echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-{{$dataMatrix->MatrixIcon}}"></span>  {{$dataMatrix->MatrixMenu}}</p></li></a>
                    @endforeach
                </ul>
                @endif 
                @endif 
                <ul class="{{Matrix::where('Type',$dataMenu->Type)->first()->TypeChildClass}}">
                    @foreach(Matrix::getUserMatrix($dataMenu->Type) as $dataMatrix)
                    @if(checkModul(Modul::find($dataMatrix->ModulInternalID)->ModulID) && $dataMatrix->Function == 0)
                    <a href="{{Route($dataMatrix->MatrixRoute)}}">
                        <li class="<?php if ($aktif == $dataMatrix->MatrixActive) echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-{{$dataMatrix->MatrixIcon}}"></span>  {{$dataMatrix->MatrixMenu}}</p></li></a>
                    @endif
                    @endforeach
                </ul>
                @endforeach
                @else
                <li class="master" style="font-weight: 700;font-size: 15px; cursor: pointer; padding-left: 10px !important;">Master <span class="caret"></span></li>
                <ul class="masterchild">
                    <a href="{{Route('showUser')}}">
                        <li class="<?php if ($aktif == 'user') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-apple"></span>  User</p></li></a>
                    <a href="{{Route('showCompany')}}">
                        <li class="<?php if ($aktif == 'company') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-apple"></span>  Company</p></li></a>
                    <a href="{{Route('showRegion')}}">
                        <li class="<?php if ($aktif == 'region') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-apple"></span>  Region</p></li></a>
                    <a href="{{Route('showCompanyProcess')}}">
                        <li class="<?php if ($aktif == 'companyProcess') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-apple"></span>  Company Process</p></li></a>
                    <a href="{{Route('showAgent')}}">
                        <li class="<?php if ($aktif == 'agent') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-apple"></span>  Agent</p></li></a>
                    <a href="{{Route('showAgentWithdraw')}}">
                        <li class="<?php if ($aktif == 'agentWithdraw') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-apple"></span>  Agent Withdraw</p></li></a>
                </ul>
                @endif
            </ul>
        </div>
        <div class="top-nav2">
            @yield('nav')
            <button type="button" class="btn btn-sm btn-green hamb">
                <span class="glyphicon glyphicon-menu-hamburger"></span>
            </button>
        </div>
    </div>

    <div class="wrap">
        @yield('content')
    </div>

    @yield('modal')

    <script src="{{Asset('js/jquery.mCustomScrollbar.concat.min.js')}}"></script>

    <script>
var us = '<?php echo Auth::user()->Company->InternalID ?>';
var halamanAktif = '<?php echo $aktif; ?>';
var toogle = '<?php echo $toogle; ?>';
    </script>

    <script src="{{Asset('js/template.js')}}"></script>
    @yield('js')
</body>
</html>