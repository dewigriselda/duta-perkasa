<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Salmon Accounting Premium</title>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400' rel='stylesheet' type='text/css'>
        <link rel="shortcut icon" href="{{Asset('fav.ico')}}" type="image/x-icon" />
        <link rel="stylesheet" href="{{Asset('lib/bootstrap/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{Asset('css/jquery.mCustomScrollbar.css')}}" />
        <link rel="stylesheet" href="{{Asset('css/kustom.css')}}">
        <script src="{{Asset('lib/bootstrap/js/jquery-1.11.1.min.js')}}"></script>
        <script src="{{Asset('lib/bootstrap/js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{Asset('js/jquery-ui.js')}}"></script>
        <script src="{{Asset('lib/bootstrap/js/jquery-ui.min.js')}}"></script>
        <script src="{{Asset('lib/bootstrap/js/jquery.easing.min.js')}}"></script>
        @yield('css')
    </head>
    <body>
        <div class="top-nav">
            <div class="logo">
                <h4>{{Config::get('companyHeader.header_company');}} Accounting Premium</h4>
            </div> 
            <p id="logout" >Hello {{Auth::user()->UserName.' '.Auth::user()->Company->CompanyName;}} |
                @if(Auth::user()->CompanyInternalID != -1)
                <a href="{{Route('showContact')}}">Contact Us</a> | 
                @endif
                <a href="{{Route('logout')}}"><span class="glyphicon glyphicon-lock"></span> Logout</a></p>
            <!-- notification-->
            @if(Auth::user()->CompanyInternalID != -1)
            <div class="pull-right notify"> 
                <div class="btn-group">
                    <button type="button" class="btn btn-green btn-sm dropdown-toggle  margr5" data-toggle="dropdown" aria-expanded="false"><span class="glyphicon glyphicon-info-sign"></span> Notification
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a style="cursor: default;">{{'Memory '.number_format(countMemory(),'2','.',',').'Mb used from '.number_format(Auth::user()->Company->Package->Memory,'0','.',',').'Mb'}}</a></li>
                        <li><a style="cursor: default;">
                                @if(Auth::user()->Company->ExpiredDate == null)
                                {{"Finish your payment."}}
                                @else
                                {{'Expired '.date("d-m-Y", strtotime(Auth::user()->Company->ExpiredDate))}}
                                @endif
                            </a></li>
                    </ul>
                </div>       
                <a href="{{Route('showHelp')}}" class="btn btn-green btn-sm dropdown-toggle margr5 badge1" ><span class="glyphicon glyphicon-question-sign"></span> Help
                </a>
            </div>
            @endif
            <!-- notification-->
        </div>
        <div class="side-nav" style="padding-bottom: 90px;">
            <ul class="side-menu">
                @if(Auth::user()->Company->InternalID != '-1')
                <a href="{{Route('showDashboard')}}">
                    <li class="<?php if ($aktif == 'dashboard') echo 'li-aktif' ?>" style="font-weight: 700;font-size: 15px; cursor: pointer; padding-left: 10px !important;"><p>Dashboard</p></li>
                </a>
                    @if(checkTypeMatrix('accounting'))
                    <li class="accountind" style="font-weight: 700;font-size: 15px; cursor: pointer; padding-left: 10px !important;"><p>Accounting <span class=" caret"></span></p></li>
                        @if(checkMatrix('AM04')||checkMatrix('AM01')||checkMatrix('AM02')||checkMatrix('AM03'))
                        <li class="accdecend" style="font-weight: 700;font-size: 15px; cursor: pointer; padding-left: 15px !important;"><p><span class="glyphicon glyphicon-tasks"></span>Chart of account <span class=" caret"></span></p></li>
                        @endif
                    @endif
                    @if(checkModul('AM01'))
                    <ul class="accdecendchild">
                        @if(checkModul('AM02'))
                            @if(checkMatrix('AM04'))
                            <a href="{{Route('showCoa')}}">
                                <li class="<?php if ($aktif == 'coa') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-list-alt"></span> COA Master</p></li></a>
                            @endif
                        @endif
                        @if(checkMatrix('AM01'))
                        <a href="{{Route('coaLevel')}}">
                            <li class="<?php if ($aktif == 'coaLevel') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-tasks"></span> COA Level 1-4</p></li></a>
                        @endif
                        @if(checkMatrix('AM02'))
                        <a href="{{Route('showCoa5')}}">
                            <li class="<?php if ($aktif == 'coa5') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-tasks"></span> COA Level 5</p></li></a>
                        @endif
                        @if(checkMatrix('AM03'))
                        <a href="{{Route('showCoa6')}}">
                            <li class="<?php if ($aktif == 'coa6') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-tasks"></span> COA Level 6</p></li></a>
                        @endif
                    </ul>
                    @endif
                <ul class="accchild" > 
                    @if(checkModul('SM02'))
                        @if(checkMatrix('SM03'))
                        <a href="{{Route('showCurrency')}}">
                            <li class="<?php if ($aktif == 'currency') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-usd"></span>  Currency</p></li></a>
                        @endif
                    @endif
                    @if(checkModul('AM04'))
                        @if(checkMatrix('AM07'))
                        <a href="{{Route('showSlip')}}">
                            <li class="<?php if ($aktif == 'slip') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-tags"></span> Slip</p></li></a>
                        @endif
                    @endif
                    @if(checkModul('AM05'))
                        @if(checkMatrix('AM08'))
                        <a href="{{Route('showDepartment')}}">
                            <li class="<?php if ($aktif == 'department') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-inbox"></span>  Department</p></li></a>
                        @endif
                    @endif
                    @if(checkModul('AT01'))
                        @if(checkMatrix('AT01'))
                        <a href="{{Route('showJournal')}}">
                            <li class="<?php if ($aktif == 'journal') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-book"></span>  Journal</p></li></a>
                        @endif
                    @endif
                    @if(checkModul('AM03'))
                        @if(checkMatrix('AM06'))
                        <a href="{{Route('showGroupDepreciation')}}">
                            <li class="<?php if ($aktif == 'groupDepreciation') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-arrow-down"></span>  Group Depreciation</p></li></a>
                        @endif
                        @if(checkMatrix('AM05'))
                        <a href="{{Route('showDepreciation')}}">
                            <li class="<?php if ($aktif == 'depreciation') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-arrow-down"></span>  Depreciation</p></li></a>
                        @endif
                    @endif
                    @if(checkModul('AR01'))
                        @if(checkMatrix('AR01'))
                        <a href="{{Route('showReportAccounting')}}">
                            <li class="<?php if ($aktif == 'reportAccounting') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-arrow-down"></span>  Report</p></li></a>
                        @endif
                    @endif
                </ul>
                @endif
                @if(checkTypeMatrix('master'))
                <li class="master" style="font-weight: 700;font-size: 15px; cursor: pointer; padding-left: 10px !important;">Master <span class="caret"></span></li>
                @endif
                <ul class="masterchild">
                    @if(checkModul('SM06'))
                        @if(checkMatrix('SM06'))
                        <a href="{{Route('showUom')}}">
                            <li class="<?php if ($aktif == 'uom') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-apple"></span>  Uom</p></li></a>
                        @endif
                        @if(checkMatrix('SM07'))
                        <a href="{{Route('showInventoryUom')}}">
                            <li class="<?php if ($aktif == 'inventoryUom') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-apple"></span> Inventory Uom</p></li></a>
                        @endif
                    @endif
                    @if(checkModul('SM01'))
                        @if(checkMatrix('SM02'))
                        <a href="{{Route('showInventoryType')}}">
                            <li class="<?php if ($aktif == 'inventoryType') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-apple"></span>  Inventory Type</p></li></a>
                        @endif
                        @if(checkMatrix('SM01'))
                        <a href="{{Route('showInventory')}}">
                            <li class="<?php if ($aktif == 'inventory') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-apple"></span>  Inventory</p></li></a>
                        @endif
                    @endif
                    @if(checkModul('SM03'))
                        @if(checkMatrix('SM04'))
                        <a href="{{Route('showWarehouse')}}">
                            <li class="<?php if ($aktif == 'warehouse') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-apple"></span>  Warehouse</p></li></a>
                        @endif
                    @endif
                    @if((checkModul('SM04') && checkMatrix('SM05')) || Auth::user()->CompanyInternalID == '-1')
                    <a href="{{Route('showUser')}}">
                        <li class="<?php if ($aktif == 'user') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-apple"></span>  User</p></li></a>
                    @endif
                    @if(Auth::user()->Company->InternalID == '-1')
                    <a href="{{Route('showCompany')}}">
                        <li class="<?php if ($aktif == 'company') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-apple"></span>  Company</p></li></a>
                    <a href="{{Route('showRegion')}}">
                        <li class="<?php if ($aktif == 'region') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-apple"></span>  Region</p></li></a>
                    <a href="{{Route('showCompanyProcess')}}">
                        <li class="<?php if ($aktif == 'companyProcess') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-apple"></span>  Company Process</p></li></a>
                    <a href="{{Route('showAgent')}}">
                        <li class="<?php if ($aktif == 'agent') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-apple"></span>  Agent</p></li></a>
                    <a href="{{Route('showAgentWithdraw')}}">
                        <li class="<?php if ($aktif == 'agentWithdraw') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-apple"></span>  Agent Withdraw</p></li></a>
                    @endif
                </ul>
                @if(Auth::user()->Company->InternalID != '-1')
                    @if(checkTypeMatrix('transaction'))
                    <li class="transaction" style="font-weight: 700;font-size: 15px; cursor: pointer; padding-left: 10px !important;">Transaction <span class="caret"></span></li>
                    @endif
                <ul class="transactionchild">
                    @if(checkModul('AST01'))
                        @if(checkMatrix('ST01'))
                        <a href="{{Route('showSalesOrder')}}">
                            <li class="<?php if ($aktif == 'salesOrder') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-book"></span>  Sales Order</p></li></a>
                        @endif
                    @endif
                    @if(checkModul('ST01'))
                        @if(checkMatrix('ST02'))
                        <a href="{{Route('showSales')}}">
                            <li class="<?php if ($aktif == 'sales') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-book"></span>  Sales</p></li></a>
                        @endif
                    @endif
                    @if(checkModul('ST02'))
                        @if(checkMatrix('ST03'))
                        <a href="{{Route('showSalesReturn')}}">
                            <li class="<?php if ($aktif == 'salesReturn') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-book"></span>  Sales Return</p></li></a>
                        @endif
                    @endif
                    @if(checkModul('AST01'))
                        @if(checkMatrix('ST04'))
                        <a href="{{Route('showPurchaseOrder')}}">
                            <li class="<?php if ($aktif == 'purchaseOrder') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-book"></span>  Purchase Order</p></li></a>
                        @endif
                    @endif
                    @if(checkModul('ST03'))
                        @if(checkMatrix('ST05'))
                        <a href="{{Route('showPurchase')}}">
                            <li class="<?php if ($aktif == 'purchase') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-book"></span>  Purchase</p></li></a>
                        @endif
                    @endif
                    @if(checkModul('ST04'))
                        @if(checkMatrix('ST06'))
                        <a href="{{Route('showPurchaseReturn')}}">
                            <li class="<?php if ($aktif == 'purchaseReturn') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-book"></span>  Purchase Return</p></li></a>
                        @endif
                    @endif
                    @if(checkModul('ST06'))
                        @if(checkMatrix('ST08'))
                        <a href="{{Route('showMemoIn')}}">
                            <li class="<?php if ($aktif == 'memoIn') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-book"></span>  Memo In</p></li></a>
                        @endif
                        @if(checkMatrix('ST09'))
                        <a href="{{Route('showMemoOut')}}">
                            <li class="<?php if ($aktif == 'memoOut') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-book"></span>  Memo Out</p></li></a>
                        @endif
                    @endif
                    @if(checkModul('ST05'))
                        @if(checkMatrix('ST07'))
                        <a href="{{Route('showTransfer')}}">
                            <li class="<?php if ($aktif == 'transfer') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-book"></span>  Transfer</p></li></a>
                        @endif
                    @endif
                </ul>
                @if(checkTypeMatrix('setting'))
                <li class="setting" style="font-weight: 700;font-size: 15px; cursor: pointer; padding-left: 10px !important;">Setting <span class="caret"></span></li>
                @endif
                <ul class="settingchild">
                    @if(checkMatrix('SS01'))
                    <a href="{{Route('settProfile')}}">
                        <li class="<?php if ($aktif == 'profile') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-user"></span>  Profile</p></li></a>
                    @endif
                    @if(checkMatrix('SS02'))
                    <a href="{{Route('settCompany')}}">
                        <li class="<?php if ($aktif == 'companyProfile') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-briefcase"></span>  Company</p></li></a>
                    @endif
                    @if(checkMatrix('SS03'))
                    <a href="{{Route('showDefaultAccount')}}">
                        <li class="<?php if ($aktif == 'defaultAccount') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-briefcase"></span>  Default Account</p></li></a>
                    @endif
                </ul>
                    @if(checkModul('AT02'))
                        @if(checkTypeMatrix('utility'))
                        <li class="utility" style="font-weight: 700;font-size: 15px; cursor: pointer; padding-left: 10px !important;"> Utility <span class="caret"></span></li>
                        @endif
                    <ul class="utilitychild">
                        @if(checkMatrix('MP01'))
                        <a href="{{Route('showMonthlyProcess')}}">
                            <li class="<?php if ($aktif == 'monthlyProcess') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-user"></span>  Monthly Process</p></li></a>
                        @endif
                    </ul>
                    @endif
                @endif
            </ul>
        </div>
        <div class="top-nav2">
            @yield('nav')
            <button type="button" class="btn btn-sm btn-green hamb">
                <span class="glyphicon glyphicon-menu-hamburger"></span>
            </button>
        </div>
    </div>
    <div class="wrap">
        @yield('content')
    </div>
    @yield('modal')

    <script src="{{Asset('js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    <script>
var us = '<?php echo Auth::user()->Company->InternalID ?>';
var halamanAktif = '<?php echo $aktif; ?>';
var toogle = '<?php echo $toogle; ?>';
    </script>
    <script src="{{Asset('js/template.js')}}"></script>
    @yield('js')
</body>
</html>