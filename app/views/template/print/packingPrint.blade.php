<!--<!DOCTYPE html>-->
<html>
    <head>
        <style>

            <!--@page { size: 21cm 32cm; margin: 0.5cm 1.4cm 0.5cm 0.1cm  }-->

            /*style="padding: 1px; border: none; font-family: arial,sans-serif;font-size: 8px;  font-weight: 500;"*/
            html {
                padding: 0 0 0 0;
                /*margin: 70px 0 0 0;*/
                margin: 0 0 0 0;
                font-size: 8px;
                position: relative;
            }

            body {
                margin: 0;
                font-family: arial, sans-serif;
                overflow:hidden;
            }

            .txt-top{
                vertical-align: text-top;
            }

            .fz14 {
                /*dulu fz 12*/
                font-size: 14px;
            }
            .fz15 {
                /*dulu fz 13*/
                font-size: 15px;
            }
            .fz12 {
                /*dulu fz 10*/
                font-size: 12px; 
            }

            .header-print {
                clear: both;
                border-bottom: 1px solid black;
            }

            .header-print .left-header {
                display: inline-block;
                float:left;
                width: 50%;
            }

            .header-print .right-header {
                display: inline-block;
                width: 50%;
            }


            .main-wrapper .title-report {
                font-size: 16px;
                font-weight: 700;
                margin: 5px 0;
                text-align: center;
            }

            .tableBorder {
                border-spacing: 0;
                border: 0px;
            }
            .tableBorder th{
                padding: 1px;
                border-spacing: 0;
                border: 0.5px solid black;
                border-left: 1px solid black;
                border-right: none;
                text-align: center;
            }

            .tableBorder td:nth-child(4),
            .tableBorder td:nth-child(5),
            .tableBorder td:nth-child(6),
            .tableBorder td:nth-child(7),
            .tableBorder td:nth-child(8) {
                text-align: right;
            }


            .tableBorder th:last-child{ 
                border-right: 1px solid black;
            }
            .tableBorder td{
                border-spacing: 0;
                border: none;
                border-left: none;
                border-right: none;
                border-top: none;
            }
            .tableBorder td:last-child{ 
                border-right: none;
            }
            .footer { 
                font-family: arial,sans-serif; 
                position: fixed; 
                bottom: 0; 
                right: 0;
            }
            .footer .page:after { 
                font-family: arial,sans-serif; 
                font-size: 8px; 
                content: "Page " counter(page);
            }

            .table {
                width: 100%;
                max-width: 100%;
                margin-bottom: 20px;
            }
            .table > thead > tr > th,
            .table > tbody > tr > th,
            .table > tfoot > tr > th,
            .table > thead > tr > td,
            .table > tbody > tr > td,
            .table > tfoot > tr > td {
                padding: 8px;
                line-height: 1.42857143;
                vertical-align: top;
                border-top: 1px solid black;
            }
            .table > thead > tr > th {
                vertical-align: bottom;
                border-bottom: 2px solid black;
            }
            .table > caption + thead > tr:first-child > th,
            .table > colgroup + thead > tr:first-child > th,
            .table > thead:first-child > tr:first-child > th,
            .table > caption + thead > tr:first-child > td,
            .table > colgroup + thead > tr:first-child > td,
            .table > thead:first-child > tr:first-child > td {
                border-top: 0;
            }
            .table > tbody + tbody {
                border-top: 2px solid black;
            }
            .table .table {
                background-color: #fff;
            }
            .table-condensed > thead > tr > th,
            .table-condensed > tbody > tr > th,
            .table-condensed > tfoot > tr > th,
            .table-condensed > thead > tr > td,
            .table-condensed > tbody > tr > td,
            .table-condensed > tfoot > tr > td {
                padding: 5px;
            }
            table.table-bordered {
                border-collapse: collapse;
                border: 1px solid black;
            }
            table.table-bordered td{
                border: 1px solid black;
                padding: 1px 2px;
            }
            table.table-bordered th{
                border: 1px solid black;
            }
        </style>
    </head>
    <body>
        @if($header->VAT == 1)
        <div class="clearfix header-print">
            <!--            <div style="height: 133px">
                            <img height="100%" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                        </div>-->
            <div class="fz14 left-header">
                <div class="fz12 left-header" style="display: inline !important;vertical-align: top;margin-top: -10px;width: 100%">
                    <p style="width: 100%"><img src="{{ Asset('img/unnamed.jpg') }}" style="float: left;width: 40px;height: auto;margin-right: 6px; display: inline-block !important"><span style="font-size: 30px; font-weight: bold;">{{Auth::user()->Company->CompanyName}} </span></p>

                    <br style="clear:both">
                </div> 
            </div> 
            <div class="right-header">
                <table  class="fz12" style="float: right;">
                    <tr style="background: none;">
                        <td>Address</td>
                        <td>:</td>
                        <td>{{ Auth::user()->Company->Address . ' ' . Auth::user()->Company->City }}</td>
                    </tr>
                    <tr style="background: none;">
                        <td>Phone / Fax</td>
                        <td>:</td>
                        <td>{{ Auth::user()->Company->Phone }} / {{Auth::user()->Company->Fax}}</td>
                    </tr>
                    <tr style="background: none;">
                        <td>Email</td>
                        <td>:</td>
                        <td>{{ Auth::user()->Company->Email }}</td>
                    </tr> 
                    <tr style="background: none;">
                        <td>Web</td>
                        <td>:</td>
                        <td> {{ getCompanyWebsite(); }}</td>
                    </tr>
                </table>
            </div>           
        </div>
        @endif
        <div class="main-wrapper">
            <h1 class="title-report">Packing List</h1>
            <div class="fz12">
                <table width="100%" style="table-layout:fixed;">
                    <tr>
                        <td width="50%" class="fz15">
                             <table width="100%">
                                <tr>
                                    <td>Packing ID</td>
                                    <td>:</td>
                                    <td>{{ $header->PackingID }}</td>
                                </tr>
                                <tr>
                                    <td>Date</td>
                                    <td>:</td>
                                    <td>{{ date("d-M-Y", strtotime($header->ShippingDate)) }}</td>
                                </tr>
                                <tr>
                                    <td>Number of Vehicle</td>
                                    <td>:</td>
                                    <td>{{ $header->NumberVehicle }}</td>
                                </tr>
                                <tr>
                                    <td>Driver Name</td>
                                    <td>:</td>
                                    <td>{{ $header->DriverName }}</td>
                                </tr>
                            </table>
                        </td>
                        <td width="40%" style="vertical-align: top;">
                            <table class="fz15" width="100%" style="margin-left:0%;">
                                <tr>
                                    <td class="txt-top">Customer</td>
                                    <td class="txt-top">:</td>
                                    <td>{{ $customer }}</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>  

            <table class="table-bordered fz14" cellspacing="0" cellpadding="0" width="100%" style="clear: both; margin-bottom: 2px;">
                <thead>
                    <tr>
                        <th width="5%">No</th>
                        <th width="40%">Inventory</th>
                        <th width="10%">Uom</th>
                        <th width="10%">Qty</th>
                        <th width="10%">Warehouse</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $counter = 1;
                    ?>

                    @foreach ($shipping as $head)
                    <?php $data = ShippingAddDetail::where('ShippingInternalID', $head->InternalID)->first(); ?>
                    <tr>
                        <td style="text-align: center;vertical-align: top">{{ $counter }}</td>
                        <td>{{Inventory::find($data->InventoryInternalID)->InventoryName }}</td>
                        <td style="text-align: center">{{ Uom::find($data->UomInternalID)->UomID }}</td>
                        <td style="text-align: center">{{ number_format($data->Qty, '0', '.', ',') }}</td>
                        <td >{{ $head->warehouse->WarehouseName }}</td>
                    </tr>
                    <?php
                    $counter++;
                    ?>
                    @endforeach
                </tbody>
            </table>

            <table class="fz14" style="width:100%;">               
                <tr style="background-color:none;">
                    <td style="width: 310px;background-color:none;vertical-align: top;">
                        <table class="fz14" width="100%" style="margin-top: 10px;text-align:left;">
                            <tr style="background-color:none;">
                                <td width="10%">Remark</td>
                                <td>:</td>
                                <td>{{ $header->Remark }}</td>
                            </tr>
                            <tr style="background-color:none;">
                                <td colspan="3">Perhatian Barang2 yang sudah diterima tidak dapat ditukar
                                    / dikembalikan</td>
                            </tr>
                        </table>   
                    </td>     
                    <td style="width: 210px;vertical-align: text-top;background-color:none;" colspan="2">
                        <table class="fz14" width="100%" style="margin-top: 10px;">
                            <tr style="background-color:none;vertical-align: top">
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 80px; font-weight: 500;" width="50%">
                                    Tanda Terima,
                                </td>
                                <td width="10%">
                                    <br/>
                                    <br/>
                                    <br/>
                                </td>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 60px; font-weight: 500;" width="40%">
                                    Hormat Kami, 
                                </td>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 20px; font-weight: 500;" width="10%">

                                </td>
                            </tr>
                            <tr><td><br/></td></tr>
                            <tr>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 80px; font-weight: 500;" width="50%">
                                    ({{ Auth::user()->UserName }})
                                </td>
                                <td width="10%">
                                    <br/>
                                    <br/>
                                    <br/>
                                </td>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 50px; font-weight: 500;" width="40%">
                                    (____________)
                                </td>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 5px; font-weight: 500;" width="10%">

                                </td>
                            </tr>                
                        </table>   
                    </td>     
                </tr>
            </table> 
        </div>
    </body>
</html>
<script src="{{Asset('lib/bootstrap/js/jquery-1.11.1.min.js')}}"></script>
<script>
$(document).ready(function () {
    window.print();
});
</script>