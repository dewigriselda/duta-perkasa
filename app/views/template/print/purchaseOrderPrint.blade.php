<!DOCTYPE html>
<html>
    <head>
        <style>

            <!--@page { size: 21cm 32cm; margin: 0.5cm 1.4cm 0.5cm 0.1cm  }-->

            /*style="padding: 1px; border: none; font-family: arial,sans-serif;font-size: 8px;  font-weight: 500;"*/
            html {
                padding: 0 0 0 0;
                /*margin: 70px 0 0 0;*/
                margin: 0 0 0 0;
                font-size: 8px;
                position: relative;
            }

            body {
                margin: 0;
                font-family: arial, sans-serif;
                overflow:hidden;
            }

            .txt-top{
                vertical-align: text-top;
            }

            .fz14 {
                /*dulu fz 12*/
                font-size: 14px;
            }
            .fz15 {
                /*dulu fz 13*/
                font-size: 15px;
            }
            .fz12 {
                /*dulu fz 10*/
                font-size: 12px; 
            }
            
            .fz10 {
                font-size: 10px; 
            }

            .header-print {
                clear: both;
                border-bottom: 1px solid black;
            }

            .header-print .left-header {
                display: inline-block;
                float:left;
                width: 50%;
            }

            .header-print .right-header {
                display: inline-block;
                width: 50%;
            }


            .main-wrapper .title-report {
                font-size: 16px;
                font-weight: 700;
                margin: 5px 0;
                text-align: center;
            }

            .tableBorder {
                border-spacing: 0;
                border: 0px;
            }
            .tableBorder th{
                padding: 1px;
                border-spacing: 0;
                border: 0.5px solid black;
                border-left: 1px solid black;
                border-right: none;
                text-align: center;
            }

            .tableBorder td:nth-child(4),
            .tableBorder td:nth-child(5),
            .tableBorder td:nth-child(6),
            .tableBorder td:nth-child(7),
            .tableBorder td:nth-child(8) {
                text-align: right;
            }


            .tableBorder th:last-child{ 
                border-right: 1px solid black;
            }
            .tableBorder td{
                border-spacing: 0;
                border: none;
                border-left: none;
                border-right: none;
                border-top: none;
            }
            .tableBorder td:last-child{ 
                border-right: none;
            }
            .footer { 
                font-family: arial,sans-serif; 
                position: fixed; 
                bottom: 0; 
                right: 0;
            }
            .footer .page:after { 
                font-family: arial,sans-serif; 
                font-size: 8px; 
                content: "Page " counter(page);
            }

            .table {
                width: 100%;
                max-width: 100%;
                margin-bottom: 20px;
            }
            .table > thead > tr > th,
            .table > tbody > tr > th,
            .table > tfoot > tr > th,
            .table > thead > tr > td,
            .table > tbody > tr > td,
            .table > tfoot > tr > td {
                padding: 8px;
                line-height: 1.42857143;
                vertical-align: top;
                border-top: 1px solid black;
            }
            .table > thead > tr > th {
                vertical-align: bottom;
                border-bottom: 2px solid black;
            }
            .table > caption + thead > tr:first-child > th,
            .table > colgroup + thead > tr:first-child > th,
            .table > thead:first-child > tr:first-child > th,
            .table > caption + thead > tr:first-child > td,
            .table > colgroup + thead > tr:first-child > td,
            .table > thead:first-child > tr:first-child > td {
                border-top: 0;
            }
            .table > tbody + tbody {
                border-top: 2px solid black;
            }
            .table .table {
                background-color: #fff;
            }
            .table-condensed > thead > tr > th,
            .table-condensed > tbody > tr > th,
            .table-condensed > tfoot > tr > th,
            .table-condensed > thead > tr > td,
            .table-condensed > tbody > tr > td,
            .table-condensed > tfoot > tr > td {
                padding: 5px;
            }
            table.table-bordered {
                border-collapse: collapse;
                border: 1px solid black;
            }
            table.table-bordered td{
                border: 1px solid black;
                padding: 1px 2px;
            }
            table.table-bordered th{
                border: 1px solid black;
            }
        </style>
    </head>
    <body>
        <div class="clearfix header-print">
            <!--            <div style="height: 133px">
                            <img height="100%" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                        </div>-->
            <div class="fz14 left-header">
                <div class="fz12 left-header" style="display: inline !important;vertical-align: top;margin-top: -10px;width: 100%">
                    <p style="width: 100%"><img src="{{ Asset('img/unnamed.jpg') }}" style="float: left;width: 40px;height: auto;margin-right: 6px; display: inline-block !important"><span style="font-size: 30px; font-weight: bold;">{{Auth::user()->Company->CompanyName}} </span></p>

                    <br style="clear:both">
                </div> 
            </div> 
            <div class="right-header">
                <table  class="fz12" style="float: right;">
                    <tr style="background: none;">
                        <td>Address</td>
                        <td>:</td>
                        <td>{{ Auth::user()->Company->Address . ' ' . Auth::user()->Company->City }}</td>
                    </tr>
                    <tr style="background: none;">
                        <td>Phone / Fax</td>
                        <td>:</td>
                        <td>{{ Auth::user()->Company->Phone }} / {{Auth::user()->Company->Fax}}</td>
                    </tr>
                    <tr style="background: none;">
                        <td>Email</td>
                        <td>:</td>
                        <td>{{ Auth::user()->Company->Email }}</td>
                    </tr> 
                    <tr style="background: none;">
                        <td>Web</td>
                        <td>:</td>
                        <td> {{ getCompanyWebsite(); }}</td>
                    </tr>
                </table>
            </div>           
        </div>

        <div class="main-wrapper">
            <h1 class="title-report">Purchase Order</h1>

            <div class="fz12">
                <table width="100%" style="table-layout:fixed">
                    <tr>
                        <td width="50%" class="fz14">
                            <table width="100%" style="table-layout:fixed">
                                <tr>
                                    <td width='35%'>Purchase Order ID</td>
                                    <td width='2%'>:</td>
                                    <td>{{ $header->PurchaseOrderID }}</td>
                                </tr>
                                <tr>
                                    <td>Date</td>
                                    <td>:</td>
                                    <td>{{ date("d-M-Y", strtotime($header->PurchaseOrderDate)) }}</td>
                                </tr>
                                <tr>
                                    <td>Currency</td>
                                    <td>:</td>
                                    <td>{{ $currencyName }}</td>
                                </tr>
                                <tr>
                                    <td>Rate</td>
                                    <td>:</td>
                                    <td>{{ number_format($header->CurrencyRate, '2', '.', ',') }}</td>
                                </tr>
                                <tr>
                                    <td>VAT</td>
                                    <td>:</td>
                                    <td>{{ $vat }}</td>
                                </tr>
                            </table>
                        </td>

                        <td width="40%" style="vertical-align: top">
                            <table class="fz14" width="100%" style="margin-left:0%;">
                                <tr>
                                    <td class="txt-top">Supplier</td>
                                    <td class="txt-top">:</td>
                                    <td>{{ $supplier }}</td>
                                </tr>
                                <tr>
                                    <td class="txt-top">Attn</td>
                                    <td class="txt-top">:</td>
                                    @if($header->Attn != '' && $header->Attn != null)
                                    <td>{{ $header->Attn }}</td>
                                    @else
                                    <td>-</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>Payment</td>
                                    <td>:</td>
                                    <td>{{ $payment }}</td>
                                </tr>
                                @if ($header->isCash != 0&& $header->isCash != 2&& $header->isCash != 3)
                                <tr style="background: none;">
                                    <td>Due Date</td>
                                    <td>:</td>
                                    <td>{{ date("d-M-Y", strtotime("+" . $header->LongTerm . " day", strtotime($header->PurchaseOrderDate))) }}</td>
                                </tr>
                                @endif
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            
            <table class="tableBorder fz14" cellspacing="0" cellpadding="0" width="100%" style="clear: both; margin-bottom: 2px;">
                <thead>
                    <tr>
                        <th width="30%">Inventory</th>
                        <th width="5%">Uom</th>
                        <th width="5%">Qty </th>
                        <th width="20%">Price</th>
                        <th width="5%">Disc (%)</th>
                        <th width="10%">Disc</th>
                        <th width="15%">Subtotal</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $total = 0;
                    $totalVAT = 0;
                    $counter = 1;
                    ?>

                    @if (count($detail) > 0)
                    @foreach ($detail as $data)
                    <tr>
                        @if($data->Remark != '')
                        <td>{{ $data->Remark }}</td>
                        @else
                        <td>{{ $data->inventory->InventoryID.' '.$data->inventory->InventoryName }}</td>
                        @endif
                        <td style="text-align: center">{{ $data->uom->UomID }}</td>
                        <td style="text-align: right">{{ number_format($data->Qty, '0', '.', ',') }}</td>
                        <td style="text-align: right">{{ number_format($data->Price, '2', '.', ',') }}</td>
                        <td style="text-align: right">{{ $data->Discount }}</td>
                        <td style="text-align: right">{{ number_format($data->DiscountNominal, '0', '.', ',') }}</td>
                        <td style="text-align: right">{{ number_format($data->SubTotal, '0', '.', ',') }}</td>
                    </tr>
                    <?php
                    $totalVAT += $data->VAT;
                    $total += $data->SubTotal;
                    $counter++;
                    ?>
                    @endforeach
                    @if ($totalVAT != 0) 
                    <?php
                    $totalVAT = $totalVAT - ($header->DiscountGlobal * 0.1);
                    ?>
                    @endif

                    @else
                    <tr>
                        <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this sales.</td>
                    </tr>
                    @endif
                </tbody>
            </table>

            <table class="fz14" style="width:100%;table-layout:fixed">               
                <tr style="background-color:none;">
                    <td style="width: 310px;vertical-align: text-top;background-color:none;">
                        <table class="fz14" width="100%" style="margin-top: 10px;text-align:left;">
                            <tr>
                                <td width="25%">Kirim Via</td>
                                <td width="2%">:</td>
                                @if($header->KirimVia  != '' && $header->KirimVia != null)
                                <td>{{ $header->KirimVia }}</td>
                                @else
                                <td>-</td>
                                @endif
                            </tr>
                            <tr>
                                <td width="25%">Remark</td>
                                <td width="2%">:</td>
                                <td>{{ $header->Remark }}</td>
                            </tr>
                        </table>   
                    </td>     

                    <td style="width: 170px;vertical-align: text-top;background-color:none;">
                        <table cellspacing="0" cellpadding="0" style="text-align:right; width:100%;table-layout: fixed;">
                            <tr>
                                <td width="50%">Total</td>
                                <td width="2%">:</td>
                                <td>{{ number_format($total, '0', '.', ',') }}</td>
                            </tr>
                            @if ($header->DiscountGlobal != 0) 
                            <tr>
                                <td>Discount</td>
                                <td>:</td>
                                <td>{{ number_format($header->DiscountGlobal, '0', '.', ',') }}</td>
                            </tr>
                            <tr>
                                <td>Grand Total</td>
                                <td>:</td>
                                <td>{{ number_format($total - $header->DiscountGlobal, '0', '.', ',') }}</td>
                            </tr>
                            @endif
                            @if ($totalVAT != 0) 
                            <tr>
                                <td>PPN</td>
                                <td>:</td>
                                <td>{{ number_format($totalVAT, '0', '.', ',') }}</td>
                            </tr>
                            <tr>
                                <td>Grand Total (PPN)</td>
                                <td>:</td>
                                <td>{{ number_format($header->GrandTotal, '0', '.', ',') }}</td>
                            </tr>
                            @endif
                        </table>
                    </td>
                </tr>
                <tr style="background-color:none;text-align: center">
                    <td style="width: 210px;vertical-align: text-top;"colspan="2">
                        @if($header->VAT == 1)
                        <table class="fz14" width="100%" style="margin-top: -80px;">
                            @else
                            <table class="fz14" width="100%" style="margin-top: 0px;" >
                                @endif
                                <tr style="background-color:none;">
                                    <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 80px; font-weight: 500;vertical-align: top" width="100%">
                                        <!--                                        Hormat Kami,-->
                                    </td>
                                    <td width="10%">
                                        <br/>
                                        <br/>
                                        <br/>
                                    </td>
                                    <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 0px; font-weight: 500;vertical-align: top" width="0%">
                                        <!--Disetujui oleh,-->
                                    </td>
                                    <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 0px; font-weight: 500;" width="0%">

                                    </td>
                                </tr>
                                <tr><td><br/><br><br><br><br><br><br><br></td></tr>
                                <tr>
                                    <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-right: 200px; font-weight: 500; text-align: right" width="100%">
                                        ({{ Auth::user()->UserName }})
                                    </td>
                                    <td width="10%">
                                    </td>
                                    <td style="font-family: helvetica,sans-serif; margin:  -4px !important ;padding-right: 100px; padding-left: -130px; font-weight: 500;" width="100%">
                                        @if($header->RequestBy != null)
                                        ({{$header->RequestBy}})
                                        @else
                                        -
                                        @endif
                                    </td>
                                    <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 0px; font-weight: 500;" width="0%">

                                    </td>
                                </tr>                
                                <tr>
                                    <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-right: 200px; font-weight: 500; text-align: right" width="100%">
                                        Prepared By
                                    </td>
                                    <td width="10%">
                                        <br/>
                                        <br/>
                                        <br/>
                                    </td>
                                    <td style="font-family: helvetica,sans-serif; margin:  4px !important;text-align: left ;padding-right: -200px; font-weight: 500;" width="100%">
                                        Request By
                                    </td>
                                    <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 0px; font-weight: 500;" width="0%">

                                    </td>

                                </tr>                
                            </table>   
                    </td>     
                </tr>
            </table> 
        </div>
    </body>
</html>
<script src="{{Asset('lib/bootstrap/js/jquery-1.11.1.min.js')}}"></script>
<script>
$(document).ready(function () {
    window.print();
});
</script>
