<!DOCTYPE html>
<html>
    <head>
        <style>
            <!--
            @page { size : 5.5in 9.4in; }
            table { page : 5.5in 9.4in; }
            -->

            /*style="padding: 1px; border: none; font-family: arial,sans-serif;font-size: 8px;  font-weight: 500;"*/
            html {
                padding: 0 0 0 0;
                /*margin: 70px 0 0 0;*/
                margin: 0 0 0 0;
                font-size: 8px;
                position: relative;
            }

            body {
                margin: 0;
                font-family: arial, sans-serif;
                overflow:hidden;
            }

            .txt-top{
                vertical-align: text-top;
            }

            .fz12 {
                font-size: 12px;
            }
            .fz10 {
                font-size: 10px; 
            }

            .header-print {
                clear: both;
                border-bottom: 1px solid black;
            }

            .header-print .left-header {
                display: inline-block;
                float:left;
                width: 60%;
            }

            .header-print .right-header {
                display: inline-block;
                width: 40%;
            }


            .main-wrapper .title-report {
                font-size: 12px;
                font-weight: 700;
                margin: 5px 0;
                text-align: center;
            }

            .tableBorder {
                border-spacing: 0;
                border: 0px;
            }
            .tableBorder th{
                padding: 1px;
                border-spacing: 0;
                border: 0.5px solid black;
                border-left: 1px solid black;
                border-right: none;
                text-align: center;
            }

            .tableBorder td:nth-child(4),
            .tableBorder td:nth-child(5),
            .tableBorder td:nth-child(6),
            .tableBorder td:nth-child(7),
            .tableBorder td:nth-child(8) {
                text-align: right;
            }


            .tableBorder th:last-child{ 
                border-right: 1px solid black;
            }
            .tableBorder td{
                border-spacing: 0;
                border: none;
                border-left: none;
                border-right: none;
                border-top: none;
            }
            .tableBorder td:last-child{ 
                border-right: none;
            }
            .footer { 
                font-family: arial,sans-serif; 
                position: fixed; 
                bottom: 0; 
                right: 0;
            }
            .footer .page:after { 
                font-family: arial,sans-serif; 
                font-size: 8px; 
                content: "Page " counter(page);
            }
        </style>
    </head>
    <body>
        <div class="clearfix header-print">
            <!--            <div style="height: 133px">
                            <img height="100%" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                        </div>-->
            <div class="fz12 left-header">
                <div class="fz10 left-header" style="display: inline !important;vertical-align: top;margin-top: -10px;width: 100%">
                    <p style="width: 100%"><img src="{{ Asset('img/unnamed.jpg') }}" style="float: left;width: 40px;height: auto;margin-right: 6px; display: inline-block !important"><span style="font-size: 30px; font-weight: bold;">{{Auth::user()->Company->CompanyName}} </span></p>

                    <br style="clear:both">
                </div> 
            </div>
            <div class="right-header">
                <table  class="fz10" style="float: right;">
                    <tr style="background: none;">
                        <td>Address</td>
                        <td>:</td>
                        <td>{{ Auth::user()->Company->Address . ' ' . Auth::user()->Company->City }}</td>
                    </tr>
                    <tr style="background: none;">
                        <td>Phone / Fax</td>
                        <td>:</td>
                        <td>{{ Auth::user()->Company->Phone }} / {{Auth::user()->Company->Fax}}</td>
                    </tr>
                </table>
            </div>           
        </div>

        <div class="main-wrapper">
            <h3 class="title-report">Purchase</h3>

            <div class="fz10">
                <table width="100%" style="table-layout:fixed">
                    <tr>
                        <td width="50%">
                            <table width="100%">
                                <tr>
                                    <td>Purchase ID</td>
                                    <td>:</td>
                                    <td>{{ $header->PurchaseID }}</td>
                                </tr>
                                <tr>
                                    <td>Date</td>
                                    <td>:</td>
                                    <td>{{ date("d-M-Y", strtotime($header->PurchaseDate)) }}</td>
                                </tr>
                                <tr>
                                    <td>Warehouse</td>
                                    <td>:</td>
                                    <td>{{ $header->warehouse->WarehouseName }}</td>
                                </tr>
                            </table>
                        </td>
                        <td width="50%">
                            <table class="fz10" width="100%" style="margin-left:40%;">
                                <tr>
                                    <td class="txt-top">Supplier</td>
                                    <td class="txt-top">:</td>
                                    <td>{{ $supplier }}</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>  

            <table class="tableBorder fz12" cellspacing="0" cellpadding="0" width="100%" style="clear: both; margin-bottom: 2px;">
                <thead>
                    <tr>
                        <th width="40%">Inventory</th>
                        <th width="5%">Uom</th>
                        <th width="5%">Qty</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $counter = 1;
                    ?>

                    @if (count($detail) > 0)
                    @foreach ($detail as $data)
                    <tr>
                        <td>{{ $data->inventory->InventoryName }}</td>
                        <td style="text-align: center">{{ $data->uom->UomID }}</td>
                        <td style="text-align: right">{{ number_format($data->Qty, '0', '.', ',') }}</td>
                    </tr>
                    <?php
                    $counter++;
                    ?>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="3" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this sales order.</td>
                    </tr>
                    @endif
                </tbody>
            </table>


            <table class="fz12" style="width:100%;">           
                <tr style="background-color:none;">
                    <td style="width: 310px;vertical-align: text-top;background-color:none;">
                        <table class="fz12" width="100%" style="margin-top: 10px;text-align:left;">
                            <tr>
                                <td width="8%">Remark</td>
                                <td width="2%">:</td>
                                <td>{{ $header->Remark }}</td>
                            </tr>
                        </table>   
                    </td>    
                </tr>
                <tr style="background-color:none;">
                    <td style="width: 170px;vertical-align: text-top;">
                        <table class="fz12" width="100%" style="margin-top: 10px;">
                            <tr style="background-color:none;">
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 80px; font-weight: 500;vertical-align: top" width="40%">
                                    Tanda Terima,
                                </td>
                                <td width="20%">
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                </td>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 80px; font-weight: 500;vertical-align: top" width="20%">
                                    Hormat Kami,
                                </td>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 20px; font-weight: 500;" width="20%">

                                </td>
                            </tr>
                            <tr><td><br/></td></tr>
                            <tr>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 70px; font-weight: 500;" width="40%">
                                    (_____________)
                                </td>
                                <td width="20%">
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                </td>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 70px; font-weight: 500;" width="20%">
                                    (_____________)
                                </td>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 5px; font-weight: 500;" width="20%">

                                </td>
                            </tr>                
<!--                            <tr>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 80px; font-weight: 500;" width="40%">
                                    (__________)
                                </td>
                                <td width="20%">
                                    <br/>
                                </td>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 65px; font-weight: 500;" width="20%">
                                </td>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 5px; font-weight: 500;" width="20%">

                                </td>
                            </tr>                -->
                        </table>   
                    </td>     
                </tr>
            </table> 
        </div>
    </body>
</html>
<script src="{{Asset('lib/bootstrap/js/jquery-1.11.1.min.js')}}"></script>
<script>
$(document).ready(function () {
    window.print();
});
</script>
