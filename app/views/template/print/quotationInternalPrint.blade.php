<!DOCTYPE html>
<html>
    <head>
        <style>
            <!--
            @page { size :  8.27in 11.69in;}

            -->

            /*style="padding: 1px; border: none; font-family: arial,sans-serif;font-size: 8px;  font-weight: 500;"*/
            html {
                padding: 0 0 0 0;
                /*margin: 70px 0 0 0;*/
                margin: 0 0 0 0;
                font-size: 8px;
                position: relative;
            }

            body {
                margin: 0;
                font-family: arial, sans-serif; 
            }

            .txt-top{
                vertical-align: text-top;
            }

            .fz12 {
                font-size: 12.5px;
            }
            .fz10 {
                font-size: 10px; 
            }

            .header-print {
                clear: both;
                /*border-bottom: 1px solid black;*/
            }

            .header-print .left-header {
                display: inline-block;
                float:left;
                width: 50%;
            }

            .header-print .right-header {
                display: inline-block;
                width: 60%;
                line-height: 0.9;
                margin-top:5px;
                margin-bottom:5px;
            }


            .main-wrapper .title-report {
                font-size: 16px;
                font-weight: 700;
                margin: 5px 0;
                text-align: center;
            }

            .tableBorder th{
                padding: 1px; 
                border: 0.5px solid black;
                border-left: 1px solid black;
                border-right: none;
                text-align: center;
            }


            .tableBorder th:last-child{ 
                border-right: 1px solid black;
            }
            .tableBorder td{
                border-spacing: 0; 
                border-left: none;
                border-right: none;
                border-top: none;
                padding: 5px;
            }
            .tableBorder td:last-child{ 
                border-right: none;
            } 
            .table { 
                margin-bottom: 20px; 
            } 
            .table > thead > tr > th { 
                border-bottom: 2px solid black;
            }  
            .table-bordered {
                border: 1px solid black;
            }
            .table-bordered > thead > tr > th,
            .table-bordered > tbody > tr > th,
            .table-bordered > tfoot > tr > th,
            .table-bordered > thead > tr > td,
            .table-bordered > tbody > tr > td,
            .table-bordered > tfoot > tr > td {
                border: 1px solid black;
                padding: 2px;
            }
        </style>
    </head>
    <body>
        @if($header->VAT == 1)
        <div class="clearfix header-print">
            <!--            <div style="height: 133px">
                            <img height="100%" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                        </div>-->
            <div class="fz10 left-header">
                <p style="font-size: 30px; font-weight: bold;margin-top: -5px;"><img src="{{ Asset('img/unnamed.jpg') }}" style="width: 40px;height: auto;margin-right: 6px;">{{Auth::user()->Company->CompanyName}} </p>

                <br style="clear:both">
            </div> 
            <div class="right-header">
                <table  class="fz10" style="float: right;">
                    <tr style="background: none;padding:0px;margin:0px;">
                        <td>Address</td>
                        <td>:</td>
                        <td>{{ Auth::user()->Company->Address . ' ' . Auth::user()->Company->City }}</td>
                    </tr>
                    <tr style="background: none;padding:0px;margin:0px;">
                        <td>Phone / Fax</td>
                        <td>:</td>
                        <td>{{ Auth::user()->Company->Phone }} / {{Auth::user()->Company->Fax}}</td>
                    </tr>
                    <tr style="background: none;padding:0px;margin:0px;">
                        <td>Email</td>
                        <td>:</td>
                        <td>{{ Auth::user()->Company->Email }}</td>
                    </tr> 
                    <tr style="background: none;padding:0px;margin:0px;">
                        <td>Web</td>
                        <td>:</td>
                        <td> {{ getCompanyWebsite(); }}</td>
                    </tr> 
                </table>
            </div>           
        </div>
        @endif
        <div class="main-wrapper">
            <h3 class="title-report" style="float:left;margin-top:10px;">Quotation</h3>
            <div class="fz10" style="margin-top:10px;margin-bottom:10px;">
                <table width="100%" style="table-layout:fixed">
                    <tr style="vertical-align: text-top">
                        <td width="40%">
                            <table width="100%">
                                <tr>
                                    <td>Quotation ID</td>
                                    <td>:</td>
                                    <td>{{ $header->QuotationID }}</td>
                                </tr>
                                <tr>
                                    <td>Date</td>
                                    <td>:</td>
                                    <td>{{ date("d-M-Y", strtotime($header->QuotationDate)) }}</td>
                                </tr>
<!--                                <tr>
                                    <td>VAT</td>
                                    <td>:</td>
                                    <td>{{ $vat }}</td>
                                </tr>-->
                            </table>
                        </td>
                        <td width="70%">
                            <table class="fz10" width="100%" style="margin-left:40%;">
                                <tr>
                                    <td class="txt-top">Customer</td>
                                    <td class="txt-top">:</td>
                                    <td>{{ $customer }}</td>
                                </tr>
                                <tr>
                                    <td class="txt-top">Attn</td>
                                    <td class="txt-top">:</td>
                                    <td>{{ $attn }}</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>  

            <table class="table-bordered fz12" cellspacing="0" cellpadding="0" width="100%" style="clear: both; margin-bottom: 2px;">
                <thead>
                    <tr>
                        <th width="4%">No</th>
                        <th width="40%">Inventory</th>
                        <th width="5%">Uom</th>
                        <th width="5%">Qty </th>
                        <th width="15%">Price</th>
                        <th width="10%">Disc (%)</th>
                        <th width="10%">Disc</th>
                        <th width="15%">Subtotal</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $total = 0;
                    $totalVAT = 0;
                    $counter = 1;
                    ?>

                    @if (count($description) > 0)
                    @foreach ($description as $data)
                    <?php
                    $detail = QuotationDetail::where('DescriptionInternalID', $data->InternalID)->where('QuotationParcelInternalID', 0)->get();
                    $parcel = QuotationParcel::where('DescriptionInternalID', $data->InternalID)->get();
                    ?>
                    <tr>
                        <td style="text-align:center;vertical-align: top;<?php echo ((1 + ($data->Spesifikasi != '' ? 1 : 0) + count($detail) + count($parcel)) > 0 ? 'border-bottom:none;' : '') ?>">{{ $counter }}</td>
                        <td>{{ $data->InventoryText }}</td>
                        <td style="text-align: center">{{ $data->UomText }}</td>
                        <td style="text-align: right">{{ number_format($data->Qty, '0', '.', ',') }}</td>
                        <td style="text-align: right">{{ number_format($data->Price, '0', '.', ',') }}</td>
                        <td style="text-align: right">{{ $data->Discount }}</td>
                        <td style="text-align: right">{{ number_format($data->DiscountNominal, '0', '.', ',') }}</td>
                        <td style="text-align: right">{{ number_format($data->SubTotal, '0', '.', ',') }}</td>
                    </tr>
                    @if($data->Spesifikasi != '' )
                    <tr>
                        <?php echo ((1 + ($data->Spesifikasi != '' ? 1 : 0) + count($detail) + count($parcel)) > 0 ? '<td style="border-top:none;border-right:1px solid black;"></td>' : '') ?>
                        <td colspan="7">
                            <?php
                            $line = explode(PHP_EOL, $data->Spesifikasi);
//                            $line = preg_split("/\\r\\n|\\r|\\n/", $data->Spesifikasi);
                            foreach ($line as $l) {
                                if (count(explode(";", $l)) > 1) {
                                    ?>
                                    <span style="display: inline-block;width: 100px">
                                    <?php echo explode(";", $l)[0]; ?>
                                    </span>
                                    <span colspan="4">
        <?php echo ": " . explode(";", $l)[1]; ?>
                                    </span>
                                    <br>
        <?php
    } else {
        ?>
                                    <span>
                                    <?php echo $l ?>
                                    </span>
                                    <br>
                                        <?php
                                    }
                                }
                                ?>
                        </td>
                    </tr>
                    @endif
                    @foreach($detail as $data2)
                    <tr>
                        <td></td>
<!--                        <td><i>{{ $data2->inventory->InventoryName }}</i></td>-->
                        @if($data2->inventory->Power > 0)
                        <td style="text-align: left;padding-left:2px;"><i>{{ "[".Variety::find(Inventory::find($data2->inventory->InternalID)->VarietyInternalID)->VarietyName.'] '.$data2->inventory->InventoryName." (".$data2->inventory->Power.")" }}</i></td>
                        @else
                        <td style="text-align: left;padding-left:2px;"><i>{{ "[".Variety::find(Inventory::find($data2->inventory->InternalID)->VarietyInternalID)->VarietyName.'] '.$data2->inventory->InventoryName }}</i></td>
                        @endif
                        <td style="text-align: center">{{ $data2->uom->UomID }}</td>
                        <td style="text-align: right">{{ number_format($data2->Qty, '0', '.', ',') }}</td>
                        <td style="text-align: right">{{ number_format($data2->Price, '0', '.', ',') }}</td>
                        <td style="text-align: right">{{ $data2->Discount }}</td>
                        <td style="text-align: right">{{ number_format($data2->DiscountNominal, '0', '.', ',') }}</td>
                        <td style="text-align: right">{{ number_format($data2->SubTotal, '0', '.', ',') }}</td>
                    </tr>
                    @endforeach
                    @foreach($parcel as $data2)
                    <tr>
                        <td></td>
                        <td><i>{{ $data2->parcel->ParcelName }}</i></td>
                        <td>-</td>
                        <td style="text-align: right">{{ number_format($data2->Qty, '0', '.', ',') }}</td>
                        <td>{{ number_format($data2->Price, '0', '.', ',') }}</td>
                        <td>{{ $data2->Discount }}</td>
                        <td>{{ number_format($data2->DiscountNominal, '0', '.', ',') }}</td>
                        <td>{{ number_format($data2->SubTotal, '0', '.', ',') }}</td>
                    </tr>
                    @endforeach
<?php
$totalVAT += $data->VAT;
$total += $data->SubTotal;
$counter++;
?>
                    @endforeach
                    @if ($totalVAT != 0) 
                    <?php
                    $totalVAT = $totalVAT - ($header->DiscountGlobal * 0.1);
                    ?>
                    @endif

                    @else
                    <tr>
                        <td colspan="8" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this sales.</td>
                    </tr>
                    @endif
                </tbody>
            </table>

            <table class="fz12" style="width:100%;">               
                <tr style="background-color:none;">
                    <td style="width: 310px !important;vertical-align: text-top;background-color:none;">
                        <table class="fz12" width="100%" style="margin-top: 10px;text-align:left;">
                            <tr style='background-color:none;'>
                                <td width="25%">Delivery Terms</td>
                                <td width="2%">:</td>
                                <td>{{ $header->DeliveryTerms }}</td>
                            </tr>           
                            <tr>
                                <td>Delivery Time</td>
                                <td>:</td>
                                <td>{{ $header->DeliveryTime }}</td>
                            </tr>     
                            <tr>
                                <td>Term of Payment</td>
                                <td>:</td>
                                <td>{{ $header->TermOfPayment }}</td>
                            </tr>
                            @if ($header->isCash != 0)
                            <tr style="background: none;">
                                <td>Due Date</td>
                                <td>:</td>
                                <td>{{ date("d-M-Y", strtotime("+" . $header->LongTerm . " day", strtotime($header->QuotationDate))) }}</td>
                            </tr>
                            @endif
                            <tr>
                                <td width="18%">Validity</td>
                                <td width="2%">:</td>
                                <td>{{ $header->Validity }}</td>
                            </tr>
                            <tr>
                                <td width="18%">Remark</td>
                                <td width="2%">:</td>
                                <td>{{ $header->Remark }}</td>
                            </tr>
                        </table>   
                    </td>     

                    <td style="width: 170px;vertical-align: text-top;background-color:none;">
                        <table cellspacing="0" cellpadding="0" style="text-align:right; width:100%;">
                            <tr>
                                <td width="50%">Total</td>
                                <td width="2%">:</td>
                                <td>{{ number_format($total, '0', '.', ',') }}</td>
                            </tr>
                            @if ($header->DiscountGlobal != 0) 
                            <tr>
                                <td>Discount</td>
                                <td>:</td>
                                <td>{{ number_format($header->DiscountGlobal, '0', '.', ',') }}</td>
                            </tr>
                            <tr>
                                <td>Grand Total</td>
                                <td>:</td>
                                <td>{{ number_format($total - $header->DiscountGlobal, '0', '.', ',') }}</td>
                            </tr>
                            @endif
                            @if ($totalVAT != 0) 
                            <tr>
                                <td>PPN</td>
                                <td>:</td>
                                <td>{{ number_format($totalVAT, '0', '.', ',') }}</td>
                            </tr>
                            <tr>
                                <td>Grand Total (PPN)</td>
                                <td>:</td>
                                <td>{{ number_format($header->GrandTotal, '2', '.', ',') }}</td>
                            </tr>
                            @endif
                        </table>
                    </td>

                </tr>
                <tr style="background-color:none;">
                    <td style="width: 210px;vertical-align: text-top;" colspan="2">
                        <table class="fz12" width="100%" style="margin-top: 10px;">
                            <tr style="background-color:none;">
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 80px; font-weight: 500;vertical-align: top" width="40%">
                                    Hormat Kami,
                                </td>
                                <td width="20%">
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                </td>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 80px; font-weight: 500;vertical-align: top" width="20%">
                                    <!--Disetujui oleh,-->
                                </td>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 20px; font-weight: 500;" width="20%">

                                </td>
                            </tr>
                            <tr><td><br/></td></tr>
                            <tr>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 80px; font-weight: 500;" width="40%">
                                    ({{ Auth::user()->UserName }})
                                </td>
                                <td width="20%">
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                </td>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 60px; font-weight: 500;" width="20%">
                                    <!--(_____________)-->
                                </td>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 5px; font-weight: 500;" width="20%">

                                </td>
                            </tr>                
                        </table>   
                    </td>     
                </tr>
            </table> 
        </div>
    </body>
</html>
<script src="{{Asset('lib/bootstrap/js/jquery-1.11.1.min.js')}}"></script>
<script>
$(document).ready(function () {
    window.print();
});
</script>
