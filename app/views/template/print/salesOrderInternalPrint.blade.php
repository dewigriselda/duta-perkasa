<!DOCTYPE html>
<html>
    <head>
        <style>

            @media print { width: 8.27in; height: 11.69in; }

            /*style="padding: 1px; border: none; font-family: arial,sans-serif;font-size: 8px;  font-weight: 500;"*/
            html {
                padding: 0 0 0 0;
                /*margin: 70px 0 0 0;*/
                margin: 0 0 0 0;
                font-size: 8px;
                position: relative;
            }

            body {
                margin: 0;
                font-family: arial, sans-serif;
                overflow:hidden;
            }

            .txt-top{
                vertical-align: text-top;
            }

            .fz12 {
                font-size: 12px;
            }
            .fz10 {
                font-size: 10px; 
            }

            .header-print {
                clear: both;
                border-bottom: 1px solid black;
            }

            .header-print .left-header {
                display: inline-block;
                float:left;
                width: 50%;
            }

            .header-print .right-header {
                display: inline-block;
                width: 50%;
            }


            .main-wrapper .title-report {
                font-size: 12px;
                font-weight: 700;
                margin: 5px 0;
                text-align: center;
            }

            .tableBorder {
                border-spacing: 0;
                border: 0px;
            }
            .tableBorder th{
                padding: 1px;
                border-spacing: 0;
                border: 0.5px solid black;
                border-left: 1px solid black;
                border-right: none;
                text-align: center;
            }

            .tableBorder td:nth-child(4),
            .tableBorder td:nth-child(5),
            .tableBorder td:nth-child(6),
            .tableBorder td:nth-child(7),
            .tableBorder td:nth-child(8) {
                text-align: right;
            }


            .tableBorder th:last-child{ 
                border-right: 1px solid black;
            }
            .tableBorder td{
                border-spacing: 0;
                border: none;
                border-left: none;
                border-right: none;
                border-top: none;
            }
            .tableBorder td:last-child{ 
                border-right: none;
            }
            .footer { 
                font-family: arial,sans-serif; 
                position: fixed; 
                bottom: 0; 
                right: 0;
            }
            .footer .page:after { 
                font-family: arial,sans-serif; 
                font-size: 8px; 
                content: "Page " counter(page);
            }

            .table {
                width: 100%;
                max-width: 100%;
                margin-bottom: 20px;
            }
            .table > thead > tr > th,
            .table > tbody > tr > th,
            .table > tfoot > tr > th,
            .table > thead > tr > td,
            .table > tbody > tr > td,
            .table > tfoot > tr > td {
                padding: 8px;
                line-height: 1.42857143;
                vertical-align: top;
                border-top: 1px solid black;
            }
            .table > thead > tr > th {
                vertical-align: bottom;
                border-bottom: 2px solid black;
            }
            .table > caption + thead > tr:first-child > th,
            .table > colgroup + thead > tr:first-child > th,
            .table > thead:first-child > tr:first-child > th,
            .table > caption + thead > tr:first-child > td,
            .table > colgroup + thead > tr:first-child > td,
            .table > thead:first-child > tr:first-child > td {
                border-top: 0;
            }
            .table > tbody + tbody {
                border-top: 2px solid black;
            }
            .table .table {
                background-color: #fff;
            }
            .table-condensed > thead > tr > th,
            .table-condensed > tbody > tr > th,
            .table-condensed > tfoot > tr > th,
            .table-condensed > thead > tr > td,
            .table-condensed > tbody > tr > td,
            .table-condensed > tfoot > tr > td {
                padding: 5px;
            }
            .table-bordered {
                border: 1px solid black;
            }
            .table-bordered > thead > tr > th,
            .table-bordered > tbody > tr > th,
            .table-bordered > tfoot > tr > th,
            .table-bordered > thead > tr > td,
            .table-bordered > tbody > tr > td,
            .table-bordered > tfoot > tr > td {
                border: 1px solid black;
            }
            .table-bordered > thead > tr > th,
            .table-bordered > thead > tr > td {
                border-bottom-width: 2px;
            }
        </style>
    </head>
    <body>
        @if($header->VAT == 1)
        <div class="clearfix header-print">
            <!--            <div style="height: 133px">
                            <img height="100%" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                        </div>-->
            <div class="fz12 left-header">
                <div class="fz10 left-header" style="display: inline !important;vertical-align: top;margin-top: -10px;width: 100%">
                    <p style="width: 100%"><img src="{{ Asset('img/unnamed.jpg') }}" style="float: left;width: 40px;height: auto;margin-right: 6px; display: inline-block !important"><span style="font-size: 30px; font-weight: bold;">{{Auth::user()->Company->CompanyName}} </span></p>

                    <br style="clear:both">
                </div> 
            </div> 
            <div class="right-header">
                <table  class="fz12" style="float: right;">
                    <tr style="background: none;">
                        <td>Address</td>
                        <td>:</td>
                        <td>{{ Auth::user()->Company->Address . ' ' . Auth::user()->Company->City }}</td>
                    </tr>
                    <tr style="background: none;">
                        <td>Phone / Fax</td>
                        <td>:</td>
                        <td>{{ Auth::user()->Company->Phone }} / {{Auth::user()->Company->Fax}}</td>
                    </tr>
                    <tr style="background: none;">
                        <td>Email</td>
                        <td>:</td>
                        <td>{{ Auth::user()->Company->Email }}</td>
                    </tr> 
                    <tr style="background: none;">
                        <td>Web</td>
                        <td>:</td>
                        <td> {{ getCompanyWebsite(); }}</td>
                    </tr> 
                </table>
            </div>           
        </div>
        @endif
        <div class="main-wrapper">
            <h3 class="title-report">Sales Order</h3>

            <div class="fz10">
                <table width="100%" style="table-layout:fixed">
                    <tr style="vertical-align: text-top">
                        <td width="50%">
                            <table width="100%">
                                <tr>
                                    <td>Order ID</td>
                                    <td>:</td>
                                    <td>{{ $header->SalesOrderID }}</td>
                                </tr>
                                <tr>
                                    <td>Date</td>
                                    <td>:</td>
                                    <td>{{ date("d-M-Y", strtotime($header->SalesOrderDate)) }}</td>
                                </tr>
                                <tr>
                                    <td>PO Customer</td>
                                    <td>:</td>
                                    <td>{{ $header->POCustomer }}</td>
                                </tr>
                                <tr>
                                    <td>Purchasing Commission</td>
                                    <td>:</td>
                                    <td>{{ number_format($header->PurchasingCommission, '2', '.', ',') }}</td>
                                </tr>
                            </table>
                        </td>

                        <td width="50%">
                            <table class="fz10" width="100%" style="margin-left:40%;">
                                <tr>
                                    <td class="txt-top">Customer</td>
                                    <td class="txt-top">:</td>
                                    <td>{{ $customer }}</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>  

            <table class="table-bordered fz12" cellspacing="0" cellpadding="0" width="100%" style="clear: both; margin-bottom: 2px;">
                <thead>
                    <tr>
                        <th width="5%">No</th>
                        <th width="40%">Inventory</th>
                        <th width="5%">Uom</th>
                        <th width="5%">Qty</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $counter = 1;
                    ?>

                    @if (count($description) > 0) 
                    @foreach ($description as $data)
                    <?php
                    $detail = SalesOrderDetail::where('DescriptionInternalID', $data->InternalID)->where('SalesOrderParcelInternalID', 0)->get();
                    $parcel = SalesOrderParcel::where('DescriptionInternalID', $data->InternalID)->get();
                    ?>
                    <tr>
                        <td style="text-align: center;vertical-align: top" rowspan="<?php echo (1 + ($data->Spesifikasi != '' ? 1 : 0) + count($detail) + count($parcel)) ?>">{{ $counter }}</td>
                        <td>{{ $data->InventoryText }}</td>
                        <td style="text-align: center">{{ $data->UomText }}</td>
                        <td style="text-align: right">{{ number_format($data->Qty, '0', '.', ',') }}</td>
                    </tr>
                    @if($data->Spesifikasi != '' )
                    <tr>
                        <td colspan="3">
                            <?php
                            $line = explode(PHP_EOL, $data->Spesifikasi);
//                            $line = preg_split("/\\r\\n|\\r|\\n/", $data->Spesifikasi);
                            foreach ($line as $l) {
                                if (count(explode(";", $l)) > 1) {
                                    ?>
                                    <span style="display: inline-block;width: 100px">
                                    <?php echo explode(";", $l)[0]; ?>
                                    </span>
                                    <span colspan="3">
        <?php echo ": " . explode(";", $l)[1]; ?>
                                    </span>
                                    <br>
        <?php
    } else {
        ?>
                                    <span>
                                    <?php echo $l ?>
                                    </span>
                                    <br>
                                        <?php
                                    }
                                }
                                ?>
                        </td>
                    </tr>
                    @endif
                    @foreach($detail as $data2)
                    <tr>
                        <td><i>{{ $data2->inventory->InventoryName }}</i></td>
                        <td style="text-align: center">{{ $data2->uom->UomID }}</td>
                        <td style="text-align: right">{{ number_format($data2->Qty, '0', '.', ',') }}</td>
                    </tr>
                    @endforeach
                    @foreach($parcel as $data2)
                    <tr>
                        <td><i>{{ $data2->parcel->ParcelName }}></i></td>
                        <td>-</td>
                        <td style="text-align: right">{{ number_format($data2->Qty, '0', '.', ',') }}</td>
                    </tr>
                    @endforeach
<?php
$counter++;
?>
                    @endforeach 
                    @else
                    <tr>
                        <td colspan="4" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this sales order.</td>
                    </tr>
                    @endif
                </tbody>
            </table>


            <table class="fz12" style="width:100%;">               
                <tr style="background-color:none;">
                    <td style="width: 310px;vertical-align: text-top;background-color:none;">
                        <table class="fz12" width="100%" style="margin-top: 10px;text-align:left;">
                            <tr style='background-color:none;'>
                                <td width="25%">Delivery Terms</td>
                                <td width="2%">:</td>
                                <td>{{ $header->DeliveryTerms }}</td>
                            </tr>           
                            <tr>
                                <td>Delivery Time</td>
                                <td>:</td>
                                <td>{{ $header->DeliveryTime }}</td>
                            </tr>           
                            <tr>
                                <td>Term of Payment</td>
                                <td>:</td>
                                <td>{{ $payment }}</td>
                            </tr>
                            @if ($header->isCash != 0)
                            <tr style="background: none;">
                                <td>Due Date</td>
                                <td>:</td>
                                <td>{{ date("d-M-Y", strtotime("+" . $header->LongTerm . " day", strtotime($header->SalesOrderDate))) }}</td>
                            </tr>
                            @endif
                            <tr>
                                <td>Remark</td>
                                <td>:</td>
                                <td>{{ $header->Remark }}</td>
                            </tr>
                        </table>   
                    </td>    
                </tr>
                <tr style="background-color:none;">
                    <td style="width: 210px;vertical-align: text-top;"colspan="2">
                        <table class="fz12" width="100%" style="margin-top: 10px;">
                            <tr style="background-color:none;">
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 80px; font-weight: 500;vertical-align: top" width="40%">
                                    Hormat Kami,
                                </td>
                                <td width="20%">
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                </td>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 80px; font-weight: 500;vertical-align: top" width="20%">
                                    Disetujui oleh,
                                </td>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 20px; font-weight: 500;" width="20%">

                                </td>
                            </tr>
                            <tr><td><br/></td></tr>
                            <tr>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 80px; font-weight: 500;" width="40%">
                                    ({{ Auth::user()->UserName }})
                                </td>
                                <td width="20%">
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                </td>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 70px; font-weight: 500;" width="20%">
                                    (_____________)
                                </td>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 5px; font-weight: 500;" width="20%">

                                </td>
                            </tr>                
<!--                            <tr>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 80px; font-weight: 500;" width="40%">
                                    (__________)
                                </td>
                                <td width="20%">
                                    <br/>
                                </td>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 65px; font-weight: 500;" width="20%">
                                </td>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 5px; font-weight: 500;" width="20%">

                                </td>
                            </tr>                -->
                        </table>   
                    </td>     
                </tr>
            </table> 
        </div>
    </body>
</html>
<script src="{{Asset('lib/bootstrap/js/jquery-1.11.1.min.js')}}"></script>
<script>
$(document).ready(function () {
    window.print();
});
</script>
