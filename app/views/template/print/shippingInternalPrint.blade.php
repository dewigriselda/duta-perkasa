<!DOCTYPE html>
<html>
    <head>
		<title>
            {{$header->ShippingID}} - {{$namacustomer}}..
        </title>
        <style> 

            <!--@page { size: 21cm 16cm; margin: 0cm 1.4cm 0.5cm 0.1cm  }-->
            /*style="padding: 1px; border: none; font-family: arial,sans-serif;font-size: 8px;  font-weight: 500;"*/
            html {
                padding: 0px;
                margin: 0px;
                font-size: 8px;
                position: relative;
            }

            body {
                margin: 0px;
                font-family: arial, sans-serif;
                overflow:hidden;
            }

            .txt-top{
                vertical-align: text-top;
            }

            .fz12 {
                font-size: 14px;
            }
			.fz13 {
                font-size: 15px;
            }
            .fz10 {
                font-size: 12px; 
            }

            .header-print {
                clear: both;
                border-bottom: 1px solid black;
            }

            .header-print .left-header {
                display: inline-block;
                float:left;
                width: 50%;
            }

            .header-print .right-header {
                display: inline-block;
                width: 50%;
            }


            .main-wrapper .title-report {
                font-size: 16px;
                font-weight: 700;
                margin: 5px 0;
                text-align: center;
            }

            .footer { 
                font-family: arial,sans-serif; 
                position: fixed; 
                bottom: 0; 
                right: 0;
            }
            .footer .page:after { 
                font-family: arial,sans-serif; 
                font-size: 8px; 
                content: "Page " counter(page);
            }

            .table {
                width: 100%;
                max-width: 100%;
                margin-bottom: 20px;
            }
            .table > thead > tr > th,
            .table > tbody > tr > th,
            .table > tfoot > tr > th,
            .table > thead > tr > td,
            .table > tbody > tr > td,
            .table > tfoot > tr > td {
                padding: 8px;
                line-height: 1.42857143;
                vertical-align: top;
            }
            .table > thead > tr > th {
                vertical-align: bottom;
            }
            .table > caption + thead > tr:first-child > th,
            .table > colgroup + thead > tr:first-child > th,
            .table > thead:first-child > tr:first-child > th,
            .table > caption + thead > tr:first-child > td,
            .table > colgroup + thead > tr:first-child > td,
            .table > thead:first-child > tr:first-child > td {
            }
            .table > tbody + tbody {
            }
            .table .table {
                background-color: #fff;
            }
            .table-condensed > thead > tr > th,
            .table-condensed > tbody > tr > th,
            .table-condensed > tfoot > tr > th,
            .table-condensed > thead > tr > td,
            .table-condensed > tbody > tr > td,
            .table-condensed > tfoot > tr > td {
                padding: 5px;
            }
            table.table-bordered {
                border-collapse: collapse;
                border: 1px solid black;
            }
            table.table-bordered td{
                border: 1px solid black;
                padding: 1px 2px;
            }
            table.table-bordered th{
                border: 1px solid black;
            }
        </style>
    </head>
    <body>
        @if($header->VAT == 1)
        <div class="clearfix header-print">
            <!--            <div style="height: 133px">
                            <img height="100%" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                        </div>-->
            <div class="fz12 left-header">
                <div class="fz10 left-header" style="display: inline !important;vertical-align: top;margin-top: -10px;width: 100%">
                    <p style="width: 100%"><img src="{{ Asset('img/unnamed.jpg') }}" style="float: left;width: 40px;height: auto;margin-right: 6px; display: inline-block !important"><span style="font-size: 30px; font-weight: bold;">{{Auth::user()->Company->CompanyName}} </span></p>

                    <br style="clear:both">
                </div> 
            </div> 
            <div class="right-header">
                <table  class="fz10" style="float: right;">
                    <tr style="background: none;">
                        <td>Address</td>
                        <td>:</td>
                        <td>{{ Auth::user()->Company->Address . ' ' . Auth::user()->Company->City }}</td>
                    </tr>
                    <tr style="background: none;">
                        <td>Phone / Fax</td>
                        <td>:</td>
                        <td>{{ Auth::user()->Company->Phone }} / {{Auth::user()->Company->Fax}}</td>
                    </tr>
                    <tr style="background: none;">
                        <td>Email</td>
                        <td>:</td>
                        <td>{{ Auth::user()->Company->Email }}</td>
                    </tr> 
                    <tr style="background: none;">
                        <td>Web</td>
                        <td>:</td>
                        <td> {{ getCompanyWebsite(); }}</td>
                    </tr> 
                </table>
            </div>           
        </div>
        @endif
        <div class="main-wrapper">
            <h1 class="title-report">Shipping</h1>

            <div class="fz10">
                <table width="100%" style="table-layout:fixed">
                    <tr>
                        <td width="50%" class="fz13">
                            <table width="100%">
                                <tr>
                                    <td>Shipping ID</td>
                                    <td>:</td>
                                    <td>{{ $header->ShippingID }}</td>
                                </tr>
                                <tr>
                                    <td>Date</td>
                                    <td>:</td>
                                    <td>{{ date("d-M-Y", strtotime($header->ShippingDate)) }}</td>
                                </tr>
<!--                                <tr>
                                    <td>Payment</td>
                                    <td>:</td>
                                    <td>{{ $payment }}</td>
                                </tr>-->
                                <tr>
                                    <td>Warehouse</td>
                                    <td>:</td>
                                    <td>{{ $header->warehouse->WarehouseName }}</td>
                                </tr>
                                <tr>
                                    <td>Number of Vehicle</td>
                                    <td>:</td>
                                    <td>{{ $header->NumberVehicle }}</td>
                                </tr>
                                <tr>
                                    <td>Driver Name</td>
                                    <td>:</td>
                                    <td>{{ $header->DriverName }}</td>
                                </tr>
                            </table>
                        </td>

                        <td width="40%" style="vertical-align: top">
                            <table class="fz10" width="100%" style="margin-left:0%;">
                                <tr>
                                    <td class="txt-top">Customer</td>
                                    <td class="txt-top">:</td>
                                    <td>{{ $customer }}</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>  

            <table class="table-bordered fz10" cellspacing="0" cellpadding="0" width="100%" style="clear: both; margin-bottom: 2px;">
                <thead>
                    <tr>
                        <th width="5%">No</th>
                        <th width="40%">Inventory</th>
                        <th width="5%">Uom</th>
                        <th width="5%">Qty</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $counter = 1;
                    ?>

                    @if (count($description) > 0)
                    @foreach ($description as $data)
                    <?php
                    $detail = ShippingAddDetail::where('DescriptionInternalID', $data->InternalID)->where('ShippingParcelInternalID', 0)->get();
                    $parcel = ShippingAddParcel::where('DescriptionInternalID', $data->InternalID)->get();
                    ?>
                    <tr>
                        <td style="text-align: center;vertical-align: top" rowspan="<?php echo (1 + ($data->Spesifikasi != '' ? 1 : 0) + count($detail) + count($parcel)) ?>">{{ $counter }}</td>
                        <td style="text-align: left;padding-left:2px;">{{ $data->InventoryText }}</td>
                        <td style="text-align: center">{{ $data->UomText }}</td>
                        <td style="text-align: right">{{ number_format($data->Qty, '0', '.', ',') }}</td>
                    </tr>
                    @if($data->Spesifikasi != '' )
                    <tr>
                        <td colspan="3" style="padding-left:2px;">
                            <?php
                            $line = explode(PHP_EOL, $data->Spesifikasi);
//                            $line = preg_split("/\\r\\n|\\r|\\n/", $data->Spesifikasi);
                            foreach ($line as $l) {
                                if (count(explode(";", $l)) > 1) {
                                    ?>
                                    <span style="display: inline-block;width: 100px">
                                        <?php echo explode(";", $l)[0]; ?>
                                    </span>
                                    <span colspan="2">
                                        <?php echo ": " . explode(";", $l)[1]; ?>
                                    </span>
                                    <br>
                                    <?php
                                } else {
                                    ?>
                                    <span>
                                        <?php echo $l ?>
                                    </span>
                                    <br>
                                    <?php
                                }
                            }
                            ?>
                        </td>
                    </tr>
                    @endif
                    @foreach($detail as $data2)
                    <tr>
                        @if($data2->inventory->Power > 0)
                        <td style="text-align: left;padding-left:2px;"><i>{{ "[".Variety::find(Inventory::find($data2->inventory->InternalID)->VarietyInternalID)->VarietyName.'] '.$data2->inventory->InventoryName." (".$data2->inventory->Power.")" }}</i></td>
                        @else
                        <td style="text-align: left;padding-left:2px;"><i>{{ "[".Variety::find(Inventory::find($data2->inventory->InternalID)->VarietyInternalID)->VarietyName.'] '.$data2->inventory->InventoryName }}</i></td>
                        @endif
                        <td style="text-align: center;">{{ $data2->uom->UomID }}</td>
                        <td style="text-align: right;">{{ number_format($data2->Qty, '0', '.', ',') }}</td>
                    </tr>
                    @endforeach
                    @foreach($parcel as $data2)
                    <tr>
                        <td><i>{{ $data2->parcel->ParcelName }}></i></td>
                        <td>-</td>
                        <td style="text-align: right">{{ number_format($data2->Qty, '0', '.', ',') }}</td>
                    </tr>
                    @endforeach
                    <?php
                    $counter++;
                    ?>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="4" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this sales order.</td>
                    </tr>
                    @endif
                </tbody>
            </table>

            <table class="fz12" style="width:100%;">               
                <tr style="background-color:none;">
                    <td style="width: 310px;vertical-align: text-top;background-color:none;vertical-align: top;">
                        <table class="fz12" width="100%" style="margin-top: 10px;text-align:left;">
                            <tr style="background-color:none;">
                                <td width="10%" style="vertical-align: top">Remark</td>
                                <td style="vertical-align: top">:</td>
                                <td>{{ $header->Remark }}</td>
                            </tr>
                            <tr style="background-color:none;">
                                <td colspan="3">Perhatian Barang2 yang sudah diterima tidak dapat ditukar
                                    / dikembalikan</td>
                            </tr>
                        </table>   
                    </td>     
                    <td style="width: 210px;vertical-align: text-top;background-color:none;"colspan="2">
                        <table class="fz12" width="100%" style="margin-top: 10px;">
                            <tr style="background-color:none;vertical-align: top">
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 80px; font-weight: 500;" width="50%">
                                    Tanda Terima,
                                </td>
                                <td width="10%">
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                </td>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 80px; font-weight: 500;" width="40%">
                                    Hormat Kami, 
                                </td>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 20px; font-weight: 500;" width="10%">

                                </td>
                            </tr>
                            <tr><td><br/></td></tr>
                            <tr>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 80px; font-weight: 500;" width="50%">
                                    ({{ Auth::user()->UserName }})
                                </td>
                                <td width="10%">
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                </td>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 70px; font-weight: 500;" width="40%">
                                    (____________)
                                </td>
                                <td style="font-family: helvetica,sans-serif; margin:  4px !important; padding-left: 5px; font-weight: 500;" width="10%">

                                </td>
                            </tr>                
                        </table>   
                    </td>     
                </tr>
            </table> 
        </div>
    </body>
</html>
<script src="{{Asset('lib/bootstrap/js/jquery-1.11.1.min.js')}}"></script>
<script>
$(document).ready(function () {
    window.print();
});
</script>
