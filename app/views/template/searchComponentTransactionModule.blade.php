<style>
    #searchwrap .chosen-container {
        width: 100% !important;
    }
</style>

<div class="tabwrap" id="searchwrap">
    <div class="tabhead">
        <h4 class="headtitle">Search</h4>
    </div>
    <div class="tableadd">
        <form method="GET" action="">
            <ul class="searchmenu clearfix">
                <li>
                    <label for="coa6">Customer</label>
                    <br>
                    <div class="row">
                        <div class="col-xs-6">
                            <select class="chosen-select" id="coa6" style="" name="coa6">
                                <option value="-1">All Customer</option>
                                @foreach(Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $coa6)
                                <option value="{{$coa6->InternalID}}">
                                    {{$coa6->ACC6ID.' '.$coa6->ACC6Name}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </li>
                <li><label for="typePayment">Payment Type</label>
                    <br>
                    <select name="typePayment" style="width: 100px">
                        <option value="-1">All</option>
                        <option value="0">Cash</option>
                        <option value="1">Credit</option>
                        <option value="2">CBD</option>
                        <option value="3">Deposit</option>
                        <option value="4">Down Payment</option>
                    </select>
                </li>
                @if(checkSeeNPPN())
<!--                <li><label for="typeTax">Tax Type</label>
                    <br>
                    <select name="typeTax" style="width: 100px">
                        <option value="-1">All</option>
                        <option value="0">Non Tax</option>
                        <option value="1">Tax</option>
                    </select>
                </li>-->
                @endif
                <li><label for="date">Start Date</label>
                    <br><input id="startDate" name="startDate" type="text" autocomplete="off">
                </li>
                <li style="margin-top: 0;"><label for="date">End Date</label>
                    <br><input id="endDate" name="endDate" type="text" autocomplete="off">
                </li>
            </ul>
            <ul class="searchmenu clearfix">
                <li  style="margin-top: 0; margin-bottom: 0;">
                    <button type="submit" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-search"></span> Search </button>
                </li>
                <li  style="margin-top: 0; margin-bottom: 0;">
                    <button id="cancel" type="reset"  class="btn btn-pure btn-sm"><span class="glyphicon glyphicon-remove"></span> Cancel </button>
                </li>
            </ul>
        </form>
    </div><!---- end div tableadd---->
</div><!---- end div tabwrap---->
