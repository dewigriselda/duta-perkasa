@extends('template.header-footer')

@section('title')
Convertion
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
@stop

@section('nav')
@stop

@section('content')
@if(myCheckIsEmpty('InventoryUom;Inventory'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one master Inventory Uom to insert Convertion.
</div>
@endif
@if(isset($messages))
@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Convertion has been inserted.
</div>
@endif
@if($messages == 'gagalInsert')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($errors))
    <ul style="margin-left: 2%">
        @foreach($errors->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesDelete')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Convertion has been deleted.
</div>
@endif
@if($messages == 'gagalDelete')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Convertion has been registered in table convertion detail.
</div>
@endif
@if($messages == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest"> 
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showConvertion')}}" type="button" class="btn btn-sm btn-pure">Convertion</a>
            </div>
            <div class="btn-group margr5">
                <a data-target="#m_convertion" data-toggle="modal" role="dialog">
                    <button <?php if (myCheckIsEmpty('InventoryUom;Inventory')) echo 'disabled'; ?> type="button" class="btn btn-green btn-sm dropdown-toggle  " aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
                </a>
            </div>
        </div>
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
<div class="leftrow pull-left">
</div>
<div class="righttrow pull-right">
</div>
<div class="wrapjour">
    <div class="primcontent">
        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Convertion</h4>
            </div>
            <div class="tableadd">
                <table id="example" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Inventory Uom 1</th>
                            <th>Inventory Uom 2</th>
                            <th>Quantity Convertion</th>
                            <th>Quantity Result</th>
                            <th>Date Convertion</th>
                            <th>Warehouse</th>
                            <th style="min-width: 65px">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $f = rand(0, 50);
                        foreach (Convertion::all() as $data) {
                            $data->dtRecordformat = date("d-m-Y H:i:s", strtotime($data->dtRecord));
                            $data->dtModifformat = date("d-m-Y H:i:s", strtotime($data->dtModified));
                            $data->Remark = str_replace("\r\n", " ", $data->Remark);
                            $data->InventoryName1 = $data->inventoryUom1->Inventory->InventoryName;
                            $data->InventoryName2 = $data->inventoryUom2->Inventory->InventoryName;
                            $data->UomID1 = $data->inventoryUom1->Uom->UomID;
                            $data->UomID2 = $data->inventoryUom2->Uom->UomID;
                            $arrData = array($data);
                            $tamp = myEscapeStringData($arrData);
                            $tamp = myEncryptJavaScriptText($tamp, $f);
                            ?>
                            <tr>
                                <td>{{$data->InventoryUom1->Inventory->InventoryName}} - {{$data->InventoryUom1->Uom->UomID}}</td>
                                <td>{{$data->InventoryUom2->Inventory->InventoryName}} - {{$data->InventoryUom2->Uom->UomID}}</td>
                                <td>{{$data->Quantity}} ({{$data->InventoryUom1->Uom->UomID}})</td>
                                <td>{{$data->QuantityResult}} ({{$data->InventoryUom2->Uom->UomID}})</td>
                                <td>{{date("d-m-Y H:i:s", strtotime($data->DateConvertion))}}</td>
                                <td>{{$data->Warehouse->WarehouseName}}</td>
                                <td class="text-center"><button id="btn-{{$data->InventoryID}}" data-target="#m_convertionUpdate" data-all='{{$tamp}}'
                                                                data-toggle="modal" role="dialog"
                                                                class="btn btn-pure-xs btn-xs btn-edit">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </button>
                                    <button data-target="#m_convertionDelete" data-internal="{{$data->InternalID}}"  data-toggle="modal" role="dialog"
                                            data-id="{{$data->InventoryID}}" class="btn btn-pure-xs btn-xs btn-delete">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div><!---end of tableadd--->
        </div><!---- end div tabwrap---->
    </div><!---- end div wrapjour---->
</div><!---- end div wrapcontent---->
@stop

@section('modal')
<div class="modal fade" id="m_convertion" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insert Convertion</h4>
            </div>
            <form action="" method="POST" class="action" id="form-insert">
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type='hidden' name='jenis' value='insertConvertion'>
                            <li>
                                <label for="InventoryUomInternalID1">Inventory Uom 1</label> *
                            </li>
                            <input class="input-theme margbot10" type="text" id="searchInventoryUom" title="Type Inventory Name or ID then 'Enter'" placeholder="Type Inventory Name or ID then 'Enter'">
                            <li id="selectInventoryUom">

                            </li>
                            <li>
                                <label for="InventoryUomInternalID2">Inventory Uom 2</label> *
                            </li>
                            <li id="liSelect">
                                <span>Please search inventory first</span>
                            </li>
                            <li>
                                <label for="Quantity">Quantity Convertion</label> *
                            </li>
                            <li>
                                <input type="text" class="qty numaja" name="Quantity" id="quantity" value="0" data-validation="required">
                            </li>
                            <li>
                                <label for="Quantity">Quantity Result</label> *
                            </li>
                            <li>
                                <input type="hidden" name="QuantityResult" value="" id="quantityResult">
                                <label id="quantityResultLabel" > - </label>
                            </li>
                            <li>
                                <label for="Warehouse">Warehouse</label> *
                            </li>
                            <li>
                                <select class="chosen-select choosen-modal" id="warehouseInternalID" style="" name="WarehouseInternalID">
                                    @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('InternalID')->get() as $data)
                                    <option value="{{$data->InternalID}}" >
                                        {{$data->WarehouseName}}
                                    </option>
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label for="remark">Remarks</label> *
                            </li>
                            <li>
                                <textarea style="resize:none;"  name="Remark" id="remark" maxlength="1000" data-validation="required"></textarea>
                            </li>
                        </ul>
                    </div>
                    <div class="required">
                        * Required
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade" id="m_convertionUpdate" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Update Convertion</h4>
            </div>
            <form action="" method="POST" class="action" id="form-update">
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="" id="idUpdate" name="InternalID">
                            <input type="hidden" value="updateConvertion" id="jenisUpdate" name="jenis">
                            <li>
                                <label for="InventoryUomInternalID1">Inventory Uom 1</label> *
                            </li>
                            <input class="input-theme margbot10" type="text" id="searchInventoryUomUpdate" title="Type Inventory Name or ID then 'Enter'" placeholder="Type Inventory Name or ID then 'Enter'">
                            <li id="selectInventoryUomUpdate">
                                <select class="input-theme" id="inventoryUom1Update" style="" name="InventoryUomInternalID1">

                                </select>
                            </li>
                            <li>
                                <label for="InventoryUomInternalID2">Inventory Uom 2</label> *
                            </li>
                            <li id="liSelectUpdate">

                            </li>
                            <li>
                                <label for="Quantity">Quantity Convertion</label> *
                            </li>
                            <li>
                                <input type="text" class="qty numaja" name="Quantity" id="quantityUpdate" data-validation="required">
                            </li>
                            <li>
                                <label for="Quantity">Quantity Result</label> *
                            </li>
                            <li>
                                <input type="hidden" name="QuantityResult" value="" id="quantityResultUpdate">
                                <label id="quantityResultLabelUpdate" > - </label>
                            </li>
                            <li>
                                <label for="Warehouse">Warehouse</label> *
                            </li>
                            <li>
                                <select class="chosen-select choosen-modal" id="warehouseInternalIDUpdate" style="" name="WarehouseInternalID">
                                    @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('InternalID')->get() as $data)
                                    <option value="{{$data->InternalID}}" >
                                        {{$data->WarehouseName}}
                                    </option>
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label for="remark">Remarks</label> *
                            </li>
                            <li>
                                <textarea style="resize:none;"  name="Remark" id="remarkUpdate" maxlength="1000" data-validation="required"></textarea>
                            </li>
                        </ul>
                    </div>
                    <div class="required">
                        * Required
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>       
        </div>
    </div>  
</div>

<div class="modal fade" id="m_convertionDelete" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Delete Convertion</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form method="POST" class="action">
                        <ul>
                            <input type="hidden" value="" id="idDelete" name="InternalID">
                            <input type="hidden" value="deleteConvertion" id="jenisDelete" name="jenis">
                            <p>Are you sure?</p>
                        </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-green">Yes</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
            </div>
            </form>      
        </div>
    </div>  
</div>
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script><script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script>
var getSelectedInventoryUom2 = "<?php echo Route("getSelectedInventoryUom2") ?>";
var getSelectedInventoryUom2Update = "<?php echo Route("getSelectedInventoryUom2Update") ?>";
var getInventoryUomInternalID = "<?php echo Route("getInventoryUomInternalID") ?>";
var getResultSearchConvertion = "<?php echo Route("getResultSearchConvertion") ?>";
var getResultSearchConvertionUpdate = "<?php echo Route("getResultSearchConvertionUpdate") ?>";
var b = <?php echo $f ?>;
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-transfer/convertion.js')}}"></script>
@stop