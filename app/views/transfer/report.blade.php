@extends('template.header-footer')

@section('title')
Accounting Report
@stop

@section('css')
<link rel="stylesheet" href="{{Asset('css/chosenCustom.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosenSearch.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
@stop

@section('nav')

@stop

@section('content')
@if(myCheckIsEmpty('Journal'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one journal to create accounting report.
</div>
@endif
@if(Session::get('messages') == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest hidden-xs"> 
            <div class="btn-group bread nomarg" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showReportTransaction')}}" type="button" class="btn btn-sm btn-pure">Report</a>
            </div>
        </div>

        <div class="bread-xs visible-xs hidden-sm hidden-md hidden-lg">
            <p class="text-center"><a href="{{route('showDashboard')}}">{{Config::get('companyHeader.header_company');}}</a> / <a>{{ucfirst($toogle)}}</a> / <a href="{{route('showReportTransaction')}}">Report</a></p>
        </div>
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
<div class="wrapjour">
    <div class="primcontentnopadd">
        <div class="row">
            @if(checkMatrix("ReportBarangMasuk"))
            <div class="col-md-4">
                <div class="tabwrap">
                    <div class="tabhead">
                        <h4 class="headtitle">Report Barang Masuk</h4>
                    </div>
                    <div class="tableadd">
                        <form class="" method="POST" action="" target="_blank">
                            <input type="hidden" value="barangmasuk" name="jenis">
                            <div class="form-group">
                                <label>Start Date</label>
                                <input type="text" name="sDate" id="startDateReport1" data-validation="required" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>End Date</label>
                                <input type="text" name="eDate" id="endDateReport1" data-validation="required" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Warehouse</label>
                                <select class="form-control chosen-select" name="warehouse">
                                    <option value="all">All</option>
                                    @foreach(Warehouse::where("Type",Auth::user()->WarehouseCheck)->get() as $b)
                                    <option value='{{ $b->InternalID }}'>{{ $b->WarehouseName }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button <?php if (myCheckIsEmpty('Journal')) echo 'disabled'; ?> class="btn btn-green btn-sm btn-save text-center pull-right" id="btn-submit"> Process </button>
                        </form>
                    </div><!---- end div tableadd---->   
                </div><!---- end div tabwrap---->                       
            </div><!---- end div col-md-4----> 
            @endif
            @if(checkMatrix("ReportBarangKeluar"))
            <div class="col-md-4">
                <div class="tabwrap">
                    <div class="tabhead">
                        <h4 class="headtitle">Report Barang Keluar</h4>
                    </div>
                    <div class="tableadd">
                        <form class="" method="POST" action="" target="_blank">
                            <input type="hidden" value="barangkeluar" name="jenis">
                            <div class="form-group">
                                <label>Start Date</label>
                                <input type="text" name="sDate" id="startDateReport2" data-validation="required" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>End Date</label>
                                <input type="text" name="eDate" id="endDateReport2" data-validation="required" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Warehouse</label>
                                <select class="form-control chosen-select" name="warehouse">
                                    <option value="all">All</option>
                                    @foreach(Warehouse::where("Type",Auth::user()->WarehouseCheck)->get() as $b)
                                    <option value='{{ $b->InternalID }}'>{{ $b->WarehouseName }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button <?php if (myCheckIsEmpty('Journal')) echo 'disabled'; ?> class="btn btn-green btn-sm btn-save text-center pull-right" id="btn-submit"> Process </button>
                        </form>
                    </div><!---- end div tableadd---->   
                </div><!---- end div tabwrap---->                       
            </div><!---- end div col-md-4----> 
            @endif
        </div><!-- end div row-->

    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop

@section('modal')

@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script src="{{Asset('js/chosen.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.2/raphael-min.js"></script>
<script src="{{Asset('morris.js')}}" type="text/javascript"></script>
<!--<script src="{{Asset('js/chosenNewJournal.jquery.js')}}" type="text/javascript"></script>-->
<script>
var config = {'.chosen-select': {}};
for (var selector in config) {
    $(selector).chosen({
        search_contains: true
    });
}
$('.appd').find('a.chosen-single').each(function () {
    $(this).addClass('chosenapp');
    var added = $(this).after().addClass('chosenapp');
    added++;
    var end = $('td.appd:last').children().find('select').addClass('chosenapp');
    end++
});

$('#startDateReport1').datepicker();
$("#startDateReport1").datepicker("option", "dateFormat", 'dd-mm-yy');
$('#endDateReport1').datepicker();
$("#endDateReport1").datepicker("option", "dateFormat", 'dd-mm-yy');
$('#startDateReport2').datepicker();
$("#startDateReport2").datepicker("option", "dateFormat", 'dd-mm-yy');
$('#endDateReport2').datepicker();
$("#endDateReport2").datepicker("option", "dateFormat", 'dd-mm-yy');
$('#startDateReport3').datepicker();
$("#startDateReport3").datepicker("option", "dateFormat", 'dd-mm-yy');
$('#endDateReport3').datepicker();
$("#endDateReport3").datepicker("option", "dateFormat", 'dd-mm-yy');
$('#startDateReport4').datepicker();
$("#startDateReport4").datepicker("option", "dateFormat", 'dd-mm-yy');
$('#endDateReport4').datepicker();
$("#endDateReport4").datepicker("option", "dateFormat", 'dd-mm-yy');
$('#startDateReport5').datepicker();
$("#startDateReport5").datepicker("option", "dateFormat", 'dd-mm-yy');
$('#endDateReport5').datepicker();
$("#endDateReport5").datepicker("option", "dateFormat", 'dd-mm-yy');
$('#endDateReport1, #startDateReport1').change(function () {
    if ($('#startDateReport1').val() == '') {
        $('#startDateReport1').val($('#endDateReport1').val());
    } else if ($('#endDateReport1').val() == '') {
        $('#endDateReport1').val($('#startDateReport1').val());
    } else if (dateCheckHigher($('#startDateReport1').val(), $('#endDateReport1').val()) == 'start') {
        $('#endDateReport1').val($('#startDateReport1').val());
    }
});
$('#endDateReport2, #startDateReport2').change(function () {
    if ($('#startDateReport2').val() == '') {
        $('#startDateReport2').val($('#endDateReport2').val());
    } else if ($('#endDateReport2').val() == '') {
        $('#endDateReport2').val($('#startDateReport2').val());
    } else if (dateCheckHigher($('#startDateReport2').val(), $('#endDateReport2').val()) == 'start') {
        $('#endDateReport2').val($('#startDateReport2').val());
    }
});
$('#endDateReport3, #startDateReport3').change(function () {
    if ($('#startDateReport3').val() == '') {
        $('#startDateReport3').val($('#endDateReport3').val());
    } else if ($('#endDateReport3').val() == '') {
        $('#endDateReport3').val($('#startDateReport3').val());
    } else if (dateCheckHigher($('#startDateReport3').val(), $('#endDateReport3').val()) == 'start') {
        $('#endDateReport3').val($('#startDateReport3').val());
    }
});
$('#endDateReport4, #startDateReport4').change(function () {
    if ($('#startDateReport4').val() == '') {
        $('#startDateReport4').val($('#endDateReport4').val());
    } else if ($('#endDateReport4').val() == '') {
        $('#endDateReport4').val($('#startDateReport4').val());
    } else if (dateCheckHigher($('#startDateReport4').val(), $('#endDateReport4').val()) == 'start') {
        $('#endDateReport4').val($('#startDateReport4').val());
    }
});
$('#endDateReport5, #startDateReport5').change(function () {
    if ($('#startDateReport5').val() == '') {
        $('#startDateReport5').val($('#endDateReport5').val());
    } else if ($('#endDateReport5').val() == '') {
        $('#endDateReport5').val($('#startDateReport5').val());
    } else if (dateCheckHigher($('#startDateReport5').val(), $('#endDateReport5').val()) == 'start') {
        $('#endDateReport5').val($('#startDateReport5').val());
    }
});

//$('#account').multiselect();
</script>
@stop