@extends('template.header-footer')

@section('title')
Transfer
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<style>
    .help-block {
        margin-left: 30% !important;
    }
</style>
@stop

@section('nav')

@stop

@section('content')
@if(myCheckIsEmpty('Inventory;Warehouse'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one master Inventory and Warehouse to insert Transfer.
</div>
@endif
@if(isset($messages))
@if($messages == 'gagalInsert')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New transfer has been inserted.
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest"> 
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showTransfer')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Transfer</a>
                <a href="{{route('transferNew')}}" type="button" class="btn btn-sm btn-pure">New Transfer</a>
            </div>
            <div class="btn-group margr5">
                <a href="{{Route('transferNew')}}">
                    <button type="button" <?php if (myCheckIsEmpty('Inventory;Warehouse')) echo 'disabled'; ?>  class="btn btn-green btn-sm dropdown-toggle  " aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
                </a>
            </div>
            <div class="btn-group margr5">
                <button type="button" <?php if (myCheckIsEmpty('SalesOrder;Default')) echo 'disabled'; ?> class="btn btn-green btn-sm dropdown-toggle" data-target="#insertSalesOrder" data-toggle="modal" role="dialog"
                        aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New from Sales Order </button>
            </div>
        </div>
        <form method="POST" action="">
            <div class="tabwrap">
                <div class="tabhead">
                    <h4 class="headtitle">Transfer <span id="transferID">{{TransferHeader::getNextIDTransfer($transfer)}}</span></h4>
                </div>
                <div class="tableadd"> 
                    <div class="headinv new">
                        <ul class="pull-left">
                            <li>
                                <label for="date">Date *</label>
                                <input id="date" name="date" type="text" autocomplete="off" data-validation="required">
                            </li>
                            <li>
                                <label for="warehouse">Source *</label>
                                <select class="chosen-select choosen-modal warehouse" id="warehouseHeaderSource" name="warehouseSource">
                                    @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->where("Type",Auth::user()->WarehouseCheck)->get() as $war)
                                    @if(isset($salesorder) && ($salesorder->WarehouseInternalID == $war->InternalID))
                                    <option selected id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                        @else
                                    <option id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                        @endif
                                        {{$war->WarehouseName;}}
                                    </option>
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label for="warehouse">Destiny *</label>
                                <select class="chosen-select choosen-modal warehouse" id="warehouseHeaderDestiny" name="warehouseDestiny">
                                    @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->where("Type",0)->get() as $war)
                                    <option id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                        {{$war->WarehouseName;}}
                                    </option>
                                    @endforeach
                                </select>
                            </li>
                        </ul>
                        <ul class="pull-right">
                            <li>
                                <label for="warehouse">Type *</label>
                                <select name="type">
                                    <option value="0">Pindah Stok</option>
                                    <option value="1">Pergudangan</option>
                                </select>
                            </li>
                            <li>
                                <label for="remark">Remark *</label>
                                <textarea name="remark" id="remark" data-validation="required"></textarea>
                            </li>
                            <li>
                                <div class="required">
                                    * Required
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="tableadd journupdate">
                        <table class="table master-data" id="table-transfer">
                            <thead>
                                <tr>
                                    <th width="15%">Inventory</th>
                                    <th width="15%">Uom</th>
                                    <th width="15%">Stock Source</th>
                                    <th width="15%">Stock Destiny</th>
                                    <th width="10%">Quantity Transfer</th>
                                    <th width="5%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="row0" style="background-color: #e5e5e5 !important">
                                    <td class="chosen-uom" style="border-color: #d8d8d8 !important">
                                        <input class="input-theme margbot10" type="text" id="searchInventory" tabindex="-1" title="Type Inventory Name or ID then 'Enter'" placeholder="Type Inventory Name or ID then 'Enter'">
                                        <div id="selectInventory">

                                        </div>
                                    </td>
                                    <td style="border-color: #d8d8d8 !important">
                                        <select id="uom-0" class="input-theme uom">

                                        </select>
                                    </td>
                                    <td style="border-color: #d8d8d8 !important" id="price-0-stocksource" class="currentStockSource">
                                        -
                                    </td>
                                    <td style="border-color: #d8d8d8 !important" id="price-0-stockdestiny" class="currentStockDestiny">
                                        -
                                    </td>
                                    <td class="text-right" style="border-color: #d8d8d8 !important">
                                        <input type="hidden" name="stockcurrent" id="stockCurrent-0">
                                        <input type="number" class="maxWidth qty right" maxlength="11" min="1" id="price-0-qty" value="1">
                                    </td>
                                    <td style="border-color: #d8d8d8 !important">
                                        <button type="button" class="btn btn-green btn-sm" disabled="" id="btn-addRow"><span class="glyphicon glyphicon-plus"></span></button>
                                    </td>
                                </tr>
                                <?php
                                $barisDetail = 1;
                                ?>
                                @if(isset($salesorder))
                            <input type="hidden" name="idSalesOrder" value="{{$salesorder->InternalID}}">
                            @foreach(SalesOrderDetail::where('SalesOrderInternalID',$salesorder->InternalID)->get() as $detail)
                            <tr id="row{{$barisDetail}}">
                                <td class="chosen-uom" >
                                    <input type="hidden" class="inventory" style="width: 100px" id="inventory-{{$barisDetail}}" name="inventory[]" value="{{$detail->InventoryInternalID}}">
                                    {{Inventory::find($detail->InventoryInternalID)->InventoryID." ".Inventory::find($detail->InventoryInternalID)->InventoryName}}
                                </td>
                                <td>
                                    <select id="uom-{{$barisDetail}}" name="uom[]" class="input-theme uom">
                                        @foreach (InventoryUom::where("InventoryInternalID", $detail->InventoryInternalID)->get() as $uom)
                                        <option value="{{$uom->UomInternalID}}" {{ ($uom->UomInternalID == $detail->UomInternalID) ? "selected" : ""}}>{{ $uom->Uom->UomID; }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td id="price-{{$barisDetail}}-stocksource" class="currentStockSource">
                                    {{''; $s = getEndStockInventoryWithWarehouse($detail->InventoryInternalID,Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->first()->InternalID)}}
                                    {{$s}}
                                    <input type="hidden" id="stocks-{{$barisDetail}}" name="curstocksource[]" value="{{explode(" ",$s)[0]}}">
                                </td>
                                <td id="price-{{$barisDetail}}-stockdestiny" class="currentStockDestiny">
                                    {{''; $d = getEndStockInventoryWithWarehouse($detail->InventoryInternalID,Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->first()->InternalID)}}
                                    {{$d}}
                                    <input type="hidden" id="stockd-{{$barisDetail}}" name="curstockdestiny[]" value="{{explode(" ",$d)[0]}}">
                                </td>
                                <td class="text-right">
                                    <input type="number" class="maxWidth qty right" name="qty[]" maxlength="11" min="1" id="price-{{$barisDetail}}-qty" value="{{$detail->Qty}}">
                                </td>
                                <td>
                                    <button class="btn btn-pure-xs btn-xs btn-deleteRow" type="button" data="row{{$barisDetail}}"><span class="glyphicon glyphicon-trash"></span></button>
                                </td>
                            </tr>
                            {{'';$barisDetail++;}}
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div><!---- end div tableadd---->      
                </div><!---- end div tableadd---->   
            </div><!---- end div tabwrap---->  
            <div class="btnnest pull-right">
                <button class="btn btn-green btn-sm btn-save" <?php if (myCheckIsEmpty('Inventory;Warehouse')) echo 'disabled'; ?>  id="btn-save"> Save </button>
            </div>
        </form>
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop
@section('modal')
<div class="modal fade" id="insertSalesOrder" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insert Transfer </h4>
            </div>
            <form action="{{Route("transferNew")}}" method="GET" class="action">
                {{'';$hitung = 0;}}
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="insertTransfer" id="jenisShipping" name="jenis">
                            <li>
                                <label for="sales">Sales Order ID</label> *
                            </li>
                            <input class="input-theme margbot10" type="text" id="searchSalesOrder" title="Type Sales Order Name or ID then 'Enter'" placeholder="Type Sales Order Name or ID then 'Enter'">
                            <li id="selectSalesOrder">

                            </li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btn-add-so" @if($hitung==0){{'disabled'}}@endif class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
@stop
@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNewJournal.jquery.js')}}" type="text/javascript"></script>
<script>
$("form").bind("submit", function (event) {
    setTimeout(function () {
        if ($(".form-error")[0]) {
            $("#btn-save").prop("disabled", false);
        } else {
          $("#btn-save").prop("disabled", true);
        }
    }, 100);

});
var getUomThisInventory = "<?php echo Route("getUomThisInventoryTransfer") ?>";
var getSearchResultInventoryTransfer = "<?php echo Route("getSearchResultInventoryTransfer") ?>";
var getStockInventorySource = "<?php echo Route("getStockInventorySO2") ?>";
var getStockInventoryDestiny = "<?php echo Route("getStockInventorySO2") ?>";
var textSelect = '';
var tanggalHariIni = '<?php echo date('d') . '-' . date('m') . '-' . date('Y') ?>';
var a = '<?php echo Route('formatCariIDTransfer') ?>';
var getResultSearchSOTransfer = "<?php echo Route("getResultSearchTransferSO") ?>";
</script>
@if(isset($salesorder))
<script>
    var baris = <?php echo $barisDetail; ?>;
</script>
@else
<script>
    var baris = 1;
</script>
@endif
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-transfer/transfer.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-transfer/transferNew.js')}}"></script>
@stop