@extends('template.header-footer')

@section('title')
Transfer
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<style>
    .help-block {
        margin-left: 30% !important;
    }
</style>
@stop

@section('nav')

@stop

@section('content')
@if(isset($messages))
@if($messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Transfer has been updated.
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest"> 
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showTransfer')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Transfer</a>
                <a href="{{route('transferUpdate',$header->TransferID)}}" type="button" class="btn btn-sm btn-pure">Update {{$header->TransferID}}</a>
            </div>
            <div class="btn-group margr5">
                <a href="{{Route('transferNew')}}">
                    <button type="button" class="btn btn-green btn-sm dropdown-toggle " aria-expanded="false"><span class="glyphicon glyphicon-plus"></span>New </button>
                </a>
            </div>
            <div class="btn-group margr5">
                <button type="button" <?php if (myCheckIsEmpty('SalesOrder;Default')) echo 'disabled'; ?> class="btn btn-green btn-sm dropdown-toggle" data-target="#insertSalesOrder" data-toggle="modal" role="dialog"
                        aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New from Sales Order </button>
            </div>
        </div>
        <form method="POST" action="">
            <input type='hidden' name='TransferInternalID' value='{{$header->InternalID}}'>
            <div class="tabwrap">
                <div class="tabhead">
                    <h4 class="headtitle">Transfer <span id="transferID">{{$header->TransferID}}</span></h4>
                </div>
                <div class="tableadd"> 
                    <div class="headinv new">
                        <ul class="pull-left">
                            <li>
                                <label for="warehouseSource">Source *</label>
                                <select class="chosen-select choosen-modal warehouse" id="warehouseHeaderSource" name="warehouseSource">
                                    @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->where("Type",Auth::user()->WarehouseCheck)->get() as $war)
                                    @if($war->InternalID == $header->WarehouseInternalID)
                                    <option selected="selected" id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                        {{$war->WarehouseName;}}
                                    </option>
                                    @else
                                    <option id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                        {{$war->WarehouseName;}}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label for="warehouseDestiny">Destiny *</label>
                                <select class="chosen-select choosen-modal warehouse" id="warehouseHeaderDestiny" name="warehouseDestiny">
                                    @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->where("Type",0)->get() as $war)
                                    @if($war->InternalID == $header->WarehouseDestinyInternalID)
                                    <option selected="selected" id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                        {{$war->WarehouseName;}}
                                    </option>
                                    @else
                                    <option id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                        {{$war->WarehouseName;}}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>
                            </li>
                        </ul>
                        <ul class="pull-right">
                            <li>
                                <label for="warehouse">Type *</label>
                                <select name="type">
                                    <option {{'';if($header->TransferType == 0) echo 'selected';}} value="0">Pindah Stok</option>
                                    <option {{'';if($header->TransferType == 1) echo 'selected';}} value="1">Pergudangan</option>
                                </select>
                            </li>
                            <li>
                                <label for="remark">Remark *</label>
                                <textarea name="remark" id="remark" data-validation="required">{{$header->Remark}}</textarea>
                            </li>
                            <li>
                                <div class="required">
                                    * Required
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="tableadd journupdate">
                        <table class="table master-data" id="table-transfer">
                            <thead>
                                <tr>
                                    <th width="15%">Inventory</th>
                                    <th width="10%">Uom</th>
                                    <th width="15%">Stock Source</th>
                                    <th width="15%">Stock Destiny</th>
                                    <th width="10%">Quantity Transfer</th>
                                    <th width="5%">Action</th>
                                </tr>
                            </thead>
                            <?php
                            $inventoryInternalID = Inventory::select('m_inventory.*')->distinct()->join('m_inventory_uom', 'm_inventory_uom.InventoryInternalID', '=', 'm_inventory.InternalID')
                                            ->where('m_inventory_uom.CompanyInternalID', Auth::user()->Company->InternalID)->first()->InternalID;
                            ?>
                            <input type="hidden" name="HidInternalIDFirst" id="hidInternalIDFirst" value="{{$inventoryInternalID}}">
                            <tbody>
                                <?php $barisTerakhir = 0; ?>
                                @if(count($detail) > 0)
                                <tr id="row0" style="background-color: #e5e5e5 !important">
                                    <td class="chosen-uom" style="border-color: #d8d8d8 !important">
                                        <input class="input-theme margbot10" type="text" id="searchInventory" tabindex="-1" title="Type Inventory Name or ID then 'Enter'" placeholder="Type Inventory Name or ID then 'Enter'">
                                        <div id="selectInventory">

                                        </div>
                                    </td>
                                    <td style="border-color: #d8d8d8 !important">
                                        <select id="uom-0" class="input-theme uom">

                                        </select>
                                    </td>
                                    <td style="border-color: #d8d8d8 !important" id="price-0-stocksource" class="currentStockSource">
                                        -
                                    </td>
                                    <td style="border-color: #d8d8d8 !important" id="price-0-stockdestiny" class="currentStockDestiny">
                                        -
                                    </td>
                                    <td class="text-right" style="border-color: #d8d8d8 !important">
                                        <input type="hidden" name="stockcurrent" id="stockCurrent-0">
                                        <input type="number" class="maxWidth qty right" maxlength="11" min="1" id="price-0-qty" value="1">
                                    </td>
                                    <td style="border-color: #d8d8d8 !important">
                                        <button type="button" class="btn btn-green btn-sm" disabled="" id="btn-addRow"><span class="glyphicon glyphicon-plus"></span></button>
                                    </td>
                                </tr>
                                {{'';$barisTerakhir++;}}
                                @foreach($detail as $data)
                                <tr id="row{{$barisTerakhir}}">
                                    <td class="chosen-uom">
                                        <?php $inventory = Inventory::find($data->InventoryInternalID); ?>
                                        <input type="hidden" class="inventory" id="inventory-{{$barisTerakhir}}" style="" name="inventory[]" value="{{$inventory->InternalID}}">
                                        {{$inventory->InventoryID}} 
                                        {{" ".$inventory->InventoryName}}
                                    </td>
                                    <td>
                                        <select id="uom-{{$barisTerakhir}}" name="uom[]" class="input-theme uom">
                                            @foreach (InventoryUom::where("InventoryInternalID", $data->InventoryInternalID)->get() as $uom)
                                            <option value="{{$uom->UomInternalID}}" {{ ($uom->UomInternalID == $data->UomInternalID) ? "selected" : ""}}>{{ $uom->Uom->UomID; }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td id="price-{{$barisTerakhir}}-stocksource" class="currentStockSource">
                                        {{''; $s = getEndStockInventoryWithWarehouseWithoutTransfer($data->InventoryInternalID,$header->WarehouseSourceInternalID,$header->InternalID)}}
                                        {{$s}}
                                        <input type="hidden" id="stocks-{{$barisTerakhir}}" name="curstocksource[]" value="{{explode(" ",$s)[0]}}">
                                    </td>
                                    <td id="price-{{$barisTerakhir}}-stockdestiny" class="currentStockDestiny">
                                        {{''; $d = getEndStockInventoryWithWarehouseWithoutTransfer($data->InventoryInternalID,$header->WarehouseDestinyInternalID,$header->InternalID)}}
                                        {{$d}}
                                        <input type="hidden" id="stockd-{{$barisTerakhir}}" name="curstockdestiny[]" value="{{explode(" ",$d)[0]}}">
                                    </td>
                                    <td class="text-right">
                                        <input type="number" class="maxWidth qty right" name="qty[]" maxlength="11" min="1" id="price-{{$barisTerakhir}}-qty" value="{{$data->Qty}}">
                                    </td>
                                    <td>
                                        @if($barisTerakhir == 0)
                                        -
                                        @else
                                        <button class="btn btn-pure-xs btn-xs btn-deleteRow" type="button" data="row{{$barisTerakhir}}"><span class="glyphicon glyphicon-trash"></span></button>
                                        @endif
                                    </td>
                                </tr>
                                {{'';$barisTerakhir++;}}
                                @endforeach
                                @else
                                <tr id="row0" style="background-color: #e5e5e5 !important">
                                    <td class="chosen-uom" style="border-color: #d8d8d8 !important">
                                        <input class="input-theme margbot10" type="text" id="searchInventory" title="Type Inventory Name or ID then 'Enter'" placeholder="Type Inventory Name or ID then 'Enter'">
                                        <div id="selectInventory">

                                        </div>
                                    </td>
                                    <td style="border-color: #d8d8d8 !important">
                                        <select id="uom-0" class="input-theme uom">

                                        </select>
                                    </td>
                                    <td style="border-color: #d8d8d8 !important" id="price-0-stocksource" class="currentStockSource">
                                        -
                                    </td>
                                    <td style="border-color: #d8d8d8 !important" id="price-0-stockdestiny" class="currentStockDestiny">
                                        -
                                    </td>
                                    <td class="text-right" style="border-color: #d8d8d8 !important">
                                        <input type="number" class="maxWidth qty right" maxlength="11" min="1" id="price-0-qty" value="1">
                                    </td>
                                    <td style="border-color: #d8d8d8 !important">
                                        <button type="button" class="btn btn-green btn-sm" disabled="" id="btn-addRow"><span class="glyphicon glyphicon-plus"></span></button>
                                    </td>
                                </tr>
                                {{'';$barisTerakhir++;}}
                                @endif
                            </tbody>
                        </table>
                    </div><!---- end div tableadd---->      
                </div><!---- end div tableadd---->   
            </div><!---- end div tabwrap---->  
            <div class="btnnest pull-right">
                <button class="btn btn-green btn-sm btn-save" id="btn-save"> Save </button>
            </div>
        </form>
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop
@section('modal')
<div class="modal fade" id="insertSalesOrder" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insert Transfer </h4>
            </div>
            <form action="{{Route("transferNew")}}" method="GET" class="action">
                {{'';$hitung = 0;}}
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="insertTransfer" id="jenisShipping" name="jenis">
                            <li>
                                <label for="sales">Sales Order ID</label> *
                            </li>
                            <input class="input-theme margbot10" type="text" id="searchSalesOrder" title="Type Sales Order Name or ID then 'Enter'" placeholder="Type Sales Order Name or ID then 'Enter'">
                            <li id="selectSalesOrder">

                            </li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btn-add-so" @if($hitung==0){{'disabled'}}@endif class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
@stop
@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNewJournal.jquery.js')}}" type="text/javascript"></script>
<script>
var getUomThisInventory = "<?php echo Route("getUomThisInventory") ?>";
var getSearchResultInventoryTransfer = "<?php echo Route("getSearchResultInventoryTransfer") ?>";
var getStockInventorySource = "<?php echo Route("getStockInventorySO2") ?>";
var getStockInventoryDestiny = "<?php echo Route("getStockInventorySO2") ?>";
var textSelect = '';
var baris = '<?php echo $barisTerakhir; ?>';
var getResultSearchSOTransfer = "<?php echo Route("getResultSearchTransferSO") ?>";
</script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-transfer/transfer.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-transfer/transferUpdate.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script>
$.validate();
</script>
@stop