@extends('template.header-footer')

@section('title')
Transformation
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosenCustom.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
@stop

@section('nav')

@stop

@section('content')
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest"> 
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showTransformation')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Transformation</a>
                <a href="{{route('transformationDetail',$headermemoin->TransformationID)}}" type="button" class="btn btn-sm btn-pure">Detail {{$headermemoin->TransformationID}}</a>
            </div>
            <div class="btn-group margr5">
                <a href="{{Route('transformationNew')}}">
                    <button type="button" class="btn btn-green btn-sm dropdown-toggle" aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
                </a>
            </div>
            <div class="btn-group margr5">
                <button type="button" <?php if (myCheckIsEmpty('SalesOrder;Default')) echo 'disabled'; ?> class="btn btn-green btn-sm dropdown-toggle" data-target="#insertSalesOrder" data-toggle="modal" role="dialog"
                        aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New from Sales Order </button>
            </div>
            <a href="{{Route('transformationUpdate',$headermemoin->TransformationID)}}">
                <button id="btn-{{$headermemoin->TransformationID}}-update"
                        class="btn btn-green btn-sm ">
                    <span class="glyphicon glyphicon-edit"></span> Edit
                </button>
            </a>
            @if(checkModul('O04'))
            <a href="{{Route('transformationPrint',$headermemoin->TransformationID)}}" target='_blank' style="margin-right: 0px !important;">
                <button id="btn-{{$headermemoin->TransformationID}}-print"
                        class="btn btn-green btn-sm ">
                    <span class="glyphicon glyphicon-print"></span> Print
                </button>
            </a>
            <!--            <button type="button" class="btn btn-green margr5" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rSummary">
                            <span class="glyphicon glyphicon-file"></span> Summary Report</button>-->
            <button type="button" class="btn btn-green" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rDetail">
                <span class="glyphicon glyphicon-file"></span> Detail Report</button>
            @endif
        </div>
        <div class="tabwrap">
            <div class="tabhead">
                <h4 class="headtitle">{{'Transformation '.$headermemoin->TransformationID}}</h4>
            </div>
            <div class="tableadd"> 
                <div class="headinv new">
                    <ul class="pull-left">
                        <li>
                            <label for="date">Date</label>
                            <span>{{date( "d-m-Y", strtotime($headermemoin->TransformationDate))}}</span>
                        </li>
                        <li>
                            <label for="currency">Currency</label>
                            <span>{{'';$currency = Currency::find($headermemoin->CurrencyInternalID); $currencyName = $currency->CurrencyName; echo $currencyName}}</span>
                        </li>
                        <li>
                            <label for="rate">Rate</label>
                            <span>{{number_format($headermemoin->CurrencyRate,'2','.',',')}}</span>
                        </li>
                    </ul>
                    <ul class="pull-right">
                        <li>
                            <label for="rate">Remark</label>
                            <span>{{$headermemoin->Remark}}</span>
                        </li>
                    </ul>
                </div>
                <div class="padrl10">
                    <h5>OUT (Barang Jadi)</h5>
                    <table class="table master-data " id="table-transformation" >
                        <thead>
                            <tr>
                                <th>Inventory</th>
                                <th>Uom</th>
                                <th>Warehouse</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Type</th>
                                <th>Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $totalIn = 0;
                            $totalOut = 0;
                            ?>
                            @foreach($detailmemoin as $data)
                            <tr>
                                <td class="left">{{'';$inventory = Inventory::find($data->InventoryInternalID); echo $inventory->InventoryID.' '.$inventory->InventoryName}}</td>
                                <td class="right">{{$data->Uom->UomID}}</td>
                                <td class="left">{{'';$warehouse = Warehouse::find($data->WarehouseInternalID); echo $warehouse->WarehouseName}}</td>
                                <td class="right">{{number_format($data->Qty,'0','.',',')}}</td>
                                <td class="right">{{number_format($data->Price,'2','.',',')}}</td>
                                <td class="center">OUT</td>
                                <td class="right">{{number_format($data->SubTotal,'2','.',',');$totalIn += $data->SubTotal}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <h5>IN (Barang Baku)</h5>
                    <table class="table master-data " id="table-transformation" >
                        <thead>
                            <tr>
                                <th>Inventory</th>
                                <th>Uom</th>
                                <th>Warehouse</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Type</th>
                                <th>Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($detailmemoout as $data)
                            <tr>
                                <td class="left">{{'';$inventory = Inventory::find($data->InventoryInternalID); echo $inventory->InventoryID.' '.$inventory->InventoryName}}</td>
                                <td class="right">{{$data->Uom->UomID}}</td>
                                <td class="left">{{'';$warehouse = Warehouse::find($data->WarehouseInternalID); echo $warehouse->WarehouseName}}</td>
                                <td class="right">{{number_format($data->Qty,'0','.',',')}}</td>
                                <td class="right">{{number_format($data->Price,'2','.',',')}}</td>
                                <td class="center">IN</td>
                                <td class="right">{{number_format($data->SubTotal,'2','.',',');$totalOut += $data->SubTotal}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @if(count($detailmemoin) > 0 && count($detailmemoout) > 0)
                    <table class="pull-right"> 
                        <tr>
                            <td><h5 class="right margr10 h5total"><b>Grand Total In</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="grandTotal">{{number_format($totalOut,'2','.',',')}}</b></h5></td>
                        </tr>
                        <tr>
                            <td><h5 class="right margr10 h5total"><b>Grand Total Out</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="grandTotal">{{number_format($totalIn,'2','.',',')}}</b></h5></td>
                        </tr>
                    </table>
                    @endif
                </div><!---- end div padrl10---->         
            </div><!---- end div tableadd---->   
        </div><!---- end div tabwrap---->                 
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop
@section('modal')
<div class="modal fade" id="insertSalesOrder" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insert Transformation </h4>
            </div>
            <form action="{{Route("transformationNew")}}" method="GET" class="action">
                {{'';$hitung = 0;}}
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="insertTransformation" id="jenisShipping" name="jenis">
                            <li>
                                <label for="sales">Sales Order ID</label> *
                            </li>
                            <input class="input-theme margbot10" type="text" id="searchSalesOrder" title="Type Sales Order Name or ID then 'Enter'" placeholder="Type Sales Order Name or ID then 'Enter'">
                            <li id="selectSalesOrder">

                            </li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btn-add-so" @if($hitung==0){{'disabled'}}@endif class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="r_summary" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Detail Transformation</h4>
            </div>
            <form action="" method="post" class="action" id="" >
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='detailReport'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateReport" data-validation="required">
                            </li>
                            <!--                            <li>
                                                            <label for="eDate">Supplier</label> *
                                                        </li>
                                                        <li>
                                                            <select class='chosen-select' name='supplier'>
                                                                <option value='-1'>All</option>
                                                                @foreach(Coa6::where('CompanyInternalID',Auth::user()->CompanyInternalID)->where('Type','s')->get() as $c)
                                                                <option value='{{$c->InternalID}}'>{{$c->ACC6Name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </li>-->
                        </ul>
                    </div>
                    <div class="required">
                        * Required
                    </div>
                </div>
                <div class="modal-footer">
                    <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                    <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>
@stop
@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script><script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-transformation/transformation.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script>
var transformationDataBackup = '<?php echo Route('transformationDataBackup') ?>';
var getResultSearchSOTransfer = "<?php echo Route("getResultSearchTransferSO") ?>";
</script>
@stop