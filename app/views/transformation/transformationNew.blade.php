@extends('template.header-footer')

@section('title')
Transformation
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<style>
    .help-block {
        margin-left: 30% !important;
    }
</style>
@stop

@section('nav')

@stop

@section('content')
@if(myCheckIsEmpty('Inventory;Warehouse;Currency'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one master Inventory, Currency and Warehouse to insert Transformation.
</div>
@endif
@if(isset($messages))
@if($messages == 'gagalInsert')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New transformation has been inserted.
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest"> 
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showTransformation')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Transformation</a>
                <a href="{{route('transformationNew')}}" type="button" class="btn btn-sm btn-pure">New Transformation</a>
            </div>
            <div class="btn-group margr5">
                <a href="{{Route('transformationNew')}}">
                    <button <?php if (myCheckIsEmpty('Inventory;Warehouse;Currency')) echo 'disabled'; ?> type="button" class="btn btn-green btn-sm dropdown-toggle  " aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
                </a>
            </div>
            <div class="btn-group margr5">
                <button type="button" <?php if (myCheckIsEmpty('SalesOrder;Default')) echo 'disabled'; ?> class="btn btn-green btn-sm dropdown-toggle" data-target="#insertSalesOrder" data-toggle="modal" role="dialog"
                        aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New from Sales Order </button>
            </div>
             @if(checkModul('O04'))
            <!--            <button type="button" class="btn btn-green margr5" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rSummary">
                            <span class="glyphicon glyphicon-file"></span> Summary Report</button>-->
            <button type="button" class="btn btn-green" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rDetail">
                <span class="glyphicon glyphicon-file"></span> Detail Report</button>
            @endif
        </div>
        <form method="POST" action="" id="form-insert">
            <div class="tabwrap">
                <div class="tabhead">
                    <h4 class="headtitle">Transformation <span id="transformationID">{{TransformationHeader::getNextIDTransformation($transformation)}}</span></h4>
                </div>
                @if(isset($salesorder) && $salesorder != null && $salesorder != '')
                <input type="hidden" name="SalesOrderInternalID" value="{{$salesorder->InternalID}}">
                @endif
                <div class="tableadd"> 
                    <div class="headinv new">
                        <ul class="pull-left">
                            <li>
                                <label for="date">Date *</label>
                                <input id="date" name="date" type="text" autocomplete="off" data-validation="required">
                            </li>
                            <li>
                                <label for="currency">Currency *</label>
                                <select class="chosen-select choosen-modal currency" id="currencyHeader" name="currency">
                                    {{'';$default = 0;}}
                                    @foreach(Currency::orderBy('Default','desc')->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $cur)
                                    <option id="cur{{$cur->InternalID}}" value="{{$cur->InternalID.'---;---'.$cur->CurrencyName.'---;---'.$cur->Rate.'---;---'.$cur->Default}}">
                                        {{$cur->CurrencyName;}}
                                        @if($default == 0)
                                        {{'';$default=$cur->Rate}}
                                        @endif
                                    </option>
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label for="rate">Rate *</label>
                                <input type="text" class="maxWidth rate numajaDesimal" name="rate" maxlength="" id="rate" value="{{$default}}" data-validation="required">
                            </li>
                        </ul>
                        <ul class="pull-right">
                            <!--                            <li>
                                                            <label for="warehouse">Warehouse *</label>
                                                            <select class="chosen-select choosen-modal warehouse" id="warehouseHeader" name="warehouse">
                                                                @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $war)
                                                                <option id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                                                    {{$war->WarehouseName;}}
                                                                </option>
                                                                @endforeach
                                                            </select>
                                                        </li>-->
                            <li>
                                <label for="remark">Remark</label>
                                <textarea name="remark" id="remark"></textarea>
                            </li>
                            <li>
                                <div class="required">
                                    * Required
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="tableadd journupdate">
                        @if(isset($salesorder))
                        <h5>SO Detail</h5>
                        <table class="table master-data">
                            <thead>
                                <tr>
                                    <th width="15%">Inventory</th>
                                    <th width="10%">Uom</th>
                                    <th width="10%">Stock</th>
                                    <th width="10%">Quantity</th>
                                    <th width="10%">Price</th>
                                    <th width="10%">Subtotal</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(SalesOrderDetail::where('SalesOrderInternalID',$salesorder->InternalID)->get() as $detail)
                                <tr>
                                    <td class="chosen-uom" >
                                        {{Inventory::find($detail->InventoryInternalID)->InventoryID." ".Inventory::find($detail->InventoryInternalID)->InventoryName}}
                                    </td>
                                    <td>
                                        {{Uom::find($detail->UomInternalID)->UomID}}
                                    </td>
                                    <td>
                                        {{getEndStockInventory($detail->InventoryInternalID)}}
                                    </td>
                                    <td class="text-right">
                                        {{$detail->Qty}}
                                    </td>
                                    <td class="text-right">
                                        {{number_format($detail->Price,'2','.',',')}}
                                    </td>
                                    <td>{{number_format($detail->Price * $detail->Qty,'2','.',',')}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @endif
                        <h5>OUT (Barang Jadi)</h5>
                        <table class="table master-data" id="table-transformation">
                            <thead>
                                <tr>
                                    <th width="15%">Inventory</th>
                                    <th width="10%">Uom</th>
                                    <th width="10%">Warehouse</th>
                                    <th width="5%">Stock</th>
                                    <th width="10%">Quantity</th>
                                    <th width="10%">Price</th>
                                    <th width="10%">Subtotal</th>
                                    <th width="7%">Type</th>
                                    <th width="5%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="row0" style="background-color: #e5e5e5 !important">
                                    <td class="" style="border-color: #d8d8d8 !important">
                                        <input class="input-theme margbot10" type="text" id="searchInventory" tabindex="-1" title="Type Inventory Name or ID then 'Enter'" placeholder="Type Inventory Name or ID then 'Enter'">
                                        <div id="selectInventory">

                                        </div>
                                    </td>
                                    <td style="border-color: #d8d8d8 !important">
                                        <select id="uom-0" class="input-theme uom">
                                        </select>
                                    </td>
                                    <td class="chosen-transaction" style="border-color: #d8d8d8 !important">
                                        <select id='warehouse-0' class="chosen-select warehouse">
                                            @foreach(Warehouse::all() as $w)
                                            <option value='{{$w->InternalID}}'>{{$w->WarehouseName}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td id="price-0-stock" class="" style="border-color: #d8d8d8 !important">
                                        -
                                    </td>
                                    <td class="text-right" style="border-color: #d8d8d8 !important">
                                        <input type="text" class="maxWidth qty right numaja" maxlength="11" min="1" id="price-0-qty" value="1">
                                    </td>
                                    <td class="text-right" style="border-color: #d8d8d8 !important">
                                        <input type="text" class="maxWidth price right numajaDesimal" maxlength="" id="price-0" value="0.00">
                                    </td>
                                    <td id="price-0-qty-hitung" class="right" style="border-color: #d8d8d8 !important">
                                        0.00
                                    </td>
                                    <td style="border-color: #d8d8d8 !important">
                                        <select class="input-theme left" id="type-0">
                                            <!--<option value="OUT" selected="">OUT</option>-->
                                            <option value="IN" >OUT</option>
                                        </select>
                                    </td>
                                    <td style="border-color: #d8d8d8 !important">
                                        <button type="button" class="btn btn-green btn-sm" disabled="" id="btn-addRow"><span class="glyphicon glyphicon-plus"></span></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <h5>IN (Barang Baku)</h5>
                        <table class="table master-data" id="table-transformation2">
                            <thead>
                                <tr>
                                    <th width="15%">Inventory</th>
                                    <th width="10%">Uom</th>
                                    <th width="10%">Warehouse</th>
                                    <th width="5%">Stock</th>
                                    <th width="10%">Quantity</th>
                                    <th width="10%">Price</th>
                                    <th width="10%">Subtotal</th>
                                    <th width="7%">Type</th>
                                    <th width="5%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="row2-0" style="background-color: #e5e5e5 !important">
                                    <td class="" style="border-color: #d8d8d8 !important">
                                        <input class="input-theme margbot10" type="text" id="searchInventory2" tabindex="-1" title="Type Inventory Name or ID then 'Enter'" placeholder="Type Inventory Name or ID then 'Enter'">
                                        <div id="selectInventory2">

                                        </div>
                                    </td>
                                    <td style="border-color: #d8d8d8 !important">
                                        <select id="uom2-0" class="input-theme uom2">
                                        </select>
                                    </td>
                                    <td class="chosen-transaction" style="border-color: #d8d8d8 !important">
                                        <select id='warehouse2-0' class="chosen-select warehouse2">
                                            @foreach(Warehouse::all() as $w)
                                            <option value='{{$w->InternalID}}'>{{$w->WarehouseName}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td id="price2-0-stock" class="" style="border-color: #d8d8d8 !important">
                                        -
                                    </td>
                                    <td class="text-right" style="border-color: #d8d8d8 !important">
                                        <input type="hidden" name="stockcurrent" id="stockCurrent-0">
                                        <input type="text" class="maxWidth qty2 right numaja" maxlength="11" min="1" id="price2-0-qty" value="1">
                                    </td>
                                    <td class="text-right" style="border-color: #d8d8d8 !important">
                                        <input type="text" class="maxWidth price2 right numajaDesimal" maxlength="" id="price2-0" value="0.00">
                                    </td>
                                    <td id="price2-0-qty-hitung" class="right" style="border-color: #d8d8d8 !important">
                                        0.00
                                    </td>
                                    <td style="border-color: #d8d8d8 !important">
                                        <select class="input-theme left" id="type2-0">
                                            <option value="OUT" selected="">IN</option>
                                            <!--<option value="IN" >IN</option>-->
                                        </select>
                                    </td>
                                    <td style="border-color: #d8d8d8 !important">
                                        <button type="button" class="btn btn-green btn-sm" disabled="" id="btn-addRow2"><span class="glyphicon glyphicon-plus"></span></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <input type="hidden" name="grandTotalValueIn" value="0" id="grandTotalValueIn">
                        <input type="hidden" name="grandTotalValueOut" value="0" id="grandTotalValueOut">
                        <table class="pull-right"> 
                            <tr>
                                <td><h5 class="right margr10 h5total"><b>Grand Total In</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b id="grandTotalOut"></b></h5></td>
                            </tr>
                            <tr>
                                <td><h5 class="right margr10 h5total"><b>Grand Total Out</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b id="grandTotalIn"></b></h5></td>
                            </tr>
                        </table>
<!--                        <table class="pull-left"> 
                            <tr>
                                <td><button class="btn btn-green btn-sm" type="button" id="btn-calculate"><span class="glyphicon glyphicon-book"></span><strong> Auto Calculate </strong></button></td>
                            </tr>
                        </table>-->
                    </div><!---- end div tableadd---->      
                </div><!---- end div tableadd---->   
            </div><!---- end div tabwrap---->  
            <div class="btnnest pull-right">
                <button class="btn btn-green btn-sm btn-save" type="button" id="btn-save" <?php if (myCheckIsEmpty('Inventory;Warehouse;Currency')) echo 'disabled'; ?>> Save </button>
            </div>
        </form>
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop
@section('modal')
<div class="modal fade" id="insertSalesOrder" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insert Transformation </h4>
            </div>
            <form action="{{Route("transformationNew")}}" method="GET" class="action">
                {{'';$hitung = 0;}}
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="insertTransformation" id="jenisShipping" name="jenis">
                            <li>
                                <label for="sales">Sales Order ID</label> *
                            </li>
                            <input class="input-theme margbot10" type="text" id="searchSalesOrder" title="Type Sales Order Name or ID then 'Enter'" placeholder="Type Sales Order Name or ID then 'Enter'">
                            <li id="selectSalesOrder">

                            </li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btn-add-so" @if($hitung==0){{'disabled'}}@endif class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="r_summary" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Detail Transformation</h4>
            </div>
            <form action="" method="post" class="action" id="" >
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='detailReport'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateReport" data-validation="required">
                            </li>
                            <!--                            <li>
                                                            <label for="eDate">Supplier</label> *
                                                        </li>
                                                        <li>
                                                            <select class='chosen-select' name='supplier'>
                                                                <option value='-1'>All</option>
                                                                @foreach(Coa6::where('CompanyInternalID',Auth::user()->CompanyInternalID)->where('Type','s')->get() as $c)
                                                                <option value='{{$c->InternalID}}'>{{$c->ACC6Name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </li>-->
                        </ul>
                    </div>
                    <div class="required">
                        * Required
                    </div>
                </div>
                <div class="modal-footer">
                    <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                    <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>
@stop
@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNewJournal.jquery.js')}}" type="text/javascript"></script>
<script>
$("form").bind("submit", function (event) {
    setTimeout(function () {
        if ($(".form-error")[0]) {
            $("#btn-save").prop("disabled", false);
        } else {
          $("#btn-save").prop("disabled", true);
        }
    }, 100);

});
var getStockInventorySO = "<?php echo Route("getStockInventorySO2") ?>";
var getHPPValueInventoryTransformation = "<?php echo Route('getHPPValueInventoryTransformation') ?>";
var getUomThisInventory = "<?php echo Route("getUomThisInventory") ?>";
var getSearchResultInventoryTransformation = "<?php echo Route("getSearchResultInventoryTransformation") ?>";
var getSearchResultInventoryTransformation2 = "<?php echo Route("getSearchResultInventoryTransformation2") ?>";
var textSelect = '';
var tanggalHariIni = '<?php echo date('d') . '-' . date('m') . '-' . date('Y') ?>';
var transformationDataBackup = '<?php echo Route('transformationDataBackup') ?>';
var getResultSearchSOTransfer = "<?php echo Route("getResultSearchTransferSO") ?>";
</script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-transformation/transformation.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-transformation/transformationNew.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script>
$.validate();
$("#date").change(function () {
    var cariText = '';
    $('#transformationID').load('<?php echo Route('formatCariIDTransformation') ?>', {
        "date": $("#date").val()
    });
});
</script>
@stop