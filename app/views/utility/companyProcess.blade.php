@extends('template.header-footer')

@section('title')
Monthly Process
@stop

@section('css')
<link rel="stylesheet" href="{{Asset('css/chosenCustom.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
@stop

@section('nav')

@stop

@section('content')
@if(myCheckIsEmpty('Company'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one Company.
</div>
@endif
@if(Session::get('messages') == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
{{''; $cekSelesai = true;}}
@if(isset($datadownload))
{{''; $cekSelesai = false;}}
@endif
@if(isset($messages))
@if($messages == 'suksesBackup')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Database has been backup.
</div>
@endif
@if($messages == 'suksesCutOff')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Cut off process from company {{$company}} has been success.
</div>
@endif
@if($messages == 'suksesDelete')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Database company {{$company}} has been deleted.
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest hidden-xs"> 
            <div class="btn-group bread" role="group">
                <a href="{{route('showUser')}}"  class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a  class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showCompanyProcess')}}"  class="btn btn-sm btn-pure">Company Process</a>
            </div>
        </div>
        
        <div class="bread-xs visible-xs hidden-sm hidden-md hidden-lg">
            <p class="text-center"><a href="{{route('showUser')}}">{{Config::get('companyHeader.header_company');}}</a> / <a>{{ucfirst($toogle)}}</a> / <a href="{{route('showCompanyProcess')}}">Company Process</a></p>
        </div>
        
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
<div class="wrapjour">
    <div class="primcontentnopadd">

        <div class="row">

            <div class="col-md-4">
                <div class="tabwrap">
                    <div class="tabhead">
                        <h4 class="headtitle">Backup Database</h4>
                    </div>
                    <div class="tableadd">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="padding-right: 0px !important"> Company ID </label>
                                <div class="col-sm-8 companyProcessID_chosen">
                                    <select class="chosen-select choosen-modal" id="backup" name="backup"> 
                                        @foreach(Company::where("InternalID", '<>', '-1')->where("InternalID", '<>', '-2')->get() as $comp)
                                        <option value="{{$comp->InternalID}}">
                                            {{$comp->CompanyID. ' | ' . $comp->CompanyName}}
                                        </option>
                                        @endforeach
                                    </select>  
                                </div>
                            </div>
                            <button type="button" <?php if (myCheckIsEmpty('Company')) echo 'disabled' ?> class="btn btn-green btn-sm btn-save pull-right" id="btn-backup" data-target="#companyProcess" data-toggle="modal" role="dialog"> Process </button>
                        </form>
                    </div><!---- end div tableadd---->   
                </div><!---- end div tabwrap---->                       
            </div><!---- end div col-md-4----> 

            <div class="col-md-4">
                <div class="tabwrap">
                    <div class="tabhead">
                        <h4 class="headtitle">Cut Off Transaction</h4>
                    </div>
                    <div class="tableadd ">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="padding-right: 0px !important"> Company ID </label>
                                <div class="col-sm-8 companyProcessID_chosen">
                                    <select class="chosen-select choosen-modal" id="cut_off" name="cut_off">
                                        @foreach(Company::where("InternalID", '<>', '-1')->where("InternalID", '<>', '-2')->get() as $comp)
                                        <option value="{{$comp->InternalID}}">
                                            {{$comp->CompanyID. ' | ' . $comp->CompanyName}}
                                        </option>
                                        @endforeach
                                    </select>  
                                </div>
                            </div>
                            <button type="button" <?php if (myCheckIsEmpty('Company')) echo 'disabled' ?> class="btn btn-green btn-sm btn-save pull-right" id="btn-cut-off" data-target="#companyProcess" data-toggle="modal" role="dialog"> Process </button>
                        </form>
                    </div><!---- end div tableadd---->   
                </div><!---- end div tabwrap---->                       
            </div><!---- end div col-md-4----> 

            <div class="col-md-4">
                <div class="tabwrap">
                    <div class="tabhead">
                        <h4 class="headtitle">Delete Database</h4>
                    </div>
                    <div class="tableadd ">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="padding-right: 0px !important"> Company ID </label>
                                <div class="col-sm-8 companyProcessID_chosen">
                                    <select class="chosen-select choosen-modal" id="delete" name="delete">
                                        @foreach(Company::where("InternalID", '<>', '-1')->where("InternalID", '<>', '-2')->get() as $comp)
                                        <option value="{{$comp->InternalID}}">
                                            {{$comp->CompanyID. ' | ' . $comp->CompanyName}}
                                        </option>
                                        @endforeach
                                    </select>  
                                </div>
                            </div>
                            <button type="button" <?php if (myCheckIsEmpty('Company')) echo 'disabled' ?> class="btn btn-green btn-sm btn-save pull-right" id="btn-delete" data-target="#companyProcess" data-toggle="modal" role="dialog"> Process </button>
                        </form>
                    </div><!---- end div tableadd---->   
                </div><!---- end div tabwrap---->                       
            </div><!---- end div col-md-4----> 

        </div><!-- end div row-->

    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop

@section('modal')
<div class="modal fade" id="companyProcess" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="header_modal_company_process"></h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form method="post" class="action" >
                        <ul>
                            <input type="hidden" value="" id="CompanyInternalID" name="InternalID">
                            <input type="hidden" value="" id="jenis" name="jenis">
                            <p id="info_text">Are you sure?</p>
                        </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-green">Yes</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
            </div>
            </form>      
        </div>
    </div>  
</div>
<form method="post" class="action" style="display: none;" id='formDownload'>
    @if(isset($datadownload))
    <input type="hidden" value="{{$datadownload}}" id="dataDownload" name="dataDownload">
    <input type="hidden" value="download" name="jenis">
    @endif
</form>      
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script>
var cekSelesai = "<?php echo $cekSelesai; ?>";
if (cekSelesai == false) {
    $("#formDownload").submit();
}
var config = {
    '.chosen-select': {}
};
for (var selector in config) {
    $(selector).chosen(config[selector]);
}

$("#btn-backup").click(function () {
    $("#header_modal_company_process").html("Backup Database");
    $("#jenis").val("backup");
    $("#CompanyInternalID").val($("#backup").val());
    $("#info_text").html("Are you sure to Backup Database ?");
});

$("#btn-cut-off").click(function () {
    $("#header_modal_company_process").html("Cut Off Transaction");
    $("#jenis").val("cut_off");
    $("#CompanyInternalID").val($("#cut_off").val());
    $("#info_text").html("Are you sure to Cut Off Transaction ?");
});

$("#btn-delete").click(function () {
    $("#header_modal_company_process").html("Delete Database");
    $("#jenis").val("delete");
    $("#CompanyInternalID").val($("#delete").val());
    $("#info_text").html("Are you sure to Delete Database ?");
});
</script>
@stop