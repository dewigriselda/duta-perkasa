@extends('template.header-footer')

@section('title')
Monthly Process
@stop

@section('css')
<link rel="stylesheet" href="{{Asset('css/chosenCustom.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
@stop

@section('nav')

@stop

@section('content')
@if(myCheckIsEmpty('Depreciation;SalesPurchase;Journal;Default;DefaultCurrency'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one depreciation, default currency, sales, purchase or journal and default COA to insert create monthly process.
</div>
@endif
@if(Session::get('messages') == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@if(isset($messages))
@if($messages == 'suksesCOGS')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Journal for Cost of Goods Sold have been inserted.
</div>
@endif
@if($messages == 'gagalCOGS')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> There is no Inventory with transaction value in chosen period.
</div>
@endif
@if($messages == 'suksesDepreciation')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Journal for Depreciation have been inserted.
</div>
@endif
@if($messages == 'gagalDepreciation')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> There is no Depreciation in chosen period.
</div>
@endif
@if($messages == 'suksesClosing')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Journal closing have been inserted.
</div>
@endif
@if($messages == 'gagalClosing')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> There is no journal in chosen period.
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest hidden-xs"> 
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a  class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showMonthlyProcess')}}"  class="btn btn-sm btn-pure">Monthly Process</a>
            </div>
        </div>

        <div class="bread-xs visible-xs hidden-sm hidden-md hidden-lg">
            <p class="text-center"><a href="{{route('showDashboard')}}">{{Config::get('companyHeader.header_company');}}</a> / <a>{{ucfirst($toogle)}}</a> / <a href="{{route('showMonthlyProcess')}}">Monthly Process</a></p>
        </div>

    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
<div class="wrapjour">
    <div class="primcontentnopadd">

        <div class="row">

            <div class="col-md-4">
                <div class="tabwrap">
                    <div class="tabhead">
                        <h4 class="headtitle">Depreciation</h4>
                    </div>
                    <div class="tableadd">
                        <?php if (!myCheckIsEmpty('Depreciation;Default;DefaultCurrency')) { ?> 
                            <form class="form-horizontal" method="POST" action="">
                                <input type="hidden" value="depreciation" name="jenis">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" style="padding-right: 0px !important"> Month </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="month" >
                                            <option value="1">January</option>
                                            <option value="2">February</option>
                                            <option value="3">March</option>  
                                            <option value="4">April</option>  
                                            <option value="5">May</option>  
                                            <option value="6">June</option>  
                                            <option value="7">July</option>  
                                            <option value="8">August</option>  
                                            <option value="9">September</option>  
                                            <option value="10">October</option>  
                                            <option value="11">November</option>  
                                            <option value="12">December</option>    
                                        </select>  
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" style="padding-right: 0px !important"> Year </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="year" >
                                            @for($i = $yearmax; $i >= $yearmindepreciation ; $i--)
                                            <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>  <br>
                                        <button <?php if (myCheckIsEmpty('Depreciation;Default;DefaultCurrency')) echo 'disabled'; ?> class="btn btn-green btn-sm btn-save pull-right" id="btn-submit"> Process </button>
                                    </div>
                                </div>
                            </form>
                        <?php } else { ?>
                            <h5>There is no depreciation.</h5>
                        <?php } ?>
                    </div><!---- end div tableadd---->   
                </div><!---- end div tabwrap---->                       
            </div><!---- end div col-md-4----> 

            <div class="col-md-4">
                <div class="tabwrap">
                    <div class="tabhead">
                        <h4 class="headtitle">Cost of Goods Sold</h4>
                    </div>
                    <div class="tableadd "> 
                        <?php if (!myCheckIsEmpty('SalesPurchase;Default;DefaultCurrency')) { ?> 
                            <form class="form-horizontal" method="POST" action="">
                                <input type="hidden" value="cogs" name="jenis">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" style="padding-right: 0px !important"> Month </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="month" >
                                            <option value="1">January</option>
                                            <option value="2">February</option>
                                            <option value="3">March</option>  
                                            <option value="4">April</option>  
                                            <option value="5">May</option>  
                                            <option value="6">June</option>  
                                            <option value="7">July</option>  
                                            <option value="8">August</option>  
                                            <option value="9">September</option>  
                                            <option value="10">October</option>  
                                            <option value="11">November</option>  
                                            <option value="12">December</option>  
                                        </select>  
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" style="padding-right: 0px !important"> Year </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="year" >
                                            @for($i = $yearmax; $i >= $yearmincogs ; $i--)
                                            <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>  <br>
                                        <button <?php if (myCheckIsEmpty('SalesPurchase;Default;DefaultCurrency')) echo 'disabled'; ?> class="btn btn-green btn-sm btn-save pull-right" id="btn-submit"> Process </button>
                                    </div>
                                </div>
                            </form>
                        <?php } else { ?>
                            <h5>There is no sales or purchase.</h5>
                        <?php } ?>
                    </div><!---- end div tableadd---->   
                </div><!---- end div tabwrap---->                       
            </div><!---- end div col-md-4----> 

            <div class="col-md-4">
                <div class="tabwrap">
                    <div class="tabhead">
                        <h4 class="headtitle">Closing Balance</h4>
                    </div>
                    <div class="tableadd ">
                        <?php if (!myCheckIsEmpty('Journal;Default;DefaultCurrency')) { ?> 
                            <form class="form-horizontal" method="POST" action="">
                                <input type="hidden" value="closing" name="jenis">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" style="padding-right: 0px !important"> Month </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="month" >
                                            <option value="1">January</option>
                                            <option value="2">February</option>
                                            <option value="3">March</option>  
                                            <option value="4">April</option>  
                                            <option value="5">May</option>  
                                            <option value="6">June</option>  
                                            <option value="7">July</option>  
                                            <option value="8">August</option>  
                                            <option value="9">September</option>  
                                            <option value="10">October</option>  
                                            <option value="11">November</option>  
                                            <option value="12">December</option>  
                                        </select>  
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" style="padding-right: 0px !important"> Year </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="year" >
                                            @for($i = $yearmax; $i >= $yearminclosingbalance ; $i--)
                                            <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>  <br>
                                        <button <?php if (myCheckIsEmpty('Journal;Default;DefaultCurrency')) echo 'disabled'; ?> class="btn btn-green btn-sm btn-save pull-right" id="btn-submit"> Process </button>
                                    </div>
                                </div>
                            </form>
                        <?php } else { ?>
                            <h5>There is no journal.</h5>
                        <?php } ?>
                    </div><!---- end div tableadd---->   
                </div><!---- end div tabwrap---->                       
            </div><!---- end div col-md-4----> 

            <div class="col-md-4">
                <div class="tabwrap">
                    <div class="tabhead">
                        <h4 class="headtitle">Balance Transfer</h4>
                    </div>
                    <div class="tableadd ">
                        <?php if (!myCheckIsEmpty('Journal;')) { ?> 
                            <form class="form-horizontal" method="POST" action="">
                                <input type="hidden" value="balancetransfer" name="jenis">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" style="padding-right: 0px !important"> Month </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="month" >
                                            <option value="1">January</option>
                                            <option value="2">February</option>
                                            <option value="3">March</option>  
                                            <option value="4">April</option>  
                                            <option value="5">May</option>  
                                            <option value="6">June</option>  
                                            <option value="7">July</option>  
                                            <option value="8">August</option>  
                                            <option value="9">September</option>  
                                            <option value="10">October</option>  
                                            <option value="11">November</option>  
                                            <option value="12">December</option>  
                                        </select>  
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" style="padding-right: 0px !important"> Year </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="year" >
                                            @for($i = $yearmax; $i >= $yearminclosingbalance ; $i--)
                                            <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>  <br>
                                        <button <?php if (myCheckIsEmpty('Journal')) echo 'disabled'; ?> class="btn btn-green btn-sm btn-save pull-right" id="btn-submit"> Process </button>
                                    </div>
                                </div>
                            </form>
                        <?php } else { ?>
                            <h5>There is no journal.</h5>
                        <?php } ?>
                    </div><!---- end div tableadd---->   
                </div><!---- end div tabwrap---->                       
            </div>
        </div><!-- end div row-->

    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop

@section('modal')

@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.2/raphael-min.js"></script>
<script src="{{Asset('morris.js')}}" type="text/javascript"></script>
<script>
</script>
@stop