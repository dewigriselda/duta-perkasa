<?php

$path = "$_SERVER[HTTP_HOST]";
$path = explode('.', $path . '.');
if ($path[1] == 'salmonacc') {
    $text = 'Salmon Demo';
    $icon = "sal.ico";
    $class = "logo_sal";
} else {
    $text = 'Esatacc';
    $icon = "esa.ico";
    $class = "logo_esa";
}
return array('header_company' => $text, "icon" => $icon, "class" => $class);
