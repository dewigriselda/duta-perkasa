<?php

class Coa1Controller extends BaseController {

    public function showCoa1() {
        return View::make('coa1');
    }

    public static function insertCoa1() {
        //rule
        $rule = array(
            'AccID' => 'required|max:1|unique:m_coa1,ACC1ID,NULL,ACC1ID,CompanyInternalID,' . Auth::user()->Company->InternalID . '',
            'AccName' => 'required|max:200',
            'remark' => 'required|max:1000'
        );
        $messages = array(
            'AccID.unique' => 'Account ID has already been taken.',
            'AccID.required' => 'Account ID field is required.',
            'AccID.max' => 'Account ID may not be greater than 1 characters.',
            'AccName.required' => 'Account name field is required.',
            'AccName.max' => 'Account name may not be greater than 200 characters.'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule, $messages);
        if ($validator->fails()) {
            //tidak valid
            return View::make('coa.coaLevel')
                            ->withMessages('gagalInsert')
                            ->withError($validator->messages())
                            ->withToogle('accounting')->withAktif('coaLevel');
        } else {
            //valid
            $coa1 = new Coa1;
            $coa1->ACC1ID = Input::get('AccID');
            $coa1->ACC1Name = Input::get('AccName');
            $coa1->UserRecord = Auth::user()->UserID;
            $coa1->CompanyInternalID = Auth::user()->Company->InternalID;
            $coa1->UserModified = "0";
            $coa1->Remark = Input::get('remark');
            $coa1->save();

            return View::make('coa.coaLevel')
                            ->withMessages('suksesInsert')
                            ->withToogle('accounting')->withAktif('coaLevel');
        }
    }

    static function updateCoa1() {
        //rule
        $rule = array(
            'AccName' => 'required|max:200',
            'remark' => 'required|max:1000'
        );
        $messages = array(
            'AccName.required' => 'Account name field is required.',
            'AccName.max' => 'Account name may not be greater than 200 characters.'
        );

        //validasi
        $validator = Validator::make(Input::all(), $rule, $messages);
        if ($validator->fails()) {
            //tidak valid
            return View::make('coa.coaLevel')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('accounting')->withAktif('coaLevel');
        } else {
            //valid
            $coa1 = Coa1::find(Input::get('InternalID'));
            if ($coa1->CompanyInternalID == Auth::user()->Company->InternalID) {
                $coa1->ACC1Name = Input::get('AccName');
                $coa1->UserModified = Auth::user()->UserID;
                $coa1->Remark = Input::get('remark');
                $coa1->save();
                return View::make('coa.coaLevel')
                                ->withMessages('suksesUpdate')
                                ->withToogle('accounting')->withAktif('coaLevel');
            } else {
                return View::make('coa.coaLevel')
                                ->withMessages('accessDenied')
                                ->withToogle('accounting')->withAktif('coaLevel');
            }
        }
    }

    static function deleteCoa1() {
        $coa = DB::table('m_coa')->where('ACC1InternalID', Input::get('InternalID'))->first();
        $coa2 = Coa2::coa2inCoa1(Input::get('AccID'));
        //cek id coa_1 ada di master coa atau tidak
        if (is_null($coa) && count($coa2) <= 0) {
            //tidak ada maka boleh dihapus
            $coa1 = Coa1::find(Input::get('InternalID'));
            if ($coa1->CompanyInternalID == Auth::user()->Company->InternalID) {
                $coa1->delete();
                return View::make('coa.coaLevel')
                                ->withMessages('suksesDelete')
                                ->withToogle('accounting')->withAktif('coaLevel');
            } else {
                return View::make('coa.coaLevel')
                                ->withMessages('accessDenied')
                                ->withToogle('accounting')->withAktif('coaLevel');
            }
        } else {
            //ada maka tidak dihapus
            return View::make('coa.coaLevel')
                            ->withMessages('gagalDelete')
                            ->withToogle('accounting')->withAktif('coaLevel');
        }
    }

}
