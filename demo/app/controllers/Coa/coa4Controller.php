<?php

class Coa4Controller extends BaseController {

    public function showCoa4() {
        return View::make('coa4');
    }

    static function insertCoa4() {
        //rule
        $rule = array(
            'AccID' => 'required|max:200',
            'AccName' => 'required|max:200',
            'remark' => 'required|max:1000',
            '0' => 'unique:m_coa4,ACC4ID,NULL,ACC4ID,CompanyInternalID,' . Auth::user()->Company->InternalID . ''
        );
        $messages = array(
            '0.unique' => 'Account ID has already been taken.',
            'AccName.required' => 'Account name field is required.',
            'AccName.max' => 'Account name may not be greater than 200 characters.',
            'AccID.required' => 'Account ID field is required.',
            'AccID.max' => 'Account ID may not be greater than 200 characters.'
        );

        //validasi    
        $data = Input::all();
        array_push($data, Input::get('parentid4') . Input::get('AccID'));
        $validator = Validator::make($data, $rule, $messages);
        if ($validator->fails()) {
            //tidak valid
            return View::make('coa.coaLevel')
                            ->withMessages('gagalInsert')
                            ->withError($validator->messages())
                            ->withToogle('accounting')->withAktif('coaLevel');
        } else {
            //valid
            $coa4 = new Coa4;
            $coa4->ACC4ID = Input::get('parentid4') . Input::get('AccID');
            $coa4->ACC4Name = Input::get('AccName');
            $coa4->UserRecord = Auth::user()->UserID;
            $coa4->CompanyInternalID = Auth::user()->Company->InternalID;
            $coa4->UserModified = '0';
            $coa4->Remark = Input::get('remark');
            $coa4->save();

            return View::make('coa.coaLevel')
                            ->withMessages('suksesInsert')
                            ->withToogle('accounting')->withAktif('coaLevel');
        }
    }

    static function updateCoa4() {
        //rule
        $rule = array(
            'AccName' => 'required|max:200',
            'remark' => 'required|max:1000'
        );
        $messages = array(
            'AccName.required' => 'Account name field is required.',
            'AccName.max' => 'Account name may not be greater than 200 characters.',
        );

        //validasi
        $validator = Validator::make(Input::all(), $rule, $messages);
        if ($validator->fails()) {
            //tidak valid
            return View::make('coa.coaLevel')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('accounting')->withAktif('coaLevel');
        } else {
            //valid
            $coa4 = Coa4::find(Input::get('InternalID'));
            if ($coa4->CompanyInternalID == Auth::user()->Company->InternalID) {
                $coa4->ACC4Name = Input::get('AccName');
                $coa4->UserModified = Auth::user()->UserID;
                $coa4->Remark = Input::get('remark');
                $coa4->save();
                return View::make('coa.coaLevel')
                                ->withMessages('suksesUpdate')
                                ->withToogle('accounting')->withAktif('coaLevel');
            } else {
                return View::make('coa.coaLevel')
                                ->withMessages('accessDenied')
                                ->withToogle('accounting')->withAktif('coaLevel');
            }
        }
    }

    static function deleteCoa4() {
        //cek apakah ID coa4 ada di tabel m_coa atau tidak
        $coa = DB::table('m_coa')->where('ACC4InternalID', Input::get('InternalID'))->first();
        if (is_null($coa)) {
            //tidak ada maka data boleh dihapus
            $coa4 = Coa4::find(Input::get('InternalID'));
            if ($coa4->CompanyInternalID == Auth::user()->Company->InternalID) {
                $coa4->delete();
                return View::make('coa.coaLevel')
                                ->withMessages('suksesDelete')
                                ->withToogle('accounting')->withAktif('coaLevel');
            } else {
                return View::make('coa.coaLevel')
                                ->withMessages('accessDenied')
                                ->withToogle('accounting')->withAktif('coaLevel');
            }
        } else {
            //ada maka data tidak boleh dihapus
            return View::make('coa.coaLevel')
                            ->withMessages('gagalDelete')
                            ->withToogle('accounting')->withAktif('coaLevel');
        }
    }

}
