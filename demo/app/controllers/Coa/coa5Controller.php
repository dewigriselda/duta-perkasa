<?php

class Coa5Controller extends BaseController {

    public function showCoa5() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertCoa') {
                return $this->insertCoa5();
            }
            if (Input::get('jenis') == 'updateCoa') {
                return $this->updateCoa5();
            }
            if (Input::get('jenis') == 'deleteCoa') {
                return $this->deleteCoa5();
            }
        }
        return View::make('coa.coa5')
                        ->withToogle('accounting')->withAktif('coa5');
    }

    static function insertCoa5() {
        //rule
        $rule = array(
            'AccID' => 'required|max:200|unique:m_coa5,ACC5ID,NULL,ACC5ID,CompanyInternalID,' . Auth::user()->Company->InternalID . '',
            'AccName' => 'required|max:200',
            'remark' => 'required|max:1000'
        );
        $messages = array(
            'AccID.unique' => 'Account ID has already been taken.',
            'AccID.required' => 'Account ID field is required.',
            'AccID.max' => 'Account ID may not be greater than 200 characters.',
            'AccName.required' => 'Account name field is required.',
            'AccName.max' => 'Account name may not be greater than 200 characters.'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule, $messages);
        if ($validator->fails()) {
            //tidak valid
            return View::make('coa.coa5')
                            ->withMessages('gagalInsert')
                            ->withError($validator->messages())
                            ->withToogle('accounting')->withAktif('coa5');
        } else {
            //valid
            $coa5 = new Coa5;
            $coa5->ACC5ID = Input::get('AccID');
            $coa5->ACC5Name = Input::get('AccName');
            $coa5->UserRecord = Auth::user()->UserID;
            $coa5->CompanyInternalID = Auth::user()->Company->InternalID;
            $coa5->UserModified = '0';
            $coa5->Remark = Input::get('remark');
            $coa5->save();

            return View::make('coa.coa5')
                            ->withMessages('suksesInsert')
                            ->withToogle('accounting')->withAktif('coa5');
        }
    }

    function updateCoa5() {
        //rule
        $rule = array(
            'AccName' => 'required|max:200',
            'remark' => 'required|max:1000'
        );
        $messages = array(
            'AccName.required' => 'Account name field is required.',
            'AccName.max' => 'Account name may not be greater than 200 characters.'
        );

        //validasi
        $validator = Validator::make(Input::all(), $rule, $messages);
        if ($validator->fails()) {
            //tidak valid
            return View::make('coa.coa5')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('accounting')->withAktif('coa5');
        } else {
            //valid
            $coa5 = Coa5::find(Input::get('InternalID'));
            if ($coa5->CompanyInternalID == Auth::user()->Company->InternalID) {
                $coa5->ACC5Name = Input::get('AccName');
                $coa5->UserModified = Auth::user()->UserID;
                $coa5->Remark = Input::get('remark');
                $coa5->save();
                return View::make('coa.coa5')
                                ->withMessages('suksesUpdate')
                                ->withToogle('accounting')->withAktif('coa5');
            } else {
                return View::make('coa.coa5')
                                ->withMessages('accessDenied')
                                ->withToogle('accounting')->withAktif('coa5');
            }
        }
    }

    function deleteCoa5() {
        //cek apakah ID coa5 ada di tabel m_coa atau tidak
        $coa = DB::table('m_coa')->where('ACC5InternalID', Input::get('InternalID'))->first();
        //cek apakah ID coa5 ada di tabel journal header atau tidak
        $journal = DB::table('t_journal_header')->where('ACC5InternalID', Input::get('InternalID'))->first();
        if (is_null($coa) && is_null($journal)) {
            //tidak ada maka data boleh dihapus
            $coa5 = Coa5::find(Input::get('InternalID'));
            if ($coa5->CompanyInternalID == Auth::user()->Company->InternalID) {
                $coa5->delete();
                return View::make('coa.coa5')
                                ->withMessages('suksesDelete')
                                ->withToogle('accounting')->withAktif('coa5');
            } else {
                return View::make('coa.coa5')
                                ->withMessages('accessDenied')
                                ->withToogle('accounting')->withAktif('coa5');
            }
        } else {
            //ada maka data tidak boleh dihapus
            return View::make('coa.coa5')
                            ->withMessages('gagalDelete')
                            ->withToogle('accounting')->withAktif('coa5');
        }
    }

    public function exportExcel() {
        Excel::create('COA_Level5', function($excel) {
            $excel->sheet('COA_Level5', function($sheet) {
                $sheet->mergeCells('B1:G1');
                $sheet->setCellValueByColumnAndRow(1, 1, "COA Level 5");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Account Name");
                $sheet->setCellValueByColumnAndRow(3, 2, "Account ID");
                $sheet->setCellValueByColumnAndRow(4, 2, "Record");
                $sheet->setCellValueByColumnAndRow(5, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(6, 2, "Remark");
                $row = 3;
                foreach (Coa5::where("InternalID", "!=", "0")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, $data->ACC5Name);
                    $sheet->setCellValueByColumnAndRow(3, $row, "`" . $data->ACC5ID);
                    $sheet->setCellValueByColumnAndRow(4, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(5, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(6, $row, $data->Remark);
                    $row++;
                }

                if (Coa5::where("InternalID", "!=", "0")->where('CompanyInternalID', Auth::user()->Company->InternalID)->count() <= 0) {
                    $sheet->mergeCells('B3:G3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "There is no COA Level 5");

                    $sheet->cells('B3:G3', function($cells) {
                        $cells->setAlignment('center');
                    });
                    
                    $sheet->setBorder('B3:G' . $row, 'thin');
                }
                
                $row--;
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B2:G2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B3:G' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
                $sheet->setBorder('B2:G' . $row, 'thin');
            });
        })->export('xls');
    }

}
