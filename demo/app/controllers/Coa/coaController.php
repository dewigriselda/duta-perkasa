<?php

class CoaController extends BaseController {

    public function showCoa() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertCoa') {
                return $this->insertCoa();
            }
            if (Input::get('jenis') == 'updateCoa') {
                return $this->updateCoa();
            }
            if (Input::get('jenis') == 'deleteCoa') {
                return $this->deleteCoa();
            }
        }
        return View::make('coa.coa')
                        ->withToogle('accounting')->withAktif('coa');
    }

    public static function insertCoa() {
        //rule
        $rule = array(
            'coa1' => 'required',
            'coa2' => 'required',
            'coa3' => 'required',
            'coa4' => 'required',
            'coa5' => 'required',
            'coa6' => 'required',
            'CoaName' => 'required|max:200',
            'Type' => 'required',
            'Balance' => 'required',
            'Header1' => 'required|max:500',
            'Header2' => 'required|max:500',
            'Header3' => 'required|max:500',
            'HeaderCashFlow1' => 'required|max:500',
            'HeaderCashFlow2' => 'required|max:500',
            'HeaderCashFlow3' => 'required|max:500',
            'remark' => 'required|max:1000',
            '0' => 'unique:m_coa,InternalID'
        );
        $messages = array(
            '0.unique' => 'Account ID has already been taken.',
            'CoaName.required' => 'Account name field is required.',
            'CoaName.max' => 'Account name may not be greater than 200 characters.'
        );

        //validasi
        $coa1 = explode('---;---', Input::get('coa1'))[2];
        $coa2 = explode('---;---', Input::get('coa2'))[2];
        $coa3 = explode('---;---', Input::get('coa3'))[2];
        $coa4 = explode('---;---', Input::get('coa4'))[2];
        $coa5 = explode('---;---', Input::get('coa5'))[2];
        $coa6 = explode('---;---', Input::get('coa6'))[2];
        $data = Input::all();
        array_push($data, Coa::getInternalID($coa1, $coa2, $coa3, $coa4, $coa5, $coa6));
        $validator = Validator::make($data, $rule, $messages);
        if ($validator->fails()) {
            //tidak valid
            return View::make('coa.coa')
                            ->withMessages('gagalInsert')
                            ->withError($validator->messages())
                            ->withToogle('accounting')->withAktif('coa');
        } else {
            //valid
            $coa = new Coa;
            $coa->ACC1InternalID = $coa1;
            $coa->ACC2InternalID = $coa2;
            $coa->ACC3InternalID = $coa3;
            $coa->ACC4InternalID = $coa4;
            $coa->ACC5InternalID = $coa5;
            $coa->ACC6InternalID = $coa6;
            $coa->COAName = Input::get('CoaName');
            $coa->Flag = Input::get('Type');
            $coa->Header1 = Input::get('Header1');
            $coa->Header2 = Input::get('Header2');
            $coa->Header3 = Input::get('Header3');
            $coa->HeaderCashFlow1 = Input::get('HeaderCashFlow1');
            $coa->HeaderCashFlow2 = Input::get('HeaderCashFlow2');
            $coa->HeaderCashFlow3 = Input::get('HeaderCashFlow3');
            $coa->Persediaan = 0;
            $coa->InitialBalance = str_replace(',', '', Input::get('Balance'));
            if (Input::get('Stock') == '1') {
                $coa->Persediaan = '1';
            }
            $coa->UserRecord = Auth::user()->UserID;
            $coa->CompanyInternalID = Auth::user()->Company->InternalID;
            $coa->UserModified = "0";
            $coa->Remark = Input::get('remark');
            $coa->save();

            return View::make('coa.coa')
                            ->withMessages('suksesInsert')
                            ->withToogle('accounting')->withAktif('coa');
        }
    }

    function updateCoa() {
        //rule
        $rule = array(
            'CoaName' => 'required|max:200',
            'Header1' => 'required|max:500',
            'Header2' => 'required|max:500',
            'Header3' => 'required|max:500',
            'HeaderCashFlow1' => 'required|max:500',
            'HeaderCashFlow2' => 'required|max:500',
            'HeaderCashFlow3' => 'required|max:500',
            'Balance' => 'required',
            'remark' => 'required|max:1000'
        );
        $messages = array(
            'CoaName.required' => 'Account name field is required.',
            'CoaName.max' => 'Account name may not be greater than 200 characters.'
        );
        //validasi
        $validator = Validator::make(Input::all(), $rule, $messages);
        if ($validator->fails()) {
            //tidak valid
            return View::make('coa.coa')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('accounting')->withAktif('coa');
        } else {
            //valid
            $coa = Coa::find(Input::get('InternalID'));
            if ($coa->CompanyInternalID == Auth::user()->Company->InternalID) {
                $coa->COAName = Input::get('CoaName');
                $coa->Header1 = Input::get('Header1');
                $coa->Header2 = Input::get('Header2');
                $coa->Header3 = Input::get('Header3');
                $coa->HeaderCashFlow1 = Input::get('HeaderCashFlow1');
                $coa->HeaderCashFlow2 = Input::get('HeaderCashFlow2');
                $coa->HeaderCashFlow3 = Input::get('HeaderCashFlow3');
                $coa->Persediaan = 0;
                $coa->InitialBalance = str_replace(',', '', Input::get('Balance'));
                if (Input::get('Stock') == '1') {
                    $coa->Persediaan = '1';
                }
                $coa->UserModified = Auth::user()->UserID;
                $coa->Remark = Input::get('remark');
                $coa->save();
                return View::make('coa.coa')
                                ->withMessages('suksesUpdate')
                                ->withToogle('accounting')->withAktif('coa');
            } else {
                return View::make('coa.coa')
                                ->withMessages('accessDenied')
                                ->withToogle('accounting')->withAktif('coa');
            }
        }
    }

    function deleteCoa() {
        $coa = Coa::find(Input::get('InternalID'));
        $ACC = explode(".", Input::get('AccID'));
        $slip = DB::table('m_slip')
                ->where('ACC1InternalID', $ACC[0])
                ->where('ACC2InternalID', $ACC[1])
                ->where('ACC3InternalID', $ACC[2])
                ->where('ACC4InternalID', $ACC[3])
                ->where('ACC5InternalID', $ACC[4])
                ->where('ACC6InternalID', $ACC[5])
                ->first();
        //cek coa ada di master slip atau tidak
        $journal = DB::table('t_journal_detail')
                ->where('ACC1InternalID', $ACC[0])
                ->where('ACC2InternalID', $ACC[1])
                ->where('ACC3InternalID', $ACC[2])
                ->where('ACC4InternalID', $ACC[3])
                ->where('ACC5InternalID', $ACC[4])
                ->where('ACC6InternalID', $ACC[5])
                ->first();
        //cek coa ada di master journal detail atau tidak
        $inventoryType = DB::table('m_inventorytype')
                ->where('ACC1InternalID', $ACC[0])
                ->where('ACC2InternalID', $ACC[1])
                ->where('ACC3InternalID', $ACC[2])
                ->where('ACC4InternalID', $ACC[3])
                ->where('ACC5InternalID', $ACC[4])
                ->where('ACC6InternalID', $ACC[5])
                ->first();
        //cek coa ada di master depreciation atau tidak
        $default = DB::table('s_default')
                ->where('ACC1InternalID', $ACC[0])
                ->where('ACC2InternalID', $ACC[1])
                ->where('ACC3InternalID', $ACC[2])
                ->where('ACC4InternalID', $ACC[3])
                ->where('ACC5InternalID', $ACC[4])
                ->where('ACC6InternalID', $ACC[5])
                ->first();
        //cek coa ada di default atau tidak
        $depreciation1 = DB::table('m_depreciation_header')
                ->where('ACC1InternalIDDebet', $ACC[0])
                ->where('ACC2InternalIDDebet', $ACC[1])
                ->where('ACC3InternalIDDebet', $ACC[2])
                ->where('ACC4InternalIDDebet', $ACC[3])
                ->where('ACC5InternalIDDebet', $ACC[4])
                ->where('ACC6InternalIDDebet', $ACC[5])
                ->first();
        //cek coa ada di master depreciation atau tidak
        $depreciation2 = DB::table('m_depreciation_header')
                ->where('ACC1InternalIDCredit', $ACC[0])
                ->where('ACC2InternalIDCredit', $ACC[1])
                ->where('ACC3InternalIDCredit', $ACC[2])
                ->where('ACC4InternalIDCredit', $ACC[3])
                ->where('ACC5InternalIDCredit', $ACC[4])
                ->where('ACC6InternalIDCredit', $ACC[5])
                ->first();
        //cek coa ada di master depreciation atau tidak
        $gdepreciation1 = DB::table('m_groupdepreciation')
                ->where('ACC1InternalIDDebet', $ACC[0])
                ->where('ACC2InternalIDDebet', $ACC[1])
                ->where('ACC3InternalIDDebet', $ACC[2])
                ->where('ACC4InternalIDDebet', $ACC[3])
                ->where('ACC5InternalIDDebet', $ACC[4])
                ->where('ACC6InternalIDDebet', $ACC[5])
                ->first();
        //cek coa ada di master group depreciation atau tidak
        $gdepreciation2 = DB::table('m_groupdepreciation')
                ->where('ACC1InternalIDCredit', $ACC[0])
                ->where('ACC2InternalIDCredit', $ACC[1])
                ->where('ACC3InternalIDCredit', $ACC[2])
                ->where('ACC4InternalIDCredit', $ACC[3])
                ->where('ACC5InternalIDCredit', $ACC[4])
                ->where('ACC6InternalIDCredit', $ACC[5])
                ->first();
        //cek coa ada di master group depreciation atau tidak
        if (is_null($slip) && is_null($journal) && is_null($inventoryType) && is_null($gdepreciation2) && is_null($gdepreciation1) && is_null($depreciation2) && is_null($depreciation1) && is_null($default)) {
            //tidak ada maka boleh dihapus
            if ($coa->CompanyInternalID == Auth::user()->Company->InternalID) {
                $coa->delete();
                return View::make('coa.coa')
                                ->withMessages('suksesDelete')
                                ->withToogle('accounting')->withAktif('coa');
            } else {
                return View::make('coa.coa')
                                ->withMessages('accessDenied')
                                ->withToogle('accounting')->withAktif('coa');
            }
        } else {
            //ada maka tidak dihapus
            return View::make('coa.coa')
                            ->withMessages('gagalDelete')
                            ->withToogle('accounting')->withAktif('coa');
        }
    }

    public function exportExcel() {
        Excel::create('COA_Master', function($excel) {
            $excel->sheet('COA_Master', function($sheet) {
                $sheet->mergeCells('B1:I1');
                $sheet->setCellValueByColumnAndRow(1, 1, "COA Master");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Account Name");
                $sheet->setCellValueByColumnAndRow(3, 2, "Account ID");
                $sheet->setCellValueByColumnAndRow(4, 2, "Type");
                $sheet->setCellValueByColumnAndRow(5, 2, "Stock");
                $sheet->setCellValueByColumnAndRow(6, 2, "Record");
                $sheet->setCellValueByColumnAndRow(7, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(8, 2, "Remark");
                $row = 3;
                foreach (Coa::showCoa() as $data) {
                    $tipe = '';
                    if ($data->Flag == 0) {
                        $tipe = 'Balance';
                    } else {
                        $tipe = 'Profit and Loss';
                    }
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, $data->COAName);
                    $sheet->setCellValueByColumnAndRow(3, $row, "`" . Coa::formatCoa($data->ACC1ID, $data->ACC2ID, $data->ACC3ID, $data->ACC4ID, $data->ACC5ID, $data->ACC6ID, '0'));
                    $sheet->setCellValueByColumnAndRow(4, $row, $tipe);
                    $sheet->setCellValueByColumnAndRow(5, $row, $data->Persediaan);
                    $sheet->setCellValueByColumnAndRow(6, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(7, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(8, $row, $data->Remark);
                    $row++;
                }

                if (Coa::where('CompanyInternalID', Auth::user()->CompanyInternalID)->count() <= 0) {
                    $sheet->mergeCells('B3:I3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "There is no COA Master");
                    $sheet->cells('B3:I3', function($cells) {
                        $cells->setAlignment('center');
                    });
                    $sheet->setBorder('B3:I' . $row, 'thin');
                }

                $row--;
                $sheet->setBorder('B2:I' . $row, 'thin');
                $sheet->cells('B2:I2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:I' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
                $sheet->cells('F3:F' . $row, function($cells) {
                    $cells->setAlignment('right');
                });
            });
        })->export('xls');
    }

}
