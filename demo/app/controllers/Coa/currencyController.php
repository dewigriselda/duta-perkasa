<?php

class CurrencyController extends BaseController {

    public function showCurrency() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertCurrency') {
                return $this->insertCurrency();
            }
            if (Input::get('jenis') == 'updateCurrency') {
                return $this->updateCurrency();
            }
            if (Input::get('jenis') == 'deleteCurrency') {
                return $this->deleteCurrency();
            }
        }
        return View::make('coa.currency')
                        ->withToogle('accounting')->withAktif('currency');
    }

    public static function insertCurrency() {
        //rule
        $rule = array(
            'CurrencyID' => 'required|max:200|unique:m_currency,CurrencyID,NULL,CurrencyID,CompanyInternalID,' . Auth::user()->Company->InternalID . '',
            'CurrencyName' => 'required|max:200',
            'Default' => 'required|Integer|max:1',
            'Rate' => 'numeric|required',
            'remark' => 'required|max:1000'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('coa.currency')
                            ->withMessages('gagalInsert')
                            ->withError($validator->messages())
                            ->withToogle('accounting')->withAktif('currency')
                            ->withErrors($validator);
        } else {
            //valid
            $currency = new Currency;
            $currency->CurrencyID = Input::get('CurrencyID');
            $currency->CurrencyName = Input::get('CurrencyName');
            $currency->Default = Input::get('Default');
            if (Input::get('Default') == '1') {
                Currency::where('Default', '=', 1)->where('CompanyInternalID', Auth::user()->Company->InternalID)->update(array('Default' => 0));
            }
            $currency->Rate = Input::get('Rate');
            $currency->UserRecord = Auth::user()->UserID;
            $currency->CompanyInternalID = Auth::user()->Company->InternalID;
            $currency->UserModified = "0";
            $currency->Remark = Input::get('remark');
            $currency->save();

            return View::make('coa.currency')
                            ->withMessages('suksesInsert')
                            ->withToogle('accounting')->withAktif('currency');
        }
    }

    static function updateCurrency() {
        //rule
        $rule = array(
            'CurrencyName' => 'required|max:200',
            'Default' => 'required|Integer|max:1',
            'Rate' => 'numeric|required',
            'remark' => 'required|max:1000'
        );
        //validasi
        $validator = Validator ::make(Input::all(), $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('coa.currency')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('accounting')->withAktif('currency');
        } else {
            //valid
            $currency = Currency::find(Input::get('InternalID'));
            if ($currency->CompanyInternalID == Auth::user()->Company->InternalID) {
                $currency->CurrencyName = Input::get('CurrencyName');
                $currency->Default = Input::get('Default');
                if (Input::get('Default') == '1' && $currency->Default != 1) {
                    Currency::where('Default', '=', 1)->where('CompanyInternalID', Auth::user()->Company->InternalID)->update(array('Default' => 0));
                }
                $currency->Rate = Input::get('Rate');
                $currency->UserModified = Auth::user()->UserID;
                $currency->Remark = Input::get('remark');
                $currency->save();
                return View::make('coa.currency')
                                ->withMessages('suksesUpdate')
                                ->withToogle('accounting')->withAktif('currency');
            } else {
                return View::make('coa.currency')
                                ->withMessages('accessDenied')
                                ->withToogle('accounting')->withAktif('currency');
            }
        }
    }

    static function deleteCurrency() {
        //cek currency ada di slip atau tidak
        $slip = DB::table('m_slip')->where('CurrencyInternalID', Input::get('InternalID'))->first();
        //cek currency ada di journal atau tidak
        $journal = DB::table('t_journal_detail')->where('CurrencyInternalID', Input::get('InternalID'))->first();
        //cek apakah ID Currency ada di tabel sales header atau tidak
        $sales = DB::table('t_sales_header')->where('CurrencyInternalID', Input::get('InternalID'))->first();
        //cek apakah ID Currency ada di tabel sales return header atau tidak
        $salesReturn = DB::table('t_salesreturn_header')->where('CurrencyInternalID', Input::get('InternalID'))->first();
        //cek apakah ID Currency ada di tabel sales order header atau tidak
        $salesOrder = DB::table('t_salesorder_header')->where('CurrencyInternalID', Input::get('InternalID'))->first();
        //cek apakah ID Currency ada di tabel purchase header atau tidak
        $purchase = DB::table('t_purchase_header')->where('CurrencyInternalID', Input::get('InternalID'))->first();
        //cek apakah ID Currency ada di tabel purchase return header atau tidak
        $purchaseReturn = DB::table('t_purchasereturn_header')->where('CurrencyInternalID', Input::get('InternalID'))->first();
        //cek apakah ID Currency ada di tabel purchase order header atau tidak
        $purchaseOrder = DB::table('t_purchaseorder_header')->where('CurrencyInternalID', Input::get('InternalID'))->first();
        //cek apakah ID Currency ada di tabel memo OUT atau tidak
        $memoOut = DB::table('t_memoout_header')->where('CurrencyInternalID', Input::get('InternalID'))->first();
        //cek apakah ID Currency ada di tabel memo IN atau tidak
        $memoIn = DB::table('t_memoin_header')->where('CurrencyInternalID', Input::get('InternalID'))->first();
        //cek apakah ID Currency ada di tabel region atau tidak
        $region = DB::table('m_region')->where('CurrencyInternalID', Input::get('InternalID'))->first();
        
        if (is_null($slip) && is_null($journal) && is_null($sales) && is_null($salesReturn) && is_null($salesOrder) && is_null($purchase) && is_null($purchaseReturn) && is_null($purchaseOrder) && is_null($memoIn) && is_null($memoOut) && is_null($region)) {
            //tidak ada maka boleh dihapus
            $currency = Currency::find(Input::get('InternalID'));
            if ($currency->CompanyInternalID == Auth::user()->Company->InternalID) {
                $currency->delete();
                return View::make('coa.currency')
                                ->withMessages('suksesDelete')
                                ->withToogle('accounting')->withAktif('currency');
            } else {
                return View::make('coa.currency')
                                ->withMessages('accessDenied')
                                ->withToogle('accounting')->withAktif('currency');
            }
        } else {
            //ada maka tidak dihapus
            return View::make('coa.currency')
                            ->withMessages('gagalDelete')
                            ->withToogle('accounting')->withAktif('currency');
        }
    }

    public function exportExcel() {
        Excel::create('Master_Currency', function($excel) {
            $excel->sheet('Master_Currency', function($sheet) {
                $sheet->mergeCells('B1:H1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Master Currency");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Currency Name");
                $sheet->setCellValueByColumnAndRow(3, 2, "Currency ID");
                $sheet->setCellValueByColumnAndRow(4, 2, "Rate");
                $sheet->setCellValueByColumnAndRow(5, 2, "Record");
                $sheet->setCellValueByColumnAndRow(6, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(7, 2, "Remark");
                $row = 3;
                foreach (Currency::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                    $default = '';
                    if ($data->Default == 1) {
                        $default = '(Default)';
                    }
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, $data->CurrencyName . ' ' . $default);
                    $sheet->setCellValueByColumnAndRow(3, $row, $data->CurrencyID);
                    $sheet->setCellValueByColumnAndRow(4, $row, number_format($data->Rate, 2, '.', ','));
                    $sheet->setCellValueByColumnAndRow(5, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(6, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(7, $row, $data->Remark);
                    $row++;
                }

                if (Currency::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() <= 0) {
                    $sheet->mergeCells('B3:H3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "No data available in table");

                    $sheet->cells('B3:H3', function($cells) {
                        $cells->setAlignment('center');
                    });
                    
                    $sheet->setBorder('B3:H' . $row, 'thin');
                }

                $row--;
                $sheet->setBorder('B2:H' . $row, 'thin');
                $sheet->cells('B2:H2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:H' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('E3:E' . $row, function($cells) {
                    $cells->setAlignment('right');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
            });
        })->export('xls');
    }

}
