<?php

class DepartmentController extends BaseController {

    public function showDepartment() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertDepartment') {
                return $this->insertDepartment();
            }
            if (Input::get('jenis') == 'updateDepartment') {
                return $this->updateDepartment();
            }
            if (Input::get('jenis') == 'deleteDepartment') {
                return $this->deleteDepartment();
            }
        }
        return View::make('coa.department')
                        ->withToogle('accounting')->withAktif('department');
    }

    public static function insertDepartment() {
        //rule
        $rule = array(
            'DepartmentID' => 'required|max:200|unique:m_department,DepartmentID,NULL,DepartmentID,CompanyInternalID,' . Auth::user()->Company->InternalID . '',
            'DepartmentName' => 'required|max:200',
            'remark' => 'required|max:1000'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('coa.department')
                            ->withMessages('gagalInsert')
                            ->withError($validator->messages())
                            ->withToogle('accounting')->withAktif('department')
                            ->withErrors($validator);
        } else {
            //valid
            $department = new Department;
            $department->DepartmentID = Input::get('DepartmentID');
            $department->DepartmentName = Input::get('DepartmentName');
            $department->Default = Input::get('Default');
            if (Input::get('Default') == '1') {
                Department::where('Default', '=', 1)->where('CompanyInternalID', Auth::user()->Company->InternalID)->update(array('Default' => 0));
            }
            $department->UserRecord = Auth::user()->UserID;
            $department->CompanyInternalID = Auth::user()->Company->InternalID;
            $department->UserModified = "0";
            $department->Remark = Input::get('remark');
            $department->save();

            return View::make('coa.department')
                            ->withMessages('suksesInsert')
                            ->withToogle('accounting')->withAktif('department');
        }
    }

    static function updateDepartment() {
        //rule
        $rule = array(
            'DepartmentName' => 'required|max:200',
            'remark' => 'required|max:1000'
        );
        //validasi
        $validator = Validator ::make(Input::all(), $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('coa.department')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('accounting')->withAktif('department');
        } else {
            //valid
            $department = Department::find(Input::get('InternalID'));
            if ($department->CompanyInternalID == Auth::user()->Company->InternalID) {
                $department->DepartmentName = Input::get('DepartmentName');
                if (Input::get('Default') == '1' && $department->Default != 1) {
                    Department::where('Default', '=', 1)->where('CompanyInternalID', Auth::user()->Company->InternalID)->update(array('Default' => 0));
                }
                $department->Default = Input::get('Default');
                $department->UserModified = Auth::user()->UserID;
                $department->Remark = Input::get('remark');
                $department->save();
                return View::make('coa.department')
                                ->withMessages('suksesUpdate')
                                ->withToogle('accounting')->withAktif('department');
            } else {
                return View::make('coa.department')
                                ->withMessages('accessDenied')
                                ->withToogle('accounting')->withAktif('department');
            }
        }
    }

    static function deleteDepartment() {
        $journal = DB::table('t_journal_header')->where('DepartmentInternalID', Input::get('InternalID'))->first();
        //cek department ada di journal atau tidak
        if (is_null($journal)) {
            //tidak ada maka boleh dihapus
            $department = Department::find(Input::get('InternalID'));
            if ($department->CompanyInternalID == Auth::user()->Company->InternalID) {
                $department->delete();
                return View::make('coa.department')
                                ->withMessages('suksesDelete')
                                ->withToogle('accounting')->withAktif('department');
            } else {
                return View::make('coa.department')
                                ->withMessages('accessDenied')
                                ->withToogle('accounting')->withAktif('department');
            }
        } else {
            //ada maka tidak dihapus
            return View::make('coa.department')
                            ->withMessages('gagalDelete')
                            ->withToogle('accounting')->withAktif('department');
        }
    }

    public function exportExcel() {
        Excel::create('Master_Department', function($excel) {
            $excel->sheet('Master_Department', function($sheet) {
                $sheet->mergeCells('B1:G1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Master Department");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Department Name");
                $sheet->setCellValueByColumnAndRow(3, 2, "Department ID");
                $sheet->setCellValueByColumnAndRow(4, 2, "Record");
                $sheet->setCellValueByColumnAndRow(5, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(6, 2, "Remark");
                $row = 3;
                foreach (Department::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, $data->DepartmentName);
                    $sheet->setCellValueByColumnAndRow(3, $row, "`" . $data->DepartmentID);
                    $sheet->setCellValueByColumnAndRow(4, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(5, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(6, $row, $data->Remark);
                    $row++;
                }

                if (Department::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() <= 0) {
                    $sheet->mergeCells('B3:G3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "No data available in table");

                    $sheet->cells('B3:G3', function($cells) {
                        $cells->setAlignment('center');
                    });

                    $sheet->setBorder('B3:G' . $row, 'thin');
                }
                
                $row--;
                $sheet->setBorder('B2:G' . $row, 'thin');
                $sheet->cells('B2:G2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:G' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
            });
        })->export('xls');
    }

}
