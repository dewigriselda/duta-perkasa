<?php

class DepreciationController extends BaseController {

    public function showDepreciation() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteDepreciation') {
                return $this->deleteDepreciation();
            } else if (Input::get('jenis') == 'insertDepreciation') {
                return $this->insertDepreciation();
            } else if (Input::get('jenis') == 'updateDepreciation') {
                return $this->updateDepreciation();
            }
        }
        return View::make('coa.depreciation')
                        ->withToogle('accounting')->withAktif('depreciation');
    }

    public function deleteDepreciation() {
        //cek apakah ID ada di tabel depreciation ada atau tidak
        //$depreciation = DB::table('t_depreciation_detail')->where('DepreciationInternalID', Input::get('InternalID'))->first();
        $depreciation = null;
        if (is_null($depreciation)) {
            //tidak ada maka data boleh dihapus
            $depreciation = DepreciationHeader::find(Input::get('InternalID'));
            if ($depreciation->CompanyInternalID == Auth::user()->Company->InternalID) {
                //cek depreciation termasuk transaksi atau tidak
                //hapus detil
                $detilData = DepreciationHeader::find(Input::get('InternalID'))->depreciationDetail;
                foreach ($detilData as $value) {
                    $detil = DepreciationDetail::find($value->InternalID);
                    $detil->delete();
                }
                //hapus depreciation
                $depreciation = DepreciationHeader::find(Input::get('InternalID'));
                $depreciation->delete();
                $messages = 'suksesDelete';
            } else {
                Session::flash('messages', $messages);
                $messages = 'accessDenied';
            }
        } else {
            //ada maka data tidak boleh dihapus
            $messages = 'gagalDelete';
        }
        return View::make('coa.depreciation')
                        ->withToogle('accounting')->withAktif('depreciation')
                        ->withMessages($messages);
    }

    public function insertDepreciation() {
        //rule
        $rule = array(
            'DepreciationID' => 'required|max:200|unique:m_depreciation_header,DepreciationID,NULL,DepreciationID,CompanyInternalID,' . Auth::user()->Company->InternalID . '',
            'date' => 'required',
            'Type' => 'required',
            'groupDepreciation' => 'required',
            'Name' => 'required|max:200',
            'Description' => 'required|max:200',
            'Nominal' => 'required',
            'Method' => 'required',
            'coaD' => 'required',
            'coaC' => 'required',
            'remark' => 'required|max:1000'
        );
        $messages = array(
            'coaD.required' => 'Assets Account field is required.',
            'coaC.required' => 'Cost Account field is required.'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule, $messages);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalInsert';
            $error = $validator->messages();
        } else {
            //valid
            //insert header
            $date = explode('-', Input::get('date'));
            $group = explode('---;---', Input::get('groupDepreciation'));
            $header = new DepreciationHeader();
            $header->DepreciationID = Input::get('DepreciationID');
            $header->DepreciationDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $header->groupDepreciationInternalID = $group[0];
            $header->RoundingOff = Input::get('Type');
            $header->Value = str_replace(',', '', Input::get('Nominal'));
            $header->DepreciationName = Input::get('Name');
            $header->Description = Input::get('Description');
            $header->Method = Input::get('Method');
            $coa = Coa::find(Input::get('coaD'));
            $header->ACC1InternalIDDebet = $coa->ACC1InternalID;
            $header->ACC2InternalIDDebet = $coa->ACC2InternalID;
            $header->ACC3InternalIDDebet = $coa->ACC3InternalID;
            $header->ACC4InternalIDDebet = $coa->ACC4InternalID;
            $header->ACC5InternalIDDebet = $coa->ACC5InternalID;
            $header->ACC6InternalIDDebet = $coa->ACC6InternalID;
            $coa = Coa::find(Input::get('coaC'));
            $header->ACC1InternalIDCredit = $coa->ACC1InternalID;
            $header->ACC2InternalIDCredit = $coa->ACC2InternalID;
            $header->ACC3InternalIDCredit = $coa->ACC3InternalID;
            $header->ACC4InternalIDCredit = $coa->ACC4InternalID;
            $header->ACC5InternalIDCredit = $coa->ACC5InternalID;
            $header->ACC6InternalIDCredit = $coa->ACC6InternalID;
            $header->UserRecord = Auth::user()->UserID;
            $header->CompanyInternalID = Auth::user()->Company->InternalID;
            $header->UserModified = '0';
            $header->Remark = Input::get('remark');
            $header->save();

            $this->insertDetail($header->InternalID);
            $messages = 'suksesInsert';
            $error = '';
        }
        return View::make('coa.depreciation')
                        ->withToogle('accounting')->withAktif('depreciation')
                        ->withError($error)
                        ->withMessages($messages);
    }

    public function updateDepreciation() {
        //rule
        $rule = array(
            'date' => 'required',
            'Type' => 'required',
            'groupDepreciation' => 'required',
            'Name' => 'required|max:200',
            'Description' => 'required|max:200',
            'Nominal' => 'required',
            'Method' => 'required',
            'coaD' => 'required',
            'coaC' => 'required',
            'remark' => 'required|max:1000'
        );
        $messages = array(
            'coaD.required' => 'Assets Account field is required.',
            'coaC.required' => 'Cost Account field is required.'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule, $messages);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalUpdate';
            $error = $validator->messages();
        } else {
            $date = explode('-', Input::get('date'));
            $group = explode('---;---', Input::get('groupDepreciation'));
            $header = DepreciationHeader::find(Input::get('InternalID'));
            $header->DepreciationDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $header->groupDepreciationInternalID = $group[0];
            $header->RoundingOff = Input::get('Type');
            $header->Value = str_replace(',', '', Input::get('Nominal'));
            $header->DepreciationName = Input::get('Name');
            $header->Description = Input::get('Description');
            $header->Method = Input::get('Method');
            $coa = Coa::find(Input::get('coaD'));
            $header->ACC1InternalIDDebet = $coa->ACC1InternalID;
            $header->ACC2InternalIDDebet = $coa->ACC2InternalID;
            $header->ACC3InternalIDDebet = $coa->ACC3InternalID;
            $header->ACC4InternalIDDebet = $coa->ACC4InternalID;
            $header->ACC5InternalIDDebet = $coa->ACC5InternalID;
            $header->ACC6InternalIDDebet = $coa->ACC6InternalID;
            $coa = Coa::find(Input::get('coaC'));
            $header->ACC1InternalIDCredit = $coa->ACC1InternalID;
            $header->ACC2InternalIDCredit = $coa->ACC2InternalID;
            $header->ACC3InternalIDCredit = $coa->ACC3InternalID;
            $header->ACC4InternalIDCredit = $coa->ACC4InternalID;
            $header->ACC5InternalIDCredit = $coa->ACC5InternalID;
            $header->ACC6InternalIDCredit = $coa->ACC6InternalID;
            $header->CompanyInternalID = Auth::user()->Company->InternalID;
            $header->UserModified = Auth::user()->UserID;
            $header->Remark = Input::get('remark');
            $header->save();

            //delete depreciation detail -- nantinya insert ulang
            DepreciationDetail::where('DepreciationInternalID', '=', Input::get('InternalID'))->delete();

            //insert ulang
            $this->insertDetail($header->InternalID);
            $messages = 'suksesUpdate';
            $error = '';
        }

        return View::make('coa.depreciation')
                        ->withToogle('accounting')->withAktif('depreciation')
                        ->withError($error)
                        ->withMessages($messages);
    }

    public function insertDetail($internal) {
        $nominal = str_replace(',', '', Input::get('Nominal'));
        $group = Input::get('groupDepreciation');
        $method = Input::get('Method');
        $type = Input::get('Type');
        $date = Input::get('date');
        $date = explode('-', $date);
        $month = $date[1];
        $year = $date[2];
        $longTime = explode('---;---', $group);
        $longTime = $longTime[1];
        if ($nominal > 0) {
            //method straight
            if ($method == '0') {
                //asumsi method straight
                if ($type == 0) {
                    $dep = $nominal / ($longTime * 12);
                    $dep = number_format($dep, '2', '.', ',');
                } else {
                    $dep = $nominal / ($longTime * 12);
                    $dep = number_format($dep, '0', '.', ',');
                }
                $tampNominal = $nominal;
                $tampTotal = 0;
                $dep = str_replace(',', '', $dep);
                for ($start = 1; $start <= $longTime * 12; $start++) {
                    if($start == $longTime * 12){
                        $dep = $tampNominal - $tampTotal;
                    }
                    $detail = new DepreciationDetail();
                    $detail->DepreciationInternalID = $internal;
                    $detail->Month = $month;
                    $detail->Year = $year;
                    $detail->Value = $dep;
                    $detail->Check1 = '0';
                    $detail->Check2 = '0';
                    if (Input::get('closed') == '1') {
                        $detail->Check2 = '1';
                    }
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->CompanyInternalID = Auth::user()->Company->InternalID;
                    $detail->UserModified = '0';
                    $detail->Remark = Input::get('remark');
                    $detail->save();
                    $tampTotal += str_replace(',', '', $dep);
                    $month++;
                    if ($month > 12) {
                        $month = 1;
                        $year++;
                    }
                }
            } else {
                //method saldo turun
                $countYear = 0;
                $monthKunci = 13 - $month;
                $monthSisa = $month - 1;
                $total = 0;
                $totalAll = 0;
                $tampNominal = $nominal;
                if ($monthSisa == 0) {
                    $countYear = 1;
                }
                for ($start = 1; $start <= ($longTime * 12 * 2); $start++) {
                    $dep = ($nominal / ($longTime * 12)) * ($monthKunci / 12);
                    if ($month == $monthSisa && $countYear == $longTime * 2) {
                        $nominal = $tampNominal - $totalAll;
                        $dep = $nominal;
                    }
                    if ($type == 0) {
                        $dep = number_format($dep, '2', '.', ',');
                    } else {
                        $dep = number_format($dep, '0', '.', ',');
                    }
                    $dep = str_replace(',', '', $dep);
                    $total += $dep;
                    $totalAll += $dep;
                    $detail = new DepreciationDetail();
                    $detail->DepreciationInternalID = $internal;
                    $detail->Month = $month;
                    $detail->Year = $year;
                    $detail->Value = $dep;
                    $detail->Check1 = '0';
                    $detail->Check2 = '0';
                    if (Input::get('closed') == '1') {
                        $detail->Check2 = '1';
                    }
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->CompanyInternalID = Auth::user()->Company->InternalID;
                    $detail->UserModified = '0';
                    $detail->Remark = Input::get('remark');
                    $detail->save();
                    $month++;
                    if ($month > 12) {
                        $nominal -= $total;
                        $total = 0;
                        $month = 1;
                        $year++;
                        $monthKunci = 12;
                        $countYear++;
                        if ($countYear == $longTime * 2) {
                            $monthKunci = $monthSisa;
                        }
                    }
                }
            }
        }
    }

    public function tableDepreciation() {
        $nominal = str_replace(',', '', Input::get('nominal'));
        $group = Input::get('group');
        $method = Input::get('method');
        $type = Input::get('type');
        $date = Input::get('date');
        $date = explode('-', $date);
        $month = $date[1];
        $year = $date[2];
        $longTime = explode('---;---', $group);
        $longTime = $longTime[1];
        if ($nominal <= 0) {
            ?>
            <tr>
                <td width="298px" colspan="3">There is no data depreciation yet.</td> 
            </tr>   
            <?php
        } else {
            //method straight
            if ($method == '0') {
                if ($type == 0) {
                    $dep = number_format($nominal / ($longTime * 12), '2', '.', ',');
                } else {
                    $dep = number_format($nominal / ($longTime * 12), '0', '.', ',');
                }
                $tampNominal = $nominal;
                $tampTotal = 0;
                for ($start = 1; $start <= $longTime * 12; $start++) {
                    if($start == $longTime * 12){
                        $dep = $tampNominal - $tampTotal;
                        $dep = number_format($dep, 2, '.', ',');
                    }
                    if ($start == 1) {
                        ?>
                        <tr>
                            <td width="80px"><?php echo date('F', mktime(0, 0, 0, $month, 10)); ?></td>
                            <td width="60px"><?php echo $year ?></td>
                            <td width="158px"><?php echo $dep ?></td>
                        </tr>
                        <?php
                    } else {
                        ?>
                        <tr>
                            <td><?php echo date('F', mktime(0, 0, 0, $month, 10)); ?></td>
                            <td><?php echo $year ?></td>
                            <td><?php echo $dep ?></td>
                        </tr>
                        <?php
                    }
                    $tampTotal += str_replace(',', '', $dep);
                    $month++;
                    if ($month > 12) {
                        $month = 1;
                        $year++;
                    }
                }
            } else {
                //method saldo turun
                $countYear = 0;
                $monthKunci = 13 - $month;
                $monthSisa = $month - 1;
                $total = 0;
                $totalAll = 0;
                $tampNominal = $nominal;
                if ($monthSisa == 0) {
                    $countYear = 1;
                }
                for ($start = 1; $start <= ($longTime * 12 * 2); $start++) {
                    $dep = ($nominal / ($longTime * 12)) * ($monthKunci / 12);
                    if ($month == $monthSisa && $countYear == $longTime * 2) {
                        $nominal = $tampNominal - $totalAll;
                        $dep = $nominal;
                    }
                    if ($type == 0) {
                        $dep = number_format($dep, '2', '.', ',');
                    } else {
                        $dep = number_format($dep, '0', '.', ',');
                    }
                    $total += str_replace(',', '', $dep);
                    $totalAll += str_replace(',', '', $dep);
                    if ($start == 1) {
                        ?>
                        <tr>
                            <td width="80px"><?php echo date('F', mktime(0, 0, 0, $month, 10)); ?></td>
                            <td width="60px"><?php echo $year ?></td>
                            <td width="158px"><?php echo $dep ?></td>
                        </tr>
                        <?php
                    } else {
                        ?>
                        <tr>
                            <td><?php echo date('F', mktime(0, 0, 0, $month, 10)); ?></td>
                            <td><?php echo $year ?></td>
                            <td><?php echo $dep ?></td>
                        </tr>
                        <?php
                    }
                    $month++;
                    if ($month > 12) {
                        $nominal -= $total;
                        $total = 0;
                        $month = 1;
                        $year++;
                        $monthKunci = 12;
                        $countYear++;
                        if ($countYear == $longTime * 2) {
                            $monthKunci = $monthSisa;
                        }
                    }
                }
            }
        }
    }

}
