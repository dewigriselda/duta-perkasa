<?php

class GroupDepreciationController extends BaseController {

    public function showGroupDepreciation() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertGroupDepreciation') {
                return $this->insertGroupDepreciation();
            }
            if (Input::get('jenis') == 'updateGroupDepreciation') {
                return $this->updateGroupDepreciation();
            }
            if (Input::get('jenis') == 'deleteGroupDepreciation') {
                return $this->deleteGroupDepreciation();
            }
        }
        return View::make('coa.groupDepreciation')
                        ->withToogle('accounting')->withAktif('groupDepreciation');
    }

    public static function insertGroupDepreciation() {
        //rule
        $rule = array(
            'GroupDepreciationID' => 'required|max:200|unique:m_groupdepreciation,GroupDepreciationID,NULL,GroupDepreciationID,CompanyInternalID,' . Auth::user()->Company->InternalID . '',
            'Description' => 'required|max:200',
            'LongDepreciation' => 'required|numeric',
            'coaDebet' => 'required',
            'coaCredit' => 'required',
            'remark' => 'required|max:1000'
        );
        $messages = array(
            'coaDebet.required' => 'Assets Account field is required.',
            'coaCredit.required' => 'Cost Account field is required.'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule, $messages);
        if ($validator->fails()) {
            //tidak valid
            return View::make('coa.groupDepreciation')
                            ->withMessages('gagalInsert')
                            ->withError($validator->messages())
                            ->withToogle('accounting')->withAktif('groupDepreciation')
                            ->withErrors($validator);
        } else {
            //valid
            $groupDepreciation = new GroupDepreciation;
            $groupDepreciation->GroupDepreciationID = Input::get('GroupDepreciationID');
            $groupDepreciation->Description = Input::get('Description');
            $groupDepreciation->LongDepreciation = Input::get('LongDepreciation');
            $coa = Coa::find(Input::get('coaDebet'));
            $groupDepreciation->ACC1InternalIDDebet = $coa->ACC1InternalID;
            $groupDepreciation->ACC2InternalIDDebet = $coa->ACC2InternalID;
            $groupDepreciation->ACC3InternalIDDebet = $coa->ACC3InternalID;
            $groupDepreciation->ACC4InternalIDDebet = $coa->ACC4InternalID;
            $groupDepreciation->ACC5InternalIDDebet = $coa->ACC5InternalID;
            $groupDepreciation->ACC6InternalIDDebet = $coa->ACC6InternalID;
            $coa = Coa::find(Input::get('coaCredit'));
            $groupDepreciation->ACC1InternalIDCredit = $coa->ACC1InternalID;
            $groupDepreciation->ACC2InternalIDCredit = $coa->ACC2InternalID;
            $groupDepreciation->ACC3InternalIDCredit = $coa->ACC3InternalID;
            $groupDepreciation->ACC4InternalIDCredit = $coa->ACC4InternalID;
            $groupDepreciation->ACC5InternalIDCredit = $coa->ACC5InternalID;
            $groupDepreciation->ACC6InternalIDCredit = $coa->ACC6InternalID;
            $groupDepreciation->UserRecord = Auth::user()->UserID;
            $groupDepreciation->CompanyInternalID = Auth::user()->Company->InternalID;
            $groupDepreciation->UserModified = "0";
            $groupDepreciation->Remark = Input::get('remark');
            $groupDepreciation->save();

            return View::make('coa.groupDepreciation')
                            ->withMessages('suksesInsert')
                            ->withToogle('accounting')->withAktif('groupDepreciation');
        }
    }

    static function updateGroupDepreciation() {
        //rule
        $rule = array(
            'Description' => 'required|max:200',
            'LongDepreciation' => 'required|numeric',
            'coaDebet' => 'required',
            'coaCredit' => 'required',
            'remark' => 'required|max:1000'
        );
        $messages = array(
            'coaDebet.required' => 'Assets Account field is required.',
            'coaCredit.required' => 'Cost Account field is required.'
        );

        //validasi
        $validator = Validator::make(Input::all(), $rule, $messages);
        if ($validator->fails()) {
            //tidak valid
            return View::make('coa.groupDepreciation')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('accounting')->withAktif('groupDepreciation');
        } else {
            //valid
            $groupDepreciation = GroupDepreciation::find(Input::get('InternalID'));
            if ($groupDepreciation->CompanyInternalID == Auth::user()->Company->InternalID) {
                $groupDepreciation->Description = Input::get('Description');
                $groupDepreciation->LongDepreciation = Input::get('LongDepreciation');
                $coa = Coa::find(Input::get('coaDebet'));
                $groupDepreciation->ACC1InternalIDDebet = $coa->ACC1InternalID;
                $groupDepreciation->ACC2InternalIDDebet = $coa->ACC2InternalID;
                $groupDepreciation->ACC3InternalIDDebet = $coa->ACC3InternalID;
                $groupDepreciation->ACC4InternalIDDebet = $coa->ACC4InternalID;
                $groupDepreciation->ACC5InternalIDDebet = $coa->ACC5InternalID;
                $groupDepreciation->ACC6InternalIDDebet = $coa->ACC6InternalID;
                $coa = Coa::find(Input::get('coaCredit'));
                $groupDepreciation->ACC1InternalIDCredit = $coa->ACC1InternalID;
                $groupDepreciation->ACC2InternalIDCredit = $coa->ACC2InternalID;
                $groupDepreciation->ACC3InternalIDCredit = $coa->ACC3InternalID;
                $groupDepreciation->ACC4InternalIDCredit = $coa->ACC4InternalID;
                $groupDepreciation->ACC5InternalIDCredit = $coa->ACC5InternalID;
                $groupDepreciation->ACC6InternalIDCredit = $coa->ACC6InternalID;
                $groupDepreciation->UserModified = Auth::user()->UserID;
                $groupDepreciation->Remark = Input::get('remark');
                $groupDepreciation->save();
                return View::make('coa.groupDepreciation')
                                ->withMessages('suksesUpdate')
                                ->withToogle('accounting')->withAktif('groupDepreciation');
            } else {
                return View::make('coa.groupDepreciation')
                                ->withMessages('accessDenied')
                                ->withToogle('accounting')->withAktif('groupDepreciation');
            }
        }
    }

    static function deleteGroupDepreciation() {
        $depreciation = DB::table('m_depreciation_header')->where('GroupDepreciationInternalID', Input::get('InternalID'))->first();
        //cek groupDepreciation ada di pakai atau tidak
        if (is_null($depreciation)) {
            //tidak ada maka boleh dihapus
            $groupDepreciation = GroupDepreciation::find(Input::get('InternalID'));
            if ($groupDepreciation->CompanyInternalID == Auth::user()->Company->InternalID) {
                $groupDepreciation->delete();
                return View::make('coa.groupDepreciation')
                                ->withMessages('suksesDelete')
                                ->withToogle('accounting')->withAktif('groupDepreciation');
            } else {
                return View::make('coa.groupDepreciation')
                                ->withMessages('accessDenied')
                                ->withToogle('accounting')->withAktif('groupDepreciation');
            }
        } else {
            //ada maka tidak dihapus
            return View::make('coa.groupDepreciation')
                            ->withMessages('gagalDelete')
                            ->withToogle('accounting')->withAktif('groupDepreciation');
        }
    }

    public function exportExcel() {
        Excel::create('Master_Depreciation_Group', function($excel) {
            $excel->sheet('Master_Depreciation_Group', function($sheet) {
                $sheet->mergeCells('B1:J1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Master Depreciation Group");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Depreciation Group ID");
                $sheet->setCellValueByColumnAndRow(3, 2, "Description");
                $sheet->setCellValueByColumnAndRow(4, 2, "Long Depreciation");
                $sheet->setCellValueByColumnAndRow(5, 2, "Assets Account");
                $sheet->setCellValueByColumnAndRow(6, 2, "Cost Account");
                $sheet->setCellValueByColumnAndRow(7, 2, "Record");
                $sheet->setCellValueByColumnAndRow(8, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(9, 2, "Remark");
                $row = 3;
                foreach (GroupDepreciation::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                    $coaD = GroupDepreciation::coaDebet($data->InternalID);
                    $coaC = GroupDepreciation::coaCredit($data->InternalID);
                    $data->coaIDDebet = $coaD[0]->COAInternalID;
                    $data->coaIDCredit = $coaC[0]->COAInternalID;
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, "`" . $data->GroupDepreciationID);
                    $sheet->setCellValueByColumnAndRow(3, $row, $data->Description);
                    $sheet->setCellValueByColumnAndRow(4, $row, $data->LongDepreciation . ' Year');
                    $sheet->setCellValueByColumnAndRow(5, $row, "`" . Coa::formatCoa($data->ACC1InternalIDDebet, $data->ACC2InternalIDDebet, $data->ACC3InternalIDDebet, $data->ACC4InternalIDDebet, $data->ACC5InternalIDDebet, $data->ACC6InternalIDDebet, 1) . ' ' . $coaD[0]->COAName);
                    $sheet->setCellValueByColumnAndRow(6, $row, "`" . Coa::formatCoa($data->ACC1InternalIDCredit, $data->ACC2InternalIDCredit, $data->ACC3InternalIDCredit, $data->ACC4InternalIDCredit, $data->ACC5InternalIDCredit, $data->ACC6InternalIDCredit, 1) . ' ' . $coaC[0]->COAName);
                    $sheet->setCellValueByColumnAndRow(7, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(8, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(9, $row, $data->Remark);
                    $row++;
                }

                if (GroupDepreciation::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() <= 0) {
                    $sheet->mergeCells('B3:J3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "No data available in table");
                    
                    $sheet->cells('B3:J3', function($cells) {
                        $cells->setAlignment('center');
                    });
                    
                    $sheet->setBorder('B3:J' . $row, 'thin');
                }


                $row--;
                $sheet->setBorder('B2:J' . $row, 'thin');
                $sheet->cells('B2:J2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:J' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
                $sheet->cells('E3:E' . $row, function($cells) {
                    $cells->setAlignment('right');
                });
            });
        })->export('xls');
    }

}
