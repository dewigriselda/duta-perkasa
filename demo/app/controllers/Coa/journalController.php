<?php

class JournalController extends BaseController {

    public function showJournal() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteJournal') {
                return $this->deleteJournal();
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('slipID') != '') {
            $data = JournalHeader::advancedSearch(Input::get('slipID'), Input::get('typeTransaction'), Input::get('startDate'), Input::get('endDate'));
            return View::make('coa.journalSearch')
                            ->withToogle('accounting')->withAktif('journal')
                            ->withData($data);
        }
        return View::make('coa.journal')
                        ->withToogle('accounting')->withAktif('journal');
    }

    public function deleteJournal() {
        //cek apakah ID ada di tabel t_journal_detail atau tidak
        //$journal = DB::table('t_journal_detail')->where('JournalInternalID', Input::get('InternalID'))->first();
        $journal = null;
        if (is_null($journal)) {
            //tidak ada maka data boleh dihapus
            $journal = JournalHeader::find(Input::get('InternalID'));
            if ($journal->CompanyInternalID == Auth::user()->Company->InternalID) {
                //cek journal termasuk transaksi atau tidak
                if ($journal->TransactionID == NULL || substr($journal->TransactionID, 0, 2) == 'PA') {
                    //hapus detil
                    $detilData = JournalHeader::find(Input::get('InternalID'))->journalDetail;
                    foreach ($detilData as $value) {
                        $detil = JournalDetail::find($value->InternalID);
                        $detil->delete();
                    }
                    //hapus journal
                    $journal = JournalHeader::find(Input::get('InternalID'));
                    $journal->delete();
                    $messages = 'suksesDelete';
                } else {
                    $messages = 'journalTransaksi';
                }
            } else {
                Session::flash('messages', $messages);
                $messages = 'accessDenied';
            }
        } else {
            //ada maka data tidak boleh dihapus
            $messages = 'gagalDelete';
        }
        $data = JournalHeader::advancedSearch(Input::get('slipID'), Input::get('typeTransaction'), Input::get('startDate'), Input::get('endDate'));
        return View::make('coa.journalSearch')
                        ->withToogle('accounting')->withAktif('journal')
                        ->withMessages($messages)
                        ->withData($data);
    }

    public function journalDetail($id) {
        $id = JournalHeader::getIdjournal($id);
        $header = JournalHeader::find($id);
        $detail = JournalHeader::find($id)->journalDetail()
                        ->orderBy(DB::raw('JournalIndex = 1000'))
                        ->orderBy('JournalTransactionID', 'asc')->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            return View::make('coa.journalDetail')
                            ->withToogle('accounting')->withAktif('journal')
                            ->withHeader($header)
                            ->withDetail($detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showJournal');
        }
    }

    public function journalNew($jenis) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            return $this->insertJournal($jenis);
        }
        $cari = $this->createTextCari($jenis, 0);
        $tipe = $this->getTipe($jenis);
        return View::make('coa.journalNew')
                        ->withToogle('accounting')->withAktif('journal')
                        ->withJenis($jenis)
                        ->withTipe($tipe)
                        ->withJournal($cari);
    }

    public function journalNewMemorial() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            return $this->insertJournalMemorial();
        }
        $cari = $this->createTextCari('Memorial', 0);
        $tipe = $this->getTipe('Memorial');
        return View::make('coa.journalNewMemorial')
                        ->withToogle('accounting')->withAktif('journal')
                        ->withJenis('Memorial')
                        ->withTipe($tipe)
                        ->withJournal($cari);
    }

    public function journalUpdate($id) {
        $id = JournalHeader::getIdjournal($id);
        $header = JournalHeader::find($id);
        if ($header->TransactionID == NULL || substr($header->TransactionID, 0, 2) == 'PA') {
            if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
                if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                    return $this->updateJournal($id);
                }
                $detail = JournalHeader::find($id)->journalDetail()
                                ->orderBy(DB::raw('JournalIndex = 1000'))
                                ->orderBy('JournalTransactionID', 'asc')->get();
                $cari = $this->createTextCari($header->JournalType, 0);
                $tipe = $this->getTipe($header->JournalType);
                return View::make('coa.journalUpdate')
                                ->withToogle('accounting')->withAktif('journal')
                                ->withHeader($header)
                                ->withDetail($detail)
                                ->withTipe($tipe)
                                ->withJournal($cari);
            } else {
                $messages = 'accessDenied';
                Session::flash('messages', $messages);
                return Redirect::Route('showJournal');
            }
        } else {
            $messages = 'journalTransaksi';
            Session::flash('messages', $messages);
            return Redirect::Route('showJournal');
        }
    }

    public function insertJournal($jenis) {
        //rule
        $rule = array(
            'date' => 'required',
            'from' => 'required|max:1000',
            'journalNotes' => 'required|max:2000',
            'slip' => 'required',
            'department' => 'required',
            'coa5' => 'required',
            'remark' => 'required|max:1000',
            'currency' => 'required',
            'rate' => 'required',
            'coa' => 'required',
            'notes' => 'required'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalInsert';
            $error = $validator->messages();
        } else {
            //valid
            $tampTransactionID = '';
            //insert header
            $header = new JournalHeader;
            $slip = explode('---;---', Input::get('slip'));
            $cari = $this->createTextCari($jenis, 1);
            $date = explode('-', Input::get('date'));
            $yearDigit = substr($date[2], 2);
            $cari .= '-' . $date[1] . $yearDigit;
            $tipe = $this->getTipe($jenis);
            $tampSlipID = Slip::find($slip[0]);
            $header->JournalID = JournalHeader::getNextIDJournal($cari . '-' . $tampSlipID->SlipID . '-');
            $header->JournalDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $header->JournalType = Input::get('journalType');
            $header->JournalFrom = Input::get('from');
            $header->Notes = Input::get('journalNotes');
            $header->DepartmentInternalID = Input::get('department');
            $header->SlipInternalID = $slip[0];
            $header->TransactionID = NULL;
            $header->ACC5InternalID = Input::get('coa5');
            $header->Lock = '0';
            $header->Check = '0';
            $header->Flag = '0';
            $header->UserRecord = Auth::user()->UserID;
            $header->CompanyInternalID = Auth::user()->Company->InternalID;
            $header->UserModified = '0';
            $header->Remark = Input::get('remark');
            $header->save();

            //insert detail
            $tampKredit = 0;
            $tampDebet = 0;
            $tampRate = 0;
            $tampNomor = 0;
            $countAdaJournalSesuai = 0;
            for ($a = 0; $a < count(Input::get('coa')); $a++) {
                $debetValue = str_replace(',', '', Input::get('Debet_value')[$a]);
                $kreditValue = str_replace(',', '', Input::get('Kredit_value')[$a]);
                if ($debetValue != 0 || $kreditValue != 0) {
                    $detail = new JournalDetail();
                    $detail->JournalInternalID = $header->InternalID;
                    $detail->JournalIndex = $a + 1;
                    $detail->JournalNotes = Input::get('notes')[$a];
                    $detail->JournalDebetMU = $debetValue;
                    $detail->JournalCreditMU = $kreditValue;
                    $currency = explode('---;---', Input::get('currency'));
                    $detail->CurrencyInternalID = $currency[0];
                    $detail->CurrencyRate = str_replace(',', '', Input::get('rate'));
                    $detail->JournalDebet = $debetValue * str_replace(',', '', Input::get('rate'));
                    $tampDebet += $debetValue;
                    $detail->JournalCredit = $kreditValue * str_replace(',', '', Input::get('rate'));
                    $tampKredit += $kreditValue;
                    $tampRate = str_replace(',', '', Input::get('rate'));
                    if (Input::get('JournalTransactionID')[$a] == '-1') {
                        $detail->JournalTransactionID = NULL;
                    } else {
                        $detail->JournalTransactionID = Input::get('JournalTransactionID')[$a];
                        $tampTransactionID .= ', ' . Input::get('JournalTransactionID')[$a];
                    }
                    $coa = Coa::find(Input::get('coa')[$a]);
                    $detail->ACC1InternalID = $coa->ACC1InternalID;
                    $detail->ACC2InternalID = $coa->ACC2InternalID;
                    $detail->ACC3InternalID = $coa->ACC3InternalID;
                    $detail->ACC4InternalID = $coa->ACC4InternalID;
                    $detail->ACC5InternalID = $coa->ACC5InternalID;
                    $detail->ACC6InternalID = $coa->ACC6InternalID;
                    $detail->COAName = $coa->COAName;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = '0';
                    $detail->save();
                    $tampNomor = $a;
                    $countAdaJournalSesuai++;
                }
            }
            if ($tampTransactionID != '') {
                $tampTransactionID = substr($tampTransactionID, 1);
                $header = JournalHeader::find($header->InternalID);
                $header->TransactionID = 'PA ' . $tampTransactionID;
                $header->save();
            }
            if ($tampKredit >= $tampDebet) {
                $tampDebet = $tampKredit - $tampDebet;
                $tampKredit = 0;
            } else {
                $tampKredit = $tampDebet - $tampKredit;
                $tampDebet = 0;
            }
            if ($countAdaJournalSesuai != 0) {
                $detail = new JournalDetail();
                $detail->JournalInternalID = $header->InternalID;
                $detail->JournalIndex = 1000;
                $detail->JournalNotes = 'Journal Auto ' . Input::get('journalNotes');
                $detail->JournalDebetMU = $tampDebet;
                $detail->JournalCreditMU = $tampKredit;
                $currency = explode('---;---', Input::get('currency'));
                $detail->CurrencyInternalID = $currency[0];
                $detail->CurrencyRate = str_replace(',', '', Input::get('rate'));
                $detail->JournalDebet = $tampDebet * str_replace(',', '', Input::get('rate'));
                $detail->JournalCredit = $tampKredit * str_replace(',', '', Input::get('rate'));
                $detail->JournalTransactionID = NULL;
                $coa = Slip::coa($slip[0]);
                $coa = Coa::find($coa[0]->COAInternalID);
                $detail->ACC1InternalID = $coa->ACC1InternalID;
                $detail->ACC2InternalID = $coa->ACC2InternalID;
                $detail->ACC3InternalID = $coa->ACC3InternalID;
                $detail->ACC4InternalID = $coa->ACC4InternalID;
                $detail->ACC5InternalID = $coa->ACC5InternalID;
                $detail->ACC6InternalID = $coa->ACC6InternalID;
                $detail->COAName = $coa->COAName;
                $detail->UserRecord = Auth::user()->UserID;
                $detail->UserModified = '0';
                $detail->save();
            }

            $messages = 'suksesInsert';
            $error = '';
        }
        $cari = $this->createTextCari($jenis, 0);
        $tipe = $this->getTipe($jenis);
        return View::make('coa.journalNew')
                        ->withToogle('accounting')->withAktif('journal')
                        ->withJenis($jenis)
                        ->withTipe($tipe)
                        ->withJournal($cari)
                        ->withError($error)
                        ->withMessages($messages);
    }

    public function updateJournal($id) {
        //tipe
        $headerUpdate = JournalHeader::find($id);
        $detailUpdate = JournalHeader::find($id)->journalDetail()->orderBy('JournalIndex', 'asc')->get();
        $cari = $this->createTextCari($headerUpdate->JournalType, 0);
        $tipe = $this->getTipe($headerUpdate->JournalType);

        //rule
        $rule = array(
            'from' => 'required|max:1000',
            'journalNotes' => 'required|max:2000',
            'department' => 'required',
            'coa5' => 'required',
            'remark' => 'required|max:1000',
            'currency' => 'required',
            'rate' => 'required',
            'coa' => 'required',
            'notes' => 'required'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalUpdate';
            $error = $validator->messages();
        } else {
            $tampTransactionID = '';

            $header = JournalHeader::find(Input::get('JournalInternalID'));
            $header->JournalFrom = Input::get('from');
            $header->Notes = Input::get('journalNotes');
            $header->DepartmentInternalID = Input::get('department');
            $header->TransactionID = NULL;
            $header->ACC5InternalID = Input::get('coa5');
            $header->UserModified = Auth::user()->UserID;
            $header->Remark = Input::get('remark');
            $header->save();

            //delete journal detail -- nantinya insert ulang
            JournalDetail::where('JournalInternalID', '=', Input::get('JournalInternalID'))->delete();

            //insert ulang
            $tampKredit = 0;
            $tampDebet = 0;
            $tampRate = 0;
            $tampNomor = 0;
            $countAdaJournalSesuai = 0;
            for ($a = 0; $a < count(Input::get('coa')); $a++) {
                $debetValue = str_replace(',', '', Input::get('Debet_value')[$a]);
                $kreditValue = str_replace(',', '', Input::get('Kredit_value')[$a]);
                if ($debetValue != 0 || $kreditValue != 0) {
                    $detail = new JournalDetail();
                    $detail->JournalInternalID = Input::get('JournalInternalID');
                    $detail->JournalIndex = $a + 1;
                    $detail->JournalNotes = Input::get('notes')[$a];
                    $detail->JournalDebetMU = $debetValue;
                    $detail->JournalCreditMU = $kreditValue;
                    $currency = explode('---;---', Input::get('currency'));
                    $detail->CurrencyInternalID = $currency[0];
                    $detail->CurrencyRate = str_replace(',', '', Input::get('rate'));
                    $detail->JournalDebet = $debetValue * str_replace(',', '', Input::get('rate'));
                    $tampDebet += $debetValue;
                    $detail->JournalCredit = $kreditValue * str_replace(',', '', Input::get('rate'));
                    $tampKredit += $kreditValue;
                    $tampRate = str_replace(',', '', Input::get('rate'));
                    if (Input::get('JournalTransactionID')[$a] == '-1') {
                        $detail->JournalTransactionID = NULL;
                    } else {
                        $detail->JournalTransactionID = Input::get('JournalTransactionID')[$a];
                        $tampTransactionID .= ', ' . Input::get('JournalTransactionID')[$a];
                    }
                    $coa = Coa::find(Input::get('coa')[$a]);
                    $detail->ACC1InternalID = $coa->ACC1InternalID;
                    $detail->ACC2InternalID = $coa->ACC2InternalID;
                    $detail->ACC3InternalID = $coa->ACC3InternalID;
                    $detail->ACC4InternalID = $coa->ACC4InternalID;
                    $detail->ACC5InternalID = $coa->ACC5InternalID;
                    $detail->ACC6InternalID = $coa->ACC6InternalID;
                    $detail->COAName = $coa->COAName;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = Auth::user()->UserID;
                    $detail->save();
                    $tampNomor = $a;
                    $countAdaJournalSesuai++;
                }
            }
            if ($tampTransactionID != '') {
                $tampTransactionID = substr($tampTransactionID, 1);
                $header = JournalHeader::find(Input::get('JournalInternalID'));
                $header->TransactionID = 'PA ' . $tampTransactionID;
                $header->save();
            }
            if ($tampKredit >= $tampDebet) {
                $tampDebet = $tampKredit - $tampDebet;
                $tampKredit = 0;
            } else {
                $tampKredit = $tampDebet - $tampKredit;
                $tampDebet = 0;
            }
            if ($tipe == 'Memo') {
                $countAdaJournalSesuai = 0;
            }
            if ($countAdaJournalSesuai != 0) {
                $detail = new JournalDetail();
                $detail->JournalInternalID = Input::get('JournalInternalID');
                $detail->JournalIndex = 1000;
                $detail->JournalNotes = 'Journal Auto ' . Input::get('journalNotes');
                $detail->JournalDebetMU = $tampDebet;
                $detail->JournalCreditMU = $tampKredit;
                $currency = explode('---;---', Input::get('currency'));
                $detail->CurrencyInternalID = $currency[0];
                $detail->CurrencyRate = str_replace(',', '', Input::get('rate'));
                $detail->JournalDebet = $tampDebet * str_replace(',', '', Input::get('rate'));
                $detail->JournalCredit = $tampKredit * str_replace(',', '', Input::get('rate'));
                $detail->JournalTransactionID = NULL;
                $slip = explode('---;---', Input::get('slip'));
                $coa = Slip::coa($slip[0]);
                $coa = Coa::find($coa[0]->COAInternalID);
                $detail->ACC1InternalID = $coa->ACC1InternalID;
                $detail->ACC2InternalID = $coa->ACC2InternalID;
                $detail->ACC3InternalID = $coa->ACC3InternalID;
                $detail->ACC4InternalID = $coa->ACC4InternalID;
                $detail->ACC5InternalID = $coa->ACC5InternalID;
                $detail->ACC6InternalID = $coa->ACC6InternalID;
                $detail->COAName = $coa->COAName;
                $detail->UserRecord = Auth::user()->UserID;
                $detail->UserModified = Auth::user()->UserID;
                $detail->save();
            }

            $messages = 'suksesUpdate';
            $error = '';
        }

        //tipe
        $header = JournalHeader::find($id);
        $detail = JournalHeader::find($id)->journalDetail()
                        ->orderBy(DB::raw('JournalIndex = 1000'))
                        ->orderBy('JournalTransactionID', 'asc')->get();
        $cari = $this->createTextCari($header->JournalType, 0);
        $tipe = $this->getTipe($header->JournalType);
        return View::make('coa.journalUpdate')
                        ->withToogle('accounting')->withAktif('journal')
                        ->withHeader($header)
                        ->withDetail($detail)
                        ->withTipe($tipe)
                        ->withJournal($cari)
                        ->withError($error)
                        ->withMessages($messages);
    }

    public function insertJournalMemorial() {
        //rule
        $rule = array(
            'date' => 'required',
            'from' => 'required|max:1000',
            'journalNotes' => 'required|max:2000',
            'department' => 'required',
            'coa5' => 'required',
            'remark' => 'required|max:1000',
            'currency' => 'required',
            'rate' => 'required',
            'coa' => 'required',
            'notes' => 'required'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalInsert';
            $error = $validator->messages();
        } else {
            //valid
            //insert header
            $header = new JournalHeader;
            $cari = $this->createTextCari('Memorial', 1);
            $date = explode('-', Input::get('date'));
            $yearDigit = substr($date[2], 2);
            $cari .= '-' . $date[1] . $yearDigit;
            $tipe = $this->getTipe('Memorial');
            $header->JournalID = JournalHeader::getNextIDJournal($cari . '-');
            $header->JournalDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $header->JournalType = Input::get('journalType');
            $header->JournalFrom = Input::get('from');
            $header->Notes = Input::get('journalNotes');
            $header->DepartmentInternalID = Input::get('department');
            $header->SlipInternalID = Null;
            $header->TransactionID = NULL;
            $header->ACC5InternalID = Input::get('coa5');
            $header->Lock = '0';
            $header->Check = '0';
            $header->Flag = '0';
            $header->UserRecord = Auth::user()->UserID;
            $header->CompanyInternalID = Auth::user()->Company->InternalID;
            $header->UserModified = '0';
            $header->Remark = Input::get('remark');
            $header->save();

            //insert detail
            $tampKredit = 0;
            $tampDebet = 0;
            $tampRate = 0;
            $tampNomor = 0;
            for ($a = 0; $a < count(Input::get('coa')); $a++) {
                $debetValue = str_replace(',', '', Input::get('Debet_value')[$a]);
                $kreditValue = str_replace(',', '', Input::get('Kredit_value')[$a]);
                if ($debetValue != 0 || $kreditValue != 0) {
                    $detail = new JournalDetail();
                    $detail->JournalInternalID = $header->InternalID;
                    $detail->JournalIndex = $a + 1;
                    $detail->JournalNotes = Input::get('notes')[$a];
                    $detail->JournalDebetMU = $debetValue;
                    $detail->JournalCreditMU = $kreditValue;
                    $currency = explode('---;---', Input::get('currency'));
                    $detail->CurrencyInternalID = $currency[0];
                    $detail->CurrencyRate = str_replace(',', '', Input::get('rate'));
                    $detail->JournalDebet = $debetValue * str_replace(',', '', Input::get('rate'));
                    $tampDebet += $debetValue;
                    $detail->JournalCredit = $kreditValue * str_replace(',', '', Input::get('rate'));
                    $tampKredit += $kreditValue;
                    $tampRate = str_replace(',', '', Input::get('rate'));
                    $detail->JournalTransactionID = NULL;
                    $coa = Coa::find(Input::get('coa')[$a]);
                    $detail->ACC1InternalID = $coa->ACC1InternalID;
                    $detail->ACC2InternalID = $coa->ACC2InternalID;
                    $detail->ACC3InternalID = $coa->ACC3InternalID;
                    $detail->ACC4InternalID = $coa->ACC4InternalID;
                    $detail->ACC5InternalID = $coa->ACC5InternalID;
                    $detail->ACC6InternalID = $coa->ACC6InternalID;
                    $detail->COAName = $coa->COAName;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = '0';
                    $detail->save();
                    $tampNomor = $a;
                }
            }
            $messages = 'suksesInsert';
            $error = '';
        }
        $cari = $this->createTextCari('Memorial', 0);
        $tipe = $this->getTipe('Memorial');
        return View::make('coa.journalNewMemorial')
                        ->withToogle('accounting')->withAktif('journal')
                        ->withJenis('Memorial')
                        ->withTipe($tipe)
                        ->withJournal($cari)
                        ->withError($error)
                        ->withMessages($messages);
    }

    function createTextCari($jenis, $tipe) {
        if ($jenis == 'Bank In' || $jenis == 'Cash In') {
            if ($jenis == 'Bank In') {
                $cari = 'BI';
            } else {
                $cari = 'CI';
            }
        } else if ($jenis == 'Bank Out' || $jenis == 'Cash Out') {
            if ($jenis == 'Bank Out') {
                $cari = 'BO';
            } else {
                $cari = 'CO';
            }
        } else {
            $cari = 'ME';
        }
        if ($tipe == 0) {
            $cari .= '-' . date('m') . date('y');
        }
        return $cari;
    }

    function getTipe($jenis) {
        if ($jenis == 'Bank In' || $jenis == 'Cash In') {
            $tipe = 'In';
        } else if ($jenis == 'Bank Out' || $jenis == 'Cash Out') {
            $tipe = 'Out';
        } else {
            $tipe = 'Memo';
        }
        return $tipe;
    }

    public function formatCariIDJournalMemorial() {
        $date = explode('-', Input::get('date'));
        $tipe = Input::get('tipe');
        $cari = $this->createTextCari($tipe, 1);
        $yearDigit = substr($date[2], 2);
        $cari .= '-' . $date[1] . $yearDigit;
        echo JournalHeader::getNextIDJournal($cari . '-');
    }

    public function formatCariIDJournal() {
        $date = explode('-', Input::get('date'));
        $tipe = Input::get('tipe');
        $cari = $this->createTextCari($tipe, 1);
        $yearDigit = substr($date[2], 2);
        $cari .= '-' . $date[1] . $yearDigit;
        $tampSlip = explode('---;---', Input::get('slip'));
        $slipSelected = $tampSlip[0];
        foreach (Slip::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $slip) {
            ?>
            <option <?php if ($slip->InternalID == $slipSelected) echo 'selected' ?> value="<?php echo $slip->InternalID . '---;---' . JournalHeader::getNextIDJournal($cari . '-' . $slip->SlipID . '-'); ?>">
                <?php echo $slip->SlipID . ' ' . $slip->SlipName ?>
            </option>
            <?php
        }
    }

    function journalPrint($id) {
        $id = JournalHeader::getIdjournal($id);
        $header = JournalHeader::find($id);
        $detail = JournalHeader::find($id)->journalDetail()->get();
        if ($header->Lock == 1) {
            $status = 'Lock';
        } else if ($header->Check == 1) {
            $status = 'Lock and Check';
        } else {
            $status = 'Not lock';
        }
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            if ($header->JournalType == "Memorial") {
                $slip = "-";
                $slipText = "-";
            } else {
                $slip = $header->Slip;
                $slipText = $slip->SlipID . ' ' . $slip->SlipName;
            }
            $department = $header->Department;
            $coa5 = $header->Coa5;
            $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 420px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Journal</h5>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 18px; width: 100%;">
                            <table>
                            <tr>
                            <td width="425px">
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Journal ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $header->JournalID . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->JournalDate)) . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Type</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $header->JournalType . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">From</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $header->JournalFrom . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Status</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $status . '</td>
                                 </tr>
                                </table>
                                </td>
                                <td>
                                <table>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Department</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $department->DepartmentName . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Slip</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $slipText . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Transaction</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . ($header->TransactionID != '' ? $header->TransactionID : '-') . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Coa5</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . ($coa5->ACC5ID == '0' ? : $coa5->ACC5ID . ' ' . $coa5->ACC5Name) . '</td>
                                     </tr>
                                </table></td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Account</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Debet MU</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Credit MU</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Currency</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Rate</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Debet</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Credit</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            $totalDebet = 0;
            $totalCredit = 0;
            if (count($detail) > 0) {
                foreach ($detail as $data) {
                    $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . Coa::formatCoa($data->ACC1InternalID, $data->ACC2InternalID, $data->ACC3InternalID, $data->ACC4InternalID, $data->ACC5InternalID, $data->ACC6InternalID, 1) . ' ' . $data->COAName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->JournalDebetMU, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->JournalCreditMU, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->Currency->CurrencyName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->CurrencyRate, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->JournalDebet, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->JournalCredit, '2', '.', ',') . '</td>
                            </tr>';
                    $totalDebet += $data->JournalDebet;
                    $totalCredit += $data->JournalCredit;
                }
            } else {
                $html.= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;  text-align: center">There is no account journal registered in this journal.</td>
                        </tr>';
            }
            $html.= '<tr>
                                <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;"><hr></td>
                    </tr>
                    <tr>
                                <td colspan="4" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;"></td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: center">Total </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right">' . number_format($totalDebet, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right">' . number_format($totalCredit, '2', '.', ',') . '</td>
                    </tr>
                            </tbody>
                            </table>
                    </div>
                </body>
            </html>';
            return PDF ::load($html, 'A4', 'portrait')->show();
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showSales');
        }
    }

    public function getReceivable() {
        $detail = SalesHeader::getSalesReceivable();
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 420px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Receivable</h5>  
                            <table class="tableBorder" width="100%"  style="margin-top: 3px; clear: both;  top: 18px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Customer</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Phone</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Due Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Currency</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Balance</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
        if (count($detail) > 0) {
            foreach ($detail as $data) {
                $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->coa6 . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . Coa6::find($data->ACC6InternalID)->Phone . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->ID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-m-Y", strtotime($data->Date)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-m-Y", strtotime("+" . $data->LongTerm . " day", strtotime($data->Date))) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->CurrencyName . ' (' . number_format($data->CurrencyRate, '2', '.', ',') . ')</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->GrandTotal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->GrandTotal - JournalDetail::where('JournalTransactionID', $data->ID)->sum('JournalCreditMU'), '2', '.', ',') . '</td>
                            </tr>';
            }
        } else {
            $html.= '<tr>
                            <td colspan="8" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;  text-align: center">There Receivable.</td>
                        </tr>';
        }
        $html.= '               </tbody>
                            </table>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('receivable_report');
    }

    public function getPayable() {
        $detail = PurchaseHeader::getPurchasePayable();
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 420px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Payable</h5>  
                            <table class="tableBorder" width="100%"  style="margin-top: 3px; clear: both;  top: 18px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Supplier</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Phone</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Purchase ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Due Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Currency</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Balance</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
        if (count($detail) > 0) {
            foreach ($detail as $data) {
                $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->coa6 . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . Coa6::find($data->ACC6InternalID)->Phone . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->ID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-m-Y", strtotime($data->Date)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-m-Y", strtotime("+" . $data->LongTerm . " day", strtotime($data->Date))) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->CurrencyName . ' (' . number_format($data->CurrencyRate, '2', '.', ',') . ')</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->GrandTotal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->GrandTotal - JournalDetail::where('JournalTransactionID', $data->ID)->sum('JournalDebetMU'), '2', '.', ',') . '</td>
                            </tr>';
            }
        } else {
            $html.= '<tr>
                            <td colspan="8" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;  text-align: center">There Receivable.</td>
                        </tr>';
        }
        $html.= '               </tbody>
                            </table>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('payable_report');
    }

    static function journalDataBackup($data) {
        $explode = explode('---;---', $data);
        $slip = $explode[0];
        $jenis = $explode[1];
        $start = $explode[2];
        $end = $explode[3];

        $where = '';
        if ($slip != '-1' && $slip != '') {
            $where .= 'SlipInternalID = "' . $slip . '" ';
        }
        if ($jenis != '-1' && $jenis != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'JournalType = "' . $jenis . '" ';
        }
        if ($start != '' && $end != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $start = explode('-', $start);
            $end = explode('-', $end);
            $startSearch = $start[2] . '-' . $start[1] . '-' . $start[0];
            $endSearch = $end[2] . '-' . $end[1] . '-' . $end[0];
            $where .= 'JournalDate between "' . $startSearch . ' 00:00:00" AND "' . $endSearch . ' 23:59:59"';
        }

        $table = 't_journal_header';
        $primaryKey = 't_journal_header`.`InternalID';
        $columns = array(
            array('db' => 'JournalID', 'dt' => 0),
            array('db' => 'JournalType', 'dt' => 1),
            array(
                'db' => 'JournalDate',
                'dt' => 2,
                'formatter' => function( $d, $row ) {
                    return date("d-m-Y", strtotime($d));
                }
            ),
            array('db' => 'DepartmentName', 'dt' => 3),
            array('db' => 'SlipName', 'dt' => 4),
            array('db' => 'ACC5Name', 'dt' => 5),
            array('db' => 'TransactionID', 'dt' => 6),
            array('db' => 't_journal_header`.`InternalID', 'dt' => 7, 'formatter' => function( $d, $row ) {
                    $data = JournalHeader::find($d);
                    $action = '<td class="text-center">
                                    <a href="' . Route('journalDetail', $data->JournalID) . '">
                                        <button id="btn-' . $data->JournalID . '-detail"
                                                class="btn btn-pure-xs btn-xs btn-detail">
                                            <span class="glyphicon glyphicon-zoom-in"></span>
                                        </button>
                                    </a>';
                    if ($data->TransactionID == 'No Transaction' || substr($data->TransactionID, 0, 2) == 'PA' || $data->TransactionID == '') {
                        $action.='<a href="' . Route('journalUpdate', $data->JournalID) . '">
                                        <button id="btn-' . $data->JournalID . '-update"
                                                class="btn btn-pure-xs btn-xs btn-edit">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </button>
                                    </a>';
                        $action.='<button data-target="#m_journalDelete" data-internal="' . $data->InternalID . '"  data-toggle="modal" role="dialog"
                                           onclick="deleteAttach(this)" data-id="' . $data->JournalID . '" data-name=' . $data->JournalID . ' class="btn btn-pure-xs btn-xs btn-delete">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>';
                    } else {
                        $action.='<button disabled class="btn btn-pure-xs btn-xs btn-edit"><span class="glyphicon glyphicon-edit"></span></button>';
                        $action.='<button disabled class="btn btn-pure-xs btn-xs btn-delete"><span class="glyphicon glyphicon-trash"></span></button>';
                    }
                    return $action;
                },
                'field' => 't_journal_header`.`InternalID')
        );

        $sql_details = getConnection();

        require('ssp.class.php');
        $ID_CLIENT_VALUE = Auth::user()->CompanyInternalID;
        if ($where != '') {
            $extraCondition = $where . ' AND ' . 't_journal_header.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        } else {
            $extraCondition = 't_journal_header.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        }
        $join = ' INNER JOIN m_department d on d.InternalID = t_journal_header.DepartmentInternalID '
                . 'LEFT JOIN m_slip s on s.InternalID = t_journal_header.SlipInternalID '
                . 'INNER JOIN m_coa5 c on c.InternalID = t_journal_header.ACC5InternalID ';

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $extraCondition, $join));
    }

}
