<?php

class ReportAccountingController extends BaseController {

    public function showReportAccounting() {
        $yearMin = JournalHeader::getYearMin();
        $yearMax = date("Y");
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'balance') {
                return $this->balanceReport();
            }
            if (Input::get('jenis') == 'profitLoss') {
                return $this->profitReport();
            }
            if (Input::get('jenis') == 'general') {
                return $this->generalReport();
            }
        }
        return View::make('coa.report')
                        ->withToogle('accounting')->withAktif('reportAccounting')
                        ->withYearmin($yearMin)
                        ->withYearmax($yearMax);
    }

    public function balanceReport() {
        $bulan = Input::get('month');
        $tahun = Input::get('year');
        $zero = Input::get('ZeroBalance');
        $date = date("Y-m-d", strtotime("+1 month", strtotime(date($tahun . '-' . str_pad($bulan, 2, '0', STR_PAD_LEFT) . '-01'))));
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:80%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Balance Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("M", strtotime($tahun . '-' . $bulan . '-01')) . ' ' . $tahun . '</span><br><br>';
        $header1 = Coa::where('CompanyInternalID', Auth::user()->CompanyInternalID)
                        ->where('Flag', 0)->select('Header1')
                        ->distinct('Header1')->get();
        foreach ($header1 as $dataHeader1) { //Loop Header 1    
            $tamp = '';
            $count2 = 0;
            $total1 = 0;
            $tamp.= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">' . $dataHeader1->Header1 . '</span><br><br>';
            $header2 = Coa::where('CompanyInternalID', Auth::user()->CompanyInternalID)
                            ->where('Flag', 0)->where('Header1', $dataHeader1->Header1)
                            ->select('Header2')
                            ->distinct('Header2')->get();
            foreach ($header2 as $dataHeader2) { //Loop Header 2 berdasarkan Header 1
                $tamp2 = '';
                $count3 = 0;
                $tamp2.= '<span style="padding-left:23px;font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">' . $dataHeader2->Header2 . '</span>';
                $total2 = 0;
                $header3 = Coa::where('CompanyInternalID', Auth::user()->CompanyInternalID)
                                ->where('Flag', 0)->where('Header1', $dataHeader1->Header1)
                                ->where('Header2', $dataHeader2->Header2)->select('Header3')
                                ->distinct('Header3')->orderBy('Header3')->get();
                $tamp2.= '<table class="tableBorder"  style="width: 95%; margin-top: 18px; clear: both; position: relative; left: 3%">';
                foreach ($header3 as $dataHeader3) { //Loop Header 3 berdasarkan Header 1 & 2
                    $debet = Coa::getJournalDebetValue($dataHeader1->Header1, $dataHeader2->Header2, $dataHeader3->Header3, $date);
                    $credit = Coa::getJournalCreditValue($dataHeader1->Header1, $dataHeader2->Header2, $dataHeader3->Header3, $date);
                    $sumInitialValue = Coa::where('CompanyInternalID', Auth::user()->CompanyInternalID)->where('Header1', $dataHeader1->Header1)
                                    ->where('Flag', 0)->where('Header2', $dataHeader2->Header2)->where('Header3', $dataHeader3->Header3)->sum('InitialBalance');
                    if (($debet != 0 || $credit != 0 || $sumInitialValue != 0) || $zero == 1) {
                        $total3 = $debet - $credit + $sumInitialValue;
                        $text = number_format($total3, '2', '.', ',');
                        if ($total3 < 0) {
                            $text = '(' . number_format($total3 * -1, '2', '.', ',') . ')';
                        }
                        //text -> Total Header 3
                        $total2 += $total3;
                        $tamp2.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $dataHeader3->Header3 . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $text . '</td>
                            </tr>';
                        $count3++;
                    }
                }
                if ($count3 != 0) {
                    $tamp2.= '<tr>
                                <td style="font-family: helvetica,sans-serif; margin: 5px !important;" colspan="2"><hr></td>
                            </tr>';
                    $tamp2.='</table>';
                    $text = number_format($total2, '2', '.', ',');
                    if ($total2 < 0) {
                        $text = '(' . number_format($total2 * -1, '2', '.', ',') . ')';
                    }
                    $tamp2.= '<table class="tableBorder"  style="width: 95%; margin-bottom: 18px; clear: both; position: relative; left: 3%">';
                    $tamp2.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="80%">Total ' . $dataHeader2->Header2 . ' : </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . $text . '</td>
                            </tr>';
                    $tamp2.='</table>';
                    $tamp.= $tamp2;
                    $count2++;
                    //text -> Total Header 2
                    $total1 += $total2;
                }
            }
            if ($count2 != 0) {
                $text = number_format($total1, '2', '.', ',');
                if ($total1 < 0) {
                    $text = '(' . number_format($total1 * -1, '2', '.', ',') . ')';
                }
                $html.= $tamp;
                $html.= '<table width="698px" style="clear: both; padding-left:-1px;">';
                $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;margin: 5px !important;" colspan="2"><hr style="height:0.1px;"></td>
                            </tr>';
                $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="80%">Total ' . $dataHeader1->Header1 . ' : </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . $text . '</td>
                            </tr>';
                $html.='</table>';
            }
        }
        $html.='</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('balance_report');
    }

    public function profitReport() {
        $bulan = Input::get('month');
        $tahun = Input::get('year');
        $zero = Input::get('ZeroProfitLoss');
        $date = date("Y-m-d", strtotime("+1 month", strtotime(date($tahun . '-' . str_pad($bulan, 2, '0', STR_PAD_LEFT) . '-01'))));
        $html = '
            <html>
                <head>
                    <style>
                        table{
                            border-spacing: 0;
                        }      
                        th{
                            padding: 3px;
                             border-spacing: 0;
                            border-bottom: 0px solid #ddd;
                            text-align: center;
                        }
                        td{
                            padding: 3px;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:80%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Profit and Loss Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("M", strtotime($tahun . '-' . $bulan . '-01')) . ' ' . $tahun . '</span><br><br>';
        $totalSemua = 0;
        $header1 = Coa::where('CompanyInternalID', Auth::user()->CompanyInternalID)
                        ->where('Flag', 1)->select('Header1')
                        ->distinct('Header1')->get();
        foreach ($header1 as $dataHeader1) { //Loop Header 1
            $tamp = '';
            $count2 = 0;
            $total1 = 0;
            $tamp.= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">' . $dataHeader1->Header1 . '</span><br><br>';
            $header2 = Coa::where('CompanyInternalID', Auth::user()->CompanyInternalID)
                            ->where('Flag', 1)->where('Header1', $dataHeader1->Header1)
                            ->select('Header2')
                            ->distinct('Header2')->get();
            foreach ($header2 as $dataHeader2) { //Loop Header 2 berdasarkan Header 1
                $tamp2 = '';
                $count3 = 0;
                $tamp2.= '<span style="padding-left:23px;font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">' . $dataHeader2->Header2 . '</span>';
                $total2 = 0;
                $header3 = Coa::where('CompanyInternalID', Auth::user()->CompanyInternalID)
                                ->where('Flag', 1)->where('Header1', $dataHeader1->Header1)
                                ->where('Header2', $dataHeader2->Header2)->select('Header3')
                                ->distinct('Header3')->orderBy('Header3')->get();
                $tamp2.= '<table width="698px"  style="margin-top: 18px; clear: both;  top: 78px; padding-left:48px;">';
                foreach ($header3 as $dataHeader3) { //Loop Header 3 berdasarkan Header 1 & 2
                    $debet = Coa::getJournalDebetValueProfit($dataHeader1->Header1, $dataHeader2->Header2, $dataHeader3->Header3, $date);
                    $credit = Coa::getJournalCreditValueProfit($dataHeader1->Header1, $dataHeader2->Header2, $dataHeader3->Header3, $date);
                    $sumInitialValue = Coa::where('CompanyInternalID', Auth::user()->CompanyInternalID)->where('Header1', $dataHeader1->Header1)
                                    ->where('Flag', 1)->where('Header2', $dataHeader2->Header2)->where('Header3', $dataHeader3->Header3)->sum('InitialBalance');
                    if (($debet != 0 || $credit != 0 || $sumInitialValue != 0) || $zero == 1) {
                        $total3 = $credit - $debet + $sumInitialValue;
                        $text = number_format($total3, '2', '.', ',');
                        if ($total3 < 0) {
                            $text = '(' . number_format($total3 * -1, '2', '.', ',') . ')';
                        }
                        //text -> Total Header 3
                        $total2 += $total3;
                        $tamp2.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $dataHeader3->Header3 . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;" colspan="2"></td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $text . '</td>
                            </tr>';
                        $count3++;
                    }
                }
                if ($count3 != 0) {
                    $tamp2.= '<tr>
                                <td style="font-family: helvetica,sans-serif; margin: 5px !important;" colspan="4"><hr></td>
                            </tr>';
                    $tamp2.='</table>';
                    $text = number_format($total2, '2', '.', ',');
                    if ($total2 < 0) {
                        $text = '(' . number_format($total2 * -1, '2', '.', ',') . ')';
                    }
                    $tamp2.= '<table width="698px" style="clear: both;  top: 78px; padding-left:20px;">';
                    $tamp2.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;" colspan="2" width="50%"></td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="30%">Total ' . $dataHeader2->Header2 . ' : </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . $text . '</td>
                            </tr>';
                    $tamp2.='</table>';
                    //text -> Total Header 2
                    $total1 += $total2;
                    $tamp.= $tamp2;
                    $count2++;
                }
            }
            if ($count2 != 0) {
                $text = number_format($total1, '2', '.', ',');
                if ($total1 < 0) {
                    $text = '(' . number_format($total1 * -1, '2', '.', ',') . ')';
                }
                $html.= $tamp;
                $html.= '<table width="698px" style="clear: both;  top: 78px;padding-left:-1px;">';
                $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;margin: 5px !important;" colspan="4"><hr style="height:0.1px;"></td>
                            </tr>';
                $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;" colspan="2" width="50%"></td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="30%">Total ' . $dataHeader1->Header1 . ' : </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . $text . '</td>
                            </tr>';
                $html.='</table>';
                $totalSemua += $total1;
            }
        }
        $text = number_format($totalSemua, '2', '.', ',');
        if ($totalSemua < 0) {
            $text = '(' . number_format($totalSemua * -1, '2', '.', ',') . ')';
        }
        $html.= '<table width="698px" style="clear: both;  top: 78px;padding-left:-1px;">
            <tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;" colspan="2" width="50%"></td>
                                <td style="font-family: helvetica,sans-serif;font-size: 14px; margin: 5px !important; font-weight: 700; text-align: right" width="30%">Total Profit : </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 14px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . $text . '</td>
                            </tr>';
        $html.='</table>';
        $html.='</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('profit_report');
    }

    public function generalReport() {
        $month = str_pad(Input::get("month"), 2, '0', STR_PAD_LEFT);
        $year = Input::get("year");
        $zero = Input::get('ZeroGeneralLedger');
        $totalDebet = 0;
        $totalCredit = 0;
        $totalInitial = 0;
        $name = "General Ledger Report";
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">' . $name . '</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">' . $name . '</span><br>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period    : ' . date('M', strtotime($year . '-' . $month . '-01')) . ' ' . $year . '</span>
                            <table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Account ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Account Name</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Initial Balance</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Debet</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Credit</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">End Balance</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
        if (Coa::where("CompanyInternalID", Auth::user()->Company->InternalID)->count() > 0) {
            foreach (Coa::where("CompanyInternalID", Auth::user()->Company->InternalID)->orderBy('COAName')->get() as $coa) {
                $debet = JournalDetail::getJournalDebetGeneral($coa, $month, $year);
                $credit = JournalDetail::getJournalCreditGeneral($coa, $month, $year);
                $initial = JournalDetail::getJournalInitialBalanceGeneral($coa, $month, $year);
                $totalDebet += $debet;
                $totalCredit += $credit;
                $totalInitial += $initial;
                if (($debet != 0 || $credit != 0 || $initial != 0) || $zero == 1) {
                    $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . Coa::formatCoa($coa->ACC1InternalID, $coa->ACC2InternalID, $coa->ACC3InternalID, $coa->ACC4InternalID, $coa->ACC5InternalID, $coa->ACC6InternalID, 1) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $coa->COAName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . (JournalDetail::getJournalInitialBalanceGeneral($coa, $month, $year) < 0 ? "(" . number_format((-1 * JournalDetail::getJournalInitialBalanceGeneral($coa, $month, $year)), 2, ".", ",") . ")" : number_format(JournalDetail::getJournalInitialBalanceGeneral($coa, $month, $year), 2, ".", ",")) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . (JournalDetail::getJournalDebetGeneral($coa, $month, $year) < 0 ? "(" . number_format((-1 * JournalDetail::getJournalDebetGeneral($coa, $month, $year)), 2, ".", ",") . ")" : number_format(JournalDetail::getJournalDebetGeneral($coa, $month, $year), 2, ".", ",")) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . (JournalDetail::getJournalCreditGeneral($coa, $month, $year) < 0 ? "(" . number_format((-1 * JournalDetail::getJournalCreditGeneral($coa, $month, $year)), 2, ".", ",") . ")" : number_format(JournalDetail::getJournalCreditGeneral($coa, $month, $year), 2, ".", ",")) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . ((JournalDetail::getJournalInitialBalanceGeneral($coa, $month, $year) + JournalDetail::getJournalDebetGeneral($coa, $month, $year) - JournalDetail::getJournalCreditGeneral($coa, $month, $year)) < 0 ? "(" . number_format((-1 * (JournalDetail::getJournalInitialBalanceGeneral($coa, $month, $year) + JournalDetail::getJournalDebetGeneral($coa, $month, $year) - JournalDetail::getJournalCreditGeneral($coa, $month, $year))), 2, ".", ",") . ")" : number_format((JournalDetail::getJournalInitialBalanceGeneral($coa, $month, $year) + JournalDetail::getJournalDebetGeneral($coa, $month, $year) - JournalDetail::getJournalCreditGeneral($coa, $month, $year)), 2, ".", ",")) . '</td>
                            </tr>';
                }
            }

            $html.= '<tr> <td colspan="6"><hr></td>
                        </tr>
                    <tr>
                                <td colspan="2" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right;"> Total </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right">' . ($totalInitial < 0 ? "(" . number_format((-1 * $totalInitial), 2, ".", ",") . ")" : number_format($totalInitial, 2, ".", ",")) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right">' . ($totalDebet < 0 ? "(" . number_format((-1 * $totalDebet), 2, ".", ",") . ")" : number_format($totalDebet, 2, ".", ",")) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right">' . ($totalCredit < 0 ? "(" . number_format((-1 * $totalCredit), 2, ".", ",") . ")" : number_format($totalCredit, 2, ".", ",")) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right">' . (($totalInitial + $totalDebet - $totalCredit) < 0 ? "(" . number_format((-1 * ($totalInitial + $totalDebet - $totalCredit)), 2, ".", ",") . ")" : number_format(($totalInitial + $totalDebet - $totalCredit), 2, ".", ",")) . '</td>
                    </tr>';
        } else {
            $html.= '<tr>
                            <td colspan="6" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: center">There is no COA account.</td>
                        </tr>';
        }
        $html.= '</tbody>
                            </div>
                    </div>
                </body>
            </html>';
        return PDF::load($html, 'A4', 'portrait')->download('general_report');
    }

}
