<?php

class DefaultAccountController extends BaseController {

    public function showDefaultAccount() {
        if ($_SERVER["REQUEST_METHOD"] === "POST") {
            if (Input::get('jenis') == 'insertDefaultAccount') {
                return $this->insertDefaultAccout();
            }
        }
        return View::make("setting.defaultAccount")
                        ->withToogle('setting')
                        ->withAktif('defaultAccount');
    }

    public function insertDefaultAccout() {
        //rule
        $rule = array(
            'salesCoa' => 'required',
            'receiveableCoa' => 'required',
            'incomeTaxCoa' => 'required',
            'payableCoa' => 'required',
            'outcomeTaxCoa' => 'required',
            'closedJournalCoa' => 'required',
            'mplCoa' => 'required',
            'cgsCoa' => 'required',
            'dpCoaSales' => 'required',
            'dpCoaPurchase' => 'required',
            'discCoa' => 'required'
        );
        $validator = Validator::make(Input::all(), $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('setting.defaultAccount')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('setting')->withAktif('defaultAccount');
        } else {
            $default = Default_s::where('CompanyInternalID', Auth::user()->CompanyInternalID)->delete();
            $data = [];
            $data[0] = 'Sales';
            $data2[0] = 'salesCoa';
            $data[1] = 'Receivable';
            $data2[1] = 'receiveableCoa';
            $data[2] = 'Income Tax';
            $data2[2] = 'incomeTaxCoa';
            $data[3] = 'Payable';
            $data2[3] = 'payableCoa';
            $data[4] = 'Outcome Tax';
            $data2[4] = 'outcomeTaxCoa';
            $data[5] = 'Closed Journal';
            $data2[5] = 'closedJournalCoa';
            $data[6] = 'Moving Profit and Loss';
            $data2[6] = 'mplCoa';
            $data[7] = 'Cost of Goods Sold';
            $data2[7] = 'cgsCoa';
            $data[8] = 'Discount';
            $data2[8] = 'discCoa';
            $data[9] = 'Down Payment Sales';
            $data2[9] = 'dpCoaSales';
            $data[10] = 'Down Payment Purchase';
            $data2[10] = 'dpCoaPurchase';

            foreach ($data as $key => $value) {
                $coa = Coa::find(Input::get($data2[$key]));
                $default = new Default_s;
                $default->DefaultID = $value;
                $default->ACC1InternalID = $coa->ACC1InternalID;
                $default->ACC2InternalID = $coa->ACC2InternalID;
                $default->ACC3InternalID = $coa->ACC3InternalID;
                $default->ACC4InternalID = $coa->ACC4InternalID;
                $default->ACC5InternalID = $coa->ACC5InternalID;
                $default->ACC6InternalID = $coa->ACC6InternalID;
                $default->CompanyInternalID = Auth::user()->CompanyInternalID;
                $default->save();
            }

            Session::flash('messages', 'suksesUpdate');
            return Redirect::Route('showDefaultAccount');
        }
    }

}
