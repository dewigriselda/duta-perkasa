<?php

class HomeController extends BaseController {
    /*
      |--------------------------------------------------------------------------
      | Default Home Controller
      |--------------------------------------------------------------------------
      |
      | You may wish to use controllers instead of, or in addition to, Closure
      | based routes. That's great! Here is an example controller method to
      | get you started. To route to this controller, just add the route:
      |
      |	Route::get('/', 'HomeController@showWelcome');
      |
     */

    public function showDashboard() {
        return View::make('dashboard')->withAktif('dashboard')->withToogle('dashboard');
    }

    public function showLogin() {
        if (Auth::check()) {
            return Redirect::Route('showDashboard');
        } else {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                if (Input::get('jenis') == 'userLogin') {
                    return $this->login();
                } else if (Input::get('jenis') == 'userForgot') {
                    return $this->forgot();
                }
            }
            return View::make('login');
        }
    }

    public function login() {
        //rule
        $rule = array(
            'email' => 'required|email',
            'password' => 'required|max:16'
        );

        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            return View::make('login')
                            ->withMessages('salahLogin')
                            ->withError($validator->messages());
        } else {
            $email = Input::get('email');
            $password = Input::get('password');
            if (Input::get('remember') == 'remember') {
                if (Auth::attempt(array('Email' => $email, 'password' => $password), true)) {
                    $dataUser = User::find(Auth::user()->InternalID);
                    $statusUser = $dataUser->Status;
                    $statusCompany = $dataUser->Company->Status;
                    if ($statusCompany == 0) {
                        Session::flash('status', 'company');
                        return $this->logout();
                    } else if ($statusUser == 0) {
                        Session::flash('status', 'user');
                        return $this->logout();
                    }
                    Session::put('LastLogin',$dataUser->LastLogin);
                    $dataUser->LastLogin = date('Y-m-d H:i:s');
                    $dataUser->save();
                    if ($dataUser->CompanyInternalID == -2) {
                        return Redirect::intended('showHomeAgent');
                    } else if ($dataUser->CompanyInternalID != -1) {
                        return Redirect::intended('showDashboard');
                    }
                    return Redirect::intended('showUser');
                }
            } else {
                if (Auth::attempt(array('Email' => $email, 'password' => $password))) {
                    $dataUser = User::find(Auth::user()->InternalID);
                    $statusUser = $dataUser->Status;
                    $statusCompany = $dataUser->Company->Status;
                    if ($statusCompany == 0) {
                        Session::flash('status', 'company');
                        return $this->logout();
                    } else if ($statusUser == 0) {
                        Session::flash('status', 'user');
                        return $this->logout();
                    }
                    Session::put('LastLogin',$dataUser->LastLogin);
                    $dataUser->LastLogin = date('Y-m-d H:i:s');
                    $dataUser->save();
                    if ($dataUser->CompanyInternalID == -2) {
                        return Redirect::intended('showHomeAgent');
                    } else if ($dataUser->CompanyInternalID != -1) {
                        return Redirect::intended('showDashboard');
                    }
                    return Redirect::intended('showUser');
                }
            }
        }
        return View::make('login')
                        ->withMessages('gagalLogin');
    }

    public function forgot() {
        if (User::where('Email', Input::get('email'))->count() > 0) {
            $user = User::where('Email', Input::get('email'))->first();
            $plainText = $user->UserID . '---;---' . $user->InternalID;
            $enkrip = Hash::make($plainText);
            $enkrip = myEncryptEmail($enkrip);
            $data['name'] = $user->UserName;
            $data["link"] = "http://application.salmonacc.com/checkForgotPassword/" . $enkrip;
            Mail::send('emails.forgotPassword', $data, function($message) {
                $user = User::where('Email', Input::get('email'))->first();
                $message->to($user->Email, $user->Company->CompanyName)->subject('Forgot Password');
            });
            return View::make("login")
                            ->withMessages('suksesResetPassword');
        } else {
            return View::make("login")
                            ->withMessages('gagalResetPassword');
        }
    }

    public function logout() {
        Auth::logout();
        return Redirect::intended('login');
    }

    public function showContact() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            return $this->contactUs();
        }
        return View::make('contact')->withAktif('none')->withToogle('none');
    }

    public function contactUs() {
        //rule
        $rule = array(
            'Subject' => 'required',
            'message' => 'required'
        );

        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            return View::make('contact')
                            ->withAktif('none')
                            ->withToogle('none')
                            ->withMessages('gagalContact')
                            ->withError($validator->messages());
        } else {
            $user = User::find(Auth::user()->InternalID);
            $data = [];
            $data["userName"] = $user->UserName;
            $data["email"] = $user->Email;
            $data["subject"] = Input::get("Subject");
            $data["pesan"] = Input::get("message");

            Mail::send('emails.feedbackUser', $data, function($message) {
                $message->to("salmon.support@salmonacc.com", "Salmon Support Team")->subject(Input::get("Subject"));
            });
            
            Mail::send('emails.feedbackUser', $data, function($message) {
                $message->to("dswitono@genesysindonesia.com", "Doni Wistono")->subject(Input::get("Subject"));
            });

            return View::make('contact')->withAktif('none')->withToogle('none')->withMessages('suksesContact');
        }
    }

    public function showHelp() {
        return View::make('help')->withAktif('help')->withToogle('');
    }
    
    public function showChangeLog() {
        return View::make('changeLog')->withAktif('changeLog')->withToogle('');
    }

}
