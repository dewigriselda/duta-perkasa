<?php

class CompanyController extends BaseController {

    public function showCompany() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            ;
            if (Input::get('jenis') == 'insertCompany') {
                return $this->insertCompany();
            }
            if (Input::get('jenis') == 'updateCompany') {
                return $this->updateCompany();
            }
            if (Input::get('jenis') == 'deleteCompany') {
                return $this->deleteCompany();
            }
            if (Input::get('jenis') == 'activeCompany') {
                return $this->activeCompany();
            }
            if (Input::get('jenis') == 'nonActiveCompany') {
                return $this->nonActiveCompany();
            }
            if (Input::get('jenis') == 'paymentCompany') {
                return $this->paymentCompany();
            }
            if (Input::get('jenis') == 'invoiceCompany') {
                return $this->invoiceCompany();
            }
        }
        return View::make('master.company')
                        ->withToogle('master')->withAktif('company');
    }

    public function showCompanyProcess() {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (Input::get("jenis") == "backup") {
                return $this->backupCompany('0');
            } else if (Input::get("jenis") == "cut_off") {
                return $this->cutOff();
            } else if (Input::get("jenis") == "delete") {
                return $this->deleteHistoryCompany();
            } else if (Input::get("jenis") == "download") {
                $data = Session::get("dataDownload");
                return Response::make($data[0], 200, $data[1]);
            }
        }
        return View::make('utility.companyProcess')
                        ->withToogle('utility')
                        ->withAktif("companyProcess");
    }

    public function settProfileCompany() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'updateProfileCompany') {
                return $this->updateProfileCompany();
            }
        }
        return View::make('setting.companyProfile')
                        ->withToogle('setting')->withAktif('companyProfile');
    }

    static function insertCompany() {
        //rule
        $rule = array(
            'CompanyID' => 'required|max:200|unique:m_company,CompanyID',
            'CompanyName' => 'required|max:200',
            'Address' => 'max:1000',
            'City' => 'max:1000',
            'Phone' => 'max:200',
            'Fax' => 'max:200',
            'Email' => 'required|max:200|email|unique:m_company,Email',
            'Logo' => 'image',
            'Region' => 'required',
            'remark' => 'required|max:1000'
        );
        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('master.company')
                            ->withMessages('gagalInsert')
                            ->withError($validator->messages())
                            ->withToogle('master')->withAktif('company');
        } else {
            $path = NULL;
            $input = Input::file('Logo');
            if ($input != '') {
                if (Input::hasFile('Logo') && Input::file('Logo')->getSize() <= 2000000) {
                    $destination = base_path() . '/uploaded_img/';
                    $filename = str_random(6) . '_' . date('d') . date('m') . date('y') . '_' . Input::file('Logo')->getClientOriginalName();
                    Input::file('Logo')->move($destination, $filename);
                    $height = Image::make(sprintf($destination . '%s', $filename))->height();
                    $width = Image::make(sprintf($destination . '%s', $filename))->width();
                    $heightResize = null;
                    $widthResize = null;
                    $countResize = 0;
                    if ($height > 200) {
                        $heightResize = 200;
                        $countResize = 1;
                    }
                    if ($width > 705) {
                        $widthResize = 705;
                        $countResize = 1;
                    }
                    if ($countResize == 1) {
                        $image = Image::make(sprintf($destination . '%s', $filename))->resize($widthResize, $heightResize)->save();
                    }
                    $path = '/uploaded_img/' . $filename;
                } else {
                    return View::make('master.company')
                                    ->withMessages('gagalUpload')
                                    ->withToogle('master')->withAktif('company');
                }
            }
            //valid
            $company = new Company;
            $company->CompanyID = Input::get('CompanyID');
            $company->CompanyName = Input::get('CompanyName');
            $company->Address = Input::get('Address');
            $company->City = Input::get('City');
            $company->TaxID = Input::get('taxID');
            $company->Phone = Input::get('Phone');
            $company->Fax = Input::get('Fax');
            $company->Email = Input::get('Email');
            $company->RegionInternalID = Input::get('Region');
            $company->Logo = $path;
            $company->Status = 1;
            $company->UserRecord = Auth::user()->UserID;
            $company->UserModified = '0';
            $company->Remark = Input::get('remark');
            $company->save();

            return View::make('master.company')
                            ->withMessages('suksesInsert')
                            ->withToogle('master')->withAktif('company');
        }
    }

    function updateCompany() {
        //rule
        $rule = array(
            'CompanyName' => 'required|max:200',
            'Address' => 'max:1000',
            'City' => 'max:1000',
            'Phone' => 'max:200',
            'Fax' => 'max:200',
            'Logo' => 'image',
            'Region' => 'required',
            'remark' => 'required|max:1000'
        );

        //cek Email sama enggak
        $emailLama = Company::find(Input::get('InternalID'))->Email;
        $data = Input::all();
        if (strtoupper(Input::get('Email')) == strtoupper($emailLama)) {
            $rule2 = array(
                'Email' => 'required|max:200|email'
            );
            $rule = array_merge($rule, $rule2);
        } else {
            $rule2 = array(
                'Email' => 'required|max:200|email|unique:m_company,Email'
            );
            $rule = array_merge($rule, $rule2);
        }
        //validasi
        $validator = Validator::make(Input::all(), $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('master.company')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('master')->withAktif('company');
        } else {
            $path = NULL;
            $input = Input::file('Logo');
            if ($input != '') {
                if (Input::hasFile('Logo') && Input::file('Logo')->getSize() <= 2000000) {
                    $destination = base_path() . '/uploaded_img/';
                    $filename = str_random(6) . '_' . date('d') . date('m') . date('y') . '_' . Input::file('Logo')->getClientOriginalName();
                    Input::file('Logo')->move($destination, $filename);
                    $height = Image::make(sprintf($destination . '%s', $filename))->height();
                    $width = Image::make(sprintf($destination . '%s', $filename))->width();
                    $heightResize = null;
                    $widthResize = null;
                    $countResize = 0;
                    if ($height > 200) {
                        $heightResize = 200;
                        $countResize = 1;
                    }
                    if ($width > 705) {
                        $widthResize = 705;
                        $countResize = 1;
                    }
                    if ($countResize == 1) {
                        $image = Image::make(sprintf($destination . '%s', $filename))->resize($widthResize, $heightResize)->save();
                    }
                    $path = '/uploaded_img/' . $filename;
                } else {
                    return View::make('master.company')
                                    ->withMessages('gagalUpload')
                                    ->withToogle('master')->withAktif('company');
                }
            }
            //valid
            $company = Company::find(Input::get('InternalID'));
            $company->CompanyName = Input::get('CompanyName');
            $company->Address = Input::get('Address');
            $company->City = Input::get('City');
            $company->TaxID = Input::get('taxID');
            $company->Phone = Input::get('Phone');
            $company->Fax = Input::get('Fax');
            $company->Email = Input::get('Email');
            $company->RegionInternalID = Input::get('Region');
            if ($path != NULL) {
                if ($company->Logo != NULL) {
                    $destination = public_path();
                    File::delete(sprintf($destination . '%s', $company->Logo));
                }
                $company->Logo = $path;
            }
            $company->UserModified = Auth::user()->UserID;
            $company->Remark = Input::get('remark');
            $company->save();

            return View::make('master.company')
                            ->withMessages('suksesUpdate')
                            ->withToogle('master')->withAktif('company');
        }
    }

    function deleteCompany() {
        //cek apakah ID company ada di tabel m_user atau tidak
        $user = DB::table('m_user')->where('CompanyInternalID', Input::get('InternalID'))->first();
        if (is_null($user)) {
            //tidak ada maka data boleh dihapus
            $company = Company::find(Input::get('InternalID'));
            if ($company->Logo != NULL) {
                $destination = public_path();
                File::delete(sprintf($destination . '%s', $company->Logo));
            }
            $company->delete();
            return View::make('master.company')
                            ->withMessages('suksesDelete')
                            ->withToogle('master')->withAktif('company');
        } else {
            //ada maka data tidak boleh dihapus
            return View::make('master.company')
                            ->withMessages('gagalDelete')
                            ->withToogle('master')->withAktif('company');
        }
    }

    function activeCompany() {
        $company = Company::find(Input::get('InternalID'));

        $company->Status = 1;
        $company->save();

        return View::make('master.company')
                        ->withMessages('suksesUpdate')
                        ->withToogle('master')->withAktif('company');
    }

    function nonActiveCompany() {
        $company = Company::find(Input::get('InternalID'));

        $company->Status = 0;
        $company->save();

        return View::make('master.company')
                        ->withMessages('suksesUpdate')
                        ->withToogle('master')->withAktif('company');
    }

    function paymentCompany() {
        $company = Company::find(Input::get('InternalID'));
        $company->StatusPayment = 1;
        $company->ConfirmationDate = date('Y-m-d');
        $company->ExpiredDate = date("Y-m-d", strtotime("+1 month", strtotime($company->ExpiredDate)));
        $company->save();

        $year = substr(date("Y"), 2, 2);
        $month = str_pad(date("m"), 2, '0', STR_PAD_LEFT);
        $date = $company->CompanyID . "-" . $month . $year . '-';

        $data["companyID"] = Payment::getNextIDPayment($date, $company->InternalID);
        //insert invoice id
        $payment = new Payment();
        $payment->CompanyInternalID = $company->InternalID;
        $payment->PaymentDate = date("Y-m-d");
        $payment->PaymentID = $data["companyID"];
        $payment->AgentCommission = 10;
        $payment->UserRecord = Auth::user()->UserName;
        $payment->UserModified = 0;
        $payment->Remark = "-";
        $payment->save();

        //send email pemberitahuan udah aktif
        $data = [];
        $data["companyID"] = $company->CompanyID;
        $data["companyName"] = $company->CompanyName;
        $data["plan"] = $company->Package->PackageName;

        Mail::send('emails.infoAccountIsActive', $data, function($message) {
            $company = Company::find(Input::get('InternalID'));
            $message->to($company->Email, $company->CompanyName)->subject("Your Account is Active");
        });

        $invoice = Invoice::where('Status', 0)
                ->where('CompanyInternalID', Input::get('InternalID'))
                ->update(['Status' => 1]);

        return View::make('master.company')
                        ->withMessages('suksesUpdate')
                        ->withToogle('master')->withAktif('company');
    }

    function invoiceCompany() {
        //send email tagihan perbulan
        $company = Company::find(Input::get('InternalID'));
        //send email pemberitahuan udah aktif
        $data = [];
        $data["companyID"] = $company->CompanyID;
        $data["companyName"] = $company->CompanyName;

        $data["plan"] = $company->Package->PackageName;
        $data["harga"] = number_format($company->Package->Price, 2, '.', ',');
        $data["kuota"] = $company->Package->Memory . " MB";

        $year = substr(date("Y"), 2, 2);
        $month = str_pad(date("m"), 2, '0', STR_PAD_LEFT);
        $date = $data["companyID"] . "-" . $month . $year . '-';

        $data["invoiceID"] = Invoice::getNextIDInvoice($date, $company->InternalID);

        //insert invoice id
        $invoice = new Invoice();
        $invoice->CompanyInternalID = $company->InternalID;
        $invoice->InvoiceDate = date("Y-m-d");
        $invoice->InvoiceID = $data["invoiceID"];
        $invoice->Status = 0;
        $invoice->UserRecord = "Server";
        $invoice->UserModified = 0;
        $invoice->Remark = "-";
        $invoice->save();

        Mail::send('emails.invoicePaymentPerMonth', $data, function($message) {
            $company = Company::find(Input::get('InternalID'));
            $message->to($company->Email, $company->CompanyName)->subject('Invoice Payment');
        });

        return View::make('master.company')
                        ->withMessages('suksesSendInvoice')
                        ->withToogle('master')->withAktif('company');
    }

    function updateProfileCompany() {
        //rule
        $rule = array(
            'CompanyName' => 'required|max:200',
            'Address' => 'max:1000',
            'City' => 'max:1000',
            'Phone' => 'max:200',
            'Fax' => 'max:200',
            'Region' => 'required',
            'Logo' => 'image'
        );
        //cek Email sama enggak
        $emailLama = Company::find(Auth::user()->Company->InternalID)->Email;
        $data = Input::all();
        if (strtoupper(Input::get('Email')) == strtoupper($emailLama)) {
            $rule2 = array(
                'Email' => 'required|max:200|email'
            );
            $rule = array_merge($rule, $rule2);
        } else {
            $rule2 = array(
                'Email' => 'required|max:200|email|unique:m_company,Email'
            );
            $rule = array_merge($rule, $rule2);
        }
        //validasi
        $validator = Validator::make(Input::all(), $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('setting.companyProfile')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('setting')->withAktif('companyProfile');
        } else {
            $path = NULL;
            $input = Input::file('Logo');
            if ($input != '') {
                if (Input::hasFile('Logo') && Input::file('Logo')->getSize() <= 2000000) {
                    $destination = base_path() . '/uploaded_img/';
                    $filename = str_random(6) . '_' . date('d') . date('m') . date('y') . '_' . Input::file('Logo')->getClientOriginalName();
                    Input::file('Logo')->move($destination, $filename);
                    $height = Image::make(sprintf($destination . '%s', $filename))->height();
                    $width = Image::make(sprintf($destination . '%s', $filename))->width();
                    $heightResize = null;
                    $widthResize = null;
                    $countResize = 0;
                    if ($height > 200) {
                        $heightResize = 200;
                        $countResize = 1;
                    }
                    if ($width > 705) {
                        $widthResize = 705;
                        $countResize = 1;
                    }
                    if ($countResize == 1) {
                        $image = Image::make(sprintf($destination . '%s', $filename))->resize($widthResize, $heightResize)->save();
                    }
                    $path = '/uploaded_img/' . $filename;
                } else {
                    return View::make('setting.companyProfile')
                                    ->withMessages('gagalUpload')
                                    ->withToogle('setting')->withAktif('companyProfile');
                }
            }
            //valid
            $company = Company::find(Auth::user()->Company->InternalID);
            $company->CompanyName = Input::get('CompanyName');
            $company->Address = Input::get('Address');
            $company->City = Input::get('City');
            $company->TaxID = Input::get('taxID');
            $company->Phone = Input::get('Phone');
            $company->Fax = Input::get('Fax');
            $company->Email = Input::get('Email');
            $company->RegionInternalID = Input::get('Region');
            if ($path != NULL) {
                if ($company->Logo != NULL) {
                    $destination = public_path();
                    File::delete(sprintf($destination . '%s', $company->Logo));
                }
                $company->Logo = $path;
            }
            $company->UserModified = Auth::user()->UserID;
            $company->Remark = Input::get('remark');
            $company->save();

            $messages = 'suksesUpdate';
            Session::flash('messages', $messages);
            return Redirect::Route('settCompany');

            return View::make('setting.companyProfile')
                            ->withMessages('suksesUpdate')
                            ->withToogle('setting')->withAktif('companyProfile');
        }
    }

    public function exportExcel() {
        Excel::create('Company_master', function($excel) {
            $excel->sheet('Company_master', function($sheet) {
                $sheet->mergeCells('B1:M1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Company");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Company Name");
                $sheet->setCellValueByColumnAndRow(3, 2, "Company ID");
                $sheet->setCellValueByColumnAndRow(4, 2, "Address");
                $sheet->setCellValueByColumnAndRow(5, 2, "City");
                $sheet->setCellValueByColumnAndRow(6, 2, "Region");
                $sheet->setCellValueByColumnAndRow(7, 2, "TaxID");
                $sheet->setCellValueByColumnAndRow(8, 2, "Phone");
                $sheet->setCellValueByColumnAndRow(9, 2, "Fax");
                $sheet->setCellValueByColumnAndRow(10, 2, "Record");
                $sheet->setCellValueByColumnAndRow(11, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(12, 2, "Remark");
                $row = 3;
                foreach (Company::all() as $data) {
                    if ($data->RegionInternalID == null) {
                        $region = "Not Register";
                    } else {
                        $region = $data->Region->RegionName;
                    }
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, $data->CompanyName);
                    $sheet->setCellValueByColumnAndRow(3, $row, "`" . $data->CompanyID);
                    $sheet->setCellValueByColumnAndRow(4, $row, $data->Address);
                    $sheet->setCellValueByColumnAndRow(5, $row, $data->City);
                    $sheet->setCellValueByColumnAndRow(6, $row, $region);
                    if ($data->TaxID == '...-.') {
                        $sheet->setCellValueByColumnAndRow(7, $row, '');
                    } else {
                        $sheet->setCellValueByColumnAndRow(7, $row, $data->TaxID);
                    }
                    $sheet->setCellValueByColumnAndRow(8, $row, '`' . $data->Phone);
                    $sheet->setCellValueByColumnAndRow(9, $row, '`' . $data->Fax);
                    $sheet->setCellValueByColumnAndRow(10, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(11, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(12, $row, $data->Remark);
                    $row++;
                }
                $row--;
                $sheet->setBorder('B2:M' . $row, 'thin');
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B2:M2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B3:M' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
            });
        })->export('xls');
    }

    public function backupCompany($jenis) {
        $company = Company::find(Input::get('InternalID'));
        //file writer
        $headers = array(
            "Content-type" => "text/html",
            "Content-Disposition" => "attachment;Filename=" . date("d-M-Y") . '|' . $company->CompanyID . ".sql"
        );

        $dataBackup = array('m_company', 'm_coa1', 'm_coa2', 'm_coa3', 'm_coa4', 'm_coa5', 'm_coa6', 'm_coa', 'm_currency', 'm_department',
            'm_inventorytype', 'm_inventory', 'm_slip', 'm_user', 'm_user_detail', 'm_warehouse', 's_default', 't_invoice', 'm_groupdepreciation',
            'm_depreciation_header', 'm_depreciation_detail', 't_journal_header', 't_journal_detail', 't_salesorder_header', 't_salesorder_detail',
            't_purchaseorder_header', 't_purchaseorder_detail', 't_purchase_header', 't_purchase_detail', 't_sales_header', 't_sales_detail',
            't_purchasereturn_header', 't_purchasereturn_detail', 't_salesreturn_header', 't_salesreturn_detail',
            't_memoin_header', 't_memoin_detail', 't_memoout_header', 't_memoout_detail', 't_transfer_header', 't_transfer_detail', 'h_payment', 'm_uom', 'm_inventory_uom');

        $content = '';
        foreach ($dataBackup as $table) {
            $foreignKey = 'CompanyInternalID';

            if ($table == 'm_company') {
                $foreignKey = 'InternalID';
            }

            if ($table == 'm_user_detail' || $table == 'm_depreciation_detail' || $table == 't_journal_detail' || $table == 't_salesorder_detail' || $table == 't_purchaseorder_detail' || $table == 't_sales_detail' || $table == 't_purchase_detail' || $table == 't_purchasereturn_detail' || $table == 't_salesreturn_detail' || $table == 't_memoin_detail' || $table == 't_memoout_detail' || $table == 't_transfer_detail') {
                if ($table == 'm_user_detail') {
                    $tampText = rtrim($table, '_detail');
                } else {
                    $tampText = rtrim($table, 'detail') . 'header';
                }
                $tampForeign = substr($table, 2);
                $tampForeign = str_replace('_detail', '', $tampForeign);
                $tampForeign = str_replace('return', 'Return', $tampForeign);
                $tampForeign = str_replace('order', 'Order', $tampForeign);
                $tampForeign = str_replace('memoin', 'memoIn', $tampForeign);
                $tampForeign = str_replace('memoout', 'memoOut', $tampForeign);
                $tampForeign = ucfirst($tampForeign) . 'InternalID';
                $comp = DB::table($table)->join($tampText, $tampText . '.InternalID', '=', $table . '.' . $tampForeign)
                                ->where($tampText . '.' . $foreignKey, $company->InternalID)->count();
                $dataTable = DB::table($table)->join($tampText, $tampText . '.InternalID', '=', $table . '.' . $tampForeign)
                                ->where($tampText . '.' . $foreignKey, $company->InternalID)->select($table . '.*')->get();
            } else {
                $comp = DB::table($table)->where($foreignKey, $company->InternalID)->count();
                $dataTable = DB::table($table)->where($foreignKey, $company->InternalID)->get();
            }

            $data = DB::select(DB::raw("SELECT COLUMN_NAME 
            FROM information_schema.columns
            WHERE table_schema = 'genesys_akun_pajak'
            AND table_name   = '" . $table . "'"));

            if ($comp > 0) {
                $content .= 'INSERT INTO `' . $table . '` (';
                foreach ($data as $column) {
                    $content .= '`' . $column->COLUMN_NAME . '`,';
                }
                $content = rtrim($content, ',');
                $content .= ') VALUES';

                foreach ($dataTable as $value) {
                    $value = (array) $value;
                    $counter = 1;
                    $content .= "\n(";
                    foreach ($value as $index) {
                        if (is_null($index)) {
                            $content .= "NULL,";
                        } else {
                            $content .= "'" . $index . "',";
                        }
                        $counter = 2;
                    }
                    $content = rtrim($content, ',');
                    $content .= "),";
                }
                $content = rtrim($content, ',');
                $content .= ";\n\n";
            }
        }
        if ($jenis == '0') {
            return Response::make($content, 200, $headers);
        } else {
            $arr = array($content, $headers);
            Session::flash('dataDownload', $arr);
            return 'ada';
        }
    }

    public function deleteHistoryCompany() {
        $company = Company::find(Input::get('InternalID'));
        $tampBackup = $this->backupCompany('1');
        //hapus Veritrans//

        $dataBackup = array('m_company', 'm_coa1', 'm_coa2', 'm_coa3', 'm_coa4', 'm_coa5', 'm_coa6', 'm_coa', 'm_currency', 'm_department',
            'm_inventorytype', 'm_inventory', 'm_slip', 'm_user', 'm_user_detail', 'm_warehouse', 's_default', 't_invoice', 'm_groupdepreciation',
            'm_depreciation_header', 'm_depreciation_detail', 't_journal_header', 't_journal_detail', 't_salesorder_header', 't_salesorder_detail',
            't_purchaseorder_header', 't_purchaseorder_detail', 't_purchase_header', 't_purchase_detail', 't_sales_header', 't_sales_detail',
            't_purchasereturn_header', 't_purchasereturn_detail', 't_salesreturn_header', 't_salesreturn_detail',
            't_memoin_header', 't_memoin_detail', 't_memoout_header', 't_memoout_detail', 't_transfer_header', 't_transfer_detail', 'm_uom', 'm_inventory_uom');
        $dataBackup = array_reverse($dataBackup);
        foreach ($dataBackup as $table) {
            $foreignKey = 'CompanyInternalID';

            if ($table == 'm_company') {
                $foreignKey = 'InternalID';
            }

            if ($table == 'm_user_detail' || $table == 'm_depreciation_detail' || $table == 't_journal_detail' || $table == 't_salesorder_detail' || $table == 't_purchaseorder_detail' || $table == 't_sales_detail' || $table == 't_purchase_detail' || $table == 't_purchasereturn_detail' || $table == 't_salesreturn_detail' || $table == 't_memoin_detail' || $table == 't_memoout_detail' || $table == 't_transfer_detail') {
                if ($table == 'm_user_detail') {
                    $tampText = rtrim($table, '_detail');
                } else {
                    $tampText = rtrim($table, 'detail') . 'header';
                }
                $tampForeign = substr($table, 2);
                $tampForeign = str_replace('_detail', '', $tampForeign);
                $tampForeign = str_replace('return', 'Return', $tampForeign);
                $tampForeign = str_replace('order', 'Order', $tampForeign);
                $tampForeign = str_replace('memoin', 'memoIn', $tampForeign);
                $tampForeign = str_replace('memoout', 'memoOut', $tampForeign);
                $tampForeign = ucfirst($tampForeign) . 'InternalID';
                $comp = DB::table($table)->join($tampText, $tampText . '.InternalID', '=', $table . '.' . $tampForeign)
                                ->where($tampText . '.' . $foreignKey, $company->InternalID)->count();
                if ($comp > 0) {
                    $dataTable = DB::table($table)->join($tampText, $tampText . '.InternalID', '=', $table . '.' . $tampForeign)
                                    ->where($tampText . '.' . $foreignKey, $company->InternalID)->delete();
                }
            } else if ($table == 'm_company') {
                $company = Company::find(Input::get('InternalID'));
                $company->Status = 0;
                $company->Save();
            } else {
                $comp = DB::table($table)->where($foreignKey, $company->InternalID)->count();
                if ($comp > 0) {
                    $dataTable = DB::table($table)->where($foreignKey, $company->InternalID)->delete();
                }
            }
        }
//        return $tampBackup;
        return View::make("utility.companyProcess")
                        ->withCompany($company->CompanyID)
                        ->withMessages("suksesDelete")
                        ->withDatadownload($tampBackup)
                        ->withToogle('utility')
                        ->withAktif("companyProcess");
    }

    public function cutOff() {
        $company = Company::find(Input::get('InternalID'));
        $tampBackup = $this->backupCompany('1');

        //Initial Quantity
        $delimiterWarehouse = '!=';
        $warehouse = '-1';
        foreach (Inventory::where('CompanyInternalID', $company->InternalID)->get() as $data) {
            $initialStock = Inventory::getInitialStockSuperAdmin(date('Y-m-d'), '<=', $data->InternalID, $warehouse, $delimiterWarehouse, $company->InternalID);
            $inventoryInsert = Inventory::find($data->InternalID);
            $inventoryInsert->InitialQuantity = $initialStock;
            $inventoryInsert->Save();
        }

        //Initial Value
        $inventory = Inventory::join('m_inventorytype', 'm_inventorytype.InternalID', '=', 'm_inventory.InventoryTypeInternalID')
                        ->select('*', DB::raw('m_inventory.InternalID as InventoryInternalID'))
                        ->where('m_inventorytype.Flag', 1)
                        ->where('m_inventorytype.CompanyInternalID', $company->InternalID)->get();
        foreach ($inventory as $data) {
            $qtySales = SalesHeader::qtyInventorySuperAdmin($data->InventoryInternalID, $company->InternalID);
            $qtySalesR = SalesReturnHeader::qtyInventorySuperAdmin($data->InventoryInternalID, $company->InternalID);
            $qtyPurchase = PurchaseHeader::qtyInventorySuperAdmin($data->InventoryInternalID, $company->InternalID);
            $qtyPurchaseR = PurchaseReturnHeader::qtyInventorySuperAdmin($data->InventoryInternalID, $company->InternalID);
            $purchase = PurchaseHeader::valueInventorySuperAdmin($data->InventoryInternalID, $company->InternalID);
            $purchaseR = PurchaseReturnHeader::valueInventorySuperAdmin($data->InventoryInternalID, $company->InternalID);
            $qtyDividen = $data->InitialQuantity + $qtyPurchase - $qtyPurchaseR;
            if ($qtyDividen == 0) {
                $average = 0;
            } else {
                $average = (($data->InitialValue * $data->InitialQuantity) + $purchase - $purchaseR) / $qtyDividen;
            }
            $inventoryInsert = Inventory::find($data->InventoryInternalID);
            $inventoryInsert->InitialValue = $average;
            $inventoryInsert->Save();
        }

        //Initial Balance Accounting
        foreach (Coa::where("CompanyInternalID", $company->InternalID)->get() as $coa) {
            $endBalance = JournalDetail::getJournalInitialBalanceSuperAdmin($coa, $company->InternalID);
            $coaInsert = Coa::find($coa->InternalID);
            $coaInsert->InitialBalance = $endBalance;
            $coaInsert->save();
        }

        $dataBackup = array('t_journal_header', 't_journal_detail', 't_salesorder_header', 't_salesorder_detail',
            't_purchaseorder_header', 't_purchaseorder_detail', 't_purchase_header', 't_purchase_detail', 't_sales_header', 't_sales_detail',
            't_purchasereturn_header', 't_purchasereturn_detail', 't_salesreturn_header', 't_salesreturn_detail',
            't_memoin_header', 't_memoin_detail', 't_memoout_header', 't_memoout_detail', 't_transfer_header', 't_transfer_detail');
        $dataBackup = array_reverse($dataBackup);
        foreach ($dataBackup as $table) {
            $foreignKey = 'CompanyInternalID';
            if ($table == 'm_company') {
                $foreignKey = 'InternalID';
            }
            if ($table == 'm_user_detail' || $table == 'm_depreciation_detail' || $table == 't_journal_detail' || $table == 't_salesorder_detail' || $table == 't_purchaseorder_detail' || $table == 't_sales_detail' || $table == 't_purchase_detail' || $table == 't_purchasereturn_detail' || $table == 't_salesreturn_detail' || $table == 't_memoin_detail' || $table == 't_memoout_detail' || $table == 't_transfer_detail') {
                if ($table == 'm_user_detail') {
                    $tampText = rtrim($table, '_detail');
                } else {
                    $tampText = rtrim($table, 'detail') . 'header';
                }
                $tampForeign = substr($table, 2);
                $tampForeign = str_replace('_detail', '', $tampForeign);
                $tampForeign = str_replace('return', 'Return', $tampForeign);
                $tampForeign = str_replace('order', 'Order', $tampForeign);
                $tampForeign = str_replace('memoin', 'memoIn', $tampForeign);
                $tampForeign = str_replace('memoout', 'memoOut', $tampForeign);
                $tampForeign = ucfirst($tampForeign) . 'InternalID';
                $comp = DB::table($table)->join($tampText, $tampText . '.InternalID', '=', $table . '.' . $tampForeign)
                                ->where($tampText . '.' . $foreignKey, $company->InternalID)->count();
                if ($comp > 0) {
                    $dataTable = DB::table($table)->join($tampText, $tampText . '.InternalID', '=', $table . '.' . $tampForeign)
                                    ->where($tampText . '.' . $foreignKey, $company->InternalID)->delete();
                }
            } else {
                $comp = DB::table($table)->where($foreignKey, $company->InternalID)->count();
                if ($comp > 0) {
                    $dataTable = DB::table($table)->where($foreignKey, $company->InternalID)->delete();
                }
            }
        }
//        return $tampBackup;
        return View::make("utility.companyProcess")
                        ->withMessages("suksesCutOff")
                        ->withCompany($company->CompanyID)
                        ->withDatadownload($tampBackup)
                        ->withToogle('utility')
                        ->withAktif("companyProcess");
    }

}
