<?php

class InventoryController extends BaseController {

    public function showInventory() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertInventory') {
                return $this->insertInventory();
            }
            if (Input::get('jenis') == 'updateInventory') {
                return $this->updateInventory();
            }
            if (Input::get('jenis') == 'deleteInventory') {
                return $this->deleteInventory();
            }
            if (Input::get('jenis') == 'detailInventory') {
                return $this->detailInventory();
            }
            if (Input::get('jenis') == 'summaryInventory') {
                return $this->summaryInventory();
            }
            if (Input::get('jenis') == 'stockInventory') {
                return $this->stockInventory();
            }
            if (Input::get('jenis') == 'reportStock') {
                return $this->reportStock();
            }
        }
        return View::make('master.inventory')
                        ->withToogle('master')->withAktif('inventory');
    }

    public static function insertInventory() {
        //rule
        $rule = array(
            'InventoryID' => 'required|max:200|unique:m_inventory,InventoryID,NULL,InventoryID,CompanyInternalID,' . Auth::user()->Company->InternalID . '',
            'Type' => 'required',
            'UoM' => 'required|max:100',
            'InventoryName' => 'required|max:200',
            'remark' => 'required|max:1000'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('master.inventory')
                            ->withMessages('gagalInsert')
                            ->withError($validator->messages())
                            ->withToogle('master')->withAktif('inventory')
                            ->withErrors($validator);
        } else {
            //valid
            $inventory = new Inventory;
            $inventory->InventoryID = Input::get('InventoryID');
            $inventory->InventoryTypeInternalID = Input::get('Type');
            $inventory->InventoryName = Input::get('InventoryName');
            $inventory->UoM = Input::get('UoM');
            $inventory->InitialValue = 0;
            $inventory->InitialQuantity = 0;
            $inventory->MaxStock = str_replace(',', '', Input::get('MaxStock'));
            $inventory->MinStock = str_replace(',', '', Input::get('MinStock'));
            $inventory->UserRecord = Auth::user()->UserID;
            $inventory->CompanyInternalID = Auth::user()->Company->InternalID;
            $inventory->UserModified = "0";
            $inventory->Remark = Input::get('remark');
            $inventory->save();

            return View::make('master.inventory')
                            ->withMessages('suksesInsert')
                            ->withToogle('master')->withAktif('inventory');
        }
    }

    static function updateInventory() {
        //rule
        $rule = array(
            'InventoryName' => 'required|max:200',
            'Type' => 'required',
            'UoM' => 'required|max:100',
            'remark' => 'required|max:1000'
        );
        //validasi
        $validator = Validator ::make(Input::all(), $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('master.inventory')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('master')->withAktif('inventory');
        } else {
            //valid
            $inventory = Inventory::find(Input::get('InternalID'));
            if ($inventory->CompanyInternalID == Auth::user()->Company->InternalID) {
                $inventory->InventoryTypeInternalID = Input::get('Type');
                $inventory->InventoryName = Input::get('InventoryName');
                $inventory->UoM = Input::get('UoM');
                $inventory->MaxStock = str_replace(',', '', Input::get('MaxStock'));
                $inventory->MinStock = str_replace(',', '', Input::get('MinStock'));
                $inventory->UserModified = Auth::user()->UserID;
                $inventory->Remark = Input::get('remark');
                $inventory->save();
                return View::make('master.inventory')
                                ->withMessages('suksesUpdate')
                                ->withToogle('master')->withAktif('inventory');
            } else {
                return View::make('master.inventory')
                                ->withMessages('accessDenied')
                                ->withToogle('master')->withAktif('inventory');
            }
        }
    }

    static function deleteInventory() {
        //cek apakah ID Inventory ada di tabel sales detail atau tidak
        $sales = DB::table('t_sales_detail')->where('InventoryInternalID', Input::get('InternalID'))->first();
        //cek apakah ID Inventory ada di tabel sales return detail atau tidak
        $salesReturn = DB::table('t_salesreturn_detail')->where('InventoryInternalID', Input::get('InternalID'))->first();
        //cek apakah ID Inventory ada di tabel sales order detail atau tidak
        $salesOrder = DB::table('t_salesorder_detail')->where('InventoryInternalID', Input::get('InternalID'))->first();
        //cek apakah ID Inventory ada di tabel purchase detail atau tidak
        $purchase = DB::table('t_purchase_detail')->where('InventoryInternalID', Input::get('InternalID'))->first();
        //cek apakah ID Inventory ada di tabel purchase return detail atau tidak
        $purchaseReturn = DB::table('t_purchasereturn_detail')->where('InventoryInternalID', Input::get('InternalID'))->first();
        //cek apakah ID Inventory ada di tabel purchase order detail atau tidak
        $purchaseOrder = DB::table('t_purchaseorder_detail')->where('InventoryInternalID', Input::get('InternalID'))->first();
        //cek apakah ID Inventory ada di tabel purchase memoin atau tidak
        $memoIn = DB::table('t_memoin_detail')->where('InventoryInternalID', Input::get('InternalID'))->first();
        //cek apakah ID Inventory ada di tabel purchase memoout atau tidak
        $memoOut = DB::table('t_memoout_detail')->where('InventoryInternalID', Input::get('InternalID'))->first();
        //cek apakah ID Inventory ada di tabel purchase transfer atau tidak
        $transfer = DB::table('t_transfer_detail')->where('InventoryInternalID', Input::get('InternalID'))->first();
        if (is_null($sales) && is_null($salesReturn) && is_null($salesOrder) && is_null($purchase) && is_null($purchaseReturn) && is_null($purchaseOrder) && is_null($memoIn) && is_null($memoOut) && is_null($transfer)) {
            //tidak ada maka boleh dihapus
            $inventory = Inventory::find(Input::get('InternalID'));
            if ($inventory->CompanyInternalID == Auth::user()->Company->InternalID) {
                $inventory->delete();
                return View::make('master.inventory')
                                ->withMessages('suksesDelete')
                                ->withToogle('master')->withAktif('inventory');
            } else {
                return View::make('master.inventory')
                                ->withMessages('accessDenied')
                                ->withToogle('master')->withAktif('inventory');
            }
        } else {
            //ada maka tidak dihapus
            return View::make('master.inventory')
                            ->withMessages('gagalDelete')
                            ->withToogle('master')->withAktif('inventory');
        }
    }

    public function exportExcel() {
        Excel::create('Master_Inventory', function($excel) {
            $excel->sheet('Master_Inventory', function($sheet) {
                $sheet->mergeCells('B1:K1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Master Inventory");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Type");
                $sheet->setCellValueByColumnAndRow(3, 2, "Inventory Name");
                $sheet->setCellValueByColumnAndRow(4, 2, "Inventory ID");
                $sheet->setCellValueByColumnAndRow(5, 2, "UoM");
                $sheet->setCellValueByColumnAndRow(6, 2, "Min Stock");
                $sheet->setCellValueByColumnAndRow(7, 2, "Max Stock");
                $sheet->setCellValueByColumnAndRow(8, 2, "Record");
                $sheet->setCellValueByColumnAndRow(9, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(10, 2, "Remark");
                $row = 3;
                foreach (Inventory::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, $data->InventoryType->InventoryTypeName);
                    $sheet->setCellValueByColumnAndRow(3, $row, $data->InventoryName);
                    $sheet->setCellValueByColumnAndRow(4, $row, "`" . $data->InventoryID);
                    $sheet->setCellValueByColumnAndRow(5, $row, $data->UoM);
                    $sheet->setCellValueByColumnAndRow(6, $row, number_format($data->MinStock, 0, '.', ','));
                    $sheet->setCellValueByColumnAndRow(7, $row, number_format($data->MaxStock, 0, '.', ','));
                    $sheet->setCellValueByColumnAndRow(8, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(9, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(10, $row, $data->Remark);
                    $row++;
                }

                if (Inventory::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() <= 0) {
                    $sheet->mergeCells('B3:K3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "No data available in table");

                    $sheet->cells('B3:K3', function($cells) {
                        $cells->setAlignment('center');
                    });

                    $sheet->setBorder('B3:K' . $row, 'thin');
                }

                $row--;
                $sheet->setBorder('B2:K' . $row, 'thin');
                $sheet->cells('B2:K2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:K' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
                $sheet->cells('F3:F' . $row, function($cells) {
                    $cells->setAlignment('right');
                });
            });
        })->export('xls');
    }

    public function detailInventory() {
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $warehouse = Input::get('warehouse');
        if ($warehouse == '-1') {
            $delimiterWarehouse = '!=';
            $warehouseName = 'All';
        } else {
            $delimiterWarehouse = '=';
            $warehouseName = Warehouse::find($warehouse)->WarehouseName;
        }
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                               <img style="width: 1025px; height: 200px" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Inventory Detail Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Warehouse ' . $warehouseName . '</span><br>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period    : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span>
                            <table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Name</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Initial Stock</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Purchase</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Purchase Return</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales Return</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Memo In</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Memo Out</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Transfer In</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Transfer Out</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">End Stock</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
        if (Inventory::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() > 0) {
            foreach (Inventory::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                $pembelian = PurchaseDetail::
                        join('t_purchase_header', 't_purchase_header.InternalID', '=', 't_purchase_detail.PurchaseInternalID')
                        ->where('t_purchase_header.CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where('t_purchase_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                        ->where('t_purchase_detail.InventoryInternalID', $data->InternalID)
                        ->whereBetween('t_purchase_header.PurchaseDate', array($start, $end))
                        ->sum('Qty');
                $penjualan = SalesDetail::
                        join('t_sales_header', 't_sales_header.InternalID', '=', 't_sales_detail.SalesInternalID')
                        ->where('t_sales_header.CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where('t_sales_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                        ->where('t_sales_detail.InventoryInternalID', $data->InternalID)
                        ->whereBetween('t_sales_header.SalesDate', array($start, $end))
                        ->sum('Qty');
                $Rpembelian = PurchaseReturnDetail::
                        join('t_purchasereturn_header', 't_purchasereturn_header.InternalID', '=', 't_purchasereturn_detail.PurchaseReturnInternalID')
                        ->where('t_purchasereturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where('t_purchasereturn_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                        ->where('t_purchasereturn_detail.InventoryInternalID', $data->InternalID)
                        ->whereBetween('t_purchasereturn_header.PurchaseReturnDate', array($start, $end))
                        ->sum('Qty');
                $Rpenjualan = SalesReturnDetail::
                        join('t_salesreturn_header', 't_salesreturn_header.InternalID', '=', 't_salesreturn_detail.SalesReturnInternalID')
                        ->where('t_salesreturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where('t_salesreturn_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                        ->where('t_salesreturn_detail.InventoryInternalID', $data->InternalID)
                        ->whereBetween('t_salesreturn_header.SalesReturnDate', array($start, $end))
                        ->sum('Qty');
                $Min = MemoInDetail::
                        join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.MemoInInternalID')
                        ->where('t_memoin_header.CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where('t_memoin_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                        ->where('t_memoin_detail.InventoryInternalID', $data->InternalID)
                        ->whereBetween('t_memoin_header.MemoInDate', array($start, $end))
                        ->sum('Qty');
                $Mout = MemoOutDetail::
                        join('t_memoout_header', 't_memoout_header.InternalID', '=', 't_memoout_detail.MemoOutInternalID')
                        ->where('t_memoout_header.CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where('t_memoout_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                        ->where('t_memoout_detail.InventoryInternalID', $data->InternalID)
                        ->whereBetween('t_memoout_header.MemoOutDate', array($start, $end))
                        ->sum('Qty');
                $Tin = TransferDetail::
                        join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                        ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where('t_transfer_header.WarehouseDestinyInternalID', $delimiterWarehouse, $warehouse)
                        ->where('t_transfer_detail.InventoryInternalID', $data->InternalID)
                        ->whereBetween('t_transfer_header.TransferDate', array($start, $end))
                        ->sum('Qty');
                $Tout = TransferDetail::
                        join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                        ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where('t_transfer_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                        ->where('t_transfer_detail.InventoryInternalID', $data->InternalID)
                        ->whereBetween('t_transfer_header.TransferDate', array($start, $end))
                        ->sum('Qty');
                $initialStock = Inventory::getInitialStock($start, '<', $data->InternalID, $warehouse, $delimiterWarehouse);
                $endStock = $initialStock + $pembelian + $Rpenjualan - $penjualan - $Rpembelian + $Min - $Mout + $Tin - $Tout;
                $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->InventoryID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->InventoryName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($initialStock, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($pembelian, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($penjualan, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($Rpembelian, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($Rpenjualan, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($Min, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($Mout, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($Tin, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($Tout, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($endStock, '0', '.', ',')) . '</td>
                            </tr>';
            }
        } else {
            $html.= '<tr>
                            <td colspan="12" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: center">There is no inventory registered.</td>
                        </tr>';
        }

        $html.= '</tbody>
                            </div>
                    </div>
                </body>
            </html>';
        return PDF::load($html, 'A4', 'landscape')->download('inventory_detail');
    }

    public function summaryInventory() {
        $dateT = explode('-', Input::get('date'));
        $date = $dateT[2] . '-' . $dateT[1] . '-' . $dateT[0];
        $warehouse = Input::get('warehouse');
        if ($warehouse == '-1') {
            $delimiterWarehouse = '!=';
            $warehouseName = 'All';
        } else {
            $delimiterWarehouse = '=';
            $warehouseName = Warehouse::find($warehouse)->WarehouseName;
        }
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:80%; bottom: -63px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Inventory Summary Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Warehouse ' . $warehouseName . '</span><br>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('date'))) . '</span>
                            <table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Name</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">End Stock</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
        if (Inventory::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() > 0) {
            foreach (Inventory::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                $initialStock = Inventory::getInitialStock($date, '<=', $data->InternalID, $warehouse, $delimiterWarehouse);
                $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->InventoryID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->InventoryName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($initialStock, '0', '.', ',')) . '</td>
                            </tr>';
            }
        } else {
            $html.= '<tr>
                            <td colspan="3" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;  text-align: center">There is no inventory registered.</td>
                        </tr>';
        }

        $html.= '</tbody>
                            </div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('inventory_summary');
    }

    public function historyPrice($id) {
        $inv = Inventory::where('InventoryID', $id)->where('CompanyInternalID', Auth::user()->Company->InternalID)->first();
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">History Price - ' . $inv->InventoryName . ' (' . $inv->InventoryID . ')</h5>';
        $html.= '<table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Customer</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Currency</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Rate</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Qty</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Price</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc (%)</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
        if (SalesDetail::join('t_sales_header', 't_sales_header.InternalID', '=', 't_sales_detail.SalesInternalID')
                        ->where('InventoryInternalID', $inv->InternalID)->count() > 0) {
            foreach (SalesDetail::join('t_sales_header', 't_sales_header.InternalID', '=', 't_sales_detail.SalesInternalID')
                    ->where('InventoryInternalID', $inv->InternalID)->orderBy('SalesDate', 'desc')->get() as $data) {
                $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->SalesID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . Coa6::find($data->ACC6InternalID)->ACC6ID . ' ' . Coa6::find($data->ACC6InternalID)->ACC6Name . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . Currency::find($data->CurrencyInternalID)->CurrencyName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->CurrencyRate, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Discount . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->DiscountNominal . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
            }
        } else {
            $html.= '<tr>
                            <td colspan="9" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: center">There is no sales registered.</td>
                        </tr>';
        }

        $html.= '</tbody>
            </table>';
        $html.='</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('history_price');
    }

    public function stockInventory() {
        $inventory = Inventory::find(Input::get('InternalID'));
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $warehouse = Input::get('warehouse');
        if ($warehouse == '-1') {
            $delimiterWarehouse = '!=';
            $warehouseName = 'All';
        } else {
            $delimiterWarehouse = '=';
            $warehouseName = Warehouse::find($warehouse)->WarehouseName;
        }
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img style="width: 1025px; height: 200px" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Inventory ' . $inventory->InventoryName . ' Stock Card</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Warehouse ' . $warehouseName . '</span><br>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period    : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span>
                            <table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Transaction ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Qty</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Price</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc (%)</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
        $count = 0;
        foreach (PurchaseDetail::join('t_purchase_header', 't_purchase_header.InternalID', '=', 't_purchase_detail.PurchaseInternalID')
                ->where('CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('InventoryInternalID', $inventory->InternalID)
                ->where('t_purchase_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                ->whereBetween('PurchaseDate', Array($start, $end))
                ->select('t_purchase_detail.Remark as Remarks', 't_purchase_detail.*', 't_purchase_header.*')
                ->orderBy('PurchaseDate')->get() as $data) {
            $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->PurchaseID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->PurchaseDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>';
            if ($data->Remarks == '') {
                $html.='<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>';
            } else {
                $html.='<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . str_replace('+', ' + ', $data->Remarks) . '</td>';
            }
            $html.='<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
            $count++;
        }
        foreach (PurchaseReturnDetail::join('t_purchasereturn_header', 't_purchasereturn_header.InternalID', '=', 't_purchasereturn_detail.PurchaseReturnInternalID')
                ->where('CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('InventoryInternalID', $inventory->InternalID)
                ->where('t_purchasereturn_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                ->whereBetween('PurchaseReturnDate', Array($start, $end))
                ->select('t_purchasereturn_detail.Remark as Remarks', 't_purchasereturn_detail.*', 't_purchasereturn_header.*')
                ->orderBy('PurchaseReturnDate')->get() as $data) {
            $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->PurchaseReturnID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->PurchaseReturnDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>';
            if ($data->Remarks == '') {
                $html.='<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Discount . ' ' . '</td>';
            } else {
                $html.='<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . str_replace('+', ' + ', $data->Remarks) . '</td>';
            }
            $html.='<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
            $count++;
        }
        foreach (SalesDetail::join('t_sales_header', 't_sales_header.InternalID', '=', 't_sales_detail.SalesInternalID')
                ->where('CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('InventoryInternalID', $inventory->InternalID)
                ->where('t_sales_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                ->whereBetween('SalesDate', Array($start, $end))
                ->select('t_sales_detail.Remark as Remarks', 't_sales_detail.*', 't_sales_header.*')
                ->orderBy('SalesDate')->get() as $data) {
            $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->SalesID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->SalesDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>';
            if ($data->Remarks == '') {
                $html.='<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Discount . ' ' . '</td>';
            } else {
                $html.='<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . str_replace('+', ' + ', $data->Remarks) . '</td>';
            }
            $html.='<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
            $count++;
        }
        foreach (SalesReturnDetail::join('t_salesreturn_header', 't_salesreturn_header.InternalID', '=', 't_salesreturn_detail.SalesReturnInternalID')
                ->where('CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('InventoryInternalID', $inventory->InternalID)
                ->where('t_salesreturn_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                ->whereBetween('SalesReturnDate', Array($start, $end))
                ->select('t_salesreturn_detail.Remark as Remarks', 't_salesreturn_detail.*', 't_salesreturn_header.*')
                ->orderBy('SalesReturnDate')->get() as $data) {
            $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->SalesReturnID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->SalesReturnDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>';
            if ($data->Remarks == '') {
                $html.='<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Discount . ' ' . '</td>';
            } else {
                $html.='<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . str_replace('+', ' + ', $data->Remarks) . '</td>';
            }
            $html.='<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
            $count++;
        }
        foreach (MemoOutDetail::join('t_memoout_header', 't_memoout_header.InternalID', '=', 't_memoout_detail.MemoOutInternalID')
                ->where('CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('InventoryInternalID', $inventory->InternalID)
                ->where('t_memoout_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                ->whereBetween('MemoOutDate', Array($start, $end))
                ->select('t_memoout_detail.Remark as Remarks', 't_memoout_detail.*', 't_memoout_header.*')
                ->orderBy('MemoOutDate')->get() as $data) {
            $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->MemoOutID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->MemoOutDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right"></td>';
            $html.='<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right"></td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
            $count++;
        }
        foreach (MemoInDetail::join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.MemoInInternalID')
                ->where('CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('InventoryInternalID', $inventory->InternalID)
                ->where('t_memoin_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                ->whereBetween('MemoInDate', Array($start, $end))
                ->select('t_memoin_detail.Remark as Remarks', 't_memoin_detail.*', 't_memoin_header.*')
                ->orderBy('MemoInDate')->get() as $data) {
            $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->MemoInID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->MemoInDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right"></td>';
            $html.='<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right"></td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
            $count++;
        }
        foreach (TransferDetail::join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                ->where('CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('InventoryInternalID', $inventory->InternalID)
                ->where('t_transfer_header.WarehouseDestinyInternalID', $delimiterWarehouse, $warehouse)
                ->whereBetween('TransferDate', Array($start, $end))
                ->select('t_transfer_detail.Remark as Remarks', 't_transfer_detail.*', 't_transfer_header.*')
                ->orderBy('TransferDate')->get() as $data) {
            $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">In - ' . $data->TransferID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->TransferDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right"></td>';
            $html.='<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right"></td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
            $count++;
        }
        foreach (TransferDetail::join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                ->where('CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('InventoryInternalID', $inventory->InternalID)
                ->where('t_transfer_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                ->whereBetween('TransferDate', Array($start, $end))
                ->select('t_transfer_detail.Remark as Remarks', 't_transfer_detail.*', 't_transfer_header.*')
                ->orderBy('TransferDate')->get() as $data) {
            $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">Out - ' . $data->TransferID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->TransferDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right"></td>';
            $html.='<td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right"></td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
            $count++;
        }

        if ($count == 0) {
            $html.= '<tr>
                            <td colspan="7" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: center">There is no inventory registered.</td>
                        </tr></tbody>';
        } else {
            $html.= '</tbody></table>';
            $html.='<h5>Summary</h5>';
            $html.='<table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Name</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Initial Stock</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Purchase</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Purchase Return</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales Return</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Memo In</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Memo Out</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Transfer In</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Transfer Out</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">End Stock</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            foreach (Inventory::where('InternalID', $inventory->InternalID)->get() as $data) {
                $pembelian = PurchaseDetail::
                        join('t_purchase_header', 't_purchase_header.InternalID', '=', 't_purchase_detail.PurchaseInternalID')
                        ->where('t_purchase_header.CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where('t_purchase_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                        ->where('t_purchase_detail.InventoryInternalID', $data->InternalID)
                        ->whereBetween('t_purchase_header.PurchaseDate', array($start, $end))
                        ->sum('Qty');
                $penjualan = SalesDetail::
                        join('t_sales_header', 't_sales_header.InternalID', '=', 't_sales_detail.SalesInternalID')
                        ->where('t_sales_header.CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where('t_sales_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                        ->where('t_sales_detail.InventoryInternalID', $data->InternalID)
                        ->whereBetween('t_sales_header.SalesDate', array($start, $end))
                        ->sum('Qty');
                $Rpembelian = PurchaseReturnDetail::
                        join('t_purchasereturn_header', 't_purchasereturn_header.InternalID', '=', 't_purchasereturn_detail.PurchaseReturnInternalID')
                        ->where('t_purchasereturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where('t_purchasereturn_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                        ->where('t_purchasereturn_detail.InventoryInternalID', $data->InternalID)
                        ->whereBetween('t_purchasereturn_header.PurchaseReturnDate', array($start, $end))
                        ->sum('Qty');
                $Rpenjualan = SalesReturnDetail::
                        join('t_salesreturn_header', 't_salesreturn_header.InternalID', '=', 't_salesreturn_detail.SalesReturnInternalID')
                        ->where('t_salesreturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where('t_salesreturn_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                        ->where('t_salesreturn_detail.InventoryInternalID', $data->InternalID)
                        ->whereBetween('t_salesreturn_header.SalesReturnDate', array($start, $end))
                        ->sum('Qty');
                $Min = MemoInDetail::
                        join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.MemoInInternalID')
                        ->where('t_memoin_header.CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where('t_memoin_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                        ->where('t_memoin_detail.InventoryInternalID', $data->InternalID)
                        ->whereBetween('t_memoin_header.MemoInDate', array($start, $end))
                        ->sum('Qty');
                $Mout = MemoOutDetail::
                        join('t_memoout_header', 't_memoout_header.InternalID', '=', 't_memoout_detail.MemoOutInternalID')
                        ->where('t_memoout_header.CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where('t_memoout_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                        ->where('t_memoout_detail.InventoryInternalID', $data->InternalID)
                        ->whereBetween('t_memoout_header.MemoOutDate', array($start, $end))
                        ->sum('Qty');
                $Tin = TransferDetail::
                        join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                        ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where('t_transfer_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                        ->where('t_transfer_detail.InventoryInternalID', $data->InternalID)
                        ->whereBetween('t_transfer_header.TransferDate', array($start, $end))
                        ->sum('Qty');
                $Tout = TransferDetail::
                        join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                        ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where('t_transfer_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                        ->where('t_transfer_detail.InventoryInternalID', $data->InternalID)
                        ->whereBetween('t_transfer_header.TransferDate', array($start, $end))
                        ->sum('Qty');
                $initialStock = Inventory::getInitialStock($start, '<', $data->InternalID, $warehouse, $delimiterWarehouse);
                $endStock = $initialStock + $pembelian + $Rpenjualan - $penjualan - $Rpembelian + $Min - $Mout + $Tin - $Tout;
                $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->InventoryID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->InventoryName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($initialStock, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($pembelian, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($penjualan, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($Rpembelian, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($Rpenjualan, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($Min, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($Mout, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($Tin, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($Tout, '0', '.', ',')) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . convertNol(number_format($endStock, '0', '.', ',')) . '</td>
                            </tr>';
            }
            $html.='</tbody></table>';
        }

        $html.= '
                            </div>
                    </div>
                </body>
            </html>';
        return PDF::load($html, 'A4', 'landscape')->download('inventory_stock_report');
    }

    public function reportStock() {
        $dateT = explode('-', Input::get('date'));
        $date = $dateT[2] . '-' . $dateT[1] . '-' . $dateT[0];
        $warehouse = Input::get('warehouse');
        if ($warehouse == '-1') {
            $delimiterWarehouse = '!=';
            $warehouseName = 'All';
        } else {
            $delimiterWarehouse = '=';
            $warehouseName = Warehouse::find($warehouse)->WarehouseName;
        }
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:80%; bottom: -63px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Buffer Stock Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Warehouse ' . $warehouseName . '</span><br>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('date'))) . '</span>
                            <table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Name</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Min Stock</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Max Stock</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Stock</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
        if (Inventory::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() > 0) {
            $countMax = 0;
            $countMin = 0;
            foreach (Inventory::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $value) {
                $totalPieces = 0;
                $color = "";
                $initialStockTamp = Inventory::getInitialStock($date, '<=', $value->InternalID, $warehouse, $delimiterWarehouse);
                $totalPieces = $initialStockTamp;
                if ((int) $value->MinStock > (int) $totalPieces) {
                    $countMin++;
                    $color = 'bgcolor="#f2dede"';
                } else if ((int) $value->MaxStock < (int) $totalPieces) {
                    $countMax++;
                    $color = 'bgcolor="#dff0d8"';
                }

                $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; ">' . $value->InventoryID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; ">' . $value->InventoryName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; ">' . number_format($value->MinStock, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; ">' . number_format($value->MaxStock, 0, '.', ',') . '</td>
                                <td ' . $color . ' style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right; ">' . number_format($totalPieces, 0, '.', ',') . '</td>
                            </tr>';
            }
            $html.= '<tr>
                        <td colspan="4" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; color: red; text-align: right;"> * Warning Stock </td>
                        <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;  text-align: right">' . $countMin . '</td>
                    </tr>
                    <tr>
                        <td colspan="4" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; color: green; text-align: right;"> * Exceed Stock </td>
                        <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;  text-align: right">' . $countMax . '</td>
                    </tr>';
        } else {
            $html.= '<tr>
                            <td colspan="5" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;  text-align: center">There is no inventory registered.</td>
                        </tr>';
        }

        $html.= '</tbody>
                            </div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('buffer_stock_report');
    }

    static function inventoryDataBackup() {
        $table = 'm_inventory';

        $primaryKey = 'm_inventory`.`InternalID';

        $columns = array(
            array('db' => 'InventoryID', 'dt' => 0),
            array(
                'db' => 'InventoryTypeName',
                'dt' => 1,
                'formatter' => function( $d, $row ) {
                    return $d;
                }
            ),
            array('db' => 'InventoryName', 'dt' => 2),
            array('db' => 'UoM', 'dt' => 3),
            array(
                'db' => 'InitialValue',
                'dt' => 4,
                'formatter' => function( $d, $row ) {
                    return number_format($d, 2, '.', ',');
                }
            ),
            array(
                'db' => 'InitialQuantity',
                'dt' => 5,
                'formatter' => function( $d, $row ) {
                    return number_format($d, 0, '.', ',');
                }
            ),
            array('db' => 'm_inventory`.`InternalID', 'dt' => 6, 'formatter' => function( $d, $row ) {
                    $inventory = Inventory::find($d);
                    $inventory->dtRecordformat = date("d-m-Y H:i:s", strtotime($inventory->dtRecord));
                    $inventory->dtModifformat = date("d-m-Y H:i:s", strtotime($inventory->dtModified));
                    $inventory->Remark = str_replace("\r\n", " ", $inventory->Remark);
                    $arrData = array($inventory);
                    $tamp = myEscapeStringData($arrData);

                    $return = '<button id="btn-' . $inventory->InventoryID . '" data-target="#m_inventoryUpdate"';
                    $return .= "data-all='" . $tamp . "'";
                    $return .= 'data-toggle="modal" role="dialog" onclick="updateAttach(this)"
                                                                class="btn btn-pure-xs btn-xs btn-edit">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </button>
                                    <button data-target="#m_inventoryDelete" data-internal="' . $inventory->InternalID . '"  data-toggle="modal" role="dialog" onclick="deleteAttach(this)"
                                            data-id="' . $inventory->InventoryID . '" data-name="' . $inventory->InventoryName . '" class="btn btn-pure-xs btn-xs btn-delete">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>';
                    if (checkModul('O04')) {
                        $return.='<a href="' . Route('historyPrice', $inventory->InventoryID) . '" target="_blank">
                                        <button id="btn-' . $inventory->InventoryID . '-print"
                                                class="btn btn-pure-xs btn-xs btn-price">
                                            History
                                        </button>
                                    </a>
                                    <button data-target="#r_stock" data-internal="' . $inventory->InternalID . '"  data-toggle="modal" role="dialog" onclick="stockAttach(this)"
                                            data-id="' . $inventory->InventoryID . '" class="btn btn-pure-xs btn-xs btn-stock">
                                        stock
                                    </button>';
                    }
                    return $return;
                },
                        'field' => 'm_inventory`.`InternalID')
                );

                $sql_details = getConnection();

                require('ssp.class.php');
                $ID_CLIENT_VALUE = Auth::user()->CompanyInternalID;
                $extraCondition = "m_inventory.`CompanyInternalID`=".$ID_CLIENT_VALUE;
                $join = ' INNER JOIN m_inventorytype on m_inventorytype.InternalID = m_inventory.InventoryTypeInternalID';

                echo json_encode(
                        SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $extraCondition, $join));
            }

            public function checkInventoryID() {
                $InventoryName = "";
                $inventory = Inventory::where("InventoryID", Input::get("id"))->where("CompanyInternalID", Auth::user()->Company->InternalID)->count();
                if ($inventory > 0) {
                    return "1";
                } else {
                    return "0";
                }
            }

        }
        