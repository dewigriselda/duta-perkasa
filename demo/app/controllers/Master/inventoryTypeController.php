<?php

class InventoryTypeController extends BaseController {

    public function showInventoryType() {
        if ($_SERVER["REQUEST_METHOD"] === 'POST') {
            if (Input::get('jenis') == 'insertInventoryType') {
                return $this->insertInventoryType();
            }
            if (Input::get('jenis') == 'updateInventoryType') {
                return $this->updateInventoryType();
            }
            if (Input::get('jenis') == 'deleteInventoryType') {
                return $this->deleteInventoryType();
            }
            if (Input::get('jenis') == 'exportInventoryType') {
                return $this->exportExcel();
            }
        }
        return View::make('master.inventoryType')
                        ->withToogle('master')
                        ->withAktif('inventoryType');
    }

    public static function insertInventoryType() {
        //rule
        $rule = array(
            'InventoryTypeID' => 'required|max:200|unique:m_inventorytype,InventoryTypeID,NULL,InventoryTypeID,CompanyInternalID,' . Auth::user()->Company->InternalID . '',
            'InventoryTypeName' => 'required|max:200',
            'coa' => 'required',
            'Type' => 'required',
            'remark' => 'required|max:1000'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('master.inventoryType')
                            ->withMessages('gagalInsert')
                            ->withError($validator->messages())
                            ->withToogle('master')->withAktif('inventoryType')
                            ->withErrors($validator);
        } else {
            //valid
            $inventory = new InventoryType;
            $inventory->InventoryTypeID = Input::get('InventoryTypeID');
            $inventory->InventoryTypeName = Input::get('InventoryTypeName');
            $inventory->Flag = Input::get('Type');
            $coa = Coa::find(Input::get('coa'));
            $inventory->ACC1InternalID = $coa->ACC1InternalID;
            $inventory->ACC2InternalID = $coa->ACC2InternalID;
            $inventory->ACC3InternalID = $coa->ACC3InternalID;
            $inventory->ACC4InternalID = $coa->ACC4InternalID;
            $inventory->ACC5InternalID = $coa->ACC5InternalID;
            $inventory->ACC6InternalID = $coa->ACC6InternalID;
            $inventory->UserRecord = Auth::user()->UserID;
            $inventory->CompanyInternalID = Auth::user()->Company->InternalID;
            $inventory->UserModified = "0";
            $inventory->Remark = Input::get('remark');
            $inventory->save();

            return View::make('master.inventoryType')
                            ->withMessages('suksesInsert')
                            ->withToogle('master')->withAktif('inventoryType');
        }
    }

    static function updateInventoryType() {
        //rule
        $rule = array(
            'InventoryTypeName' => 'required|max:200',
            'coa' => 'required',
            'Type' => 'required',
            'remark' => 'required|max:1000'
        );
        //validasi
        $validator = Validator ::make(Input::all(), $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('master.inventoryType')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('master')->withAktif('inventoryType');
        } else {
            //valid
            $inventory = InventoryType::find(Input::get('InternalID'));
            if ($inventory->CompanyInternalID == Auth::user()->Company->InternalID) {
                $inventory->InventoryTypeName = Input::get('InventoryTypeName');
                $inventory->Flag = Input::get('Type');
                $coa = Coa::find(Input::get('coa'));
                $inventory->ACC1InternalID = $coa->ACC1InternalID;
                $inventory->ACC2InternalID = $coa->ACC2InternalID;
                $inventory->ACC3InternalID = $coa->ACC3InternalID;
                $inventory->ACC4InternalID = $coa->ACC4InternalID;
                $inventory->ACC5InternalID = $coa->ACC5InternalID;
                $inventory->ACC6InternalID = $coa->ACC6InternalID;
                $inventory->UserModified = Auth::user()->UserID;
                $inventory->Remark = Input::get('remark');
                $inventory->save();
                return View::make('master.inventoryType')
                                ->withMessages('suksesUpdate')
                                ->withToogle('master')->withAktif('inventoryType');
            } else {
                return View::make('master.inventoryType')
                                ->withMessages('accessDenied')
                                ->withToogle('master')->withAktif('inventoryType');
            }
        }
    }

    static function deleteInventoryType() {
        $Inventory = DB::table('m_inventory')->where('InventoryTypeInternalID', Input::get('InternalID'))->first();
        if (is_null($Inventory)) {
            //tidak ada maka boleh hapus
            $inventoryType = InventoryType::find(Input::get('InternalID'));
            if ($inventoryType->CompanyInternalID == Auth::user()->Company->InternalID) {
                $inventoryType->delete();
                return View::make('master.inventoryType')
                                ->withMessages('suksesDelete')
                                ->withToogle('master')->withAktif('inventoryType');
            } else {
                return View::make('master.inventoryType')
                                ->withMessages('accessDenied')
                                ->withToogle('master')->withAktif('inventoryType');
            }
        } else {
            //ada maka tidak dihapus
            return View::make('master.inventoryType')
                            ->withMessages('gagalDelete')
                            ->withToogle('master')->withAktif('inventoryType');
        }
    }

    public function exportExcel() {
        Excel::create('Master_Inventory_Type', function($excel) {
            $excel->sheet('Master_Inventory_Type', function($sheet) {
                $sheet->mergeCells('B1:I1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Master Inventory");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Inventory Name");
                $sheet->setCellValueByColumnAndRow(3, 2, "Inventory ID");
                $sheet->setCellValueByColumnAndRow(4, 2, "Account ID");
                $sheet->setCellValueByColumnAndRow(5, 2, "Account Name");
                $sheet->setCellValueByColumnAndRow(6, 2, "Record");
                $sheet->setCellValueByColumnAndRow(7, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(8, 2, "Remark");
                $row = 3;
                foreach (InventoryType::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, $data->InventoryTypeName);
                    $sheet->setCellValueByColumnAndRow(3, $row, "`" . $data->InventoryTypeID);
                    $sheet->setCellValueByColumnAndRow(4, $row, "`" . Coa::FormatCoa($data->ACC1InternalID, $data->ACC2InternalID, $data->ACC3InternalID, $data->ACC4InternalID, $data->ACC5InternalID, $data->ACC6InternalID, '1'));
                    $coa = InventoryType::coaName($data->InternalID);
                    $data->COAName = $coa[0]->COAName;
                    $sheet->setCellValueByColumnAndRow(5, $row, $data->COAName);
                    $sheet->setCellValueByColumnAndRow(6, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(7, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(8, $row, $data->Remark);
                    $row++;
                }

                if (InventoryType::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() <= 0) {
                    $sheet->mergeCells('B3:I3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "No data available in table");

                    $sheet->cells('B3:I3', function($cells) {
                        $cells->setAlignment('center');
                    });
                    $sheet->setBorder('B3:I' . $row, 'thin');
                }

                $row--;
                $sheet->setBorder('B2:I' . $row, 'thin');
                $sheet->cells('B2:I2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:I' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
            });
        })->export('xls');
    }

}
