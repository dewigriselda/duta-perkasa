<?php

class InventoryUomController extends BaseController {

    public function showInventoryUom() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertInventoryUom') {
                return $this->insertInventoryUom();
            }
            if (Input::get('jenis') == 'updateInventoryUom') {
                return $this->updateInventoryUom();
            }
            if (Input::get('jenis') == 'deleteInventoryUom') {
                return $this->deleteInventoryUom();
            }
        }
        return View::make('master.inventoryUom')
                        ->withToogle('master')->withAktif('inventoryUom');
    }

    public static function insertInventoryUom() {
        //rule
        $rule = array(
            'InventoryInternalID' => 'required',
            'UomInternalID' => 'required',
            'Default' => 'required|Integer|max:1',
            'Value' => 'numeric|required',
            'remark' => 'required|max:1000'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('master.inventoryUom')
                            ->withMessages('gagalInsert')
                            ->withErrors($validator->messages())
                            ->withToogle('master')->withAktif('inventoryUom');
        } else {
            //valid
            $InventoryUom = new InventoryUom;
            $InventoryUom->InventoryInternalID = Input::get('InventoryInternalID');
            $InventoryUom->UomInternalID = Input::get('UomInternalID');
            $InventoryUom->Default = Input::get('Default');
            if (Input::get('Default') == '1') {
                InventoryUom::where('Default', '=', 1)->where("InventoryInternalID", Input::get('InventoryInternalID'))->where('CompanyInternalID', Auth::user()->Company->InternalID)->update(array('Default' => 0));
            }
            $InventoryUom->Value = Input::get('Value');
            $InventoryUom->UserRecord = Auth::user()->UserID;
            $InventoryUom->CompanyInternalID = Auth::user()->Company->InternalID;
            $InventoryUom->UserModified = "0";
            $InventoryUom->Remark = Input::get('remark');
            $InventoryUom->save();

            return View::make('master.inventoryUom')
                            ->withMessages('suksesInsert')
                            ->withToogle('master')->withAktif('inventoryUom');
        }
    }

    static function updateInventoryUom() {
        //rule
        $rule = array(
            'InventoryInternalID' => 'required',
            'UomInternalID' => 'required',
            'Default' => 'required|Integer|max:1',
            'Value' => 'numeric|required',
            'remark' => 'required|max:1000'
        );
        //validasi
        $validator = Validator ::make(Input::all(), $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('master.inventoryUom')
                            ->withMessages('gagalInsert')
                            ->withErrors($validator->messages())
                            ->withToogle('master')->withAktif('inventoryUom');
        } else {
            //valid
            $InventoryUom = InventoryUom::find(Input::get('InternalID'));
            if ($InventoryUom->CompanyInternalID == Auth::user()->Company->InternalID) {
                $InventoryUom->Default = Input::get('Default');
                if (Input::get('Default') == '1') {
                    InventoryUom::where('Default', '=', 1)->where("InventoryInternalID", Input::get('InventoryInternalID'))->where('CompanyInternalID', Auth::user()->Company->InternalID)->update(array('Default' => 0));
                }
                if ($InventoryUom->Default == '1') {
                    return View::make('master.inventoryUom')
                                    ->withMessages('gagalDefault')
                                    ->withToogle('master')->withAktif('inventoryUom');
                }
                $InventoryUom->InventoryInternalID = Input::get('InventoryInternalID');
                $InventoryUom->UomInternalID = Input::get('UomInternalID');
                $InventoryUom->Value = Input::get('Value');
                $InventoryUom->UserModified = Auth::user()->UserID;
                $InventoryUom->Remark = Input::get('remark');
                $InventoryUom->save();
                return View::make('master.inventoryUom')
                                ->withMessages('suksesUpdate')
                                ->withToogle('master')->withAktif('inventoryUom');
            } else {
                return View::make('master.inventoryUom')
                                ->withMessages('accessDenied')
                                ->withToogle('master')->withAktif('inventoryUom');
            }
        }
    }

    static function deleteInventoryUom() {
        //tidak ada maka boleh dihapus
        $InventoryUom = InventoryUom::find(Input::get('InternalID'));
        if ($InventoryUom->CompanyInternalID == Auth::user()->Company->InternalID) {
            $InventoryUom->delete();
            return View::make('master.inventoryUom')
                            ->withMessages('suksesDelete')
                            ->withToogle('master')->withAktif('inventoryUom');
        } else {
            return View::make('master.inventoryUom')
                            ->withMessages('accessDenied')
                            ->withToogle('master')->withAktif('inventoryUom');
        }
    }

    public function exportInventoryUom() {
        Excel::create('Master_Inventory_Uom', function($excel) {
            $excel->sheet('Master_Inventory_Uom', function($sheet) {
                $sheet->mergeCells('B1:I1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Master Inventory Uom");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Inventory");
                $sheet->setCellValueByColumnAndRow(3, 2, "Uom ID");
                $sheet->setCellValueByColumnAndRow(4, 2, "Default");
                $sheet->setCellValueByColumnAndRow(5, 2, "Value");
                $sheet->setCellValueByColumnAndRow(6, 2, "Record");
                $sheet->setCellValueByColumnAndRow(7, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(8, 2, "Remark");
                $row = 3;
                foreach (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                    $default = '';
                    if ($data->Default == 1) {
                        $default = '(Default)';
                    }
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, $data->inventory->InventoryName . ' ' . $default);
                    $sheet->setCellValueByColumnAndRow(3, $row, $data->uom->UomID);
                    $sheet->setCellValueByColumnAndRow(4, $row, $default);
                    $sheet->setCellValueByColumnAndRow(5, $row, number_format($data->Value, 2, '.', ','));
                    $sheet->setCellValueByColumnAndRow(6, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(7, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(8, $row, $data->Remark);
                    $row++;
                }

                if (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() <= 0) {
                    $sheet->mergeCells('B3:I3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "No data available in table");

                    $sheet->cells('B3:I3', function($cells) {
                        $cells->setAlignment('center');
                    });

                    $sheet->setBorder('B3:I' . $row, 'thin');
                }

                $row--;
                $sheet->setBorder('B2:I' . $row, 'thin');
                $sheet->cells('B2:I2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:I' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('E3:E' . $row, function($cells) {
                    $cells->setAlignment('right');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
            });
        })->export('xls');
    }

}
