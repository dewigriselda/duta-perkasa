<?php

class RegionController extends BaseController {

    public function showRegion() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertRegion') {
                return $this->insertRegion();
            }
            if (Input::get('jenis') == 'updateRegion') {
                return $this->updateRegion();
            }
            if (Input::get('jenis') == 'deleteRegion') {
                return $this->deleteRegion();
            }
        }
        return View::make('master.region')
                        ->withToogle('master')->withAktif('region');
    }

    public static function insertRegion() {
        //rule
        $rule = array(
            'RegionID' => 'required|max:200|unique:m_region,RegionID',
            'RegionName' => 'required|max:200',
            'currency' => 'required',
            'remark' => 'required|max:1000'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('master.region')
                            ->withMessages('gagalInsert')
                            ->withError($validator->messages())
                            ->withToogle('master')->withAktif('region')
                            ->withErrors($validator);
        } else {
            //valid
            $region = new Region;
            $region->RegionID = Input::get('RegionID');
            $region->RegionName = Input::get('RegionName');
            $region->UserRecord = Auth::user()->UserID;
            $region->CurrencyInternalID = Input::get('currency');
            $region->UserModified = "0";
            $region->Remark = Input::get('remark');
            $region->save();

            return View::make('master.region')
                            ->withMessages('suksesInsert')
                            ->withToogle('master')->withAktif('region');
        }
    }

    static function updateRegion() {
        //rule
        $rule = array(
            'RegionName' => 'required|max:200',
            'currency' => 'required',
            'remark' => 'required|max:1000'
        );
        //validasi
        $validator = Validator ::make(Input::all(), $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('master.region')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('master')->withAktif('region');
        } else {
            //valid
            $region = Region::find(Input::get('InternalID'));
            $region->RegionName = Input::get('RegionName');
            $region->CurrencyInternalID = Input::get('currency');
            $region->UserModified = Auth::user()->UserID;
            $region->Remark = Input::get('remark');
            $region->save();
            return View::make('master.region')
                            ->withMessages('suksesUpdate')
                            ->withToogle('master')->withAktif('region');
        }
    }

    static function deleteRegion() {
        $company = DB::table('m_company')->where('RegionInternalID', Input::get('InternalID'))->first();
        //cek region ada di journal atau tidak
        if (is_null($company)) {
            //tidak ada maka boleh dihapus
            $region = Region::find(Input::get('InternalID'));
            $region->delete();
            return View::make('master.region')
                            ->withMessages('suksesDelete')
                            ->withToogle('master')->withAktif('region');
        } else {
            //ada maka tidak dihapus
            return View::make('master.region')
                            ->withMessages('gagalDelete')
                            ->withToogle('master')->withAktif('region');
        }
    }

    public function exportExcel() {
        Excel::create('Master_Region', function($excel) {
            $excel->sheet('Master_Region', function($sheet) {
                $sheet->mergeCells('B1:H1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Master Region");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "Region Name");
                $sheet->setCellValueByColumnAndRow(3, 2, "Region ID");
                $sheet->setCellValueByColumnAndRow(4, 2, "Currency");
                $sheet->setCellValueByColumnAndRow(5, 2, "Record");
                $sheet->setCellValueByColumnAndRow(6, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(7, 2, "Remark");
                $row = 3;
                foreach (Region::all() as $data) {
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, $data->RegionName);
                    $sheet->setCellValueByColumnAndRow(3, $row, "`" . $data->RegionID);
                    $sheet->setCellValueByColumnAndRow(4, $row, $data->Currency->CurrencyName);
                    $sheet->setCellValueByColumnAndRow(5, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(6, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(7, $row, $data->Remark);
                    $row++;
                }
                $row--;
                $sheet->setBorder('B2:H' . $row, 'thin');
                $sheet->cells('B2:H2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:H' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
            });
        })->export('xls');
    }

}
