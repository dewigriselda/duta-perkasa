<?php

class UserController extends BaseController {

    public function showUser() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertUser') {
                return $this->insertUser();
            }
            if (Input::get('jenis') == 'updateUser') {
                return $this->updateUser();
            }
            if (Input::get('jenis') == 'activeUser') {
                return $this->activeUser();
            }
            if (Input::get('jenis') == 'nonActiveUser') {
                return $this->nonActiveUser();
            }
            if (Input::get('jenis') == 'deleteUser') {
                return $this->deleteUser();
            }
        }
        return View::make('master.user')
                        ->withToogle('master')->withAktif('user');
    }

    public function showUserMatrix($userID) {
        $user = User::where("UserID", $userID)->where("CompanyInternalID", Auth::user()->Company->InternalID)->first();
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertUserMatrix') {
                return $this->insertUserMatrix($user);
            }
        }
        return View::make('master.userMatrix')
                        ->withToogle('master')
                        ->withAktif('user')
                        ->withUser($user);
    }

    public function settProfile() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'updateProfile') {
                return $this->updateProfile();
            } else {
                return $this->updatePassword();
            }
        }
        return View::make('setting.profile')
                        ->withToogle('setting')->withAktif('profile');
    }

    public static function insertUser() {
        //rule
        $rule = array(
            'UserID' => 'required|max:16|unique:m_user,UserID',
            'Name' => 'required|max:200',
            'Password' => 'required|max:16',
            'Phone' => 'required|max:200',
            'Email' => 'required|email|max:200|unique:m_user,Email',
            'remark' => 'required|max:1000'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('master.user')
                            ->withMessages('gagalInsert')
                            ->withError($validator->messages())
                            ->withToogle('master')->withAktif('user')
                            ->withErrors($validator);
        } else {
            //valid
            $user = new User;
            $user->UserID = Input::get('UserID');
            $user->UserName = Input::get('Name');
            $user->Phone = Input::get('Phone');
            $user->Email = Input::get('Email');
            $user->UserPwd = Hash::make(Input::get('Password'));
            if (Auth::user()->Company->InternalID == '-1') {
                $user->CompanyInternalID = Input::get('company');
            } else {
                $user->CompanyInternalID = Auth::user()->Company->InternalID;
            }
            $user->Picture = NULL;
            $user->Status = 1;
            $user->UserRecord = Auth::user()->UserID;
            $user->UserModified = "0";
            $user->Remark = Input::get('remark');
            $user->save();

            if (Auth::user()->Company->PackageInternalID == 1) {
                $array = explode(',', Package::find(1)->MatrixNotIn);
                foreach (Matrix::whereNotIn('MatrixID', $array)->get() as $data) {
                    $userDetail = new UserDetail();
                    $userDetail->UserInternalID = $user->InternalID;
                    $userDetail->MatrixInternalID = $data->InternalID;
                    $userDetail->save();
                }
            }

            return View::make('master.user')
                            ->withMessages('suksesInsert')
                            ->withToogle('master')->withAktif('user');
        }
    }

    public static function insertUserMatrix($user) {
        $dataMatrix = Input::get("matrix");
        UserDetail::where("UserInternalID", $user->InternalID)->delete();

        if (!is_null($dataMatrix)) {
            foreach ($dataMatrix as $data) {
                $userDetail = new UserDetail();
                $userDetail->UserInternalID = $user->InternalID;
                $userDetail->MatrixInternalID = $data;
                $userDetail->save();
            }
        }
        return View::make('master.userMatrix')
                        ->withToogle('master')
                        ->withAktif('user')
                        ->withUser($user)
                        ->withMessages("suksesUpdate");
    }

    static function updateUser() {
        //rule
        $rule = array(
            'Name' => 'required|max:200',
            'remark' => 'required|max:1000',
            'Phone' => 'required|max:200'
        );
        //cek Email sama enggak
        $emailLama = User::find(Input::get('InternalID'))->Email;
        $data = Input::all();
        if (strtoupper(Input::get('Email')) == strtoupper($emailLama)) {
            $rule2 = array(
                'Email' => 'required|max:200|email'
            );
            $rule = array_merge($rule, $rule2);
        } else {
            $rule2 = array(
                'Email' => 'required|max:200|email|unique:m_user,Email'
            );
            $rule = array_merge($rule, $rule2);
        }

        //validasi
        $validator = Validator::make(Input::all(), $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('master.user')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('master')->withAktif('user');
        } else {
            //valid
            $user = User::find(Input::get('InternalID'));
            if ($user->CompanyInternalID == Auth::user()->Company->InternalID || Auth::user()->Company->InternalID == '-1') {
                $user->UserName = Input::get('Name');
                if (Auth::user()->Company->InternalID == '-1') {
                    $user->CompanyInternalID = Input::get('company');
                } else {
                    $user->CompanyInternalID = Auth::user()->Company->InternalID;
                }
                $user->UserModified = Auth::user()->UserID;
                $user->Phone = Input::get('Phone');
                $user->Email = Input::get('Email');
                $user->Remark = Input::get('remark');
                $user->save();
                return View::make('master.user')
                                ->withMessages('suksesUpdate')
                                ->withToogle('master')->withAktif('user');
            } else {
                return View::make('master.user')
                                ->withMessages('accessDenied')
                                ->withToogle('master')->withAktif('user');
            }
        }
    }

    static function deleteUser() {
        $user = User::find(Input::get('InternalID'));
        $sales = DB::table('t_sales_header')->where('UserRecord', $user->UserID)->orWhere('UserModified', $user->UserID)->where('CompanyInternalID', Auth::user()->CompanyInternalID)->first();
        $purchase = DB::table('t_purchase_header')->where('UserRecord', $user->UserID)->orWhere('UserModified', $user->UserID)->where('CompanyInternalID', Auth::user()->CompanyInternalID)->first();
        $salesOrder = DB::table('t_salesorder_header')->where('UserRecord', $user->UserID)->orWhere('UserModified', $user->UserID)->where('CompanyInternalID', Auth::user()->CompanyInternalID)->first();
        $purchaseOrder = DB::table('t_purchaseorder_header')->where('UserRecord', $user->UserID)->orWhere('UserModified', $user->UserID)->where('CompanyInternalID', Auth::user()->CompanyInternalID)->first();
        $salesReturn = DB::table('t_salesreturn_header')->where('UserRecord', $user->UserID)->orWhere('UserModified', $user->UserID)->where('CompanyInternalID', Auth::user()->CompanyInternalID)->first();
        $purchaseReturn = DB::table('t_purchasereturn_header')->where('UserRecord', $user->UserID)->orWhere('UserModified', $user->UserID)->where('CompanyInternalID', Auth::user()->CompanyInternalID)->first();
        //cek user ada di pakai atau tidak
        if (is_null($sales) && is_null($purchase) && is_null($salesOrder) && is_null($purchaseOrder) && is_null($salesReturn) && is_null($purchaseReturn)) {
            //tidak ada maka boleh dihapus
            $user = User::find(Input::get('InternalID'));
            if ($user->CompanyInternalID == Auth::user()->Company->InternalID || Auth::user()->Company->InternalID == '-1') {
                $user->delete();
                return View::make('master.user')
                                ->withMessages('suksesDelete')
                                ->withToogle('master')->withAktif('user');
            } else {
                return View::make('master.user')
                                ->withMessages('accessDenied')
                                ->withToogle('master')->withAktif('user');
            }
        } else {
            //ada maka tidak dihapus
            return View::make('master.user')
                            ->withMessages('gagalDelete')
                            ->withToogle('master')->withAktif('user');
        }
    }

    static function nonActiveUser() {
        $user = User::find(Input::get('InternalID'));
        if ($user->CompanyInternalID == Auth::user()->Company->InternalID || Auth::user()->Company->InternalID == '-1') {

            $user->Status = 0;
            $user->save();

            return View::make('master.user')
                            ->withMessages('suksesUpdate')
                            ->withToogle('master')->withAktif('user');
        } else { //bukan se company area dan bukan super admin
            return View::make('master.user')
                            ->withMessages('accessDenied')
                            ->withToogle('master')->withAktif('user');
        }
    }

    static function activeUser() {
        $user = User::find(Input::get('InternalID'));
        if ($user->CompanyInternalID == Auth::user()->Company->InternalID || Auth::user()->Company->InternalID == '-1') {

            $user->Status = 1;
            $user->save();

            return View::make('master.user')
                            ->withMessages('suksesUpdate')
                            ->withToogle('master')->withAktif('user');
        } else { //bukan se company area dan bukan super admin
            return View::make('master.user')
                            ->withMessages('accessDenied')
                            ->withToogle('master')->withAktif('user');
        }
    }

    public function updateProfile() {
        //rule
        $rule = array(
            'Name' => 'required|max:200',
            'Phone' => 'required|max:200',
            'ProfilePicture' => 'image'
        );
        //cek Email sama enggak
        $emailLama = User::find(Auth::user()->InternalID)->Email;
        $data = Input::all();
        if (strtoupper(Input::get('Email')) == strtoupper($emailLama)) {
            $rule2 = array(
                'Email' => 'required|max:200|email'
            );
            $rule = array_merge($rule, $rule2);
        } else {
            $rule2 = array(
                'Email' => 'required|max:200|email|unique:m_user,Email'
            );
            $rule = array_merge($rule, $rule2);
        }

        //validasi
        $validator = Validator::make(Input::all(), $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('setting.profile')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('setting')->withAktif('profile');
        } else {
            $path = NULL;
            $input = Input::file('ProfilePicture');
            if ($input != '') {
                if (Input::hasFile('ProfilePicture') && Input::file('ProfilePicture')->getSize() <= 2000000) {
                    $destination = base_path() . '/uploaded_img/';
                    $filename = str_random(6) . '_' . date('d') . date('m') . date('y') . '_' . Input::file('ProfilePicture')->getClientOriginalName();
                    Input::file('ProfilePicture')->move($destination, $filename);
                    $height = Image::make(sprintf($destination . '%s', $filename))->height();
                    $width = Image::make(sprintf($destination . '%s', $filename))->width();
                    $heightResize = null;
                    $widthResize = null;
                    $countResize = 0;
                    if ($height > 180) {
                        $heightResize = 180;
                        $countResize = 1;
                    }
                    if ($width > 160) {
                        $widthResize = 160;
                        $countResize = 1;
                    }
                    if ($countResize == 1) {
                        $image = Image::make(sprintf($destination . '%s', $filename))->resize($widthResize, $heightResize)->save();
                    }
                    $path = '/uploaded_img/' . $filename;
                } else {
                    return View::make('setting.profile')
                                    ->withMessages('gagalUpload')
                                    ->withToogle('setting')->withAktif('profile');
                }
            }
            //valid
            $user = User::find(Auth::user()->InternalID);
            if ($user->CompanyInternalID == Auth::user()->Company->InternalID || Auth::user()->Company->InternalID == '-1') {
                $user->UserName = Input::get('Name');
                if (Auth::user()->Company->InternalID == '-1') {
                    $user->CompanyInternalID = Input::get('company');
                } else {
                    $user->CompanyInternalID = Auth::user()->Company->InternalID;
                }
                $user->Phone = Input::get('Phone');
                $user->Email = Input::get('Email');
                $user->UserModified = Auth::user()->UserID;
                if ($path != NULL) {
                    if ($user->Picture != NULL) {
                        $destination = public_path();
                        File::delete(sprintf($destination . '%s', $user->Picture));
                    }
                    $user->Picture = $path;
                }
                $user->Remark = Input::get('remark');
                $user->save();
                $messages = 'suksesUpdate';
                Session::flash('messages', $messages);
                return Redirect::Route('settProfile');
            } else {
                return View::make('setting.profile')
                                ->withMessages('accessDenied')
                                ->withToogle('setting')->withAktif('profile');
            }
        }
    }

    public function updatePassword() {
        //rule
        $rule = array(
            'Password' => 'required|max:16'
        );
        //validasi
        $validator = Validator::make(Input::all(), $rule);
        if ($validator->fails()) {
            //tidak valid
            return View::make('setting.profile')
                            ->withMessages('gagalUpdate')
                            ->withError($validator->messages())
                            ->withToogle('setting')->withAktif('profile');
        } else {
            if (!Hash::check(Input::get('PasswordLama'), Auth::user()->UserPwd)) {
                return View::make('setting.profile')
                                ->withMessages('gagalPassword')
                                ->withError($validator->messages())
                                ->withToogle('setting')->withAktif('profile');
            }
            $path = NULL;
            //valid
            $user = User::find(Auth::user()->InternalID);
            if ($user->CompanyInternalID == Auth::user()->Company->InternalID || Auth::user()->Company->InternalID == '-1') {
                $user->UserPwd = Hash::make(Input::get('Password'));
                $user->save();
                $messages = 'suksesUpdatePassword';
                Session::flash('messages', $messages);
                return Redirect::Route('settProfile');
            } else {
                return View::make('setting.profile')
                                ->withMessages('accessDenied')
                                ->withToogle('setting')->withAktif('profile');
            }
        }
    }

    public function exportExcel() {
        Excel::create('Master_User', function($excel) {
            $excel->sheet('Master_User', function($sheet) {
                $sheet->mergeCells('B1:J1');
                $sheet->setCellValueByColumnAndRow(1, 1, "Master User");
                $sheet->setCellValueByColumnAndRow(1, 2, "No.");
                $sheet->setCellValueByColumnAndRow(2, 2, "User Name");
                $sheet->setCellValueByColumnAndRow(3, 2, "User ID");
                $sheet->setCellValueByColumnAndRow(4, 2, "Email");
                $sheet->setCellValueByColumnAndRow(5, 2, "Phone");
                $sheet->setCellValueByColumnAndRow(6, 2, "Company");
                $sheet->setCellValueByColumnAndRow(7, 2, "Record");
                $sheet->setCellValueByColumnAndRow(8, 2, "Modified");
                $sheet->setCellValueByColumnAndRow(9, 2, "Remark");
                $row = 3;
                $userData = User::where('CompanyInternalID', Auth::user()->Company->InternalID)->get();
                if (Auth::user()->Company->InternalID == '-1') {
                    $userData = User::all();
                }
                foreach ($userData as $data) {
                    $company = User::find($data->InternalID)->Company;
                    $sheet->setCellValueByColumnAndRow(1, $row, $row - 2);
                    $sheet->setCellValueByColumnAndRow(2, $row, $data->UserName);
                    $sheet->setCellValueByColumnAndRow(3, $row, "`" . $data->UserID);
                    $sheet->setCellValueByColumnAndRow(4, $row, $data->Email);
                    $sheet->setCellValueByColumnAndRow(5, $row, '`' . $data->Phone);
                    $sheet->setCellValueByColumnAndRow(6, $row, $company->CompanyName);
                    $sheet->setCellValueByColumnAndRow(7, $row, $data->UserRecord . ' ' . date("d-m-Y H:i:s", strtotime($data->dtRecord)));
                    $sheet->setCellValueByColumnAndRow(8, $row, $data->UserModified . ' ' . date("d-m-Y H:i:s", strtotime($data->dtModified)));
                    $sheet->setCellValueByColumnAndRow(9, $row, $data->Remark);
                    $row++;
                }

                if (User::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() <= 0) {
                    $sheet->mergeCells('B3:J3');
                    $sheet->setCellValueByColumnAndRow(1, 3, "No data available in table");

                    $sheet->cells('B3:J3', function($cells) {
                        $cells->setAlignment('center');
                    });

                    $sheet->setBorder('B3:J' . $row, 'thin');
                }

                $row--;
                $sheet->setBorder('B2:J' . $row, 'thin');
                $sheet->cells('B2:J2', function($cells) {
                    $cells->setBackground('#eaf6f7');
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                });
                $sheet->cells('B1', function($cells) {
                    $cells->setValignment('middle');
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize('16');
                });
                $sheet->cells('B3:J' . $row, function($cells) {
                    $cells->setAlignment('left');
                    $cells->setValignment('middle');
                });
                $sheet->cells('B3:B' . $row, function($cells) {
                    $cells->setAlignment('center');
                });
            });
        })->export('xls');
    }

}
