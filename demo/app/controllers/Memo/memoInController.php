<?php

class MemoInController extends BaseController {

    public function showMemoIn() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteMemoIn') {
                return $this->deleteMemoIn();
            }
        }
        return View::make('memo.memoIn')
                        ->withToogle('transaction')->withAktif('memoIn');
    }

    public function memoInNew() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            return $this->insertMemoIn();
        }
        $memoin = $this->createID(0) . '.';
        return View::make('memo.memoInNew')
                        ->withToogle('transaction')->withAktif('memoIn')
                        ->withMemoin($memoin);
    }

    public function memoInDetail($id) {
        $id = MemoInHeader::getIdmemoIn($id);
        $header = MemoInHeader::find($id);
        $detail = MemoInHeader::find($id)->memoInDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            return View::make('memo.memoInDetail')
                            ->withToogle('transaction')->withAktif('memoIn')
                            ->withHeader($header)
                            ->withDetail($detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showMemoIn');
        }
    }

    public function memoInUpdate($id) {
        $id = MemoInHeader::getIdmemoIn($id);
        $header = MemoInHeader::find($id);
        $detail = MemoInHeader::find($id)->memoInDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                return $this->updateMemoIn($id);
            }
            $memoIn = $this->createID(0) . '.';
            return View::make('memo.memoInUpdate')
                            ->withToogle('transaction')->withAktif('memoIn')
                            ->withHeader($header)
                            ->withDetail($detail)
                            ->withMemoin($memoIn);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showMemoIn');
        }
    }

    public function insertMemoIn() {
        //rule
        $rule = array(
            'date' => 'required',
            'remark' => 'required|max:1000',
            'currency' => 'required',
            'warehouse' => 'required',
            'rate' => 'required',
            'inventory' => 'required'
        );
        $memoInNumber = '';
        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalInsert';
            $error = $validator->messages();
        } else {
            //valid
            //insert header
            $header = new MemoInHeader;
            $memoIn = $this->createID(1) . '.';
            $date = explode('-', Input::get('date'));
            $yearDigit = substr($date[2], 2);
            $memoIn .= $date[1] . $yearDigit . '.';
            $memoInNumber = MemoInHeader::getNextIDMemoIn($memoIn);
            $header->MemoInID = $memoInNumber;
            $header->MemoInDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $currency = explode('---;---', Input::get('currency'));
            $header->WarehouseInternalID = Input::get('warehouse');
            $header->CurrencyInternalID = $currency[0];
            $header->CurrencyRate = str_replace(',', '', Input::get('rate'));
            $header->GrandTotal = Input::get('grandTotalValue');
            $header->UserRecord = Auth::user()->UserID;
            $header->CompanyInternalID = Auth::user()->Company->InternalID;
            $header->UserModified = '0';
            $header->Remark = Input::get('remark');
            $header->save();
            //insert detail
            $total = 0;
            for ($a = 0; $a < count(Input::get('inventory')); $a++) {
                $qtyValue = str_replace(',', '', Input::get('qty')[$a]);
                $priceValue = str_replace(',', '', Input::get('price')[$a]);
                $subTotal = ($priceValue * $qtyValue);
                if ($qtyValue > 0) {
                    $detail = new MemoInDetail();
                    $detail->MemoInInternalID = $header->InternalID;
                    $detail->InventoryInternalID = Input::get('inventory')[$a];
                    $detail->Qty = $qtyValue;
                    $detail->Price = $priceValue;
                    $detail->SubTotal = $subTotal;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = '0';
                    $detail->save();
                }
                $total += $subTotal;
            }
            
            $dataType = MemoInDetail::getTipeInventoryData($header->InternalID);
            $slip = NULL;
            $this->insertJournal($memoInNumber, $total, $currency[0], str_replace(',', '', Input::get('rate')), $date, $slip, $dataType);

            $messages = 'suksesInsert';
            $error = '';
        }
        $memoIn = $this->createID(0) . '.';
        return View::make('memo.memoInNew')
                        ->withToogle('transaction')->withAktif('memoIn')
                        ->withMemoin($memoIn)
                        ->withError($error)
                        ->withMessages($messages);
    }

    public function updateMemoIn($id) {
        //tipe
        $headerUpdate = MemoInHeader::find($id);
        $detailUpdate = MemoInHeader::find($id)->memoInDetail()->get();
        //rule
        $rule = array(
            'remark' => 'required|max:1000',
            'currency' => 'required',
            'warehouse' => 'required',
            'rate' => 'required',
            'inventory' => 'required'
        );
        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalUpdate';
            $error = $validator->messages();
        } else {
            $header = MemoInHeader::find(Input::get('MemoInInternalID'));
            $currency = explode('---;---', Input::get('currency'));
            $header->WarehouseInternalID = Input::get('warehouse');
            $header->CurrencyInternalID = $currency[0];
            $header->CurrencyRate = str_replace(',', '', Input::get('rate'));
            $header->GrandTotal = Input::get('grandTotalValue');
            $header->UserModified = Auth::user()->UserID;
            $header->Remark = Input::get('remark');
            $header->save();

            //delete memoIn detail -- nantinya insert ulang
            MemoInDetail::where('MemoInInternalID', '=', Input::get('MemoInInternalID'))->delete();

            //insert detail
            $total = 0;
            for ($a = 0; $a < count(Input::get('inventory')); $a++) {
                $qtyValue = str_replace(',', '', Input::get('qty')[$a]);
                $priceValue = str_replace(',', '', Input::get('price')[$a]);
                $subTotal = ($priceValue * $qtyValue);
                $total += $subTotal;
                if ($qtyValue > 0) {
                    $detail = new MemoInDetail();
                    $detail->MemoInInternalID = Input::get('MemoInInternalID');
                    $detail->InventoryInternalID = Input::get('inventory')[$a];
                    $detail->Qty = $qtyValue;
                    $detail->Price = $priceValue;
                    $detail->SubTotal = $subTotal;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = Auth::user()->UserID;
                    $detail->save();
                }
            }
            
            $journal = JournalHeader::where('TransactionID', '=', $header->MemoInID)->get();
            foreach ($journal as $value) {
                JournalDetail::where('JournalInternalID', '=', $value->InternalID)->delete();
                JournalHeader::where('InternalID', '=', $value->InternalID)->delete();
            }

            $date = date("d-m-Y", strtotime($header->MemoInDate));
            $date = explode('-', $date);
            $dataType = MemoInDetail::getTipeInventoryData($header->InternalID);
            $slip = NULL;
            $this->insertJournal($header->MemoInID, $total, $currency[0], str_replace(',', '', Input::get('rate')), $date, $slip, $dataType);

            $messages = 'suksesUpdate';
            $error = '';
        }

        //tipe
        $header = MemoInHeader::find($id);
        $detail = MemoInHeader::find($id)->memoInDetail()->get();
        $memoIn = $this->createID(0) . '.';
        return View::make('memo.memoInUpdate')
                        ->withToogle('transaction')->withAktif('memoIn')->withHeader($header)
                        ->withDetail($detail)
                        ->withMemoIn($memoIn)->withError($error)
                        ->withMessages($messages);
    }

    public function deleteMemoIn() {
        $memoIn = null;
        if (is_null($memoIn)) {
            //tidak ada yang menggunakan data memoIn maka data boleh dihapus
            //hapus journal
            $memoInHeader = MemoInHeader::find(Input::get('InternalID'));
            if ($memoInHeader->CompanyInternalID == Auth::user()->Company->InternalID) {
                $journal = JournalHeader::where('TransactionID', '=', $memoInHeader->MemoInID)->get();
                foreach ($journal as $value) {
                    JournalDetail::where('JournalInternalID', '=', $value->InternalID)->delete();
                    JournalHeader::where('InternalID', '=', $value->InternalID)->delete();
                }
                //hapus detil
                $detilData = MemoInHeader::find(Input::get('InternalID'))->memoInDetail;
                foreach ($detilData as $value) {
                    $detil = memoInDetail::find($value->InternalID);
                    $detil->delete();
                }
                //hapus memoIn
                $memoIn = MemoInHeader::find(Input::get('InternalID'));
                $memoIn->delete();
                $messages = 'suksesDelete';
            } else {
                $messages = 'accessDenied';
            }
        } else {
            //ada maka data tidak boleh dihapus
            $messages = 'gagalDelete';
        }
        return View::make('memo.memoIn')
                        ->withToogle('transaction')->withAktif('memoIn')
                        ->withMessages($messages);
    }

    function insertJournal($memoNumber, $total, $currency, $rate, $date, $slip, $dataType) {
        $header = new JournalHeader;
        $yearDigit = substr($date[2], 2);
        $dateText = $date[1] . $yearDigit;
//        $defaultPembelian = Default_s::find(4)->DefaultID;
//        $defaultHutang = Default_s::find(5)->DefaultID;
//        $defaultOutcome = Default_s::find(6)->DefaultID;
//        $defaultDiscount = Default_s::find(10)->DefaultID;
//        $defaultDP = Default_s::find(12)->DefaultID;
        $defaultHPP = Default_s::find(9)->DefaultID;

        $cari = 'ME-' . $dateText;
        $header->JournalType = 'Memorial';
        $header->JournalID = JournalHeader::getNextIDJournal($cari . '-');
        $header->SlipInternalID = Null;
        $header->JournalDate = $date[2] . '-' . $date[1] . '-' . $date[0];
        $header->JournalFrom = Auth::user()->UserID;
        $header->Notes = '';
        $department = Department::where('Default', '1')->first();
        $header->DepartmentInternalID = $department->InternalID;
        $header->TransactionID = $memoNumber;
        $header->ACC5InternalID = '0';
        $header->Lock = '0';
        $header->Check = '0';
        $header->Flag = '0';
        $header->UserRecord = Auth::user()->UserID;
        $header->CompanyInternalID = Auth::user()->Company->InternalID;
        $header->UserModified = '0';
        $header->Remark = '';
        $header->save();

        //insert detail
        $count = 1;
        $tampTotal = 0;
        foreach ($dataType as $data2) {
            $tipe = InventoryType::find($data2->InternalID);
            $detail = new JournalDetail();
            $detail->JournalInternalID = $header->InternalID;
            $detail->JournalIndex = $count;
            $detail->JournalNotes = $tipe->InventoryTypeName;
            $detail->JournalDebetMU = $data2->total;
            $detail->JournalCreditMU = 0;
            $detail->CurrencyInternalID = $currency;
            $detail->CurrencyRate = $rate;
            $detail->JournalDebet = $data2->total * $rate;
            $detail->JournalCredit = 0;
            $detail->JournalTransactionID = NULL;
            $detail->ACC1InternalID = $tipe->ACC1InternalID;
            $detail->ACC2InternalID = $tipe->ACC2InternalID;
            $detail->ACC3InternalID = $tipe->ACC3InternalID;
            $detail->ACC4InternalID = $tipe->ACC4InternalID;
            $detail->ACC5InternalID = $tipe->ACC5InternalID;
            $detail->ACC6InternalID = $tipe->ACC6InternalID;
            $coa = Coa::getInternalID($tipe->ACC1InternalID, $tipe->ACC2InternalID, $tipe->ACC3InternalID, $tipe->ACC4InternalID, $tipe->ACC5InternalID, $tipe->ACC6InternalID);
            $detail->COAName = Coa::find($coa)->COAName;
            $detail->UserRecord = Auth::user()->UserID;
            $detail->UserModified = '0';
            $detail->save();
            $count++;
        }

        $detail = new JournalDetail();
        $detail->JournalInternalID = $header->InternalID;
        $detail->JournalIndex = $count;
        $detail->JournalNotes = 'Cost of Goods Sold';
        $detail->JournalDebetMU = 0;
        $detail->JournalCreditMU = $total;
        $detail->CurrencyInternalID = $currency;
        $detail->CurrencyRate = $rate;
        $detail->JournalDebet = 0;
        $detail->JournalCredit = $total * $rate;
        $detail->JournalTransactionID = NULL;
        $default = Default_s::getInternalCoa('Cost of Goods Sold');
        $detail->ACC1InternalID = $default->ACC1InternalID;
        $detail->ACC2InternalID = $default->ACC2InternalID;
        $detail->ACC3InternalID = $default->ACC3InternalID;
        $detail->ACC4InternalID = $default->ACC4InternalID;
        $detail->ACC5InternalID = $default->ACC5InternalID;
        $detail->ACC6InternalID = $default->ACC6InternalID;
        $coa = Coa::getInternalID($default->ACC1InternalID, $default->ACC2InternalID, $default->ACC3InternalID, $default->ACC4InternalID, $default->ACC5InternalID, $default->ACC6InternalID);
        $detail->COAName = Coa::find($coa)->COAName;
        $detail->UserRecord = Auth::user()->UserID;
        $detail->UserModified = '0';
        $detail->save();
    }
    
    function memoInPrint($id) {
        $id = MemoInHeader::getIdmemoIn($id);
        $header = MemoInHeader::find($id);
        $detail = MemoInHeader::find($id)->memoInDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $currency = Currency::find($header->CurrencyInternalID);
            $currencyName = $currency->CurrencyName;
            $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                    .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">MemoIn</h5>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 18px; width: 100%;">
                            <table>
                            <tr>
                            <td width="423px">
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">MemoIn ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $header->MemoInID . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->MemoInDate)) . '</td>
                                 </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Currency</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $currencyName . '</td>
                                     </tr>
                                </table>
                                </td>
                                <td>
                                <table>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Rate</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . number_format($header->CurrencyRate, '2', '.', ',') . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Warehouse</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $header->Warehouse->WarehouseName . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Remark</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $header->Remark . '</td>
                                     </tr>
                                </table></td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Qty</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Price</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            $total = 0;
            if (count($detail) > 0) {
                foreach ($detail as $data) {
                    $inventory = Inventory::find($data->InventoryInternalID);
                    $inv = $inventory->InventoryID . ' ' . $inventory->InventoryName;
                    $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $inv . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
                    $total += $data->SubTotal;
                }
            } else {
                $html.= '<tr>
                            <td colspan="4" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;  text-align: center">There is no inventory registered in this memoIn.</td>
                        </tr>';
            }
            $html.= '</tbody>
                            </table>
                            <br>
                            <div style="box-sizing: border-box;min-width: 198px; margin-left: 448px; display: inline-block; clear: both;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Grand Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '</td>
                                 </tr>
                                </table>
                            </div>
                    </div>
                </body>
            </html>';
            return PDF ::load($html, 'A4', 'portrait')->show();
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showMemoIn');
        }
    }

    function createID($tipe) {
        $memoIn = 'ME-IN';
        if ($tipe == 0) {
            $memoIn .= '.' . date('m') . date('y');
        }
        return $memoIn;
    }

    public function formatCariIDMemoIn() {
        $date = explode('-', Input::get('date'));
        $id = $this->createID(1);
        $yearDigit = substr($date[2], 2);
        $id .= '.' . $date[1] . $yearDigit . '.';
        echo MemoInHeader::getNextIDMemoIn($id);
    }

    //====================ajax==========================
    public function getHPPValueInventoryMemoIn() {
        $date = Input::get("date");
        $arrDate = explode("-", $date);
        $inventoryInternalID = Input::get("inventory");
        $month = $arrDate[1] - 1;
        $year = $arrDate[2];
        if ($month == 0) {
            $month = 12;
            $year = $arrDate[2] - 1;
        }
        $hpp = 0;
        $value = InventoryValue::where("InventoryInternalID", $inventoryInternalID)
                        ->where("Month", $month)
                        ->where("Year", $year)->get();
        if (count($value) == 0) {
            $hpp = Inventory::find($inventoryInternalID)->InitialValue;
        } else {
            $hpp = $value[0]->Value;
        }
        return $hpp;
    }

    //====================ajax==========================
}
