<?php

class MemoOutController extends BaseController {

    public function showMemoOut() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteMemoOut') {
                return $this->deleteMemoOut();
            }
        }
        return View::make('memo.memoOut')
                        ->withToogle('transaction')->withAktif('memoOut');
    }

    public function memoOutNew() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            return $this->insertMemoOut();
        }
        $memoout = $this->createID(0) . '.';
        return View::make('memo.memoOutNew')
                        ->withToogle('transaction')->withAktif('memoOut')
                        ->withMemoout($memoout);
    }

    public function memoOutDetail($id) {
        $id = MemoOutHeader::getIdmemoOut($id);
        $header = MemoOutHeader::find($id);
        $detail = MemoOutHeader::find($id)->memoOutDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            return View::make('memo.memoOutDetail')
                            ->withToogle('transaction')->withAktif('memoOut')
                            ->withHeader($header)
                            ->withDetail($detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showMemoOut');
        }
    }

    public function memoOutUpdate($id) {
        $id = MemoOutHeader::getIdmemoOut($id);
        $header = MemoOutHeader::find($id);
        $detail = MemoOutHeader::find($id)->memoOutDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                return $this->updateMemoOut($id);
            }
            $memoOut = $this->createID(0) . '.';
            return View::make('memo.memoOutUpdate')
                            ->withToogle('transaction')->withAktif('memoOut')
                            ->withHeader($header)
                            ->withDetail($detail)
                            ->withMemoout($memoOut);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showMemoOut');
        }
    }

    public function insertMemoOut() {
        //rule
        $rule = array(
            'date' => 'required',
            'remark' => 'required|max:1000',
            'currency' => 'required',
            'rate' => 'required',
            'inventory' => 'required',
            'warehouse' => 'required'
        );
        $memoOutNumber = '';
        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalInsert';
            $error = $validator->messages();
        } else {
            //valid
            //insert header
            $header = new MemoOutHeader;
            $memoOut = $this->createID(1) . '.';
            $date = explode('-', Input::get('date'));
            $yearDigit = substr($date[2], 2);
            $memoOut .= $date[1] . $yearDigit . '.';
            $memoOutNumber = MemoOutHeader::getNextIDMemoOut($memoOut);
            $header->MemoOutID = $memoOutNumber;
            $header->MemoOutDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $currency = explode('---;---', Input::get('currency'));
            $header->CurrencyInternalID = $currency[0];
            $header->CurrencyRate = str_replace(',', '', Input::get('rate'));
            $header->GrandTotal = Input::get('grandTotalValue');
            $header->UserRecord = Auth::user()->UserID;
            $header->CompanyInternalID = Auth::user()->Company->InternalID;
            $header->UserModified = '0';
            $header->Remark = Input::get('remark');
            $header->WarehouseInternalID = Input::get('warehouse');
            $header->save();
            //insert detail
            $total = 0;
            for ($a = 0; $a < count(Input::get('inventory')); $a++) {
                $qtyValue = str_replace(',', '', Input::get('qty')[$a]);
                $priceValue = str_replace(',', '', Input::get('price')[$a]);
                $subTotal = ($priceValue * $qtyValue);
                if ($qtyValue > 0) {
                    $detail = new MemoOutDetail();
                    $detail->MemoOutInternalID = $header->InternalID;
                    $detail->InventoryInternalID = Input::get('inventory')[$a];
                    $detail->Qty = $qtyValue;
                    $detail->Price = $priceValue;
                    $detail->SubTotal = $subTotal;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = '0';
                    $detail->save();
                }
                $total += $subTotal;
            }
            $messages = 'suksesInsert';
            $error = '';
        }
        $memoOut = $this->createID(0) . '.';
        return View::make('memo.memoOutNew')
                        ->withToogle('transaction')->withAktif('memoOut')
                        ->withMemoout($memoOut)
                        ->withError($error)
                        ->withMessages($messages);
    }

    public function updateMemoOut($id) {
        //tipe
        $headerUpdate = MemoOutHeader::find($id);
        $detailUpdate = MemoOutHeader::find($id)->memoOutDetail()->get();
        //rule
        $rule = array(
            'remark' => 'required|max:1000',
            'currency' => 'required',
            'rate' => 'required',
            'inventory' => 'required',
            'warehouse' => 'required'
        );
        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalUpdate';
            $error = $validator->messages();
        } else {
            $header = MemoOutHeader::find(Input::get('MemoOutInternalID'));
            $currency = explode('---;---', Input::get('currency'));
            $header->CurrencyInternalID = $currency[0];
            $header->CurrencyRate = str_replace(',', '', Input::get('rate'));
            $header->GrandTotal = Input::get('grandTotalValue');
            $header->UserModified = Auth::user()->UserID;
            $header->Remark = Input::get('remark');
            $header->WarehouseInternalID = Input::get('warehouse');
            $header->save();

            //delete memoOut detail -- nantinya insert ulang
            MemoOutDetail::where('MemoOutInternalID', '=', Input::get('MemoOutInternalID'))->delete();

            //insert detail
            $total = 0;
            for ($a = 0; $a < count(Input::get('inventory')); $a++) {
                $qtyValue = str_replace(',', '', Input::get('qty')[$a]);
                $priceValue = str_replace(',', '', Input::get('price')[$a]);
                $subTotal = ($priceValue * $qtyValue);
                $total += $subTotal;
                if ($qtyValue > 0) {
                    $detail = new MemoOutDetail();
                    $detail->MemoOutInternalID = Input::get('MemoOutInternalID');
                    $detail->InventoryInternalID = Input::get('inventory')[$a];
                    $detail->Qty = $qtyValue;
                    $detail->Price = $priceValue;
                    $detail->SubTotal = $subTotal;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = Auth::user()->UserID;
                    $detail->save();
                }
            }
            $messages = 'suksesUpdate';
            $error = '';
        }

        //tipe
        $header = MemoOutHeader::find($id);
        $detail = MemoOutHeader::find($id)->memoOutDetail()->get();
        $memoOut = $this->createID(0) . '.';
        return View::make('memo.memoOutUpdate')
                        ->withToogle('transaction')->withAktif('memoOut')->withHeader($header)
                        ->withDetail($detail)
                        ->withMemoOut($memoOut)->withError($error)
                        ->withMessages($messages);
    }

    public function deleteMemoOut() {
        $memoOut = null;
        if (is_null($memoOut)) {
            //tidak ada yang menggunakan data memoOut maka data boleh dihapus
            //hapus journal
            $memoOutHeader = MemoOutHeader::find(Input::get('InternalID'));
            if ($memoOutHeader->CompanyInternalID == Auth::user()->Company->InternalID) {
                //hapus detil
                $detilData = MemoOutHeader::find(Input::get('InternalID'))->memoOutDetail;
                foreach ($detilData as $value) {
                    $detil = memoOutDetail::find($value->InternalID);
                    $detil->delete();
                }
                //hapus memoOut
                $memoOut = MemoOutHeader::find(Input::get('InternalID'));
                $memoOut->delete();
                $messages = 'suksesDelete';
            } else {
                $messages = 'accessDenied';
            }
        } else {
            //ada maka data tidak boleh dihapus
            $messages = 'gagalDelete';
        }
        return View::make('memo.memoOut')
                        ->withToogle('transaction')->withAktif('memoOut')
                        ->withMessages($messages);
    }

    function memoOutPrint($id) {
        $id = MemoOutHeader::getIdmemoOut($id);
        $header = MemoOutHeader::find($id);
        $detail = MemoOutHeader::find($id)->memoOutDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $currency = Currency::find($header->CurrencyInternalID);
            $currencyName = $currency->CurrencyName;
            $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                    .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">MemoOut</h5>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 18px; width: 100%;">
                            <table>
                            <tr>
                            <td width="423px">
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">MemoOut ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $header->MemoOutID . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->MemoOutDate)) . '</td>
                                 </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Currency</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $currencyName . '</td>
                                     </tr>
                                </table>
                                </td>
                                <td>
                                <table>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Rate</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . number_format($header->CurrencyRate, '2', '.', ',') . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Warehouse</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $header->Warehouse->WarehouseName . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Remark</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $header->Remark . '</td>
                                     </tr>
                                </table></td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Qty</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Price</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            $total = 0;
            if (count($detail) > 0) {
                foreach ($detail as $data) {
                    $inventory = Inventory::find($data->InventoryInternalID);
                    $inv = $inventory->InventoryID . ' ' . $inventory->InventoryName;
                    $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $inv . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, '0', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
                    $total += $data->SubTotal;
                }
            } else {
                $html.= '<tr>
                            <td colspan="4" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;  text-align: center">There is no inventory registered in this memoOut.</td>
                        </tr>';
            }
            $html.= '</tbody>
                            </table>
                            <br>
                            <div style="box-sizing: border-box;min-width: 198px; margin-left: 448px; display: inline-block; clear: both;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Grand Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '</td>
                                 </tr>
                                </table>
                            </div>
                    </div>
                </body>
            </html>';
            return PDF ::load($html, 'A4', 'portrait')->show();
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showMemoOut');
        }
    }

    function createID($tipe) {
        $memoOut = 'ME-OUT';
        if ($tipe == 0) {
            $memoOut .= '.' . date('m') . date('y');
        }
        return $memoOut;
    }

    public function formatCariIDMemoOut() {
        $date = explode('-', Input::get('date'));
        $id = $this->createID(1);
        $yearDigit = substr($date[2], 2);
        $id .= '.' . $date[1] . $yearDigit . '.';
        echo MemoOutHeader::getNextIDMemoOut($id);
    }

    //==============ajax=======================
    public function getHPPValueInventoryMemoOut() {
        $date = Input::get("date");
        $arrDate = explode("-", $date);
        $inventoryInternalID = Input::get("inventory");
        $month = $arrDate[1] - 1;
        $year = $arrDate[2];
        if ($month == 0) {
            $month = 12;
            $year = $arrDate[2] - 1;
        }
        $hpp = 0;
        $value = InventoryValue::where("InventoryInternalID", $inventoryInternalID)
                        ->where("Month", $month)
                        ->where("Year", $year)->get();
        if (count($value) == 0) {
            $hpp = Inventory::find($inventoryInternalID)->InitialValue;
        } else {
            $hpp = $value[0]->Value;
        }
        return $hpp;
    }

    //==============ajax=======================
}
