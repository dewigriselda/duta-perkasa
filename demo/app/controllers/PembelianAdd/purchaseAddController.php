<?php

class PurchaseAddController extends BaseController {

    public function showPurchase() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deletePurchase') {
                return $this->deletePurchase();
            } else if (Input::get('jenis') == 'summaryPurchase') {
                return $this->summaryPurchase();
            } else if (Input::get('jenis') == 'detailPurchase') {
                return $this->detailPurchase();
            } else if (Input::get('jenis') == 'insertPurchase') {
                return Redirect::Route('purchaseNew', Input::get('purchase'));
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = PurchaseAddHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
            return View::make('pembelianAdd.purchaseAddSearch')
                            ->withToogle('transaction')->withAktif('purchase')
                            ->withData($data);
        }
        return View::make('pembelianAdd.purchaseAdd')
                        ->withToogle('transaction')->withAktif('purchase');
    }

    public function purchaseNew($id) {
        $id = PurchaseOrderHeader::getIdpurchaseOrder($id);
        $header = PurchaseOrderHeader::find($id);
        $detail = PurchaseOrderHeader::find($id)->purchaseOrderDetail()->get();
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deletePurchase') {
                return $this->deletePurchase();
            } else if (Input::get('jenis') == 'summaryPurchase') {
                return $this->summaryPurchase();
            } else if (Input::get('jenis') == 'detailPurchase') {
                return $this->detailPurchase();
            } else if (Input::get('jenis') == 'insertPurchase') {
                return Redirect::Route('purchaseNew', Input::get('purchase'));
            } else {
                return $this->insertPurchase($id);
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = PurchaseAddHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
            return View::make('pembelianAdd.purchaseAddSearch')
                            ->withToogle('transaction')->withAktif('purchase')
                            ->withData($data);
        }
        $purchase = $this->createID(0) . '.';
        return View::make('pembelianAdd.purchaseAddNew')
                        ->withToogle('transaction')->withAktif('purchase')
                        ->withHeader($header)
                        ->withDetail($detail)
                        ->withPurchase($purchase);
    }

    public function purchaseDetail($id) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'summaryPurchase') {
                return $this->summaryPurchase();
            } else if (Input::get('jenis') == 'detailPurchase') {
                return $this->detailPurchase();
            } else if (Input::get('jenis') == 'insertPurchase') {
                return Redirect::Route('purchaseNew', Input::get('purchase'));
            }
        }
        $id = PurchaseAddHeader::getIdpurchase($id);
        $header = PurchaseAddHeader::find($id);
        $detail = PurchaseAddHeader::find($id)->purchaseDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            return View::make('pembelianAdd.purchaseAddDetail')
                            ->withToogle('transaction')->withAktif('purchase')
                            ->withHeader($header)
                            ->withDetail($detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showPurchase');
        }
    }

    public function purchaseUpdate($id) {
        $id = PurchaseAddHeader::getIdpurchase($id);
        $header = PurchaseAddHeader::find($id);
        $detail = PurchaseAddHeader::find($id)->purchaseDetail()->get();

        $headerOrder = PurchaseOrderHeader::find($header->PurchaseOrderHeader->InternalID);
        $detailOrder = PurchaseOrderHeader::find($header->PurchaseOrderHeader->InternalID)->purchaseOrderDetail()->get();

        if ($header->CompanyInternalID == Auth::user()->Company->InternalID && PurchaseAddHeader::isReturn($header->PurchaseID) == false) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                if (Input::get('jenis') == 'insertPurchase') {
                    return Redirect::Route('purchaseNew', Input::get('purchase'));
                } else if (Input::get('jenis') == 'summaryPurchase') {
                    return $this->summaryPurchase();
                } else if (Input::get('jenis') == 'detailPurchase') {
                    return $this->detailPurchase();
                } else {
                    return $this->updatePurchase($id);
                }
            }
            $purchase = $this->createID(0) . '.';
            return View::make('pembelianAdd.purchaseAddUpdate')
                            ->withToogle('transaction')->withAktif('purchase')
                            ->withHeader($header)
                            ->withDetail($detail)
                            ->withHeaderorder($headerOrder)
                            ->withDetailorder($detailOrder)
                            ->withPurchase($purchase);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showPurchase');
        }
    }

    public function insertPurchase($id) {
        //rule
        $PurchaseOrderInternalID = Input::get('PurchaseOrderInternalID');
        $PurchaseOrderHeader = PurchaseOrderHeader::find($PurchaseOrderInternalID);
        $Detail = PurchaseOrderHeader::find($PurchaseOrderInternalID)->purchaseOrderDetail()->get();

        //$tamp = tabel dengan relasi:: fungsi select tabel dengan id
        $jenis = $PurchaseOrderHeader->isCash;
        //jenis = akses fieldnya
        if ($jenis == '0') {//cash
            $rule = array(
                'date' => 'required',
                'remark' => 'required|max:1000',
                'warehouse' => 'required',
                'slip' => 'required',
                'inventory' => 'required'
            );
        } else {//kredit
            $rule = array(
                'date' => 'required',
                'remark' => 'required|max:1000',
                'warehouse' => 'required',
                'inventory' => 'required'
            );
        }

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalInsert';
            $error = $validator->messages();
        } else {
            //valid
            //insert header
            $header = new PurchaseAddHeader;
            $purchase = $this->createID(1) . '.';
            $date = explode('-', Input::get('date'));
            $yearDigit = substr($date[2], 2);
            $purchase .= $date[1] . $yearDigit . '.';
            $purchaseNumber = PurchaseAddHeader::getNextIDPurchase($purchase);
            $header->PurchaseID = $purchaseNumber;
            $header->PurchaseDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $header->ACC6InternalID = $PurchaseOrderHeader->coa6->InternalID;
            $header->LongTerm = $PurchaseOrderHeader->LongTerm;
            $header->isCash = $PurchaseOrderHeader->isCash;
            $header->WarehouseInternalID = Input::get('warehouse');
            $header->CurrencyInternalID = $PurchaseOrderHeader->CurrencyInternalID;
            $header->CurrencyRate = $PurchaseOrderHeader->CurrencyRate;
            $header->VAT = $PurchaseOrderHeader->VAT;
            $header->PurchaseOrderInternalID = $PurchaseOrderInternalID;
            $header->DiscountGlobal = str_replace(",", "", Input::get("DiscountGlobal"));
            $header->GrandTotal = Input::get("grandTotalValue");
            $header->DownPayment = str_replace(",", "", Input::get('DownPayment'));
            $header->TransactionType = Input::get('TransactionType');
            if (Input::get('Replacement') == '') {
                $header->Replacement = '0';
            } else {
                $header->Replacement = Input::get('Replacement');
            }
            if (Input::get('TaxNumber') == '') {
                $header->TaxNumber = '.-.';
            } else {
                $header->TaxNumber = Input::get('TaxNumber');
            }
            $header->TaxMonth = Input::get('TaxMonth');
            $header->TaxYear = Input::get('TaxYear');
            $header->TaxDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $header->UserRecord = Auth::user()->UserID;
            $header->CompanyInternalID = Auth::user()->Company->InternalID;
            $header->UserModified = '0';
            $header->Remark = Input::get('remark');
            $header->save();

            //insert detail
            $total = 0;
            for ($a = 0; $a < count(Input::get('inventory')); $a++) {
                $qtyValue = str_replace(',', '', Input::get('qty')[$a]);
                $qtyPurchaseValue = str_replace(',', '', Input::get('Purchase')[$a]);
                $sumPurchase = PurchaseAddHeader::getSum(Input::get('inventory')[$a], $PurchaseOrderInternalID, Input::get('InternalDetail')[$a]);
                if ($sumPurchase == '') {
                    $sumPurchase = '0.00';
                }
                $sumPurchaseReturn = PurchaseReturnHeader::getSumReturnOrder(Input::get('inventory')[$a], $PurchaseOrderInternalID, Input::get('InternalDetail')[$a]);
                if ($sumPurchaseReturn == '') {
                    $sumPurchaseReturn = '0';
                }
                $qtyValue = $qtyValue - $sumPurchase + $sumPurchaseReturn;
                if ($qtyValue < $qtyPurchaseValue) {
                    $qtyPurchaseValue = $qtyValue;
                }
                $priceValue = Input::get('price')[$a];
                $discValue = str_replace(',', '', Input::get('discountNominal')[$a]);
                $subTotal = ($priceValue * $qtyPurchaseValue) - (($priceValue * $qtyPurchaseValue) * Input::get('discount')[$a] / 100) - $discValue * $qtyPurchaseValue;
                if ($header->VAT == '1') {
                    $vatValue = $subTotal / 10;
                } else {
                    $vatValue = 0;
                }
                if ($qtyPurchaseValue > 0) {
                    $detail = new PurchaseAddDetail();
                    $detail->PurchaseInternalID = $header->InternalID;
                    $detail->InventoryInternalID = Input::get('inventory')[$a];
                    $detail->Qty = $qtyPurchaseValue;
                    $detail->Price = $priceValue;
                    $detail->Discount = Input::get('discount')[$a];
                    $detail->DiscountNominal = $discValue;
                    $detail->VAT = $vatValue;
                    $detail->SubTotal = $subTotal;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = '0';
                    $detail->PurchaseOrderDetailInternalID = Input::get('InternalDetail')[$a];
                    $detail->save();
                }
                $total += $subTotal;
            }
            if ($header->VAT == '1') {
                $vatValueHeader = ($total - str_replace(",", "", Input::get("DiscountGlobal")) - str_replace(",", "", Input::get('DownPayment'))) / 10;
            } else {
                $vatValueHeader = 0;
            }
            $header->GrandTotal = $total - str_replace(",", "", Input::get("DiscountGlobal")) - str_replace(",", "", Input::get('DownPayment')) + $vatValueHeader;
            $header->save();

            $currency = $header->CurrencyInternalID;
            $rate = $header->CurrencyRate;
            $dataType = PurchaseDetail::getTipeInventoryData($header->InternalID);
            $total -= str_replace(",", "", Input::get('DiscountGlobal'));
            $total -= str_replace(",", "", Input::get('DownPayment'));
            if ($jenis == 0) {
                $slip = Input::get('slip');
                $this->insertJournal($purchaseNumber, $total, $header->VAT, $currency, $rate, $date, $slip, $dataType, str_replace(",", "", Input::get('DiscountGlobal')), str_replace(",", "", Input::get('DownPayment')));
            } else {
                $slip = '-1';
                $this->insertJournal($purchaseNumber, $total, $header->VAT, $currency, $rate, $date, $slip, $dataType, str_replace(",", "", Input::get('DiscountGlobal')), str_replace(",", "", Input::get('DownPayment')));
            }

            $messages = 'suksesInsert';
            $error = '';
        }
        $purchase = $this->createID(0) . '.';
        return View::make('pembelianAdd.purchaseAddNew')
                        ->withToogle('transaction')->withAktif('purchase')
                        ->withPurchase($purchase)
                        ->withError($error)
                        ->withHeader($PurchaseOrderHeader)
                        ->withDetail($Detail)
                        ->withMessages($messages);
    }

    public function updatePurchase($id) {
        //tipe
        $headerUpdate = PurchaseAddHeader::find($id);
        $detailUpdate = PurchaseAddHeader::find($id)->purchaseDetail()->get();

        $headerOrder = PurchaseOrderHeader::find($headerUpdate->PurchaseOrderHeader->InternalID);
        $detailOrder = PurchaseOrderHeader::find($headerUpdate->PurchaseOrderHeader->InternalID)->purchaseOrderDetail()->get();
        //rule
        if ($headerUpdate->isCash == '0') {
            $rule = array(
                'slip' => 'required',
                'warehouse' => 'required',
                'remark' => 'required|max:1000',
                'inventory' => 'required'
            );
        } else {
            $rule = array(
                'warehouse' => 'required',
                'remark' => 'required|max:1000',
                'inventory' => 'required'
            );
            $longTerm = $headerUpdate->LongTerm;
        }

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalUpdate';
            $error = $validator->messages();
        } else {
            $header = PurchaseAddHeader::find(Input::get('PurchaseInternalID'));
            $header->ACC6InternalID = $headerUpdate->coa6->InternalID;
            $header->isCash = $headerUpdate->isCash;
            $header->LongTerm = $headerUpdate->LongTerm;
            $header->WarehouseInternalID = Input::get('warehouse');
            $header->CurrencyInternalID = $headerUpdate->CurrencyInternalID;
            $header->CurrencyRate = $headerUpdate->CurrencyRate;
            if ($headerUpdate->VAT == '') {
                $header->VAT = '0';
            } else {
                $header->VAT = $headerUpdate->VAT;
            }
            $header->DiscountGlobal = str_replace(",", "", Input::get("DiscountGlobal"));
            $header->GrandTotal = Input::get("grandTotalValue");
            $header->DownPayment = str_replace(",", "", Input::get('DownPayment'));
            $header->TransactionType = Input::get('TransactionType');
            if (Input::get('Replacement') == '') {
                $header->Replacement = '0';
            } else {
                $header->Replacement = Input::get('Replacement');
            }
            if (Input::get('TaxNumber') == '') {
                $header->TaxNumber = '.-.';
            } else {
                $header->TaxNumber = Input::get('TaxNumber');
            }
            $header->TaxMonth = Input::get('TaxMonth');
            $header->TaxYear = Input::get('TaxYear');
            $header->UserModified = Auth::user()->UserID;
            $header->Remark = Input::get('remark');
            $header->save();

            //insert detail
            //delete purchase detail -- nantinya insert ulang
            PurchaseDetail::where('PurchaseInternalID', '=', Input::get('PurchaseInternalID'))->delete();

            $total = 0;
            for ($a = 0; $a < count(Input::get('inventory')); $a++) {
                $qtyValue = str_replace(',', '', Input::get('qty')[$a]);
                $qtyValuePurchase = str_replace(',', '', Input::get('Purchase')[$a]);
                $sumPurchase = PurchaseAddHeader::getSumExcept(Input::get('inventory')[$a], $headerOrder->InternalID, $header->InternalID, Input::get('InternalDetail')[$a]);
                if ($sumPurchase == '') {
                    $sumPurchase = '0';
                }
                $sumPurchaseReturn = PurchaseReturnHeader::getSumReturnOrder(Input::get('inventory')[$a], $headerOrder->InternalID, Input::get('InternalDetail')[$a]);
                if ($sumPurchaseReturn == '') {
                    $sumPurchaseReturn = '0';
                }
                $qtyValue = $qtyValue - $sumPurchase + $sumPurchaseReturn;
                if ($qtyValue < $qtyValuePurchase) {
                    $qtyValuePurchase = $qtyValue;
                }
                $priceValue = Input::get('price')[$a];
                $discValue = str_replace(',', '', Input::get('discountNominal')[$a]);
                $subTotal = ($priceValue * $qtyValuePurchase) - (($priceValue * $qtyValuePurchase) * Input::get('discount')[$a] / 100) - $discValue * $qtyValuePurchase;
                if ($headerUpdate->VAT == '1') {
                    $vatValue = $subTotal / 10;
                } else {
                    $vatValue = 0;
                }
                if ($qtyValuePurchase > 0) {
                    $detail = new PurchaseDetail();
                    $detail->PurchaseInternalID = Input::get('PurchaseInternalID');
                    $detail->InventoryInternalID = Input::get('inventory')[$a];
                    $detail->Qty = $qtyValuePurchase;
                    $detail->Price = $priceValue;
                    $detail->Discount = Input::get('discount')[$a];
                    $detail->DiscountNominal = $discValue;
                    $detail->VAT = $vatValue;
                    $detail->SubTotal = $subTotal;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = Auth::user()->UserID;
                    $detail->PurchaseOrderDetailInternalID = Input::get('InternalDetail')[$a];
                    $detail->save();
                }
                $total += $subTotal;
            }
            if ($header->VAT == '1') {
                $vatValueHeader = ($total - str_replace(",", "", Input::get("DiscountGlobal")) - str_replace(",", "", Input::get('DownPayment'))) / 10;
            } else {
                $vatValueHeader = 0;
            }
            $header->GrandTotal = $total - str_replace(",", "", Input::get("DiscountGlobal")) - str_replace(",", "", Input::get('DownPayment')) + $vatValueHeader;
            $header->save();

            $journal = JournalHeader::where('TransactionID', '=', $headerUpdate->PurchaseID)->get();
            foreach ($journal as $value) {
                JournalDetail::where('JournalInternalID', '=', $value->InternalID)->delete();
                JournalHeader::where('InternalID', '=', $value->InternalID)->delete();
            }

            $currency = $headerUpdate->CurrencyInternalID;
            $rate = str_replace(',', '', $headerUpdate->CurrencyRate);
            $date = date("d-m-Y", strtotime($headerUpdate->PurchaseDate));
            $date = explode('-', $date);
            $dataType = PurchaseDetail::getTipeInventoryData($header->InternalID);
            $total -= str_replace(",", "", Input::get('DiscountGlobal'));
            $total -= str_replace(",", "", Input::get('DownPayment'));
            if ($headerUpdate->isCash == 0) {
                $slip = Input::get('slip');
                $this->insertJournal($headerUpdate->PurchaseID, $total, $headerUpdate->VAT, $currency, $rate, $date, $slip, $dataType, str_replace(",", "", Input::get('DiscountGlobal')), str_replace(",", "", Input::get('DownPayment')));
            } else {
                $slip = '-1';
                $this->insertJournal($headerUpdate->PurchaseID, $total, $headerUpdate->VAT, $currency, $rate, $date, $slip, $dataType, str_replace(",", "", Input::get('DiscountGlobal')), str_replace(",", "", Input::get('DownPayment')));
            }

            $messages = 'suksesUpdate';
            $error = '';
        }

        //tipe
        $header = PurchaseAddHeader::find($id);
        $detail = PurchaseAddHeader::find($id)->purchaseDetail()->get();
        $purchase = $this->createID(0) . '.';
        return View::make('pembelianAdd.purchaseAddUpdate')
                        ->withToogle('transaction')->withAktif('purchase')
                        ->withHeader($header)
                        ->withDetail($detail)
                        ->withHeaderorder($headerOrder)
                        ->withDetailorder($detailOrder)
                        ->withPurchase($purchase)
                        ->withError($error)
                        ->withMessages($messages);
    }

    public function deletePurchase() {
        $purchaseHeader = PurchaseAddHeader::find(Input::get('InternalID'));
        $purchase = DB::select(DB::raw('SELECT * FROM t_purchasereturn_header WHERE CompanyInternalID = "' . Auth::user()->CompanyInternalID . '" AND PurchaseReturnID LIKE "%' . $purchaseHeader->PurchaseID . '"'));
        if (is_null($purchase) == '') {
            //tidak ada yang menggunakan data purchase maka data boleh dihapus
            ////hapus journal
            $purchaseHeader = PurchaseAddHeader::find(Input::get('InternalID'));
            if ($purchaseHeader->CompanyInternalID == Auth::user()->Company->InternalID) {
                $journal = JournalHeader::where('TransactionID', '=', $purchaseHeader->PurchaseID)->get();
                foreach ($journal as $value) {
                    JournalDetail::where('JournalInternalID', '=', $value->InternalID)->delete();
                    JournalHeader::where('InternalID', '=', $value->InternalID)->delete();
                }
                //hapus detil
                $detilData = PurchaseAddHeader::find(Input::get('InternalID'))->purchaseDetail;
                foreach ($detilData as $value) {
                    $detil = purchaseDetail::find($value->InternalID);
                    $detil->delete();
                }
                //hapus purchase
                $purchase = PurchaseAddHeader::find(Input::get('InternalID'));
                $purchase->delete();
                $messages = 'suksesDelete';
            } else {
                $messages = 'accessDenied';
            }
        } else {
            //ada maka data tidak boleh dihapus
            $messages = 'gagalDelete';
        }
        $data = PurchaseAddHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
        return View::make('pembelianAdd.purchaseAddSearch')
                        ->withToogle('transaction')->withAktif('purchase')
                        ->withMessages($messages)
                        ->withData($data);
    }

    function insertJournal($purchaseNumber, $total, $vat, $currency, $rate, $date, $slip, $dataType, $discountGlobal, $dp) {
        $header = new JournalHeader;
        $yearDigit = substr($date[2], 2);
        $dateText = $date[1] . $yearDigit;
//        $defaultPembelian = Default_s::find(4)->DefaultID;
        $defaultHutang = Default_s::find(5)->DefaultID;
        $defaultOutcome = Default_s::find(6)->DefaultID;
        $defaultDiscount = Default_s::find(10)->DefaultID;
        $defaultDP = Default_s::find(12)->DefaultID;
        if ($slip == '-1') {
            $cari = 'ME-' . $dateText;
            $header->JournalType = 'Memorial';
            $akun = array($defaultHutang, $defaultOutcome, $defaultDiscount, $defaultDP);
            $header->JournalID = JournalHeader::getNextIDJournal($cari . '-');
            $header->SlipInternalID = Null;
        } else {
            $akun = array("Slip", $defaultOutcome, $defaultDiscount, $defaultDP);
            $tampSlipID = Slip::find($slip);
            if ($tampSlipID->Flag == '0') {
                $cari = 'CO-' . $dateText;
                $header->JournalType = 'Cash Out';
            } else if ($tampSlipID->Flag == '1') {
                $cari = 'BO-' . $dateText;
                $header->JournalType = 'Bank Out';
            }
            $header->JournalID = JournalHeader::getNextIDJournal($cari . '-' . $tampSlipID->SlipID . '-');
            $header->SlipInternalID = $slip;
        }
        $header->JournalDate = $date[2] . '-' . $date[1] . '-' . $date[0];
        $header->JournalFrom = Auth::user()->UserID;
        $header->Notes = '';
        $department = Department::where('Default', '1')->first();
        $header->DepartmentInternalID = $department->InternalID;
        $header->TransactionID = $purchaseNumber;
        $header->ACC5InternalID = '0';
        $header->Lock = '0';
        $header->Check = '0';
        $header->Flag = '0';
        $header->UserRecord = Auth::user()->UserID;
        $header->CompanyInternalID = Auth::user()->Company->InternalID;
        $header->UserModified = '0';
        $header->Remark = '';
        $header->save();

        //insert detail
        if ($vat == 1) {
            $vatValue = 10 * $total / 100;
        } else {
            $vatValue = 0;
        }
        $count = 1;
        foreach ($dataType as $data2) {
            $tipe = InventoryType::find($data2->InternalID);
            $detail = new JournalDetail();
            $detail->JournalInternalID = $header->InternalID;
            $detail->JournalIndex = $count;
            $detail->JournalNotes = $tipe->InventoryTypeName;
            $detail->JournalDebetMU = $data2->total;
            $detail->JournalCreditMU = 0;
            $detail->CurrencyInternalID = $currency;
            $detail->CurrencyRate = $rate;
            $detail->JournalDebet = $data2->total * $rate;
            $detail->JournalCredit = 0;
            $detail->JournalTransactionID = NULL;
            $detail->ACC1InternalID = $tipe->ACC1InternalID;
            $detail->ACC2InternalID = $tipe->ACC2InternalID;
            $detail->ACC3InternalID = $tipe->ACC3InternalID;
            $detail->ACC4InternalID = $tipe->ACC4InternalID;
            $detail->ACC5InternalID = $tipe->ACC5InternalID;
            $detail->ACC6InternalID = $tipe->ACC6InternalID;
            $coa = Coa::getInternalID($tipe->ACC1InternalID, $tipe->ACC2InternalID, $tipe->ACC3InternalID, $tipe->ACC4InternalID, $tipe->ACC5InternalID, $tipe->ACC6InternalID);
            $detail->COAName = Coa::find($coa)->COAName;
            $detail->UserRecord = Auth::user()->UserID;
            $detail->UserModified = '0';
            $detail->save();
            $count++;
        }

        foreach ($akun as $data) {
            $kreditValue = 0;
            $debetValue = 0;
            if (($data != $defaultOutcome || $vatValue != 0) && ($data != $defaultDiscount || $discountGlobal != 0) && ($data != $defaultDP || $dp != 0)) {
                $detail = new JournalDetail();
                $detail->JournalInternalID = $header->InternalID;
                $detail->JournalIndex = $count;
                $detail->JournalNotes = $data;
                if ($data == $defaultHutang || $data == 'Slip') {
                    $kreditValue = $total + $vatValue;
                    $detail->JournalDebetMU = 0;
                    $detail->JournalCreditMU = $kreditValue;
                } else if ($data == $defaultDiscount) {
                    $kreditValue = $discountGlobal;
                    $detail->JournalDebetMU = 0;
                    $detail->JournalCreditMU = $kreditValue;
                } else if ($data == $defaultDP) {
                    $kreditValue = $dp;
                    $detail->JournalDebetMU = 0;
                    $detail->JournalCreditMU = $kreditValue;
                } else {
                    $debetValue = $vatValue;
                    $detail->JournalDebetMU = $debetValue;
                    $detail->JournalCreditMU = 0;
                }
                $detail->CurrencyInternalID = $currency;
                $detail->CurrencyRate = $rate;
                $detail->JournalDebet = $debetValue * $rate;
                $detail->JournalCredit = $kreditValue * $rate;
                $detail->JournalTransactionID = NULL;
                if ($data != 'Slip') {
                    $default = Default_s::getInternalCoa($data);
                } else {
                    $default = Slip::getInternalCoa($slip);
                }
                $detail->ACC1InternalID = $default->ACC1InternalID;
                $detail->ACC2InternalID = $default->ACC2InternalID;
                $detail->ACC3InternalID = $default->ACC3InternalID;
                $detail->ACC4InternalID = $default->ACC4InternalID;
                $detail->ACC5InternalID = $default->ACC5InternalID;
                $detail->ACC6InternalID = $default->ACC6InternalID;
                $coa = Coa::getInternalID($default->ACC1InternalID, $default->ACC2InternalID, $default->ACC3InternalID, $default->ACC4InternalID, $default->ACC5InternalID, $default->ACC6InternalID);
                $detail->COAName = Coa::find($coa)->COAName;
                $detail->UserRecord = Auth::user()->UserID;
                $detail->UserModified = '0';
                $detail->save();
                $count++;
            }
        }
    }

    function purchasePrint($id) {
        $id = PurchaseAddHeader::getIdpurchase($id);
        $header = PurchaseAddHeader::find($id);
        $detail = PurchaseAddHeader::find($id)->purchaseDetail()->get();
        $headerOrder = PurchaseOrderHeader::find($header->PurchaseOrderInternalID);
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $coa6 = PurchaseAddHeader::find($header->InternalID)->coa6;
            $supplier = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . ', ' . $coa6->Address . '<br>Phone: ' . $coa6->Phone . ', Fax: ' . $coa6->Fax;
            if ($header->isCash == 0) {
                $payment = 'Cash';
            } else {
                $payment = 'Credit';
            }
            $currency = Currency::find($header->CurrencyInternalID);
            $currencyName = $currency->CurrencyName;
            if ($header->VAT == 0) {
                $vat = 'Non Tax';
            } else {
                $vat = 'Tax';
            }
            $html = '
        <html>
            <head>
                <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 6px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box; position: relative;">
                    <div style="height: 195px;border-bottom:1px solid #ddd;margin-bottom:20px;">
                        <div style="height: 133px">
                        <img height="100%" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                        </div>
                        <div style="width: 420px; float: left;">
                            <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->Company->CompanyName . '</b>
                        </div> 
                        <div style=" box-sizing: border-box; margin-top: 0px;position: absolute; right: 2px; float: right;">
                             <table>
                             <tr style="background: none;">
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Address</td>
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                             </tr>
                            <tr style="background: none;">
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                             </tr>
                            <tr style="background: none;">
                                <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Phone Number</td>
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                             </tr>
                             </table>
                        </div>           
                    </div>
                    <h5 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Purchase</h5>
                    <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                        <table>
                        <tr>
                        <td width="275px">
                            <table>
                             <tr style="background: none;">
                                <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Purchase ID</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->PurchaseID . '</td>
                             </tr>
                            <tr style="background: none;">
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Date</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->PurchaseDate)) . '</td>
                             </tr>
                            <tr style="background: none;">
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Supplier</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $supplier . '</td>
                             </tr>
                            <tr style="background: none;">
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Payment</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $payment . '</td>
                             </tr>
                             ';
            if ($header->isCash != 0) {
                $html .= '<tr style="background: none;">
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Due Date</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime("+" . $header->LongTerm . " day", strtotime($header->PurchaseDate))) . '</td>
                         </tr>';
            }
            $html .= '
                            </table>
                            </td>
                            <td>
                            <table><tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Warehouse</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Warehouse->WarehouseName . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Purchase Order</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $headerOrder->PurchaseOrderID . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Currency</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $currencyName . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Rate</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . number_format($header->CurrencyRate, '2', '.', ',') . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Vat</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $vat . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Remark</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Remark . '</td>
                                 </tr>
                            </table></td>
                            </tr>
                        </table>
                    </div>    
                        <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
                                <thead >
                                    <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="25%">Inventory</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Qty</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="20%">Price</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc (%)</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="20%">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>';

            $total = 0;
            $totalVAT = 0;
            if (count($detail) > 0) {
                foreach ($detail as $data) {
                    $inventory = Inventory::find($data->InventoryInternalID);
                    $inv = $inventory->InventoryID . ' ' . $inventory->InventoryName;
                    $html.= '<tr>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $inv . '</td>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                        </tr>';
                    $totalVAT += $data->VAT;
                    $total += $data->SubTotal;
                }
                if ($totalVAT != 0) {
                    $totalVAT = $totalVAT - ($header->DiscountGlobal * 0.1);
                }
            } else {
                $html.= '<tr>
                        <td colspan="6" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this purchase.</td>
                    </tr>';
            }
            $html.= '</tbody>
                        </table>
                        <div style="box-sizing: border-box;min-width: 200px; margin-left: 320px; display: inline-block; clear: both;">
                             <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Discount</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Grand Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total - $header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Tax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($totalVAT, '2', '.', ',') . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; ">Grand Total (Tax)</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">'
                    . number_format($header->GrandTotal, '2', '.', ',') . '</td>
                                 </tr>
                                </table>
                        </div>
                </div>
            </body>
        </html>';
            return PDF::load($html, 'A5', 'potrait')->show();
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showPurchase');
        }
    }

    function purchasePrintSJ($id) {
        $id = PurchaseAddHeader::getIdpurchase($id);
        $header = PurchaseAddHeader::find($id);
        $detail = PurchaseAddHeader::find($id)->purchaseDetail()->get();
        $headerOrder = PurchaseOrderHeader::find($header->PurchaseOrderInternalID);
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $coa6 = PurchaseAddHeader::find($header->InternalID)->coa6;
            $supplier = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . ', ' . $coa6->Address . '<br>Phone: ' . $coa6->Phone . ', Fax: ' . $coa6->Fax;
            if ($header->isCash == 0) {
                $payment = 'Cash';
            } else {
                $payment = 'Credit';
            }
            $currency = Currency::find($header->CurrencyInternalID);
            $currencyName = $currency->CurrencyName;
            if ($header->VAT == 0) {
                $vat = 'Non Tax';
            } else {
                $vat = 'Tax';
            }
            $html = '
        <html>
            <head>
                <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 6px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box; position: relative;">
                    <div style="height: 195px;border-bottom:1px solid #ddd;margin-bottom:20px;">
                        <div style="height: 133px">
                        <img height="100%" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                        </div>
                        <div style="width: 420px; float: left;">
                            <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->Company->CompanyName . '</b>
                        </div> 
                        <div style=" box-sizing: border-box; margin-top: 0px;position: absolute; right: 2px; float: right;">
                             <table>
                             <tr style="background: none;">
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Address</td>
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                             </tr>
                            <tr style="background: none;">
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                             </tr>
                            <tr style="background: none;">
                                <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Phone Number</td>
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
                                <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                             </tr>
                             </table>
                        </div>           
                    </div>
                    <h5 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Purchase</h5>
                    <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                        <table>
                        <tr>
                        <td width="275px">
                            <table>
                             <tr style="background: none;">
                                <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Purchase ID</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->PurchaseID . '</td>
                             </tr>
                            <tr style="background: none;">
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Date</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->PurchaseDate)) . '</td>
                             </tr>
                            <tr style="background: none;">
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Supplier</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $supplier . '</td>
                             </tr>
                            </table>
                            </td>
                            <td>
                            <table><tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Warehouse</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Warehouse->WarehouseName . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Purchase Order</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $headerOrder->PurchaseOrderID . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Remark</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Remark . '</td>
                                 </tr>
                            </table></td>
                            </tr>
                        </table>
                    </div>    
                            <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
                                <thead >
                                    <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="75%">Inventory</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="25%">Qty</th>
                                    </tr>
                                </thead>
                                <tbody>';

            $total = 0;
            $totalVAT = 0;
            if (count($detail) > 0) {
                foreach ($detail as $data) {
                    $inventory = Inventory::find($data->InventoryInternalID);
                    $inv = $inventory->InventoryID . ' ' . $inventory->InventoryName;
                    $html.= '<tr>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $inv . '</td>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                        </tr>';
                    $totalVAT += $data->VAT;
                    $total += $data->SubTotal;
                }
                if ($totalVAT != 0) {
                    $totalVAT = $totalVAT - ($header->DiscountGlobal * 0.1);
                }
            } else {
                $html.= '<tr>
                        <td colspan="2" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this purchase.</td>
                    </tr>';
            }
            $html.= '</tbody>
                        </table>
                            <table width="100%" style="margin-top: 10px;">
                            <tr>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; padding-left: 50px; font-weight: 500;" width="60%">
                            Tanda Terima.
                            </td>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; padding-left: 50px; font-weight: 500;" width="40%">
                            Hormat Kami.
                            </td>
                            </tr>
                            </table>
                </div>
            </body>
        </html>';
            return PDF::load($html, 'A5', 'potrait')->show();
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showPurchase');
        }
    }

    function createID($tipe) {
        $purchase = 'PI';
        if ($tipe == 0) {
            $purchase .= '.' . date('m') . date('y');
        }
        return $purchase;
    }

    public function formatCariIDPurchase() {
        $date = explode('-', Input::get('date'));
        $id = $this->createID(1);
        $yearDigit = substr($date[2], 2);
        $id .= '.' . $date[1] . $yearDigit . '.';
        echo PurchaseAddHeader::getNextIDPurchase($id);
    }

    function purchaseCSV($id) {
        $id = PurchaseHeader::getIdpurchase($id);
        $header = PurchaseHeader::find($id);
        Session::flash('idCSV', $id);
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            Excel::create('Tax_Report_purchase', function($excel) {
                $excel->sheet('Tax_Report_purchase', function($sheet) {
                    //baris pertama
                    $sheet->setCellValueByColumnAndRow(0, 1, "FM");
                    $sheet->setCellValueByColumnAndRow(1, 1, "KD_JENIS_TRANSAKSI");
                    $sheet->setCellValueByColumnAndRow(2, 1, "FG_PENGGANTI");
                    $sheet->setCellValueByColumnAndRow(3, 1, "NOMOR_FAKTUR");
                    $sheet->setCellValueByColumnAndRow(4, 1, "MASA_PAJAK");
                    $sheet->setCellValueByColumnAndRow(5, 1, "TAHUN_PAJAK");
                    $sheet->setCellValueByColumnAndRow(6, 1, "TANGGAL_FAKTUR");
                    $sheet->setCellValueByColumnAndRow(7, 1, "NPWP");
                    $sheet->setCellValueByColumnAndRow(8, 1, "NAMA");
                    $sheet->setCellValueByColumnAndRow(9, 1, "ALAMAT_LENGKAP");
                    $sheet->setCellValueByColumnAndRow(10, 1, "JUMLAH_DPP");
                    $sheet->setCellValueByColumnAndRow(11, 1, "JUMLAH_PPN");
                    $sheet->setCellValueByColumnAndRow(12, 1, "JUMLAH_PPNBM");
                    $sheet->setCellValueByColumnAndRow(13, 1, "IS_CREDITABLE");

                    $row = 2;
                    
                    $data = PurchaseHeader::find(Session::get('idCSV'));
                    $sheet->setCellValueByColumnAndRow(0, $row, "FM");
                    $sheet->setCellValueByColumnAndRow(1, $row, "01");
                    $sheet->setCellValueByColumnAndRow(2, $row, "0");
                    //nomorPajak
                    $pajakSupplier = str_replace('.', '', $data->coa6->TaxID);
                    $pajakSupplier = str_replace('-', '', $pajakSupplier);
                    
                    if ($pajakSupplier == "" || $pajakSupplier == 0) {
                        $pajakSupplier = "'000000000000000";
                    }
                    $pajak = str_replace('.', '', $data->TaxNumber);
                    $pajak = str_replace('-', '', $pajak);
                    $pajak = substr($pajak, 4);
                    if ($pajak == "" || $pajak == 0) {
                        $pajak = "'000000000000000";
                    }
                    $total = PurchaseDetail::where('PurchaseInternalID', $data->InternalID)->sum('SubTotal') - $data->DiscountGlobal - $data->DownPayment;
                    $sheet->setCellValueByColumnAndRow(3, $row, $pajak);
                    $sheet->setCellValueByColumnAndRow(4, $row, $data->TaxMonth);
                    $sheet->setCellValueByColumnAndRow(5, $row, $data->TaxYear);
                    $sheet->setCellValueByColumnAndRow(6, $row, date('d/m/Y', strtotime($data->TaxDate)));
                    $sheet->setCellValueByColumnAndRow(7, $row, $pajakSupplier);
                    $sheet->setCellValueByColumnAndRow(8, $row, $data->coa6->ACC6Name);
                    $sheet->setCellValueByColumnAndRow(9, $row, $data->coa6->Address);
                    $sheet->setCellValueByColumnAndRow(10, $row, $total);
                    $sheet->setCellValueByColumnAndRow(11, $row, $total * 0.1);
                    $sheet->setCellValueByColumnAndRow(12, $row, 0);
                    $sheet->setCellValueByColumnAndRow(13, $row, 1);
                    $row++;

                    $row--;
                });
            })->export('csv');
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showPurchase');
        }
    }

    public function summaryPurchase() {
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $totalP = 0;
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Purchase Summary Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span>';
        $hitung = 0;
        foreach (Coa6::where("Type", "s")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $dataSupplier) {
            if (PurchaseHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                            ->where('ACC6InternalID', $dataSupplier->InternalID)
                            ->whereBetween('PurchaseDate', Array($start, $end))->count() > 0) {
            $html.= '<table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=7>' . $dataSupplier->ACC6Name . '</th>
                                        </tr>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Purchase ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Currency</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Rate</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total(After Discount)</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Vat</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Grand Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                $sumGrandTotal = 0;
                foreach (PurchaseHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where('ACC6InternalID', $dataSupplier->InternalID)
                        ->whereBetween('PurchaseDate', Array($start, $end))->get() as $data) {
                    $grandTotal = $data->GrandTotal;
                    $sumGrandTotal += $grandTotal;
                    $totalSA += $grandTotal;
                    $total = $grandTotal;
                    $vat = 0;
                    if ($data->VAT == 1) {
                        $total = $total * 10 / 11;
                        $vat = $total / 10;
                    }
                    $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->PurchaseID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->PurchaseDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->Currency->CurrencyName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->CurrencyRate, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($total, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($vat, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($grandTotal, '2', '.', ',') . '</td>
                            </tr>';
                }
                $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="80%" colspan="6">Total : </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . number_format($sumGrandTotal, '2', '.', ',') . '</td>
                        </tr>';
            $html.= '</tbody>
            </table>';
                $hitung++;
            }
        }

        if ($hitung == 0) {
            $html.='<br><span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no purchase.</span>';
        }
        $html.= '<table class="tableBorder" width="100%"  style="margin-top: 10px; clear: both;  top: 50px;">
                    <thead>
                        <tr>
                            <th style="font-family: helvetica,sans-serif;font-size: 14px; margin: 5px !important; font-weight: 700; text-align: right" width="80%" colspan="6">Total Purchase : </th>
                            <th style="font-family: helvetica,sans-serif;font-size: 14px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . number_format($totalP, '2', '.', ',') . '</th>
                        </tr>
                    <thead>
                </table>';
        $html.='</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('purchase_summary');
    }

    public function detailPurchase() {
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $dateTamp = '';
        $coa6Tamp = '';
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Purchase Detail Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span><br><br>';
        if (PurchaseHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                        ->whereBetween('PurchaseDate', Array($start, $end))
                        ->orderBy('PurchaseDate')->orderBy('ACC6InternalID')->count() > 0) {
            foreach (PurchaseHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                    ->whereBetween('PurchaseDate', Array($start, $end))
                    ->orderBy('PurchaseDate')->orderBy('ACC6InternalID')->get() as $dataPembelian) {
                if ($dateTamp != date("d-M-Y", strtotime($dataPembelian->PurchaseDate))) {
                    $html.= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Purchase Date : ' . date("d-M-Y", strtotime($dataPembelian->PurchaseDate)) . '</span><br><br>';
                    $dateTamp = date("d-M-Y", strtotime($dataPembelian->PurchaseDate));
                    $coa6Tamp = '';
                }
                if ($coa6Tamp != $dataPembelian->ACC6InternalID) {
                    $html.= '<span style="padding-left:23px;font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Supplier : ' . $dataPembelian->coa6->ACC6Name . '</span>';
                    $coa6Tamp = $dataPembelian->ACC6InternalID;
                }
                $html.= '<table class="tableBorder" style="width:95%; margin-top: 18px; clear: both; position: relative; left: 4%">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=7>' . $dataPembelian->PurchaseID . ' | ' . $dataPembelian->Currency->CurrencyName . ' | Rate : ' . number_format($dataPembelian->CurrencyRate, '2', '.', ',') . ' </th>
                                        </tr>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory ID</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Name</th>
                                            <th width="10%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Qty</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Price</th>
                                            <th width="10%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc (%)</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                $total = 0;
                $vat = 0;
                foreach ($dataPembelian->purchaseDetail as $data) {
                    $total += $data->SubTotal;
                    $vat += $data->VAT;
                    $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->inventory->InventoryID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->inventory->InventoryName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
                }
                $html.= '<tr>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=4></td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=2>Total <br> Discount <br> Grand Total <br> VAT <br> Grand Total </td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '<br> '
                        . '' . number_format($dataPembelian->DiscountGlobal, '2', '.', ',') . '<br>'
                        . '' . number_format($total - $dataPembelian->DiscountGlobal, '2', '.', ',') . '<br>'
                        . '' . number_format($vat, '2', '.', ',') . '<br>'
                        . '' . number_format($dataPembelian->GrandTotal, '2', '.', ',') . '</td>
                    </tr>
                </tbody>
            </table>';
            }
        } else {
            $html.= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no purchase.</span><br><br>';
        }
        $html.='</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('purchase_detail');
    }

    public function getResultSearchP() {
        $hitung = 0;
        $input = splitSearchValue(Input::get("id"));
        $purchasseHeader = PurchaseOrderHeader::join("m_coa6", 'm_coa6.InternalID', '=', 't_purchaseorder_header.ACC6InternalID')
                ->where('t_purchaseorder_header.CompanyInternalID', '=', Auth::user()->Company->InternalID)
                ->Where(function($query) use($input) {
                    $query->orWhere('PurchaseOrderID', "like", '%' . $input . "%")
                    ->orWhere("PurchaseOrderDate", "like", '%' . date("Y-m-d", strtotime(Input::get("id"))) . "%")
                    ->orWhere("ACC6Name", "like", '%' . $input . "%");
                })
                ->OrderBy('PurchaseOrderDate', 'desc')
                ->select('t_purchaseorder_header.*', 'ACC6Name')
                ->take(100)
                ->get();
        if (count($purchasseHeader) == 0) {
            ?>
            <span>There is no result with keywords <i><u><?php echo Input::get("id"); ?></u></i></span>
            <?php
        } else {
            ?>
            <select class="chosen-select choosen-modal" id="purchase" style="" name="purchase">
                <?php
                foreach ($purchasseHeader as $purchaseorder) {
                    if (checkPurchaseAdd($purchaseorder->InternalID)) {
                        ?>
                        <option value="<?php echo $purchaseorder->PurchaseOrderID ?>"><?php echo $purchaseorder->PurchaseOrderID . ' | ' . date("d-m-Y", strtotime($purchaseorder->PurchaseOrderDate)) . ' | ' . $purchaseorder->ACC6Name ?></option>
                        <?php
                        $hitung = 1;
                    }
                }
                ?>
            </select>
            <script>
                $(document).ready(function () {
                    var hitung = '<?php echo $hitung; ?>';
                    if (hitung == 0) {
                        $('#purchase').after('<span>There is no result.</span>');
                        $('#purchase').remove();
                    } else {
                        $("#btn-add-sr").removeAttr("disabled");
                    }
                });
            </script>
            <?php
        }
    }

    static function purchaseDataBackup($data) {
        $explode = explode('---;---', $data);
        $typePayment = $explode[0];
        $typeTax = $explode[1];
        $start = $explode[2];
        $end = $explode[3];
        $where = '';
        if ($typePayment != '-1' && $typePayment != '') {
            $where .= 'isCash = "' . $typePayment . '" ';
        }
        if ($typeTax != '-1' && $typeTax != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'VAT = "' . $typeTax . '" ';
        }
        if ($start != '' && $end != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $start = explode('-', $start);
            $end = explode('-', $end);
            $startSearch = $start[2] . '-' . $start[1] . '-' . $start[0];
            $endSearch = $end[2] . '-' . $end[1] . '-' . $end[0];
            $where .= 'PurchaseDate between "' . $startSearch . ' 00:00:00" AND "' . $endSearch . ' 23:59:59"';
        }
        $table = 't_purchase_header';
        $primaryKey = 't_purchase_header`.`InternalID';
        $columns = array(
            array('db' => 'PurchaseID', 'dt' => 0),
            array('db' => 't_purchase_header`.`InternalID', 'dt' => 1, 'formatter' => function( $d, $row ) {
                    $purchase = PurchaseHeader::find($d);
                    if ($purchase->isCash == 0) {
                        return 'Cash (-)';
                    } else {
                        return 'Credit ' . $purchase->LongTerm;
                    }
                },
                'field' => 't_purchase_header`.`InternalID'),
            array(
                'db' => 'PurchaseDate',
                'dt' => 2,
                'formatter' => function( $d, $row ) {
                    return date("d-m-Y", strtotime($d));
                }
            ),
            array('db' => 'CurrencyName', 'dt' => 3),
            array(
                'db' => 'CurrencyRate',
                'dt' => 4,
                'formatter' => function( $d, $row ) {
                    return number_format($d, '2', '.', ',');
                }
            ),
            array(
                'db' => 'ACC6Name',
                'dt' => 5,
                'formatter' => function( $d, $row ) {
                    return $d;
                }
            ),
            array(
                'db' => 'VAT',
                'dt' => 6,
                'formatter' => function( $d, $row ) {
                    if ($d == 0) {
                        return 'Non Tax';
                    } else {
                        return 'Tax';
                    }
                }
            ),
            array(
                'db' => 'GrandTotal',
                'dt' => 7,
                'formatter' => function( $d, $row ) {
                    return number_format($d, '2', '.', ',');
                }
            ),
            array(
                'db' => 'DownPayment',
                'dt' => 8,
                'formatter' => function( $d, $row ) {
                    return number_format($d, '2', '.', ',');
                }
            ),
            array(
                'db' => 'PurchaseID',
                'dt' => 9,
                'formatter' => function( $d, $row ) {
                    $tampReceiv = 'Completed';
                    foreach (PurchaseHeader::getPurchasePayable() as $receiv) {
                        if ($receiv->ID == $d) {
                            $tampReceiv = 'Uncompleted ';
                            $tampReceiv .= number_format($receiv->GrandTotal - JournalDetail::where('JournalTransactionID',$receiv->ID)->sum('JournalDebetMU'),'2','.',',');
                        }
                    }
                    return $tampReceiv;
                }
            ),
            array('db' => 'TaxNumber', 'dt' => 10),
            array('db' => 't_purchase_header`.`InternalID', 'dt' => 11, 'formatter' => function( $d, $row ) {
                    $data = PurchaseHeader::find($d);
                    $action = '<td class="text-center">
                                    <a href="' . Route('purchaseDetail', $data->PurchaseID) . '">
                                        <button id="btn-' . $data->PurchaseID . '-detail"
                                                class="btn btn-pure-xs btn-xs btn-detail">
                                            <span class="glyphicon glyphicon-zoom-in"></span>
                                        </button>
                                    </a>';
                    if (!PurchaseHeader::isReturn($data->PurchaseID) && JournalDetail::where('JournalTransactionID', $data->PurchaseID)->sum('JournalCreditMU') == 0) {
                        $action.='<a href="' . Route('purchaseUpdate', $data->PurchaseID) . '">
                                        <button id="btn-' . $data->PurchaseID . '-update"
                                                class="btn btn-pure-xs btn-xs btn-edit">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </button>
                                    </a>
                                    <button data-target="#m_purchaseDelete" data-internal="' . $data->InternalID . '"  data-toggle="modal" role="dialog"
                                           onclick="deleteAttach(this)" data-id="' . $data->PurchaseID . '" data-name=' . $data->PurchaseID . ' class="btn btn-pure-xs btn-xs btn-delete">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>';
                    } else {
                        $action.='<button disabled class="btn btn-pure-xs btn-xs btn-edit"><span class="glyphicon glyphicon-edit"></span></button>
                                    <button disabled class="btn btn-pure-xs btn-xs btn-delete"><span class="glyphicon glyphicon-trash"></span></button>';
                    }
                    if (checkModul('O05')) {
                        $action.='<a href="' . Route('purchaseCSV', $data->PurchaseID) . '" target="_blank">
                                        <button id="btn-' . $data->PurchaseID . '-print"
                                                class="btn btn-pure-xs btn-xs">
                                            <span class="glyphicon glyphicon-download"></span> CSV
                                        </button>
                                    </a>';
                    }
                    return $action;
                },
                'field' => 't_purchase_header`.`InternalID')
        );

        $sql_details = getConnection();

        require('ssp.class.php');
        $ID_CLIENT_VALUE = Auth::user()->CompanyInternalID;
        if ($where != '') {
            $extraCondition = $where . ' AND ' . 't_purchase_header.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        } else {
            $extraCondition = 't_purchase_header.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        }
        $join = ' INNER JOIN m_currency on m_currency.InternalID = t_purchase_header.CurrencyInternalID '
                . 'INNER JOIN m_coa6 on m_coa6.InternalID = t_purchase_header.ACC6InternalID';

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $extraCondition, $join));
    }

}
