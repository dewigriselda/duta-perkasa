<?php

class SalesReturnController extends BaseController {

    public function showSalesReturn() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteSalesReturn') {
                return $this->deleteSalesReturn();
            } else if (Input::get('jenis') == 'summarySales') {
                return $this->summarySalesReturn();
            } else if (Input::get('jenis') == 'detailSales') {
                return $this->detailSalesReturn();
            } else if (Input::get('jenis') == 'insertSalesReturn') {
                return Redirect::Route('salesReturnNew', Input::get('sales'));
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = SalesReturnHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
            return View::make('penjualan.salesReturnSearch')
                            ->withToogle('transaction')->withAktif('salesReturn')
                            ->withData($data);
        }
        return View::make('penjualan.salesReturn')
                        ->withToogle('transaction')->withAktif('salesReturn');
    }

    public function salesReturnNew($id) {
        $id = SalesHeader::getIdsales($id);
        $header = SalesHeader::find($id);
        $detail = SalesHeader::find($id)->salesDetail()->get();
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertSalesReturn') {
                return Redirect::Route('salesReturnNew', Input::get('sales'));
            } else if (Input::get('jenis') == 'summarySales') {
                return $this->summarySalesReturn();
            } else if (Input::get('jenis') == 'detailSales') {
                return $this->detailSalesReturn();
            } else if (Input::get('jenis') == 'deleteSalesReturn') {
                return $this->deleteSalesReturn();
            } else {
                return $this->insertSalesReturn();
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = SalesReturnHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
            return View::make('penjualan.salesReturnSearch')
                            ->withToogle('transaction')->withAktif('salesReturn')
                            ->withData($data);
        }
        return View::make('penjualan.salesReturnNew')
                        ->withToogle('transaction')->withAktif('salesReturn')
                        ->withHeader($header)
                        ->withDetail($detail);
    }

    public function salesReturnDetail($id) {
        $id = SalesReturnHeader::getIdsalesReturn($id);
        $header = SalesReturnHeader::find($id);
        $detail = SalesReturnHeader::find($id)->salesReturnDetail()->get();
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'insertSalesReturn') {
                return Redirect::Route('salesReturnNew', Input::get('sales'));
            } else if (Input::get('jenis') == 'summarySales') {
                return $this->summarySalesReturn();
            } else if (Input::get('jenis') == 'detailSales') {
                return $this->detailSalesReturn();
            }
        }
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            return View::make('penjualan.salesReturnDetail')
                            ->withToogle('transaction')->withAktif('salesReturn')
                            ->withHeader($header)
                            ->withDetail($detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showSalesReturn');
        }
    }

    public function salesReturnUpdate($id) {
        $salesIDReal = substr($id, 7);
        $id = SalesReturnHeader::getIdsalesReturn($id);
        $header = SalesReturnHeader::find($id);
        $detail = SalesReturnHeader::find($id)->salesReturnDetail()->get();
        $idSales = SalesHeader::getIdsales($salesIDReal);
        $headerSales = SalesHeader::find($idSales);
        $detailSales = SalesHeader::find($idSales)->salesDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                if (Input::get('jenis') == 'insertSalesReturn') {
                    return Redirect::Route('salesReturnNew', Input::get('sales'));
                } else if (Input::get('jenis') == 'summarySales') {
                    return $this->summarySalesReturn();
                } else if (Input::get('jenis') == 'detailSales') {
                    return $this->detailSalesReturn();
                } else {
                    return $this->updateSalesReturn($id);
                }
            }
            return View::make('penjualan.salesReturnUpdate')
                            ->withToogle('transaction')->withAktif('salesReturn')
                            ->withHeader($header)
                            ->withDetail($detail)
                            ->withHeadersales($headerSales)
                            ->withDetailsales($detailSales);
        } else {
            $messages = 'accessDenied';

            Session::flash('messages', $messages);
            return Redirect::Route('showSalesReturn');
        }
    }

    public function insertSalesReturn() {
        //rule
        $rule = array(
            'date' => 'required',
            'warehouse' => 'required',
            'remark' => 'required|max:1000'
        );
        $salesNumber = '';
        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalInsert';
            $error = $validator->messages();
        } else {
            $id = Input::get('SalesInternalID');
            $header = SalesHeader::find($id);
            $date = explode('-', Input::get('date'));
            //insert header
            $headerI = new SalesReturnHeader;
            $salesReturnNumber = SalesReturnHeader::getNextIDSalesReturn($header->SalesID);
            $headerI->SalesReturnID = $salesReturnNumber;
            $headerI->SalesReturnDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $headerI->ACC6InternalID = $header->ACC6InternalID;
            $headerI->LongTerm = $header->LongTerm;
            $headerI->isCash = $header->isCash;
            $headerI->CurrencyInternalID = $header->CurrencyInternalID;
            $headerI->WarehouseInternalID = Input::get('warehouse');
            $headerI->CurrencyRate = $header->CurrencyRate;
            $headerI->VAT = $header->VAT;
            $headerI->DiscountGlobal = str_replace(",", "", Input::get('DiscountGlobal'));
            $headerI->GrandTotal = Input::get('grandTotalValue');
            $headerI->UserRecord = Auth::user()->UserID;
            $headerI->CompanyInternalID = Auth::user()->Company->InternalID;
            $headerI->UserModified = '0';
            $headerI->Remark = Input::get('remark');
            $headerI->save();
            //insert detail
            $total = 0;
            for ($a = 0; $a < count(Input::get('inventory')); $a++) {
                $qtyValue = str_replace(',', '', Input::get('qty')[$a]);
                $qtyReturnValue = str_replace(',', '', Input::get('returnSales')[$a]);
                if ($qtyValue < $qtyReturnValue) {
                    $qtyReturnValue = $qtyValue;
                }
                $priceValue = Input::get('price')[$a];
                $discValue = Input::get('discountNominal')[$a];
                $subTotal = ($priceValue * $qtyReturnValue ) - (($priceValue * $qtyReturnValue) * Input::get('discount')[$a] / 100) - $discValue * $qtyReturnValue;
                if ($header->VAT == '1') {
                    $vatValue = $subTotal / 10;
                } else {
                    $vatValue = 0;
                }
                if ($qtyReturnValue > 0) {
                    $detail = new SalesReturnDetail();
                    $detail->SalesReturnInternalID = $headerI->InternalID;
                    $detail->InventoryInternalID = Input::get('inventory')[$a];
                    $detail->Qty = $qtyReturnValue;
                    $detail->Price = $priceValue;
                    $detail->Discount = Input::get('discount')[$a];
                    $detail->DiscountNominal = $discValue;
                    $detail->VAT = $vatValue;
                    $detail->SubTotal = $subTotal;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = '0';
                    $detail->SalesDetailInternalID = Input::get('InternalDetail')[$a];
                    $detail->save();
                }
                $total += $subTotal;
            }
            if ($headerI->VAT == '1') {
                $vatValueHeader = ($total - str_replace(",", "", Input::get("DiscountGlobal"))) / 10;
            } else {
                $vatValueHeader = 0;
            }
            $headerI->GrandTotal = $total - str_replace(",", "", Input::get("DiscountGlobal")) + $vatValueHeader;
            $headerI->save();

            $rate = $header->CurrencyRate;
            $total -= str_replace(",", "", Input::get('DiscountGlobal'));
            if ($header->isCash == 0) {
                $slip = SalesHeader ::getSlipInternalID($header->SalesID);
                $this->insertJournal($salesReturnNumber, $total, $header->VAT, $header->CurrencyInternalID, $rate, Input::get('date'), $slip);
            } else {
                $slip = '-1';
                $this->insertJournal($salesReturnNumber, $total, $header->VAT, $header->CurrencyInternalID, $rate, Input::get('date'), $slip);
            }
            $messages = 'suksesInsert';
            $error = '';
        }
        $header = SalesHeader::find($id);
        $detail = SalesHeader::find($id)->salesDetail()->get();
        return View::make('penjualan.salesReturnNew')
                        ->withToogle('transaction')->withAktif('salesReturn')
                        ->withError($error)
                        ->withMessages
                                ($messages)
                        ->withHeader($header)
                        ->withDetail($detail);
    }

    public function updateSalesReturn($id) {
        //tipe
        $headerUpdate = SalesReturnHeader::find($id);
        $detailUpdate = SalesReturnHeader::find($id)->salesReturnDetail()->get();

        //rule
        $rule = array(
            'warehouse' => 'required',
            'remark' => 'required|max:1000',
            'inventory' => 'required'
        );

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalUpdate';
            $error = $validator->messages();
        } else {
            //delete salesReturn detail -- nantinya insert ulang
            SalesReturnDetail::where('SalesReturnInternalID', '=', Input::get('SalesReturnInternalID'))->delete();

            //insert detail
            $total = 0;
            for ($a = 0; $a < count(Input::get('inventory')); $a++) {
                $qtyValue = str_replace(',', '', Input::get('qty')[$a]);
                $qtyReturnValue = str_replace(',', '', Input::get('returnSales')[$a]);
                if ($qtyValue < $qtyReturnValue) {
                    $qtyReturnValue = $qtyValue;
                }
                $priceValue = Input::get('price')[$a];
                $discValue = Input::get('discountNominal')[$a];
                $subTotal = ($priceValue * $qtyReturnValue ) - (($priceValue * $qtyReturnValue) * Input::get('discount')[$a] / 100) - $discValue * $qtyReturnValue;
                if ($headerUpdate->VAT == '1') {
                    $vatValue = $subTotal / 10;
                } else {
                    $vatValue = 0;
                }
                if ($qtyReturnValue > 0) {
                    $detail = new SalesReturnDetail();
                    $detail->SalesReturnInternalID = Input::get('SalesReturnInternalID');
                    $detail->InventoryInternalID = Input::get('inventory')[$a];
                    $detail->Qty = $qtyReturnValue;
                    $detail->Price = $priceValue;
                    $detail->Discount = Input::get('discount')[$a];
                    $detail->DiscountNominal = $discValue;
                    $detail->VAT = $vatValue;
                    $detail->SubTotal = $subTotal;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = Auth::user()->UserID;
                    $detail->SalesDetailInternalID = Input::get('InternalDetail')[$a];
                    $detail->save();
                }
                $total += $subTotal;
            }

            //change grandtotal jd yg bener
            $headerI = SalesReturnHeader::find(Input::get('SalesReturnInternalID'));
            $headerI->Remark = Input::get('remark');
            $headerI->WarehouseInternalID = Input::get('warehouse');
            $headerI->DiscountGlobal = str_replace(",", "", Input::get('DiscountGlobal'));
            if ($headerI->VAT == '1') {
                $vatValueHeader = ($total - str_replace(",", "", Input::get("DiscountGlobal"))) / 10;
            } else {
                $vatValueHeader = 0;
            }
            $headerI->GrandTotal = $total - str_replace(",", "", Input::get("DiscountGlobal")) + $vatValueHeader;
            $headerI->save();

            $journal = JournalHeader::where('TransactionID', '=', $headerUpdate->SalesReturnID)->get();
            foreach ($journal as $value) {
                JournalDetail:: where('JournalInternalID', '=', $value->InternalID)->delete();
                JournalHeader::where('InternalID', '=', $value->InternalID)->delete();
            }

            $currency = $headerUpdate->CurrencyInternalID;
            $rate = $headerUpdate->CurrencyRate;
            $date = date("d-m-Y", strtotime($headerUpdate->SalesReturnDate));
            $total -= str_replace(",", "", Input::get('DiscountGlobal'));
            if ($headerUpdate->isCash == 0) {
                $salesIDReal = substr($headerUpdate->SalesReturnID, 7);
                $slip = SalesHeader:: getSlipInternalID($salesIDReal);
                $this->insertJournal($headerUpdate->SalesReturnID, $total, $headerUpdate->VAT, $currency, $rate, $date, $slip);
            } else {
                $slip = '-1';
                $this->insertJournal($headerUpdate->SalesReturnID, $total, $headerUpdate->VAT, $currency, $rate, $date, $slip);
            }
            $messages = 'suksesUpdate';
            $error = '';
        }

        //tipe
        $header = SalesReturnHeader::find($id);
        $detail = SalesReturnHeader::find($id)->salesReturnDetail()->get();
        $salesReturnID = SalesReturnHeader::find($id)->SalesReturnID;
        $salesIDReal = substr($salesReturnID, 7);
        $idSales = SalesHeader::getIdsales($salesIDReal);
        $headerSales = SalesHeader::find($idSales);
        $detailSales = SalesHeader::find($idSales)->salesDetail()->get();
        return View::make('penjualan.salesReturnUpdate')
                        ->withToogle('transaction')->withAktif('salesReturn')
                        ->withHeader($header)
                        ->withDetail($detail)
                        ->withHeadersales($headerSales)
                        ->withDetailsales($detailSales)
                        ->withError($error)
                        ->withMessages($messages);
    }

    public function deleteSalesReturn() {
        $salesReturn = JournalDetail::where('JournalTransactionID',SalesReturnHeader::find(Input::get('InternalID'))->SalesReturnID)->count();
        if ($salesReturn <= 0) {
            //tidak ada yang menggunakan data salesReturn maka data boleh dihapus
            //hapus journal
            $salesReturnHeader = SalesReturnHeader::find(Input::get('InternalID'));
            if ($salesReturnHeader->CompanyInternalID == Auth::user()->Company->InternalID) {
                $journal = JournalHeader::where('TransactionID', '=', $salesReturnHeader->SalesReturnID)->get();
                foreach ($journal as $value) {
                    JournalDetail:: where('JournalInternalID', '=', $value->InternalID)->delete();
                    JournalHeader::where('InternalID', '=', $value->InternalID)->delete();
                }
                //hapus detil
                $detilData = SalesReturnHeader::find(Input::get('InternalID'))->salesReturnDetail;
                foreach ($detilData as $value) {
                    $detil = salesReturnDetail::find($value->InternalID);
                    $detil->delete();
                }
                //hapus salesReturn
                $salesReturn = SalesReturnHeader::find(Input::get('InternalID'));
                $salesReturn->delete();
                $messages = 'suksesDelete';
            } else {
                $messages = 'accessDenied';
            }
        } else {
            //ada maka data tidak boleh dihapus
            $messages = 'gagalDelete';
        } $data = SalesReturnHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
        return View::make('penjualan.salesReturnSearch')
                        ->withToogle('transaction')->withAktif(
                                'salesReturn')
                        ->withMessages($messages)->withData($data);
    }

    function insertJournal($salesReturnNumber, $total, $vat, $currency, $rate, $date, $slip) {
        $header = new JournalHeader;
        $date = explode('-', $date);
        $yearDigit = substr($date[2], 2);
        $dateText = $date[1] . $yearDigit;
        $defaultPenjualan = Default_s::find(1)->DefaultID;
        $defaultPiutang = Default_s::find(2)->DefaultID;
        $defaultIncome = Default_s::find(3)->DefaultID;
        if ($slip == '-1') {
            $cari = 'ME-' . $dateText;
            $header->JournalType = 'Memorial';
            $akun = array($defaultPenjualan, $defaultPiutang, $defaultIncome);
            $header->JournalID = JournalHeader::getNextIDJournal($cari . '-');
            $header->SlipInternalID = Null;
        } else {
            $akun = array($defaultPenjualan, 'Slip', $defaultIncome);
            $tampSlipID = Slip::find($slip);
            if ($tampSlipID->Flag == '0') {
                $cari = 'CO-' . $dateText;
                $header->JournalType = 'Cash In';
            } else if ($tampSlipID->Flag == '1') {
                $cari = 'BO-' . $dateText;
                $header->JournalType = 'Bank In';
            }
            $header->JournalID = JournalHeader::getNextIDJournal($cari . '-' . $tampSlipID->SlipID . '-');
            $header->SlipInternalID = $slip;
        }
        $header->JournalDate = $date[2] . '-' . $date[1] . '-' . $date[0];
        $header->JournalFrom = Auth::user()->UserID;
        $header->Notes = '';
        $department = Department::where('Default', '1')->first();
        $header->DepartmentInternalID = $department->InternalID;
        $header->TransactionID = $salesReturnNumber;
        $header->ACC5InternalID = '0';
        $header->Lock = '0';
        $header->Check = '0';
        $header->Flag = '0';
        $header->UserRecord = Auth::user()->UserID;
        $header->CompanyInternalID = Auth::user()->Company->InternalID;
        $header->UserModified = '0';
        $header->Remark = '';
        $header->save();

        //insert detail
        if ($vat == 1) {
            $vatValue = 10 * $total / 100;
        } else {
            $vatValue = 0;
        }
        $count = 1;
        foreach ($akun as $data) {
            $kreditValue = 0;
            $debetValue = 0;
            if ($data != $defaultIncome || ($data == $defaultIncome && $vatValue != 0)) {
                $detail = new JournalDetail();
                $detail->JournalInternalID = $header->InternalID;
                $detail->JournalIndex = $count;
                $detail->JournalNotes = $data;
                if ($data == $defaultPenjualan) {
                    $debetValue = $total;
                    $detail->JournalDebetMU = $debetValue;
                    $detail->JournalCreditMU = 0;
                } else if ($data == $defaultPiutang || $data == 'Slip') {
                    $kreditValue = $total + $vatValue;
                    $detail->JournalDebetMU = 0;
                    $detail->JournalCreditMU = $kreditValue;
                } else {
                    $debetValue = $vatValue;
                    $detail->JournalDebetMU = $debetValue;
                    $detail->JournalCreditMU = 0;
                }
                $detail->CurrencyInternalID = $currency;
                $detail->CurrencyRate = $rate;
                $detail->JournalDebet = $debetValue * $rate;
                $detail->JournalCredit = $kreditValue * $rate;
                $detail->JournalTransactionID = NULL;
                if ($data != 'Slip') {
                    $default = Default_s::getInternalCoa($data);
                } else {
                    $default = Slip::getInternalCoa($slip);
                }
                $detail->ACC1InternalID = $default->ACC1InternalID;
                $detail->ACC2InternalID = $default->ACC2InternalID;
                $detail->ACC3InternalID = $default->ACC3InternalID;
                $detail->ACC4InternalID = $default->ACC4InternalID;
                $detail->ACC5InternalID = $default->ACC5InternalID;
                $detail->ACC6InternalID = $default->ACC6InternalID;
                $coa = Coa::getInternalID($default->ACC1InternalID, $default->ACC2InternalID, $default->ACC3InternalID, $default->ACC4InternalID, $default->ACC5InternalID, $default->ACC6InternalID);
                $detail->COAName = Coa::find($coa)->COAName;
                $detail->UserRecord = Auth::user()->UserID;
                $detail->UserModified = '0';
                $detail->save();
                $count++;
            }
        }
    }

    function salesReturnPrint($id) {
        $id = SalesReturnHeader::getIdsalesReturn($id);
        $header = SalesReturnHeader::find($id);
        $detail = SalesReturnHeader::find($id)->salesReturnDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $coa6 = SalesReturnHeader::find($header->InternalID)->coa6;
            $customer = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . ', ' . $coa6->Address . '<br>Phone: ' . $coa6->Phone . ', Fax: ' . $coa6->Fax;
            if ($header->isCash == 0) {
                $payment = 'Cash';
            } else {
                $payment = 'Credit';
            }
            $currency = Currency::find($header->CurrencyInternalID);
            $currencyName = $currency->CurrencyName;
            if ($header->VAT == 0) {
                $vat = 'Non Tax';
            } else {
                $vat = 'Tax';
            }
            $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 6px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box; position: relative;">
                            <div style="height: 195px;border-bottom:1px solid #ddd;margin-bottom:20px;">
                        <div style="height: 133px">
                        <img height="100%" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                        </div>
                        <div style="width: 420px; float: left;">
                            <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->Company->CompanyName . '</b>
                        </div> 
                            <div style=" box-sizing: border-box; margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Address</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Sales Return</h5>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                            <table>
                            <tr>
                            <td width="275px">
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Sales Return ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->SalesReturnID . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->SalesReturnDate)) . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Customer</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $customer . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Payment</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $payment . '</td>
                                 </tr>
                                </table>
                                </td>
                                <td>
                                <table>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Warehouse</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Warehouse->WarehouseName . '</td>
                                     </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Currency</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $currencyName . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Rate</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . number_format($header->CurrencyRate, '2', '.', ',') . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Vat</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $vat . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Remark</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Remark . '</td>
                                     </tr>
                                </table></td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" style="width:100%; margin-top: 5px; ">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="25%">Inventory</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Qty</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Price</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc (%)</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            $total = 0;
            $totalVAT = 0;
            if (count($detail) > 0) {
                foreach ($detail as $data) {
                    $inventory = Inventory::find($data->InventoryInternalID);
                    $inv = $inventory->InventoryID . ' ' . $inventory->InventoryName;
                    $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $inv . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
                    $totalVAT += $data->VAT;
                    $total += $data->SubTotal;
                }
                if ($totalVAT != 0) {
                    $totalVAT = $totalVAT - ($header->DiscountGlobal * 0.1);
                }
            } else {
                $html.= '<tr>
                            <td colspan="6" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this salesReturn order.</td>
                        </tr>';
            }
            $html.= '</tbody>
                            </table>
                            <div style="box-sizing: border-box;min-width: 200px; margin-left: 320px; display: inline-block; clear: both;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Discount</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Grand Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total - $header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Tax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($totalVAT, '2', '.', ',') . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; ">Grand Total (Tax)</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">'
                    . number_format($header->GrandTotal, '2', '.', ',') . '</td>
                                 </tr>
                                </table>
                            </div>
                    </div>
                </body>
            </html>';
            return PDF ::load($html, 'A5', 'portrait')->show();
        } else {
            $messages = 'accessDenied';

            Session::flash('messages', $messages);
            return Redirect ::Route('showSalesReturn');
        }
    }

    public function summarySalesReturn() {
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $totalSR = 0;
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Sales Return Summary Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span>';
        $hitung = 0;
        foreach (Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $dataCustomer) {
            if (SalesReturnHeader ::where('CompanyInternalID', Auth::user()->Company->InternalID)
                            ->where('ACC6InternalID', $dataCustomer->InternalID)
                            ->whereBetween('SalesReturnDate', Array($start, $end))->count() > 0) {
                $html.= '<table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=7>' . $dataCustomer->ACC6Name . '</th>
                                        </tr>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales Return ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Currency</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Rate</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total(After Discount)</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Vat</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Grand Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                $sumGrandTotal = 0;
                foreach (SalesReturnHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where('ACC6InternalID', $dataCustomer->InternalID)
                        ->whereBetween('SalesReturnDate', Array($start, $end))->get() as $data) {
                    $grandTotal = $data->GrandTotal;
                    $sumGrandTotal += $grandTotal;
                    $totalSR += $grandTotal;
                    $total = $grandTotal;
                    $vat = 0;
                    if ($data->VAT == 1) {
                        $total = $total * 10 / 11;
                        $vat = $total / 10;
                    }
                    $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->SalesReturnID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->SalesReturnDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->Currency->CurrencyName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->CurrencyRate, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($total, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($vat, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($grandTotal, '2', '.', ',') . '</td>
                            </tr>';
                }
                $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="80%" colspan="6">Total : </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . number_format($sumGrandTotal, '2', '.', ',') . '</td>
                        </tr>';
                $html.= '</tbody>
            </table>';
                $hitung++;
            }
        }

        if ($hitung == 0) {
            $html.='<br><span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no sales return.</span>';
        }
        $html.= '<table class="tableBorder" width="100%"  style="margin-top: 10px; clear: both;  top: 50px;">
                    <thead>
                        <tr>
                            <th style="font-family: helvetica,sans-serif;font-size: 14px; margin: 5px !important; font-weight: 700; text-align: right" width="80%" colspan="6">Total Sales Return : </th>
                            <th style="font-family: helvetica,sans-serif;font-size: 14px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . number_format($totalSR, '2', '.', ',') . '</th>
                        </tr>
                    <thead>
                </table>';
        $html.='</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('sales_return_summary');
    }

    public function detailSalesReturn() {
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $dateTamp = '';
        $coa6Tamp = '';
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Sales Return Detail Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span><br><br>';
        if (SalesReturnHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                        ->whereBetween('SalesReturnDate', Array($start, $end))
                        ->orderBy('SalesReturnDate')->orderBy('ACC6InternalID')->count() > 0) {
            foreach (SalesReturnHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                    ->whereBetween('SalesReturnDate', Array($start, $end))
                    ->orderBy('SalesReturnDate')->orderBy('ACC6InternalID')->get() as $dataPenjualan) {
                if ($dateTamp != date("d-M-Y", strtotime($dataPenjualan->SalesReturnDate))) {
                    $html.= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales Return Date : ' . date("d-M-Y", strtotime($dataPenjualan->SalesReturnDate)) . '</span><br><br>';
                    $dateTamp = date("d-M-Y", strtotime($dataPenjualan->SalesReturnDate));
                    $coa6Tamp = '';
                }
                if ($coa6Tamp != $dataPenjualan->ACC6InternalID) {
                    $html.= '<span style="padding-left:23px;font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Customer : ' . $dataPenjualan->coa6->ACC6Name . '</span>';
                    $coa6Tamp = $dataPenjualan->ACC6InternalID;
                }
                $html.= '<table class="tableBorder" style="width:95%; margin-top: 18px; clear: both; position: relative; left: 4%">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=7>' . $dataPenjualan->SalesReturnID . ' | ' . $dataPenjualan->Currency->CurrencyName . ' | Rate : ' . number_format($dataPenjualan->CurrencyRate, '2', '.', ',') . ' </th>
                                        </tr>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory ID</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Name</th>
                                            <th width="10%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Qty</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Price</th>
                                            <th width="10%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc (%)</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                $total = 0;
                $vat = 0;
                foreach ($dataPenjualan->salesReturnDetail as $data) {
                    $total += $data->SubTotal;
                    $vat += $data->VAT;
                    $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->inventory->InventoryID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->inventory->InventoryName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
                }
                if ($vat != 0) {
                    $vat = $vat - ($dataPenjualan->DiscountGlobal * 0.1);
                }
                $html.= '<tr>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=4></td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=2>Total <br> Discount <br> Grand Total <br> VAT <br> Grand Total </td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '<br> '
                        . '' . number_format($dataPenjualan->DiscountGlobal, '2', '.', ',') . '<br>'
                        . '' . number_format($total - $dataPenjualan->DiscountGlobal, '2', '.', ',') . '<br>'
                        . '' . number_format($vat, '2', '.', ',') . '<br>'
                        . '' . number_format($dataPenjualan->GrandTotal, '2', '.', ',') . '</td>
                    </tr>
                </tbody>
            </table>';
            }
        } else {
            $html.= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no sales return.</span><br><br>';
        }
        $html.='</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('sales_return_detail');
    }

    public function getResultSearchSR() {
        $hitung = 0;
        $input = splitSearchValue(Input::get("id"));
        $salesReturnHeader = SalesHeader::join("m_coa6", 'm_coa6.InternalID', '=', 't_sales_header.ACC6InternalID')
                ->where('t_sales_header.CompanyInternalID', '=', Auth::user()->Company->InternalID)
                ->Where(function($query) use($input) {
                    $query->orWhere('SalesID', "like", '%' . $input . "%")
                    ->orWhere("SalesDate", "like", '%' . date("Y-m-d", strtotime(Input::get("id"))) . "%")
                    ->orWhere("ACC6Name", "like", '%' . $input . "%");
                })
                ->OrderBy('SalesDate', 'desc')
                ->select('t_sales_header.*', 'ACC6Name')
                ->take(100)
                ->get();
        if (count($salesReturnHeader) == 0) {
            ?>
            <span>There is no result with keywords <i><u><?php echo Input::get("id"); ?></u></i></span>
            <?php
        } else {
            ?>
            <select class="chosen-select choosen-modal" id="sales" style="" name="sales">
                <?php
                foreach ($salesReturnHeader as $sales) {
                    if (checkSalesReturn($sales->InternalID)) {
                        ?>
                        <option value="<?php echo $sales->SalesID ?>"><?php echo $sales->SalesID . ' | ' . date("d-m-Y", strtotime($sales->SalesDate)) . ' | ' . $sales->ACC6Name ?></option>
                        <?php
                        $hitung = 1;
                    }
                }
                ?>
            </select>
            <script>
                $(document).ready(function () {
                    var hitung = '<?php echo $hitung; ?>';
                    if (hitung == 0) {
                        $('#sales').after('<span>There is no result.</span>');
                        $('#sales').remove();
                    } else {
                        $("#btn-add-sr").removeAttr("disabled");
                    }
                });
            </script>
            <?php
        }
    }

    static function salesReturnDataBackup($data) {
        $explode = explode('---;---', $data);
        $typePayment = $explode[0];
        $typeTax = $explode[1];
        $start = $explode[2];
        $end = $explode[3];
        $where = '';
        if ($typePayment != '-1' && $typePayment != '') {
            $where .= 'isCash = "' . $typePayment . '" ';
        }
        if ($typeTax != '-1' && $typeTax != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'VAT = "' . $typeTax . '" ';
        }
        if ($start != '' && $end != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $start = explode('-', $start);
            $end = explode('-', $end);
            $startSearch = $start[2] . '-' . $start[1] . '-' . $start[0];
            $endSearch = $end[2] . '-' . $end[1] . '-' . $end[0];
            $where .= 'SalesReturnDate between "' . $startSearch . ' 00:00:00" AND "' . $endSearch . ' 23:59:59"';
        }
        $table = 't_salesreturn_header';
        $primaryKey = 't_salesreturn_header`.`InternalID';
        $columns = array(
            array('db' => 'SalesReturnID', 'dt' => 0),
            array('db' => 't_salesreturn_header`.`InternalID', 'dt' => 1, 'formatter' => function( $d, $row ) {
                    $sales = SalesReturnHeader::find($d);
                    if ($sales->isCash == 0) {
                        return 'Cash (-)';
                    } else {
                        return 'Credit ' . $sales->LongTerm;
                    }
                },
                'field' => 't_salesreturn_header`.`InternalID'),
            array(
                'db' => 'SalesReturnDate',
                'dt' => 2,
                'formatter' => function( $d, $row ) {
                    return date("d-m-Y", strtotime($d));
                }
            ),
            array('db' => 'CurrencyName', 'dt' => 3),
            array(
                'db' => 'CurrencyRate',
                'dt' => 4,
                'formatter' => function( $d, $row ) {
                    return number_format($d, '2', '.', ',');
                }
            ),
            array(
                'db' => 'ACC6Name',
                'dt' => 5,
                'formatter' => function( $d, $row ) {
                    return $d;
                }
            ),
            array(
                'db' => 'VAT',
                'dt' => 6,
                'formatter' => function( $d, $row ) {
                    if ($d == 0) {
                        return 'Non Tax';
                    } else {
                        return 'Tax';
                    }
                }
            ),
            array(
                'db' => 'GrandTotal',
                'dt' => 7,
                'formatter' => function( $d, $row ) {
                    return number_format($d, '2', '.', ',');
                }
            ),
            array('db' => 't_salesreturn_header`.`InternalID', 'dt' => 8, 'formatter' => function( $d, $row ) {
                    $data = SalesReturnHeader::find($d);
                    $action = '<td class="text-center">
                                    <a href="' . Route('salesReturnDetail', $data->SalesReturnID) . '">
                                        <button id="btn-' . $data->SalesReturnID . '-detail"
                                                class="btn btn-pure-xs btn-xs btn-detail">
                                            <span class="glyphicon glyphicon-zoom-in"></span>
                                        </button>
                                    </a>';
                    $action.='<a href="' . Route('salesReturnUpdate', $data->SalesReturnID) . '">
                                        <button id="btn-' . $data->SalesReturnID . '-update"
                                                class="btn btn-pure-xs btn-xs btn-edit">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </button>
                                    </a>
                                    <button data-target="#m_salesReturnDelete" data-internal="' . $data->InternalID . '"  data-toggle="modal" role="dialog"
                                           onclick="deleteAttach(this)" data-id="' . $data->SalesReturnID . '" data-name=' . $data->SalesReturnID . ' class="btn btn-pure-xs btn-xs btn-delete">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>';
                    return $action;
                },
                'field' => 't_salesreturn_header`.`InternalID')
        );

        $sql_details = getConnection();

        require('ssp.class.php');
        $ID_CLIENT_VALUE = Auth::user()->CompanyInternalID;
        if ($where != '') {
            $extraCondition = $where . ' AND ' . 't_salesreturn_header.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        } else {
            $extraCondition = 't_salesreturn_header.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        }
        $join = ' INNER JOIN m_currency on m_currency.InternalID = t_salesreturn_header.CurrencyInternalID '
                . 'INNER JOIN m_coa6 on m_coa6.InternalID = t_salesreturn_header.ACC6InternalID';

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $extraCondition, $join));
    }

}
