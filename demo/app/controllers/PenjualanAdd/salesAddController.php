<?php

class SalesAddController extends BaseController {

    public function showSales() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteSales') {
                return $this->deleteSales();
            } else if (Input::get('jenis') == 'summarySales') {
                return $this->summarySales();
            } else if (Input::get('jenis') == 'detailSales') {
                return $this->detailSales();
            } else if (Input::get('jenis') == 'insertSales') {
                return Redirect::Route('salesNew', Input::get('sales'));
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = SalesAddHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
            return View::make('penjualanAdd.salesAddSearch')
                            ->withToogle('transaction')->withAktif('sales')
                            ->withData($data);
        }
        return View::make('penjualanAdd.salesAdd')
                        ->withToogle('transaction')->withAktif('sales');
    }

    public function salesNew($id) {
        $id = SalesOrderHeader::getIdsalesOrder($id);
        $header = SalesOrderHeader::find($id);
        $detail = SalesOrderHeader::find($id)->salesOrderDetail()->get();
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteSales') {
                return $this->deleteSales();
            } else if (Input::get('jenis') == 'summarySales') {
                return $this->summarySales();
            } else if (Input::get('jenis') == 'detailSales') {
                return $this->detailSales();
            } else if (Input::get('jenis') == 'insertSales') {
                return Redirect::Route('salesNew', Input::get('sales'));
            } else {
                return $this->insertSales($id);
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = SalesAddHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
            return View::make('penjualanAdd.salesAddSearch')
                            ->withToogle('transaction')->withAktif('sales')
                            ->withData($data);
        }
        $sales = $this->createID(0) . '.';
        return View::make('penjualanAdd.salesAddNew')
                        ->withToogle('transaction')->withAktif('sales')
                        ->withHeader($header)
                        ->withDetail($detail)
                        ->withSales($sales);
    }

    public function salesDetail($id) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'summarySales') {
                return $this->summarySales();
            } else if (Input::get('jenis') == 'detailSales') {
                return $this->detailSales();
            } else if (Input::get('jenis') == 'insertSales') {
                return Redirect::Route('salesNew', Input::get('sales'));
            }
        }
        $id = SalesAddHeader::getIdsales($id);
        $header = SalesAddHeader::find($id);
        $detail = SalesAddHeader::find($id)->salesDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            return View::make('penjualanAdd.salesAddDetail')
                            ->withToogle('transaction')->withAktif('sales')
                            ->withHeader($header)
                            ->withDetail($detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showSales');
        }
    }

    public function salesUpdate($id) {
        $id = SalesAddHeader::getIdsales($id);
        $header = SalesAddHeader::find($id);
        $detail = SalesAddHeader::find($id)->salesDetail()->get();
        $idSalesOrder = $header->SalesOrderInternalID;
        $headerSalesOrder = SalesOrderHeader::find($idSalesOrder);
        $detailSalesOrder = SalesOrderHeader::find($idSalesOrder)->salesOrderDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID && SalesAddHeader::isReturn($header->SalesID) == false) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                if (Input::get('jenis') == 'summarySales') {
                    return $this->summarySales();
                } else if (Input::get('jenis') == 'detailSales') {
                    return $this->detailSales();
                } else if (Input::get('jenis') == 'insertSales') {
                    return Redirect::Route('salesNew', Input::get('sales'));
                } else {
                    return $this->updateSales($id);
                }
            }
            return View::make('penjualanAdd.salesAddUpdate')
                            ->withToogle('transaction')->withAktif('sales')
                            ->withHeader($header)
                            ->withDetail($detail)
                            ->withHeaderorder($headerSalesOrder)
                            ->withDetailorder($detailSalesOrder);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showSales');
        }
    }

    public function insertSales($id) {
        //rule
        $salesOrderInternalID = Input::get('SalesOrderInternalID');
        $salesOrder = SalesOrderHeader::find($salesOrderInternalID);
        if ($salesOrder->isCash == 0) {
            $rule = array(
                'date' => 'required',
                'remark' => 'required|max:1000',
                'warehouse' => 'required',
                'slip' => 'required',
                'inventory' => 'required'
            );
        } else {
            $rule = array(
                'date' => 'required',
                'remark' => 'required|max:1000',
                'warehouse' => 'required',
                'inventory' => 'required'
            );
            $longTerm = 0;
        }
        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalInsert';
            $error = $validator->messages();
        } else {
            //valid
            //insert header
            $header = new SalesHeader;
            $sales = $this->createID(1) . '.';
            $date = explode('-', Input::get('date'));
            $yearDigit = substr($date[2], 2);
            $sales .= $date[1] . $yearDigit . '.';
            $salesNumber = SalesAddHeader::getNextIDSales($sales);
            $header->SalesID = $salesNumber;
            $header->SalesDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $header->ACC6InternalID = $salesOrder->ACC6InternalID;
            $header->LongTerm = $salesOrder->LongTerm;
            $header->isCash = $salesOrder->isCash;
            $header->WarehouseInternalID = Input::get('warehouse');
            $header->CurrencyInternalID = $salesOrder->CurrencyInternalID;
            $header->CurrencyRate = $salesOrder->CurrencyRate;
            $header->VAT = $salesOrder->VAT;
            $header->DiscountGlobal = str_replace(",", "", Input::get("DiscountGlobal"));
            $header->GrandTotal = Input::get("grandTotalValue");
            $header->DownPayment = str_replace(",", "", Input::get('DownPayment'));
            $header->TransactionType = Input::get('TransactionType');
            if (Input::get('Replacement') == '') {
                $header->Replacement = '0';
            } else {
                $header->Replacement = Input::get('Replacement');
            }
            if (Input::get('TaxNumber') == '') {
                $header->TaxNumber = '.-.';
            } else {
                $header->TaxNumber = Input::get('TaxNumber');
            }
            $header->TaxMonth = Input::get('TaxMonth');
            $header->TaxYear = Input::get('TaxYear');
            $header->TaxDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $header->UserRecord = Auth::user()->UserID;
            $header->CompanyInternalID = Auth::user()->Company->InternalID;
            $header->SalesOrderInternalID = $salesOrderInternalID;
            $header->UserModified = '0';
            $header->Remark = Input::get('remark');
            $header->save();
            //insert detail
            $total = 0;
            for ($a = 0; $a < count(Input::get('inventory')); $a++) {
                $qtyValue = str_replace(',', '', Input::get('qty')[$a]);
                $qtySalesValue = str_replace(',', '', Input::get('qtySales')[$a]);
                $sumSales = SalesAddHeader::getSumSales(Input::get('inventory')[$a], $salesOrderInternalID, Input::get('InternalDetail')[$a]);
                if ($sumSales == '') {
                    $sumSales = '0';
                }
                $sumSalesReturn = SalesReturnHeader::getSumReturnOrder(Input::get('inventory')[$a], $salesOrderInternalID, Input::get('InternalDetail')[$a]);
                if ($sumSalesReturn == '') {
                    $sumSalesReturn = '0';
                }
                $qtyValue = $qtyValue - $sumSales + $sumSalesReturn;
                if ($qtyValue < $qtySalesValue) {
                    $qtySalesValue = $qtyValue;
                }
                $priceValue = Input::get('price')[$a];
                $discValue = Input::get('discountNominal')[$a];
                $subTotal = ($priceValue * $qtySalesValue ) - (($priceValue * $qtySalesValue) * Input::get('discount')[$a] / 100) - $discValue * $qtySalesValue;
                if ($header->VAT == '1') {
                    $vatValue = $subTotal / 10;
                } else {
                    $vatValue = 0;
                }
                if ($qtySalesValue > 0) {
                    $detail = new SalesDetail();
                    $detail->SalesInternalID = $header->InternalID;
                    $detail->InventoryInternalID = Input::get('inventory')[$a];
                    $detail->Qty = $qtySalesValue;
                    $detail->Price = $priceValue;
                    $detail->Discount = Input::get('discount')[$a];
                    $detail->DiscountNominal = $discValue;
                    $detail->VAT = $vatValue;
                    $detail->SubTotal = $subTotal;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = '0';
                    $detail->SalesOrderDetailInternalID = Input::get('InternalDetail')[$a];
                    $detail->save();
                }
                $total += $subTotal;
            }
            if ($header->VAT == '1') {
                $vatValueHeader = ($total - str_replace(",", "", Input::get("DiscountGlobal")) - str_replace(",", "", Input::get('DownPayment'))) / 10;
            } else {
                $vatValueHeader = 0;
            }
            $header->GrandTotal = $total - str_replace(",", "", Input::get("DiscountGlobal")) - str_replace(",", "", Input::get('DownPayment')) + $vatValueHeader;
            $header->save();

            $currency = $salesOrder->CurrencyInternalID;
            $rate = $salesOrder->CurrencyRate;
            $total -= str_replace(",", "", Input::get('DiscountGlobal'));
            $total -= str_replace(",", "", Input::get('DownPayment'));
            if ($header->isCash == 0) {
                $slip = Input::get('slip');
                $this->insertJournal($salesNumber, $total, $salesOrder->VAT, $currency, $rate, $date, $slip, str_replace(",", "", Input::get('DownPayment')));
            } else {
                $slip = '-1';
                $this->insertJournal($salesNumber, $total, $salesOrder->VAT, $currency, $rate, $date, $slip, str_replace(",", "", Input::get('DownPayment')));
            }
            $messages = 'suksesInsert';
            $error = '';
        }
        $sales = $this->createID(0) . '.';
        $header = SalesOrderHeader::find($id);
        $detail = SalesOrderHeader::find($id)->salesOrderDetail()->get();
        return View::make('penjualanAdd.salesAddNew')
                        ->withToogle('transaction')->withAktif('sales')
                        ->withSales($sales)
                        ->withError($error)
                        ->withHeader($header)
                        ->withDetail($detail)
                        ->withMessages($messages);
    }

    public function updateSales($id) {
        //tipe
        $headerUpdate = SalesAddHeader::find($id);
        $detailUpdate = SalesAddHeader::find($id)->salesDetail()->get();

        //rule
        if ($headerUpdate->isCash == 0) {
            $rule = array(
                'remark' => 'required|max:1000',
                'warehouse' => 'required',
                'slip' => 'required',
                'inventory' => 'required'
            );
        } else {
            $rule = array(
                'remark' => 'required|max:1000',
                'warehouse' => 'required',
                'inventory' => 'required'
            );
            $longTerm = 0;
        }

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalUpdate';
            $error = $validator->messages();
        } else {
            $header = SalesAddHeader::find(Input::get('SalesInternalID'));
            $header->WarehouseInternalID = Input::get('warehouse');
            $header->DiscountGlobal = str_replace(",", "", Input::get("DiscountGlobal"));
            $header->GrandTotal = Input::get("grandTotalValue");
            $header->DownPayment = str_replace(",", "", Input::get('DownPayment'));
            $header->TransactionType = Input::get('TransactionType');
            if (Input::get('Replacement') == '') {
                $header->Replacement = '0';
            } else {
                $header->Replacement = Input::get('Replacement');
            }
            if (Input::get('TaxNumber') == '') {
                $header->TaxNumber = '.-.';
            } else {
                $header->TaxNumber = Input::get('TaxNumber');
            }
            $header->TaxMonth = Input::get('TaxMonth');
            $header->TaxYear = Input::get('TaxYear');
            $header->UserModified = Auth::user()->UserID;
            $header->Remark = Input::get('remark');
            $header->save();

            //delete sales detail -- nantinya insert ulang
            SalesAddDetail::where('SalesInternalID', '=', Input::get('SalesInternalID'))->delete();

            //insert detail
            $total = 0;
            for ($a = 0; $a < count(Input::get('inventory')); $a++) {
                $qtyValue = str_replace(',', '', Input::get('qty')[$a]);
                $qtySalesValue = str_replace(',', '', Input::get('qtySales')[$a]);
                $sumSales = SalesAddHeader::getSumSalesExcept(Input::get('inventory')[$a], $header->InternalID, $header->SalesOrderInternalID, Input::get('InternalDetail')[$a]);
                if ($sumSales == '') {
                    $sumSales = '0';
                }
                $sumSalesReturn = SalesReturnHeader::getSumReturnOrder(Input::get('inventory')[$a], $header->SalesOrderInternalID, Input::get('InternalDetail')[$a]);
                if ($sumSalesReturn == '') {
                    $sumSalesReturn = '0';
                }
                $qtyValue = $qtyValue - $sumSales + $sumSalesReturn;
                if ($qtyValue < $qtySalesValue) {
                    $qtySalesValue = $qtyValue;
                }
                $priceValue = Input::get('price')[$a];
                $discValue = Input::get('discountNominal')[$a];
                $subTotal = ($priceValue * $qtySalesValue ) - (($priceValue * $qtySalesValue) * Input::get('discount')[$a] / 100) - $discValue * $qtySalesValue;
                if ($headerUpdate->VAT == '1') {
                    $vatValue = $subTotal / 10;
                } else {
                    $vatValue = 0;
                }
                if ($qtySalesValue > 0) {
                    $detail = new SalesDetail();
                    $detail->SalesInternalID = Input::get('SalesInternalID');
                    $detail->InventoryInternalID = Input::get('inventory')[$a];
                    $detail->Qty = $qtySalesValue;
                    $detail->Price = $priceValue;
                    $detail->Discount = Input::get('discount')[$a];
                    $detail->DiscountNominal = $discValue;
                    $detail->VAT = $vatValue;
                    $detail->SubTotal = $subTotal;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = Auth::user()->UserID;
                    $detail->SalesOrderDetailInternalID = Input::get('InternalDetail')[$a];
                    $detail->save();
                }
                $total += $subTotal;
            }
            if ($header->VAT == '1') {
                $vatValueHeader = ($total - str_replace(",", "", Input::get("DiscountGlobal")) - str_replace(",", "", Input::get('DownPayment'))) / 10;
            } else {
                $vatValueHeader = 0;
            }
            $header->GrandTotal = $total - str_replace(",", "", Input::get("DiscountGlobal")) - str_replace(",", "", Input::get('DownPayment')) + $vatValueHeader;
            $header->save();

            $journal = JournalHeader::where('TransactionID', '=', $headerUpdate->SalesID)->get();
            foreach ($journal as $value) {
                JournalDetail::where('JournalInternalID', '=', $value->InternalID)->delete();
                JournalHeader::where('InternalID', '=', $value->InternalID)->delete();
            }

            $currency = $headerUpdate->CurrencyInternalID;
            $rate = $headerUpdate->CurrencyRate;
            $date = date("d-m-Y", strtotime($headerUpdate->SalesDate));
            $date = explode('-', $date);
            $total -= str_replace(",", "", Input::get('DiscountGlobal'));
            $total -= str_replace(",", "", Input::get('DownPayment'));
            if ($headerUpdate->isCash == 0) {
                $slip = Input::get('slip');
                $this->insertJournal($headerUpdate->SalesID, $total, $headerUpdate->VAT, $currency, $rate, $date, $slip, str_replace(",", "", Input::get('DownPayment')));
            } else {
                $slip = '-1';
                $this->insertJournal($headerUpdate->SalesID, $total, $headerUpdate->VAT, $currency, $rate, $date, $slip, str_replace(",", "", Input::get('DownPayment')));
            }

            $messages = 'suksesUpdate';
            $error = '';
        }

        //tipe
        $header = SalesAddHeader::find($id);
        $detail = SalesAddHeader::find($id)->salesDetail()->get();
        $idSalesOrder = $header->SalesOrderInternalID;
        $headerSalesOrder = SalesOrderHeader::find($idSalesOrder);
        $detailSalesOrder = SalesOrderHeader::find($idSalesOrder)->salesOrderDetail()->get();
        return View::make('penjualanAdd.salesAddUpdate')
                        ->withToogle('transaction')->withAktif('sales')
                        ->withHeader($header)
                        ->withDetail($detail)
                        ->withHeaderorder($headerSalesOrder)
                        ->withDetailorder($detailSalesOrder)
                        ->withError($error)
                        ->withMessages($messages);
    }

    public function deleteSales() {
        $salesHeader = SalesAddHeader::find(Input::get('InternalID'));
        $sales = DB::select(DB::raw('SELECT * FROM t_salesreturn_header WHERE CompanyInternalID = "' . Auth::user()->CompanyInternalID . '" AND SalesReturnID LIKE "%' . $salesHeader->SalesID . '"'));
        if (is_null($sales) == '') {
            //tidak ada yang menggunakan data sales maka data boleh dihapus
            //hapus journal
            if ($salesHeader->CompanyInternalID == Auth::user()->Company->InternalID) {
                $journal = JournalHeader::where('TransactionID', '=', $salesHeader->SalesID)->get();
                foreach ($journal as $value) {
                    JournalDetail::where('JournalInternalID', '=', $value->InternalID)->delete();
                    JournalHeader::where('InternalID', '=', $value->InternalID)->delete();
                }
                //hapus detil
                $detilData = SalesAddHeader::find(Input::get('InternalID'))->salesDetail;
                foreach ($detilData as $value) {
                    $detil = SalesAddDetail::find($value->InternalID);
                    $detil->delete();
                }
                //hapus sales
                $sales = SalesAddHeader::find(Input::get('InternalID'));
                $sales->delete();
                $messages = 'suksesDelete';
            } else {
                $messages = 'accessDenied';
            }
        } else {
            //ada maka data tidak boleh dihapus
            $messages = 'gagalDelete';
        }
        $data = SalesAddHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
        return View::make('penjualanAdd.salesAddSearch')
                        ->withToogle('transaction')->withAktif('sales')
                        ->withMessages($messages)
                        ->withData($data);
    }

    function insertJournal($salesNumber, $total, $vat, $currency, $rate, $date, $slip, $dp) {
        $header = new JournalHeader;
        $yearDigit = substr($date[2], 2);
        $dateText = $date[1] . $yearDigit;
        $defaultPenjualan = Default_s::find(1)->DefaultID;
        $defaultPiutang = Default_s::find(2)->DefaultID;
        $defaultIncome = Default_s::find(3)->DefaultID;
        $defaultDP = Default_s::find(11)->DefaultID;
        if ($slip == '-1') {
            $cari = 'ME-' . $dateText;
            $header->JournalType = 'Memorial';
            $akun = array($defaultPenjualan, $defaultPiutang, $defaultIncome, $defaultDP);
            $header->JournalID = JournalHeader::getNextIDJournal($cari . '-');
            $header->SlipInternalID = Null;
        } else {
            $akun = array($defaultPenjualan, "Slip", $defaultIncome, $defaultDP);
            $tampSlipID = Slip::find($slip);
            if ($tampSlipID->Flag == '0') {
                $cari = 'CI-' . $dateText;
                $header->JournalType = 'Cash In';
            } else if ($tampSlipID->Flag == '1') {
                $cari = 'BI-' . $dateText;
                $header->JournalType = 'Bank In';
            }
            $header->JournalID = JournalHeader::getNextIDJournal($cari . '-' . $tampSlipID->SlipID . '-');
            $header->SlipInternalID = $slip;
        }
        $header->JournalDate = $date[2] . '-' . $date[1] . '-' . $date[0];
        $header->JournalFrom = Auth::user()->UserID;
        $header->Notes = '';
        $department = Department::where('Default', '1')->first();
        $header->DepartmentInternalID = $department->InternalID;
        $header->TransactionID = $salesNumber;
        $header->ACC5InternalID = '0';
        $header->Lock = '0';
        $header->Check = '0';
        $header->Flag = '0';
        $header->UserRecord = Auth::user()->UserID;
        $header->CompanyInternalID = Auth::user()->Company->InternalID;
        $header->UserModified = '0';
        $header->Remark = '';
        $header->save();

        //insert detail
        if ($vat == 1) {
            $vatValue = 10 * $total / 100;
        } else {
            $vatValue = 0;
        }
        $count = 1;
        foreach ($akun as $data) {
            $kreditValue = 0;
            $debetValue = 0;
            if (($data != $defaultIncome || $vatValue != 0) && ($data != $defaultDP || $dp != 0)) {
                $detail = new JournalDetail();
                $detail->JournalInternalID = $header->InternalID;
                $detail->JournalIndex = $count;
                $detail->JournalNotes = $data;
                if ($data == $defaultPenjualan) {
                    $kreditValue = $total + $dp;
                    $detail->JournalDebetMU = 0;
                    $detail->JournalCreditMU = $kreditValue;
                } else if ($data == $defaultPiutang || $data == 'Slip') {
                    $debetValue = $total + $vatValue;
                    $detail->JournalDebetMU = $debetValue;
                    $detail->JournalCreditMU = 0;
                } else if ($data == $defaultDP) {
                    $debetValue = $dp;
                    $detail->JournalDebetMU = $debetValue;
                    $detail->JournalCreditMU = 0;
                } else {
                    $kreditValue = $vatValue;
                    $detail->JournalDebetMU = 0;
                    $detail->JournalCreditMU = $kreditValue;
                }
                $detail->CurrencyInternalID = $currency;
                $detail->CurrencyRate = $rate;
                $detail->JournalDebet = $debetValue * $rate;
                $detail->JournalCredit = $kreditValue * $rate;
                $detail->JournalTransactionID = NULL;
                if ($data != 'Slip') {
                    $default = Default_s::getInternalCoa($data);
                } else {
                    $default = Slip::getInternalCoa($slip);
                }
                $detail->ACC1InternalID = $default->ACC1InternalID;
                $detail->ACC2InternalID = $default->ACC2InternalID;
                $detail->ACC3InternalID = $default->ACC3InternalID;
                $detail->ACC4InternalID = $default->ACC4InternalID;
                $detail->ACC5InternalID = $default->ACC5InternalID;
                $detail->ACC6InternalID = $default->ACC6InternalID;
                $coa = Coa::getInternalID($default->ACC1InternalID, $default->ACC2InternalID, $default->ACC3InternalID, $default->ACC4InternalID, $default->ACC5InternalID, $default->ACC6InternalID);
                $detail->COAName = Coa::find($coa)->COAName;
                $detail->UserRecord = Auth::user()->UserID;
                $detail->UserModified = '0';
                $detail->save();
                $count++;
            }
        }
    }

    function salesPrint($id) {
        $id = SalesAddHeader::getIdsales($id);
        $header = SalesAddHeader::find($id);
        $detail = SalesAddHeader::find($id)->salesDetail()->get();
        $headerOrder = SalesOrderHeader::find($header->SalesOrderInternalID);
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $coa6 = SalesAddHeader::find($header->InternalID)->coa6;
            $customer = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . ', ' . $coa6->Address . '<br>Phone: ' . $coa6->Phone . ', Fax: ' . $coa6->Fax;
            if ($header->isCash == 0) {
                $payment = 'Cash';
            } else {
                $payment = 'Credit';
            }
            $currency = Currency::find($header->CurrencyInternalID);
            $currencyName = $currency->CurrencyName;
            if ($header->VAT == 0) {
                $vat = 'Non Tax';
            } else {
                $vat = 'Tax';
            }
            $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 6px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 195px;border-bottom:1px solid #ddd;margin-bottom:20px;">
                        <div style="height: 133px">
                        <img height="100%" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                        </div>
                        <div style="width: 420px; float: left;">
                            <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->Company->CompanyName . '</b>
                        </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Address</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Sales</h5>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                            <table>
                            <tr>
                            <td width="275px">
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Sales ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->SalesID . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->SalesDate)) . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Customer</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $customer . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Payment</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $payment . '</td>
                                 </tr>';
            if ($header->isCash != 0) {
                $html.='<tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Due Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime("+" . $header->LongTerm . " day", strtotime($header->SalesDate))) . '</td>
                                 </tr>';
            }
            $html.='
                                </table>
                                </td>
                                <td>
                                <table>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Warehouse</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Warehouse->WarehouseName . '</td>
                                     </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Sales Order</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $headerOrder->SalesOrderID . '</td>
                                     </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Currency</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $currencyName . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Rate</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . number_format($header->CurrencyRate, '2', '.', ',') . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Vat</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $vat . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Remark</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Remark . '</td>
                                     </tr>
                                </table></td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="25%">Inventory</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Qty</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Price</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc (%)</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            $total = 0;
            $totalVAT = 0;
            if (count($detail) > 0) {
                foreach ($detail as $data) {
                    $inventory = Inventory::find($data->InventoryInternalID);
                    $inv = $inventory->InventoryID . ' ' . $inventory->InventoryName;
                    $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $inv . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
                    $totalVAT += $data->VAT;
                    $total += $data->SubTotal;
                }
                if ($totalVAT != 0) {
                    $totalVAT = $totalVAT - ($header->DiscountGlobal * 0.1);
                }
            } else {
                $html.= '<tr>
                            <td colspan="6" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this sales.</td>
                        </tr>';
            }
            $html.= '</tbody>
                            </table>
                            <div style="box-sizing: border-box;min-width: 200px; margin-left: 320px; display: inline-block; clear: both;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Discount</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Grand Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total - $header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Tax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($totalVAT, '2', '.', ',') . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; ">Grand Total (Tax)</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">'
                    . number_format($header->GrandTotal, '2', '.', ',') . '</td>
                                 </tr>
                                </table>
                            </div>
                    </div>
                </body>
            </html>';
            return PDF ::load($html, 'A5', 'portrait')->show();
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showSales');
        }
    }

    function salesPrintSJ($id) {
        $id = SalesAddHeader::getIdsales($id);
        $header = SalesAddHeader::find($id);
        $detail = SalesAddHeader::find($id)->salesDetail()->get();
        $headerOrder = SalesOrderHeader::find($header->SalesOrderInternalID);
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $coa6 = SalesAddHeader::find($header->InternalID)->coa6;
            $customer = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . ', ' . $coa6->Address . '<br>Phone: ' . $coa6->Phone . ', Fax: ' . $coa6->Fax;
            if ($header->isCash == 0) {
                $payment = 'Cash';
            } else {
                $payment = 'Credit';
            }
            $currency = Currency::find($header->CurrencyInternalID);
            $currencyName = $currency->CurrencyName;
            if ($header->VAT == 0) {
                $vat = 'Non Tax';
            } else {
                $vat = 'Tax';
            }
            $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 6px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 195px;border-bottom:1px solid #ddd;margin-bottom:20px;">
                        <div style="height: 133px">
                        <img height="100%" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                        </div>
                        <div style="width: 420px; float: left;">
                            <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->Company->CompanyName . '</b>
                        </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Address</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Sales</h5>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                            <table>
                            <tr>
                            <td width="275px">
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Sales ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->SalesID . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->SalesDate)) . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Customer</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $customer . '</td>
                                 </tr>
                                </table>
                                </td>
                                <td>
                                <table>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Warehouse</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Warehouse->WarehouseName . '</td>
                                     </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Sales Order</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $headerOrder->SalesOrderID . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Remark</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Remark . '</td>
                                     </tr>
                                </table></td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="65%">Inventory</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="25%">Qty</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            $total = 0;
            $totalVAT = 0;
            if (count($detail) > 0) {
                foreach ($detail as $data) {
                    $inventory = Inventory::find($data->InventoryInternalID);
                    $inv = $inventory->InventoryID . ' ' . $inventory->InventoryName;
                    $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $inv . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                            </tr>';
                    $totalVAT += $data->VAT;
                    $total += $data->SubTotal;
                }
                if ($totalVAT != 0) {
                    $totalVAT = $totalVAT - ($header->DiscountGlobal * 0.1);
                }
            } else {
                $html.= '<tr>
                            <td colspan="2" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this sales.</td>
                        </tr>';
            }
            $html.= '</tbody>
                            </table>
                            <table width="100%" style="margin-top: 10px;">
                            <tr>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; padding-left: 50px; font-weight: 500;" width="60%">
                            Tanda Terima.
                            </td>
                            <td style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; padding-left: 50px; font-weight: 500;" width="40%">
                            Hormat Kami.
                            </td>
                            </tr>
                            </table>
                    </div>
                </body>
            </html>';
            return PDF ::load($html, 'A5', 'portrait')->show();
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showSales');
        }
    }

    function createID($tipe) {
        $sales = 'SI';
        if ($tipe == 0) {
            $sales .= '.' . date('m') . date('y');
        }
        return $sales;
    }

    public function formatCariIDSales() {
        $date = explode('-', Input::get('date'));
        $id = $this->createID(1);
        $yearDigit = substr($date[2], 2);
        $id .= '.' . $date[1] . $yearDigit . '.';
        echo SalesAddHeader::getNextIDSales($id);
    }

    function salesCSV($id) {
        $id = SalesHeader::getIdsales($id);
        $header = SalesHeader::find($id);
        Session::flash('idCSV', $id);
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            Excel::create('Tax_Report_sales', function($excel) {
                $excel->sheet('Tax_Report_sales', function($sheet) {
                    //baris pertama
                    $sheet->setCellValueByColumnAndRow(0, 1, "FK");
                    $sheet->setCellValueByColumnAndRow(1, 1, "KD_JENIS_TRANSAKSI");
                    $sheet->setCellValueByColumnAndRow(2, 1, "FG_PENGGANTI");
                    $sheet->setCellValueByColumnAndRow(3, 1, "NOMOR_FAKTUR");
                    $sheet->setCellValueByColumnAndRow(4, 1, "MASA_PAJAK");
                    $sheet->setCellValueByColumnAndRow(5, 1, "TAHUN_PAJAK");
                    $sheet->setCellValueByColumnAndRow(6, 1, "TANGGAL_FAKTUR");
                    $sheet->setCellValueByColumnAndRow(7, 1, "NPWP");
                    $sheet->setCellValueByColumnAndRow(8, 1, "NAMA");
                    $sheet->setCellValueByColumnAndRow(9, 1, "ALAMAT_LENGKAP");
                    $sheet->setCellValueByColumnAndRow(10, 1, "JUMLAH_DPP");
                    $sheet->setCellValueByColumnAndRow(11, 1, "JUMLAH_PPN");
                    $sheet->setCellValueByColumnAndRow(12, 1, "JUMLAH_PPNBM");
                    $sheet->setCellValueByColumnAndRow(13, 1, "ID_KETERANGAN_TAMBAHAN");
                    $sheet->setCellValueByColumnAndRow(14, 1, "FG_UANG_MUKA");
                    $sheet->setCellValueByColumnAndRow(15, 1, "UANG_MUKA_DPP");
                    $sheet->setCellValueByColumnAndRow(16, 1, "UANG_MUKA_PPN");
                    $sheet->setCellValueByColumnAndRow(17, 1, "UANG_MUKA_PPNBM");
                    $sheet->setCellValueByColumnAndRow(18, 1, "REFERENSI");
                    //baris kedua
                    $sheet->setCellValueByColumnAndRow(0, 2, "LT");
                    $sheet->setCellValueByColumnAndRow(1, 2, "NPWP");
                    $sheet->setCellValueByColumnAndRow(2, 2, "NAMA");
                    $sheet->setCellValueByColumnAndRow(3, 2, "JALAN");
                    $sheet->setCellValueByColumnAndRow(4, 2, "BLOK");
                    $sheet->setCellValueByColumnAndRow(5, 2, "NOMOR");
                    $sheet->setCellValueByColumnAndRow(6, 2, "RT");
                    $sheet->setCellValueByColumnAndRow(7, 2, "RW");
                    $sheet->setCellValueByColumnAndRow(8, 2, "KECAMATAN");
                    $sheet->setCellValueByColumnAndRow(9, 2, "KELURAHAN");
                    $sheet->setCellValueByColumnAndRow(10, 2, "KABUPATEN");
                    $sheet->setCellValueByColumnAndRow(11, 2, "PROPINSI");
                    $sheet->setCellValueByColumnAndRow(12, 2, "KODE_POS");
                    $sheet->setCellValueByColumnAndRow(13, 2, "NOMOR_TELEPON");
                    //baris ketiga
                    $sheet->setCellValueByColumnAndRow(0, 3, "OF");
                    $sheet->setCellValueByColumnAndRow(1, 3, "KODE_OBJEK");
                    $sheet->setCellValueByColumnAndRow(2, 3, "NAMA");
                    $sheet->setCellValueByColumnAndRow(3, 3, "HARGA_SATUAN");
                    $sheet->setCellValueByColumnAndRow(4, 3, "JUMLAH_BARANG");
                    $sheet->setCellValueByColumnAndRow(5, 3, "HARGA_TOTAL");
                    $sheet->setCellValueByColumnAndRow(6, 3, "DISKON");
                    $sheet->setCellValueByColumnAndRow(7, 3, "DPP");
                    $sheet->setCellValueByColumnAndRow(8, 3, "PPN");
                    $sheet->setCellValueByColumnAndRow(9, 3, "TARIF_PPNBM");
                    $sheet->setCellValueByColumnAndRow(10, 3, "PPNBM");
                    //isi baris pertama
                    $row = 4;
                    $header = SalesHeader::find(Session::get('idCSV'));
                    $sheet->setCellValueByColumnAndRow(0, $row, "FK");
                    $sheet->setCellValueByColumnAndRow(1, $row, '0' . $header->TransactionType);
                    $sheet->setCellValueByColumnAndRow(2, $row, $header->Replacement);
                    //nomorPajak
                    $nomorPajak = str_replace('.', '', $header->TaxNumber);
                    $nomorPajak = str_replace('-', '', $nomorPajak);
                    $nomorPajak = substr($nomorPajak, 4);
                    if ($nomorPajak == '' || $nomorPajak == 0) {
                        $nomorPajak = "'000000000000000";
                    }
                    $sheet->setCellValueByColumnAndRow(3, $row, $nomorPajak);
                    $sheet->setCellValueByColumnAndRow(4, $row, $header->TaxMonth);
                    $sheet->setCellValueByColumnAndRow(5, $row, $header->TaxYear);
                    $sheet->setCellValueByColumnAndRow(6, $row, date('d/m/Y', strtotime($header->TaxDate)));

                    //nomorPajak
                    $pajakCustomer = str_replace('.', '', $header->Coa6->TaxID);
                    $pajakCustomer = str_replace('-', '', $pajakCustomer);
                    if ($pajakCustomer == '' || $pajakCustomer == 0) {
                        $pajakCustomer = "'000000000000000";
                    }

                    $sheet->setCellValueByColumnAndRow(7, $row, $pajakCustomer);
                    $sheet->setCellValueByColumnAndRow(8, $row, $header->Coa6->ACC6Name);
                    $sheet->setCellValueByColumnAndRow(9, $row, $header->Coa6->Address);
                    //total
                    $total = $header->GrandTotal * 10 / 11;
                    $sheet->setCellValueByColumnAndRow(10, $row, floor($total));
                    $sheet->setCellValueByColumnAndRow(11, $row, floor(($total * 0.1)));
                    $sheet->setCellValueByColumnAndRow(12, $row, 0);
                    $sheet->setCellValueByColumnAndRow(13, $row, '');
                    if ($header->DownPayment == 0) {
                        $dp = 0;
                    } else {
                        $dp = 1;
                    }
                    $sheet->setCellValueByColumnAndRow(14, $row, $dp);
                    $sheet->setCellValueByColumnAndRow(15, $row, $header->DownPayment);
                    $sheet->setCellValueByColumnAndRow(16, $row, $header->DownPayment * 0.1);
                    $sheet->setCellValueByColumnAndRow(17, $row, "0");
                    $sheet->setCellValueByColumnAndRow(18, $row, $header->SalesID);

                    $row++;
                    $detail = $header->salesDetail()->get();
                    foreach ($detail as $data) {
                        $sheet->setCellValueByColumnAndRow(0, $row, "OF");
                        $sheet->setCellValueByColumnAndRow(1, $row, $data->Inventory->InventoryID);
                        $sheet->setCellValueByColumnAndRow(2, $row, $data->Inventory->InventoryName);
                        $sheet->setCellValueByColumnAndRow(3, $row, $data->Price);
                        $sheet->setCellValueByColumnAndRow(4, $row, $data->Qty);
                        $sheet->setCellValueByColumnAndRow(5, $row, $data->Qty * $data->Price);
                        $sheet->setCellValueByColumnAndRow(6, $row, $data->Qty * $data->Price - $data->SubTotal);
                        $sheet->setCellValueByColumnAndRow(7, $row, $data->SubTotal);
                        $sheet->setCellValueByColumnAndRow(8, $row, $data->VAT);
                        $sheet->setCellValueByColumnAndRow(9, $row, 0);
                        $sheet->setCellValueByColumnAndRow(10, $row, 0);
                        $row++;
                    }
                });
            })->export('csv');
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showSales');
        }
    }

    public function summarySales() {
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $totalSA = 0;
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Sales Summary Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span>';
        $hitung = 0;
        foreach (Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $dataCustomer) {
            if (SalesAddHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                            ->where('ACC6InternalID', $dataCustomer->InternalID)
                            ->whereBetween('SalesDate', Array($start, $end))->count() > 0) {
            $html.= '<table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=7>' . $dataCustomer->ACC6Name . '</th>
                                        </tr>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Currency</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Rate</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total(After Discount)</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Vat</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Grand Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                $sumGrandTotal = 0;
                foreach (SalesAddHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where('ACC6InternalID', $dataCustomer->InternalID)
                        ->whereBetween('SalesDate', Array($start, $end))->get() as $data) {
                    $grandTotal = $data->GrandTotal;
                    $sumGrandTotal += $grandTotal;
                    $totalSA += $grandTotal;
                    $total = $grandTotal;
                    $vat = 0;
                    if ($data->VAT == 1) {
                        $total = $total * 10 / 11;
                        $vat = $total / 10;
                    }
                    $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->SalesID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->SalesDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->Currency->CurrencyName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->CurrencyRate, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($total, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($vat, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($grandTotal, '2', '.', ',') . '</td>
                            </tr>';
                }
                $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="80%" colspan="6">Total : </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . number_format($sumGrandTotal, '2', '.', ',') . '</td>
                        </tr>';

            $html.= '</tbody>
            </table>';
                $hitung++;
            }
        }
        
        if ($hitung == 0) {
            $html.='<br><span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no sales.</span>';
        }
        $html.= '<table class="tableBorder" width="100%"  style="margin-top: 10px; clear: both;  top: 50px;">
                    <thead>
                        <tr>
                            <th style="font-family: helvetica,sans-serif;font-size: 14px; margin: 5px !important; font-weight: 700; text-align: right" width="80%" colspan="6">Total Sales : </th>
                            <th style="font-family: helvetica,sans-serif;font-size: 14px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . number_format($totalSA, '2', '.', ',') . '</th>
                        </tr>
                    <thead>
                </table>';
        $html.='</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('sales_summary');
    }

    public function detailSales() {
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $dateTamp = '';
        $coa6Tamp = '';
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Sales Detail Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span><br><br>';
        if (SalesAddHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                        ->whereBetween('SalesDate', Array($start, $end))
                        ->orderBy('SalesDate')->orderBy('ACC6InternalID')->count() > 0) {
            foreach (SalesAddHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                    ->whereBetween('SalesDate', Array($start, $end))
                    ->orderBy('SalesDate')->orderBy('ACC6InternalID')->get() as $dataPenjualan) {
                if ($dateTamp != date("d-M-Y", strtotime($dataPenjualan->SalesDate))) {
                    $html.= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales Date : ' . date("d-M-Y", strtotime($dataPenjualan->SalesDate)) . '</span><br><br>';
                    $dateTamp = date("d-M-Y", strtotime($dataPenjualan->SalesDate));
                    $coa6Tamp = '';
                }
                if ($coa6Tamp != $dataPenjualan->ACC6InternalID) {
                    $html.= '<span style="padding-left:23px;font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Customer : ' . $dataPenjualan->coa6->ACC6Name . '</span>';
                    $coa6Tamp = $dataPenjualan->ACC6InternalID;
                }
                $html.= '<table class="tableBorder" style="width:95%; margin-top: 18px; clear: both; position: relative; left: 4%">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=6>' . $dataPenjualan->SalesID . ' | ' . $dataPenjualan->Currency->CurrencyName . ' | Rate : ' . number_format($dataPenjualan->CurrencyRate, '2', '.', ',') . ' </th>
                                        </tr>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory ID</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Name</th>
                                            <th width="10%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Qty</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Price</th>
                                            <th width="10%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc (%)</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                $total = 0;
                $vat = 0;
                foreach ($dataPenjualan->salesDetail as $data) {
                    $total += $data->SubTotal;
                    $vat += $data->VAT;
                    $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->inventory->InventoryID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->inventory->InventoryName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
                }
                if ($vat != 0) {
                    $vat = $vat - ($dataPenjualan->DiscountGlobal * 0.1);
                }
                $html.= '<tr>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=4></td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=2>Total <br> Discount <br> Grand Total <br> VAT <br> Grand Total </td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '<br>'
                        . '' . number_format($dataPenjualan->DiscountGlobal, '2', '.', ',') . '<br>'
                        . '' . number_format($total - $dataPenjualan->DiscountGlobal, '2', '.', ',') . '<br>'
                        . '' . number_format($vat, '2', '.', ',') . '<br>'
                        . '' . number_format($dataPenjualan->GrandTotal, '2', '.', ',') . '</td>
                    </tr>
                </tbody>
            </table>';
            }
        } else {
            $html.= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no sales.</span><br><br>';
        }
        $html.='</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('sales_detail');
    }

    static function salesDataBackup($data) {
        $explode = explode('---;---', $data);
        $typePayment = $explode[0];
        $typeTax = $explode[1];
        $start = $explode[2];
        $end = $explode[3];
        $where = '';
        if ($typePayment != '-1' && $typePayment != '') {
            $where .= 'isCash = "' . $typePayment . '" ';
        }
        if ($typeTax != '-1' && $typeTax != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'VAT = "' . $typeTax . '" ';
        }
        if ($start != '' && $end != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $start = explode('-', $start);
            $end = explode('-', $end);
            $startSearch = $start[2] . '-' . $start[1] . '-' . $start[0];
            $endSearch = $end[2] . '-' . $end[1] . '-' . $end[0];
            $where .= 'SalesDate between "' . $startSearch . ' 00:00:00" AND "' . $endSearch . ' 23:59:59"';
        }
        $table = 't_sales_header';
        $primaryKey = 't_sales_header`.`InternalID';
        $columns = array(
            array('db' => 'SalesID', 'dt' => 0),
            array('db' => 't_sales_header`.`InternalID', 'dt' => 1, 'formatter' => function( $d, $row ) {
                    $sales = SalesHeader::find($d);
                    if ($sales->isCash == 0) {
                        return 'Cash (-)';
                    } else {
                        return 'Credit ' . $sales->LongTerm;
                    }
                },
                'field' => 't_sales_header`.`InternalID'),
            array(
                'db' => 'SalesDate',
                'dt' => 2,
                'formatter' => function( $d, $row ) {
                    return date("d-m-Y", strtotime($d));
                }
            ),
            array('db' => 'CurrencyName', 'dt' => 3),
            array(
                'db' => 'CurrencyRate',
                'dt' => 4,
                'formatter' => function( $d, $row ) {
                    return number_format($d, '2', '.', ',');
                }
            ),
            array(
                'db' => 'ACC6Name',
                'dt' => 5,
                'formatter' => function( $d, $row ) {
                    return $d;
                }
            ),
            array(
                'db' => 'VAT',
                'dt' => 6,
                'formatter' => function( $d, $row ) {
                    if ($d == 0) {
                        return 'Non Tax';
                    } else {
                        return 'Tax';
                    }
                }
            ),
            array(
                'db' => 'GrandTotal',
                'dt' => 7,
                'formatter' => function( $d, $row ) {
                    return number_format($d, '2', '.', ',');
                }
            ),
            array(
                'db' => 'DownPayment',
                'dt' => 8,
                'formatter' => function( $d, $row ) {
                    return number_format($d, '2', '.', ',');
                }
            ),
            array(
                'db' => 'SalesID',
                'dt' => 9,
                'formatter' => function( $d, $row ) {
                    $tampReceiv = 'Completed';
                    foreach (SalesHeader::getSalesReceivable() as $receiv) {
                        if ($receiv->ID == $d) {
                            $tampReceiv = 'Uncompleted ';
                            $tampReceiv .= number_format($receiv->GrandTotal - JournalDetail::where('JournalTransactionID',$receiv->ID)->sum('JournalCreditMU'),'2','.',',');
                        }
                    }
                    return $tampReceiv;
                }
            ),
            array('db' => 'TaxNumber', 'dt' => 10),
            array('db' => 't_sales_header`.`InternalID', 'dt' => 11, 'formatter' => function( $d, $row ) {
                    $data = SalesHeader::find($d);
                    $action = '<td class="text-center">
                                    <a href="' . Route('salesDetail', $data->SalesID) . '">
                                        <button id="btn-' . $data->SalesID . '-detail"
                                                class="btn btn-pure-xs btn-xs btn-detail">
                                            <span class="glyphicon glyphicon-zoom-in"></span>
                                        </button>
                                    </a>';
                    if (!SalesHeader::isReturn($data->SalesID) && JournalDetail::where('JournalTransactionID', $data->SalesID)->sum('JournalCreditMU') == 0) {
                        $action.='<a href="' . Route('salesUpdate', $data->SalesID) . '">
                                        <button id="btn-' . $data->SalesID . '-update"
                                                class="btn btn-pure-xs btn-xs btn-edit">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </button>
                                    </a>
                                    <button data-target="#m_salesDelete" data-internal="' . $data->InternalID . '"  data-toggle="modal" role="dialog"
                                          onclick="deleteAttach(this)"  data-id="' . $data->SalesID . '" data-name=' . $data->SalesID . ' class="btn btn-pure-xs btn-xs btn-delete">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>';
                    } else {
                        $action.='<button disabled class="btn btn-pure-xs btn-xs btn-edit"><span class="glyphicon glyphicon-edit"></span></button>
                                    <button disabled class="btn btn-pure-xs btn-xs btn-delete"><span class="glyphicon glyphicon-trash"></span></button>';
                    }
                    if (checkModul('O05')) {
                        $action.='<a href="' . Route('salesCSV', $data->SalesID) . '" target="_blank">
                                        <button id="btn-' . $data->SalesID . '-print"
                                                class="btn btn-pure-xs btn-xs">
                                            <span class="glyphicon glyphicon-download"></span> CSV
                                        </button>
                                    </a>';
                    }
                    return $action;
                },
                'field' => 't_sales_header`.`InternalID')
        );

        $sql_details = getConnection();

        require('ssp.class.php');
        $ID_CLIENT_VALUE = Auth::user()->CompanyInternalID;
        if ($where != '') {
            $extraCondition = $where . ' AND ' . 't_sales_header.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        } else {
            $extraCondition = 't_sales_header.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        }
        $join = ' INNER JOIN m_currency on m_currency.InternalID = t_sales_header.CurrencyInternalID '
                . 'INNER JOIN m_coa6 on m_coa6.InternalID = t_sales_header.ACC6InternalID';

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $extraCondition, $join));
    }

    public function getResultSearchS() {
        $hitung = 0;
        $input = splitSearchValue(Input::get("id"));
        $purchasseHeader = SalesOrderHeader::join("m_coa6", 'm_coa6.InternalID', '=', 't_salesorder_header.ACC6InternalID')
                ->where('t_salesorder_header.CompanyInternalID', '=', Auth::user()->Company->InternalID)
                ->Where(function($query) use($input) {
                    $query->orWhere('SalesOrderID', "like", '%' . $input . "%")
                    ->orWhere("SalesOrderDate", "like", '%' . date("Y-m-d", strtotime(Input::get("id"))) . "%")
                    ->orWhere("ACC6Name", "like", '%' . $input . "%");
                })
                ->OrderBy('SalesOrderDate', 'desc')
                ->select('t_salesorder_header.*', 'ACC6Name')
                ->take(100)
                ->get();
        if (count($purchasseHeader) == 0) {
            ?>
            <span>There is no result with keywords <i><u><?php echo Input::get("id"); ?></u></i></span>
            <?php
        } else {
            ?>
            <select class="chosen-select choosen-modal" id="sales" style="" name="sales">
                <?php
                foreach ($purchasseHeader as $salesorder) {
                    if (checkSalesAdd($salesorder->InternalID)) {
                        ?>
                        <option value="<?php echo $salesorder->SalesOrderID ?>"><?php echo $salesorder->SalesOrderID . ' | ' . date("d-m-Y", strtotime($salesorder->SalesOrderDate)) . ' | ' . $salesorder->ACC6Name ?></option>
                        <?php
                        $hitung = 1;
                    }
                }
                ?>
            </select>
            <script>
                $(document).ready(function () {
                    var hitung = '<?php echo $hitung; ?>';
                    if (hitung == 0) {
                        $('#sales').after('<span>There is no result.</span>');
                        $('#sales').remove();
                    } else {
                        $("#btn-add-sr").removeAttr("disabled");
                    }
                });
            </script>
            <?php
        }
    }

}
