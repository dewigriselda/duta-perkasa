<?php

class SalesOrderController extends BaseController {

    public function showSalesOrder() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteSalesOrder') {
                return $this->deleteSalesOrder();
            } else if (Input::get('jenis') == 'summarySalesOrder') {
                return $this->summarySalesOrder();
            } else if (Input::get('jenis') == 'detailSalesOrder') {
                return $this->detailSalesOrder();
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = SalesOrderHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
            return View::make('penjualanAdd.salesOrderSearch')
                            ->withToogle('transaction')->withAktif('salesOrder')
                            ->withData($data);
        }
        return View::make('penjualanAdd.salesOrder')
                        ->withToogle('transaction')->withAktif('salesOrder');
    }

    public function salesOrderNew() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteSalesOrder') {
                return $this->deleteSalesOrder();
            } else if (Input::get('jenis') == 'summarySalesOrder') {
                return $this->summarySalesOrder();
            } else if (Input::get('jenis') == 'detailSalesOrder') {
                return $this->detailSalesOrder();
            } else {
                return $this->insertSalesOrder();
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && Input::get('typePayment') != '') {
            $data = SalesOrderHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
            return View::make('penjualanAdd.salesOrderSearch')
                            ->withToogle('transaction')->withAktif('salesOrder')
                            ->withData($data);
        }
        $salesOrder = $this->createID(0) . '.';
        return View::make('penjualanAdd.salesOrderNew')
                        ->withToogle('transaction')->withAktif('salesOrder')
                        ->withSalesorder($salesOrder);
    }

    public function salesOrderDetail($id) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'summarySalesOrder') {
                return $this->summarySalesOrder();
            } else if (Input::get('jenis') == 'detailSalesOrder') {
                return $this->detailSalesOrder();
            }
        }
        $id = SalesOrderHeader::getIdsalesOrder($id);
        $header = SalesOrderHeader::find($id);
        $detail = SalesOrderHeader::find($id)->salesOrderDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            return View::make('penjualanAdd.salesOrderDetail')
                            ->withToogle('transaction')->withAktif('salesOrder')
                            ->withHeader($header)
                            ->withDetail($detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showSalesOrder');
        }
    }

    public function salesOrderUpdate($id) {
        $id = SalesOrderHeader::getIdsalesOrder($id);
        $header = SalesOrderHeader::find($id);
        $detail = SalesOrderHeader::find($id)->salesOrderDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID && SalesOrderHeader::isInvoice($header->InternalID) == false) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                if (Input::get('jenis') == 'summarySalesOrder') {
                    return $this->summarySalesOrder();
                } else if (Input::get('jenis') == 'detailSalesOrder') {
                    return $this->detailSalesOrder();
                } else {
                    return $this->updateSalesOrder($id);
                }
            }
            $salesOrder = $this->createID(0) . '.';
            return View::make('penjualanAdd.salesOrderUpdate')
                            ->withToogle('transaction')->withAktif('salesOrder')
                            ->withHeader($header)
                            ->withDetail($detail)
                            ->withSalesorder($salesOrder);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showSalesOrder');
        }
    }

    public function insertSalesOrder() {
        //rule
        $jenis = Input::get('isCash');
        if ($jenis == '0') {
            $rule = array(
                'date' => 'required',
                'coa6' => 'required',
                'remark' => 'required|max:1000',
                'currency' => 'required',
                'warehouse' => 'required',
                'rate' => 'required',
                'inventory' => 'required'
            );
            $longTerm = 0;
        } else {
            $rule = array(
                'date' => 'required',
                'longTerm' => 'required|integer',
                'coa6' => 'required',
                'remark' => 'required|max:1000',
                'currency' => 'required',
                'warehouse' => 'required',
                'rate' => 'required',
                'inventory' => 'required'
            );
            $longTerm = Input::get('longTerm');
        }
        $salesOrderNumber = '';
        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalInsert';
            $error = $validator->messages();
        } else {
            //valid
            //insert header
            $header = new SalesOrderHeader;
            $salesOrder = $this->createID(1) . '.';
            $date = explode('-', Input::get('date'));
            $yearDigit = substr($date[2], 2);
            $salesOrder .= $date[1] . $yearDigit . '.';
            $salesOrderNumber = SalesOrderHeader::getNextIDSalesOrder($salesOrder);
            $header->SalesOrderID = $salesOrderNumber;
            $header->SalesOrderDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $header->ACC6InternalID = Input::get('coa6');
            $header->LongTerm = $longTerm;
            $header->isCash = $jenis;
            $currency = explode('---;---', Input::get('currency'));
            $header->WarehouseInternalID = Input::get('warehouse');
            $header->CurrencyInternalID = $currency[0];
            $header->CurrencyRate = str_replace(',', '', Input::get('rate'));
            if (Input::get('vat') == '') {
                $header->VAT = '0';
            } else {
                $header->VAT = Input::get('vat');
            }
            $header->DiscountGlobal = str_replace(",", "", Input::get('DiscountGlobal'));
            $header->GrandTotal = Input::get('grandTotalValue');
            $header->UserRecord = Auth::user()->UserID;
            $header->CompanyInternalID = Auth::user()->Company->InternalID;
            $header->UserModified = '0';
            $header->Remark = Input::get('remark');
            $header->save();
            //insert detail
            $total = 0;
            for ($a = 0; $a < count(Input::get('inventory')); $a++) {
                $qtyValue = str_replace(',', '', Input::get('qty')[$a]);
                $priceValue = str_replace(',', '', Input::get('price')[$a]);
                $discValue = str_replace(',', '', Input::get('discountNominal')[$a]);
                $subTotal = ($priceValue * $qtyValue) - (($priceValue * $qtyValue) * Input::get('discount')[$a] / 100) - $discValue * $qtyValue;
                if (Input::get('vat') == '1') {
                    $vatValue = $subTotal / 10;
                } else {
                    $vatValue = 0;
                }
                if ($qtyValue > 0) {
                    $detail = new SalesOrderDetail();
                    $detail->SalesOrderInternalID = $header->InternalID;
                    $detail->InventoryInternalID = Input::get('inventory')[$a];
                    $detail->Qty = $qtyValue;
                    $detail->Price = $priceValue;
                    $detail->Discount = Input::get('discount')[$a];
                    $detail->DiscountNominal = $discValue;
                    $detail->VAT = $vatValue;
                    $detail->SubTotal = $subTotal;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = '0';
                    $detail->save();
                }
                $total += $subTotal;
            }

            $messages = 'suksesInsert';
            $error = '';
        }
        $salesOrder = $this->createID(0) . '.';
        return View::make('penjualanAdd.salesOrderNew')
                        ->withToogle('transaction')->withAktif('salesOrder')
                        ->withSalesorder($salesOrder)
                        ->withError($error)
                        ->withMessages($messages);
    }

    public function updateSalesOrder($id) {
        //tipe
        $headerUpdate = SalesOrderHeader::find($id);
        $detailUpdate = SalesOrderHeader::find($id)->salesOrderDetail()->get();

        //rule
        if (Input::get('isCash') == '0') {
            $rule = array(
                'coa6' => 'required',
                'remark' => 'required|max:1000',
                'currency' => 'required',
                'warehouse' => 'required',
                'rate' => 'required',
                'inventory' => 'required'
            );
            $longTerm = 0;
        } else {
            $rule = array(
                'longTerm' => 'required|integer',
                'coa6' => 'required',
                'remark' => 'required|max:1000',
                'currency' => 'required',
                'warehouse' => 'required',
                'rate' => 'required',
                'inventory' => 'required'
            );
            $longTerm = Input::get('longTerm');
        }

        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalUpdate';
            $error = $validator->messages();
        } else {
            $header = SalesOrderHeader::find(Input::get('SalesOrderInternalID'));
            $header->ACC6InternalID = Input::get('coa6');
            $header->isCash = Input::get('isCash');
            $header->LongTerm = $longTerm;
            $currency = explode('---;---', Input::get('currency'));
            $header->WarehouseInternalID = Input::get('warehouse');
            $header->CurrencyInternalID = $currency[0];
            $header->CurrencyRate = str_replace(',', '', Input::get('rate'));
            if (Input::get('vat') == '') {
                $header->VAT = '0';
            } else {
                $header->VAT = Input::get('vat');
            }
            $header->DiscountGlobal = str_replace(",", "", Input::get('DiscountGlobal'));
            $header->GrandTotal = Input::get('grandTotalValue');
            $header->UserModified = Auth::user()->UserID;
            $header->Remark = Input::get('remark');
            $header->save();

            //delete salesOrder detail -- nantinya insert ulang
            SalesOrderDetail::where('SalesOrderInternalID', '=', Input::get('SalesOrderInternalID'))->delete();

            //insert detail
            $total = 0;
            for ($a = 0; $a < count(Input::get('inventory')); $a++) {
                $qtyValue = str_replace(',', '', Input::get('qty')[$a]);
                $priceValue = str_replace(',', '', Input::get('price')[$a]);
                $discValue = str_replace(',', '', Input::get('discountNominal')[$a]);
                $subTotal = ($priceValue * $qtyValue) - ($priceValue * $qtyValue) * Input::get('discount')[$a] / 100 - $discValue * $qtyValue;
                $total += $subTotal;
                if (Input::get('vat') == '1') {
                    $vatValue = $subTotal / 10;
                } else {
                    $vatValue = 0;
                }
                if ($qtyValue > 0) {
                    $detail = new SalesOrderDetail();
                    $detail->SalesOrderInternalID = Input::get('SalesOrderInternalID');
                    $detail->InventoryInternalID = Input::get('inventory')[$a];
                    $detail->Qty = $qtyValue;
                    $detail->Price = $priceValue;
                    $detail->Discount = Input::get('discount')[$a];
                    $detail->DiscountNominal = $discValue;
                    $detail->VAT = $vatValue;
                    $detail->SubTotal = $subTotal;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = Auth::user()->UserID;
                    $detail->save();
                }
            }

            $messages = 'suksesUpdate';
            $error = '';
        }

        //tipe
        $header = SalesOrderHeader::find($id);
        $detail = SalesOrderHeader::find($id)->salesOrderDetail()->get();
        $salesOrder = $this->createID(0) . '.';
        return View::make('penjualanAdd.salesOrderUpdate')
                        ->withToogle('transaction')->withAktif('salesOrder')->withHeader($header)
                        ->withDetail($detail)
                        ->withSalesorder($salesOrder)->withError($error)
                        ->withMessages($messages);
    }

    public function deleteSalesOrder() {
        $salesOrder = DB::select(DB::raw('SELECT * FROM t_sales_header WHERE CompanyInternalID = "' . Auth::user()->CompanyInternalID . '" AND SalesOrderInternalID = "' . Input::get('InternalID') . '"'));
        if (is_null($salesOrder) == '') {
            //tidak ada yang menggunakan data salesOrder maka data boleh dihapus
            $salesOrderHeader = SalesOrderHeader::find(Input::get('InternalID'));
            if ($salesOrderHeader->CompanyInternalID == Auth::user()->Company->InternalID) {
                //hapus detil
                $detilData = SalesOrderHeader::find(Input::get('InternalID'))->salesOrderDetail;
                foreach ($detilData as $value) {
                    $detil = salesOrderDetail::find($value->InternalID);
                    $detil->delete();
                }
                //hapus salesOrder
                $salesOrder = SalesOrderHeader::find(Input::get('InternalID'));
                $salesOrder->delete();
                $messages = 'suksesDelete';
            } else {
                $messages = 'accessDenied';
            }
        } else {
            //ada maka data tidak boleh dihapus
            $messages = 'gagalDelete';
        }
        $data = SalesOrderHeader::advancedSearch(Input::get('typePayment'), Input::get('typeTax'), Input::get('startDate'), Input::get('endDate'));
        return View::make('penjualanAdd.salesOrderSearch')
                        ->withToogle('transaction')->withAktif('salesOrder')
                        ->withMessages($messages)
                        ->withData($data);
    }

    function salesOrderPrint($id) {
        $id = SalesOrderHeader::getIdsalesOrder($id);
        $header = SalesOrderHeader::find($id);
        $detail = SalesOrderHeader::find($id)->salesOrderDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $coa6 = SalesOrderHeader::find($header->InternalID)->coa6;
            $customer = $coa6->ACC6ID . ' ' . $coa6->ACC6Name . ', ' . $coa6->Address . '<br>Phone: ' . $coa6->Phone . ', Fax: ' . $coa6->Fax;
            if ($header->isCash == 0) {
                $payment = 'Cash';
            } else {
                $payment = 'Credit';
            }
            $currency = Currency::find($header->CurrencyInternalID);
            $currencyName = $currency->CurrencyName;
            if ($header->VAT == 0) {
                $vat = 'Non Tax';
            } else {
                $vat = 'Tax';
            }
            $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 6px; position: fixed; left:84%; bottom: -85px; right: 0px; height: 80px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 6px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 195px;border-bottom:1px solid #ddd;margin-bottom:20px;">
                        <div style="height: 133px">
                        <img height="100%" src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                        </div>
                        <div style="width: 420px; float: left;">
                            <b style="font-family: tahoma,sans-serif;margin: 0;font-size: 12px;">' . Auth::user()->Company->CompanyName . '</b>
                        </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 2px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Address</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500;">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 1px; border: none;  font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; ">:</td>
                                    <td style="padding: 1px; border: none; font-family: helvetica,sans-serif;font-size: 8px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 12px; text-align: center;margin-bottom: 0px;">Sales Order</h5>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 0px; width: 100%;">
                            <table>
                            <tr>
                            <td width="275px">
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Order ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->SalesOrderID . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->SalesOrderDate)) . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Customer</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $customer . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Payment</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $payment . '</td>
                                 </tr>';
            if ($header->isCash != 0) {
                $html.='<tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Due Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . date("d-M-Y", strtotime("+" . $header->LongTerm . " day", strtotime($header->SalesOrderDate))) . '</td>
                                 </tr>';
            }
            $html.='
                                </table>
                                </td>
                                <td>
                                <table>
                                <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Warehouse</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Warehouse->WarehouseName . '</td>
                                     </tr>
                                     <tr style="background: none;">
                                        <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Currency</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $currencyName . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Rate</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . number_format($header->CurrencyRate, '2', '.', ',') . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Vat</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $vat . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Remark</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">' . $header->Remark . '</td>
                                     </tr>
                                </table></td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 2px solid #ddd;">
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="25%">Inventory</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="5%">Qty</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Price</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc (%)</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Disc</th>
                                        <th style="font-family: helvetica,sans-serif;font-size: 9px; margin:  7px !important; font-weight: 700;" width="15%">Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            $total = 0;
            $totalVAT = 0;
            if (count($detail) > 0) {
                foreach ($detail as $data) {
                    $inventory = Inventory::find($data->InventoryInternalID);
                    $inv = $inventory->InventoryID . ' ' . $inventory->InventoryName;
                    $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;">' . $inv . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
                    $totalVAT += $data->VAT;
                    $total += $data->SubTotal;
                }
                if ($totalVAT != 0) {
                    $totalVAT = $totalVAT - ($header->DiscountGlobal * 0.1);
                }
            } else {
                $html.= '<tr>
                            <td colspan="6" style="font-family: helvetica,sans-serif;font-size: 9px; margin: 7px !important; font-weight: 500;  text-align: center">There is no inventory registered in this salesOrder.</td>
                        </tr>';
            }
            $html.= '</tbody>
                            </table>
                            <div style="box-sizing: border-box;min-width: 200px; margin-left: 320px; display: inline-block; clear: both;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Discount</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Grand Total</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($total - $header->DiscountGlobal, '2', '.', ',') . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">Tax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">' . number_format($totalVAT, '2', '.', ',') . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; ">Grand Total (Tax)</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 9px;  font-weight: 700; text-align: right;">'
                    . number_format($header->GrandTotal, '2', '.', ',') . '</td>
                                 </tr>
                                </table>
                            </div>
                    </div>
                </body>
            </html>';
            return PDF ::load($html, 'A5', 'portrait')->show();
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showSalesOrder');
        }
    }

    function createID($tipe) {
        $salesOrder = 'SO';
        if ($tipe == 0) {
            $salesOrder .= '.' . date('m') . date('y');
        }
        return $salesOrder;
    }

    public function formatCariIDSalesOrder() {
        $date = explode('-', Input::get('date'));
        $id = $this->createID(1);
        $yearDigit = substr($date[2], 2);
        $id .= '.' . $date[1] . $yearDigit . '.';
        echo SalesOrderHeader::getNextIDSalesOrder($id);
    }

    public function summarySalesOrder() {
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $totalSO = 0;
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Sales Order Summary Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span>';
        $hitung = 0;
        foreach (Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $dataCustomer) {
            if (SalesOrderHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                            ->where('ACC6InternalID', $dataCustomer->InternalID)
                            ->whereBetween('SalesOrderDate', Array($start, $end))->count() > 0) {
                $html.= '<table class="tableBorder" width="100%"  style="margin-top: 18px; clear: both;  top: 78px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=7>' . $dataCustomer->ACC6Name . '</th>
                                        </tr>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales Order ID</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Date</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Currency</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Rate</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total (After Discount)</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Vat</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Grand Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                $sumGrandTotal = 0;
                foreach (SalesOrderHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                        ->where('ACC6InternalID', $dataCustomer->InternalID)
                        ->whereBetween('SalesOrderDate', Array($start, $end))->get() as $data) {
                    $grandTotal = $data->GrandTotal;
                    $sumGrandTotal += $grandTotal;
                    $totalSO += $grandTotal;
                    $total = $grandTotal;
                    $vat = 0;
                    if ($data->VAT == 1) {
                        $total = $total * 10 / 11;
                        $vat = $total / 10;
                    }
                    $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->SalesOrderID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . date("d-M-Y", strtotime($data->SalesOrderDate)) . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->Currency->CurrencyName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->CurrencyRate, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($total, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($vat, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($grandTotal, '2', '.', ',') . '</td>
                            </tr>';
                }
                $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="80%" colspan="6">Total : </td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . number_format($sumGrandTotal, '2', '.', ',') . '</td>
                            </tr>';
                $html.= '</tbody>
                        </table>';
                $hitung++;
            }
        }
        if ($hitung == 0) {
            $html.='<br><span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no sales order.</span>';
        }
        $html.= '<table class="tableBorder" width="100%"  style="margin-top: 10px; clear: both;  top: 50px;">
                    <thead>
                        <tr>
                            <th style="font-family: helvetica,sans-serif;font-size: 14px; margin: 5px !important; font-weight: 700; text-align: right" width="80%" colspan="6">Total Sales Order : </th>
                            <th style="font-family: helvetica,sans-serif;font-size: 14px; margin: 5px !important; font-weight: 700; text-align: right" width="20%">' . number_format($totalSO, '2', '.', ',') . '</th>
                        </tr>
                    <thead>
                </table>';
        $html.='</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('sales_order_summary');
    }

    public function detailSalesOrder() {
        $startT = explode('-', Input::get('sDate'));
        $endT = explode('-', Input::get('eDate'));
        $start = $startT[2] . '-' . $startT[1] . '-' . $startT[0];
        $end = $endT[2] . '-' . $endT[1] . '-' . $endT[0];
        $dateTamp = '';
        $coa6Tamp = '';
        $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0px;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Sales Order Detail Report</h5>
                        <span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Period : ' . date("d-M-Y", strtotime(Input::get('sDate'))) . ' to ' . date("d-M-Y", strtotime(Input::get('eDate'))) . '</span><br><br>';
        if (SalesOrderHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                        ->whereBetween('SalesOrderDate', Array($start, $end))
                        ->orderBy('SalesOrderDate')->orderBy('ACC6InternalID')->count() > 0) {
            foreach (SalesOrderHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)
                    ->whereBetween('SalesOrderDate', Array($start, $end))
                    ->orderBy('SalesOrderDate')->orderBy('ACC6InternalID')->get() as $dataPenjualan) {
                if ($dateTamp != date("d-M-Y", strtotime($dataPenjualan->SalesOrderDate))) {
                    $html.= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Sales Order Date : ' . date("d-M-Y", strtotime($dataPenjualan->SalesOrderDate)) . '</span><br><br>';
                    $dateTamp = date("d-M-Y", strtotime($dataPenjualan->SalesOrderDate));
                    $coa6Tamp = '';
                }
                if ($coa6Tamp != $dataPenjualan->ACC6InternalID) {
                    $html.= '<span style="padding-left:23px;font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Customer : ' . $dataPenjualan->coa6->ACC6Name . '</span>';
                    $coa6Tamp = $dataPenjualan->ACC6InternalID;
                }
                $html.= '<table class="tableBorder" style="width:95%; margin-top: 18px; clear: both; position: relative; left: 4%">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=6>' . $dataPenjualan->SalesOrderID . ' | ' . $dataPenjualan->Currency->CurrencyName . ' | Rate : ' . number_format($dataPenjualan->CurrencyRate, '2', '.', ',') . ' </th>
                                        </tr>
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory ID</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Name</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Qty</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Price</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc (%)</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Disc</th>
                                            <th width="15%" style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                $total = 0;
                $vat = 0;
                foreach ($dataPenjualan->salesOrderDetail as $data) {
                    $total += $data->SubTotal;
                    $vat += $data->VAT;
                    $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->inventory->InventoryID . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $data->inventory->InventoryName . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Qty, 0, '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->Price, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Discount . '' . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->DiscountNominal, '2', '.', ',') . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . number_format($data->SubTotal, '2', '.', ',') . '</td>
                            </tr>';
                }
                if ($vat != 0) {
                    $vat = $vat - ($dataPenjualan->DiscountGlobal * 0.1);
                }
                $html.= '<tr>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=4></td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: left;" colspan=2>Total <br> Discount <br> Grand Total <br> VAT <br> Grand Total (Tax)</td>
                    <td style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700; text-align: right;">' . number_format($total, '2', '.', ',') . '<br>'
                        . '' . number_format($dataPenjualan->DiscountGlobal, '2', '.', ',') . '<br>'
                        . '' . number_format($total - $dataPenjualan->DiscountGlobal, '2', '.', ',') . '<br>'
                        . '' . number_format($vat, '2', '.', ',') . '<br>'
                        . '' . number_format($dataPenjualan->GrandTotal, '2', '.', ',') . '</td>
                    </tr>
                </tbody>
            </table>';
            }
        } else {
            $html.= '<span style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">There is no sales order.</span><br><br>';
        }
        $html.='</div>
                    </div>
                </body>
            </html>';
        return PDF ::load($html, 'A4', 'portrait')->download('sales_order_detail');
    }

    static function salesOrderDataBackup($data) {
        $explode = explode('---;---', $data);
        $typePayment = $explode[0];
        $typeTax = $explode[1];
        $start = $explode[2];
        $end = $explode[3];
        $where = '';
        if ($typePayment != '-1' && $typePayment != '') {
            $where .= 'isCash = "' . $typePayment . '" ';
        }
        if ($typeTax != '-1' && $typeTax != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'VAT = "' . $typeTax . '" ';
        }
        if ($start != '' && $end != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $start = explode('-', $start);
            $end = explode('-', $end);
            $startSearch = $start[2] . '-' . $start[1] . '-' . $start[0];
            $endSearch = $end[2] . '-' . $end[1] . '-' . $end[0];
            $where .= 'SalesOrderDate between "' . $startSearch . ' 00:00:00" AND "' . $endSearch . ' 23:59:59"';
        }
        $table = 't_salesorder_header';
        $primaryKey = 't_salesorder_header`.`InternalID';
        $columns = array(
            array('db' => 'SalesOrderID', 'dt' => 0),
            array('db' => 't_salesorder_header`.`InternalID', 'dt' => 1, 'formatter' => function( $d, $row ) {
                    $sales = SalesOrderHeader::find($d);
                    if ($sales->isCash == 0) {
                        return 'Cash (-)';
                    } else {
                        return 'Credit ' . $sales->LongTerm;
                    }
                },
                'field' => 't_salesorder_header`.`InternalID'),
            array(
                'db' => 'SalesOrderDate',
                'dt' => 2,
                'formatter' => function( $d, $row ) {
                    return date("d-m-Y", strtotime($d));
                }
            ),
            array('db' => 'CurrencyName', 'dt' => 3),
            array(
                'db' => 'CurrencyRate',
                'dt' => 4,
                'formatter' => function( $d, $row ) {
                    return number_format($d, '2', '.', ',');
                }
            ),
            array(
                'db' => 'ACC6Name',
                'dt' => 5,
                'formatter' => function( $d, $row ) {
                    return $d;
                }
            ),
            array(
                'db' => 'VAT',
                'dt' => 6,
                'formatter' => function( $d, $row ) {
                    if ($d == 0) {
                        return 'Non Tax';
                    } else {
                        return 'Tax';
                    }
                }
            ),
            array(
                'db' => 'GrandTotal',
                'dt' => 7,
                'formatter' => function( $d, $row ) {
                    return number_format($d, '2', '.', ',');
                }
            ),
            array('db' => 't_salesorder_header`.`InternalID', 'dt' => 8, 'formatter' => function( $d, $row ) {
                    $data = SalesOrderHeader::find($d);
                    $action = '<td class="text-center">
                                    <a href="' . Route('salesOrderDetail', $data->SalesOrderID) . '">
                                        <button id="btn-' . $data->SalesOrderID . '-detail"
                                                class="btn btn-pure-xs btn-xs btn-detail">
                                            <span class="glyphicon glyphicon-zoom-in"></span>
                                        </button>
                                    </a>';
                    if (!SalesOrderHeader::isInvoice($data->InternalID)) {
                        $action.='<a href="' . Route('salesOrderUpdate', $data->SalesOrderID) . '">
                                        <button id="btn-' . $data->SalesOrderID . '-update"
                                                class="btn btn-pure-xs btn-xs btn-edit">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </button>
                                    </a>
                                    <button data-target="#m_salesOrderDelete" data-internal="' . $data->InternalID . '"  data-toggle="modal" role="dialog"
                                           onclick="deleteAttach(this)" data-id="' . $data->SalesOrderID . '" data-name=' . $data->SalesOrderID . ' class="btn btn-pure-xs btn-xs btn-delete">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>';
                    } else {
                        $action.='<button disabled class="btn btn-pure-xs btn-xs btn-edit"><span class="glyphicon glyphicon-edit"></span></button>
                                    <button disabled class="btn btn-pure-xs btn-xs btn-delete"><span class="glyphicon glyphicon-trash"></span></button>';
                    }
                    return $action;
                },
                'field' => 't_salesorder_header`.`InternalID')
        );

        $sql_details = getConnection();

        require('ssp.class.php');
        $ID_CLIENT_VALUE = Auth::user()->CompanyInternalID;
        if ($where != '') {
            $extraCondition = $where . ' AND ' . 't_salesorder_header.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        } else {
            $extraCondition = 't_salesorder_header.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        }
        $join = ' INNER JOIN m_currency on m_currency.InternalID = t_salesorder_header.CurrencyInternalID '
                . 'INNER JOIN m_coa6 on m_coa6.InternalID = t_salesorder_header.ACC6InternalID';

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $extraCondition, $join));
    }

}
