<?php

class TransferController extends BaseController {

    public function showTransfer() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (Input::get('jenis') == 'deleteTransfer') {
                return $this->deleteTransfer();
            }
        }
        return View::make('transfer.transfer')
                        ->withToogle('transaction')->withAktif('transfer');
    }

    public function transferNew() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            return $this->insertTransfer();
        }
        $transfer = $this->createID(0) . '.';
        return View::make('transfer.transferNew')
                        ->withToogle('transaction')->withAktif('transfer')
                        ->withTransfer($transfer);
    }

    public function transferDetail($id) {
        $id = TransferHeader::getIdtransfer($id);
        $header = TransferHeader::find($id);
        $detail = TransferHeader::find($id)->transferDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            return View::make('transfer.transferDetail')
                            ->withToogle('transaction')->withAktif('transfer')
                            ->withHeader($header)
                            ->withDetail($detail);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showTransfer');
        }
    }

    public function transferUpdate($id) {
        $id = TransferHeader::getIdtransfer($id);
        $header = TransferHeader::find($id);
        $detail = TransferHeader::find($id)->transferDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                return $this->updateTransfer($id);
            }
            $transfer = $this->createID(0) . '.';
            return View::make('transfer.transferUpdate')
                            ->withToogle('transaction')->withAktif('transfer')
                            ->withHeader($header)
                            ->withDetail($detail)
                            ->withTransfer($transfer);
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showTransfer');
        }
    }

    public function insertTransfer() {
        //rule
        $rule = array(
            'date' => 'required',
            'remark' => 'required|max:1000',
            'warehouseSource' => 'required',
            'warehouseDestiny' => 'required',
            'inventory' => 'required'
        );
        $transferNumber = '';
        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalInsert';
            $error = $validator->messages();
        } else {
            //valid
            //insert header
            $header = new TransferHeader;
            $transfer = $this->createID(1) . '.';
            $date = explode('-', Input::get('date'));
            $yearDigit = substr($date[2], 2);
            $transfer .= $date[1] . $yearDigit . '.';
            $transferNumber = TransferHeader::getNextIDTransfer($transfer);
            $header->TransferID = $transferNumber;
            $header->TransferDate = $date[2] . '-' . $date[1] . '-' . $date[0];
            $header->WarehouseInternalID = Input::get('warehouseSource');
            $header->WarehouseDestinyInternalID = Input::get('warehouseDestiny');
            $header->UserRecord = Auth::user()->UserID;
            $header->CompanyInternalID = Auth::user()->Company->InternalID;
            $header->UserModified = '0';
            $header->Remark = Input::get('remark');
            $header->save();
            //insert detail
            $total = 0;
            for ($a = 0; $a < count(Input::get('inventory')); $a++) {
                $qtyValue = Input::get('qty')[$a];
                if ($qtyValue > 0) {
                    $detail = new TransferDetail();
                    $detail->TransferInternalID = $header->InternalID;
                    $detail->InventoryInternalID = Input::get('inventory')[$a];
                    $detail->Qty = $qtyValue;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = '0';
                    $detail->save();
                }
            }
            $messages = 'suksesInsert';
            $error = '';
        }
        $transfer = $this->createID(0) . '.';
        return View::make('transfer.transferNew')
                        ->withToogle('transaction')->withAktif('transfer')
                        ->withTransfer($transfer)
                        ->withError($error)
                        ->withMessages($messages);
    }

    public function updateTransfer($id) {
        //tipe
        $headerUpdate = TransferHeader::find($id);
        $detailUpdate = TransferHeader::find($id)->transferDetail()->get();
        //rule
        $rule = array(
            'remark' => 'required|max:1000',
            'warehouseSource' => 'required',
            'warehouseDestiny' => 'required',
            'inventory' => 'required'
        );
        //validasi
        $data = Input::all();
        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            //tidak valid
            $messages = 'gagalUpdate';
            $error = $validator->messages();
        } else {
            $header = TransferHeader::find(Input::get('TransferInternalID'));
            $header->WarehouseInternalID = Input::get('warehouseSource');
            $header->WarehouseDestinyInternalID = Input::get('warehouseDestiny');
            $header->UserModified = Auth::user()->UserID;
            $header->Remark = Input::get('remark');
            $header->save();

            //delete transfer detail -- nantinya insert ulang
            TransferDetail::where('TransferInternalID', '=', Input::get('TransferInternalID'))->delete();

            //insert detail
            $total = 0;
            for ($a = 0; $a < count(Input::get('inventory')); $a++) {
                $qtyValue = Input::get('qty')[$a];
                if ($qtyValue > 0) {
                    $detail = new TransferDetail();
                    $detail->TransferInternalID = Input::get('TransferInternalID');
                    $detail->InventoryInternalID = Input::get('inventory')[$a];
                    $detail->Qty = $qtyValue;
                    $detail->UserRecord = Auth::user()->UserID;
                    $detail->UserModified = Auth::user()->UserID;
                    $detail->save();
                }
            }
            $messages = 'suksesUpdate';
            $error = '';
        }

        //tipe
        $header = TransferHeader::find($id);
        $detail = TransferHeader::find($id)->transferDetail()->get();
        $transfer = $this->createID(0) . '.';
        return View::make('transfer.transferUpdate')
                        ->withToogle('transaction')->withAktif('transfer')->withHeader($header)
                        ->withDetail($detail)
                        ->withTransfer($transfer)->withError($error)
                        ->withMessages($messages);
    }

    public function deleteTransfer() {
        $transfer = null;
        if (is_null($transfer)) {
            //tidak ada yang menggunakan data transfer maka data boleh dihapus
            //hapus journal
            $transferHeader = TransferHeader::find(Input::get('InternalID'));
            if ($transferHeader->CompanyInternalID == Auth::user()->Company->InternalID) {
                //hapus detil
                $detilData = TransferHeader::find(Input::get('InternalID'))->transferDetail;
                foreach ($detilData as $value) {
                    $detil = transferDetail::find($value->InternalID);
                    $detil->delete();
                }
                //hapus transfer
                $transfer = TransferHeader::find(Input::get('InternalID'));
                $transfer->delete();
                $messages = 'suksesDelete';
            } else {
                $messages = 'accessDenied';
            }
        } else {
            //ada maka data tidak boleh dihapus
            $messages = 'gagalDelete';
        }
        return View::make('transfer.transfer')
                        ->withToogle('transaction')->withAktif('transfer')
                        ->withMessages($messages);
    }

    function transferPrint($id) {
        $id = TransferHeader::getIdtransfer($id);
        $header = TransferHeader::find($id);
        $detail = TransferHeader::find($id)->transferDetail()->get();
        if ($header->CompanyInternalID == Auth::user()->Company->InternalID) {
            $html = '
            <html>
                <head>
                    <style>
                        .tableBorder {
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                        }
                        .tableBorder th{
                            padding: 3px;
                            border-spacing: 0;
                            border: 0.5px solid #ddd;
                            text-align: center;
                        }
                        .tableBorder td{
                            border-spacing: 0;
                            padding: 8px;
                            border: 0.5px solid #ddd;
                        }
                        .footer { font-family: helvetica,sans-serif; font-size: 8px; position: fixed; left:84%; bottom: -83px; right: 0px; height: 78px;}
                        .footer .page:after { 
                        font-family: helvetica,sans-serif; font-size: 8px; content: "Page " counter(page);}
                    </style>
                </head>
                <body>
                    <div class="footer">
                        <span class="page"></span>
                    </div>
                    <div style="padding:0px; box-sizing: border-box;">
                        <div style="height: 298px;border-bottom:1px solid #ddd;margin-bottom:18px;">
                                <img src = "' . substr(Auth::user()->Company->Logo, 1) . '">
                            <div style="width: 418px; float: left;">
                                <h3 style="font-family: tahoma,sans-serif;margin: 0;">' . Auth::user()->Company->CompanyName . '</h3>
                            </div> 
                            <div style=" box-sizing: border-box;margin-top: 0px;position: absolute; right: 0px; float: right;">
                                 <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Address</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Address . ' ' . Auth::user()->Company->City . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">Fax</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Fax . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none;  font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">Phone Number</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; ">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 500; text-align: right;">' . Auth::user()->Company->Phone . '</td>
                                 </tr>
                                 </table>
                            </div>           
                        </div>
                        <h5 style="font-family: helvetica,sans-serif;font-size: 16px; text-align: center;">Transfer</h5>
                        <div style="display: inline-block; clear: both; position: static; margin-bottom: 18px; width: 100%;">
                            <table>
                            <tr>
                            <td width="423px">
                                <table>
                                 <tr style="background: none;">
                                    <td style="padding: 2px;border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Transfer ID</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $header->TransferID . '</td>
                                 </tr>
                                <tr style="background: none;">
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Date</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                    <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . date("d-M-Y", strtotime($header->TransferDate)) . '</td>
                                 </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Source</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $header->WarehouseSource->WarehouseName . '</td>
                                     </tr>
                                </table>
                                </td>
                                <td>
                                <table>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Destiny</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $header->WarehouseDestiny->WarehouseName . '</td>
                                     </tr>
                                    <tr style="background: none;">
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">Remark</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">:</td>
                                        <td style="padding: 2px; border: none; font-family: helvetica,sans-serif;font-size: 10px;  font-weight: 700;">' . $header->Remark . '</td>
                                     </tr>
                                </table></td>
                                </tr>
                            </table>
                        </div>    
                            <table class="tableBorder" width="100%" style="margin-top: 5px; clear: both; top: 80px;">
                                    <thead >
                                        <tr style="border-collapse: separate; border: 0px solid #ddd;">
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Inventory</th>
                                            <th style="font-family: helvetica,sans-serif;font-size: 10px; margin:  5px !important; font-weight: 700;">Quantity Transfer</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            $total = 0;
            if (count($detail) > 0) {
                foreach ($detail as $data) {
                    $inventory = Inventory::find($data->InventoryInternalID);
                    $inv = $inventory->InventoryID . ' ' . $inventory->InventoryName;
                    $html.= '<tr>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;">' . $inv . '</td>
                                <td style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500; text-align: right">' . $data->Qty . '</td>
                            </tr>';
                    $total += $data->SubTotal;
                }
            } else {
                $html.= '<tr>
                            <td colspan="2" style="font-family: helvetica,sans-serif;font-size: 10px; margin: 5px !important; font-weight: 500;  text-align: center">There is no inventory registered in this transfer.</td>
                        </tr>';
            }
            $html.= '</tbody>
                            </table>
                            <br>
                    </div>
                </body>
            </html>';
            return PDF ::load($html, 'A4', 'portrait')->show();
        } else {
            $messages = 'accessDenied';
            Session::flash('messages', $messages);
            return Redirect::Route('showTransfer');
        }
    }

    function createID($tipe) {
        $transfer = 'TF';
        if ($tipe == 0) {
            $transfer .= '.' . date('m') . date('y');
        }
        return $transfer;
    }

    public function formatCariIDTransfer() {
        $date = explode('-', Input::get('date'));
        $id = $this->createID(1);
        $yearDigit = substr($date[2], 2);
        $id .= '.' . $date[1] . $yearDigit . '.';
        echo TransferHeader::getNextIDTransfer($id);
    }

}
