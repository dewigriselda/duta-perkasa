<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Coa extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_coa';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showCoa() {
        return Coa::join('m_coa1', 'm_coa.ACC1InternalID', '=', 'm_coa1.InternalID')
                        ->join('m_coa2', 'm_coa.ACC2InternalID', '=', 'm_coa2.InternalID')
                        ->join('m_coa3', 'm_coa.ACC3InternalID', '=', 'm_coa3.InternalID')
                        ->join('m_coa4', 'm_coa.ACC4InternalID', '=', 'm_coa4.InternalID')
                        ->join('m_coa5', 'm_coa.ACC5InternalID', '=', 'm_coa5.InternalID')
                        ->join('m_coa6', 'm_coa.ACC6InternalID', '=', 'm_coa6.InternalID')
                        ->select("m_coa.*", 'm_coa1.ACC1ID', 'm_coa2.ACC2ID', 'm_coa3.ACC3ID', 'm_coa4.ACC4ID', 'm_coa5.ACC5ID', 'm_coa6.ACC6ID', 'm_coa1.InternalID as m1id', 'm_coa2.InternalID as m2id', 'm_coa3.InternalID as m3id', 'm_coa4.InternalID as m4id', 'm_coa5.InternalID as m5id', 'm_coa6.InternalID as m6id')
                        ->where('m_coa.CompanyInternalID', Auth::user()->Company->InternalID)
                        ->get();
    }

    public static function header() {
        return Coa::select('Header1', 'Header2', 'Header3', 'HeaderCashFlow1', 'HeaderCashFlow2', 'HeaderCashFlow3')
                        ->where('CompanyInternalID', Auth::user()->Company->InternalID)
                        ->get();
    }

    public static function formatCoa($acc1, $acc2, $acc3, $acc4, $acc5, $acc6, $jenis) {
        if ($jenis == 1) {
            //Pasingan berupa internal ID, so convert dulu ke ACCID
            $acc1 = Coa1::find($acc1)->ACC1ID;
            $acc2 = Coa2::find($acc2)->ACC2ID;
            $acc3 = Coa3::find($acc3)->ACC3ID;
            $acc4 = Coa4::find($acc4)->ACC4ID;
            $acc5 = Coa5::find($acc5)->ACC5ID;
            $acc6 = Coa6::find($acc6)->ACC6ID;
        }
        if ($acc4 == '0') {
            if ($acc3 == '0') {
                if ($acc2 == '0') {
                    $text = $acc1;
                } else {
                    $text = $acc2;
                }
            } else {
                $text = $acc3;
            }
        } else {
            $text = $acc4;
        }
        if ($acc5 != '0') {
            $text.='.' . $acc5;
        }
        if ($acc6 != '0') {
            $text.='.' . $acc6;
        }
        return $text;
    }

    public static function coaAccID() {
        return Coa::select(DB::raw('CONCAT(ACC1InternalID,".",ACC2InternalID,".",ACC3InternalID,".",ACC4InternalID,".",ACC5InternalID,".",ACC6InternalID) as accID'))
                        ->where('CompanyInternalID', Auth::user()->Company->InternalID)
                        ->get();
    }

    public static function getInternalID($acc1, $acc2, $acc3, $acc4, $acc5, $acc6) {
        $internal = Coa::where('ACC1InternalID', $acc1)
                ->where('ACC2InternalID', $acc2)
                ->where('ACC3InternalID', $acc3)
                ->where('ACC4InternalID', $acc4)
                ->where('ACC5InternalID', $acc5)
                ->where('ACC6InternalID', $acc6)
                ->pluck('InternalID');
        return $internal;
    }

    public static function getCoaNotSlip() {
        $slipCoa = Slip::select(DB::raw('CONCAT(ACC1InternalID,".",ACC2InternalID,".",ACC3InternalID,".",'
                                . 'ACC4InternalID,".",ACC5InternalID,".",ACC6InternalID) as slipCOA'))
                ->where('CompanyInternalID', Auth::user()->Company->InternalID)
                ->get();
        $textSlip = array();
        array_push($textSlip, "0");
        foreach ($slipCoa as $data) {
            array_push($textSlip, $data->slipCOA);
        }
        return Coa::whereNotIn(DB::raw('CONCAT(m_coa.ACC1InternalID,".",m_coa.ACC2InternalID,".",m_coa.ACC3InternalID,".",'
                                        . 'm_coa.ACC4InternalID,".",m_coa.ACC5InternalID,".",m_coa.ACC6InternalID)'), $textSlip)
                        ->where('CompanyInternalID', Auth::user()->Company->InternalID)
                        ->get();
    }

    public static function cekCOA($acc1,$acc2,$acc3,$acc4,$acc5,$acc6,$acc1D,$acc2D,$acc3D,$acc4D,$acc5D,$acc6D){
        if($acc1 == $acc1D && $acc2 == $acc2D && $acc3 == $acc3D && $acc4 == $acc4D && $acc5 == $acc5D && $acc6 == $acc6D){
            return 'selected';
        }
        return '';
    }
    
    public static function getJournalDebetValue($header1,$header2,$header3,$date) {
        $result = DB::select(DB::raw('select SUM(td.JournalDebet) as sumJ '
                                . 'from `m_coa` c '
                                . 'inner join `t_journal_detail` td on td.`ACC1InternalID` = c.`ACC1InternalID` '
                                . 'AND td.`ACC2InternalID` = c.`ACC2InternalID` '
                                . 'AND td.`ACC3InternalID` = c.`ACC3InternalID` '
                                . 'AND td.`ACC4InternalID` = c.`ACC4InternalID` '
                                . 'AND td.`ACC5InternalID` = c.`ACC5InternalID` '
                                . 'AND td.`ACC6InternalID` = c.`ACC6InternalID` '
                                . 'inner join `t_journal_header` th on th.`InternalID` = td.`JournalInternalID` '
                                . 'WHERE th.`CompanyInternalID` = ' . Auth::user()->Company->InternalID . ' '
                                . 'AND c.Header1 = "'.$header1.'" AND c.Header2 = "'.$header2.'" AND c.Header3 = "'.$header3.'" '
                                . 'AND th.JournalDate < "'.$date.'"'));
        return $result[0]->sumJ;
    }
    
    public static function getJournalCreditValue($header1,$header2,$header3,$date) {
        $result = DB::select(DB::raw('select SUM(td.JournalCredit) as sumJ '
                                . 'from `m_coa` c ' 
                                . 'inner join `t_journal_detail` td on td.`ACC1InternalID` = c.`ACC1InternalID` '
                                . 'AND td.`ACC2InternalID` = c.`ACC2InternalID` '
                                . 'AND td.`ACC3InternalID` = c.`ACC3InternalID` '
                                . 'AND td.`ACC4InternalID` = c.`ACC4InternalID` '
                                . 'AND td.`ACC5InternalID` = c.`ACC5InternalID` '
                                . 'AND td.`ACC6InternalID` = c.`ACC6InternalID` '
                                . 'inner join `t_journal_header` th on th.`InternalID` = td.`JournalInternalID` '
                                . 'WHERE th.`CompanyInternalID` = ' . Auth::user()->Company->InternalID . ' '
                                . 'AND c.Header1 = "'.$header1.'" AND c.Header2 = "'.$header2.'" AND c.Header3 = "'.$header3.'" '
                                . 'AND th.JournalDate < "'.$date.'"'));
        return $result[0]->sumJ;
    }
    
    
    public static function getJournalDebetValueProfit($header1,$header2,$header3,$date) {
        $result = DB::select(DB::raw('select SUM(td.JournalDebet) as sumJ '
                                . 'from `m_coa` c '
                                . 'inner join `t_journal_detail` td on td.`ACC1InternalID` = c.`ACC1InternalID` '
                                . 'AND td.`ACC2InternalID` = c.`ACC2InternalID` '
                                . 'AND td.`ACC3InternalID` = c.`ACC3InternalID` '
                                . 'AND td.`ACC4InternalID` = c.`ACC4InternalID` '
                                . 'AND td.`ACC5InternalID` = c.`ACC5InternalID` '
                                . 'AND td.`ACC6InternalID` = c.`ACC6InternalID` '
                                . 'inner join `t_journal_header` th on th.`InternalID` = td.`JournalInternalID` '
                                . 'WHERE th.`CompanyInternalID` = ' . Auth::user()->Company->InternalID . ' '
                                . 'AND c.Header1 = "'.$header1.'" AND c.Header2 = "'.$header2.'" AND c.Header3 = "'.$header3.'" '
                                . 'AND th.JournalDate < "'.$date.'" '
                                . 'AND th.TransactionID != "Journal Closing" '
                                . 'AND th.TransactionID != "Journal Closing Reverse" '));
        return $result[0]->sumJ;
    }
    
    public static function getJournalCreditValueProfit($header1,$header2,$header3,$date) {
        $result = DB::select(DB::raw('select SUM(td.JournalCredit) as sumJ '
                                . 'from `m_coa` c ' 
                                . 'inner join `t_journal_detail` td on td.`ACC1InternalID` = c.`ACC1InternalID` '
                                . 'AND td.`ACC2InternalID` = c.`ACC2InternalID` '
                                . 'AND td.`ACC3InternalID` = c.`ACC3InternalID` '
                                . 'AND td.`ACC4InternalID` = c.`ACC4InternalID` '
                                . 'AND td.`ACC5InternalID` = c.`ACC5InternalID` '
                                . 'AND td.`ACC6InternalID` = c.`ACC6InternalID` '
                                . 'inner join `t_journal_header` th on th.`InternalID` = td.`JournalInternalID` '
                                . 'WHERE th.`CompanyInternalID` = ' . Auth::user()->Company->InternalID . ' '
                                . 'AND c.Header1 = "'.$header1.'" AND c.Header2 = "'.$header2.'" AND c.Header3 = "'.$header3.'" '
                                . 'AND th.JournalDate < "'.$date.'" '
                                . 'AND th.TransactionID != "Journal Closing" '
                                . 'AND th.TransactionID != "Journal Closing Reverse" '));
        return $result[0]->sumJ;
    }
    
    public function coa1() {
        return $this->belongsTo('Coa1', 'ACC1InternalID', 'InternalID');
    }

    public function coa2() {
        return $this->belongsTo('Coa2', 'ACC2InternalID', 'InternalID');
    }

    public function coa3() {
        return $this->belongsTo('Coa3', 'ACC3InternalID', 'InternalID');
    }

    public function coa4() {
        return $this->belongsTo('Coa4', 'ACC4InternalID', 'InternalID');
    }

    public function coa5() {
        return $this->belongsTo('Coa5', 'ACC5InternalID', 'InternalID');
    }

    public function coa6() {
        return $this->belongsTo('Coa6', 'ACC6InternalID', 'InternalID');
    }

}
