<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Coa1 extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_coa1';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';


    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showCoa1() {
        return Coa1::all();
    }

    public static function idCoa1() {
        return Coa1::select('InternalID', 'ACC1ID', 'ACC1Name')
                ->where('CompanyInternalID',Auth::user()->Company->InternalID)
                ->orderBy('ACC1ID','asc')
                ->get();
    }

    public function coa() {
        return $this->hasMany('Coa', 'ACC1InternalID', 'InternalID');
    }

    public function journalDetail() {
        return $this->hasMany('JournalDetail', 'ACC1InternalID', 'InternalID');
    }

}
