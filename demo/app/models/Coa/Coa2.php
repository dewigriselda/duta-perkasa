<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Coa2 extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_coa2';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showCoa2() {
        return Coa2::all();
    }

    public static function idCoa2() {
        return Coa2::select('InternalID', 'ACC2ID', 'ACC2Name')
                ->where('InternalID','!=','0')
                ->where('CompanyInternalID',Auth::user()->Company->InternalID)
                ->orderBy('ACC2ID','asc')
                ->get();
    }

    public static function coa2inCoa1($ACC1ID) {
        $results = DB::select(DB::raw("Select * From m_coa2 "
                                . "where LEFT(ACC2ID," . strlen($ACC1ID) . ") = '" . $ACC1ID . "'"
                                . "AND CompanyInternalID = '".Auth::user()->Company->InternalID."'"
                                . "order by ACC2ID asc"));
        return $results;
    }

    public function coa() {
        return $this->hasMany('Coa', 'ACC2InternalID', 'InternalID');
    }
    
    public function journalDetail() {
        return $this->hasMany('JournalDetail', 'ACC2InternalID', 'InternalID');
    }
}
