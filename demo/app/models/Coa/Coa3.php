<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Coa3 extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_coa3';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showCoa3() {
        return Coa3::all();
    }

    public static function idCoa3() {
        return Coa3::select('InternalID', 'ACC3ID', 'ACC3Name')
                ->where('InternalID','!=','0')
                ->where('CompanyInternalID',Auth::user()->Company->InternalID)
                ->orderBy('ACC3ID','asc')
                ->get();
    }
    
    public static function coa3inCoa2($ACC2ID) {
        $results = DB::select(DB::raw("Select * From m_coa3 "
                                . "where LEFT(ACC3ID," . strlen($ACC2ID) . ") = '" . $ACC2ID . "'"
                                . "AND CompanyInternalID = '".Auth::user()->Company->InternalID."'"
                                . "order by ACC3ID asc"));
        return $results;
    }

    public function coa() {
        return $this->hasMany('Coa', 'ACC3InternalID', 'InternalID');
    }
    
    public function journalDetail() {
        return $this->hasMany('JournalDetail', 'ACC3InternalID', 'InternalID');
    }
}
