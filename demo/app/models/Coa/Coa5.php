<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
class Coa5 extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_coa5';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';
    public static function showCoa5() {
        return Coa5::all();
    }

    public static function idCoa5() {
        return Coa5::select('InternalID', 'ACC5ID', 'ACC5Name')->where('CompanyInternalID',Auth::user()->Company->InternalID);
    }
    
    public function coa() {
        return $this->hasMany('Coa', 'ACC5InternalID', 'InternalID');
    }

    public function journalHeader() {
        return $this->hasMany('JournalHeader', 'ACC5InternalID', 'InternalID');
    }

    public function journalDetail() {
        return $this->hasMany('JournalDetail', 'ACC5InternalID', 'InternalID');
    }

}
