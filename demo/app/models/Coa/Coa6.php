<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Coa6 extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_coa6';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showCoa6() {
        return Coa6::all();
    }

    public static function idCoa6() {
        return Coa6::select('InternalID', 'ACC6ID', 'ACC6Name')->where('CompanyInternalID', Auth::user()->Company->InternalID);
    }

    public static function maxID($tipe) {
        if ($tipe == 'C') {
            $data = Coa6::
                    orderBy('InternalID', 'desc')
                    ->where('Type', "c")
                    ->where('CompanyInternalID', Auth::user()->Company->InternalID)
                    ->first();
        } else {
            $data = Coa6::
                    orderBy('InternalID', 'desc')
                    ->where('Type', "s")
                    ->where('CompanyInternalID', Auth::user()->Company->InternalID)
                    ->first();
        }
        if (is_null($data)) {
            $value = 0;
        } else {
            $value = substr($data->ACC6ID, 1);
        }
        $value += 1;
        return str_pad($value, 6, '0', STR_PAD_LEFT);
    }

    public static function customerTop10Order() {
        $id = array();
        $nilai = array();
        $customer = Coa6::where('Type', "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get();
        $text = '';
        $count = 0;
        $hasil = array();
        foreach ($customer as $data) {
            $sales10 = SalesOrderHeader::getSalesOrder10($data->InternalID);
            $id[$count] = $data->InternalID;
            $nilai[$count] = $sales10[0]->hasil;
            $count++;
        }
        arsort($nilai);
        $count = 0;
        foreach ($nilai as $key => $value) {
            if ($value == '') {
                $value = 0;
            }
            if ($count < 10) {
                $hasil[$count] = $value . '---;---' . $id[$key];
            }
            $count++;
        }
        return $hasil;
    }

    public static function supplierTop10Order() {
        $id = array();
        $nilai = array();
        $supplier = Coa6::where('Type', "s")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get();
        $text = '';
        $count = 0;
        $hasil = array();
        foreach ($supplier as $data) {
            $purchase10 = PurchaseOrderHeader::getPurchaseOrder10($data->InternalID);
            $id[$count] = $data->InternalID;
            $nilai[$count] = $purchase10[0]->hasil;
            $count++;
        }
        arsort($nilai);
        $count = 0;
        foreach ($nilai as $key => $value) {
            if ($value == '') {
                $value = 0;
            }
            if ($count < 10) {
                $hasil[$count] = $value . '---;---' . $id[$key];
            }
            $count++;
        }
        return $hasil;
    }

    public static function customerTop10() {
        $id = array();
        $nilai = array();
        $customer = Coa6::where('Type', "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get();
        $text = '';
        $count = 0;
        $hasil = array();
        foreach ($customer as $data) {
            $sales10 = SalesHeader::getSales10($data->InternalID);
            $id[$count] = $data->InternalID;
            $nilai[$count] = $sales10[0]->hasil;
            $count++;
        }
        arsort($nilai);
        $count = 0;
        foreach ($nilai as $key => $value) {
            if ($value == '') {
                $value = 0;
            }
            if ($count < 10) {
                $hasil[$count] = $value . '---;---' . $id[$key];
            }
            $count++;
        }
        return $hasil;
    }

    public static function supplierTop10() {
        $id = array();
        $nilai = array();
        $supplier = Coa6::where('Type', "s")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get();
        $text = '';
        $count = 0;
        $hasil = array();
        foreach ($supplier as $data) {
            $purchase10 = PurchaseHeader::getPurchase10($data->InternalID);
            $id[$count] = $data->InternalID;
            $nilai[$count] = $purchase10[0]->hasil;
            $count++;
        }
        arsort($nilai);
        $count = 0;
        foreach ($nilai as $key => $value) {
            if ($value == '') {
                $value = 0;
            }
            if ($count < 10) {
                $hasil[$count] = $value . '---;---' . $id[$key];
            }
            $count++;
        }
        return $hasil;
    }

    public static function customerTop10Return() {
        $id = array();
        $nilai = array();
        $customer = Coa6::where('Type', "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get();
        $text = '';
        $count = 0;
        $hasil = array();
        foreach ($customer as $data) {
            $sales10 = SalesReturnHeader::getSalesReturn10($data->InternalID);
            $id[$count] = $data->InternalID;
            $nilai[$count] = $sales10[0]->hasil;
            $count++;
        }
        arsort($nilai);
        $count = 0;
        foreach ($nilai as $key => $value) {
            if ($value == '') {
                $value = 0;
            }
            if ($count < 10) {
                $hasil[$count] = $value . '---;---' . $id[$key];
            }
            $count++;
        }
        return $hasil;
    }

    public static function supplierTop10Return() {
        $id = array();
        $nilai = array();
        $supplier = Coa6::where('Type', "s")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get();
        $text = '';
        $count = 0;
        $hasil = array();
        foreach ($supplier as $data) {
            $purchase10 = PurchaseReturnHeader::getPurchaseReturn10($data->InternalID);
            $id[$count] = $data->InternalID;
            $nilai[$count] = $purchase10[0]->hasil;
            $count++;
        }
        arsort($nilai);
        $count = 0;
        foreach ($nilai as $key => $value) {
            if ($value == '') {
                $value = 0;
            }
            if ($count < 10) {
                $hasil[$count] = $value . '---;---' . $id[$key];
            }
            $count++;
        }
        return $hasil;
    }

    public function coa() {
        return $this->hasMany('Coa', 'ACC6InternalID', 'InternalID');
    }

    public function journalDetail() {
        return $this->hasMany('JournalDetail', 'ACC6InternalID', 'InternalID');
    }

    public function salesHeader() {
        return $this->hasMany('SalesHeader', 'ACC6InternalID', 'InternalID');
    }

}
