<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Currency extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_currency';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';


    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showCurrency() {
        return Currency::all();
    }
    
    public function slip() {
        return $this->hasMany('Slip', 'CurrencyInternalID', 'InternalID');
    }
    
    public function journalDetail() {
        return $this->hasMany('JournalDetail', 'CurrencyInternalID', 'InternalID');
    }

    public function salesHeader() {
        return $this->hasMany('SalesHeader', 'CurrencyInternalID', 'InternalID');
    }
    
    public function region() {
        return $this->hasMany('Region', 'CurrencyInternalID', 'InternalID');
    }
}
