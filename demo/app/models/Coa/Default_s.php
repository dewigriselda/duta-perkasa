<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Default_s extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 's_default';
    public $timestamps = false;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showDefault() {
        return Default_s::all();
    }

    public static function getInternalCoa($name) {
        $default = Default_s::select(DB::raw('ACC1InternalID,ACC2InternalID,ACC3InternalID,ACC4InternalID,ACC5InternalID,ACC6InternalID'))
                ->where('DefaultID', '=', $name)
                ->where('CompanyInternalID', Auth::user()->CompanyInternalID)
                ->first();
        return $default;
    }

    public static function getACCInternalID($defaultID, $cari) {
        if ($cari == 1) {
            $result = Default_s::where("CompanyInternalID", Auth::user()->Company->InternalID)
                            ->where('DefaultID', $defaultID)->first()->ACC1InternalID;
        }
        if ($cari == 2) {
            $result = Default_s::where("CompanyInternalID", Auth::user()->Company->InternalID)
                            ->where('DefaultID', $defaultID)->first()->ACC2InternalID;
        }
        if ($cari == 3) {
            $result = Default_s::where("CompanyInternalID", Auth::user()->Company->InternalID)
                            ->where('DefaultID', $defaultID)->first()->ACC3InternalID;
        }
        if ($cari == 4) {
            $result = Default_s::where("CompanyInternalID", Auth::user()->Company->InternalID)
                            ->where('DefaultID', $defaultID)->first()->ACC4InternalID;
        }
        if ($cari == 5) {
            $result = Default_s::where("CompanyInternalID", Auth::user()->Company->InternalID)
                            ->where('DefaultID', $defaultID)->first()->ACC5InternalID;
        }
        if ($cari == 6) {
            $result = Default_s::where("CompanyInternalID", Auth::user()->Company->InternalID)
                            ->where('DefaultID', $defaultID)->first()->ACC6InternalID;
        }
        return $result;
    }

}
