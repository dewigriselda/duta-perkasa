<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class DepreciationHeader extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_depreciation_header';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showDepreciationHeader() {
        return DepreciationHeader::all();
    }

    public static function getIddepreciation($depreciationID) {
        $internalID = DepreciationHeader::where('DepreciationID', '=', $depreciationID)->where('CompanyInternalID', Auth::user()->Company->InternalID)->pluck('InternalID');
        return $internalID;
    }

    public static function coaDebet($idGroupDepreciation) {
        $result = DB::select(DB::raw('select `m_depreciation_header`.*, `m_coa`.`COAName`, `m_coa`.`InternalID` as COAInternalID '
                                . 'from `m_depreciation_header` '
                                . 'inner join `m_coa` on `m_depreciation_header`.`ACC1InternalIDDebet` = `m_coa`.`ACC1InternalID` '
                                . 'AND `m_depreciation_header`.`ACC2InternalIDDebet` = `m_coa`.`ACC2InternalID` '
                                . 'AND `m_depreciation_header`.`ACC3InternalIDDebet` = `m_coa`.`ACC3InternalID` '
                                . 'AND `m_depreciation_header`.`ACC4InternalIDDebet` = `m_coa`.`ACC4InternalID` '
                                . 'AND `m_depreciation_header`.`ACC5InternalIDDebet` = `m_coa`.`ACC5InternalID` '
                                . 'AND `m_depreciation_header`.`ACC6InternalIDDebet` = `m_coa`.`ACC6InternalID` '
                                . 'where `m_depreciation_header`.`InternalID` = ' . $idGroupDepreciation . ' '
                                . 'AND `m_depreciation_header`.`CompanyInternalID` = ' . Auth::user()->Company->InternalID . ' '));
        return $result;
    }

    public static function coaCredit($idGroupDepreciation) {
        $result = DB::select(DB::raw('select `m_depreciation_header`.*, `m_coa`.`COAName`, `m_coa`.`InternalID` as COAInternalID '
                                . 'from `m_depreciation_header` '
                                . 'inner join `m_coa` on `m_depreciation_header`.`ACC1InternalIDCredit` = `m_coa`.`ACC1InternalID` '
                                . 'AND `m_depreciation_header`.`ACC2InternalIDCredit` = `m_coa`.`ACC2InternalID` '
                                . 'AND `m_depreciation_header`.`ACC3InternalIDCredit` = `m_coa`.`ACC3InternalID` '
                                . 'AND `m_depreciation_header`.`ACC4InternalIDCredit` = `m_coa`.`ACC4InternalID` '
                                . 'AND `m_depreciation_header`.`ACC5InternalIDCredit` = `m_coa`.`ACC5InternalID` '
                                . 'AND `m_depreciation_header`.`ACC6InternalIDCredit` = `m_coa`.`ACC6InternalID` '
                                . 'where `m_depreciation_header`.`InternalID` = ' . $idGroupDepreciation . ' '
                                . 'AND `m_depreciation_header`.`CompanyInternalID` = ' . Auth::user()->Company->InternalID . ' '));
        return $result;
    }
    
    public static function getYearMin(){
        $result = DepreciationHeader::orderBy('DepreciationDate')->select(DB::raw('YEAR(DepreciationDate) as DepreciationDates'))->where("CompanyInternalID",Auth::User()->CompanyInternalID)->first();
        if($result == ''){
            return '0';
        }
        return $result->DepreciationDates;
    }

    public function depreciationDetail() {
        return $this->hasMany('DepreciationDetail', 'DepreciationInternalID', 'InternalID');
    }

    public function groupDepreciation() {
        return $this->belongsTo('GroupDepreciation', 'GroupDepreciationInternalID', 'InternalID');
    }

}
