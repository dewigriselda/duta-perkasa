<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class JournalDetail extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_journal_detail';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showJournalDetail() {
        return JournalDetail::all();
    }

    public static function reportSlipMU($slip, $start, $end, $default) {
        if ($default == 0) {
            return JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                            ->where('t_journal_detail.ACC1InternalID', $slip->ACC1InternalID)
                            ->where('t_journal_detail.ACC2InternalID', $slip->ACC2InternalID)
                            ->where('t_journal_detail.ACC3InternalID', $slip->ACC3InternalID)
                            ->where('t_journal_detail.ACC4InternalID', $slip->ACC4InternalID)
                            ->where('t_journal_detail.ACC5InternalID', $slip->ACC5InternalID)
                            ->where('t_journal_detail.ACC6InternalID', $slip->ACC6InternalID)
                            ->where('t_journal_detail.CurrencyInternalID', $slip->CurrencyInternalID)
                            ->where('t_journal_header.CompanyInternalID', Auth::user()->Company->InternalID)
                            ->whereBetween('t_journal_header.JournalDate', Array($start, $end))
                            ->orderBy('t_journal_header.JournalDate')->get();
        } else {
            return JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                            ->where('t_journal_detail.ACC1InternalID', $slip->ACC1InternalID)
                            ->where('t_journal_detail.ACC2InternalID', $slip->ACC2InternalID)
                            ->where('t_journal_detail.ACC3InternalID', $slip->ACC3InternalID)
                            ->where('t_journal_detail.ACC4InternalID', $slip->ACC4InternalID)
                            ->where('t_journal_detail.ACC5InternalID', $slip->ACC5InternalID)
                            ->where('t_journal_detail.ACC6InternalID', $slip->ACC6InternalID)
                            ->where('t_journal_header.CompanyInternalID', Auth::user()->Company->InternalID)
                            ->whereBetween('t_journal_header.JournalDate', Array($start, $end))
                            ->orderBy('t_journal_header.JournalDate')->get();
        }
    }

    public static function reportSlipInitialDebet($slip, $date, $default) {
        if ($default == 0) {
            return JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                            ->where('t_journal_detail.ACC1InternalID', $slip->ACC1InternalID)
                            ->where('t_journal_detail.ACC2InternalID', $slip->ACC2InternalID)
                            ->where('t_journal_detail.ACC3InternalID', $slip->ACC3InternalID)
                            ->where('t_journal_detail.ACC4InternalID', $slip->ACC4InternalID)
                            ->where('t_journal_detail.ACC5InternalID', $slip->ACC5InternalID)
                            ->where('t_journal_detail.ACC6InternalID', $slip->ACC6InternalID)
                            ->where('t_journal_detail.CurrencyInternalID', $slip->CurrencyInternalID)
                            ->where('t_journal_header.CompanyInternalID', Auth::user()->Company->InternalID)
                            ->where('t_journal_header.JournalDate', '<', $date)
                            ->sum('JournalDebetMU');
        } else {
            return JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                            ->where('t_journal_detail.ACC1InternalID', $slip->ACC1InternalID)
                            ->where('t_journal_detail.ACC2InternalID', $slip->ACC2InternalID)
                            ->where('t_journal_detail.ACC3InternalID', $slip->ACC3InternalID)
                            ->where('t_journal_detail.ACC4InternalID', $slip->ACC4InternalID)
                            ->where('t_journal_detail.ACC5InternalID', $slip->ACC5InternalID)
                            ->where('t_journal_detail.ACC6InternalID', $slip->ACC6InternalID)
                            ->where('t_journal_header.CompanyInternalID', Auth::user()->Company->InternalID)
                            ->where('t_journal_header.JournalDate', '<', $date)
                            ->sum('JournalDebet');
        }
    }

    public static function reportSlipInitialCredit($slip, $date, $default) {
        if ($default == 0) {
            return JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                            ->where('t_journal_detail.ACC1InternalID', $slip->ACC1InternalID)
                            ->where('t_journal_detail.ACC2InternalID', $slip->ACC2InternalID)
                            ->where('t_journal_detail.ACC3InternalID', $slip->ACC3InternalID)
                            ->where('t_journal_detail.ACC4InternalID', $slip->ACC4InternalID)
                            ->where('t_journal_detail.ACC5InternalID', $slip->ACC5InternalID)
                            ->where('t_journal_detail.ACC6InternalID', $slip->ACC6InternalID)
                            ->where('t_journal_detail.CurrencyInternalID', $slip->CurrencyInternalID)
                            ->where('t_journal_header.CompanyInternalID', Auth::user()->Company->InternalID)
                            ->where('t_journal_header.JournalDate', '<', $date)
                            ->sum('JournalCreditMU');
        } else {
            return JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                            ->where('t_journal_detail.ACC1InternalID', $slip->ACC1InternalID)
                            ->where('t_journal_detail.ACC2InternalID', $slip->ACC2InternalID)
                            ->where('t_journal_detail.ACC3InternalID', $slip->ACC3InternalID)
                            ->where('t_journal_detail.ACC4InternalID', $slip->ACC4InternalID)
                            ->where('t_journal_detail.ACC5InternalID', $slip->ACC5InternalID)
                            ->where('t_journal_detail.ACC6InternalID', $slip->ACC6InternalID)
                            ->where('t_journal_header.CompanyInternalID', Auth::user()->Company->InternalID)
                            ->where('t_journal_header.JournalDate', '<', $date)
                            ->sum('JournalCredit');
        }
    }

    public static function getJournalDebet($coa, $month, $year) {
        return JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                        ->where('t_journal_detail.ACC1InternalID', $coa->ACC1InternalID)
                        ->where('t_journal_detail.ACC2InternalID', $coa->ACC2InternalID)
                        ->where('t_journal_detail.ACC3InternalID', $coa->ACC3InternalID)
                        ->where('t_journal_detail.ACC4InternalID', $coa->ACC4InternalID)
                        ->where('t_journal_detail.ACC5InternalID', $coa->ACC5InternalID)
                        ->where('t_journal_detail.ACC6InternalID', $coa->ACC6InternalID)
                        ->where('t_journal_header.CompanyInternalID', Auth::user()->Company->InternalID)
                        ->whereRaw('YEAR(t_journal_header.JournalDate) = ' . $year . ' AND MONTH(t_journal_header.JournalDate) = ' . $month . ' ')
                        ->sum("t_journal_detail.JournalDebet");
    }

    public static function getJournalCredit($coa, $month, $year) {
        return JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                        ->where('t_journal_detail.ACC1InternalID', $coa->ACC1InternalID)
                        ->where('t_journal_detail.ACC2InternalID', $coa->ACC2InternalID)
                        ->where('t_journal_detail.ACC3InternalID', $coa->ACC3InternalID)
                        ->where('t_journal_detail.ACC4InternalID', $coa->ACC4InternalID)
                        ->where('t_journal_detail.ACC5InternalID', $coa->ACC5InternalID)
                        ->where('t_journal_detail.ACC6InternalID', $coa->ACC6InternalID)
                        ->where('t_journal_header.CompanyInternalID', Auth::user()->Company->InternalID)
                        ->whereRaw('YEAR(t_journal_header.JournalDate) = ' . $year . ' AND MONTH(t_journal_header.JournalDate) = ' . $month . ' ')
                        ->sum("t_journal_detail.JournalCredit");
    }

    public static function getJournalDebetGeneral($coa, $month, $year) {
        return JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                        ->where('t_journal_detail.ACC1InternalID', $coa->ACC1InternalID)
                        ->where('t_journal_detail.ACC2InternalID', $coa->ACC2InternalID)
                        ->where('t_journal_detail.ACC3InternalID', $coa->ACC3InternalID)
                        ->where('t_journal_detail.ACC4InternalID', $coa->ACC4InternalID)
                        ->where('t_journal_detail.ACC5InternalID', $coa->ACC5InternalID)
                        ->where('t_journal_detail.ACC6InternalID', $coa->ACC6InternalID)
                        ->where('t_journal_header.CompanyInternalID', Auth::user()->Company->InternalID)
                        ->whereRaw('YEAR(t_journal_header.JournalDate) = ' . $year . ' '
                                . 'AND MONTH(t_journal_header.JournalDate) = ' . $month . ' '
                                . 'AND t_journal_header.TransactionID != "Journal Closing" '
                                . 'AND t_journal_header.TransactionID != "Journal Closing Reverse" ')
                        ->sum("t_journal_detail.JournalDebet");
    }

    public static function getJournalCreditGeneral($coa, $month, $year) {
        return JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                        ->where('t_journal_detail.ACC1InternalID', $coa->ACC1InternalID)
                        ->where('t_journal_detail.ACC2InternalID', $coa->ACC2InternalID)
                        ->where('t_journal_detail.ACC3InternalID', $coa->ACC3InternalID)
                        ->where('t_journal_detail.ACC4InternalID', $coa->ACC4InternalID)
                        ->where('t_journal_detail.ACC5InternalID', $coa->ACC5InternalID)
                        ->where('t_journal_detail.ACC6InternalID', $coa->ACC6InternalID)
                        ->where('t_journal_header.CompanyInternalID', Auth::user()->Company->InternalID)
                        ->whereRaw('YEAR(t_journal_header.JournalDate) = ' . $year . ' '
                                . 'AND MONTH(t_journal_header.JournalDate) = ' . $month . ' '
                                . 'AND t_journal_header.TransactionID != "Journal Closing" '
                                . 'AND t_journal_header.TransactionID != "Journal Closing Reverse" ')
                        ->sum("t_journal_detail.JournalCredit");
    }

    public static function getJournalInitialBalance($coa, $month, $year) {
        $date = $year . '-' . $month . '-01';
        $debet = JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                ->where('t_journal_detail.ACC1InternalID', $coa->ACC1InternalID)
                ->where('t_journal_detail.ACC2InternalID', $coa->ACC2InternalID)
                ->where('t_journal_detail.ACC3InternalID', $coa->ACC3InternalID)
                ->where('t_journal_detail.ACC4InternalID', $coa->ACC4InternalID)
                ->where('t_journal_detail.ACC5InternalID', $coa->ACC5InternalID)
                ->where('t_journal_detail.ACC6InternalID', $coa->ACC6InternalID)
                ->where('t_journal_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->whereRaw('t_journal_header.JournalDate < "' . $date . '"')
                ->sum("t_journal_detail.JournalDebet");
        $credit = JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                ->where('t_journal_detail.ACC1InternalID', $coa->ACC1InternalID)
                ->where('t_journal_detail.ACC2InternalID', $coa->ACC2InternalID)
                ->where('t_journal_detail.ACC3InternalID', $coa->ACC3InternalID)
                ->where('t_journal_detail.ACC4InternalID', $coa->ACC4InternalID)
                ->where('t_journal_detail.ACC5InternalID', $coa->ACC5InternalID)
                ->where('t_journal_detail.ACC6InternalID', $coa->ACC6InternalID)
                ->where('t_journal_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->whereRaw('t_journal_header.JournalDate < "' . $date . '"')
                ->sum("t_journal_detail.JournalCredit");
        $hasil = ($debet - $credit) + $coa->InitialBalance;
        return $hasil;
    }

      public static function getJournalInitialBalanceGeneral($coa, $month, $year) {
        $date = $year . '-' . $month . '-01';
        $debet = JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                ->where('t_journal_detail.ACC1InternalID', $coa->ACC1InternalID)
                ->where('t_journal_detail.ACC2InternalID', $coa->ACC2InternalID)
                ->where('t_journal_detail.ACC3InternalID', $coa->ACC3InternalID)
                ->where('t_journal_detail.ACC4InternalID', $coa->ACC4InternalID)
                ->where('t_journal_detail.ACC5InternalID', $coa->ACC5InternalID)
                ->where('t_journal_detail.ACC6InternalID', $coa->ACC6InternalID)
                ->where('t_journal_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->whereRaw('t_journal_header.JournalDate < "' . $date . '" '
                                . 'AND t_journal_header.TransactionID != "Journal Closing" '
                                . 'AND t_journal_header.TransactionID != "Journal Closing Reverse" ')
                ->sum("t_journal_detail.JournalDebet");
        $credit = JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                ->where('t_journal_detail.ACC1InternalID', $coa->ACC1InternalID)
                ->where('t_journal_detail.ACC2InternalID', $coa->ACC2InternalID)
                ->where('t_journal_detail.ACC3InternalID', $coa->ACC3InternalID)
                ->where('t_journal_detail.ACC4InternalID', $coa->ACC4InternalID)
                ->where('t_journal_detail.ACC5InternalID', $coa->ACC5InternalID)
                ->where('t_journal_detail.ACC6InternalID', $coa->ACC6InternalID)
                ->where('t_journal_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->whereRaw('t_journal_header.JournalDate < "' . $date . '" '
                                . 'AND t_journal_header.TransactionID != "Journal Closing" '
                                . 'AND t_journal_header.TransactionID != "Journal Closing Reverse" ')
                ->sum("t_journal_detail.JournalCredit");
        $hasil = ($debet - $credit) + $coa->InitialBalance;
        return $hasil;
    }

    public static function getJournalInitialBalanceSuperAdmin($coa, $company) {
        $debet = JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                ->where('t_journal_detail.ACC1InternalID', $coa->ACC1InternalID)
                ->where('t_journal_detail.ACC2InternalID', $coa->ACC2InternalID)
                ->where('t_journal_detail.ACC3InternalID', $coa->ACC3InternalID)
                ->where('t_journal_detail.ACC4InternalID', $coa->ACC4InternalID)
                ->where('t_journal_detail.ACC5InternalID', $coa->ACC5InternalID)
                ->where('t_journal_detail.ACC6InternalID', $coa->ACC6InternalID)
                ->where('t_journal_header.CompanyInternalID', $company)
                ->sum("t_journal_detail.JournalDebet");
        $credit = JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                ->where('t_journal_detail.ACC1InternalID', $coa->ACC1InternalID)
                ->where('t_journal_detail.ACC2InternalID', $coa->ACC2InternalID)
                ->where('t_journal_detail.ACC3InternalID', $coa->ACC3InternalID)
                ->where('t_journal_detail.ACC4InternalID', $coa->ACC4InternalID)
                ->where('t_journal_detail.ACC5InternalID', $coa->ACC5InternalID)
                ->where('t_journal_detail.ACC6InternalID', $coa->ACC6InternalID)
                ->where('t_journal_header.CompanyInternalID', $company)
                ->sum("t_journal_detail.JournalCredit");
        $hasil = ($debet - $credit) + $coa->InitialBalance;
        return $hasil;
    }

    public function journalHeader() {
        return $this->belongsTo('JournalHeader', 'JournalInternalID', 'InternalID');
    }

    public function currency() {
        return $this->belongsTo('Currency', 'CurrencyInternalID', 'InternalID');
    }

    public function coa1() {
        return $this->belongsTo('Coa1', 'ACC1InternalID', 'InternalID');
    }

    public function coa2() {
        return $this->belongsTo('Coa2', 'ACC2InternalID', 'InternalID');
    }

    public function coa3() {
        return $this->belongsTo('Coa3', 'ACC3InternalID', 'InternalID');
    }

    public function coa4() {
        return $this->belongsTo('Coa4', 'ACC4InternalID', 'InternalID');
    }

    public function coa5() {
        return $this->belongsTo('Coa5', 'ACC5InternalID', 'InternalID');
    }

    public function coa6() {
        return $this->belongsTo('Coa6', 'ACC6InternalID', 'InternalID');
    }

}