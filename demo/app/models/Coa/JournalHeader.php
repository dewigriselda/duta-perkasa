<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class JournalHeader extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_journal_header';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showJournalHeader() {
        return JournalHeader::all();
    }

    public static function advancedSearch($slip, $jenis, $start, $end) {
        $where = '';
        if ($slip != '-1' && $slip != '') {
            $where .= 'SlipInternalID = "' . $slip . '" ';
        }
        if ($jenis != '-1' && $jenis != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'JournalType = "' . $jenis . '" ';
        }
        if ($start != '' && $end != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $start = explode('-', $start);
            $end = explode('-', $end);
            $startSearch = $start[2] . '-' . $start[1] . '-' . $start[0];
            $endSearch = $end[2] . '-' . $end[1] . '-' . $end[0];
            $where .= 'JournalDate between "' . $startSearch . ' 00:00:00" AND "' . $endSearch . ' 23:59:59"';
        }

        $query = 'SELECT th.*, d.DepartmentName, IFNULL(s.SlipName,"No Slip") as SlipName, s.SlipID, c.ACC5Name, c.ACC5ID, IFNULL(th.TransactionID,"No Transaction") as TransactionID '
                . 'FROM t_journal_header th '
                . 'INNER JOIN m_department d on d.InternalID = th.DepartmentInternalID '
                . 'LEFT JOIN m_slip s on s.InternalID = th.SlipInternalID '
                . 'INNER JOIN m_coa5 c on c.InternalID = th.ACC5InternalID';

        if ($where != '') {
            $query .= ' WHERE ' . $where . ' AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        } else {
            $query .= ' WHERE th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        }
        return $result = DB::select(DB::raw($query));
    }

    public static function getIdjournal($journalID) {
        $internalID = JournalHeader::where('JournalID', '=', $journalID)->where('CompanyInternalID', Auth::user()->Company->InternalID)->pluck('InternalID');
        return $internalID;
    }

    public static function getNextIDJournal($text) {
        $query = 'SELECT JournalID From t_journal_header Where JournalID LIKE "' . $text . '%" AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" order by JournalID desc';
        $journalID = DB::select(DB::raw($query));

        if (count($journalID) <= 0) {
            $journalID = '';
        } else {
            $journalID = $journalID[0]->JournalID;
        }

        if ($journalID == '') {
            $journalID = $text . '0001';
        } else {
            $textTamp = $journalID;
            $journalID = substr($textTamp, (strlen($textTamp) - 4), 4) + 1;
            $journalID = str_pad($journalID, 4, '0', STR_PAD_LEFT);
            $journalID = $text . $journalID;
        }
        return $journalID;
    }

    public static function getYearMin() {
        $result = JournalHeader::orderBy('JournalDate')->select(DB::raw('YEAR(JournalDate) as JournalDates'))->where("CompanyInternalID", Auth::User()->CompanyInternalID)->first();
        if($result == ''){
            return '0';
        }
        return $result->JournalDates;
    }

    public static function getDebetCOA($bulan,$tahun,$acc1,$acc2,$acc3,$acc4,$acc5,$acc6) {
        $result = JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                ->whereRaw('YEAR(JournalDate) = "' . $tahun . '" AND MONTH(JournalDate) = "' . $bulan . '"')
                ->where('t_journal_detail.ACC1InternalID', $acc1)
                ->where('t_journal_detail.ACC2InternalID', $acc2)
                ->where('t_journal_detail.ACC3InternalID', $acc3)
                ->where('t_journal_detail.ACC4InternalID', $acc4)
                ->where('t_journal_detail.ACC5InternalID', $acc5)
                ->where('t_journal_detail.ACC6InternalID', $acc6)
                ->where('t_journal_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                ->sum('t_journal_detail.JournalDebet');
        return $result;
    }

    public static function getCreditCOA($bulan,$tahun,$acc1,$acc2,$acc3,$acc4,$acc5,$acc6) {
        $result = JournalDetail::join('t_journal_header', 't_journal_header.InternalID', '=', 't_journal_detail.JournalInternalID')
                ->whereRaw('YEAR(JournalDate) = "' . $tahun . '" AND MONTH(JournalDate) = "' . $bulan . '"')
                ->where('t_journal_detail.ACC1InternalID', $acc1)
                ->where('t_journal_detail.ACC2InternalID', $acc2)
                ->where('t_journal_detail.ACC3InternalID', $acc3)
                ->where('t_journal_detail.ACC4InternalID', $acc4)
                ->where('t_journal_detail.ACC5InternalID', $acc5)
                ->where('t_journal_detail.ACC6InternalID', $acc6)
                ->where('t_journal_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                ->sum('t_journal_detail.JournalCredit');
        return $result;
    }

    public function journalDetail() {
        return $this->hasMany('JournalDetail', 'JournalInternalID', 'InternalID');
    }

    public function coa5() {
        return $this->belongsTo('Coa5', 'ACC5InternalID', 'InternalID');
    }

    public function slip() {
        return $this->belongsTo('Slip', 'SlipInternalID', 'InternalID');
    }

    public function department() {
        return $this->belongsTo('Department', 'DepartmentInternalID', 'InternalID');
    }

}
