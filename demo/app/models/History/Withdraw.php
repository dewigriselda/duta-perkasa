<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Withdraw extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'h_withdraw';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';
    
    public static function showWithdraw() {
        return Withdraw::all();
    }
    
    public static function getNextIDWithDraw($text, $AgentInternalID) {
        $query = 'SELECT WithdrawID From h_withdraw Where WithdrawID LIKE "' . $text . '%" AND AgentInternalID = "' . $AgentInternalID . '" order by WithdrawID desc';
        $withdrawID = DB::select(DB::raw($query));

        if (count($withdrawID) <= 0) {
            $withdrawID = '';
        } else {
            $withdrawID = $withdrawID[0]->WithdrawID;
        }

        if ($withdrawID == '') {
            $withdrawID = $text . '0001';
        } else {
            $textTamp = $withdrawID;
            $withdrawID = substr($textTamp, (strlen($textTamp) - 4), 4) + 1;
            $withdrawID = str_pad($withdrawID, 4, '0', STR_PAD_LEFT);
            $withdrawID = $text . $withdrawID;
        }
        return $withdrawID;
    }
    
    public function user() {
        return $this->belongsTo('user', 'AgentInternalID', 'InternalID');
    }
}