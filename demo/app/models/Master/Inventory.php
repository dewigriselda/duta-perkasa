<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Inventory extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_inventory';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showInventory() {
        return Inventory::all();
    }

    public static function getInitialStock($date, $if, $internal, $warehouse, $delimiterWarehouse) {
        $initial = Inventory::find($internal)->InitialQuantity;
        $pembelian = PurchaseDetail::
                join('t_purchase_header', 't_purchase_header.InternalID', '=', 't_purchase_detail.PurchaseInternalID')
                ->where('t_purchase_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_purchase_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                ->where('t_purchase_detail.InventoryInternalID', $internal)
                ->where('t_purchase_header.PurchaseDate', $if, $date)
                ->sum('Qty');
        $penjualan = SalesDetail::
                join('t_sales_header', 't_sales_header.InternalID', '=', 't_sales_detail.SalesInternalID')
                ->where('t_sales_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_sales_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                ->where('t_sales_detail.InventoryInternalID', $internal)
                ->where('t_sales_header.SalesDate', $if, $date)
                ->sum('Qty');
        $Rpembelian = PurchaseReturnDetail::
                join('t_purchasereturn_header', 't_purchasereturn_header.InternalID', '=', 't_purchasereturn_detail.PurchaseReturnInternalID')
                ->where('t_purchasereturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_purchasereturn_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                ->where('t_purchasereturn_detail.InventoryInternalID', $internal)
                ->where('t_purchasereturn_header.PurchaseReturnDate', $if, $date)
                ->sum('Qty');
        $Rpenjualan = SalesReturnDetail::
                join('t_salesreturn_header', 't_salesreturn_header.InternalID', '=', 't_salesreturn_detail.SalesReturnInternalID')
                ->where('t_salesreturn_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_salesreturn_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                ->where('t_salesreturn_detail.InventoryInternalID', $internal)
                ->where('t_salesreturn_header.SalesReturnDate', $if, $date)
                ->sum('Qty');
        $Min = MemoInDetail::
                join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.MemoInInternalID')
                ->where('t_memoin_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_memoin_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                ->where('t_memoin_detail.InventoryInternalID', $internal)
                ->where('t_memoin_header.MemoInDate', $if, $date)
                ->sum('Qty');
        $Mout = MemoOutDetail::
                join('t_memoout_header', 't_memoout_header.InternalID', '=', 't_memoout_detail.MemoOutInternalID')
                ->where('t_memoout_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_memoout_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                ->where('t_memoout_detail.InventoryInternalID', $internal)
                ->where('t_memoout_header.MemoOutDate', $if, $date)
                ->sum('Qty');
        $Tin = TransferDetail::
                join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transfer_header.WarehouseDestinyInternalID', $delimiterWarehouse, $warehouse)
                ->where('t_transfer_detail.InventoryInternalID', $internal)
                ->where('t_transfer_header.TransferDate', $if, $date)
                ->sum('Qty');
        $Tout = TransferDetail::
                join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                ->where('t_transfer_header.CompanyInternalID', Auth::user()->Company->InternalID)
                ->where('t_transfer_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                ->where('t_transfer_detail.InventoryInternalID', $internal)
                ->where('t_transfer_header.TransferDate', $if, $date)
                ->sum('Qty');
        return $pembelian + $Rpenjualan - $penjualan - $Rpembelian + $Min - $Mout + $Tin - $Tout + $initial;
    }
    
    public static function getInitialStockSuperAdmin($date, $if, $internal, $warehouse, $delimiterWarehouse, $company) {
        $initial = Inventory::find($internal)->InitialQuantity;
        $pembelian = PurchaseDetail::
                join('t_purchase_header', 't_purchase_header.InternalID', '=', 't_purchase_detail.PurchaseInternalID')
                ->where('t_purchase_header.CompanyInternalID', $company)
                ->where('t_purchase_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                ->where('t_purchase_detail.InventoryInternalID', $internal)
                ->where('t_purchase_header.PurchaseDate', $if, $date)
                ->sum('Qty');
        $penjualan = SalesDetail::
                join('t_sales_header', 't_sales_header.InternalID', '=', 't_sales_detail.SalesInternalID')
                ->where('t_sales_header.CompanyInternalID', $company)
                ->where('t_sales_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                ->where('t_sales_detail.InventoryInternalID', $internal)
                ->where('t_sales_header.SalesDate', $if, $date)
                ->sum('Qty');
        $Rpembelian = PurchaseReturnDetail::
                join('t_purchasereturn_header', 't_purchasereturn_header.InternalID', '=', 't_purchasereturn_detail.PurchaseReturnInternalID')
                ->where('t_purchasereturn_header.CompanyInternalID', $company)
                ->where('t_purchasereturn_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                ->where('t_purchasereturn_detail.InventoryInternalID', $internal)
                ->where('t_purchasereturn_header.PurchaseReturnDate', $if, $date)
                ->sum('Qty');
        $Rpenjualan = SalesReturnDetail::
                join('t_salesreturn_header', 't_salesreturn_header.InternalID', '=', 't_salesreturn_detail.SalesReturnInternalID')
                ->where('t_salesreturn_header.CompanyInternalID', $company)
                ->where('t_salesreturn_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                ->where('t_salesreturn_detail.InventoryInternalID', $internal)
                ->where('t_salesreturn_header.SalesReturnDate', $if, $date)
                ->sum('Qty');
        $Min = MemoInDetail::
                join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.MemoInInternalID')
                ->where('t_memoin_header.CompanyInternalID', $company)
                ->where('t_memoin_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                ->where('t_memoin_detail.InventoryInternalID', $internal)
                ->where('t_memoin_header.MemoInDate', $if, $date)
                ->sum('Qty');
        $Mout = MemoOutDetail::
                join('t_memoout_header', 't_memoout_header.InternalID', '=', 't_memoout_detail.MemoOutInternalID')
                ->where('t_memoout_header.CompanyInternalID', $company)
                ->where('t_memoout_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                ->where('t_memoout_detail.InventoryInternalID', $internal)
                ->where('t_memoout_header.MemoOutDate', $if, $date)
                ->sum('Qty');
        $Tin = TransferDetail::
                join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                ->where('t_transfer_header.CompanyInternalID', $company)
                ->where('t_transfer_header.WarehouseDestinyInternalID', $delimiterWarehouse, $warehouse)
                ->where('t_transfer_detail.InventoryInternalID', $internal)
                ->where('t_transfer_header.TransferDate', $if, $date)
                ->sum('Qty');
        $Tout = TransferDetail::
                join('t_transfer_header', 't_transfer_header.InternalID', '=', 't_transfer_detail.TransferInternalID')
                ->where('t_transfer_header.CompanyInternalID', $company)
                ->where('t_transfer_header.WarehouseInternalID', $delimiterWarehouse, $warehouse)
                ->where('t_transfer_detail.InventoryInternalID', $internal)
                ->where('t_transfer_header.TransferDate', $if, $date)
                ->sum('Qty');
        return $pembelian + $Rpenjualan - $penjualan - $Rpembelian + $Min - $Mout + $Tin - $Tout + $initial;
    }

    public function salesDetail() {
        return $this->hasMany('SalesDetail', 'InventoryInternalID', 'InternalID');
    }

    public function purchaseDetail() {
        return $this->hasMany('PurchaseDetail', 'InventoryInternalID', 'InternalID');
    }

    public function salesReturnDetail() {
        return $this->hasMany('SalesReturnDetail', 'InventoryInternalID', 'InternalID');
    }

    public function purchaseReturnDetail() {
        return $this->hasMany('PurchaseReturnDetail', 'InventoryInternalID', 'InternalID');
    }
    
    public function inventoryUom() {
        return $this->hasMany('InventoryUom', 'InventoryInternalID', 'InternalID');
    }

    public function inventoryType() {
        return $this->belongsTo('InventoryType', 'InventoryTypeInternalID', 'InternalID');
    }

}
