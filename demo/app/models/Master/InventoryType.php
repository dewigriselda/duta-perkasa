<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class InventoryType extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_inventorytype';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';
    
    
    //buat set relasi hubungan table, subquery dan database
    public static function showInventoryType() {
        return InventoryType::all();
    }
    
    public static function coaName($idInventoryType) {
        $result = DB::select(DB::raw('select `m_coa`.`COAName` as COAName , `m_coa`.`InternalID` as COAID '
                                . 'from `m_inventorytype` '
                                . 'inner join `m_coa` on `m_inventorytype`.`ACC1InternalID` = `m_coa`.`ACC1InternalID` '
                                . 'AND `m_inventorytype`.`ACC2InternalID` = `m_coa`.`ACC2InternalID` '
                                . 'AND `m_inventorytype`.`ACC3InternalID` = `m_coa`.`ACC3InternalID` '
                                . 'AND `m_inventorytype`.`ACC4InternalID` = `m_coa`.`ACC4InternalID` '
                                . 'AND `m_inventorytype`.`ACC5InternalID` = `m_coa`.`ACC5InternalID` '
                                . 'AND `m_inventorytype`.`ACC6InternalID` = `m_coa`.`ACC6InternalID` '
                                . 'where `m_inventorytype`.`InternalID` = ' . $idInventoryType . ' '
                                . 'AND `m_inventorytype`.`CompanyInternalID` = ' . Auth::user()->Company->InternalID . ' '));
        return $result;
    }
    
    //Relasi
    public function inventory() {
        return $this->hasMany('Inventory', 'InventoryTypeInternalID', 'InternalID');
    }
}
