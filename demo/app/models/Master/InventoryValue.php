<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class InventoryValue extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'h_inventory_value';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showInventoryValue() {
        return InventoryValue::all();
    }

    public static function qtyInventoryBefore($inventory, $bulan, $tahun) {
        $bulan = $bulan - 1;
        if ($bulan == 0) {
            $bulan = 12;
            $tahun = $tahun - 1;
        }
        $result = InventoryValue::where('InventoryInternalID', $inventory)
                ->where('Month', $bulan)
                ->where('Year', $tahun)
                ->sum('Quantity');
        if (count($result) <= 0) {
            return 0;
        }
        return $result;
    }

    public static function valueInventoryBefore($inventory, $bulan, $tahun) {
        $bulan = $bulan - 1;
        if ($bulan == 0) {
            $bulan = 12;
            $tahun = $tahun - 1;
        }
        $result = InventoryValue::where('InventoryInternalID', $inventory)
                ->where('Month', $bulan)
                ->where('Year', $tahun)
                ->sum('Value');
        if (count($result) <= 0) {
            return 0;
        }
        return $result;
    }

}
