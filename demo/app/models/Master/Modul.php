<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Modul extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_modul';
    public $timestamps = false;
    protected $primaryKey = 'InternalID';
    
    public static function showModul() {
        return Modul::all();
    }
    
    public function packageDetail() {
        return $this->hasMany('PackageDetail', 'ModulInternalID', 'InternalID');
    }
}
