<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class UserDetail extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_user_detail';
    public $timestamps = false;
    protected $primaryKey = 'InternalID';

    public static function showUserDetail() {
        return UserDetail::all();
    }
    
    public static function checkDetail($InternalMatrix, $InternalUser) {
        $tamp = UserDetail::where('UserInternalID', $InternalUser)->where('MatrixInternalID', $InternalMatrix)->count();
        if($tamp > 0){
            return 'checked';
        }
        return '';
    }
    
    public function user() {
        return $this->belongsTo('User', 'UserInternalID', 'InternalID');
    }
    
    public function matrix() {
        return $this->belongsTo('Matrix', 'MatrixInternalID', 'InternalID');
    }
}
