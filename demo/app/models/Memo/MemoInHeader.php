<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class MemoInHeader extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_memoin_header';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showMemoInHeader() {
        return MemoInHeader::all();
    }

    public static function getIdmemoIn($memoInID) {
        $internalID = MemoInHeader::where('MemoInID', '=', $memoInID)->where('CompanyInternalID', Auth::user()->Company->InternalID)->pluck('InternalID');
        return $internalID;
    }

    public static function qtyInventory($inventory, $bulan, $tahun) {
        $result = MemoInDetail::join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.MemoInInternalID')
                ->whereRaw('YEAR(MemoInDate) = "' . $tahun . '" AND MONTH(MemoInDate) = "' . $bulan . '"')
                ->where('InventoryInternalID', $inventory)
                ->where('t_memoin_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                ->sum('Qty');
        return $result;
    }

    public static function valueInventory($inventory, $bulan, $tahun) {
        $result = MemoInDetail::join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.MemoInInternalID')
                ->whereRaw('YEAR(MemoInDate) = "' . $tahun . '" AND MONTH(MemoInDate) = "' . $bulan . '"')
                ->where('InventoryInternalID', $inventory)
                ->where('t_memoin_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                ->select(DB::raw('sum(SubTotal*CurrencyRate) as SubTotalCurrency'))
                ->pluck('SubTotalCurrency');
        return $result;
    }

    public static function qtyInventorySuperAdmin($inventory, $company) {
        $result = MemoInDetail::join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.MemoInInternalID')
                ->where('InventoryInternalID', $inventory)
                ->where('t_memoin_header.CompanyInternalID', $company)
                ->sum('Qty');
        return $result;
    }

    public static function valueInventorySuperAdmin($inventory, $company) {
        $result = MemoInDetail::join('t_memoin_header', 't_memoin_header.InternalID', '=', 't_memoin_detail.MemoInInternalID')
                ->where('InventoryInternalID', $inventory)
                ->where('t_memoin_header.CompanyInternalID', $company)
                ->select(DB::raw('sum(SubTotal*CurrencyRate) as SubTotalCurrency'))
                ->pluck('SubTotalCurrency');
        return $result;
    }

    public static function getNextIDMemoIn($text) {
        $query = 'SELECT MemoInID From t_memoin_header Where MemoInID LIKE "' . $text . '%" AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" order by MemoInID desc';
        $memoInID = DB::select(DB::raw($query));

        if (count($memoInID) <= 0) {
            $memoInID = '';
        } else {
            $memoInID = $memoInID[0]->MemoInID;
        }

        if ($memoInID == '') {
            $memoInID = $text . '0001';
        } else {
            $textTamp = $memoInID;
            $memoInID = substr($textTamp, (strlen($textTamp) - 4), 4) + 1;
            $memoInID = str_pad($memoInID, 4, '0', STR_PAD_LEFT);
            $memoInID = $text . $memoInID;
        }
        return $memoInID;
    }

    public function memoInDetail() {
        return $this->hasMany('MemoInDetail', 'MemoInInternalID', 'InternalID');
    }

    public function currency() {
        return $this->belongsTo('Currency', 'CurrencyInternalID', 'InternalID');
    }

    public function warehouse() {
        return $this->belongsTo('Warehouse', 'WarehouseInternalID', 'InternalID');
    }

}
