<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class MemoOutHeader extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_memoout_header';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showMemoOutHeader() {
        return MemoOutHeader::all();
    }

    public static function getIdmemoOut($memoOutID) {
        $internalID = MemoOutHeader::where('MemoOutID', '=', $memoOutID)->where('CompanyInternalID', Auth::user()->Company->InternalID)->pluck('InternalID');
        return $internalID;
    }

    public static function qtyInventory($inventory, $bulan, $tahun) {
        $result = MemoOutDetail::join('t_memoout_header', 't_memoout_header.InternalID', '=', 't_memoout_detail.MemoOutInternalID')
                ->whereRaw('YEAR(MemoOutDate) = "' . $tahun . '" AND MONTH(MemoOutDate) = "' . $bulan . '"')
                ->where('InventoryInternalID', $inventory)
                ->where('t_memoout_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                ->sum('Qty');
        return $result;
    }

    public static function valueInventory($inventory, $bulan, $tahun) {
        $result = MemoOutDetail::join('t_memoout_header', 't_memoout_header.InternalID', '=', 't_memoout_detail.MemoOutInternalID')
                ->whereRaw('YEAR(MemoOutDate) = "' . $tahun . '" AND MONTH(MemoOutDate) = "' . $bulan . '"')
                ->where('InventoryInternalID', $inventory)
                ->where('t_memoout_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                ->select(DB::raw('sum(SubTotal*CurrencyRate) as SubTotalCurrency'))
                ->pluck('SubTotalCurrency');
        return $result;
    }
    
    public static function qtyInventorySuperAdmin($inventory, $company) {
        $result = MemoOutDetail::join('t_memoout_header', 't_memoout_header.InternalID', '=', 't_memoout_detail.MemoOutInternalID')
                ->where('InventoryInternalID', $inventory)
                ->where('t_memoout_header.CompanyInternalID', $company)
                ->sum('Qty');
        return $result;
    }

    public static function valueInventorySuperAdmin($inventory, $company) {
        $result = MemoOutDetail::join('t_memoout_header', 't_memoout_header.InternalID', '=', 't_memoout_detail.MemoOutInternalID')
                ->where('InventoryInternalID', $inventory)
                ->where('t_memoout_header.CompanyInternalID', $company)
                ->select(DB::raw('sum(SubTotal*CurrencyRate) as SubTotalCurrency'))
                ->pluck('SubTotalCurrency');
        return $result;
    }

    public static function getNextIDMemoOut($text) {
        $query = 'SELECT MemoOutID From t_memoout_header Where MemoOutID LIKE "' . $text . '%" AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" order by MemoOutID desc';
        $memoOutID = DB::select(DB::raw($query));

        if (count($memoOutID) <= 0) {
            $memoOutID = '';
        } else {
            $memoOutID = $memoOutID[0]->MemoOutID;
        }

        if ($memoOutID == '') {
            $memoOutID = $text . '0001';
        } else {
            $textTamp = $memoOutID;
            $memoOutID = substr($textTamp, (strlen($textTamp) - 4), 4) + 1;
            $memoOutID = str_pad($memoOutID, 4, '0', STR_PAD_LEFT);
            $memoOutID = $text . $memoOutID;
        }
        return $memoOutID;
    }

    public function memoOutDetail() {
        return $this->hasMany('MemoOutDetail', 'MemoOutInternalID', 'InternalID');
    }

    public function currency() {
        return $this->belongsTo('Currency', 'CurrencyInternalID', 'InternalID');
    }

    public function warehouse() {
        return $this->belongsTo('Warehouse', 'WarehouseInternalID', 'InternalID');
    }

}
