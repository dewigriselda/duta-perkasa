<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class PurchaseReturnHeader extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_purchasereturn_header';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showPurchaseReturnHeader() {
        return PurchaseReturnHeader::all();
    }

    public static function advancedSearch($typePayment, $typeTax, $start, $end) {
        $where = '';
        if ($typePayment != '-1' && $typePayment != '') {
            $where .= 'isCash = "' . $typePayment . '" ';
        }
        if ($typeTax != '-1' && $typeTax != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'VAT = "' . $typeTax . '" ';
        }
        if ($start != '' && $end != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $start = explode('-', $start);
            $end = explode('-', $end);
            $startSearch = $start[2] . '-' . $start[1] . '-' . $start[0];
            $endSearch = $end[2] . '-' . $end[1] . '-' . $end[0];
            $where .= 'PurchaseReturnDate between "' . $startSearch . ' 00:00:00" AND "' . $endSearch . ' 23:59:59"';
        }

        $query = 'SELECT sh.*, c.CurrencyName, cc.ACC6Name, cc.ACC6ID '
                . 'FROM t_purchasereturn_header sh '
                . 'INNER JOIN m_currency c on c.InternalID = sh.CurrencyInternalID '
                . 'INNER JOIN m_coa6 cc on cc.InternalID = sh.ACC6InternalID';
        if ($where != '') {
            $query .= ' WHERE ' . $where . ' AND ' . 'sh.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        } else {
            $query .= ' WHERE sh.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        }
        return $result = DB::select(DB::raw($query));
    }

    public static function getIdpurchaseReturn($purchaseReturnID) {
        $internalID = PurchaseReturnHeader::where('PurchaseReturnID', '=', $purchaseReturnID)->where('CompanyInternalID', Auth::user()->Company->InternalID)->pluck('InternalID');
        return $internalID;
    }

    public static function getNextIDPurchaseReturn($text) {
        $query = 'SELECT PurchaseReturnID '
                . 'From t_purchasereturn_header '
                . 'Where PurchaseReturnID LIKE "%-' . $text . '" '
                . 'AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" '
                . 'order by PurchaseReturnID desc';
        $purchaseReturnID = DB::select(DB::raw($query));

        if (count($purchaseReturnID) <= 0) {
            $purchaseReturnID = '';
        } else {
            $purchaseReturnID = $purchaseReturnID[0]->PurchaseReturnID;
        }

        if ($purchaseReturnID == '') {
            $purchaseReturnID = 'R.0001-'.$text;
        } else {
            $textTamp = $purchaseReturnID;
            $purchaseReturnID = substr($textTamp, 2, 4) + 1;
            $purchaseReturnID = str_pad($purchaseReturnID, 4, '0', STR_PAD_LEFT);
            $purchaseReturnID = 'R.'.$purchaseReturnID.'-'.$text;
        }
        return $purchaseReturnID;
    }
    
    public static function getSumReturn($inventoryID, $purchaseID, $InternalID) {
        $query = 'SELECT SUM(td.Qty) as total '
                . 'From t_purchasereturn_detail td INNER JOIN t_purchasereturn_header th on td.PurchaseReturnInternalID = th.InternalID '
                . 'Where td.InventoryInternalID = "'.$inventoryID.'" AND PurchaseReturnID LIKE "%-' . $purchaseID . '" '
                . 'AND td.PurchaseDetailInternalID = "'.$InternalID.'" '
                . 'AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" ';
        $purchaseReturnID = DB::select(DB::raw($query));
        return $purchaseReturnID[0]->total;
    }
    
    public static function getSumReturnOrder($inventoryID, $purchaseOrderID, $InternalID) {
        $query = 'SELECT SUM(td.Qty) as total '
                . 'From t_purchasereturn_detail td INNER JOIN t_purchasereturn_header th on td.PurchaseReturnInternalID = th.InternalID '
                . 'INNER JOIN t_purchase_detail td2 on td.PurchaseDetailInternalID = td2.InternalID '
                . 'INNER JOIN t_purchase_header th2 on td2.PurchaseInternalID = th2.InternalID '
                . 'Where td.InventoryInternalID = "' . $inventoryID . '" AND th2.PurchaseOrderInternalID = "' . $purchaseOrderID . '" '
                . 'AND td2.PurchaseOrderDetailInternalID = "' . $InternalID . '" '
                . 'AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '" ';
        $purchaseReturnID = DB::select(DB::raw($query));
        return $purchaseReturnID[0]->total;
    }
    
    public static function getSumReturnExcept($inventoryID, $purchaseReturnID, $InternalID) {
        $purchaseID = substr($purchaseReturnID, 7);
        $query = 'SELECT SUM(td.Qty) as total '
                . 'From t_purchasereturn_detail td INNER JOIN t_purchasereturn_header th on td.PurchaseReturnInternalID = th.InternalID '
                . 'Where td.InventoryInternalID = "'.$inventoryID.'" AND PurchaseReturnID LIKE "%-' . $purchaseID . '" '
                . 'AND th.PurchaseReturnID != "'.$purchaseReturnID.'" '
                . 'AND td.PurchaseDetailInternalID = "'.$InternalID.'" '
                . 'AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" ';
        $purchaseReturnID = DB::select(DB::raw($query));
        return $purchaseReturnID[0]->total;
    }
    
    public static function getTopTen() {
        $query = 'SELECT table2.* FROM ('
                . 'SELECT PurchaseReturnDate, GrandTotal*CurrencyRate as hasil From t_purchasereturn_header '
                . 'WHERE CompanyInternalID = "' . Auth::user()->Company->InternalID . '" '
                . 'Order by PurchaseReturnDate desc Limit 0,10'
                . ') as table2 '
                . 'Order by table2.PurchaseReturnDate asc';
        $top = DB::select(DB::raw($query));
        return $top;
    }

    public static function getPurchaseReturn10($id) {
        $query = 'SELECT SUM(table2.hasil) as hasil'
                . ' FROM (SELECT s.ACC6Name,th.GrandTotal*th.CurrencyRate as hasil '
                . ' From t_purchasereturn_header th INNER JOIN m_coa6 as s on s.InternalID = th.ACC6InternalID '
                . ' Where s.InternalID = "' . $id . '" '
                . ' AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"'
                . ' Order by th.PurchaseReturnDate desc Limit 0,10) as table2';
        $purchasereturn = DB::select(DB::raw($query));
        return $purchasereturn;
    }
    
    public static function getPurchaseReturnReceivable() {
        $query = 'SELECT th.*, th.PurchaseReturnID as ID, th.PurchaseReturnDate as Date, coa.ACC6Name as coa6, cur.CurrencyName '
                . 'FROM t_purchasereturn_header th INNER JOIN m_coa6 as coa on th.ACC6InternalID = coa.InternalID '
                . 'INNER JOIN m_currency as cur on cur.InternalID = th.CurrencyInternalID '
                . 'Where th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '" '
                . 'AND th.`GrandTotal`*th.`CurrencyRate` > (Select IFNULL(SUM(td.JournalCreditMU),0) From t_journal_detail td Where td.JournalTransactionID = th.`PurchaseReturnID`)'
                . 'order by coa.ACC6Name Asc, th.InternalID DESC';
        $purchase = DB::select(DB::raw($query));
        return $purchase;
    }

    public static function qtyInventory($inventory, $bulan, $tahun) {
        $result = PurchaseReturnDetail::join('t_purchasereturn_header', 't_purchasereturn_header.InternalID', '=', 't_purchasereturn_detail.PurchaseReturnInternalID')
                ->whereRaw('YEAR(PurchaseReturnDate) = "' . $tahun . '" AND MONTH(PurchaseReturnDate) = "' . $bulan . '"')
                ->where('InventoryInternalID', $inventory)
                ->where('t_purchasereturn_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                ->sum('Qty');
        return $result;
    }
    
    public static function valueInventory($inventory, $bulan, $tahun) {
        $result = PurchaseReturnDetail::join('t_purchasereturn_header', 't_purchasereturn_header.InternalID', '=', 't_purchasereturn_detail.PurchaseReturnInternalID')
                ->whereRaw('YEAR(PurchaseReturnDate) = "' . $tahun . '" AND MONTH(PurchaseReturnDate) = "' . $bulan . '"')
                ->where('InventoryInternalID', $inventory)
                ->where('t_purchasereturn_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                ->select(DB::raw('sum(SubTotal*CurrencyRate) as SubTotalCurrency'))
                ->pluck('SubTotalCurrency');
        return $result;
    }
    
    public static function qtyInventorySuperAdmin($inventory, $company) {
        $result = PurchaseReturnDetail::join('t_purchasereturn_header', 't_purchasereturn_header.InternalID', '=', 't_purchasereturn_detail.PurchaseReturnInternalID')
                ->where('InventoryInternalID', $inventory)
                ->where('t_purchasereturn_header.CompanyInternalID', $company)
                ->sum('Qty');
        return $result;
    }
    
    public static function valueInventorySuperAdmin($inventory, $company) {
        $result = PurchaseReturnDetail::join('t_purchasereturn_header', 't_purchasereturn_header.InternalID', '=', 't_purchasereturn_detail.PurchaseReturnInternalID')
                ->where('InventoryInternalID', $inventory)
                ->where('t_purchasereturn_header.CompanyInternalID', $company)
                ->select(DB::raw('sum(SubTotal*CurrencyRate) as SubTotalCurrency'))
                ->pluck('SubTotalCurrency');
        return $result;
    }
    
    public static function getSumDiscountGlobalPurchaseReturn($purchaseID) {
        $query = 'SELECT SUM(th.DiscountGlobal) as total '
                . 'From t_purchasereturn_header th '
                . 'Where PurchaseReturnID LIKE "%-' . $purchaseID . '" '
                . 'AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" ';
        $purchaseID = DB::select(DB::raw($query));
        return $purchaseID[0]->total;
    }

    public static function getSumDiscountGlobalReturnExcept($purchaseID, $purchaseReturnID) {
        $query = 'SELECT SUM(th.DiscountGlobal) as total '
                . 'From t_purchasereturn_header th '
                . 'Where PurchaseReturnID LIKE "%-' . $purchaseID . '" '
                . 'AND th.PurchaseReturnID != "' . $purchaseReturnID . '" '
                . 'AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" ';
        $purchaseID = DB::select(DB::raw($query));
        return $purchaseID[0]->total;
    }
    
    public function purchaseReturnDetail() {
        return $this->hasMany('PurchaseReturnDetail', 'PurchaseReturnInternalID', 'InternalID');
    }

    public function coa6() {
        return $this->belongsTo('Coa6', 'ACC6InternalID', 'InternalID');
    }

    public function currency() {
        return $this->belongsTo('Currency', 'CurrencyInternalID', 'InternalID');
    }
    public function warehouse() {
        return $this->belongsTo('Warehouse', 'WarehouseInternalID', 'InternalID');
    }

}
