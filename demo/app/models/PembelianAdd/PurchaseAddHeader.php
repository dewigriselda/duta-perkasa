<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class PurchaseAddHeader extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_purchase_header';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showPurchaseHeader() {
        return PurchaseHeader::all();
    }

    public static function advancedSearch($typePayment, $typeTax, $start, $end) {
        $where = '';
        if ($typePayment != '-1' && $typePayment != '') {
            $where .= 'isCash = "' . $typePayment . '" ';
        }
        if ($typeTax != '-1' && $typeTax != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'VAT = "' . $typeTax . '" ';
        }
        if ($start != '' && $end != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $start = explode('-', $start);
            $end = explode('-', $end);
            $startSearch = $start[2] . '-' . $start[1] . '-' . $start[0];
            $endSearch = $end[2] . '-' . $end[1] . '-' . $end[0];
            $where .= 'PurchaseDate between "' . $startSearch . ' 00:00:00" AND "' . $endSearch . ' 23:59:59"';
        }

        $query = 'SELECT sh.*, c.CurrencyName, cc.ACC6Name, cc.ACC6ID '
                . 'FROM t_purchase_header sh '
                . 'INNER JOIN m_currency c on c.InternalID = sh.CurrencyInternalID '
                . 'INNER JOIN m_coa6 cc on cc.InternalID = sh.ACC6InternalID';
        if ($where != '') {
            $query .= ' WHERE ' . $where . ' AND ' . 'sh.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        } else {
            $query .= ' WHERE sh.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        }
        return $result = DB::select(DB::raw($query));
    }

    public static function getIdpurchase($purchaseID) {
        $internalID = PurchaseHeader::where('PurchaseID', '=', $purchaseID)->where('CompanyInternalID', Auth::user()->Company->InternalID)->pluck('InternalID');
        return $internalID;
    }

    public static function getNextIDPurchase($text) {
        $query = 'SELECT PurchaseID From t_purchase_header Where PurchaseID LIKE "' . $text . '%" AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" order by PurchaseID desc';
        $purchaseID = DB::select(DB::raw($query));

        if (count($purchaseID) <= 0) {
            $purchaseID = '';
        } else {
            $purchaseID = $purchaseID[0]->PurchaseID;
        }

        if ($purchaseID == '') {
            $purchaseID = $text . '0001';
        } else {
            $textTamp = $purchaseID;
            $purchaseID = substr($textTamp, (strlen($textTamp) - 4), 4) + 1;
            $purchaseID = str_pad($purchaseID, 4, '0', STR_PAD_LEFT);
            $purchaseID = $text . $purchaseID;
        }
        return $purchaseID;
    }

    public static function getTopTen() {
        $query = 'SELECT table2.* FROM ('
                . 'SELECT PurchaseDate, GrandTotal*CurrencyRate as hasil From t_purchase_header '
                . 'WHERE CompanyInternalID = "' . Auth::user()->Company->InternalID . '" '
                . 'Order by PurchaseDate desc Limit 0,10'
                . ') as table2 '
                . 'Order by table2.PurchaseDate asc';
        $top = DB::select(DB::raw($query));
        return $top;
    }

    public static function getPurchase10($id) {
        $query = 'SELECT SUM(table2.hasil) as hasil'
                . ' FROM (SELECT s.ACC6Name,th.GrandTotal*th.CurrencyRate as hasil '
                . ' From t_purchase_header th INNER JOIN m_coa6 as s on s.InternalID = th.ACC6InternalID '
                . ' Where s.InternalID = "' . $id . '" '
                . ' AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"'
                . ' Order by th.PurchaseDate desc Limit 0,10) as table2';
        $purchase = DB::select(DB::raw($query));
        return $purchase;
    }

    public static function getPurchasePayable() {
        $query = 'SELECT th.*, th.PurchaseID as ID, th.PurchaseDate as Date, coa.ACC6Name as coa6, cur.CurrencyName '
                . 'FROM t_purchase_header th INNER JOIN m_coa6 as coa on th.ACC6InternalID = coa.InternalID '
                . 'INNER JOIN m_currency as cur on cur.InternalID = th.CurrencyInternalID '
                . 'where th.isCash = 1 '
                . 'AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '" '
                . 'AND th.`GrandTotal`*th.`CurrencyRate` > (Select IFNULL(SUM(td.JournalDebetMU),0) From t_journal_detail td Where td.JournalTransactionID = th.`PurchaseID`)';
        $purchase = DB::select(DB::raw($query));
        $tamp = array();
        foreach ($purchase as $value) {
            $data = date("Y-m-d", strtotime("+" . $value->LongTerm . " day", strtotime($value->Date)));
            array_push($tamp, $data);
        }
        array_multisort($tamp, $purchase);
        return $purchase;
    }

    public static function getPurchasePayableAging() {
        $query = 'SELECT DATEDIFF(DATE_ADD(th.PurchaseDate,INTERVAL th.LongTerm DAY),Now()) as selisihHari, th.GrandTotal*th.CurrencyRate as GrandTotal '
                . 'FROM t_purchase_header th INNER JOIN m_coa6 as coa on th.ACC6InternalID = coa.InternalID '
                . 'where th.isCash = 1 '
                . 'AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '" '
                . 'AND th.`GrandTotal`*th.`CurrencyRate` > (Select IFNULL(SUM(td.JournalDebetMU),0) From t_journal_detail td Where td.JournalTransactionID = th.`PurchaseID`)';
        $purchase = DB::select(DB::raw($query));
        return $purchase;
    }
    
    public static function getSlipInternalID($purchaseID){
        $journal = JournalHeader::where('TransactionID',$purchaseID)->first();
        $detail = Slip::find($journal->SlipInternalID);
        $slip = Slip::where('ACC1InternalID',$detail->ACC1InternalID)
                ->where('ACC2InternalID',$detail->ACC2InternalID)
                ->where('ACC3InternalID',$detail->ACC3InternalID)
                ->where('ACC4InternalID',$detail->ACC4InternalID)
                ->where('ACC5InternalID',$detail->ACC5InternalID)
                ->where('ACC6InternalID',$detail->ACC6InternalID)->first();
        return $slip->InternalID;
    }
    
    public static function getSum($inventoryID, $purchaseID, $InternalID) {
        $query = 'SELECT SUM(td.Qty) as total '
                . 'From t_purchase_detail td INNER JOIN t_purchase_header th on td.PurchaseInternalID = th.InternalID '
                . 'Where td.InventoryInternalID = "'.$inventoryID.'" AND PurchaseOrderInternalID = "' .$purchaseID. '"'
                . 'AND td.PurchaseOrderDetailInternalID = "'.$InternalID.'" '
                . 'AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" ';
        $purchaseReturnID = DB::select(DB::raw($query));
        return $purchaseReturnID[0]->total;
    }
    
    public static function getSumExcept($inventoryID, $purchaseOrderID, $PurchaseID, $InternalID) {
        $query = 'SELECT SUM(td.Qty) as total '
                . 'From t_purchase_detail td INNER JOIN t_purchase_header th on td.PurchaseInternalID = th.InternalID '
                . 'Where td.InventoryInternalID = "'.$inventoryID.'" AND PurchaseOrderInternalID = "' .$purchaseOrderID. '"'
                . 'AND th.InternalID != "'.$PurchaseID.'" '
                . 'AND td.PurchaseOrderDetailInternalID = "'.$InternalID.'" '
                . 'AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" ';
        $purchaseReturnID = DB::select(DB::raw($query));
        return $purchaseReturnID[0]->total;
    }
    
    public static function isReturn($purchaseID){
        $query = 'SELECT COUNT(*) as total '
                . 'From t_purchasereturn_header th '
                . 'Where PurchaseReturnID LIKE "%-' . $purchaseID . '" '
                . 'AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" ';
        $purchaseReturnID = DB::select(DB::raw($query));
        if($purchaseReturnID[0]->total > 0)
        {
            return true;
        }
        return false;
    }
    public static function getYearMin(){
        $result = PurchaseHeader::orderBy('PurchaseDate')->select(DB::raw('YEAR(PurchaseDate) as PurchaseDates'))->where("CompanyInternalID",Auth::User()->CompanyInternalID)->first();
        if($result == ''){
            return '0';
        }
        return $result->PurchaseDates;
    }

    public static function qtyInventory($inventory, $bulan, $tahun) {
        $result = PurchaseDetail::join('t_purchase_header', 't_purchase_header.InternalID', '=', 't_purchase_detail.PurchaseInternalID')
                ->whereRaw('YEAR(PurchaseDate) = "' . $tahun . '" AND MONTH(PurchaseDate) = "' . $bulan . '"')
                ->where('InventoryInternalID', $inventory)
                ->where('t_purchase_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                ->sum('Qty');
        return $result;
    }

    public static function valueInventory($inventory, $bulan, $tahun) {
        $result = PurchaseDetail::join('t_purchase_header', 't_purchase_header.InternalID', '=', 't_purchase_detail.PurchaseInternalID')
                ->whereRaw('YEAR(PurchaseDate) = "' . $tahun . '" AND MONTH(PurchaseDate) = "' . $bulan . '"')
                ->where('InventoryInternalID', $inventory)
                ->where('t_purchase_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                ->select(DB::raw('sum(SubTotal*CurrencyRate) as SubTotalCurrency'))
                ->pluck('SubTotalCurrency');
        return $result;
    }
    
    public static function qtyInventorySuperAdmin($inventory, $company) {
        $result = PurchaseDetail::join('t_purchase_header', 't_purchase_header.InternalID', '=', 't_purchase_detail.PurchaseInternalID')
                ->where('InventoryInternalID', $inventory)
                ->where('t_purchase_header.CompanyInternalID', $company)
                ->sum('Qty');
        return $result;
    }

    public static function valueInventorySuperAdmin($inventory, $company) {
        $result = PurchaseDetail::join('t_purchase_header', 't_purchase_header.InternalID', '=', 't_purchase_detail.PurchaseInternalID')
                ->where('InventoryInternalID', $inventory)
                ->where('t_purchase_header.CompanyInternalID', $company)
                ->select(DB::raw('sum(SubTotal*CurrencyRate) as SubTotalCurrency'))
                ->pluck('SubTotalCurrency');
        return $result;
    }
    
    public static function getSumDiscountGlobalPurchase($purchaseID) {
        $query = 'SELECT SUM(th.DiscountGlobal) as total '
                . 'From t_purchase_header th '
                . 'Where th.PurchaseOrderInternalID = "' . $purchaseID . '" '
                . 'AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" ';
        $purchaseID = DB::select(DB::raw($query));
        return $purchaseID[0]->total;
    }

    public static function getSumDiscountGlobalExcept($purchaseID, $purchaseOrder) {
        $query = 'SELECT SUM(th.DiscountGlobal) as total '
                . 'From t_purchase_header th '
                . 'Where th.PurchaseOrderInternalID = "' . $purchaseOrder . '" '
                . 'AND th.InternalID != "' . $purchaseID . '" '
                . 'AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" ';
        $purchaseID = DB::select(DB::raw($query));
        return $purchaseID[0]->total;
    }
    
    public function purchaseDetail() {
        return $this->hasMany('PurchaseDetail', 'PurchaseInternalID', 'InternalID');
    }
    

    public function coa6() {
        return $this->belongsTo('Coa6', 'ACC6InternalID', 'InternalID');
    }

    public function currency() {
        return $this->belongsTo('Currency', 'CurrencyInternalID', 'InternalID');
    }
    
    public function warehouse() {
        return $this->belongsTo('Warehouse', 'WarehouseInternalID', 'InternalID');
    }

    public function purchaseOrderHeader() {
        return $this->belongsTo('PurchaseOrderHeader', 'PurchaseOrderInternalID', 'InternalID');
    }
}
