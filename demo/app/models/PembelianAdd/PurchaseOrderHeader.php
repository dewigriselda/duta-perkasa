<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class PurchaseOrderHeader extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_purchaseorder_header';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showPurchaseOrderHeader() {
        return PurchaseOrderHeader::all();
    }

    public static function advancedSearch($typePayment, $typeTax, $start, $end) {
        $where = '';
        if ($typePayment != '-1' && $typePayment != '') {
            $where .= 'isCash = "' . $typePayment . '" ';
        }
        if ($typeTax != '-1' && $typeTax != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'VAT = "' . $typeTax . '" ';
        }
        if ($start != '' && $end != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $start = explode('-', $start);
            $end = explode('-', $end);
            $startSearch = $start[2] . '-' . $start[1] . '-' . $start[0];
            $endSearch = $end[2] . '-' . $end[1] . '-' . $end[0];
            $where .= 'PurchaseOrderDate between "' . $startSearch . ' 00:00:00" AND "' . $endSearch . ' 23:59:59"';
        }

        $query = 'SELECT sh.*, c.CurrencyName, cc.ACC6Name, cc.ACC6ID '
                . 'FROM t_purchaseorder_header sh '
                . 'INNER JOIN m_currency c on c.InternalID = sh.CurrencyInternalID '
                . 'INNER JOIN m_coa6 cc on cc.InternalID = sh.ACC6InternalID';
        if ($where != '') {
            $query .= ' WHERE ' . $where . ' AND ' . 'sh.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        } else {
            $query .= ' WHERE sh.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        }
        return $result = DB::select(DB::raw($query));
    }

    public static function getIdpurchaseOrder($purchaseOrderID) {
        $internalID = PurchaseOrderHeader::where('PurchaseOrderID', '=', $purchaseOrderID)->where('CompanyInternalID', Auth::user()->Company->InternalID)->pluck('InternalID');
        return $internalID;
    }

    public static function getNextIDPurchaseOrder($text) {
        $query = 'SELECT PurchaseOrderID From t_purchaseorder_header Where PurchaseOrderID LIKE "' . $text . '%" AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" order by PurchaseOrderID desc';
        $purchaseOrderID = DB::select(DB::raw($query));

        if (count($purchaseOrderID) <= 0) {
            $purchaseOrderID = '';
        } else {
            $purchaseOrderID = $purchaseOrderID[0]->PurchaseOrderID;
        }

        if ($purchaseOrderID == '') {
            $purchaseOrderID = $text . '0001';
        } else {
            $textTamp = $purchaseOrderID;
            $purchaseOrderID = substr($textTamp, (strlen($textTamp) - 4), 4) + 1;
            $purchaseOrderID = str_pad($purchaseOrderID, 4, '0', STR_PAD_LEFT);
            $purchaseOrderID = $text . $purchaseOrderID;
        }
        return $purchaseOrderID;
    }

    public static function getTopTen() {
        $query = 'SELECT table2.* FROM ('
                . 'SELECT PurchaseOrderDate, GrandTotal*CurrencyRate as hasil From t_purchaseorder_header '
                . 'WHERE CompanyInternalID = "' . Auth::user()->Company->InternalID . '" '
                . 'Order by PurchaseOrderDate desc Limit 0,10'
                . ') as table2 '
                . 'Order by table2.PurchaseOrderDate asc';
        $top = DB::select(DB::raw($query));
        return $top;
    }

    public static function getPurchaseOrder10($id) {
        $query = 'SELECT SUM(table2.hasil) as hasil'
                . ' FROM (SELECT s.ACC6Name,th.GrandTotal*th.CurrencyRate as hasil '
                . ' From t_purchaseorder_header th INNER JOIN m_coa6 as s on s.InternalID = th.ACC6InternalID '
                . ' Where s.InternalID = "' . $id . '" '
                . ' AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"'
                . ' Order by th.PurchaseOrderDate desc Limit 0,10) as table2';
        $purchaseOrder = DB::select(DB::raw($query));
        return $purchaseOrder;
    }

    public static function getPurchaseOrderPayable() {
        $query = 'SELECT th.*, th.PurchaseOrderID as ID, th.PurchaseOrderDate as Date, coa.ACC6Name as coa6, cur.CurrencyName '
                . 'FROM t_purchaseorder_header th INNER JOIN m_coa6 as coa on th.ACC6InternalID = coa.InternalID '
                . 'INNER JOIN m_currency as cur on cur.InternalID = th.CurrencyInternalID '
                . 'where th.isCash = 1 '
                . 'AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '" '
                . 'AND th.`GrandTotal`*th.`CurrencyRate` > (Select IFNULL(SUM(td.JournalDebetMU),0) From t_journal_detail td Where td.JournalTransactionID = th.`PurchaseOrderID`)';
        $purchaseOrder = DB::select(DB::raw($query));
        return $purchaseOrder;
    }

    public static function getPurchaseOrderPayableAging() {
        $query = 'SELECT DATEDIFF(DATE_ADD(th.PurchaseOrderDate,INTERVAL th.LongTerm DAY),Now()) as selisihHari, th.GrandTotal*th.CurrencyRate as GrandTotal '
                . 'FROM t_purchaseorder_header th INNER JOIN m_coa6 as coa on th.ACC6InternalID = coa.InternalID '
                . 'where th.isCash = 1 '
                . 'AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '" '
                . 'AND th.`GrandTotal`*th.`CurrencyRate` > (Select IFNULL(SUM(td.JournalDebetMU),0) From t_journal_detail td Where td.JournalTransactionID = th.`PurchaseOrderID`)';
        $purchaseOrder = DB::select(DB::raw($query));
        return $purchaseOrder;
    }

    public static function isInvoice($internalID) {
        $purchase = PurchaseHeader::where('CompanyInternalID', Auth::user()->CompanyInternalID)
                ->where('PurchaseOrderInternalID', $internalID)->count();
        if ($purchase > 0) {
            return true;
        }
        return false;
    }

    public function purchaseOrderDetail() {
        return $this->hasMany('PurchaseOrderDetail', 'PurchaseOrderInternalID', 'InternalID');
    }

    public function purchaseAddHeader() {
        return $this->hasMany('PurchaseAddHeader', 'PurchaseOrderInternalID', 'InternalID');
        //nama file model, id FK, yang dicocokan dengan FK
    }

    public function coa6() {
        return $this->belongsTo('Coa6', 'ACC6InternalID', 'InternalID');
    }

    public function currency() {
        return $this->belongsTo('Currency', 'CurrencyInternalID', 'InternalID');
    }

    public function warehouse() {
        return $this->belongsTo('Warehouse', 'WarehouseInternalID', 'InternalID');
    }

}
