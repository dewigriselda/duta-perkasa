<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class SalesHeader extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_sales_header';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showSalesHeader() {
        return SalesHeader::all();
    }

    public static function advancedSearch($typePayment, $typeTax, $start, $end) {
        $where = '';
        if ($typePayment != '-1' && $typePayment != '') {
            $where .= 'isCash = "' . $typePayment . '" ';
        }
        if ($typeTax != '-1' && $typeTax != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $where .= 'VAT = "' . $typeTax . '" ';
        }
        if ($start != '' && $end != '') {
            if ($where != '') {
                $where .= ' AND ';
            }
            $start = explode('-', $start);
            $end = explode('-', $end);
            $startSearch = $start[2] . '-' . $start[1] . '-' . $start[0];
            $endSearch = $end[2] . '-' . $end[1] . '-' . $end[0];
            $where .= 'SalesDate between "' . $startSearch . ' 00:00:00" AND "' . $endSearch . ' 23:59:59"';
        }

        $query = 'SELECT sh.*, c.CurrencyName, cc.ACC6Name, cc.ACC6ID '
                . 'FROM t_sales_header sh '
                . 'INNER JOIN m_currency c on c.InternalID = sh.CurrencyInternalID '
                . 'INNER JOIN m_coa6 cc on cc.InternalID = sh.ACC6InternalID';
        if ($where != '') {
            $query .= ' WHERE ' . $where . ' AND ' . 'sh.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        } else {
            $query .= ' WHERE sh.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"';
        }
        return $result = DB::select(DB::raw($query));
    }

    public static function getIdsales($salesID) {
        $internalID = SalesHeader::where('SalesID', '=', $salesID)->where('CompanyInternalID', Auth::user()->Company->InternalID)->pluck('InternalID');
        return $internalID;
    }

    public static function getNextIDSales($text) {
        $query = 'SELECT SalesID From t_sales_header Where SalesID LIKE "' . $text . '%" AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" order by SalesID desc';
        $salesID = DB::select(DB::raw($query));

        if (count($salesID) <= 0) {
            $salesID = '';
        } else {
            $salesID = $salesID[0]->SalesID;
        }

        if ($salesID == '') {
            $salesID = $text . '0001';
        } else {
            $textTamp = $salesID;
            $salesID = substr($textTamp, (strlen($textTamp) - 4), 4) + 1;
            $salesID = str_pad($salesID, 4, '0', STR_PAD_LEFT);
            $salesID = $text . $salesID;
        }
        return $salesID;
    }

    public static function getTopTen() {
        $query = 'SELECT table2.* FROM ('
                . 'SELECT SalesDate, GrandTotal*CurrencyRate as hasil From t_sales_header '
                . 'WHERE CompanyInternalID = "' . Auth::user()->Company->InternalID . '" '
                . 'Order by SalesDate desc Limit 0,10'
                . ') as table2 '
                . 'Order by table2.SalesDate asc';
        $top = DB::select(DB::raw($query));
        return $top;
    }

    public static function getSales10($id) {
        $query = 'SELECT SUM(table2.hasil) as hasil'
                . ' FROM '
                . '(SELECT c.ACC6Name,th.GrandTotal*th.CurrencyRate as hasil '
                . ' From t_sales_header th INNER JOIN m_coa6 as c on c.InternalID = th.ACC6InternalID '
                . ' Where c.InternalID = "' . $id . '" '
                . ' AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '"'
                . ' Order by th.SalesDate desc Limit 0,10) as table2';
        $sales = DB::select(DB::raw($query));
        return $sales;
    }

    public static function getSalesReceivable() {
        $query = 'SELECT th.*, th.SalesID as ID, th.SalesDate as Date, coa.ACC6Name as coa6, cur.CurrencyName '
                . 'FROM t_sales_header th INNER JOIN m_coa6 as coa on th.ACC6InternalID = coa.InternalID '
                . 'INNER JOIN m_currency as cur on cur.InternalID = th.CurrencyInternalID '
                . 'where th.isCash = 1 '
                . 'AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '" '
                . 'AND th.`GrandTotal`*th.`CurrencyRate` > (Select IFNULL(SUM(td.JournalCreditMU),0) From t_journal_detail td Where td.JournalTransactionID = th.`SalesID`) '
                . 'order by coa.ACC6Name Asc, th.InternalID DESC';
        $sales = DB::select(DB::raw($query));
        $tamp = array();
        foreach ($sales as $value) {
            $data = date("Y-m-d", strtotime("+" . $value->LongTerm . " day", strtotime($value->Date)));
            array_push($tamp, $data);
        }
        array_multisort($tamp,$sales);
        return $sales;
    }

    public static function getSalesReceivableAging() {
        $query = 'SELECT DATEDIFF(DATE_ADD(th.SalesDate,INTERVAL th.LongTerm DAY),Now()) as selisihHari, th.GrandTotal*th.CurrencyRate as GrandTotal, th.SalesID '
                . 'FROM t_sales_header th INNER JOIN m_coa6 as coa on th.ACC6InternalID = coa.InternalID '
                . 'where th.isCash = 1 '
                . 'AND th.CompanyInternalID = "' . Auth::user()->Company->InternalID . '" '
                . 'AND th.`GrandTotal`*th.`CurrencyRate` > (Select IFNULL(SUM(td.JournalCreditMU),0) From t_journal_detail td Where td.JournalTransactionID = th.`SalesID`)';
        $sales = DB::select(DB::raw($query));
        return $sales;
    }

    public static function getSlipInternalID($salesID) {
        $journal = JournalHeader::where('TransactionID', $salesID)->where('CompanyInternalID',Auth::user()->Company->InternalID)->first();
        $detail = JournalDetail::where('JournalInternalID', $journal->InternalID)->where('JournalIndex', '2')->first();
        $slip = Slip::where('ACC1InternalID', $detail->ACC1InternalID)
                        ->where('ACC2InternalID', $detail->ACC2InternalID)
                        ->where('ACC3InternalID', $detail->ACC3InternalID)
                        ->where('ACC4InternalID', $detail->ACC4InternalID)
                        ->where('ACC5InternalID', $detail->ACC5InternalID)
                        ->where('ACC6InternalID', $detail->ACC6InternalID)->where('CompanyInternalID',Auth::user()->Company->InternalID)->first();
        return $slip->InternalID;
    }

    public static function isReturn($salesID) {
        $query = 'SELECT COUNT(*) as total '
                . 'From t_salesreturn_header th '
                . 'Where SalesReturnID LIKE "%-' . $salesID . '" '
                . 'AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" ';
        $salesReturnID = DB::select(DB::raw($query));
        if ($salesReturnID[0]->total > 0) {
            return true;
        }
        return false;
    }

    public static function qtyInventory($inventory, $bulan, $tahun) {
        $result = SalesDetail::join('t_sales_header', 't_sales_header.InternalID', '=', 't_sales_detail.SalesInternalID')
                ->whereRaw('YEAR(SalesDate) = "' . $tahun . '" AND MONTH(SalesDate) = "' . $bulan . '"')
                ->where('InventoryInternalID', $inventory)
                ->where('t_sales_header.CompanyInternalID', Auth::user()->CompanyInternalID)
                ->sum('Qty');
        return $result;
    }
    
    public static function qtyInventorySuperAdmin($inventory, $company) {
        $result = SalesDetail::join('t_sales_header', 't_sales_header.InternalID', '=', 't_sales_detail.SalesInternalID')
                ->where('InventoryInternalID', $inventory)
                ->where('t_sales_header.CompanyInternalID', $company)
                ->sum('Qty');
        return $result;
    }

    public static function getYearMin() {
        $result = SalesHeader::orderBy('SalesDate')->select(DB::raw('YEAR(SalesDate) as SalesDates'))->where("CompanyInternalID", Auth::User()->CompanyInternalID)->first();
        if($result == ''){
            return '0';
        }
        return $result->SalesDates;
    }

    public function salesDetail() {
        return $this->hasMany('SalesDetail', 'SalesInternalID', 'InternalID');
    }

    public function coa6() {
        return $this->belongsTo('Coa6', 'ACC6InternalID', 'InternalID');
    }

    public function currency() {
        return $this->belongsTo('Currency', 'CurrencyInternalID', 'InternalID');
    }

    public function warehouse() {
        return $this->belongsTo('Warehouse', 'WarehouseInternalID', 'InternalID');
    }

    public function salesOrder() {
        return $this->belongsTo('SalesOrderHeader', 'SalesOrderInternalID', 'InternalID');
    }

}
