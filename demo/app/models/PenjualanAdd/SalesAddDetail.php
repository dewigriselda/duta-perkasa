<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class SalesAddDetail extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_sales_detail';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';


    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showSalesDetail() {
        return SalesAddDetail::all();
    }
    
    public function salesHeader() {
        return $this->belongsTo('SalesAddHeader', 'SalesInternalID', 'InternalID');
    }

    public function inventory() {
        return $this->belongsTo('Inventory', 'InventoryInternalID', 'InternalID');
    }
}
