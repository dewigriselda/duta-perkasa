<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class TransferHeader extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_transfer_header';
    protected $timestamp = true;
    protected $primaryKey = 'InternalID';

    const CREATED_AT = 'dtRecord';
    const UPDATED_AT = 'dtModified';

    public static function showTransferHeader() {
        return TransferHeader::all();
    }

    public static function getIdtransfer($transferID) {
        $internalID = TransferHeader::where('TransferID', '=', $transferID)->where('CompanyInternalID', Auth::user()->Company->InternalID)->pluck('InternalID');
        return $internalID;
    }

    public static function getNextIDTransfer($text) {
        $query = 'SELECT TransferID From t_transfer_header Where TransferID LIKE "' . $text . '%" AND CompanyInternalID = "' . Auth::user()->Company->InternalID . '" order by TransferID desc';
        $transferID = DB::select(DB::raw($query));

        if (count($transferID) <= 0) {
            $transferID = '';
        } else {
            $transferID = $transferID[0]->TransferID;
        }

        if ($transferID == '') {
            $transferID = $text . '0001';
        } else {
            $textTamp = $transferID;
            $transferID = substr($textTamp, (strlen($textTamp) - 4), 4) + 1;
            $transferID = str_pad($transferID, 4, '0', STR_PAD_LEFT);
            $transferID = $text . $transferID;
        }
        return $transferID;
    }
    
    public function transferDetail() {
        return $this->hasMany('TransferDetail', 'TransferInternalID', 'InternalID');
    }
    
    public function warehouseSource() {
        return $this->belongsTo('Warehouse', 'WarehouseInternalID', 'InternalID');
    }
    
    public function warehouseDestiny() {
        return $this->belongsTo('Warehouse', 'WarehouseDestinyInternalID', 'InternalID');
    }

}
