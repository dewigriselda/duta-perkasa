<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */

Route::get('login', array('as' => 'login', 'uses' => 'HomeController@showLogin'));
Route::post('login', array('as' => 'login', 'uses' => 'HomeController@showLogin'));
Route::get('logout', array('as' => 'logout', 'uses' => 'HomeController@logout'));
Route::get('/', function() {
    return Redirect::route('login');
});
//email 
Route::get('checkActivationAccount/{id}', array('as' => 'checkActivationAccount', 'uses' => 'EmailController@checkActivationAccount'));
//reset password
Route::get('checkForgotPassword/{id}', array('as' => 'checkForgotPassword', 'uses' => 'EmailController@checkForgotPassword'));
Route::group(array('before' => 'auth'), function() {
    Route::group(array('before' => 'status'), function() {
        Route::group(array('before' => 'client'), function() {
            Route::group(array('before' => 'payment'), function() {
                Route::group(array('before' => 'memory'), function() {
                    //routing khusus client
                    //home routing'));
                    Route::get('showDashboard', array('as' => 'showDashboard', 'uses' => 'HomeController@showDashboard'));

                    //coa level
                    Route::get('coaLevel', array('as' => 'coaLevel', 'before' => 'module:AM01|matrix:AM01', 'uses' => 'CoaLevelController@showCoaLevel'));
                    Route::post('coaLevel', array('as' => 'coaLevel', 'before' => 'module:AM01|matrix:AM01', 'uses' => 'CoaLevelController@showCoaLevel'));
                    Route::get('exportCoaLevel', array('as' => 'exportCoaLevel', 'before' => 'module:O01|matrix:AM01', 'before' => 'module:O01', 'uses' => 'CoaLevelController@exportExcel'));

                    //Coa 1 routing
                    Route::get('showCoa1', array('as' => 'showCoa1', 'before' => 'module:AM01|matrix:AM01', 'uses' => 'Coa1Controller@showCoa1'));
                    Route::post('insertCoa1', array('as' => 'insertCoa1', 'before' => 'module:AM01|matrix:AM01', 'uses' => 'Coa1Controller@insertCoa1'));
                    Route::post('updateCoa1', array('as' => 'updateCoa1', 'before' => 'module:AM01|matrix:AM01', 'uses' => 'Coa1Controller@updateCoa1'));
                    Route::post('deleteCoa1', array('as' => 'deleteCoa1', 'before' => 'module:AM01|matrix:AM01', 'uses' => 'Coa1Controller@deleteCoa1'));

                    //Coa 2 routing
                    Route::get('showCoa2', array('as' => 'showCoa2', 'before' => 'module:AM01|matrix:AM01', 'uses' => 'Coa2Controller@showCoa2'));
                    Route::post('insertCoa2', array('as' => 'insertCoa2', 'before' => 'module:AM01|matrix:AM01', 'uses' => 'Coa2Controller@insertCoa2'));
                    Route::post('updateCoa2', array('as' => 'updateCoa2', 'before' => 'module:AM01|matrix:AM01', 'uses' => 'Coa2Controller@updateCoa2'));
                    Route::post('deleteCoa2', array('as' => 'deleteCoa2', 'before' => 'module:AM01|matrix:AM01', 'uses' => 'Coa2Controller@deleteCoa2'));

                    //Coa 3 routing
                    Route::get('showCoa3', array('as' => 'showCoa3', 'before' => 'module:AM01|matrix:AM01', 'uses' => 'Coa3Controller@showCoa3'));
                    Route::post('insertCoa3', array('as' => 'insertCoa3', 'before' => 'module:AM01|matrix:AM01', 'uses' => 'Coa3Controller@insertCoa3'));
                    Route::post('updateCoa3', array('as' => 'updateCoa3', 'before' => 'module:AM01|matrix:AM01', 'uses' => 'Coa3Controller@updateCoa3'));
                    Route::post('deleteCoa3', array('as' => 'deleteCoa3', 'before' => 'module:AM01|matrix:AM01', 'uses' => 'Coa3Controller@deleteCoa3'));

                    //Coa 4 routing
                    Route::get('showCoa4', array('as' => 'showCoa4', 'before' => 'module:AM01|matrix:AM01', 'uses' => 'Coa4Controller@showCoa4'));
                    Route::post('insertCoa4', array('as' => 'insertCoa4', 'before' => 'module:AM01|matrix:AM01', 'uses' => 'Coa4Controller@insertCoa4'));
                    Route::post('updateCoa4', array('as' => 'updateCoa4', 'before' => 'module:AM01|matrix:AM01', 'uses' => 'Coa4Controller@updateCoa4'));
                    Route::post('deleteCoa4', array('as' => 'deleteCoa4', 'before' => 'module:AM01|matrix:AM01', 'uses' => 'Coa4Controller@deleteCoa4'));

                    //Coa 5 routing
                    Route::get('showCoa5', array('as' => 'showCoa5', 'before' => 'module:AM01|matrix:AM02', 'uses' => 'Coa5Controller@showCoa5'));
                    Route::post('showCoa5', array('as' => 'showCoa5', 'before' => 'module:AM01|matrix:AM02', 'uses' => 'Coa5Controller@showCoa5'));
                    Route::post('insertCoa5', array('as' => 'insertCoa5', 'before' => 'module:AM01|matrix:AM02', 'uses' => 'Coa5Controller@insertCoa5'));
                    Route::post('updateCoa5', array('as' => 'updateCoa5', 'before' => 'module:AM01|matrix:AM02', 'uses' => 'Coa5Controller@updateCoa5'));
                    Route::post('deleteCoa5', array('as' => 'deleteCoa5', 'before' => 'module:AM01|matrix:AM02', 'uses' => 'Coa5Controller@deleteCoa5'));
                    Route::get('exportCoa5', array('as' => 'exportCoa5', 'before' => 'module:O01|matrix:AM02', 'uses' => 'Coa5Controller@exportExcel'));

                    //Coa 6 routing
                    Route::get('showCoa6', array('as' => 'showCoa6', 'before' => 'module:AM01|matrix:AM03', 'uses' => 'Coa6Controller@showCoa6'));
                    Route::post('showCoa6', array('as' => 'showCoa6', 'before' => 'module:AM01|matrix:AM03', 'uses' => 'Coa6Controller@showCoa6'));
                    Route::post('insertCoa6', array('as' => 'insertCoa6', 'before' => 'module:AM01|matrix:AM03', 'uses' => 'Coa6Controller@insertCoa6'));
                    Route::post('updateCoa6', array('as' => 'updateCoa6', 'before' => 'module:AM01|matrix:AM03', 'uses' => 'Coa6Controller@updateCoa6'));
                    Route::post('deleteCoa6', array('as' => 'deleteCoa6', 'before' => 'module:AM01|matrix:AM03', 'uses' => 'Coa6Controller@deleteCoa6'));
                    Route::get('exportCoa6', array('as' => 'exportCoa6', 'before' => 'module:O01|matrix:AM03', 'uses' => 'Coa6Controller@exportExcel'));
                    Route::get('historyCustomer/{id}', array('as' => 'historyCustomer', 'before' => 'module:O04|matrix:AM03', 'uses' => 'Coa6Controller@historyCustomer'));
                    Route::get('historySupplier/{id}', array('as' => 'historySupplier', 'before' => 'module:O04|matrix:AM03', 'uses' => 'Coa6Controller@historySupplier'));

                    //Coa routing
                    Route::get('showCoa', array('as' => 'showCoa', 'before' => 'module:AM02|matrix:AM04', 'uses' => 'CoaController@showCoa'));
                    Route::post('showCoa', array('as' => 'showCoa', 'before' => 'module:AM02|matrix:AM04', 'uses' => 'CoaController@showCoa'));
                    Route::post('insertCoa', array('as' => 'insertCoa', 'before' => 'module:AM02|matrix:AM04', 'uses' => 'CoaController@insertCoa'));
                    Route::post('updateCoa', array('as' => 'updateCoa', 'before' => 'module:AM02|matrix:AM04', 'uses' => 'CoaController@updateCoa'));
                    Route::post('deleteCoa', array('as' => 'deleteCoa', 'before' => 'module:AM02|matrix:AM04', 'uses' => 'CoaController@deleteCoa'));
                    Route::get('exportCoa', array('as' => 'exportCoa', 'before' => 'module:O01|matrix:AM04', 'uses' => 'CoaController@exportExcel'));

                    //Currency routing
                    Route::get('showCurrency', array('as' => 'showCurrency', 'before' => 'module:SM02|matrix:SM03', 'uses' => 'CurrencyController@showCurrency'));
                    Route::post('showCurrency', array('as' => 'showCurrency', 'before' => 'module:SM02|matrix:SM03', 'uses' => 'CurrencyController@showCurrency'));
                    Route::post('insertCurrency', array('as' => 'insertCurrency', 'before' => 'module:SM02|matrix:SM03', 'uses' => 'CurrencyController@insertCurrency'));
                    Route::post('updateCurrency', array('as' => 'updateCurrency', 'before' => 'module:SM02|matrix:SM03', 'uses' => 'CurrencyController@updateCurrency'));
                    Route::post('deleteCurrency', array('as' => 'deleteCurrency', 'before' => 'module:SM02|matrix:SM03', 'uses' => 'CurrencyController@deleteCurrency'));
                    Route::get('exportCurrency', array('as' => 'exportCurrency', 'before' => 'module:O01|matrix:SM03', 'uses' => 'CurrencyController@exportExcel'));

                    //Slip routing
                    Route::get('showSlip', array('as' => 'showSlip', 'before' => 'module:AM04|matrix:AM07', 'uses' => 'SlipController@showSlip'));
                    Route::post('showSlip', array('as' => 'showSlip', 'before' => 'module:AM04|matrix:AM07', 'uses' => 'SlipController@showSlip'));
                    Route::post('insertSlip', array('as' => 'insertSlip', 'before' => 'module:AM04|matrix:AM07', 'uses' => 'SlipController@insertSlip'));
                    Route::post('updateSlip', array('as' => 'updateSlip', 'before' => 'module:AM04|matrix:AM07', 'uses' => 'SlipController@updateSlip'));
                    Route::post('deleteSlip', array('as' => 'deleteSlip', 'before' => 'module:AM04|matrix:AM07', 'uses' => 'SlipController@deleteSlip'));
                    Route::get('exportSlip', array('as' => 'exportSlip', 'before' => 'module:O01|matrix:AM07', 'uses' => 'SlipController@exportExcel'));

                    //Department routing
                    Route::get('showDepartment', array('as' => 'showDepartment', 'before' => 'module:AM05|matrix:AM08', 'uses' => 'DepartmentController@showDepartment'));
                    Route::post('showDepartment', array('as' => 'showDepartment', 'before' => 'module:AM05|matrix:AM08', 'uses' => 'DepartmentController@showDepartment'));
                    Route::post('insertDepartment', array('as' => 'insertDepartment', 'before' => 'module:AM05|matrix:AM08', 'uses' => 'DepartmentController@insertDepartment'));
                    Route::post('updateDepartment', array('as' => 'updateDepartment', 'before' => 'module:AM05|matrix:AM08', 'uses' => 'DepartmentController@updateDepartment'));
                    Route::post('deleteDepartment', array('as' => 'deleteDepartment', 'before' => 'module:AM05|matrix:AM08', 'uses' => 'DepartmentController@deleteDepartment'));
                    Route::get('exportDepartment', array('as' => 'exportDepartment', 'before' => 'module:O01|matrix:AM08', 'uses' => 'DepartmentController@exportExcel'));

                    //Jurnal routing
                    Route::get('showJournal', array('as' => 'showJournal', 'before' => 'module:AT01|matrix:AT01', 'uses' => 'JournalController@showJournal'));
                    Route::post('showJournal', array('as' => 'showJournal', 'before' => 'module:AT01|matrix:AT01', 'uses' => 'JournalController@showJournal'));
                    Route::get('journalDetail/{id}', array('as' => 'journalDetail', 'before' => 'module:AT01|matrix:AT01', 'uses' => 'JournalController@journalDetail'));
                    Route::get('journalPrint/{id}', array('as' => 'journalPrint', 'before' => 'module:O04|matrix:AT01', 'uses' => 'JournalController@journalPrint'));
                    Route::get('journalUpdate/{id}', array('as' => 'journalUpdate', 'before' => 'module:AT01|matrix:AT01', 'uses' => 'JournalController@journalUpdate'));
                    Route::post('journalUpdate/{id}', array('as' => 'journalUpdate', 'before' => 'module:AT01|matrix:AT01', 'uses' => 'JournalController@journalUpdate'));
                    Route::get('journalNew/Memorial', array('as' => 'journalNew/Memorial', 'before' => 'module:AT01|matrix:AT01', 'uses' => 'JournalController@journalNewMemorial'));
                    Route::post('journalNew/Memorial', array('as' => 'journalNew/Memorial', 'before' => 'module:AT01|matrix:AT01', 'uses' => 'JournalController@journalNewMemorial'));
                    Route::get('journalNew/{jenis}', array('as' => 'journalNew', 'before' => 'module:AT01|matrix:AT01', 'uses' => 'JournalController@journalNew'));
                    Route::post('journalNew/{jenis}', array('as' => 'journalNew', 'before' => 'module:AT01|matrix:AT01', 'uses' => 'JournalController@journalNew'));
                    Route::post('formatCariIDJournal', array('as' => 'formatCariIDJournal', 'before' => 'module:AT01|matrix:AT01', 'uses' => 'JournalController@formatCariIDJournal'));
                    Route::post('formatCariIDJournalMemorial', array('as' => 'formatCariIDJournalMemorial', 'before' => 'module:AT01|matrix:AT01', 'uses' => 'JournalController@formatCariIDJournalMemorial'));
                    Route::any('journalDataBackup/{id}', array('as' => 'journalDataBackup', 'before' => '', 'uses' => 'JournalController@journalDataBackup'));

                    Route::get('getPayable', array('as' => 'getPayable', 'before' => 'module:AT01|matrix:AT01', 'uses' => 'JournalController@getPayable'));
                    Route::get('getReceivable', array('as' => 'getReceivable', 'before' => 'module:AT01|matrix:AT01', 'uses' => 'JournalController@getReceivable'));

                    //GroupDepreciation routing
                    Route::get('showGroupDepreciation', array('as' => 'showGroupDepreciation', 'before' => 'module:AM03|matrix:AM06', 'uses' => 'GroupDepreciationController@showGroupDepreciation'));
                    Route::post('showGroupDepreciation', array('as' => 'showGroupDepreciation', 'before' => 'module:AM03|matrix:AM06', 'uses' => 'GroupDepreciationController@showGroupDepreciation'));
                    Route::post('insertGroupDepreciation', array('as' => 'insertGroupDepreciation', 'before' => 'module:AM03|matrix:AM06', 'uses' => 'GroupDepreciationController@insertGroupDepreciation'));
                    Route::post('updateGroupDepreciation', array('as' => 'updateGroupDepreciation', 'before' => 'module:AM03|matrix:AM06', 'uses' => 'GroupDepreciationController@updateGroupDepreciation'));
                    Route::post('deleteGroupDepreciation', array('as' => 'deleteGroupDepreciation', 'before' => 'module:AM03|matrix:AM06', 'uses' => 'GroupDepreciationController@deleteGroupDepreciation'));
                    Route::get('exportGroupDepreciation', array('as' => 'exportGroupDepreciation', 'before' => 'module:O01|matrix:AM06', 'uses' => 'GroupDepreciationController@exportExcel'));

                    //Depreciation
                    Route::get('showDepreciation', array('as' => 'showDepreciation', 'before' => 'module:AM03|matrix:AM05', 'uses' => 'DepreciationController@showDepreciation'));
                    Route::post('showDepreciation', array('as' => 'showDepreciation', 'before' => 'module:AM03|matrix:AM05', 'uses' => 'DepreciationController@showDepreciation'));
                    Route::get('depreciationDetail/{id}', array('as' => 'depreciationDetail', 'before' => 'module:AM03|matrix:AM05', 'uses' => 'DepreciationController@depreciationDetail'));
                    Route::get('depreciationUpdate/{id}', array('as' => 'depreciationUpdate', 'before' => 'module:AM03|matrix:AM05', 'uses' => 'DepreciationController@depreciationUpdate'));
                    Route::post('depreciationUpdate/{id}', array('as' => 'depreciationUpdate', 'before' => 'module:AM03|matrix:AM05', 'uses' => 'DepreciationController@depreciationUpdate'));
                    Route::get('depreciationNew', array('as' => 'depreciationNew', 'before' => 'module:AM03|matrix:AM05', 'uses' => 'DepreciationController@depreciationNew'));
                    Route::post('depreciationNew', array('as' => 'depreciationNew', 'before' => 'module:AM03|matrix:AM05', 'uses' => 'DepreciationController@depreciationNew'));
                    Route::post('tableDepreciation', array('as' => 'tableDepreciation', 'before' => 'module:AM03|matrix:AM05', 'uses' => 'DepreciationController@tableDepreciation'));

                    //Inventory routing
                    Route::get('showInventory', array('as' => 'showInventory', 'before' => 'module:SM01|matrix:SM01', 'uses' => 'InventoryController@showInventory'));
                    Route::post('showInventory', array('as' => 'showInventory', 'before' => 'module:SM01|matrix:SM01', 'uses' => 'InventoryController@showInventory'));
                    Route::post('insertInventory', array('as' => 'insertInventory', 'before' => 'module:SM01|matrix:SM01', 'uses' => 'InventoryController@insertInventory'));
                    Route::post('updateInventory', array('as' => 'updateInventory', 'before' => 'module:SM01|matrix:SM01', 'uses' => 'InventoryController@updateInventory'));
                    Route::post('deleteInventory', array('as' => 'deleteInventory', 'before' => 'module:SM01|matrix:SM01', 'uses' => 'InventoryController@deleteInventory'));
                    Route::get('exportInventory', array('as' => 'exportInventory', 'before' => 'module:O01|matrix:SM01', 'uses' => 'InventoryController@exportExcel'));
                    Route::get('historyPrice/{id}', array('as' => 'historyPrice', 'before' => 'module:O04|matrix:SM01', 'uses' => 'InventoryController@historyPrice'));
                    Route::get('inventoryDataBackup', array('as' => 'inventoryDataBackup', 'before' => '', 'uses' => 'InventoryController@inventoryDataBackup'));
                    Route::post('inventoryDataBackup', array('as' => 'inventoryDataBackup', 'before' => '', 'uses' => 'InventoryController@inventoryDataBackup'));
                    Route::get('checkInventoryID', array('as' => 'checkInventoryID', 'before' => '', 'uses' => 'InventoryController@checkInventoryID'));
                    Route::post('checkInventoryID', array('as' => 'checkInventoryID', 'before' => '', 'uses' => 'InventoryController@checkInventoryID'));

                    //inventory type, 
                    // url, as =  nama panggilnya pas di header-footer.blade, uses = controlenya @ fungsinya pada cotroler tersebut
                    Route::get('showInventoryType', array('as' => 'showInventoryType', 'before' => 'module:SM01|matrix:SM02', 'uses' => 'InventoryTypeController@showInventoryType'));
                    Route::post('showInventoryType', array('as' => 'showInventoryType', 'before' => 'module:SM01|matrix:SM02', 'uses' => 'InventoryTypeController@showInventoryType'));
                    Route::post('insertInventoryType', array('as' => 'insertInventoryType', 'before' => 'module:SM01|matrix:SM02', 'uses' => 'InventoryTypeController@insertInventoryType'));
                    Route::post('updateInventoryType', array('as' => 'updateInventoryType', 'before' => 'module:SM01|matrix:SM02', 'uses' => 'InventoryTypeController@updateInventoryType'));
                    Route::post('deleteInventoryType', array('as' => 'deleteInventoryType', 'before' => 'module:SM01|matrix:SM02', 'uses' => 'InventoryTypeController@deleteInventoryType'));
                    Route::get('exportInventoryType', array('as' => 'exportInventoryType', 'before' => 'module:O01|matrix:SM02', 'uses' => 'InventoryTypeController@exportExcel'));

                    //ware house
                    Route::get('showWarehouse', array('as' => 'showWarehouse', 'before' => 'module:SM03|matrix:SM04', 'uses' => 'WarehouseController@showWarehouse'));
                    Route::post('showWarehouse', array('as' => 'showWarehouse', 'before' => 'module:SM03|matrix:SM04', 'uses' => 'WarehouseController@showWarehouse'));
                    Route::post('insertWarehouse', array('as' => 'insertWarehouse', 'before' => 'module:SM03|matrix:SM04', 'uses' => 'WarehouseController@insertWarehouse'));
                    Route::post('updateWarehouse', array('as' => 'updateWarehouse', 'before' => 'module:SM03|matrix:SM04', 'uses' => 'WarehouseController@updateWarehouse'));
                    Route::post('deleteWarehouse', array('as' => 'deleteWarehouse', 'before' => 'module:SM03|matrix:SM04', 'uses' => 'WarehouseController@deleteWarehouse'));
                    Route::get('exportWarehouse', array('as' => 'exportWarehouse', 'before' => 'module:O01|matrix:SM04', 'uses' => 'WarehouseController@exportExcel'));

                    //utility routing                                                
                    Route::get('showReportAccounting', array('as' => 'showReportAccounting', 'before' => 'module:AR01|matrix:AR01', 'uses' => 'ReportAccountingController@showReportAccounting'));
                    Route::post('showReportAccounting', array('as' => 'showReportAccounting', 'before' => 'module:AR01|matrix:AR01', 'uses' => 'ReportAccountingController@showReportAccounting'));

                    if (!checkModul('AST01')) {
                        //Sales routing
                        Route::get('showSales', array('as' => 'showSales', 'before' => 'module:ST01|matrix:ST02', 'uses' => 'SalesController@showSales'));
                        Route::post('showSales', array('as' => 'showSales', 'before' => 'module:ST01|matrix:ST02', 'uses' => 'SalesController@showSales'));
                        Route::get('salesDetail/{id}', array('as' => 'salesDetail', 'before' => 'module:ST01|matrix:ST02', 'uses' => 'SalesController@salesDetail'));
                        Route::post('salesDetail/{id}', array('as' => 'salesDetail', 'before' => 'module:ST01|matrix:ST02', 'uses' => 'SalesController@salesDetail'));
                        Route::get('salesCSV/{id}', array('as' => 'salesCSV', 'before' => 'module:O05|matrix:ST02', 'uses' => 'SalesController@salesCSV'));
                        Route::get('salesPrint/{id}', array('as' => 'salesPrint', 'before' => 'module:O04|matrix:ST02', 'uses' => 'SalesController@salesPrint'));
                        Route::get('salesPrintSJ/{id}', array('as' => 'salesPrintSJ', 'before' => 'module:O04|matrix:ST02', 'uses' => 'SalesController@salesPrintSJ'));
                        Route::get('salesUpdate/{id}', array('as' => 'salesUpdate', 'before' => 'module:ST01|matrix:ST02', 'uses' => 'SalesController@salesUpdate'));
                        Route::post('salesUpdate/{id}', array('as' => 'salesUpdate', 'before' => 'module:ST01|matrix:ST02', 'uses' => 'SalesController@salesUpdate'));
                        Route::get('salesNew', array('as' => 'salesNew', 'before' => 'module:ST01|matrix:ST02', 'uses' => 'SalesController@salesNew'));
                        Route::post('salesNew', array('as' => 'salesNew', 'before' => 'module:ST01|matrix:ST02', 'uses' => 'SalesController@salesNew'));
                        Route::post('formatCariIDSales', array('as' => 'formatCariIDSales', 'before' => 'module:ST01|matrix:ST02', 'uses' => 'SalesController@formatCariIDSales'));
                        Route::any('salesDataBackup/{id}', array('as' => 'salesDataBackup', 'before' => '', 'uses' => 'SalesController@salesDataBackup'));

                        //Purchase routing
                        Route::get('showPurchase', array('as' => 'showPurchase', 'before' => 'module:ST03|matrix:ST05', 'uses' => 'PurchaseController@showPurchase'));
                        Route::post('showPurchase', array('as' => 'showPurchase', 'before' => 'module:ST03|matrix:ST05', 'uses' => 'PurchaseController@showPurchase'));
                        Route::get('purchaseDetail/{id}', array('as' => 'purchaseDetail', 'before' => 'module:ST03|matrix:ST05', 'uses' => 'PurchaseController@purchaseDetail'));
                        Route::post('purchaseDetail/{id}', array('as' => 'purchaseDetail', 'before' => 'module:ST03|matrix:ST05', 'uses' => 'PurchaseController@purchaseDetail'));
                        Route::get('purchasePrint/{id}', array('as' => 'purchasePrint', 'before' => 'module:O04|matrix:ST05', 'uses' => 'PurchaseController@purchasePrint'));
                        Route::get('purchasePrintSJ/{id}', array('as' => 'purchasePrintSJ', 'before' => 'module:O04|matrix:ST05', 'uses' => 'PurchaseController@purchasePrintSJ'));
                        Route::get('purchaseCSV/{id}', array('as' => 'purchaseCSV', 'before' => 'module:O05|matrix:ST05', 'uses' => 'PurchaseController@purchaseCSV'));
                        Route::get('purchaseUpdate/{id}', array('as' => 'purchaseUpdate', 'before' => 'module:ST03|matrix:ST05', 'uses' => 'PurchaseController@purchaseUpdate'));
                        Route::post('purchaseUpdate/{id}', array('as' => 'purchaseUpdate', 'before' => 'module:ST03|matrix:ST05', 'uses' => 'PurchaseController@purchaseUpdate'));
                        Route::get('purchaseNew', array('as' => 'purchaseNew', 'before' => 'module:ST03|matrix:ST05', 'uses' => 'PurchaseController@purchaseNew'));
                        Route::post('purchaseNew', array('as' => 'purchaseNew', 'before' => 'module:ST03|matrix:ST05', 'uses' => 'PurchaseController@purchaseNew'));
                        Route::post('formatCariIDPurchase', array('as' => 'formatCariIDPurchase', 'before' => 'module:ST03|matrix:ST05', 'uses' => 'PurchaseController@formatCariIDPurchase'));
                        Route::any('purchaseDataBackup/{id}', array('as' => 'purchaseDataBackup', 'before' => '', 'uses' => 'PurchaseController@purchaseDataBackup'));
                    } else {
                        //Sales routing
                        Route::get('showSales', array('as' => 'showSales', 'before' => 'module:ST01|matrix:ST02', 'uses' => 'SalesAddController@showSales'));
                        Route::post('showSales', array('as' => 'showSales', 'before' => 'module:ST01|matrix:ST02', 'uses' => 'SalesAddController@showSales'));
                        Route::get('salesDetail/{id}', array('as' => 'salesDetail', 'before' => 'module:ST01|matrix:ST02', 'uses' => 'SalesAddController@salesDetail'));
                        Route::post('salesDetail/{id}', array('as' => 'salesDetail', 'before' => 'module:ST01|matrix:ST02', 'uses' => 'SalesAddController@salesDetail'));
                        Route::get('salesPrint/{id}', array('as' => 'salesPrint', 'before' => 'module:O04|matrix:ST02', 'uses' => 'SalesAddController@salesPrint'));
                        Route::get('salesPrintSJ/{id}', array('as' => 'salesPrintSJ', 'before' => 'module:O04|matrix:ST02', 'uses' => 'SalesAddController@salesPrintSJ'));
                        Route::get('salesCSV/{id}', array('as' => 'salesCSV', 'before' => 'module:O05|matrix:ST02', 'uses' => 'SalesAddController@salesCSV'));
                        Route::get('salesUpdate/{id}', array('as' => 'salesUpdate', 'before' => 'module:ST01|matrix:ST02', 'uses' => 'SalesAddController@salesUpdate'));
                        Route::post('salesUpdate/{id}', array('as' => 'salesUpdate', 'before' => 'module:ST01|matrix:ST02', 'uses' => 'SalesAddController@salesUpdate'));
                        Route::get('salesNew/{id}', array('as' => 'salesNew', 'before' => 'module:ST01|matrix:ST02', 'uses' => 'SalesAddController@salesNew'));
                        Route::post('salesNew/{id}', array('as' => 'salesNew', 'before' => 'module:ST01|matrix:ST02', 'uses' => 'SalesAddController@salesNew'));
                        Route::post('formatCariIDSales', array('as' => 'formatCariIDSales', 'before' => 'module:ST01|matrix:ST02', 'uses' => 'SalesAddController@formatCariIDSales'));
                        Route::any('salesDataBackup/{id}', array('as' => 'salesDataBackup', 'before' => '', 'uses' => 'SalesAddController@salesDataBackup'));

                        //Purchase ADD routing
                        Route::get('showPurchase', array('as' => 'showPurchase', 'before' => 'module:ST03|matrix:ST05', 'uses' => 'PurchaseAddController@showPurchase'));
                        Route::post('showPurchase', array('as' => 'showPurchase', 'before' => 'module:ST03|matrix:ST05', 'uses' => 'PurchaseAddController@showPurchase'));
                        Route::get('purchaseDetail/{id}', array('as' => 'purchaseDetail', 'before' => 'module:ST03|matrix:ST05', 'uses' => 'PurchaseAddController@purchaseDetail'));
                        Route::post('purchaseDetail/{id}', array('as' => 'purchaseDetail', 'before' => 'module:ST03|matrix:ST05', 'uses' => 'PurchaseAddController@purchaseDetail'));
                        Route::get('purchasePrint/{id}', array('as' => 'purchasePrint', 'before' => 'module:O04|matrix:ST05', 'uses' => 'PurchaseAddController@purchasePrint'));
                        Route::get('purchasePrintSJ/{id}', array('as' => 'purchasePrintSJ', 'before' => 'module:O04|matrix:ST05', 'uses' => 'PurchaseAddController@purchasePrintSJ'));
                        Route::get('purchaseCSV/{id}', array('as' => 'purchaseCSV', 'before' => 'module:O05|matrix:ST05', 'uses' => 'PurchaseAddController@purchaseCSV'));
                        Route::get('purchaseUpdate/{id}', array('as' => 'purchaseUpdate', 'before' => 'module:ST03|matrix:ST05', 'uses' => 'PurchaseAddController@purchaseUpdate'));
                        Route::post('purchaseUpdate/{id}', array('as' => 'purchaseUpdate', 'before' => 'module:ST03|matrix:ST05', 'uses' => 'PurchaseAddController@purchaseUpdate'));
                        Route::get('purchaseNew/{id}', array('as' => 'purchaseNew', 'before' => 'module:ST03|matrix:ST05', 'uses' => 'PurchaseAddController@purchaseNew'));
                        Route::post('purchaseNew/{id}', array('as' => 'purchaseNew', 'before' => 'module:ST03|matrix:ST05', 'uses' => 'PurchaseAddController@purchaseNew'));
                        Route::post('formatCariIDPurchase', array('as' => 'formatCariIDPurchase', 'before' => 'module:ST03|matrix:ST05', 'uses' => 'PurchaseAddController@formatCariIDPurchase'));
                        Route::any('purchaseDataBackup/{id}', array('as' => 'purchaseDataBackup', 'before' => '', 'uses' => 'PurchaseAddController@purchaseDataBackup'));
                    }

                    //SalesOrder routing
                    Route::get('showSalesOrder', array('as' => 'showSalesOrder', 'before' => 'module:AST01|matrix:ST01', 'uses' => 'SalesOrderController@showSalesOrder'));
                    Route::post('showSalesOrder', array('as' => 'showSalesOrder', 'before' => 'module:AST01|matrix:ST01', 'uses' => 'SalesOrderController@showSalesOrder'));
                    Route::get('salesOrderDetail/{id}', array('as' => 'salesOrderDetail', 'before' => 'module:AST01|matrix:ST01', 'uses' => 'SalesOrderController@salesOrderDetail'));
                    Route::post('salesOrderDetail/{id}', array('as' => 'salesOrderDetail', 'before' => 'module:AST01|matrix:ST01', 'uses' => 'SalesOrderController@salesOrderDetail'));
                    Route::get('salesOrderPrint/{id}', array('as' => 'salesOrderPrint', 'before' => 'module:O04|matrix:ST01', 'uses' => 'SalesOrderController@salesOrderPrint'));
                    Route::get('salesOrderUpdate/{id}', array('as' => 'salesOrderUpdate', 'before' => 'module:AST01|matrix:ST01', 'uses' => 'SalesOrderController@salesOrderUpdate'));
                    Route::post('salesOrderUpdate/{id}', array('as' => 'salesOrderUpdate', 'before' => 'module:AST01|matrix:ST01', 'uses' => 'SalesOrderController@salesOrderUpdate'));
                    Route::get('salesOrderNew', array('as' => 'salesOrderNew', 'before' => 'module:AST01|matrix:ST01', 'uses' => 'SalesOrderController@salesOrderNew'));
                    Route::post('salesOrderNew', array('as' => 'salesOrderNew', 'before' => 'module:AST01|matrix:ST01', 'uses' => 'SalesOrderController@salesOrderNew'));
                    Route::post('formatCariIDSalesOrder', array('as' => 'formatCariIDSalesOrder', 'before' => 'module:AST01|matrix:ST01', 'uses' => 'SalesOrderController@formatCariIDSalesOrder'));
                    Route::any('salesOrderDataBackup/{id}', array('as' => 'salesOrderDataBackup', 'before' => '', 'uses' => 'SalesOrderController@salesOrderDataBackup'));

                    //Purchase Order routing
                    Route::get('showPurchaseOrder', array('as' => 'showPurchaseOrder', 'before' => 'module:AST01|matrix:ST04', 'uses' => 'PurchaseOrderController@showPurchaseOrder'));
                    Route::post('showPurchaseOrder', array('as' => 'showPurchaseOrder', 'before' => 'module:AST01|matrix:ST04', 'uses' => 'PurchaseOrderController@showPurchaseOrder'));
                    Route::get('purchaseOrderDetail/{id}', array('as' => 'purchaseOrderDetail', 'before' => 'module:AST01|matrix:ST04', 'uses' => 'PurchaseOrderController@purchaseOrderDetail'));
                    Route::post('purchaseOrderDetail/{id}', array('as' => 'purchaseOrderDetail', 'before' => 'module:AST01|matrix:ST04', 'uses' => 'PurchaseOrderController@purchaseOrderDetail'));
                    Route::get('purchaseOrderPrint/{id}', array('as' => 'purchaseOrderPrint', 'before' => 'module:O04|matrix:ST04', 'uses' => 'PurchaseOrderController@purchaseOrderPrint'));
                    Route::get('purchaseOrderUpdate/{id}', array('as' => 'purchaseOrderUpdate', 'before' => 'module:AST01|matrix:ST04', 'uses' => 'PurchaseOrderController@purchaseOrderUpdate'));
                    Route::post('purchaseOrderUpdate/{id}', array('as' => 'purchaseOrderUpdate', 'before' => 'module:AST01|matrix:ST04', 'uses' => 'PurchaseOrderController@purchaseOrderUpdate'));
                    Route::get('purchaseOrderNew', array('as' => 'purchaseOrderNew', 'before' => 'module:AST01|matrix:ST04', 'uses' => 'PurchaseOrderController@purchaseOrderNew'));
                    Route::post('purchaseOrderNew', array('as' => 'purchaseOrderNew', 'before' => 'module:AST01|matrix:ST04', 'uses' => 'PurchaseOrderController@purchaseOrderNew'));
                    Route::post('formatCariIDPurchaseOrder', array('as' => 'formatCariIDPurchaseOrder', 'before' => 'module:AST01|matrix:ST04', 'uses' => 'PurchaseOrderController@formatCariIDPurchaseOrder'));
                    Route::any('purchaseOrderDataBackup/{id}', array('as' => 'purchaseOrderDataBackup', 'before' => '', 'uses' => 'PurchaseOrderController@purchaseOrderDataBackup'));

                    //SalesReturn routing
                    Route::get('showSalesReturn', array('as' => 'showSalesReturn', 'before' => 'module:ST02|matrix:ST03', 'uses' => 'SalesReturnController@showSalesReturn'));
                    Route::post('showSalesReturn', array('as' => 'showSalesReturn', 'before' => 'module:ST02|matrix:ST03', 'uses' => 'SalesReturnController@showSalesReturn'));
                    Route::get('salesReturnDetail/{id}', array('as' => 'salesReturnDetail', 'before' => 'module:ST02|matrix:ST03', 'uses' => 'SalesReturnController@salesReturnDetail'));
                    Route::post('salesReturnDetail/{id}', array('as' => 'salesReturnDetail', 'before' => 'module:ST02|matrix:ST03', 'uses' => 'SalesReturnController@salesReturnDetail'));
                    Route::get('salesReturnPrint/{id}', array('as' => 'salesReturnPrint', 'before' => 'module:O04|matrix:ST03', 'uses' => 'SalesReturnController@salesReturnPrint'));
                    Route::get('salesReturnUpdate/{id}', array('as' => 'salesReturnUpdate', 'before' => 'module:ST02|matrix:ST03', 'uses' => 'SalesReturnController@salesReturnUpdate'));
                    Route::post('salesReturnUpdate/{id}', array('as' => 'salesReturnUpdate', 'before' => 'module:ST02|matrix:ST03', 'uses' => 'SalesReturnController@salesReturnUpdate'));
                    Route::get('salesReturnNew/{id}', array('as' => 'salesReturnNew', 'before' => 'module:ST02|matrix:ST03', 'uses' => 'SalesReturnController@salesReturnNew'));
                    Route::post('salesReturnNew/{id}', array('as' => 'salesReturnNew', 'before' => 'module:ST02|matrix:ST03', 'uses' => 'SalesReturnController@salesReturnNew'));
                    Route::post('formatCariIDSalesReturn', array('as' => 'formatCariIDSalesReturn', 'before' => 'module:ST02|matrix:ST03', 'uses' => 'SalesReturnController@formatCariIDSalesReturn'));
                    Route::any('salesReturnDataBackup/{id}', array('as' => 'salesReturnDataBackup', 'before' => '', 'uses' => 'SalesReturnController@salesReturnDataBackup'));

                    //PurchaseReturn routing
                    Route::get('showPurchaseReturn', array('as' => 'showPurchaseReturn', 'before' => 'module:ST04|matrix:ST06', 'uses' => 'PurchaseReturnController@showPurchaseReturn'));
                    Route::post('showPurchaseReturn', array('as' => 'showPurchaseReturn', 'before' => 'module:ST04|matrix:ST06', 'uses' => 'PurchaseReturnController@showPurchaseReturn'));
                    Route::get('purchaseReturnDetail/{id}', array('as' => 'purchaseReturnDetail', 'before' => 'module:ST04|matrix:ST06', 'uses' => 'PurchaseReturnController@purchaseReturnDetail'));
                    Route::post('purchaseReturnDetail/{id}', array('as' => 'purchaseReturnDetail', 'before' => 'module:ST04|matrix:ST06', 'uses' => 'PurchaseReturnController@purchaseReturnDetail'));
                    Route::get('purchaseReturnPrint/{id}', array('as' => 'purchaseReturnPrint', 'before' => 'module:O04|matrix:ST06', 'uses' => 'PurchaseReturnController@purchaseReturnPrint'));
                    Route::get('purchaseReturnUpdate/{id}', array('as' => 'purchaseReturnUpdate', 'before' => 'module:ST04|matrix:ST06', 'uses' => 'PurchaseReturnController@purchaseReturnUpdate'));
                    Route::post('purchaseReturnUpdate/{id}', array('as' => 'purchaseReturnUpdate', 'before' => 'module:ST04|matrix:ST06', 'uses' => 'PurchaseReturnController@purchaseReturnUpdate'));
                    Route::get('purchaseReturnNew/{id}', array('as' => 'purchaseReturnNew', 'before' => 'module:ST04|matrix:ST06', 'uses' => 'PurchaseReturnController@purchaseReturnNew'));
                    Route::post('purchaseReturnNew/{id}', array('as' => 'purchaseReturnNew', 'before' => 'module:ST04|matrix:ST06', 'uses' => 'PurchaseReturnController@purchaseReturnNew'));
                    Route::post('formatCariIDPurchaseReturn', array('as' => 'formatCariIDPurchaseReturn', 'before' => 'module:ST04|matrix:ST06', 'uses' => 'PurchaseReturnController@formatCariIDPurchaseReturn'));
                    Route::any('purchaseReturnDataBackup/{id}', array('as' => 'purchaseReturnDataBackup', 'before' => '', 'uses' => 'PurchaseReturnController@purchaseReturnDataBackup'));

                    //setting routing                                                
                    Route::get('settProfile', array('as' => 'settProfile', 'before' => 'matrix:SS01', 'uses' => 'UserController@settProfile'));
                    Route::post('settProfile', array('as' => 'settProfile', 'before' => 'matrix:SS01', 'uses' => 'UserController@settProfile'));
                    Route::get('settCompany', array('as' => 'settCompany', 'before' => 'matrix:SS02', 'uses' => 'CompanyController@settProfileCompany'));
                    Route::post('settCompany', array('as' => 'settCompany', 'before' => 'matrix:SS02', 'uses' => 'CompanyController@settProfileCompany'));

                    //MemoIn routing
                    Route::get('showMemoIn', array('as' => 'showMemoIn', 'before' => 'module:ST06|matrix:ST08', 'uses' => 'MemoInController@showMemoIn'));
                    Route::post('showMemoIn', array('as' => 'showMemoIn', 'before' => 'module:ST06|matrix:ST08', 'uses' => 'MemoInController@showMemoIn'));
                    Route::get('memoInDetail/{id}', array('as' => 'memoInDetail', 'before' => 'module:ST06|matrix:ST08', 'uses' => 'MemoInController@memoInDetail'));
                    Route::post('memoInDetail/{id}', array('as' => 'memoInDetail', 'before' => 'module:ST06|matrix:ST08', 'uses' => 'MemoInController@memoInDetail'));
                    Route::get('memoInPrint/{id}', array('as' => 'memoInPrint', 'before' => 'module:O04|matrix:ST08', 'uses' => 'MemoInController@memoInPrint'));
                    Route::get('memoInUpdate/{id}', array('as' => 'memoInUpdate', 'before' => 'module:ST06|matrix:ST08', 'uses' => 'MemoInController@memoInUpdate'));
                    Route::post('memoInUpdate/{id}', array('as' => 'memoInUpdate', 'before' => 'module:ST06|matrix:ST08', 'uses' => 'MemoInController@memoInUpdate'));
                    Route::get('memoInNew', array('as' => 'memoInNew', 'before' => 'module:ST06|matrix:ST08', 'uses' => 'MemoInController@memoInNew'));
                    Route::post('memoInNew', array('as' => 'memoInNew', 'before' => 'module:ST06|matrix:ST08', 'uses' => 'MemoInController@memoInNew'));
                    Route::post('formatCariIDMemoIn', array('as' => 'formatCariIDMemoIn', 'before' => 'module:ST06|matrix:ST08', 'uses' => 'MemoInController@formatCariIDMemoIn'));
                    Route::get('getHPPValueInventoryMemoIn', array('as' => 'getHPPValueInventoryMemoIn', 'uses' => 'MemoInController@getHPPValueInventoryMemoIn'));
                    Route::post('getHPPValueInventoryMemoIn', array('as' => 'getHPPValueInventoryMemoIn', 'uses' => 'MemoInController@getHPPValueInventoryMemoIn'));

                    //MemoOut routing
                    Route::get('showMemoOut', array('as' => 'showMemoOut', 'before' => 'module:ST06|matrix:ST09', 'uses' => 'MemoOutController@showMemoOut'));
                    Route::post('showMemoOut', array('as' => 'showMemoOut', 'before' => 'module:ST06|matrix:ST09', 'uses' => 'MemoOutController@showMemoOut'));
                    Route::get('memoOutDetail/{id}', array('as' => 'memoOutDetail', 'before' => 'module:ST06|matrix:ST09', 'uses' => 'MemoOutController@memoOutDetail'));
                    Route::post('memoOutDetail/{id}', array('as' => 'memoOutDetail', 'before' => 'module:ST06|matrix:ST09', 'uses' => 'MemoOutController@memoOutDetail'));
                    Route::get('memoOutPrint/{id}', array('as' => 'memoOutPrint', 'before' => 'module:O04|matrix:ST09', 'uses' => 'MemoOutController@memoOutPrint'));
                    Route::get('memoOutUpdate/{id}', array('as' => 'memoOutUpdate', 'before' => 'module:ST06|matrix:ST09', 'uses' => 'MemoOutController@memoOutUpdate'));
                    Route::post('memoOutUpdate/{id}', array('as' => 'memoOutUpdate', 'before' => 'module:ST06|matrix:ST09', 'uses' => 'MemoOutController@memoOutUpdate'));
                    Route::get('memoOutNew', array('as' => 'memoOutNew', 'before' => 'module:ST06|matrix:ST09', 'uses' => 'MemoOutController@memoOutNew'));
                    Route::post('memoOutNew', array('as' => 'memoOutNew', 'before' => 'module:ST06|matrix:ST09', 'uses' => 'MemoOutController@memoOutNew'));
                    Route::post('formatCariIDMemoOut', array('as' => 'formatCariIDMemoOut', 'before' => 'module:ST06|matrix:ST09', 'uses' => 'MemoOutController@formatCariIDMemoOut'));
                    Route::get('getHPPValueInventoryMemoOut', array('as' => 'getHPPValueInventoryMemoOut', 'uses' => 'MemoOutController@getHPPValueInventoryMemoOut'));
                    Route::post('getHPPValueInventoryMemoOut', array('as' => 'getHPPValueInventoryMemoOut', 'uses' => 'MemoOutController@getHPPValueInventoryMemoOut'));

                    //Transfer routing
                    Route::get('showTransfer', array('as' => 'showTransfer', 'before' => 'module:ST05|matrix:ST07', 'uses' => 'TransferController@showTransfer'));
                    Route::post('showTransfer', array('as' => 'showTransfer', 'before' => 'module:ST05|matrix:ST07', 'uses' => 'TransferController@showTransfer'));
                    Route::get('transferDetail/{id}', array('as' => 'transferDetail', 'before' => 'module:ST05|matrix:ST07', 'uses' => 'TransferController@transferDetail'));
                    Route::post('transferDetail/{id}', array('as' => 'transferDetail', 'before' => 'module:ST05|matrix:ST07', 'uses' => 'TransferController@transferDetail'));
                    Route::get('transferPrint/{id}', array('as' => 'transferPrint', 'before' => 'module:O04|matrix:ST07', 'uses' => 'TransferController@transferPrint'));
                    Route::get('transferUpdate/{id}', array('as' => 'transferUpdate', 'before' => 'module:ST05|matrix:ST07', 'uses' => 'TransferController@transferUpdate'));
                    Route::post('transferUpdate/{id}', array('as' => 'transferUpdate', 'before' => 'module:ST05|matrix:ST07', 'uses' => 'TransferController@transferUpdate'));
                    Route::get('transferNew', array('as' => 'transferNew', 'before' => 'module:ST05|matrix:ST07', 'uses' => 'TransferController@transferNew'));
                    Route::post('transferNew', array('as' => 'transferNew', 'before' => 'module:ST05|matrix:ST07', 'uses' => 'TransferController@transferNew'));
                    Route::post('formatCariIDTransfer', array('as' => 'formatCariIDTransfer', 'before' => 'module:ST05|matrix:ST07', 'uses' => 'TransferController@formatCariIDTransfer'));

                    //utility routing                                                
                    Route::get('showMonthlyProcess', array('as' => 'showMonthlyProcess', 'before' => 'module:AT02|matrix:MP01', 'uses' => 'MonthlyProcessController@showMonthlyProcess'));
                    Route::post('showMonthlyProcess', array('as' => 'showMonthlyProcess', 'before' => 'module:AT02|matrix:MP01', 'uses' => 'MonthlyProcessController@showMonthlyProcess'));

                    //help
                    Route::get('help', array('as' => 'showHelp', 'uses' => 'HomeController@showHelp'));
                    Route::post('help', array('as' => 'showHelp', 'uses' => 'HomeController@showHelp'));

                    //change log
                    Route::get('showChangeLog', array('as' => 'showChangeLog', 'uses' => 'HomeController@showChangeLog'));
                    Route::post('showChangeLog', array('as' => 'showChangeLog', 'uses' => 'HomeController@showChangeLog'));

                    //default account
                    Route::get('showDefaultAccount', array('as' => 'showDefaultAccount', 'before' => 'module:AS01|matrix:SS03', 'uses' => 'DefaultAccountController@showDefaultAccount'));
                    Route::post('showDefaultAccount', array('as' => 'showDefaultAccount', 'before' => 'module:AS01|matrix:SS03', 'uses' => 'DefaultAccountController@showDefaultAccount'));

                    //routing keduanya
                    //User routing
                    Route::get('showUser', array('as' => 'showUser', 'before' => 'module:SM04|matrix:SM05', 'uses' => 'UserController@showUser'));
                    Route::post('showUser', array('as' => 'showUser', 'before' => 'module:SM04|matrix:SM05', 'uses' => 'UserController@showUser'));
                    Route::post('insertUser', array('as' => 'insertUser', 'before' => 'module:SM04|matrix:SM05', 'uses' => 'UserController@insertUser'));
                    Route::post('updateUser', array('as' => 'updateUser', 'before' => 'module:SM04|matrix:SM05', 'uses' => 'UserController@updateUser'));
                    Route::post('deleteUser', array('as' => 'deleteUser', 'before' => 'module:SM04|matrix:SM05', 'uses' => 'UserController@deleteUser'));
                    Route::get('exportUser', array('as' => 'exportUser', 'before' => 'module:O01|matrix:SM05', 'uses' => 'UserController@exportExcel'));

                    Route::get('showUserMatrix/{id}', array('as' => 'showUserMatrix', 'before' => 'module:SM05|userAdmin|matrix:SM05', 'uses' => 'UserController@showUserMatrix'));
                    Route::post('showUserMatrix/{id}', array('as' => 'showUserMatrix', 'before' => 'module:SM05|userAdmin|matrix:SM05', 'uses' => 'UserController@showUserMatrix'));


                    //UOM
                    Route::get('showUom', array('as' => 'showUom', 'before' => 'module:SM06|matrix:SM06', 'uses' => 'UomController@showUom'));
                    Route::post('showUom', array('as' => 'showUom', 'before' => 'module:SM06|matrix:SM06', 'uses' => 'UomController@showUom'));
                    Route::get('exportUom', array('as' => 'exportUom', 'before' => 'module:O01|matrix:SM06', 'uses' => 'UomController@exportUom'));

                    //Inventory Uom
                    Route::get('showInventoryUom', array('as' => 'showInventoryUom', 'before' => 'module:SM06|matrix:SM07', 'uses' => 'InventoryUomController@showInventoryUom'));
                    Route::post('showInventoryUom', array('as' => 'showInventoryUom', 'before' => 'module:SM06|matrix:SM07', 'uses' => 'InventoryUomController@showInventoryUom'));
                    Route::get('exportInventoryUom', array('as' => 'exportInventoryUom', 'before' => 'module:O01|matrix:SM07', 'uses' => 'InventoryUomController@exportInventoryUom'));
                    
                    Route::post('getResultSearchSR', array('as' => 'getResultSearchSR', 'before' => '', 'uses' => 'SalesReturnController@getResultSearchSR'));
                    Route::post('getResultSearchPR', array('as' => 'getResultSearchPR', 'before' => '', 'uses' => 'PurchaseReturnController@getResultSearchPR'));
                    Route::post('getResultSearchS', array('as' => 'getResultSearchS', 'before' => '', 'uses' => 'SalesAddController@getResultSearchS'));
                    Route::post('getResultSearchP', array('as' => 'getResultSearchP', 'before' => '', 'uses' => 'PurchaseAddController@getResultSearchP'));
                });
            });
        });

        Route::group(array('before' => 'agent'), function() {
            //routing khusus agen
            Route::get('showHomeAgent', array('as' => 'showHomeAgent', 'uses' => 'HomeAgentController@showHomeAgent'));
            Route::post('showHomeAgent', array('as' => 'showHomeAgent', 'uses' => 'HomeAgentController@showHomeAgent'));
            Route::get('showContactAgent', array('as' => 'showContactAgent', 'uses' => 'HomeAgentController@showContactAgent'));
            Route::post('showContactAgent', array('as' => 'showContactAgent', 'uses' => 'HomeAgentController@showContactAgent'));
            Route::get('exportCompanyAgent', array('as' => 'exportCompanyAgent', 'uses' => 'HomeAgentController@exportCompanyAgent'));
            Route::post('exportCompanyAgent', array('as' => 'exportCompanyAgent', 'uses' => 'HomeAgentController@exportCompanyAgent'));
            Route::post('getDataHistory', array('as' => 'getDataHistory', 'uses' => 'HomeAgentController@getDataHistory'));
            Route::get('showProfileAgent', array('as' => 'showProfileAgent', 'uses' => 'HomeAgentController@showProfileAgent'));
            Route::post('showProfileAgent', array('as' => 'showProfileAgent', 'uses' => 'HomeAgentController@showProfileAgent'));
            Route::get('showCommissionAgent', array('as' => 'showCommissionAgent', 'uses' => 'HomeAgentController@showCommissionAgent'));
            Route::post('showCommissionAgent', array('as' => 'showCommissionAgent', 'uses' => 'HomeAgentController@showCommissionAgent'));

            Route::get('showWithdrawAgent', array('as' => 'showWithdrawAgent', 'uses' => 'HomeAgentController@showWithdrawAgent'));
            Route::post('showWithdrawAgent', array('as' => 'showWithdrawAgent', 'uses' => 'HomeAgentController@showWithdrawAgent'));
            Route::post('getSaldoWithdraw', array('as' => 'getSaldoWithdraw', 'uses' => 'HomeAgentController@getSaldoWithdraw'));
        });
        Route::group(array('before' => 'superAdmin'), function() {
            //routing khusus superAdmin
            //Company routing
            Route::get('showCompany', array('as' => 'showCompany', 'uses' => 'CompanyController@showCompany'));
            Route::post('showCompany', array('as' => 'showCompany', 'uses' => 'CompanyController@showCompany'));
            Route::post('insertCompany', array('as' => 'insertCompany', 'uses' => 'CompanyController@insertCompany'));
            Route::post('updateCompany', array('as' => 'updateCompany', 'uses' => 'CompanyController@updateCompany'));
            Route::post('deleteCompany', array('as' => 'deleteCompany', 'uses' => 'CompanyController@deleteCompany'));
            Route::get('exportCompany', array('as' => 'exportCompany', 'uses' => 'CompanyController@exportExcel'));
            //Company region
            Route::get('showCompany', array('as' => 'showCompany', 'uses' => 'CompanyController@showCompany'));
            Route::post('showCompany', array('as' => 'showCompany', 'uses' => 'CompanyController@showCompany'));
            Route::post('insertCompany', array('as' => 'insertCompany', 'uses' => 'CompanyController@insertCompany'));
            Route::post('updateCompany', array('as' => 'updateCompany', 'uses' => 'CompanyController@updateCompany'));
            Route::post('deleteCompany', array('as' => 'deleteCompany', 'uses' => 'CompanyController@deleteCompany'));
            Route::get('exportCompany', array('as' => 'exportCompany', 'uses' => 'CompanyController@exportExcel'));
            Route::get('showRegion', array('as' => 'showRegion', 'uses' => 'RegionController@showRegion'));
            Route::post('showRegion', array('as' => 'showRegion', 'uses' => 'RegionController@showRegion'));
            Route::post('insertRegion', array('as' => 'insertRegion', 'uses' => 'RegionController@insertRegion'));
            Route::post('updateRegion', array('as' => 'updateRegion', 'uses' => 'RegionController@updateRegion'));
            Route::post('deleteRegion', array('as' => 'deleteRegion', 'uses' => 'RegionController@deleteRegion'));
            Route::get('exportRegion', array('as' => 'exportRegion', 'uses' => 'RegionController@exportExcel'));

            Route::get('showCompanyProcess', array('as' => 'showCompanyProcess', 'uses' => 'CompanyController@showCompanyProcess'));
            Route::post('showCompanyProcess', array('as' => 'showCompanyProcess', 'uses' => 'CompanyController@showCompanyProcess'));


            Route::get('showAgent', array('as' => 'showAgent', 'uses' => 'AgentController@showAgent'));
            Route::post('showAgent', array('as' => 'showAgen', 'uses' => 'AgentController@showAgent'));
            Route::get('exportAgent', array('as' => 'exportAgent', 'uses' => 'AgentController@exportAgent'));

            Route::get('showAgentWithdraw', array('as' => 'showAgentWithdraw', 'uses' => 'AgentController@showAgentWithdraw'));
            Route::post('showAgentWithdraw', array('as' => 'showAgenWithdraw', 'uses' => 'AgentController@showAgentWithdraw'));

            if (Auth::check()) {
                if (Auth::user()->CompanyInternalID == -1) {
                    //routing keduanya
                    //User routing
                    Route::get('showUser', array('as' => 'showUser', 'before' => 'module:SM04', 'uses' => 'UserController@showUser'));
                    Route::post('showUser', array('as' => 'showUser', 'before' => 'module:SM04', 'uses' => 'UserController@showUser'));
                    Route::post('insertUser', array('as' => 'insertUser', 'before' => 'module:SM04', 'uses' => 'UserController@insertUser'));
                    Route::post('updateUser', array('as' => 'updateUser', 'before' => 'module:SM04', 'uses' => 'UserController@updateUser'));
                    Route::post('deleteUser', array('as' => 'deleteUser', 'before' => 'module:SM04', 'uses' => 'UserController@deleteUser'));
                    Route::get('exportUser', array('as' => 'exportUser', 'before' => 'module:O01', 'uses' => 'UserController@exportExcel'));
                }
            }
        });

        //contact
        Route::get('showContact', array('as' => 'showContact', 'uses' => 'HomeController@showContact'));
        Route::post('showContact', array('as' => 'showContact', 'uses' => 'HomeController@showContact'));
    });
});
