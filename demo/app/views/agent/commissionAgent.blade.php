@extends('template.header-footer-agent')

@section('title')
Company
@stop

@section('css')
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
@stop

@section('nav')

@stop

@section('content')
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread" role="group">
                <a href="{{route('showHomeAgent')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showCommissionAgent')}}" type="button" class="btn btn-sm btn-pure">Commission</a>
            </div>
            <button type="button" class="btn btn-green btn-insert" data-target="#r_commission" data-toggle="modal" role="dialog">
                Commission Report</button>
        </div>
        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Commission</h4>
            </div>            
            <div class="tableadd">
                <table id="example" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Company ID</th>
                            <th>Name</th>
                            <th>Payment ID</th>
                            <th>Percentage(%)</th>
                            <th>Commission</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $f = rand(0, 50);
                        foreach (Payment::join("m_company", "m_company.InternalID", "=", "CompanyInternalID")->where("m_company.AgentInternalID", Auth::user()->InternalID)->get() as $data) {
                            $data->dtRecordformat = date("d-m-Y H:i:s", strtotime($data->dtRecord));
                            $data->dtModifformat = date("d-m-Y H:i:s", strtotime($data->dtModified));
                            $data->Addressformat = str_replace("\r\n", "&#013", $data->Address);
                            $arrData = array($data);
                            $tamp = myEscapeStringData($arrData);
                            $tamp = myEncryptJavaScriptText($tamp, $f);
                            ?>
                            <tr>
                                <td>{{$data->CompanyID}}</td>
                                <td>{{$data->CompanyName}}</td>
                                <td>{{$data->PaymentID}}</td>
                                <td class="text-right">{{$data->AgentCommission}}</td>
                                {{''; 
                                                                                                                                                $komisi = $data->Company->Package->Price * ($data->AgentCommission/100);
                                }}
                                <td class="text-right">{{number_format($komisi,'2','.',',')}}</td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div><!---- end div tabwrap---->
        </div><!---- end div tabwrap---->
    </div><!--end primcontent-->
</div><!--end wrapjour--->


@stop

@section('modal')
<div class="modal fade" id="r_commission" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Commission Report</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="" target="_blank">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='reportWithdraw'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateReport" data-validation="required">
                            </li>
                        </ul>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>      
        </div>
    </div>  
</div>
@stop
@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/agent-js-commission/commission-agent.js')}}"></script>
@stop