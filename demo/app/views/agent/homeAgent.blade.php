@extends('template.header-footer-agent')

@section('title')
Company
@stop

@section('css')
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
@stop

@section('nav')

@stop

@section('content')
@if(isset($messages))
@if($messages == 'gagalInsert' || $messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New company has been inserted.
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Company has been updated.
</div>
@endif
@if($messages == 'suksesSendInvoice')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Invoice to company has been updated.
</div>
@endif
@if($messages == 'suksesDelete')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Company has been deleted.
</div>
@endif
@if($messages == 'gagalDelete')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Company has been registered in other tables.
</div>
@endif
@if($messages == 'gagalUpload')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> There is something wrong with upload file, Please check picture type and size.
</div>
@endif
@endif

<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread" role="group">
                <a href="{{route('showHomeAgent')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showHomeAgent')}}" type="button" class="btn btn-sm btn-pure">Company</a>
            </div>

            <a target="_blank" href="{{Route('exportCompanyAgent')}}">
                <button type="button" class="btn btn-sm btn-green btn-export" id="btn-export">Export Excel</button>    
            </a>

            <button type="button" class="btn btn-green btn-insert" data-target="#m_company" data-toggle="modal" role="dialog">
                <span class="glyphicon glyphicon-plus"></span> New</button>
        </div>
        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Company</h4>
            </div>            
            <div class="tableadd">
                <table id="example" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Company ID</th>
                            <th>Name</th>
                            <th width="10%">Address</th>
                            <th>Phone</th>
                            <th>Package</th>
                            <th>Payment</th>
                            <th>Using Memory</th>
                            <th>Expired Date</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $f = rand(0, 50);
                        foreach (Company::where("AgentInternalID", Auth::user()->InternalID)->get() as $data) {
                            $data->dtRecordformat = date("d-m-Y H:i:s", strtotime($data->dtRecord));
                            $data->dtModifformat = date("d-m-Y H:i:s", strtotime($data->dtModified));
                            $data->Addressformat = str_replace("\r\n", "&#013", $data->Address);
                            $arrData = array($data);
                            $tamp = myEscapeStringData($arrData);
                            $tamp = myEncryptJavaScriptText($tamp, $f);
                            ?>
                            <tr>
                                <td>{{$data->CompanyID}}</td>
                                <td>{{$data->CompanyName}}</td>
                                <td>{{$data->Address}}</td>
                                <td>{{$data->Phone}}</td>
                                @if($data->PackageInternalID != NULL)
                                <td>{{($data->PackageInternalID == 1 ? 'Starter' : 'Enterprise')}}</td>
                                @else
                                <td>Super admin</td>
                                @endif
                                <td>
                                    @if($data->StatusPayment == 0)
                                    Not Complete
                                    @elseif($data->StatusPayment == 1)
                                    Complete
                                    @endif
                                </td>
                                <td class='right'>{{countMemorySuperAdmin($data->InternalID)}}</td>
                                <td>
                                    @if($data->ExpiredDate == NULL)
                                    New register
                                    @else
                                    {{ date("d-m-Y", strtotime($data->ExpiredDate))}}
                                    @endif
                                </td>
                                <td>
                                    @if($data->Status == 0)
                                    Not Active
                                    @elseif($data->Status == 1)
                                    Active
                                    @endif
                                </td>
                                <td class="text-center">
                                    <?php
                                    if ($data->Logo == NULL) {
                                        $logo = '-1';
                                    } else {
                                        $logo = Asset($data->Logo);
                                    }
                                    $lastInvoice = Invoice::where('CompanyInternalID', $data->InternalID)->orderBy('InternalID', 'desc')->first();
                                    if ($lastInvoice != '') {
                                        $lastInvoice = $lastInvoice->InvoiceID . ' | ' . date('d M Y', strtotime($lastInvoice->InvoiceDate));
                                    }
                                    ?>
                                    <button id="btn-{{$data->CompanyID}}" data-target="#m_companyUpdate" data-all='{{$tamp}}'
                                            data-toggle="modal" role="dialog" data-logo="{{$logo}}"
                                            class="btn btn-pure-xs btn-xs btn-view">
                                        <span class="glyphicon glyphicon-zoom-in"></span>
                                    </button>
                                    <button data-target="#m_companyHistory" data-toggle="modal" role="dialog" data-companyid ="{{$data->CompanyID}}"
                                            data-companyname ="{{$data->CompanyName}}" data-id="{{$data->InternalID}}" class="btn btn-pure-xs btn-xs btn-history">
                                        History
                                    </button>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div><!---- end div tabwrap---->
        </div><!---- end div tabwrap---->
    </div><!--end primcontent-->
</div><!--end wrapjour--->


@stop

@section('modal')
<div class="modal fade bs-example-modal-lg" id="m_company" role="dialog">
    <div class="modal-dialog" style="width: 1080px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insert Company and User</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form coawd" style="padding: 0 5% !important;">
                    <form action="" method="post" class="action" id="form-insert" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-8">
                                <p class="text-center" style="font-size: 17px;">Company Information</p><hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <ul class="pull-left">
                                            <input type='hidden' name='jenis' value='insertCompanyUser'>
                                            <li>
                                                <label for="Package">Package</label>
                                            </li>
                                            <li>
                                                <select class="chosen-select" id="package" name="Package">
                                                    <option value="Starter">
                                                        Package Starter
                                                    </option>
                                                    <option value="Enterprise">
                                                        Package Enterprise
                                                    </option>
                                                </select>
                                            </li>
                                            <li>
                                                <label for="CompanyID">Company ID</label> *
                                            </li>
                                            <li>
                                                <input type="text" name="CompanyID" id="companyID" maxlength="200" data-validation="required">
                                            </li>
                                            <li>
                                                <label for="CompanyName">Name Company</label> *
                                            </li>
                                            <li>
                                                <input type="text" name="CompanyName" id="name" maxlength="200" data-validation="required">
                                            </li>
                                            <li>
                                                <label for="address">Address</label>
                                            </li>
                                            <li>
                                                <textarea style="resize:none;" name="Address" maxlength="1000"></textarea>
                                            </li>
                                            <li>
                                                <label for="region">Region</label>
                                            </li>
                                            <li>
                                                <select class="chosen-select" id="region" style="" name="Region">
                                                    @foreach(Region::all() as $region)
                                                    <option value="{{$region->InternalID}}">
                                                        {{$region->RegionName}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </li>

                                        </ul>
                                    </div>
                                    <div class="col-md-6">
                                        <ul class="pull-left" style="margin:10px 0px">
                                            <li>
                                                <label for="city">City</label>
                                            </li>
                                            <li>
                                                <input type="text" name="City" maxlength="1000">
                                            </li>
                                            <li>
                                                <label for="Email">Email Company *</label> 
                                            </li>
                                            <li>
                                                <input type="text" name="EmailCompany" id="emailCompanyID" maxlength="200" data-validation="email">
                                            </li>
                                            <li>
                                                <label for="taxID">TaxID</label>
                                            </li>
                                            <li>
                                                <div id="npwpin">
                                                    <input type="hidden" name="taxID" value="" id="taxID">
                                                    <input class="persen11 autoTab numaja" type="text" name="taxID1" id="taxID1" maxlength="2"
                                                           data-validation="length" data-validation-length="min2" 
                                                           data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                                    <input class="persen15 autoTab numaja" type="text" name="taxID2" id="taxID2" maxlength="3"
                                                           data-validation="length" data-validation-length="min3" 
                                                           data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                                    <input class="persen15 autoTab numaja" type="text" name="taxID3" id="taxID3" maxlength="3"
                                                           data-validation="length" data-validation-length="min3" 
                                                           data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                                    <input class="persen8 autoTab numaja" type="text" name="taxID4" id="taxID4" maxlength="1"
                                                           data-validation="length" data-validation-length="min1" 
                                                           data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> - 
                                                    <input class="persen15 autoTab numaja" type="text" name="taxID5" id="taxID5" maxlength="3"
                                                           data-validation="length" data-validation-length="min3" 
                                                           data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                                    <input class="persen15 autoTab numaja" type="text" name="taxID6" id="taxID6" maxlength="3"
                                                           data-validation="length" data-validation-length="min3" 
                                                           data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information.">
                                                </div>
                                            </li>
                                            <li>
                                                <label for="phone">Phone</label>
                                            </li>
                                            <li>
                                                <input type="text" name="Phone" maxlength="200">
                                            </li>
                                            <li>
                                                <label for="fax">Fax</label>
                                            </li>
                                            <li>
                                                <input type="text" name="Fax" maxlength="200">
                                            </li>
                                            <li>
                                                <label for="logo">Logo</label>
                                            </li>
                                            <li>
                                                <input type="file" name="Logo">
                                            </li>
                                        </ul>  
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4" style="border-left: 1px solid #e5e5e5">
                                <p class="text-center" style="font-size: 17px;">User Information</p><hr>
                                <ul class="pull-left">
                                    <li>
                                        <label for="Email">Username *</label> 
                                    </li>
                                    <li>
                                        <input type="text" name="Name" id="nameUser" maxlength="200" data-validation="required">
                                    </li>
                                    <li>
                                        <label for="taxID">User ID *</label>
                                    </li>
                                    <li>
                                        <input type="text" name="UserID" id="userID" maxlength="200" data-validation="required">
                                    </li>
                                    <li>
                                        <label for="phone">Email User *</label>
                                    </li>
                                    <li>
                                        <input type="email" name="Email" id="emailID"maxlength="200" data-validation="required">
                                    </li>
                                    <li>
                                        <label for="fax">Phone Number</label>
                                    </li>
                                    <li>
                                        <input type="text" name="PhoneNumber" maxlength="200">
                                    </li>
                                    <li>
                                        <label for="logo">Password</label>
                                    </li>
                                    <li>
                                        <input type="password" name="Password" data-validation="required">
                                    </li>
                                    <li>
                                        <label for="remark">Confirm Password</label> *
                                    </li>
                                    <li>
                                        <input type="password" name="ConfirmPassword" data-validation="required">
                                    </li>
                                </ul>
                            </div>
                        </div>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>      
        </div>
    </div>  
</div>


<div class="modal fade bs-example-modal-lg" id="m_companyUpdate" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Update company</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form coawd">
                    <form action="" method="post" class="action" id="form-update" enctype="multipart/form-data">
                        <ul class="pull-left">
                            <input type="hidden" value="" id="idUpdate" name="InternalID">
                            <input type="hidden" value="updateCompany" id="jenisUpdate" name="jenis">
                            <li>
                                <label for="CompanyName">Name</label>
                            </li>
                            <li>
                                <input type="text" name="CompanyName" id="nameUpdate" maxlength="200" data-validation="required" disabled="true">
                            </li>
                            <li>
                                <label for="address">Address</label>
                            </li>
                            <li>
                                <textarea style="resize:none;"  name="Address" id="addressUpdate" maxlength="1000" disabled="true"></textarea>
                            </li>
                            <li>
                                <label for="city">City</label>
                            </li>
                            <li>
                                <input type="text" id="cityUpdate" name="City" maxlength="1000" disabled="true">
                            </li>
                            <li>
                                <label for="region">Region</label>
                            </li>
                            <li>
                                <input type="text" id="regionUpdate" name="Region" maxlength="1000" value="{{$region->RegionName}}" disabled="true">
<!--                                <select class="chosen-select" id="regionUpdate" style="" name="Region" disabled="true">
                                    @foreach(Region::all() as $region)
                                    <option id="reg{{$region->InternalID}}" value="{{$region->InternalID}}">
                                        {{$region->RegionName}}
                                    </option>
                                    @endforeach
                                </select>-->
                            </li>
                            <li>
                                <label for="Email">Email</label> 
                            </li>
                            <li>
                                <input type="text" name="Email" id="emailUpdate" maxlength="200" data-validation="email" disabled="true">
                            </li>
                            <li>
                                <label for="taxID">TaxID</label>
                            </li>
                            <li>
                                <div id="npwpin">
                                    <input type="hidden" name="taxID" value="" id="taxIDupdate" disabled="true">
                                    <input disabled="true" class="persen11 autoTab numaja" type="text" name="taxID1" id="taxID1update" maxlength="2"
                                           data-validation="length" data-validation-length="min2" 
                                           data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                    <input disabled="true" class="persen15 autoTab numaja" type="text" name="taxID2" id="taxID2update" maxlength="3"
                                           data-validation="length" data-validation-length="min3" 
                                           data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                    <input disabled="true" class="persen15 autoTab numaja" type="text" name="taxID3" id="taxID3update" maxlength="3"
                                           data-validation="length" data-validation-length="min3" 
                                           data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                    <input disabled="true" class="persen8 autoTab numaja" type="text" name="taxID4" id="taxID4update" maxlength="1"
                                           data-validation="length" data-validation-length="min1" 
                                           data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> - 
                                    <input disabled="true" class="persen15 autoTab numaja" type="text" name="taxID5" id="taxID5update" maxlength="3"
                                           data-validation="length" data-validation-length="min3" 
                                           data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                    <input disabled="true" class="persen15 autoTab numaja" type="text" name="taxID6" id="taxID6update" maxlength="3"
                                           data-validation="length" data-validation-length="min3" 
                                           data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information.">
                                </div>
                            </li>

                        </ul>
                        <ul class="pull-right">
                            <li>
                                <label for="phone">Phone</label>
                            </li>
                            <li>
                                <input type="text" name="Phone" id="phoneUpdate" maxlength="200" disabled="true">
                            </li>
                            <li>
                                <label for="fax">Fax</label>
                            </li>
                            <li>
                                <input type="text" name="Fax" id="faxUpdate" maxlength="200" disabled="true">
                            </li>
                            <li>
                                <label for="logo">Logo</label>
                            </li>
                            <!--                           <li>
                                                          <input type="file" name="Logo"> 
                                                      </li>-->
                            <li>
                                <!--<span style="padding:3px;">Last Logo : </span>-->
                                <img style="height:200px; max-width: 300px;" id="logoUpdate" src="">
                            </li>
                            <li>
                                <small>Created by <span id="createdDetail"></span></small><br>
                                <small>Modified by <span id="modifiedDetail"></span></small>
                            </li>
                        </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </div>
            </form>
        </div>
    </div>  
</div>
<div class="modal fade" id="m_companyHistory" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">History Payment Company</h4>
            </div>
            <div class="modal-body" >
                <div class="coa-form coawd">
                    <p id="companyIDPayment"> Company ID : </p>
                    <p id="companyNamePayment"> Company Name : </p>
                    <div  style="overflow-y: scroll; height: 250px">
                        <table class="table table-hover" >
                            <thead>
                            <th>No</th>
                            <th>Payment Date</th>
                            <th>Payment ID</th>
                            </thead>
                            <tbody id="tbodyPayment">
                                
                            </tbody>
                        </table>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>  
</div>
@stop
<?php
$u = myEncryptJavaScript(User::select('UserID as userID')->get(), $f);
$u2 = myEncryptJavaScript(User::select('Email as emailID')->get(), $f);
$c = myEncryptJavaScript(Company::select('CompanyID as companyID')->get(), $f);
$c2 = myEncryptJavaScript(Company::select('Email as emailCompanyID')->get(), $f);
?>
@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript">
var a = '<?php echo $u; ?>';
var b = '<?php echo $c; ?>';
var a2 = '<?php echo $u2; ?>';
var b2 = '<?php echo $c2; ?>';
var c = <?php echo $f; ?>;
var alamat_post = "<?php echo Route("getDataHistory") ?>";
</script>
<script> var imageKosong = "<?php echo Asset('/img/briefsmall.png') ?>"</script>
<script type="text/javascript" src="{{Asset('js/entry-js-master/company-agent.js')}}"></script>
@stop