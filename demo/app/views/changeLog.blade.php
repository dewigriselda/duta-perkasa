@extends('template.header-footer')

@section('title')
Change Log - Salmon Accounting
@stop 

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.css')}}">
@stop

@section('nav')

@stop

@section('content')

<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread margr30min" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a href="{{route('showChangeLog')}}" type="button" class="btn btn-sm btn-pure">Change Log</a>
            </div>

        </div>

        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">What's New</h4>
            </div>
            <div class="tableadd">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingSeven">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven">
                                    February 5, 2015 - New Features
                                </a>
                            </h4>
                        </div>
                        <div id="collapseSeven" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingSeven">
                            <div class="panel-body">
                                <div>
                                    <h4>Inventory Report</h4>
                                    <div class="col-xs-12">
                                        <h5>Inventory Stock Report</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingSix">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                    January 12, 2015 - Upgrade performance 
                                </a>
                            </h4>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                            <div class="panel-body">
                                <div>
                                    <h4>General</h4>
                                    <div class="col-xs-12">
                                        <h5> Optimization performance </h5>
                                        <h5> Bug Fixing </h5>
                                        <h5> View gadget optimizer </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    November 24, 2015 - New Features
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                <div>
                                    <h4>E-Faktur</h4>
                                    <div class="col-xs-12">
                                        <h5>E-Factur compatible in Sales and Purchase Invoice</h5>
                                        <h5>Export CSV from Sales and Purchase Invoice to E-Faktur</h5>
                                        <h5>Data customer and supplier adjust to E-Faktur</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    November 24, 2015 - Upgrade performance 
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body">
                                <div>
                                    <h4>General</h4>
                                    <div class="col-xs-12">
                                        <h5> Optimization performance </h5>
                                        <h5> Bug Fixing </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    November 10, 2015 - Bug Fixed
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="panel-body">
                                <div>
                                    <h4>Transaction</h4>
                                    <div class="col-xs-12">
                                        <h5>1. Top Ten Customer From Each Last Ten Sales Order. </h5>
                                        <h5>2. Top Ten Customer From Each Last Ten Sales. </h5>
                                        <h5>3. Top Ten Customer From Each Last Ten Sales Return. </h5>
                                        <h5>4. Top Ten Supplier From Each Last Ten Purchase Order. </h5>
                                        <h5>5. Top Ten Supplier From Each Last Ten Purchase. </h5>
                                        <h5>6. Top Ten Supplier From Each Last Ten Purchase Return. </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFour">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    November 10, 2015 - New Features
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                            <div class="panel-body">
                                <div>
                                    <h4>Reports</h4>
                                    <div class="col-xs-12">
                                        <h5>1. History Price each Customer and Supplier, in COA Level 6's Menu. </h5>
                                        <h5>2. History Price each Inventory in Inventory's Menu. </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFive">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    November 3, 2015 - New Features
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                            <div class="panel-body">
                                <div>
                                    <h4>Reports</h4>
                                    <div class="col-xs-12">
                                        <h5>1. Receivable, in Journal's Menu. </h5>
                                        <h5>2. Payable, in Journal's Menu. </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!---- end div tabwrap---->



        </div><!---end primcontent--->
    </div><!---end wrapjour--->

    @stop

    @section('js')

    @stop
