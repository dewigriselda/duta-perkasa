@extends('template.header-footer')

@section('title')
Coa level 5
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
@stop

@section('nav')@stop

@section('content')


@if(isset($messages))
@if($messages == 'gagalInsert' || $messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New account has been inserted.
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Account has been updated.
</div>
@endif
@if($messages == 'suksesDelete')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Account has been deleted.
</div>
@endif
@if($messages == 'gagalDelete')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Account has been registered in table coa or journal.
</div>
@endif
@if($messages == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@endif


<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread hidden-xs" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showCoa5')}}" type="button" class="btn btn-sm btn-pure">COA Level 5</a>
            </div>
            @if(checkModul('O01'))
            <a target="_blank" href="{{Route('exportCoa5')}}">
                <button type="button" class="btn btn-sm btn-green btn-export" id="btn-export">Export Excel</button>  
            </a>
            @endif
            <button type="button" class="btn btn-green btn-insert" data-target="#m_coa" data-toggle="modal" role="dialog">
                <span class="glyphicon glyphicon-plus"></span> New</button>
        </div>

        <div class="bread-xs visible-xs hidden-sm hidden-md hidden-lg">
            <p class="text-center"><a href="{{route('showDashboard')}}">{{Config::get('companyHeader.header_company');}}</a> / <a>{{ucfirst($toogle)}}</a> / <a href="{{route('showCoa5')}}">COA Level 5</a></p>
        </div>

        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">COA Level 5</h4>
            </div>

            <div class="tableadd">
                <table id="example" class="display table-rwd table-coa5" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Account ID</th>
                            <th>Name</th>
                            <th>Record</th>
                            <th>Modified</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $f = rand(0, 50);
                        foreach (Coa5::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data5) {
                            if ($data5->InternalID != 0) {
                                $data5->dtRecordformat = date("d-m-Y H:i:s", strtotime($data5->dtRecord));
                                $data5->dtModifformat = date("d-m-Y H:i:s", strtotime($data5->dtModified));
                                $data5->Remark = str_replace("\r\n", " ", $data5->Remark);
                                $arrData = array($data5);
                                $tamp = myEscapeStringData($arrData);
                                $tamp = myEncryptJavaScriptText($tamp, $f);
                                ?>
                                <tr>
                                    <td>{{$data5->ACC5ID}}</td>
                                    <td>{{$data5->ACC5Name}}</td>
                                    <td>{{$data5->UserRecord." ".date( "d-m-Y H:i:s", strtotime($data5->dtRecord))}}</td>
                                    @if($data5->UserModified == "0")
                                    <td>-</td>
                                    @else
                                    <td>{{$data5->UserModified." ".date( "d-m-Y H:i:s", strtotime($data5->dtModified))}}</td>
                                    @endif
                                    <td class="text-center"><button id="btn-{{$data5->ACC5ID}}" data-target="#m_coaUpdate" data-all='{{$tamp}}'
                                                                    data-level="5" data-toggle="modal" role="dialog"
                                                                    class="btn btn-pure-xs btn-xs btn-edit">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </button>
                                        <button data-target="#m_coaDelete" data-internal="{{$data5->InternalID}}"  data-toggle="modal" role="dialog"
                                                data-id="{{$data5->ACC5ID}}" data-name='{{$data5->ACC5Name}}' data-level="5" class="btn btn-pure-xs btn-xs btn-delete">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </button></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div><!---end of tableadd--->
        </div><!---- end div tabwrap---->
    </div><!---end of primcontent--->
</div><!---end of wrapjour--->

@stop

@section('modal')
<div class="modal fade bs-example-modal-lg" id="m_coa" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <form action="" method="post" class="action" id="form-insert">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Insert COA Level 5</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="margbot10">
                                    <input type='hidden' name='jenis' value='insertCoa'>
                                </div>

                                <div class="margbot10">
                                    <label for="AccID">Account ID *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="noSpecialCharacter" type="text" name="AccID" id="accID" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="ACC2Name">Name *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="noSpecialCharacter" type="text" name="AccName" id="name" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;"  name="remark" id="remark" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>


<div class="modal fade bs-example-modal-lg" id="m_coaUpdate" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <form action="" method="post" class="action" id="form-update">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update COA Level 5</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="margbot10">
                                    <input type="hidden" value="" id="idUpdate" name="InternalID">
                                    <input type="hidden" value="updateCoa" id="jenisUpdate" name="jenis">
                                </div>

                                <div class="margbot10">
                                    <label for="ACCName">Name *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="noSpecialCharacter" type="text" name="AccName" id="nameUpdate" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;"  name="remark" id="remarkUpdate" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <small>Created by <span id="createdDetail"></span></small><br>
                                    <small>Modified by <span id="modifiedDetail"></span></small>
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Update</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade bs-example-modal-lg" id="m_coaDelete" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <form action="" method="post" class="action">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Delete COA Level 5</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                      
                            <input type="hidden" value="" id="idDelete" name="InternalID">
                            <input type="hidden" value="" id="AccID" name="AccID">
                            <input type="hidden" value="deleteCoa" id="jenisDelete" name="jenis">
                            <p>Are you sure want to delete <span id="deleteName"></span>?</p>
                       
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Yes</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
                </div>
            </form>      
        </div>
    </div>  
</div>
@stop
<?php
$s = myEncryptJavaScript(Coa5::select('ACC5ID as accID')->where('CompanyInternalID', Auth::user()->Company->InternalID)
                ->orWhere('CompanyInternalID', NULL)->get(), $f);
?>
@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript">
var a = '<?php echo $s; ?>';
var b = <?php echo $f; ?>;
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-coa/coa5.js')}}"></script>
@stop