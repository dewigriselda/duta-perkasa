@extends('template.header-footer')

@section('title')
Coa level 6
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
@stop

@section('nav')

@stop

@section('content')


@if(isset($messages))
@if($messages == 'gagalInsert' || $messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New account has been inserted.
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Account has been updated.
</div>
@endif
@if($messages == 'suksesDelete')
<div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Account has been deleted.
</div>
@endif
@if($messages == 'gagalDelete')
<div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Account has been registered in other tables.
</div>
@endif
@if($messages == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@endif


<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread hidden-xs" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showCoa6')}}" type="button" class="btn btn-sm btn-pure">COA Level 6</a>
            </div>
            @if(checkModul('O01'))
            <a target="_blank" href="{{Route('exportCoa6')}}">
                <button type="button" class="btn btn-sm btn-green btn-export" id="btn-export">Export Excel</button>    
            </a>
            @endif
            <button type="button" class="btn btn-green btn-insert" data-target="#m_coa" data-toggle="modal" role="dialog">
                <span class="glyphicon glyphicon-plus"></span> New</button>
        </div>

        <div class="bread-xs visible-xs hidden-sm hidden-md hidden-lg">
            <p class="text-center"><a href="{{route('showDashboard')}}">{{Config::get('companyHeader.header_company');}}</a> / <a>{{ucfirst($toogle)}}</a> / <a href="{{route('showCoa6')}}">COA Level 6</a></p>
        </div>

        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">COA Level 6</h4>
            </div>            
            <div class="tableadd">
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs no-border" role="tablist">
                        <li role="presentation" class="customer active"><a href="#customer" aria-controls="customer" role="tab" data-toggle="tab">Customer</a></li>
                        <li role="presentation"><a href="#supplier" aria-controls="profile" role="tab" data-toggle="tab">Supplier</a></li>  
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content tab-content-pad">
                        <div role="tabpanel" class="tab-pane active" id="customer">
                            <table id="example" class="display table-rwd table-coa6-customer" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Customer ID</th>
                                        <th>Name</th>
                                        <th>Address</th>
                                        <th>City</th>
                                        <th>Postal Code</th>
                                        <th>TaxID</th>
                                        <th>Phone</th>
                                        <th>Credit Limit</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $f = rand(0, 50);
                                    foreach (Coa6::where("Type", "c")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data6) {
                                        $data6->dtRecordformat = date("d-m-Y H:i:s", strtotime($data6->dtRecord));
                                        $data6->dtModifformat = date("d-m-Y H:i:s", strtotime($data6->dtModified));
                                        $data6->Addressformat = str_replace("\r\n", " ", $data6->Address);
                                        $data6->Remark = str_replace("\r\n", " ", $data6->Remark);
                                        $arrData = array($data6);
                                        $tamp = myEscapeStringData($arrData);
                                        $tamp = myEncryptJavaScriptText($tamp, $f);
                                        ?>
                                        <tr>
                                            <td>{{$data6->ACC6ID}}</td>
                                            <td>{{$data6->ACC6Name}}</td>
                                            @if($data6->Block != NULL)
                                            <td>{{$data6->Address.' '.$data6->Block. ' ' .$data6->AddressNumber. ' RT:' .$data6->RT. ' RW' .$data6->RW.', '.$data6->District.', '.$data6->Subdistrict.', '.$data6->Province}}</td>
                                            @else
                                            <td>{{$data6->Address}}</td>
                                            @endif
                                            <td>{{$data6->City}}</td>
                                            <td>{{$data6->PostalCode}}</td>
                                            @if($data6->TaxID == "...-.")
                                            <td></td>
                                            @else
                                            <td>{{$data6->TaxID}}</td>
                                            @endif
                                            <td>{{$data6->Phone}}</td>
                                            <td class="right">{{number_format($data6->CreditLimit,2,'.',',')}}</td>
                                            <td class="text-center">
                                                <button id="btn-{{$data6->ACC6ID}}" data-target="#m_coaUpdate" data-all='{{$tamp}}'
                                                        data-level="6" data-toggle="modal" role="dialog"
                                                        class="btn btn-pure-xs btn-xs btn-edit">
                                                    <span class="glyphicon glyphicon-edit"></span>
                                                </button>
                                                <button data-target="#m_coaDelete" data-internal="{{$data6->InternalID}}"  data-toggle="modal" role="dialog"
                                                        data-id="{{$data6->ACC6ID}}" data-name='{{$data6->ACC6Name}}' data-level="5" class="btn btn-pure-xs btn-xs btn-delete">
                                                    <span class="glyphicon glyphicon-trash"></span>
                                                </button>
                                                @if(checkModul('O04'))
                                                <a href="{{Route('historyCustomer',$data6->ACC6ID)}}" target='_blank'>
                                                    <button id="btn-{{$data6->ACC6ID}}-print"
                                                            class="btn btn-pure-xs btn-xs btn-price">
                                                        History
                                                    </button>
                                                </a>
                                                @endif
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="supplier">
                            <table id="example2" class="display table-coa6-supplier" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Supplier ID</th>
                                        <th>Name</th>
                                        <th>Address</th>
                                        <th>City</th>
                                        <th>Postal Code</th>
                                        <th>TaxID</th>
                                        <th>Phone</th>
                                        <th>Credit Limit</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach (Coa6::where("Type", "s")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data6) {
                                        $data6->dtRecordformat = date("d-m-Y H:i:s", strtotime($data6->dtRecord));
                                        $data6->dtModifformat = date("d-m-Y H:i:s", strtotime($data6->dtModified));
                                        $data6->Addressformat = str_replace("\r\n", " ", $data6->Address);
                                        $data6->Remark = str_replace("\r\n", " ", $data6->Remark);
                                        $arrData = array($data6);
                                        $tamp = myEscapeStringData($arrData);
                                        $tamp = myEncryptJavaScriptText($tamp, $f);
                                        ?>
                                        <tr>
                                            <td>{{$data6->ACC6ID}}</td>
                                            <td>{{$data6->ACC6Name}}</td>
                                            @if($data6->Block != NULL)
                                            <td>{{$data6->Address.' '.$data6->Block. ' ' .$data6->AddressNumber. ' RT:' .$data6->RT. ' RW' .$data6->RW.', '.$data6->District.', '.$data6->Subdistrict.', '.$data6->Province}}</td>
                                            @else
                                            <td>{{$data6->Address}}</td>
                                            @endif
                                            <td>{{$data6->City}}</td>
                                            <td>{{$data6->PostalCode}}</td>
                                            @if($data6->TaxID == "...-.")
                                            <td></td>
                                            @else
                                            <td>{{$data6->TaxID}}</td>
                                            @endif
                                            <td>{{$data6->Phone}}</td>
                                            <td class="right">{{number_format($data6->CreditLimit,2,'.',',')}}</td>
                                            <td class="text-center">
                                                <button id="btn-{{$data6->ACC6ID}}" data-target="#m_coaUpdate" data-all='{{$tamp}}'
                                                        data-level="6" data-toggle="modal" role="dialog"
                                                        class="btn btn-pure-xs btn-xs btn-edit">
                                                    <span class="glyphicon glyphicon-edit"></span>
                                                </button>
                                                <button data-target="#m_coaDelete" data-internal="{{$data6->InternalID}}"  data-toggle="modal" role="dialog"
                                                        data-id="{{$data6->ACC6ID}}" data-name='{{$data6->ACC6Name}}' data-level="5" class="btn btn-pure-xs btn-xs btn-delete">
                                                    <span class="glyphicon glyphicon-trash"></span>
                                                </button>
                                                @if(checkModul('O04'))
                                                <a href="{{Route('historySupplier',$data6->ACC6ID)}}" target='_blank'>
                                                    <button id="btn-{{$data6->ACC6ID}}-print"
                                                            class="btn btn-pure-xs btn-xs btn-price">
                                                        History
                                                    </button>
                                                </a>
                                                @endif
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div><!--end tab-content-->
                </div><!--end tabpanel-->
            </div><!--end tableadd-->
        </div><!---- end div tabwrap---->
    </div><!--end primcontent-->
</div><!--end wrapjour--->


@stop

@section('modal')
<div class="modal fade bs-example-modal-lg" id="m_coa" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="" method="post" class="action" id="form-insert">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Insert coa level 6</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form coawd">
                        
                        <div class="row">
                            <div class="col-md-5">
                                <div class="margbot10">
                                    <input type='hidden' name='jenis' value='insertCoa'>
                                </div>
                                <div class="margbot10">
                                    <label for="AccID">Type *</label>
                                </div>
                                <div class="margbot10">
                                    <div class="radio-inline margr10">
                                        <input type="radio" class="radio-tipe" id="tipeCustomer" name="Type" value="c" checked="checked"><label for="tipeCustomer">Customer</label>
                                    </div>
                                    <div class="radio-inline nomargl">
                                        <input type="radio" class="radio-tipe" id="tipeSupplier" name="Type" value="s"><label for="tipeSupplier">Supplier</label>
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="AccID">ID *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="noSpecialCharacter" type="text" name="AccID" id="accID" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="ACCName">Name *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="noSpecialCharacter" type="text" name="AccName" id="name" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="taxID">TaxID</label>
                                </div>
                                <div class="margbot10">
                                    <div id="npwpin">
                                        <input type="hidden" name="taxID" value="" id="taxID">
                                        <input class="persen11 autoTab numaja" type="text" name="taxID1" id="taxID1" maxlength="2"
                                               data-validation="length" data-validation-length="min2" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID2" id="taxID2" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID3" id="taxID3" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen8 autoTab numaja" type="text" name="taxID4" id="taxID4" maxlength="1"
                                               data-validation="length" data-validation-length="min1" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> - 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID5" id="taxID5" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID6" id="taxID6" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information.">
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="address">Address</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;" name="Address" maxlength="1000"></textarea>
                                </div>
                                <div class="margbot10">
                                    <label for="Block">Block *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Block" id="block" maxlength="200">
                                </div>
                                <div class="margbot10">
                                    <label for="AddressNumber">Address Number *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="AddressNumber" id="addressNumber" maxlength="200">
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <div class="margbot10">
                                    <label for="rt">RT *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="RT" id="rt" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="rw">RW</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="RW" id="rw" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="district">District</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="District" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="subdistrict">Sub District</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Subdistrict" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="city">City</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="City" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="province">Province</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Province" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                    <label for="postalcode">Postal Code</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="PostalCode" maxlength="200" >
                                </div>
                            </div>
                            
                            <div class="col-md-4">
                                <div class="margbot10">
                                    <label for="origin">Origin</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Origin" maxlength="200">
                                </div>
                                <div class="margbot10">
                                    <label for="phone">Phone</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Phone" maxlength="200">
                                </div>
                                <div class="margbot10">
                                    <label for="fax">Fax</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Fax" maxlength="200">
                                </div>
                                <div class="margbot10">
                                    <label for="creditlimit">Credit limit</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" class="numajaDesimal" id="uangCredit" name="CreditLimit" maxlength="">
                                </div>
                                <div class="margbot10">
                                    <label for="contactperscon">Contact person</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="ContactPerson" maxlength="200">
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;"  name="remark" id="" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>


<div class="modal fade bs-example-modal-lg" id="m_coaUpdate" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="" method="post" class="action" id="form-update">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update coa level 6</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form coawd">
                        
                        <div class="row">
                            <div class="col-md-4">
                                <div class="margbot10">
                                    <input type="hidden" value="" id="idUpdate" name="InternalID">
                                    <input type="hidden" value="updateCoa" id="jenisUpdate" name="jenis">
                                </div>
                                <div class="margbot10">
                                    <label for="ACCName">Name *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="noSpecialCharacter" type="text" name="AccName" id="nameUpdate" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="taxID">TaxID</label>
                                </div>
                                <div class="margbot10">
                                    <div id="npwpin">
                                        <input type="hidden" name="taxID" value="" id="taxIDupdate">
                                        <input class="persen11 autoTab numaja" type="text" name="taxID1" id="taxID1update" maxlength="2"
                                               data-validation="length" data-validation-length="min2" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID2" id="taxID2update" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID3" id="taxID3update" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen8 autoTab numaja" type="text" name="taxID4" id="taxID4update" maxlength="1"
                                               data-validation="length" data-validation-length="min1" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> - 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID5" id="taxID5update" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID6" id="taxID6update" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information.">
                                    </div>
                                </div>
                                <div class="margbot10">
                                   <label for="origin">Origin</label>
                                </div>
                                <div class="margbot10">
                                   <input type="text" name="Origin" id="originUpdate" maxlength="200">
                                </div>
                                <div class="margbot10">
                                   <label for="address">Address</label>
                                </div>
                                <div class="margbot10">
                                   <textarea style="resize:none;"  name="Address" id="addressUpdate" maxlength="1000"></textarea>
                                </div>
                                <div class="margbot10">
                                   <label for="block">Block</label>
                                </div>
                                <div class="margbot10">
                                   <input type="text" id="blockUpdate" name="Block" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                   <label for="addressnumber">Address Number</label>
                                </div>
                                <div class="margbot10">
                                   <input type="text" id="addressNumberUpdate" name="AddressNumber" maxlength="200" >
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="margbot10">
                                   <label for="rt">RT</label>
                                </div>
                                <div class="margbot10">
                                   <input type="text" id="rtUpdate" name="RT" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                   <label for="rw">RW</label>
                                </div>
                                <div class="margbot10">
                                   <input type="text" id="rwUpdate" name="RW" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                   <label for="district">District</label>
                                </div>
                                <div class="margbot10">
                                   <input type="text" id="districtUpdate" name="District" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                   <label for="subdistrict">Subdistrict</label>
                                </div>
                                <div class="margbot10">
                                   <input type="text" id="subdistrictUpdate" name="Subdistrict" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                   <label for="city">City</label>
                                </div>
                                <div class="margbot10">
                                   <input type="text" id="cityUpdate" name="City" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                   <label for="province">Province</label>
                                </div>
                                <div class="margbot10">
                                   <input type="text" id="provinceUpdate" name="Province" maxlength="200" >
                                </div>
                                <div class="margbot10">
                                   <label for="postalCode">Postal Code</label>
                                </div>
                                <div class="margbot10">
                                   <input type="text" id="postalCodeUpdate" name="PostalCode" maxlength="200" >
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="margbot10">
                                    <label for="phone">Phone</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Phone" id="phoneUpdate" maxlength="200">
                                </div>
                                <div class="margbot10">
                                    <label for="fax">Fax</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Fax" id="faxUpdate" maxlength="200">
                                </div>
                                <div class="margbot10">
                                    <label for="creditlimit">Credit limit</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" class="numajaDesimal" name="CreditLimit" id="creditLimitUpdate" maxlength="200">
                                </div>
                                <div class="margbot10">
                                    <label for="contactperscon">Contact person</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="ContactPerson" id="contactPersonUpdate" maxlength="200">
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;"  name="remark" id="remarkUpdate" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <small>Created by <span id="createdDetail"></span></small><br>
                                    <small>Modified by <span id="modifiedDetail"></span></small>
                                </div>
                                <div class="required margbot10">
                                    * Required
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Update</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade bs-example-modal-lg" id="m_coaDelete" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Delete coa level 6</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action">
                        <ul>
                            <input type="hidden" value="" id="idDelete" name="InternalID">
                            <input type="hidden" value="" id="AccID" name="AccID">
                            <input type="hidden" value="deleteCoa" id="jenisDelete" name="jenis">
                            <p>Are you sure want to delete <span id="deleteName"></span>?</p>
                        </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-green">Yes</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
            </div>
            </form>      
        </div>
    </div>  
</div>
@stop
<?php
$s = myEncryptJavaScript(Coa6::select('ACC6ID as accID')->where('CompanyInternalID', Auth::user()->Company->InternalID)
                ->orWhere('CompanyInternalID', NULL)->get(), $f);
?>
@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript">
$(document).ready(function () {
    $(".btn-insert").click(function () {
        var idAkhir = "<?php echo Coa6::maxID('C') ?>";
        $('#accID').val("C" + idAkhir);
        $('#tipeSupplier').removeAttr('checked');
        $('#tipeCustomer').attr('checked', 'checked');
    });
    $("#tipeCustomer,#tipeSupplier").change(function () {
        var idAkhirCustomer = "<?php echo Coa6::maxID('S') ?>";
        var idAkhirSupplier = "<?php echo Coa6::maxID('C') ?>";
        autoAccID(idAkhirCustomer, idAkhirSupplier)
    });
});

var a = '<?php echo $s; ?>';
var b = <?php echo $f; ?>;
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-coa/coa6.js')}}"></script>

@stop