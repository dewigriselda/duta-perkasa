@extends('template.header-footer')

@section('title')
Department
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
@stop

@section('nav')

@stop

@section('content')

@if(isset($messages))
@if($messages == 'gagalInsert' || $messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New department has been inserted.
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Department has been updated.
</div>
@endif
@if($messages == 'suksesDelete')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Department has been deleted.
</div>
@endif
@if($messages == 'gagalDelete')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Department has been registered in table journal.
</div>
@endif
@if($messages == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@endif


<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group hidden-xs bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showDepartment')}}" type="button" class="btn btn-sm btn-pure">Department</a>
            </div>
            @if(checkModul('O01'))
            <a target="_blank" href="{{Route('exportDepartment')}}">
                <button type="button" class="btn btn-sm btn-green btn-export" id="btn-export">Export Excel</button>  
            </a>
            @endif
            <button type="button" class="btn btn-green btn-insert" data-target="#m_department" data-toggle="modal" role="dialog">
                <span class="glyphicon glyphicon-plus"></span> New</button>
        </div>

        <div class="bread-xs visible-xs hidden-sm hidden-md hidden-lg">
            <p class="text-center"><a href="{{route('showDashboard')}}">{{Config::get('companyHeader.header_company');}}</a> / <a>{{ucfirst($toogle)}}</a> / <a href="{{route('showDepartment')}}">Department</a></p>
        </div>

        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Department</h4>
            </div>

            <div class="tableadd">
                <table id="example" class="display table-rwd table-department" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Department ID</th>
                            <th>Name</th>
                            <th>Default</th>
                            <th>Record</th>
                            <th>Modified</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $f = rand(0, 50);
                        foreach (Department::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                            $data->dtRecordformat = date("d-m-Y H:i:s", strtotime($data->dtRecord));
                            $data->dtModifformat = date("d-m-Y H:i:s", strtotime($data->dtModified));
                            $data->Remark = str_replace("\r\n", " ", $data->Remark);
                            $arrData = array($data);
                            $tamp = myEscapeStringData($arrData);
                            $tamp = myEncryptJavaScriptText($tamp, $f);
                            ?>
                            <tr>
                                <td>{{$data->DepartmentID}}</td>
                                <td>{{$data->DepartmentName}}</td>
                                <td>{{($data->Default == 1 ? "Default" : "Not Default")}}</td>
                                <td>{{$data->UserRecord." ".date( "d-m-Y H:i:s", strtotime($data->dtRecord))}}</td>
                                @if($data->UserModified == "0")
                                <td>-</td>
                                @else
                                <td>{{$data->UserModified." ".date( "d-m-Y H:i:s", strtotime($data->dtModified))}}</td>
                                @endif
                                <td class="text-center"><button id="btn-{{$data->DepartmentID}}" data-target="#m_departmentUpdate" data-all='{{$tamp}}'
                                                                data-toggle="modal" role="dialog"
                                                                class="btn btn-pure-xs btn-xs btn-edit">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </button>
                                    <button data-target="#m_departmentDelete" data-internal="{{$data->InternalID}}"  data-toggle="modal" role="dialog"
                                            data-id="{{$data->DepartmentID}}" data-name='{{$data->DepartmentName}}' class="btn btn-pure-xs btn-xs btn-delete">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div><!---end tableadd-->
        </div><!---- end div tabwrap---->
    </div><!---end primcontent-->
</div><!---end wrapjour-->

@stop

@section('modal')
<div class="modal fade bs-example-modal-lg" id="m_department" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action" id="form-insert">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Insert Department</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="margbot10">
                                    <input type='hidden' name='jenis' value='insertDepartment'>
                                </div>
                                <div class="margbot10">
                                    <label for="Default">Default *</label>
                                </div>
                                <div class="margbot10">
                                    <div class="radio-inline">
                                        <input type="radio" class="radio-tipe" id="tipeDefault" name="Default" value="1" checked="checked"><label for="tipeDefault">Default</label>
                                    </div>
                                    <div class="radio-inline">
                                        <input type="radio" class="radio-tipe" id="tipeNoDefault" name="Default" value="0" ><label for="tipeNoDefault">Not default</label>
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="DepartmentID">Department ID *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="noSpecialCharacter" type="text" name="DepartmentID" id="departmentID" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="DepartmentName">Name *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="DepartmentName" id="name" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;"  name="remark" id="remark" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>


<div class="modal fade bs-example-modal-lg" id="m_departmentUpdate" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action" id="form-update">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Department</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="margbot10">
                                    <input type="hidden" value="" id="idUpdate" name="InternalID">
                                    <input type="hidden" value="updateDepartment" id="jenisUpdate" name="jenis">
                                </div>
                                <div class="margbot10">
                                    <label for="Default">Default *</label>
                                </div>
                                <div class="margbot10">
                                    <div class="radio-inline">
                                        <input type="radio" class="radio-tipe" id="tipe1Default" name="Default" value="1" checked="checked"><label for="tipeDefault">Default</label>
                                    </div>
                                    <div class="radio-inline">
                                        <input type="radio" class="radio-tipe" id="tipe0Default" name="Default" value="0" ><label for="tipeNoDefault">Not default</label>
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="DepartmentName">Name *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="DepartmentName" id="nameUpdate" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;" name="remark" id="remarkUpdate" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <small>Created by <span id="createdDetail"></span></small><br>
                                    <small>Modified by <span id="modifiedDetail"></span></small>
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Update</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade bs-example-modal-lg" id="m_departmentDelete" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <form action="" method="post" class="action">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Delete Department</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="" id="idDelete" name="InternalID">
                            <input type="hidden" value="deleteDepartment" id="jenisDelete" name="jenis">
                            <p>Are you sure want to delete <span id="deleteName"></span>?</p>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Yes</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
                </div>
            </form>      
        </div>
    </div>  
</div>
@stop
<?php
$s = myEncryptJavaScript(Department::select('DepartmentID')->where('CompanyInternalID', Auth::user()->Company->InternalID)->get(), $f);
?>
@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript">
var a = '<?php echo $s; ?>';
var b = <?php echo $f; ?>;
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-coa/department.js')}}"></script>
@stop
