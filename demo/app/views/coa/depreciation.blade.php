@extends('template.header-footer')

@section('title')
Depreciation
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
@stop

@section('nav')
@stop

@section('content')
@if(myCheckIsEmpty('Coa;GroupDepreciation'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one COA and Depreciation Group to insert Depreciation.
</div>
@endif
@if(isset($messages))
@if($messages == 'gagalInsert' || $messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New depreciation has been inserted.
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Depreciation has been updated.
</div>
@endif
@if($messages == 'suksesDelete')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Depreciation has been deleted.
</div>
@endif
@if($messages == 'gagalDelete')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Depreciation has been registered in other tables.
</div>
@endif
@if($messages == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@endif


<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest"> 
            <div class="btn-group bread hidden-xs" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showDepreciation')}}" type="button" class="btn btn-sm btn-pure">Depreciation</a>
            </div>
            <button type="button" <?php if (myCheckIsEmpty('Coa;GroupDepreciation')) echo 'disabled' ?> class="btn btn-green btn-insert" id="btn-insert" data-target="#m_depreciation" data-toggle="modal" role="dialog">
                <span class="glyphicon glyphicon-plus"></span> New </button>
        </div>
        <div class="bread-xs visible-xs hidden-sm hidden-md hidden-lg">
            <p class="text-center"><a href="{{route('showDashboard')}}">{{Config::get('companyHeader.header_company');}}</a> / <a>{{ucfirst($toogle)}}</a> / <a href="{{route('showDepreciation')}}">Depreciation</a></p>
        </div>

        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Depreciation</h4>
            </div>
            <div class="tableadd">
                <table id="example" class="display table-rwd table-depreciation" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Depreciation ID</th>
                            <th>Depreciation Group</th>
                            <th>Name</th>
                            <th>Method</th>
                            <th width="20%">Assets Account</th>
                            <th width="20%">Cost Account</th>
                            <th style="min-width: 65px">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $f = rand(0, 50);
                        foreach (DepreciationHeader::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                            $data->dtRecordformat = date("d-m-Y H:i:s", strtotime($data->dtRecord));
                            $data->dtModifformat = date("d-m-Y H:i:s", strtotime($data->dtModified));
                            $data->DepreciationDate = date("d-m-Y", strtotime($data->DepreciationDate));
                            $data->Remark = str_replace("\r\n", " ", $data->Remark);
                            $coaD = DepreciationHeader::coaDebet($data->InternalID);
                            $coaC = DepreciationHeader::coaCredit($data->InternalID);
                            $data->coaD = $coaD[0]->COAInternalID;
                            $data->coaC = $coaC[0]->COAInternalID;
                            $data->Check2 = DepreciationDetail::getCheck2FromDetail($data->InternalID);
                            $arrData = array($data);
                            $tamp = myEscapeStringData($arrData);
                            $tamp = myEncryptJavaScriptText($tamp, $f);
                            ?>
                            <tr>
                                <td>{{$data->DepreciationID}}</td>
                                <td>{{$data->GroupDepreciation->GroupDepreciationID.' '.$data->GroupDepreciation->Description}}</td>
                                <td>{{$data->DepreciationName}}</td>
                                @if($data->Method == '0')
                                <td>Straight Line</td>
                                @else
                                <td>Declining Balance</td>
                                @endif
                                <td>{{Coa::formatCoa($data->ACC1InternalIDDebet,$data->ACC2InternalIDDebet,
                                                                                                                                                            $data->ACC3InternalIDDebet,$data->ACC4InternalIDDebet,
                                                                                                                                                            $data->ACC5InternalIDDebet,$data->ACC6InternalIDDebet,1).' '.$coaD[0]->COAName}}</td>
                                <td>{{Coa::formatCoa($data->ACC1InternalIDCredit,$data->ACC2InternalIDCredit,
                                                                                                                                                            $data->ACC3InternalIDCredit,$data->ACC4InternalIDCredit,
                                                                                                                                                            $data->ACC5InternalIDCredit,$data->ACC6InternalIDCredit,1).' '.$coaC[0]->COAName}}</td>
                                <td class="text-center">
                                    <button id="btn-{{$data->DepreciationID}}" data-target="#m_depreciationUpdate" data-all='{{$tamp}}'
                                            data-toggle="modal" role="dialog"
                                            class="btn btn-pure-xs btn-xs btn-edit">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </button>
                                    <button data-target="#m_depreciationDelete" data-internal="{{$data->InternalID}}" data-toggle="modal" role="dialog"
                                            data-id="{{$data->DepreciationID}}" data-name='{{$data->DepreciationName}}' class="btn btn-pure-xs btn-xs btn-delete">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div><!---end of tableadd--->
        </div><!---- end div tabwrap---->
    </div><!---- end div wrapjour---->
</div><!---- end div wrapcontent---->
@stop

@section('modal')
<div class="modal fade bs-example-modal-lg " id="m_depreciation" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="" method="post" id="form-insert" class="action formCoaInsert">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Insert Depreciation</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form coawd">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="margbot10">
                                    <input type='hidden' name='jenis' value='insertDepreciation'>
                                </div>
                                <div class="margbot10">
                                    <label for="DepreciationID">Depreciation ID</label> * <span style="float: right"><input style="width:12px; height: 12px;" type="checkbox" name="closed" id="closed" value="1">Closed</span>
                                </div>
                                <div class="margbot10">
                                    <input class="noSpecialCharacter" type="text" name="DepreciationID" id="depreciationID" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="date">Date *</label>
                                </div>
                                <div class="margbot10">
                                    <input id="date" name="date" type="text" autocomplete="off">
                                </div>
                                <div class="margbot10">
                                    <label for="Type">Type *</label>
                                </div>
                                <div class="margbot10">
                                    <div class="radio-inline">
                                        <input type="radio" class="radio-tipe" id="tipeDecimal" name="Type" value="0" checked="checked"><label for="tipeDecimal">Decimal</label>
                                    </div>
                                    <div class="radio-inline">
                                        <input type="radio" class="radio-tipe" id="tipeRounding" name="Type" value="1"><label for="tipeRounding">Rounding off</label>
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="groupDepreciation">Depreciation Group *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal" id="GroupDepreciation" style="" name="groupDepreciation">
                                        {{'';                             
                                        $assetGroup = 0;
                                        $costGroup = 0;
                                        }}
                                        @foreach(GroupDepreciation::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $group)
                                        <?php
                                        $groupDepre = GroupDepreciation::where('CompanyInternalID', Auth::user()->Company->InternalID)->first();
                                        if (!is_null($groupDepre)) {
                                            $assetGroup = Coa::getInternalID($groupDepre->ACC1InternalIDDebet, $groupDepre->ACC2InternalIDDebet, $groupDepre->ACC3InternalIDDebet, $groupDepre->ACC4InternalIDDebet, $groupDepre->ACC5InternalIDDebet, $groupDepre->ACC6InternalIDDebet);
                                            $costGroup = Coa::getInternalID($groupDepre->ACC1InternalIDCredit, $groupDepre->ACC2InternalIDCredit, $groupDepre->ACC3InternalIDCredit, $groupDepre->ACC4InternalIDCredit, $groupDepre->ACC5InternalIDCredit, $groupDepre->ACC6InternalIDCredit);
                                        }
                                        ?>
                                        <option value="{{$group->InternalID.'---;---'.$group->LongDepreciation.'---;---'.$assetGroup.'---;---'.$costGroup}}">
                                            {{$group->Description}}
                                        </option>
                                        @endforeach
                                        <?php
                                        $groupDepre = GroupDepreciation::where('CompanyInternalID', Auth::user()->Company->InternalID)->first();
                                        if (!is_null($groupDepre)) {
                                            $assetGroup = Coa::getInternalID($groupDepre->ACC1InternalIDDebet, $groupDepre->ACC2InternalIDDebet, $groupDepre->ACC3InternalIDDebet, $groupDepre->ACC4InternalIDDebet, $groupDepre->ACC5InternalIDDebet, $groupDepre->ACC6InternalIDDebet);
                                            $costGroup = Coa::getInternalID($groupDepre->ACC1InternalIDCredit, $groupDepre->ACC2InternalIDCredit, $groupDepre->ACC3InternalIDCredit, $groupDepre->ACC4InternalIDCredit, $groupDepre->ACC5InternalIDCredit, $groupDepre->ACC6InternalIDCredit);
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="Name">Name *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Name" id="name" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="Description">Description *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Description" id="description" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="Nominal">Nominal *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" class="numajaDesimal nominal" id="nominal" name="Nominal" maxlength="" data-validation="required" value="0">
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="margbot10">
                                    <label for="Method">Method *</label>
                                </div>
                                <div class="margbot10">
                                    <div class="radio-inline">
                                        <input type="radio" class="radio-tipe" id="methodGaris" name="Method" value="0" checked="checked">Straight Line
                                    </div>
                                    <div class="radio-inline">
                                        <input type="radio" class="radio-tipe" id="methodSaldo" name="Method" value="1">Declining Balance
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="coaD">Assets Account *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal" id="coaD" style="" name="coaD">
                                        @foreach(Coa::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('ACC1InternalID')->orderBy('InternalID')->get() as $coa)
                                        <option @if($coa->InternalID == $assetGroup) {{'selected'}} @endif value="{{$coa->InternalID}}" id="coaDInsert{{$coa->InternalID}}">
                                                 {{Coa::formatCoa($coa->ACC1InternalID,$coa->ACC2InternalID,$coa->ACC3InternalID,$coa->ACC4InternalID,$coa->ACC5InternalID,$coa->ACC6InternalID,'1')}} 
                                                 {{" ".$coa->COAName}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="margbot10"><label for="coaC">Cost Account *</label></div>
                            <div class="margbot10">
                                <select class="chosen-select choosen-modal" id="coaC" style="" name="coaC">
                                    @foreach(Coa::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('ACC1InternalID')->orderBy('InternalID')->get() as $coa)
                                    <option @if($coa->InternalID == $costGroup) {{'selected'}} @endif value="{{$coa->InternalID}}" id="coaCInsert{{$coa->InternalID}}">
                                             {{Coa::formatCoa($coa->ACC1InternalID,$coa->ACC2InternalID,$coa->ACC3InternalID,$coa->ACC4InternalID,$coa->ACC5InternalID,$coa->ACC6InternalID,'1')}} 
                                             {{" ".$coa->COAName}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="margbot10">
                            <label for="remark">Remarks *</label>
                        </div>
                        <div class="margbot10">
                            <textarea style="resize:none;"  name="remark" id="remark" maxlength="1000" data-validation="required"></textarea>
                        </div>
                        <div class="margbot10">
                            <label for="depreciation">Depreciation *</label>
                        </div>
                        <div class="margbot10">
                            <table class="table scroll">
                                <thead>
                                    <tr>
                                        <td width="80px">Month</td>
                                        <td width="60px">Year</td>
                                        <td width="158px">Nominal</td>
                                    </tr>
                                </thead>
                                <tbody id='depreciationTable'>
                                    <tr>
                                        <td width="298px" colspan="3">There is no data depreciation yet.</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="margbot10">
                            <div class="required">
                                * Required
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-green">Submit</button>
            <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
        </div>
    </form>      
</div>
</div>  
</div>

<div class="modal fade bs-example-modal-lg " id="m_depreciationUpdate" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="" method="post" id="form-update" class="action">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Depreciation</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form coawd">
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="margbot10">
                                    <input type="hidden" value="" id="idUpdate" name="InternalID">
                                    <input type='hidden' name='jenis' value='updateDepreciation'>
                                </div>
                                <div class="margbot10">
                                    <label for="date">Date *</label> <span style="float: right"><input style="width:12px; height: 12px;" type="checkbox" name="closed" id="closedUpdate" value="1">Closed</span>
                                </div>
                                <div class="margbot10">
                                    <input id="dateUpdate" name="date" type="text" autocomplete="off">
                                </div>
                                <div class="margbot10">
                                    <label for="Type">Type *</label>
                                </div>
                                <div class="margbot10">
                                    <div class="radio-inline">
                                        <input type="radio" class="radio-tipe" id="tipeDecimalUpdate" name="Type" value="0"><label for="tipeDecimalUpdate">Decimal</label>
                                    </div>
                                    <div class="radio-inline">
                                        <input type="radio" class="radio-tipe" id="tipeRoundingUpdate" name="Type" value="1"><label for="tipeRoundingUpdate">Rounding off</label>
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="groupDepreciation">Depreciation Group *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal" id="GroupDepreciationUpdate" style="" name="groupDepreciation">
                                        @foreach(GroupDepreciation::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $group)
                                        <?php
                                        $groupDepre = GroupDepreciation::where('CompanyInternalID', Auth::user()->Company->InternalID)->first();
                                        if (!is_null($groupDepre)) {
                                            $assetGroup = Coa::getInternalID($groupDepre->ACC1InternalIDDebet, $groupDepre->ACC2InternalIDDebet, $groupDepre->ACC3InternalIDDebet, $groupDepre->ACC4InternalIDDebet, $groupDepre->ACC5InternalIDDebet, $groupDepre->ACC6InternalIDDebet);
                                            $costGroup = Coa::getInternalID($groupDepre->ACC1InternalIDCredit, $groupDepre->ACC2InternalIDCredit, $groupDepre->ACC3InternalIDCredit, $groupDepre->ACC4InternalIDCredit, $groupDepre->ACC5InternalIDCredit, $groupDepre->ACC6InternalIDCredit);
                                        }
                                        ?>
                                        <option id="group{{$group->InternalID}}" value="{{$group->InternalID.'---;---'.$group->LongDepreciation.'---;---'.$assetGroup.'---;---'.$costGroup}}">
                                            {{$group->Description}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="Name">Name *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Name" id="nameUpdate" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="Description">Description *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Description" id="descriptionUpdate" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="Nominal">Nominal *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" class="numajaDesimal nominal" id="nominalUpdate" name="Nominal" maxlength="" data-validation="required" value="0">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="margbot10">
                                    <label for="Method">Method *</label>
                                </div>
                                <div class="margbot10">
                                    <div class="radio-inline">
                                        <input type="radio" class="radio-tipe" id="methodGarisUpdate" name="Method" value="0">Straight Line
                                    </div>
                                    <div class="radio-inline">
                                        <input type="radio" class="radio-tipe" id="methodSaldoUpdate" name="Method" value="1">Declining Balance
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="coaD">Assets Account *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal" id="coaDUpdate" style="" name="coaD">
                                        @foreach(Coa::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('ACC1InternalID')->orderBy('InternalID')->get() as $coa)
                                        <option id="coaD{{$coa->InternalID}}" value="{{$coa->InternalID}}">
                                            {{Coa::formatCoa($coa->ACC1InternalID,$coa->ACC2InternalID,$coa->ACC3InternalID,$coa->ACC4InternalID,$coa->ACC5InternalID,$coa->ACC6InternalID,'1')}} 
                                            {{" ".$coa->COAName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="coaC">Cost Account *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal" id="coaCUpdate" style="" name="coaC">
                                        @foreach(Coa::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('ACC1InternalID')->orderBy('InternalID')->get() as $coa)
                                        <option id="coaC{{$coa->InternalID}}" value="{{$coa->InternalID}}">
                                            {{Coa::formatCoa($coa->ACC1InternalID,$coa->ACC2InternalID,$coa->ACC3InternalID,$coa->ACC4InternalID,$coa->ACC5InternalID,$coa->ACC6InternalID,'1')}} 
                                            {{" ".$coa->COAName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                     <label for="remark">Remarks *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;"  name="remark" id="remarkUpdate" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <label for="depreciation">Depreciation *</label>
                                </div>
                                <div class="margbot10">
                                    <table class="table scroll">
                                        <thead>
                                            <tr>
                                                <td width="80px">Month</td>
                                                <td width="60px">Year</td>
                                                <td width="158px">Nominal</td>
                                            </tr>
                                        </thead>
                                        <tbody id='depreciationTableUpdate'>
                                            <tr>
                                                <td width="298px" colspan="3">There is no data depreciation yet.</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="margbot10">
                                    <small>Created by <span id="createdDetail"></span></small><br>
                                    <small>Modified by <span id="modifiedDetail"></span></small>
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade" id="m_depreciationDelete" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <form action="" method="post" class="action">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Delete Depreciation</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <input type="hidden" value="" id="idDelete" name="InternalID">
                        <input type="hidden" value="deleteDepreciation" id="jenisDelete" name="jenis">
                            <p>Are you sure want to delete <span id="deleteName"></span>?</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Yes</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

@stop
<?php
$s = myEncryptJavaScript(DepreciationHeader::select('DepreciationID')->where('CompanyInternalID', Auth::user()->Company->InternalID)->get(), $f);
?>
@section('js')
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script><script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script>
var tanggalHariIni = '<?php echo date('d') . '-' . date('m') . '-' . date('Y') ?>';
var routeDepreciationTable = '<?php echo Route('tableDepreciation') ?>';
var a = '<?php echo $s; ?>';
var b = <?php echo $f; ?>;
</script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-coa/depreciation.js')}}"></script>
<script type="text/javascript">
</script>
@stop