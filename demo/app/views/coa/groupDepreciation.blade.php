@extends('template.header-footer')

@section('title')
Depreciation Group
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
@stop

@section('nav')@stop

@section('content')

@if(myCheckIsEmpty('Coa'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one COA to insert Depreciation Group.
</div>
@endif
@if(isset($messages))
@if($messages == 'gagalInsert' || $messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New depreciation group has been inserted.
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Depreciation group has been updated.
</div>
@endif
@if($messages == 'suksesDelete')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Depreciation group has been deleted.
</div>
@endif
@if($messages == 'gagalDelete')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Depreciation group has been registered in table depreciation.
</div>
@endif
@if($messages == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@endif

<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread hidden-xs" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showGroupDepreciation')}}" type="button" class="btn btn-sm btn-pure">Depreciation Group</a>
            </div>

            @if(checkModul('O01'))
            <a target="_blank" href="{{Route('exportGroupDepreciation')}}">
                <button type="button" class="btn btn-sm btn-green btn-export" id="btn-export">Export Excel</button>    
            </a>
            @endif
            <button type="button" <?php if (myCheckIsEmpty('Coa')) echo 'disabled' ?>  class="btn btn-green btn-insert" data-target="#m_groupDepreciation" data-toggle="modal" role="dialog">
                <span class="glyphicon glyphicon-plus"></span> New</button>
        </div>

        <div class="bread-xs visible-xs hidden-sm hidden-md hidden-lg">
            <p class="text-center"><a href="{{route('showDashboard')}}">{{Config::get('companyHeader.header_company');}}</a> / <a>{{ucfirst($toogle)}}</a> / <a href="{{route('showGroupDepreciation')}}">Depreciation Group</a></p>
        </div>

        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Depreciation Group</h4>
            </div>

            <div class="tableadd">
                <table id="example" class="display table-rwd table-group-depreciation" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Depreciation Group ID</th>
                            <th>Description</th>
                            <th>Long Depreciation</th>
                            <th>Assets Account</th>
                            <th>Cost Account</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $f = rand(0, 50);
                        foreach (GroupDepreciation::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                            $data->dtRecordformat = date("d-m-Y H:i:s", strtotime($data->dtRecord));
                            $data->dtModifformat = date("d-m-Y H:i:s", strtotime($data->dtModified));
                            $data->Remark = str_replace("\r\n", " ", $data->Remark);
                            $coaD = GroupDepreciation::coaDebet($data->InternalID);
                            $coaC = GroupDepreciation::coaCredit($data->InternalID);
                            $data->coaIDDebet = $coaD[0]->COAInternalID;
                            $data->coaIDCredit = $coaC[0]->COAInternalID;
                            $arrData = array($data);
                            $tamp = myEscapeStringData($arrData);
                            $tamp = myEncryptJavaScriptText($tamp, $f);
                            ?>
                            <tr>
                                <td>{{$data->GroupDepreciationID}}</td>
                                <td>{{$data->Description}}</td>
                                <td>{{$data->LongDepreciation.' Year'}}</td>
                                <td>{{Coa::formatCoa($data->ACC1InternalIDDebet,$data->ACC2InternalIDDebet,
                                                                                $data->ACC3InternalIDDebet,$data->ACC4InternalIDDebet,
                                                                                $data->ACC5InternalIDDebet,$data->ACC6InternalIDDebet,1).' '.$coaD[0]->COAName}}</td>
                                <td>{{Coa::formatCoa($data->ACC1InternalIDCredit,$data->ACC2InternalIDCredit,
                                                                                $data->ACC3InternalIDCredit,$data->ACC4InternalIDCredit,
                                                                                $data->ACC5InternalIDCredit,$data->ACC6InternalIDCredit,1).' '.$coaC[0]->COAName}}</td>
                                <td class="text-center"><button id="btn-{{$data->GroupDepreciationID}}" data-target="#m_groupDepreciationUpdate" data-all='{{$tamp}}'
                                                                data-toggle="modal" role="dialog"
                                                                class="btn btn-pure-xs btn-xs btn-edit">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </button>
                                    <button data-target="#m_groupDepreciationDelete" data-internal="{{$data->InternalID}}"  data-toggle="modal" role="dialog"
                                            data-id="{{$data->GroupDepreciationID}}" data-name='{{$data->Description}}' class="btn btn-pure-xs btn-xs btn-delete">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>    
            </div><!---end tableadd-->
        </div><!---- end div tabwrap---->
    </div><!---end primcontent-->
</div><!---end wrapjour-->
@stop

@section('modal')
<div class="modal fade bs-example-modal-lg nopaddl" id="m_groupDepreciation" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action" id="form-insert">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Insert Depreciation Group</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="margbot10">
                                    <input type='hidden' name='jenis' value='insertGroupDepreciation'>
                                </div>
                                <div class="margbot10">
                                    <label for="GroupDepreciationID">Depreciation Group ID *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="noSpecialCharacter" type="text" name="GroupDepreciationID" id="groupDepreciationID" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="GroupDepreciationDescription">Description *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Description" id="description" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="LongDepreciation">Long Depreciation (year) *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="number" maxlength="4" max="1000" min="1" name="LongDepreciation" id="longDepreciation" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="coaDebet">Assets Account *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal" id="coaD" style="" name="coaDebet">
                                        @foreach(Coa::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('ACC1InternalID')->orderBy('InternalID')->get() as $coa)
                                        <option value="{{$coa->InternalID}}">
                                            {{Coa::formatCoa($coa->ACC1InternalID,$coa->ACC2InternalID,$coa->ACC3InternalID,$coa->ACC4InternalID,$coa->ACC5InternalID,$coa->ACC6InternalID,'1')}} 
                                            {{" ".$coa->COAName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="coaCredit">Cost Account *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal" id="coaC" style="" name="coaCredit">
                                        @foreach(Coa::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('ACC1InternalID')->orderBy('InternalID')->get() as $coa)
                                        <option value="{{$coa->InternalID}}">
                                            {{Coa::formatCoa($coa->ACC1InternalID,$coa->ACC2InternalID,$coa->ACC3InternalID,$coa->ACC4InternalID,$coa->ACC5InternalID,$coa->ACC6InternalID,'1')}} 
                                            {{" ".$coa->COAName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;" name="remark" id="remark" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>


<div class="modal fade bs-example-modal-lg nopaddl" id="m_groupDepreciationUpdate" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action" id="form-update">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Depreciation Group</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="margbot10">
                                    <input type="hidden" value="" id="idUpdate" name="InternalID">
                                    <input type="hidden" value="updateGroupDepreciation" id="jenisUpdate" name="jenis">
                                </div>
                                <div class="margbot10">
                                    <label for="GroupDepreciationDescription">Description *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="Description" id="descriptionUpdate" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="LongDepreciation">Long Depreciation (year) *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="number" maxlength="4" max="1000" min="1" name="LongDepreciation" id="longDepreciationUpdate" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="coaDebet">Assets Account *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal" id="coaDUpdate" style="" name="coaDebet">
                                        @foreach(Coa::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('ACC1InternalID')->orderBy('InternalID')->get() as $coa)
                                        <option id="coaD{{$coa->InternalID}}" value="{{$coa->InternalID}}">
                                            {{Coa::formatCoa($coa->ACC1InternalID,$coa->ACC2InternalID,$coa->ACC3InternalID,$coa->ACC4InternalID,$coa->ACC5InternalID,$coa->ACC6InternalID,'1')}} 
                                            {{" ".$coa->COAName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="coaCredit">Cost Account *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal" id="coaCUpdate" style="" name="coaCredit">
                                        @foreach(Coa::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('ACC1InternalID')->orderBy('InternalID')->get() as $coa)
                                        <option id="coaC{{$coa->InternalID}}" value="{{$coa->InternalID}}">
                                            {{Coa::formatCoa($coa->ACC1InternalID,$coa->ACC2InternalID,$coa->ACC3InternalID,$coa->ACC4InternalID,$coa->ACC5InternalID,$coa->ACC6InternalID,'1')}} 
                                            {{" ".$coa->COAName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;"  name="remark" id="remarkUpdate" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <small>Created by <span id="createdDetail"></span></small><br>
                                    <small>Modified by <span id="modifiedDetail"></span></small>
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Update</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade" id="m_groupDepreciationDelete" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content ">
            <form action="" method="post" class="action">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Delete Depreciation Group</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">

                        <input type="hidden" value="" id="idDelete" name="InternalID">
                        <input type="hidden" value="deleteGroupDepreciation" id="jenisDelete" name="jenis">
                        <p>Are you sure want to delete <span id="deleteName"></span>?</p>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Yes</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
                </div>
            </form>      
        </div>
    </div>  
</div>
@stop
<?php
$s = myEncryptJavaScript(GroupDepreciation::select('GroupDepreciationID')->where('CompanyInternalID', Auth::user()->Company->InternalID)->get(), $f);
?>
@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript">
var a = '<?php echo $s; ?>';
var b = <?php echo $f; ?>;
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-coa/groupDepreciation.js')}}"></script>
@stop
