@extends('template.header-footer')

@section('title')
Journal
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosenCustom.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<link rel="stylesheet" href="{{Asset('morris.css')}}">
@stop

@section('nav')

@stop

@section('content')
@if(myCheckIsEmpty('Coa;Currency;Department;Coa5;Default'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one master COA, Currency, Department, Default COA and COA Level 5 to insert Journal.
</div>
@endif
@if(Session::get('messages') == 'journalTransaksi')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Journal which created by transaction cannot be update or delete.
</div>
@endif
@if(Session::get('messages') == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest"> 
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showJournal')}}" type="button" class="btn btn-sm btn-pure">Journal</a>
            </div>
            <div class="btn-group">
                <button <?php if (myCheckIsEmpty('Coa;Currency;Department;Coa5;Default')) echo 'disabled'; ?> type="button" class="btn btn-green btn-sm dropdown-toggle  margr5" data-toggle="dropdown" aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New 
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{Route('journalNew','Cash In')}}">Cash In</a></li>
                    <li><a href="{{Route('journalNew','Cash Out')}}">Cash Out</a></li>
                    <li><a href="{{Route('journalNew','Bank In')}}">Bank In</a></li>
                    <li><a href="{{Route('journalNew','Bank Out')}}">Bank Out</a></li>
                    <li><a href="{{Route('journalNew/Memorial')}}">Memorial</a></li>
                </ul>
            </div>
            <button <?php if (myCheckIsEmpty('Coa;Currency;Department;Coa5;Default')) echo 'disabled'; ?> id="search-button" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-search"></span> Search </button>
            @if(checkModul('O04'))
            <a href="{{Route("getPayable")}}" target="_blank" type="button" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-file"></span> Report Payable </a>
            <a href="{{Route("getReceivable")}}" target="_blank" type="button" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-file"></span> Report Receivable </a>
            @endif
        </div>
        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Search</h4>
            </div>
            <div class="tableadd">     
                <ul class="searchmenu">
                    <form method="GET" action="">
                        <li><label for="slipnumber">Slip</label>
                            <br>
                            <select class="chosen-select choosen-modal" id="slipID" style="" name="slipID">
                                <option value="-1">All</option>
                                @foreach(Slip::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $slip)
                                <option value="{{$slip->InternalID}}">
                                    {{$slip->SlipID.' '.$slip->SlipName}}
                                </option>
                                @endforeach
                            </select>
                        </li>
                        <li><label for="typeTransaction">Transaction Type</label>
                            <br><select name="typeTransaction" class="typetransaction">
                                <option value="-1">All</option>
                                <option value="Cash In">Cash In</option>
                                <option value="Cash Out">Cash Out</option>  
                                <option value="Bank In">Bank In</option>
                                <option value="Bank Out">Bank Out</option>
                                <option value="Memorial">Memorial</option>
                            </select>
                        </li>
                        <li><label for="date">Start Date</label>
                            <br><input id="startDate" name="startDate" type="text" autocomplete="off">
                        </li>
                        <li><label for="date">End Date</label>
                            <br><input id="endDate" name="endDate" type="text" autocomplete="off">
                        </li>
                        <li><br>
                            <button type="submit" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-search"></span> Search </button>
                        </li>
                        <li><br>
                            <button id="cancel" type="reset"  class="btn btn-pure btn-sm"><span class="glyphicon glyphicon-remove"></span> Cancel </button>
                        </li>
                    </form>
                </ul>
            </div><!---- end div tableadd---->
        </div><!---- end div tabwrap---->
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
<div class="wrapjour">
    <div class="primcontentnopadd">
        <div class="leftrow pull-left">
            <div class="tabwrap">
                <div class="tabhead">
                    <h4 class="headtitle">Account Payable Aging</h4>
                </div>
                <div class="tableadd overhide"> 
                    @if(Count(PurchaseHeader::getPurchasePayableAging()) > 0)
                    <div class="headinv new" id="graph">

                    </div><!---- end div new---->      
                    @else
                    <b><center>There is no data.</center></b>
                    @endif     
                </div><!---- end div tableadd---->   
            </div><!---- end div tabwrap---->                       
        </div><!---- end div leftrow---->
        <div class="rightrow pull-right">
            <div class="tabwrap">
                <div class="tabhead">
                    <h4 class="headtitle">Account Receivable Aging</h4>
                </div>
                <div class="tableadd ovaerhide"> 
                    @if(Count(SalesHeader::getSalesReceivableAging()) > 0)
                    <div class="headinv new" id="graph2">

                    </div><!---- end div new---->        
                    @else
                    <b><center>There is no data.</center></b>
                    @endif 
                </div><!---- end div tableadd---->   
            </div><!---- end div tabwrap---->                       
        </div><!---- end div rigthrow---->  
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->

@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script><script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNewJournal.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-coa/journal.js')}}"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.2/raphael-min.js"></script>
<script src="{{Asset('morris.js')}}" type="text/javascript"></script>

<?php
$telatKurang90S = 0;
$telatKurang60S = 0;
$telatKurang30S = 0;
$telatLebih90S = 0;
$telatKurang90P = 0;
$telatKurang60P = 0;
$telatKurang30P = 0;
$telatLebih90P = 0;
$interval = -30;
foreach (PurchaseHeader::getPurchasePayableAging() as $data) {
    if ($data->selisihHari >= $interval) {
        $telatKurang30P += $data->GrandTotal - JournalDetail::where('JournalTransactionID', $data->PurchaseID)->sum('JournalDebetMU');
        echo '$telatKurang30P' . $telatKurang30P;
    } else if ($data->selisihHari >= $interval * 2) {
        $telatKurang60P += $data->GrandTotal - JournalDetail::where('JournalTransactionID', $data->PurchaseID)->sum('JournalDebetMU');
        echo '$telatKurang60P' . $telatKurang60P;
    } else if ($data->selisihHari >= $interval * 3) {
        $telatKurang90P += $data->GrandTotal - JournalDetail::where('JournalTransactionID', $data->PurchaseID)->sum('JournalDebetMU');
        echo '$telatKurang90P' . $telatKurang90P;
    } else {
        $telatLebih90P += $data->GrandTotal - JournalDetail::where('JournalTransactionID', $data->PurchaseID)->sum('JournalDebetMU');
    }
}
foreach (SalesHeader::getSalesReceivableAging() as $data) {
    if ($data->selisihHari >= $interval) {
        $telatKurang30S += $data->GrandTotal - JournalDetail::where('JournalTransactionID', $data->SalesID)->sum('JournalCreditMU');
    } else if ($data->selisihHari >= $interval * 2) {
        $telatKurang60S += $data->GrandTotal - JournalDetail::where('JournalTransactionID', $data->SalesID)->sum('JournalCreditMU');
        ;
    } else if ($data->selisihHari >= $interval * 3) {
        $telatKurang90S += $data->GrandTotal - JournalDetail::where('JournalTransactionID', $data->SalesID)->sum('JournalCreditMU');
        ;
    } else {
        $telatLebih90S += $data->GrandTotal - JournalDetail::where('JournalTransactionID', $data->SalesID)->sum('JournalCreditMU');
        ;
    }
}
?>
<script>
<?php if (Count(PurchaseHeader::getPurchasePayableAging()) > 0) { ?>
    Morris.Bar({
        element: 'graph',
        data: [
            {y: '<=30', a: '<?php echo $telatKurang30P; ?>'},
            {y: '<=60', a: '<?php echo $telatKurang60P; ?>'},
            {y: '<=90', a: '<?php echo $telatKurang90P; ?>'},
            {y: '>90', a: '<?php echo $telatLebih90P; ?>'}
        ],
        xkey: 'y',
        xLabelMargin: 0,
        ykeys: ['a'],
        labels: ['Purchase Aging'],
        resize: true
    });
<?php } if (count(SalesHeader::getSalesReceivableAging()) > 0) { ?>
    Morris.Bar({
        element: 'graph2',
        data: [
            {y: '<=30', a: '<?php echo $telatKurang30S; ?>'},
            {y: '<=60', a: '<?php echo $telatKurang60S; ?>'},
            {y: '<=90', a: '<?php echo $telatKurang90S; ?>'},
            {y: '>90', a: '<?php echo $telatLebih90S; ?>'}
        ],
        xkey: 'y',
        xLabelMargin: 0,
        ykeys: ['a'],
        labels: ['Sales Aging'],
        resize: true
    });
<?php } ?>
</script>

@stop