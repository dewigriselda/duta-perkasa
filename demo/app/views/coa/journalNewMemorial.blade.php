@extends('template.header-footer')

@section('title')
Journal
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<style>
    .help-block {
        margin-left: 30% !important;
    }
</style>
@stop

@section('nav')

@stop

@section('content')
@if(myCheckIsEmpty('Coa;Currency;Department;Coa5;Default'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one master COA, Currency, Department, Default COA, and COA Level 5 to insert Journal memorial.
</div>
@endif
@if(isset($messages))
@if($messages == 'gagalInsert')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New journal has been inserted.
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontent mw1088">
        <div class="btnnest"> 
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showJournal')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Journal</a>
                <a href="{{route('journalNew/Memorial')}}" type="button" class="btn btn-sm btn-pure">New Journal Memorial</a>
            </div>
            <div class="btn-group">
                <button type="button" <?php if (myCheckIsEmpty('Coa;Currency;Department;Coa5;Default')) echo 'disabled'; ?>  class="btn btn-green btn-sm dropdown-toggle  margr5" data-toggle="dropdown" aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New 
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{Route('journalNew','Cash In')}}">Cash In</a></li>
                    <li><a href="{{Route('journalNew','Cash Out')}}">Cash Out</a></li>
                    <li><a href="{{Route('journalNew','Bank In')}}">Bank In</a></li>
                    <li><a href="{{Route('journalNew','Bank Out')}}">Bank Out</a></li>
                    <li><a href="{{Route('journalNew/Memorial')}}">Memorial</a></li>
                </ul>
            </div>
            <button id="search-button" <?php if (myCheckIsEmpty('Coa;Currency;Department;Coa5;Default')) echo 'disabled'; ?> class="btn btn-green btn-sm"><span class="glyphicon glyphicon-search"></span> Search </button>
            @if(checkModul('O04'))
            <a href="{{Route("getPayable")}}" target="_blank" type="button" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-file"></span> Report Payable </a>
            <a href="{{Route("getReceivable")}}" target="_blank" type="button" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-file"></span> Report Receivable </a>
            @endif
        </div>
        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Search</h4>
            </div>
            <div class="tableadd">     
                <ul class="searchmenu">
                    <form method="GET" action="{{Route('showJournal')}}">
                        <li><label for="slipnumber">Slip</label>
                            <br>
                            <select class="chosen-select choosen-modal" id="slipID" style="" name="slipID">
                                <option value="-1">All</option>
                                @foreach(Slip::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $slip)
                                <option value="{{$slip->InternalID}}">
                                    {{$slip->SlipID.' '.$slip->SlipName}}
                                </option>
                                @endforeach
                            </select>
                        </li>
                        <li><label for="typeTransaction">Transaction Type</label>
                            <br><select name="typeTransaction" >
                                <option value="-1">All</option>
                                <option value="Cash In">Cash In</option>
                                <option value="Cash Out">Cash Out</option>  
                                <option value="Bank In">Bank In</option>
                                <option value="Bank Out">Bank Out</option>
                                <option value="Memorial">Memorial</option>
                            </select>
                        </li>
                        <li><label for="date">Start Date</label>
                            <br><input id="startDate" name="startDate" type="text" autocomplete="off">
                        </li>
                        <li><label for="date">End Date</label>
                            <br><input id="endDate" name="endDate" type="text" autocomplete="off">
                        </li>
                        <li><br>
                            <button type="submit" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-search"></span> Search </button>
                        </li>
                        <li><br>
                            <button id="cancel" type="reset"  class="btn btn-pure btn-sm"><span class="glyphicon glyphicon-remove"></span> Cancel </button>
                        </li>
                    </form>
                </ul>
            </div><!---- end div tableadd---->
        </div><!---- end div tabwrap---->

        <form method="POST" action="">
            <input type="hidden" name="journalType" value="{{$jenis}}">
            <div class="tabwrap">
                <div class="tabhead">
                    <?php
                    $hariIni = date('d') . '-' . date('m') . '-' . date('Y');
                    $date = explode('-', $hariIni);
                    $yearDigit = substr($date[2], 2);
                    $cari = 'ME-' . $date[1] . $yearDigit;
                    ?>
                    <h4 class="headtitle">{{$jenis}} <span id="journalID">{{JournalHeader::getNextIDJournal($cari. '-')}}</span></h4>
                </div>
                <div class="tableadd"> 
                    <div class="headinv new">
                        <ul class="pull-left">
                            <li>
                                <label for="date">Date *</label>
                                <input id="date" name="date" type="text" autocomplete="off">
                            </li>
                            <li>
                                <label for="from">From *</label>
                                <input type="text" name="from" id="from" data-validation="required">
                            </li>
                            <li>
                                <label for="notes">Notes *</label>
                                <textarea name="journalNotes" id="journalNotes" data-validation="required"></textarea>
                            </li>
                        </ul>
                        <ul class="pull-right">
                            <li>
                                <label for="department">Department *</label>
                                <select class="chosen-select" id="department" style="" name="department">
                                    @foreach(Department::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $department)
                                    <option value="{{$department->InternalID}}">
                                        {{$department->DepartmentName}}
                                    </option>
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label for="coa5">COA 5*</label>
                                <select class="chosen-select" id="coa5" style="" name="coa5">
                                    @foreach(Coa5::where('InternalID','!=','0')->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $coa5)
                                    <option value="{{$coa5->InternalID}}">
                                        {{$coa5->ACC5ID.' '.$coa5->ACC5Name}}
                                    </option>
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label for="remark">Remark *</label>
                                <textarea name="remark" id="remark" data-validation="required"></textarea>
                            </li>
                            <li>
                                <div class="required">
                                    * Required
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="tableadd journupdate">
                        <table class="table master-data" id="table-jurnal">
                            <thead>
                                <tr>
                                    <th width="15%">Account</th>
                                    <th width="10%">Debet MU</th>
                                    <th width="10%">Credit MU</th>
                                    <th width="15%">Currency</th>
                                    <th width="10%">Rate</th>
                                    <th width="10%">Debet</th>
                                    <th width="10%">Credit</th>
                                    <th width="15%">Notes</th>
                                    <th width="5%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="row0">
                                    <td class="appd">
                                        <select class="chosen-select choosen-modal left" id="coa-0" name="coa[]">
                                            @foreach(Coa::getCoaNotSlip() as $coa)
                                            <option id="coa{{$coa->InternalID}}" value="{{$coa->InternalID}}">
                                                {{Coa::formatCoa($coa->ACC1InternalID,$coa->ACC2InternalID,$coa->ACC3InternalID,$coa->ACC4InternalID,$coa->ACC5InternalID,$coa->ACC6InternalID,'1')}} 
                                                {{" ".$coa->COAName}}
                                            </option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="dcmu">
                                        <input type="text" class="maxWidth debetJournal right numajaDesimal" name="Debet_value[]" maxlength="200" id="debet-0" value="0">
                                    </td>
                                    <td class="dcmu">
                                        <input type="text" class="maxWidth kreditJournal right numajaDesimal" name="Kredit_value[]" maxlength="200" id="kredit-0" value="0">
                                    </td>
                                    <td>
                                        <select class="chosen-select choosen-modal currency" id="currency" name="currency">
                                            {{'';$default = 0;}}
                                            @foreach(Currency::orderBy('Default','desc')->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $cur)
                                            <option id="cur{{$cur->InternalID}}" value="{{$cur->InternalID.'---;---'.$cur->CurrencyName.'---;---'.$cur->Rate.'---;---'.$cur->Default}}">
                                                {{$cur->CurrencyName;}}
                                                @if($default == 0)
                                                {{'';$default=$cur->Rate}}
                                                @endif
                                            </option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="ratetd">
                                        <input type="text" class="maxWidth rate right numajaDesimal" name="rate" maxlength="" id="rate" value="{{$default}}">
                                    </td>
                                    <td id="debet-0-hitung" class="right">
                                        0
                                    </td>
                                    <td id="kredit-0-hitung" class="right">
                                        0
                                    </td>
                                    <td>
                                        <textarea name="notes[]" id="notes" style="resize:none"></textarea>
                                    </td>
                                    <td class="text-center">
                                        -
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div><!---- end div tableadd----> 
                    <h5 class="right margr10 h5total"><b id="totalDebet">Total Debet : 0</b></h5>
                    <h5 class="right margr10 h5total"><b id="totalKredit">Total Credit : 0</b></h5>
                    <button type="button" class="btn btn-green btn-sm margl10" id="btn-addRow"><span class="glyphicon glyphicon-plus"></span> Add row</button>    
                </div><!---- end div tableadd---->   
            </div><!---- end div tabwrap---->  
            <div class="btnnest pull-right">
                <button class="btn btn-green btn-sm btn-save" id="btn-save" <?php if (myCheckIsEmpty('Coa;Currency;Department;Coa5;Default')) echo 'disabled'; ?> > Save </button>
            </div>
        </form>
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script><script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNewJournal.jquery.js')}}" type="text/javascript"></script>
<script>
var textSelect = '<select class="chosen-select choosen-modal" id="" style="" name="coa[]">' +
<?php foreach (Coa::getCoaNotSlip() as $coa) { ?> '<option id="coa<?php echo $coa->InternalID ?>" value="<?php echo $coa->InternalID ?>">' +
            '<?php echo Coa::formatCoa($coa->ACC1InternalID, $coa->ACC2InternalID, $coa->ACC3InternalID, $coa->ACC4InternalID, $coa->ACC5InternalID, $coa->ACC6InternalID, '1') . " " . $coa->COAName ?>' + '</option>' +<?php } ?>
'</select>';
var tanggalHariIni = '<?php echo date('d') . '-' . date('m') . '-' . date('Y') ?>';
<?php if ($tipe == 'Memo') { ?>
    var tipeJournal = 'Memo';
<?php } else { ?>
    var tipeJournal = 'InOut';
<?php } ?>
var journalSearch = '<?php echo Route('formatCariIDJournalMemorial') ?>';
    var jenis = '<?php echo $jenis; ?>';
</script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-coa/journal.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-coa/journalNewMemorial.js')}}"></script>
@stop