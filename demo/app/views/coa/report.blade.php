@extends('template.header-footer')

@section('title')
Accounting Report
@stop

@section('css')
<link rel="stylesheet" href="{{Asset('css/chosenCustom.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
@stop

@section('nav')

@stop

@section('content')
@if(myCheckIsEmpty('Journal'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one journal to create accounting report.
</div>
@endif
@if(Session::get('messages') == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest hidden-xs"> 
            <div class="btn-group bread nomarg" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showReportAccounting')}}" type="button" class="btn btn-sm btn-pure">Accounting Report</a>
            </div>
        </div>

        <div class="bread-xs visible-xs hidden-sm hidden-md hidden-lg">
            <p class="text-center"><a href="{{route('showDashboard')}}">{{Config::get('companyHeader.header_company');}}</a> / <a>{{ucfirst($toogle)}}</a> / <a href="{{route('showReportAccounting')}}">Accounting Report</a></p>
        </div>
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
<div class="wrapjour">
    <div class="primcontentnopadd">
        <div class="row">
            <div class="col-md-4">
                <div class="tabwrap">
                    <div class="tabhead">
                        <h4 class="headtitle">Balance</h4>
                    </div>
                    <div class="tableadd">
                        <?php if (!myCheckIsEmpty('Journal')) { ?> 
                            <form class="form-horizontal" method="POST" action="" target="_blank">
                                <input type="hidden" value="balance" name="jenis">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" style="padding-right: 0px !important"> Month </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="month" >
                                            <option value="1">January</option>
                                            <option value="2">February</option>
                                            <option value="3">March</option>  
                                            <option value="4">April</option>  
                                            <option value="5">May</option>  
                                            <option value="6">June</option>  
                                            <option value="7">July</option>  
                                            <option value="8">August</option>  
                                            <option value="9">September</option>  
                                            <option value="10">October</option>  
                                            <option value="11">November</option>  
                                            <option value="12">December</option>    
                                        </select>  
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" style="padding-right: 0px !important"> Year </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="year" >
                                            @for($i = $yearmax; $i >= $yearmin ; $i--)
                                            <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>  
                                        <input type="checkbox" id="ZeroBalance" name="ZeroBalance" checked="true" value="1"> <label for="ZeroBalance">Zero Value</label>
                                        <br>
                                        <button <?php if (myCheckIsEmpty('Journal')) echo 'disabled'; ?> class="btn btn-green btn-sm btn-save pull-right" id="btn-submit"> Process </button>
                                    </div>
                                </div>
                            </form>
                        <?php } else { ?>
                            <h5>There is no journal.</h5>
                        <?php } ?>
                    </div><!---- end div tableadd---->   
                </div><!---- end div tabwrap---->                       
            </div><!---- end div col-md-4----> 

            <div class="col-md-4">
                <div class="tabwrap">
                    <div class="tabhead">
                        <h4 class="headtitle">Profit and Loss</h4>
                    </div>
                    <div class="tableadd "> 
                        <?php if (!myCheckIsEmpty('Journal')) { ?> 
                            <form class="form-horizontal" method="POST" action="" target="_blank">
                                <input type="hidden" value="profitLoss" name="jenis">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" style="padding-right: 0px !important"> Month </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="month" >
                                            <option value="1">January</option>
                                            <option value="2">February</option>
                                            <option value="3">March</option>  
                                            <option value="4">April</option>  
                                            <option value="5">May</option>  
                                            <option value="6">June</option>  
                                            <option value="7">July</option>  
                                            <option value="8">August</option>  
                                            <option value="9">September</option>  
                                            <option value="10">October</option>  
                                            <option value="11">November</option>  
                                            <option value="12">December</option>     
                                        </select>  
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" style="padding-right: 0px !important"> Year </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="year" >
                                            @for($i = $yearmax; $i >= $yearmin ; $i--)
                                            <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>  
                                        <input type="checkbox" id="ZeroProfitLoss" name="ZeroProfitLoss" checked="true" value="1"> <label for="ZeroProfitLoss">Zero Value</label>
                                        <br>
                                        <button <?php if (myCheckIsEmpty('Journal')) echo 'disabled'; ?> class="btn btn-green btn-sm btn-save pull-right" id="btn-submit"> Process </button>
                                    </div>
                                </div>
                            </form>
                        <?php } else { ?>
                            <h5>There is no journal.</h5>
                        <?php } ?>
                    </div><!---- end div tableadd---->   
                </div><!---- end div tabwrap---->                       
            </div><!---- end div col-md-4----> 

            <div class="col-md-4">
                <div class="tabwrap">
                    <div class="tabhead">
                        <h4 class="headtitle">General Ledger</h4>
                    </div>
                    <div class="tableadd ">
                        <?php if (!myCheckIsEmpty('Journal')) { ?> 
                            <form class="form-horizontal" method="POST" action="" target="_blank">
                                <input type="hidden" value="general" name="jenis">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" style="padding-right: 0px !important"> Month </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="month" >
                                            <option value="1">January</option>
                                            <option value="2">February</option>
                                            <option value="3">March</option>  
                                            <option value="4">April</option>  
                                            <option value="5">May</option>  
                                            <option value="6">June</option>  
                                            <option value="7">July</option>  
                                            <option value="8">August</option>  
                                            <option value="9">September</option>  
                                            <option value="10">October</option>  
                                            <option value="11">November</option>  
                                            <option value="12">December</option>    
                                        </select>  
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" style="padding-right: 0px !important"> Year </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="year" >
                                            @for($i = $yearmax; $i >= $yearmin ; $i--)
                                            <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>  
                                        <input type="checkbox" name="ZeroGeneralLedger" id="ZeroGeneralLedger" checked="true" value="1"> <label for="ZeroGeneralLedger">Zero Value</label>
                                        <br>
                                        <button <?php if (myCheckIsEmpty('Journal')) echo 'disabled'; ?> class="btn btn-green btn-sm btn-save pull-right" id="btn-submit"> Process </button>
                                    </div>
                                </div>
                            </form>
                        <?php } else { ?>
                            <h5>There is no journal.</h5>
                        <?php } ?>
                    </div><!---- end div tableadd---->   
                </div><!---- end div tabwrap---->                       
            </div><!---- end div col-md-4----> 

        </div><!-- end div row-->

    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop

@section('modal')

@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.2/raphael-min.js"></script>
<script src="{{Asset('morris.js')}}" type="text/javascript"></script>
<script>
</script>
@stop