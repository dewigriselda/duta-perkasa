@extends('template.header-footer')

@section('title')
Slip
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
@stop

@section('nav')

@stop

@section('content')

@if(myCheckIsEmpty('Coa;Currency'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one COA and currency to insert Slip.
</div>
@endif
@if(myCheckIsEmpty('DefaultCurrency'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one default currency to create Slip Report.
</div>
@endif
@if(isset($messages))
@if($messages == 'gagalInsert' || $messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New slip has been inserted.
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Slip has been updated.
</div>
@endif
@if($messages == 'suksesDelete')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Slip has been deleted.
</div>
@endif
@if($messages == 'gagalDelete')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Slip has been registered in table journal.
</div>
@endif
@if($messages == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@endif


<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest option-slip">
            <div class="btn-group bread hidden-xs" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showSlip')}}" type="button" class="btn btn-sm btn-pure">Slip</a>
            </div>
            @if(checkModul('O01'))
            <a target="_blank" href="{{Route('exportSlip')}}">
                <button type="button" class="btn btn-sm btn-green btn-export" id="btn-export">Export Excel</button>    
            </a>
            @endif
            <button type="button" <?php if (myCheckIsEmpty('Coa;Currency')) echo 'disabled'; ?> class="btn btn-green btn-insert margr5" data-target="#m_slip" data-toggle="modal" role="dialog">
                <span class="glyphicon glyphicon-plus"></span> New</button>
            @if(checkModul('O04'))
            <button type="button" class="btn btn-green margr5" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rSlipMU">
                <span class="glyphicon glyphicon-file"></span> Slip Report MU</button>
            <button type="button" <?php if (myCheckIsEmpty('DefaultCurrency')) echo 'disabled'; ?> class="btn btn-green " data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rSlip">
                <span class="glyphicon glyphicon-file"></span> Slip Report</button>
            @endif
        </div>

        <div class="bread-xs visible-xs hidden-sm hidden-md hidden-lg">
            <p class="text-center"><a href="{{route('showDashboard')}}">{{Config::get('companyHeader.header_company');}}</a> / <a>{{ucfirst($toogle)}}</a> / <a href="{{route('showSlip')}}">Slip</a></p>
        </div>

        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Slip</h4>
            </div>

            <div class="tableadd">
                <table id="example" class="display table-rwd table-slip" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Slip ID</th>
                            <th>Name</th>
                            <th>Account ID</th>
                            <th>Account Name</th>
                            <th>Currency</th>
                            <th>Type</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{''; $f = rand(0,50);}}
                        <?php
                        foreach (Slip::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                            $data->dtRecordformat = date("d-m-Y H:i:s", strtotime($data->dtRecord));
                            $data->dtModifformat = date("d-m-Y H:i:s", strtotime($data->dtModified));
                            $data->Remark = str_replace("\r\n", " ", $data->Remark);
                            $coa = Slip::coa($data->InternalID);
                            $data->coaID = $coa[0]->COAInternalID;
                            $currency = Slip::find($data->InternalID)->Currency;
                            $arrData = array($data);
                            $tamp = myEscapeStringData($arrData);
                            $tamp = myEncryptJavaScriptText($tamp, $f);
                            ?>
                            <tr>
                                <td>{{$data->SlipID}}</td>
                                <td>{{$data->SlipName}}</td>
                                <td>{{Coa::formatCoa($data->ACC1InternalID,$data->ACC2InternalID,
                                                                                        $data->ACC3InternalID,$data->ACC4InternalID,
                                                                                        $data->ACC5InternalID,$data->ACC6InternalID,1)}}</td>
                                <td>{{$coa[0]->COAName}}</td>
                                <td>{{$currency->CurrencyName}}</td>
                                @if($data->Flag == "0")
                                <td>Kas</td>
                                @endif
                                @if($data->Flag == "1")
                                <td>Bank</td>
                                @endif
                                @if($data->Flag == "2")
                                <td>Piutang giro</td>
                                @endif
                                @if($data->Flag == "3")
                                <td>Hutang giro</td>
                                @endif
                                <td class="text-center"><button id="btn-{{$data->SlipID}}" data-target="#m_slipUpdate" data-all='{{$tamp}}'
                                                                data-toggle="modal" role="dialog"
                                                                class="btn btn-pure-xs btn-xs btn-edit">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </button>
                                    <button data-target="#m_slipDelete" data-internal="{{$data->InternalID}}"  data-toggle="modal" role="dialog"
                                            data-id="{{$data->SlipID}}" data-name='{{$data->SlipName}}' class="btn btn-pure-xs btn-xs btn-delete">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>    
            </div><!---end tableadd-->
        </div><!---- end div tabwrap---->
    </div><!---end primcontent-->
</div><!---end wrapjour-->
@stop

@section('modal')
<div class="modal fade bs-example-modal-lg " id="m_slip" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action" id="form-insert">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Insert Slip</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="margbot10">
                                    <input type='hidden' name='jenis' value='insertSlip'>
                                </div>
                                <div class="margbot10">
                                    <label for="SlipID">Slip ID *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="noSpecialCharacter" type="text" name="SlipID" id="slipID" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="SlipName">Name *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="noSpecialCharacter" type="text" name="SlipName" id="name" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="Type">Type *</label>
                                </div>
                                <div class="margbot10">
                                    <select name="Type" id="selectJenis" class="theme">
                                        <option value="0">Kas</option>
                                        <option value="1">Bank</option>
                                        <!--                                    <option value="2">Piutang giro</option>
                                                                            <option value="3">Hutang giro</option>-->
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="coa">Account *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal" id="coa" style="" name="coa">
                                        @foreach(Coa::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('ACC1InternalID')->orderBy('InternalID')->get() as $coa)
                                        <option value="{{$coa->InternalID}}">
                                            {{Coa::formatCoa($coa->ACC1InternalID,$coa->ACC2InternalID,$coa->ACC3InternalID,$coa->ACC4InternalID,$coa->ACC5InternalID,$coa->ACC6InternalID,'1')}} 
                                            {{" ".$coa->COAName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="currency">Currency *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal" id="currencyInsert" style="" name="currency">
                                        @foreach(Currency::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('Default','desc')->get() as $cur)
                                        <option value="{{$cur->InternalID}}">
                                            {{$cur->CurrencyName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;" name="remark" id="remark" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>


<div class="modal fade bs-example-modal-lg" id="m_slipUpdate" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action" id="form-update">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Slip</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="margbot10">
                                    <input type="hidden" value="" id="idUpdate" name="InternalID">
                                    <input type="hidden" value="updateSlip" id="jenisUpdate" name="jenis">
                                </div>
                                <div class="margbot10">
                                    <label for="SlipName">Name *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="noSpecialCharacter" type="text" name="SlipName" id="nameUpdate" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="Type">Type *</label>
                                </div>
                                <div class="margbot10">
                                    <select name="Type" id="jenisUpdate" class="theme">
                                        <option id="type0" value="0">Kas</option>
                                        <option id="type1" value="1">Bank</option>
                                        <!--                                    <option id="type2" value="2">Piutang giro</option>
                                                                            <option id="type3" value="3">Hutang giro</option>-->
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="coa">Account *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal" id="coaUpdate" style="" name="coa">
                                        @foreach(Coa::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('ACC1InternalID')->orderBy('InternalID')->get() as $coa)
                                        <option id="coa{{$coa->InternalID}}" value="{{$coa->InternalID}}">
                                            {{Coa::formatCoa($coa->ACC1InternalID,$coa->ACC2InternalID,$coa->ACC3InternalID,$coa->ACC4InternalID,$coa->ACC5InternalID,$coa->ACC6InternalID,'1')}} 
                                            {{" ".$coa->COAName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="currency">Currency *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal" id="currencyUpdate" style="" name="currency">
                                        @foreach(Currency::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('Default','desc')->get() as $cur)
                                        <option id="cur{{$cur->InternalID}}" value="{{$cur->InternalID}}">
                                            {{$cur->CurrencyName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;"  name="remark" id="remarkUpdate" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <small>Created by <span id="createdDetail"></span></small><br>
                                    <small>Modified by <span id="modifiedDetail"></span></small>
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Update</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade bs-example-modal-lg" id="m_slipDelete" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <form action="" method="post" class="action">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Delete Slip</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="" id="idDelete" name="InternalID">
                            <input type="hidden" value="deleteSlip" id="jenisDelete" name="jenis">
                            <p>Are you sure want to delete <span id="deleteName"></span>?</p>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Yes</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade bs-example-modal-lg" id="r_summary" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action" id="" target="_blank">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="titleReport">Slip Report</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="margbot10">
                                    <input type='hidden' name='jenis' id="jenisReport" value='slipReport'>
                                </div>
                                <div class="margbot10">
                                    <label for="slip">Slip ID *</label>
                                    <select class="chosen-select" id="slip" style="" name="slip">
                                        <?php $currencySlip = ''; ?>
                                        @foreach(Slip::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $slip)
                                        <option value="{{$slip->InternalID}}">
                                            {{$slip->SlipID.' '.$slip->SlipName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="sDate">Start Date *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="sDate" id="startDateReport" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="eDate">End Date *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="eDate" id="endDateReport" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                    <?php if (Slip::where('CompanyInternalID', Auth::user()->Company->InternalID)->count() > 0) { ?>
                        <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                    <?php } else { ?>
                        <button type="button" disabled class="btn btn-green">Submit</button>
                    <?php } ?>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>
@stop
<?php
$s = myEncryptJavaScript(Slip::select('SlipID')->where('CompanyInternalID', Auth::user()->Company->InternalID)->get(), $f);
?>
@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript">
var a = '<?php echo $s; ?>';
var b = <?php echo $f; ?>;
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-coa/slip.js')}}"></script>
@stop