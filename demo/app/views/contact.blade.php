@extends('template.header-footer')

@section('title')
Contact Us
@stop

@section('css')
<link rel="stylesheet" href="{{Asset('css/chosenCustom.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
@stop

@section('nav')

@stop

@section('content')
@if(isset($messages))
@if($messages == 'suksesContact')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Your message has been sent, please wait our response in your email.
</div>
@else
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@endif
<?php $payment = 'Sudah'; $memory = 'Belum';?>
@if(Auth::user()->Company->StatusPayment == '0' || date("Y-m-d", strtotime(Auth::user()->Company->ExpiredDate)) < date("Y-m-d", strtotime(date('Y-m-d'))))
<?php $payment = 'Belum'?>
@endif
@if(countMemory() >= Auth::user()->Company->Package->Memory)
<?php $memory = 'Limit'?>
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest"> 
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showContact')}}" type="button" class="btn btn-sm btn-pure">Contact Us</a>
            </div>
        </div>
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
<div class="wrapjour">
    <div class="primcontentnopadd">
        <div class="row">
            <div class="col-md-12">
                <div class="tabwrap">
                    <div class="tabhead">
                        <h4 class="headtitle">Message From {{Auth::user()->UserName.' | '.Auth::user()->Email}}</h4>
                    </div>
                    <div class="tableadd">
                        <form class="form-horizontal" method="POST" action="">
                            <input type="hidden" value="contact" name="jenis">
                            <div class="form-group">
                                <label class="col-sm-2 control-label"> Subject </label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="text" data-validation="required" name="Subject" maxlength="32">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"> Message </label>
                                <div class="col-sm-9">
                                    <textarea  class="form-control" data-validation="required" name="message" style="resize: none"></textarea><br>
                                    <button class="btn btn-green btn-sm btn-save pull-right" id="btn-submit"> Submit </button>
                                </div>
                            </div>
                        </form>
                    </div><!---- end div tableadd---->   
                </div><!---- end div tabwrap---->                       
            </div><!---- end div col-md-4----> 
        </div><!-- end div row-->
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop

@section('modal')
<div class="modal fade" id="payment" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Warning Payment</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action">
                        <ul>
                            <p>Please complete your payment to start using this application.</p>
                            <p>Via Transfer : </p>
                            <b><p> 512-044-1622 (BCA KCP: Prapen Surabaya) </p>
                            <p> AN: Donny Susanto Witono</p></b>
                            <br>
                            <p>Regards,</p>
                            <p>Salmon Accounting</p>
                        </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>      
        </div>
    </div>  
</div>
<div class="modal fade" id="memory" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Warning Memory</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action">
                        <ul>
                            <p>You have reached your memory limit.</p>
                            <p>You can contact us via phone or website.</p>
                            <p>Regards,</p>
                            <p>Salmon Accounting</p>
                        </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>      
        </div>
    </div>  
</div>
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script>
var message = '<?php echo $payment; ?>'
if (message == 'Belum') {
    $("#payment").modal("show");
}
var message2 = '<?php echo $memory; ?>'
if (message2 == 'Limit') {
    $("#memory").modal("show");
}
//$.validate();
</script>
@stop