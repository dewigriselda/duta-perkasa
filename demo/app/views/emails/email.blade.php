<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <div>
            <h2>Activation Your Account</h2>

            <p>Dear, {{$companyName}}</p>
            <p>You're only one step away from using your account.
                Just click on the link below to verify your e-mail address.
                <a href="{{$link}}">Verify Now</a>
            </p>
            <p>If you are unable to activate your account by clicking on the button above, click the link below or copy and paste it into the address bar of your web browser:
            </p>
            <a href="{{$link}}">{{$link}}</a>
            
            <br> <p>Thank you.</p>
            <br>
            <p>Best Regards, </p>
            <p>Salmon Accounting</p>
        </div>
    </body>
</html>