<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <div style="font-size: 12px;">
            <h2>Transfer Commission for Withdraw {{$WithdrawID . ' ' . $WithdrawDate}}</h2>

            <p>Detail :</p>
            <table>
                <tbody>
                    <tr>
                        <td style="text-align: right">Commission ID :</td>
                        <td>{{'C.' . $WithdrawID}}</td>
                        <td style="width: 10%"></td>
                        <td>Salmon Accounting</td>
                    </tr>
                    <tr>
                        <td style="text-align: right">Date :</td>
                        <td>{{date("d M Y")}}</td>
                        <td style="width: 10%"></td>
                        <td>Prapen Indah Timur 7 AD 19, Surabaya - Indonesia</td>
                    </tr>
                    <tr>
                        <td style="text-align: right">Subject :</td>
                        <td>Transfer Commission</td>
                        <td style="width: 10%"></td>
                        <td>Phone: 031-569740 / 0815566468 || Fax: 02584237</td>
                    </tr>
                </tbody>
            </table>
            <br>
            <table style="width: 80%;">
                <thead style="border: 1px solid;">
                    <tr>
                        <th style="text-align: center; width: 10%;">No</th>
                        <th style="text-align: center; width: 50%;">Remark</th>
                        <th style="text-align: center; width: 20%;">Account</th>
                        <th style="text-align: center; width: 20%;">Nominal</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="text-align: center">1. </td>
                        <td style="text-align: left">Transfer Commission to Agent {{$UserID . ' - ' .$UserName}} </td>
                        <td style="text-align: left">{{$BankAccount . ' ' . $AccountNumber . ' - ' . $AccountName}} </td>
                        <td style="text-align: right">{{$WithdrawNominal}}</td>
                    </tr>
                    <tr>
                        <td colspan="4"><hr></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align: right">Total</td>
                        <td style="text-align: right">Rp. {{$WithdrawNominal}}</td>
                    </tr>
                </tbody>
            </table>
            <p>If there's something wrong or missing with your commission.</p>
            <p>Please contact us at </p>
            <p>Email: salmon.support@salmonacc.com || Phone: 031-569740 / 0815566468 || Fax: 02584237.</p>
            <br>
            <p>Best Regards, </p>
            <p>Salmon Accounting</p>
        </div>
    </body>
</html>