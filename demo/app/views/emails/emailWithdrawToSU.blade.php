<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <div style="font-size: 12px;">
            <h2>Agent {{$UserID . ' - ' .$UserName}}</h2>

            <p>Withdraw Detail:</p>
            <table>
                <tbody>
                    <tr>
                        <td style="text-align: right">Withdraw ID :</td>
                        <td>{{$WithdrawID}}</td>
                        <td style="width: 10%"></td>
                        <td>Agent {{$UserID . ' - ' .$UserName}}</td>
                    </tr>
                    <tr>
                        <td style="text-align: right">Date :</td>
                        <td>{{$WithdrawDate}}</td>
                        <td style="width: 10%"></td>
                        <td>{{$Email}}</td>
                    </tr>
                    <tr>
                        <td style="text-align: right">Subject :</td>
                        <td>Withdraw Commission</td>
                        <td style="width: 10%"></td>
                        <td>{{$BankAccount . ' ' . $AccountNumber . ' - ' . $AccountName}}</td>
                    </tr>
                </tbody>
            </table>
            <br>
            <table style="width: 80%;">
                <thead style="border: 1px solid;">
                    <tr>
                        <th style="text-align: center; width: 10%;">No</th>
                        <th style="text-align: center; width: 70%;">Remark</th>
                        <th style="text-align: center; width: 20%;">Nominal</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="text-align: center">1. </td>
                        <td style="text-align: left">Withdraw Commission from Agent {{$UserID . ' - ' .$UserName}}</td>
                        <td style="text-align: right">{{$WithdrawNominal}}</td>
                    </tr>
                    <tr>
                        <td colspan="3"><hr></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: right">Total</td>
                        <td style="text-align: right">Rp. {{$WithdrawNominal}}</td>
                    </tr>
                </tbody>
            </table>
            <br>
            <p>Best Regards, </p>
            <p>Salmon Server</p>
        </div>
    </body>
</html>