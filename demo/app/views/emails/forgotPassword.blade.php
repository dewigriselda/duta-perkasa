<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <div>
            <h2>Reset Your Password</h2>

            <p>Dear, {{$name}}</p>
            <p>To reset your password please click link below:
                <a href="{{$link}}">Reset your password</a>
            </p>
            <p>If you are unable to reset your password by clicking on the button above, click the link below or copy and paste it into the address bar of your web browser:
            </p>
            <a href="{{$link}}">{{$link}}</a>
            
            <br>
            <p>Best Regards, </p>
            <p>Salmon Accounting</p>
        </div>
    </body>
</html>