<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <div>
            <h2>Your Account is Active</h2>

            <p>Dear, {{$companyName}}</p>
            <p>Thank you, your payment has been completed.
                Now Salmon Application is ready to use.
            </p>
            <p>
                Please Login using the following link : <a href="http://application.salmonacc.com/login">http://application.salmonacc.com/login</a>
            </p>
            <br>
            <p>Best Regards, </p>
            <p>Salmon Accounting</p>
        </div>
    </body>
</html>