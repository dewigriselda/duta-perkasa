<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <div style="font-size: 12px;">
            <h2>Invoice Salmon Application Web Package</h2>

            <p>Dear, {{$companyName}}</p>
            <p>We have sent your invoice for Salmon Application Web Package {{$plan}} as follows:</p>
            <table>
                <tbody>
                    <tr>
                        <td style="text-align: right">Invoice ID :</td>
                        <td>{{$invoiceID}}</td>
                        <td style="width: 10%"></td>
                        <td>Salmon Accounting</td>
                    </tr>
                    <tr>
                        <td style="text-align: right">Company ID :</td>
                        <td>{{$companyID}}</td>
                        <td style="width: 10%"></td>
                        <td>Jalan Prapen Indah Timur 7 AD 19</td>
                    </tr>
                    <tr>
                        <td style="text-align: right">Company Name :</td>
                        <td>{{$companyName}}</td>
                        <td style="width: 10%"></td>
                        <td>Tlp: 031-569740 || HP: 0815566468 || Fax: 02584237</td>
                    </tr>
                </tbody>
            </table>
            <br>
            <table style="width: 80%;">
                <thead style="border: 1px solid;">
                    <tr>
                        <th style="text-align: center; width: 10%;">No</th>
                        <th style="text-align: center; width: 70%;">Remark</th>
                        <th style="text-align: center; width: 20%;">Price</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="text-align: center">1. </td>
                        <td style="text-align: left">Application Salmon {{$plan}} Version (Kuota {{$kuota}})</td>
                        <td style="text-align: right">{{$harga}}</td>
                    </tr>
                    <tr>
                        <td colspan="3"><hr></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: right">Total</td>
                        <td style="text-align: right">{{$harga}}</td>
                    </tr>
                </tbody>
            </table>   
            <p>Please finish your payment via Transfer : </p> 
            <p><b>512-044-1622 (BCA KCP: Prapen Surabaya) | AN: Donny Susanto Witono</b></p>
	    <p>After finished your payment, application will be activated maximum in 3 days work.</p>
            <br>
            <p>Best Regards, </p>
            <p>Salmon Accounting</p>
        </div>
    </body>
</html>