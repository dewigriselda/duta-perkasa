@extends('template.header-footer')

@section('title')
Help - Salmon Accounting
@stop 

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.css')}}">
@stop

@section('nav')

@stop

@section('content')

<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a href="{{route('showHelp')}}" type="button" class="btn btn-sm btn-pure">Help</a>
            </div>
            <a target="_blank" href="{{Asset('User Manual Salmon Online.zip')}}">
                <button type="button" class="btn btn-sm btn-green btn-export" id="btn-export">Download Help File</button>  
            </a>
        </div>

        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">How to use Salmon Accounting</h4>
            </div>
            <div class="tableadd">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    How to customize Chart of Account (COA)
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                <h4>Insert new account </h4>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <h5>1. Click new button that marked in image below. </h5>
                                        <img src="{{Asset('img/helpimage/new-coa-insert.jpg')}}" class="img-thumbnail">
                                    </div>

                                    <div class="col-xs-6">
                                        <h5>2. Fill the blank input from the popped up modal then click submit button.  </h5>
                                        <img src="{{Asset('img/helpimage/coa-insert-modal.jpg')}}" class="img-thumbnail">
                                    </div>
                                </div>
                                <h4>Update and delete account </h4>
                                <div class="row">
                                    <div class="col-xs-8">
                                        <h5>Press button from <b>Action</b> row that has icon pencil to edit and trash icon to delete.</h5>
                                        <img src="{{Asset('img/helpimage/new-coa-update.jpg')}}" class="img-thumbnail">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    How to set up Currency 
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <h5>1. Press new button marked below.</h5>
                                        <img src="{{Asset('img/helpimage/currency-insert.jpg')}}" class="img-thumbnail">
                                    </div>
                                    <div class="col-xs-6">
                                        <h5>2. After modal popped up fill the blank input.</h5>
                                        <h5>Note: you can change the new currency is default or not and then click submit button. </h5>
                                        <img src="{{Asset('img/helpimage/currency-modal.jpg')}}" class="img-thumbnail">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    How to create new Customer and Supplier 
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <h5>1. Go to COA 6 page, in this page you have your customer and supplier data.</h5>
                                        <img src="{{Asset('img/helpimage/coa6.jpg')}}" class="img-thumbnail">
                                    </div>
                                    <div class="col-xs-6">
                                        <h5>2. Press new button to open insert COA level 6 modal.</h5>
                                        <img src="{{Asset('img/helpimage/coa6-insert.jpg')}}" class="img-thumbnail">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-8">
                                        <h5>3. Fill the blank input, choose your COA 6 type, and then click save button.</h5>
                                        <img src="{{Asset('img/helpimage/coa6-insert-modal.jpg')}}" class="img-thumbnail">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFour">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    How to create a new Journal
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <h5>1. Press new button and choose journal's type from the drop down menu.</h5>
                                        <img src="{{Asset('img/helpimage/jurnal.jpg')}}" class="img-thumbnail">
                                    </div>
                                    <div class="col-xs-6">
                                        <h5>2. Fill the blank input, and then click save button. </h5>
                                        <img src="{{Asset('img/helpimage/jurnal-new.jpg')}}" class="img-thumbnail">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFive">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    How to create a new Transaction
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <h5>1. Go to Sales Order page and press new button marked below.</h5>
                                        <h5>Note: If you are not an user with order extension then you will create transaction from sales page.</h5>
                                        <img src="{{Asset('img/helpimage/sales-order.jpg')}}" class="img-thumbnail">
                                    </div>
                                    <div class="col-xs-6">
                                        <h5>2. Fill the blank input and then save.</h5>
                                        <img src="{{Asset('img/helpimage/sales-order-new.jpg')}}" class="img-thumbnail">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingSix">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                    How to update your Profile and Company information
                                </a>
                            </h4>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <h5>1. Fill input that provided if you want to update a new data for your profile, and then click save button.</h5>
                                        <img src="{{Asset('img/helpimage/profile.jpg')}}" class="img-thumbnail">
                                    </div>
                                    <div class="col-xs-6">
                                        <h5>2. Fill input that provided if you want to update a new data for your profile, and then click save button. </h5>
                                        <img src="{{Asset('img/helpimage/company.jpg')}}" class="img-thumbnail">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingSeven">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                    How to setting your Default COA
                                </a>
                            </h4>
                        </div>
                        <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <h5>1. Go to default account page in setting menu.</h5>
                                        <h5>Choose eight default COA that will be used to create a journal.</h5>
                                        <h5>Note: Default COA should be determined before making a transaction and journals.</h5>
                                        <img src="{{Asset('img/helpimage/settingCoa.jpg')}}" class="img-thumbnail">
                                    </div>
                                    <div class="col-xs-6">
                                        <h5>2. After you choose eight default COA click save button to save your setting. </h5>
                                        <img src="{{Asset('img/helpimage/settingCoa-save.jpg')}}" class="img-thumbnail">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingEight">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                    How to create a new User
                                </a>
                            </h4>
                        </div>
                        <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <h5>1. Go to master user page in setting menu and then click new button marked bellow.</h5>
                                        <img src="{{Asset('img/helpimage/user.jpg')}}" class="img-thumbnail">
                                    </div>
                                    <div class="col-xs-6">
                                        <h5>2. Fill the blank input and then save.</h5>
                                        <img src="{{Asset('img/helpimage/user-insert.jpg')}}" class="img-thumbnail">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!---- end div tabwrap---->



    </div><!---end primcontent--->
</div><!---end wrapjour--->

@stop

@section('js')

@stop
