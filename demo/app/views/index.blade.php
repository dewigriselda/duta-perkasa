<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>{{Config::get('companyHeader.header_company');}} master data</title>
        <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/kustom.css">
    </head>
    <body>
        <div class="top-nav">
            <h4>{{Config::get('companyHeader.header_company');}} master data</h4>
        </div>
        <div class="side-nav">
            <ul class="side-menu">
                <a href=".m_coa2" data-toggle="modal" role="dialog"><li><p><span class="glyphicon glyphicon-tasks"></span> m_coa</p></li></a>
            </ul>
        </div>
        <div class="top-nav2">
            <label for="search-box">search</label>
            <div class="searchwrap">
                <input type="search" name="search-box" id="search-box" autocomplete="on">
            </div>
            <div class="button">    
                <button type="button" class="btn btn-green" data-target=".m_coa2" data-toggle="modal" role="dialog"><span class="glyphicon glyphicon-plus"</span>Add</button>
            </div>
        </div>
    <div class="wrap">
        <div class="wraphead">
            <div class="wrap-cont head-cont no-marg-right no-bord">
                <div class="head border-right treelist col-sm-8 ">
                    <h4>Account Name</h4>       
                </div>

                <div class="head border-right level col-sm-2 ">
                    <h4>Account No</h4>
                </div>

                <div class="head border-right border-right account col-sm-1">
                    <h4>Level</h4>
                </div>

                <div class="head border-right  no-bord account col-sm-1">
                    <h4>Actions</h4>
                </div>

            </div>
        </div>

        <div class="wrap-cont list-data border-bot aktiva" >
            <div class="col-sm-12 no-padd list-height ">
                <div class="col-sm-8 border-right list-height ">
                    <p class="parent1">
                        <span class="glyphicon glyphicon-plus" id="aktiva"></span> Aktiva</p>
                </div>
                <div class="col-sm-2 border-right list-height">
                    <p class="parent1">1</p>
                </div>
                <div class="col-sm-1 border-right list-height">
                    <p class="parent1">1</p>
                </div>
                <div class="col-sm-1 list-height">
                    <p class="parent1"><span class="glyphicon glyphicon-edit"></span></p>
                </div>
            </div>
        </div>

        <div class="wrap-cont list-data-child border-bot aktiva-hide aktiva-lancar ">
            <div class="col-sm-12 no-padd list-height ">
                <div class="col-sm-8 border-right list-height ">
                    <p class="parent1 child-padd-left"><span class="glyphicon glyphicon-plus" id="aktiva-lancar"></span> Aktiva Lancar</p>
                </div>
                <div class="col-sm-2 border-right list-height">
                    <p class="parent1">11</p>
                </div>
                <div class="col-sm-1 border-right list-height">
                    <p class="parent1">2</p>
                </div>
                <div class="col-sm-1 list-height">
                    <p class="parent1"><span class="glyphicon glyphicon-edit"></span></p>
                </div>
            </div>
        </div>

        <div class="wrap-cont list-data-child2 border-bot aktiva-lancar-hide kas">

            <div class="col-sm-12 no-padd list-height ">

                <div class="col-sm-8 border-right list-height ">
                    <p class="parent1 child-padd-left2"><span class="glyphicon glyphicon-plus" id="kas"></span> Kas</p>
                </div>

                <div class="col-sm-2 border-right list-height">
                    <p class="parent1">1101</p>
                </div>

                <div class="col-sm-1 border-right list-height">
                    <p class="parent1">3</p>
                </div>

                <div class="col-sm-1 list-height">
                    <p class="parent1"><span class="glyphicon glyphicon-edit"></span></p>
                </div>


            </div>

        </div>

        <div class="wrap-cont list-data-child3 border-bot kas-hide">

            <div class="col-sm-12 no-padd list-height ">

                <div class="col-sm-8 border-right list-height ">
                    <p class="parent1 child-padd-left3">Kas Kecil</p>
                </div>

                <div class="col-sm-2 border-right list-height">
                    <p class="parent1">110101</p>
                </div>

                <div class="col-sm-1 border-right list-height">
                    <p class="parent1">4</p>
                </div>

                <div class="col-sm-1 list-height">
                    <p class="parent1"><span class="glyphicon glyphicon-edit"></span></p>
                </div>


            </div>

        </div>


        <div class="wrap-cont list-data-child2 border-bot  aktiva-lancar-hide bank">

            <div class="col-sm-12 no-padd list-height ">

                <div class="col-sm-8 border-right list-height ">
                    <p class="parent1 child-padd-left2"><span class="glyphicon glyphicon-plus" id="bank"></span> Bank</p>
                </div>

                <div class="col-sm-2 border-right list-height">
                    <p class="parent1">1102</p>
                </div>

                <div class="col-sm-1 border-right list-height">
                    <p class="parent1">3</p>
                </div>

                <div class="col-sm-1 list-height">
                    <p class="parent1"><span class="glyphicon glyphicon-edit"></span></p>
                </div>


            </div>

        </div>

        <div class="wrap-cont list-data-child3 border-bot bank-hide">

            <div class="col-sm-12 no-padd list-height ">

                <div class="col-sm-8 border-right list-height ">
                    <p class="parent1 child-padd-left3">Bank BCA</p>
                </div>

                <div class="col-sm-2 border-right list-height">
                    <p class="parent1">110201</p>
                </div>

                <div class="col-sm-1 border-right list-height">
                    <p class="parent1">4</p>
                </div>

                <div class="col-sm-1 list-height">
                    <p class="parent1"><span class="glyphicon glyphicon-edit"></span></p>
                </div>


            </div>

        </div>


        <div class="wrap-cont list-data-child border-bot aktiva-hide">

            <div class="col-sm-12 no-padd list-height ">

                <div class="col-sm-8 border-right list-height ">
                    <p class="parent1 child-padd-left"><span class="glyphicon glyphicon-plus"></span> Aktiva Tetap</p>
                </div>

                <div class="col-sm-2 border-right list-height">
                    <p class="parent1">12</p>
                </div>

                <div class="col-sm-1 border-right list-height">
                    <p class="parent1">2</p>
                </div>

                <div class="col-sm-1 list-height">
                    <p class="parent1"><span class="glyphicon glyphicon-edit"></span></p>
                </div>


            </div>

        </div>


    </div><!---- end div wrap---->





    <!--coa data modal  -->


    <div class="modal fade m_coa2" id="m_coa2" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">m_coa</h4>
                </div>
                <div class="modal-body">

                    <div class="coa-form">
                        <form action="" method="post" class="action">
                            <ul>

                                <li>
                                    <label for="level">choose a level</label>
                                </li>
                                <li>
                                    <select onchange="" name="level" id="selectLevel">
                                        <option selected value="choose">levels</option>
                                        <option value="level1">level 1</option>
                                        <option class="enable" value="level2">level 2</option>
                                        <option class="enable" value="level3">level 3</option>
                                        <option class="enable" value="level4">level 4</option>
                                    </select>
                                </li>          

                                <li>
                                    <label for="parentid">Parent id</label>
                                </li>
                                <li>
                                    <select id="parentCOA" name="parentid" disabled='disabled'>
                                        <option value="m_coa1">coa1</option>
                                        <option value="m_coa2">coa2</option>
                                        <option value="m_coa3">coa3</option>
                                        <option value="m_coa4">coa4</option>
                                    </select>
                                </li>
                                <li>
                                    <label for="ACC2ID">ID</label>
                                </li>
                                <li>
                                    <input type="text" name="ACC2ID" id="ID2" maxlength="200">
                                </li>
                                <li>
                                    <label for="ACC2Name">Name</label>
                                </li>
                                <li>
                                    <input type="text" name="ACC2Name" id="name2" maxlength="200">
                                </li>
                                <li>
                                    <label for="UserRecord">User record</label>
                                </li>
                                <li>
                                    <input type="text" name="UserRecord" id="UserRecord2" maxlength="200">
                                </li>
                                <li>
                                    <label for="remark">Remarks</label>
                                </li>
                                <li>
                                    <textarea name="remark" id="remark2" maxlength="1000"></textarea>
                                </li>
                            </ul>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-green">Submit</button>
                </div>
                </form>      
            </div>
        </div>  
    </div>




    <script src="lib/bootstrap/js/jquery-1.11.1.min.js"></script>
    <script src="lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="lib/bootstrap/js/jquery.easing.min.js"></script>
    <script src="js/slide.js"></script>

</body>
</html>
