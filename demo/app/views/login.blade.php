<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Login</title>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400' rel='stylesheet' type='text/css'>
        <link rel="shortcut icon" href="{{Asset('fav.ico')}}" type="image/x-icon" />
        <link rel="stylesheet" href="{{Asset('lib/bootstrap/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{Asset('css/kustom.css')}}">
        <script src="{{Asset('lib/bootstrap/js/jquery-1.11.1.min.js')}}"></script>
        <script src="{{Asset('lib/bootstrap/js/bootstrap.min.js')}}"></script>
        <style>
            body {
                padding-top: 40px;
                padding-bottom: 40px;
                background-color: #aedd3e;
            }
            .form-signin {
                max-width: 330px;
                padding: 15px;
                margin: 0 auto;
            }
            .loginnest{
                border-radius: 10px;
                background-color: #fff;
                box-shadow: 0px 1px 3px #dadadb;
                padding: 10px;
                display: block;
                border: 1px solid #a8bdc4 !important;
                margin-top: 3%;
                position: relative;
            }
            input{
                margin-bottom: 10px;
                outline: none;
            }
            .form-signin input[type="text"] {
                border: 1px solid #d7e3e4;
                border-radius: 5px;
                height: 35px;
                padding-left: 3px;
                outline: none !important;
                background-color: #fff;

            }
            .form-signin input[type="password"] {
                border: 1px solid #d7e3e4;
                border-radius: 5px;
                height: 35px;
                padding-left: 3px;
                outline: none !important;
                background-color: #fff;
            }
            .rem{
                font-weight: 500;
                color:#bbbbbb;
            }
            .btn-login{
                height: 40px;
                font-size: 16px !important;
            }
            #forgot{
                font-size: 12px;
                position: absolute;
                right: 55px;
                color: #bbbbbb;
                cursor: pointer;
            }
        </style>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-xs-12 col-xs-offset-0">

                    @if(Session::get('messages') == 'suksesAktifasi')
                    <div class="alert alert-success alert-dismissible" role="alert" >
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Success!</strong> Your account has been activated.
                    </div>
                    @endif
                    @if(Session::get('messages') == 'belumLogin')
                    <div class="alert alert-danger alert-dismissible" role="alert" >
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Access denied!</strong> You must login first.
                    </div>
                    @elseif(Session::get('messages') == 'suksesResetPassword')
                    <div class="alert alert-success alert-dismissible" role="alert" >
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Success!</strong> Please check your email for new password.
                    </div>
                    @endif
                    @if(isset($messages))
                    @if($messages == 'salahLogin')
                    <div class="alert alert-danger alert-dismissible" role="alert" >
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Failed!</strong> Data entry error occurred.
                        @if(isset($error))
                        <ul>
                            @foreach($error->all('<li>:message</li>') as $message)
                            {{$message}}
                            @endforeach
                        </ul>
                        @endif
                    </div>
                    @endif
                    @if($messages == 'gagalLogin')
                    <div class="alert alert-danger alert-dismissible" role="alert" >
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Failed!</strong> Email and Password doesn't exist.
                    </div>
                    @elseif($messages == 'gagalResetPassword')
                    <div class="alert alert-danger alert-dismissible" role="alert" >
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Failed!</strong> Email ID doesn't exist.
                    </div>
                    @elseif($messages == 'suksesResetPassword')
                    <div class="alert alert-success alert-dismissible" role="alert" >
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Success!</strong> Please check your email to reset password.
                    </div>
                    @endif
                    @endif

                    @if(Session::get('status') == 'company')
                    <div class="alert alert-danger alert-dismissible" role="alert" >
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Failed!</strong> Your Company ID is not active.
                    </div>
                    @endif

                    @if(Session::get('status') == 'user')
                    <div class="alert alert-danger alert-dismissible" role="alert" >
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Failed!</strong> Your User Account is not active.
                    </div>
                    @endif

                </div>
            </div>
        </div>


        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-xs-12">

                    <div class="loginnest">
                        <form action="" method="post" class="form-signin" id="loginForm">
                            <h4 class="form-signin-heading text-center">Login to {{Config::get('companyHeader.header_company');}} Accounting</h4>
                            <input type="hidden" name="jenis" value="userLogin">
                            <hr>
                            <label for="email">Email</label>
                            <input class="form-control" type="email" name="email" id="email" placeholder="Email" autofocus value="admin@gmail.com">
                            <label for="password">Password</label> 
                            <!--<span><a id="forgot">Forgot your password?</a></span>-->
                            <input class="form-control" type="password" name="password" id="password" maxlength="16" placeholder="Password" value="admin123">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember" value="remember"> <span class="rem">Remember me</span>
                                </label>
                                <label>
                                    <a style="font-weight: 500" id="btnForgot"> Forgot your password?</a>
                                </label>
                            </div>
                            <hr>
                            <button type="submit" class="btn btn-green btn-block btn-login">Login</button>
                        </form>  
                        <form action="" method="post" class="form-signin" id="forgotForm" style="display:none">
                            <h4 class="form-signin-heading text-center">Forgot Password</h4>
                            <input type="hidden" name="jenis" value="userForgot">
                            <hr>
                            <label for="email">Email</label>
                            <input class="form-control" type="text" name="email" id="email" placeholder="Email" autofocus>
                            <div class="checkbox">
                                <label>
                                    <a style="font-weight: 500" id="btnBack"> Back to Login.</a>
                                </label>
                            </div>
                            <hr>
                            <button type="submit" class="btn btn-green btn-block btn-login">Submit</button>
                        </form>  
                    </div>

                </div>
            </div>
        </div>

        <script>
$("#btnBack").click(function () {
    $('#forgotForm').fadeOut('slow', function () {
        $('#loginForm').fadeIn('slow');
    });
});

$("#btnForgot").click(function () {
    $('#loginForm').fadeOut('slow', function () {
        $('#forgotForm').fadeIn('slow');
    });
});
        </script>    
    </body>
</html>