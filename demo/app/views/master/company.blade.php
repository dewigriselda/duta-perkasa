@extends('template.header-footer')

@section('title')
Company
@stop

@section('css')
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
@stop

@section('nav')

@stop

@section('content')
@if(isset($messages))
@if($messages == 'gagalInsert' || $messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New company has been inserted.
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Company has been updated.
</div>
@endif
@if($messages == 'suksesSendInvoice')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Invoice to company has been updated.
</div>
@endif
@if($messages == 'suksesDelete')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Company has been deleted.
</div>
@endif
@if($messages == 'gagalDelete')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Company has been registered in other tables.
</div>
@endif
@if($messages == 'gagalUpload')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> There is something wrong with upload file, Please check picture type and size.
</div>
@endif
@endif

<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread" role="group">
                <a href="{{route('showUser')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showCompany')}}" type="button" class="btn btn-sm btn-pure">Company</a>
            </div>
            @if(checkModul('O01') || Auth::user()->CompanyInternalID == -1)
            <a target="_blank" href="{{Route('exportCompany')}}">
                <button type="button" class="btn btn-sm btn-green btn-export" id="btn-export">Export Excel</button>    
            </a>
            @endif
            <button type="button" class="btn btn-green btn-insert" data-target="#m_company" data-toggle="modal" role="dialog">
                <span class="glyphicon glyphicon-plus"></span> New</button>
        </div>
        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Company</h4>
            </div>            
            <div class="tableadd">
                <table id="example" class="display table-rwd table-company" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Company ID</th>
                            <th>Name</th>
                            <th width="10%">Address</th>
                            <th>Phone</th>
                            <th>Package</th>
                            <th>Payment</th>
                            <th>Using Memory</th>
                            <th>Expired Date</th>
                            <th>Status</th>
                            <th width='23%'>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $f = rand(0, 50);
                        foreach (Company::where('InternalID', '>', 0)->get() as $data) {
                            $data->dtRecordformat = date("d-m-Y H:i:s", strtotime($data->dtRecord));
                            $data->dtModifformat = date("d-m-Y H:i:s", strtotime($data->dtModified));
                            $data->Addressformat = str_replace("\r\n", " ", $data->Address);
                            $data->Remark = str_replace("\r\n", " ", $data->Remark);
                            $arrData = array($data);
                            $tamp = myEscapeStringData($arrData);
                            $tamp = myEncryptJavaScriptText($tamp, $f);
                            ?>
                            <tr>
                                <td>{{$data->CompanyID}}</td>
                                <td>{{$data->CompanyName}}</td>
                                <td>{{$data->Address}}</td>
                                <td>{{$data->Phone}}</td>
                                @if($data->PackageInternalID != NULL)
                                <td>{{$data->Package->PackageName}}</td>
                                @else
                                <td>Super admin</td>
                                @endif
                                <td>
                                    @if($data->StatusPayment == 0)
                                    Not Complete
                                    @elseif($data->StatusPayment == 1)
                                    Complete
                                    @endif
                                </td>
                                <td class='right'>{{countMemorySuperAdmin($data->InternalID)}}</td>
                                <td>
                                    @if($data->ExpiredDate == NULL)
                                    New register
                                    @else
                                    {{ date("d-m-Y", strtotime($data->ExpiredDate))}}
                                    @endif
                                </td>
                                <td>
                                    @if($data->Status == 0)
                                    Not Active
                                    @elseif($data->Status == 1)
                                    Active
                                    @endif
                                </td>
                                <td class="text-center">
                                    <?php
                                    if ($data->Logo == NULL) {
                                        $logo = '-1';
                                    } else {
                                        $logo = Asset($data->Logo);
                                    }
                                    $lastInvoice = Invoice::where('CompanyInternalID', $data->InternalID)->orderBy('InternalID', 'desc')->first();
                                    if ($lastInvoice != '') {
                                        $lastInvoice = $lastInvoice->InvoiceID . ' | ' . date('d M Y', strtotime($lastInvoice->InvoiceDate));
                                    }
                                    ?>
                                    <button id="btn-{{$data->CompanyID}}" data-target="#m_companyUpdate" data-all='{{$tamp}}'
                                            data-toggle="modal" role="dialog" data-logo="{{$logo}}"
                                            class="btn btn-pure-xs btn-xs btn-edit">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </button>
                                    <button data-target="#m_companyActive" data-internal="{{$data->InternalID}}"  data-toggle="modal" role="dialog"
                                            data-id="{{$data->UserID}}" class="btn btn-pure-xs btn-xs btn-active" @if($data->Status == 1) {{"disabled";}} @endif>
                                            <b> Active </b>
                                    </button>
                                    <button data-target="#m_companyNonActive" data-internal="{{$data->InternalID}}"  data-toggle="modal" role="dialog"
                                            data-id="{{$data->UserID}}" class="btn btn-pure-xs btn-xs btn-non-active" @if($data->Status == 0) {{"disabled";}} @endif>
                                            <b> Un Active </b>
                                    </button>
                                    <button <?php if (Auth::user()->Company->StatusPayment == 1 && date("Y-m-d", strtotime("+14 day", strtotime(date('Y-m-d')))) < date("Y-m-d", strtotime($data->ExpiredDate))) echo 'disabled' ?> 
                                        data-target="#m_companyPayment" data-internal="{{$data->InternalID}}"  data-toggle="modal" role="dialog"
                                        data-id="{{$data->UserID}}" class="btn btn-pure-xs btn-xs btn-payment">
                                        <b> Pay </b>
                                    </button>
                                    <button <?php if (Auth::user()->Company->StatusPayment == 1 && date("Y-m-d", strtotime("+14 day", strtotime(date('Y-m-d')))) < date("Y-m-d", strtotime($data->ExpiredDate))) echo 'disabled' ?>
                                        data-target="#m_companyInvoice" data-internal="{{$data->InternalID}}"  data-toggle="modal" role="dialog"
                                        data-id="{{$data->UserID}}" data-lastinvoice="{{$lastInvoice}}"  class="btn btn-pure-xs btn-xs btn-invoice">
                                        <b> Invoice </b>
                                    </button>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div><!---- end div tabwrap---->
        </div><!---- end div tabwrap---->
    </div><!--end primcontent-->
</div><!--end wrapjour--->


@stop

@section('modal')
<div class="modal fade bs-example-modal-lg" id="m_company" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="" method="post" class="action" id="form-insert" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Insert company</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form coawd">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="margbot10">
                                    <input type='hidden' name='jenis' value='insertCompany'>
                                </div>
                                <div class="margbot10">
                                    <label for="CompanyID">ID *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme noSpecialCharacter" type="text" name="CompanyID" id="companyID" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="CompanyName">Name *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" type="text" name="CompanyName" id="name" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="address">Address</label>
                                </div>
                                <div class="margbot10">
                                    <textarea class="area-theme" style="resize:none;" name="Address" maxlength="1000"></textarea>
                                </div>
                                <div class="margbot10">
                                    <label for="city">City</label>
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" type="text" name="City" maxlength="1000">
                                </div>
                                <div class="margbot10">
                                    <label for="region">Region</label>
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select input-theme" id="region" style="" name="Region">
                                        @foreach(Region::all() as $region)
                                        <option value="{{$region->InternalID}}">
                                            {{$region->RegionName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="city">City</label>
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" type="text" name="City" maxlength="1000">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="margbot10">
                                    <label for="Email">Email *</label> 
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" type="text" name="Email" id="email" maxlength="200" data-validation="email">
                                </div>
                                <div class="margbot10">
                                    <label for="taxID">TaxID</label>
                                </div>
                                <div class="margbot10">
                                    <div id="npwpin">
                                        <input type="hidden" name="taxID" value="" id="taxID">
                                        <input class="persen11 autoTab numaja" type="text" name="taxID1" id="taxID1" maxlength="2"
                                               data-validation="length" data-validation-length="min2" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID2" id="taxID2" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID3" id="taxID3" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen8 autoTab numaja" type="text" name="taxID4" id="taxID4" maxlength="1"
                                               data-validation="length" data-validation-length="min1" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> - 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID5" id="taxID5" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID6" id="taxID6" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information.">
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="phone">Phone</label>
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" type="text" name="Phone" maxlength="200">
                                </div>
                                <div class="margbot10">
                                    <label for="fax">Fax</label>
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" type="text" name="Fax" maxlength="200">
                                </div>
                                <div class="margbot10">
                                    <label for="logo">Logo</label>
                                </div>
                                <div class="margbot10">
                                    <input type="file" name="Logo">
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea class="area-theme" style="resize:none;"  name="remark" id="" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>


<div class="modal fade bs-example-modal-lg" id="m_companyUpdate" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="" method="post" class="action" id="form-update" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update company</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form coawd">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="margbot10">
                                    <input type="hidden" value="" id="idUpdate" name="InternalID">
                                    <input type="hidden" value="updateCompany" id="jenisUpdate" name="jenis">
                                </div>
                                <div class="margbot10">
                                    <label for="CompanyName">Name *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" type="text" name="CompanyName" id="nameUpdate" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="address">Address</label>
                                </div>
                                <div class="margbot10">
                                    <textarea class="area-theme" style="resize:none;"  name="Address" id="addressUpdate" maxlength="1000"></textarea>
                                </div>
                                <div class="margbot10">
                                    <label for="city">City</label>
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" type="text" id="cityUpdate" name="City" maxlength="1000">
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select area-theme" id="regionUpdate" style="" name="Region">
                                        @foreach(Region::all() as $region)
                                        <option id="reg{{$region->InternalID}}" value="{{$region->InternalID}}">
                                            {{$region->RegionName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="Email">Email *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" type="text" name="Email" id="emailUpdate" maxlength="200" data-validation="email">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="margbot10">
                                    <label for="taxID">TaxID</label>
                                </div>
                                <div class="margbot10">
                                    <div id="npwpin">
                                        <input type="hidden" name="taxID" value="" id="taxIDupdate">
                                        <input class="persen11 autoTab numaja" type="text" name="taxID1" id="taxID1update" maxlength="2"
                                               data-validation="length" data-validation-length="min2" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID2" id="taxID2update" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID3" id="taxID3update" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen8 autoTab numaja" type="text" name="taxID4" id="taxID4update" maxlength="1"
                                               data-validation="length" data-validation-length="min1" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> - 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID5" id="taxID5update" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                        <input class="persen15 autoTab numaja" type="text" name="taxID6" id="taxID6update" maxlength="3"
                                               data-validation="length" data-validation-length="min3" 
                                               data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information.">
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="phone">Phone</label>
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" type="text" name="Phone" id="phoneUpdate" maxlength="200">
                                </div>
                                <div class="margbot10">
                                    <label for="fax">Fax</label>
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" type="text" name="Fax" id="faxUpdate" maxlength="200">
                                </div>
                                <div class="margbot10">
                                    <label for="logo">Logo</label>
                                </div>
                                <div class="margbot10">
                                    <input type="file" name="Logo"> 
                                </div>
                                <div class="margbot10">
                                    <span style="padding:3px;">Last Logo : </span>
                                    <img style="height:200px; max-width: 300px;" id="logoUpdate" src="">
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea class="area-theme" style="resize:none;"  name="remark" id="remarkUpdate" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <small>Created by <span id="createdDetail"></span></small><br>
                                    <small>Modified by <span id="modifiedDetail"></span></small>
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Update</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>  
</div>

<div class="modal fade" id="m_companyActive" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Active company</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="" id="idActive" name="InternalID">
                            <input type="hidden" value="activeCompany" id="jenisDelete" name="jenis">
                            <p>Are you sure?</p>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Yes</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade" id="m_companyNonActive" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Non Active company</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="" id="idNonActive" name="InternalID">
                            <input type="hidden" value="nonActiveCompany" id="jenisDelete" name="jenis">
                            <p>Are you sure?</p>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Yes</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade" id="m_companyPayment" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Finish Payment Company</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="" id="idPayment" name="InternalID">
                            <input type="hidden" value="paymentCompany" id="jenisPayment" name="jenis">
                            <p>Are you sure?</p>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Yes</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade" id="m_companyInvoice" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Send Invoice Company</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="" id="idInvoice" name="InternalID">
                            <input type="hidden" value="invoiceCompany" id="jenisInvoice" name="jenis">
                            <p>Last invoice : </p>
                            <p><b><span id="lastInvoice"></span></b></p>
                            <p>Are you sure?</p>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Yes</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
                </div>
            </form>      
        </div>
    </div>  
</div>
@stop
<?php
$c = myEncryptJavaScript(Company::select('CompanyID as accID')->get(), $f);
$d = myEncryptJavaScript(Company::select('Email')->get(), $f);
?>
@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript">
var a = '<?php echo $c; ?>';
var c = '<?php echo $d; ?>';
var b = <?php echo $f; ?>;
</script>
<script> var imageKosong = "<?php echo Asset('/img/briefsmall.png') ?>"</script>
<script type="text/javascript" src="{{Asset('js/entry-js-master/company.js')}}"></script>
@stop