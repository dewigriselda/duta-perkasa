@extends('template.header-footer')

@section('title')
Inventory
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
@stop

@section('nav')

@stop

@section('content')
@if(myCheckIsEmpty('InventoryType'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one Inventory Type to insert Inventory.
</div>
@endif
@if(isset($messages))
@if($messages == 'gagalInsert' || $messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New inventory has been inserted.
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Inventory has been updated.
</div>
@endif
@if($messages == 'suksesDelete')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Inventory has been deleted.
</div>
@endif
@if($messages == 'gagalDelete')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Inventory has been registered in other tables.
</div>
@endif
@if($messages == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">

        <div class="btnnest option-slip">
            <div class="btn-group bread hidden-xs" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showInventory')}}" type="button" class="btn btn-sm btn-pure">Inventory</a>
            </div>
            @if(checkModul('O01'))
            <a target="_blank" href="{{Route('exportInventory')}}">
                <button type="button" class="btn btn-sm btn-green btn-export" id="btn-export">Export Excel</button>  
            </a>
            @endif
            <button type="button" <?php if (myCheckIsEmpty('InventoryType')) echo 'disabled' ?>  class="btn btn-green btn-insert" data-target="#m_inventory" data-toggle="modal" role="dialog">
                <span class="glyphicon glyphicon-plus"></span> New</button>
            <!--            @if(checkModul('O04'))
                        <button type="button" class="btn btn-green" data-target="#r_detail" data-toggle="modal" role="dialog">
                            <span class="glyphicon glyphicon-file"></span> Detail Report</button>
                        <button type="button" class="btn btn-green" data-target="#r_summary" data-toggle="modal" role="dialog">
                            <span class="glyphicon glyphicon-file"></span> Summary Report</button>
                        @endif-->
            @if(checkModul('O04'))
            <div class="btn-group">
                <button type="button" class="btn btn-green dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="glyphicon glyphicon-file"></span> Report <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a data-target="#r_detail" data-toggle="modal" role="dialog" href="#">Detail Report</a></li>
                    <li><a data-target="#r_summary" data-toggle="modal" role="dialog" href="#">Summary Report</a></li>
                    <li><a data-target="#r_bufferStock" data-toggle="modal" role="dialog" href="#">Buffer Stock Report</a></li>  
                </ul>
            </div>
            @endif
        </div>

        <div class="bread-xs visible-xs hidden-sm hidden-md hidden-lg">
            <p class="text-center"><a href="{{route('showDashboard')}}">{{Config::get('companyHeader.header_company');}}</a> / <a>{{ucfirst($toogle)}}</a> / <a href="{{route('showInventory')}}">Inventory</a></p>
        </div>

        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Inventory</h4>
            </div>

            <div class="tableadd">
                <table id="example" class="display table-rwd table-inventory" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Inventory ID</th>
                            <th>Type</th>
                            <th>Name</th>
                            <th>UoM</th>
                            <th>Initial Value</th>
                            <th>Initial Quantity</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                </table>
            </div><!---end tableadd-->
        </div><!---- end div tabwrap---->
    </div><!---end primcontent-->
</div><!---end wrapjour-->

@stop

@section('modal')
<div class="modal fade" id="r_detail" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action" id=""  target="_blank">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detail Report</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="margbot10">
                                    <input type='hidden' name='jenis' value='detailInventory'>
                                </div>
                                <div class="margbot10">
                                    <label for="warehouse">Warehouse *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal" id="warehouse" style="" name="warehouse">
                                        <option value="-1"> All </option>
                                        @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $warehouse)
                                        <option value="{{$warehouse->InternalID}}">
                                            {{$warehouse->WarehouseName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="sDate">Start Date *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="sDate" id="startDate" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="eDate">End Date *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="eDate" id="endDate" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                    <button type="submit" id="btn-detail-report" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade" id="r_summary" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action" id="" target="_blank">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Summary Report</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="margbot10">
                                    <input type='hidden' name='jenis' value='summaryInventory'>
                                </div>
                                <div class="margbot10">
                                    <label for="warehouse">Warehouse *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal" id="warehouse" style="" name="warehouse">
                                        <option value="-1"> All </option>
                                        @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $warehouse)
                                        <option value="{{$warehouse->InternalID}}">
                                            {{$warehouse->WarehouseName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="date">Date *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="date" id="date" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                    <button type="submit" id="btn-summary-report" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade" id="r_stock" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action" id="" target="_blank">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Stock Report</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <div class="row">
                            <div class="col-xs-12">
                                <input type="hidden" value="" id="idStock" name="InternalID">
                                <div class="margbot10">
                                    <input type='hidden' name='jenis' value='stockInventory'>
                                </div>
                                <div class="margbot10">
                                    <label for="warehouse">Warehouse *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal" id="warehouse" style="" name="warehouse">
                                        <option value="-1"> All </option>
                                        @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $warehouse)
                                        <option value="{{$warehouse->InternalID}}">
                                            {{$warehouse->WarehouseName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="sDate">Start Date *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="sDate" id="startDateStock" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="eDate">End Date *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="eDate" id="endDateStock" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                    <button type="submit" id="btn-stock-report" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>
<div class="modal fade" id="r_bufferStock" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action" id="" target="_blank">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Report Buffer Stock</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="margbot10">
                                    <input type='hidden' name='jenis' value='reportStock'>
                                </div>
                                <div class="margbot10">
                                    <label for="warehouse">Warehouse *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal" id="warehouse" style="" name="warehouse">
                                        <option value="-1"> All </option>
                                        @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $warehouse)
                                        <option value="{{$warehouse->InternalID}}">
                                            {{$warehouse->WarehouseName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="date">Date *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="date" id="date1" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                    <button type="submit" id="btn-buffer-stock-report" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade" id="m_inventory" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action" id="form-insert">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Insert Inventory</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="margbot10">
                                    <input type='hidden' name='jenis' value='insertInventory'>
                                </div>
                                <div class="margbot10">
                                    <label for="InventoryID">Inventory ID *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="noSpecialCharacter" type="text" name="InventoryID" id="inventoryID" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="InventoryType">Type *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal" id="type" style="" name="Type">
                                        @foreach(InventoryType::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('InternalID')->get() as $InventoryType)
                                        <option value="{{$InventoryType->InternalID}}">
                                            {{$InventoryType->InventoryTypeName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="InventoryName">Name *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="noSpecialCharacter" type="text" name="InventoryName" id="name" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="UoM">UoM *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="UoM" id="uom" maxlength="100" data-validation="required">
                                </div>
                                <!--                                <div class="margbot10">
                                                                    <label for="initialValue">Initial Value *</label>
                                                                </div>
                                                                <div class="margbot10">
                                                                    <input type="text" class="numajaDesimal" name="InitialValue" id="initialValue" data-validation="required">
                                                                </div>
                                                                <div class="margbot10">
                                                                    <label for="initialQuantity">Initial Quantity *</label>
                                                                </div>
                                                                <div class="margbot10">
                                                                    <input type="text" class="numajaDesimal" name="InitialQuantity" id="initialQuantity" data-validation="required">
                                                                </div>-->
                                <div class="margbot10">
                                    <label for="bufferStock">Buffer Stock</label> *
                                </div>
                                <div class="margbot10">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <span for="bufferStock">Max</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" class="numaja price" name="MaxStock" id="max" data-validation="required">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <span for="bufferStock">Min</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" class="numaja price" name="MinStock" id="min" data-validation="required">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;"  name="remark" id="remark" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>


<div class="modal fade" id="m_inventoryUpdate" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action" id="form-update">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Inventory</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="margbot10">
                                    <input type="hidden" value="" id="idUpdate" name="InternalID">
                                    <input type="hidden" value="updateInventory" id="jenisUpdate" name="jenis">
                                </div>
                                <div class="margbot10">
                                    <label for="InventoryType">Type *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="chosen-select choosen-modal" id="typeUpdate" style="" name="Type">
                                        @foreach(InventoryType::where('CompanyInternalID', Auth::user()->Company->InternalID)->orderBy('InternalID')->get() as $InventoryType)
                                        <option id="type{{$InventoryType->InternalID}}" value="{{$InventoryType->InternalID}}">
                                            {{$InventoryType->InventoryTypeName}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="InventoryName">Name *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="noSpecialCharacter" type="text" name="InventoryName" id="nameUpdate" maxlength="200" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="UoM">UoM *</label>
                                </div>
                                <div class="margbot10">
                                    <input type="text" name="UoM" id="uomUpdate" maxlength="100" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="bufferStock">Buffer Stock</label> *
                                </div>
                                <div class="margbot10">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <span for="bufferStock">Max</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" class="numaja price" name="MaxStock" id="maxStockUpdate" data-validation="required">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <span for="bufferStock">Min</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" class="numaja price" name="MinStock" id="minStockUpdate" data-validation="required">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea style="resize:none;" name="remark" id="remarkUpdate" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <small>Created by <span id="createdDetail"></span></small><br>
                                    <small>Modified by <span id="modifiedDetail"></span></small>
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Update</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade" id="m_inventoryDelete" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Delete Inventory</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action">
                        <ul>
                            <input type="hidden" value="" id="idDelete" name="InternalID">
                            <input type="hidden" value="deleteInventory" id="jenisDelete" name="jenis">
                            <p>Are you sure want to delete <span id="deleteName"></span>?</p>
                        </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-green">Yes</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
            </div>
            </form>      
        </div>
    </div>  
</div>
@stop
<?php
?>
@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/chosenNoHide.jquery.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script>
    var checkInventoryID = "<?php echo Route("checkInventoryID") ?>";
    var inventoryDataBackup = '<?php echo Route('inventoryDataBackup') ?>';
</script>
<script>
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-master/inventory.js')}}"></script>
@stop
