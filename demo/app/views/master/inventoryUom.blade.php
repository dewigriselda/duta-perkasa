@extends('template.header-footer')

@section('title')
Inventory Uom
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
@stop

@section('nav')

@stop

@section('content')
@if(isset($messages))
@if($messages == 'gagalInsert' || $messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New inventory uom has been inserted.
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Inventory uom has been updated.
</div>
@endif
@if($messages == 'suksesDelete')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Inventory uom has been deleted.
</div>
@endif
@if($messages == 'gagalDelete')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> inventory uom has been registered in other tables.
</div>
@endif
@if($messages == 'gagalDefault')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Inventory uom is uom default, can't be deleted or changed. Insert new default uom first.
</div>
@endif
@if($messages == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@endif

<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest">
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showInventoryUom')}}" type="button" class="btn btn-sm btn-pure">Inventory Uom</a>
            </div>
            @if(checkModul('O01'))
            <a target="_blank" href="{{Route('exportInventoryUom')}}">
                <button type="button" class="btn btn-sm btn-green btn-export" id="btn-export">Export Excel</button>    
            </a>
            @endif
            <button type="button" class="btn btn-green btn-insert" data-target="#m_inventoryUom" data-toggle="modal" role="dialog">
                <span class="glyphicon glyphicon-plus"></span> New</button>
        </div>
        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Inventory Uom</h4>
            </div>

            <div class="tableadd">
                <table id="example" class="display table-rwd table-inventory-uom" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Inventory</th>
                            <th>Uom</th>
                            <th>Default</th>
                            <th>Value</th>
                            <th>Record</th>
                            <th>Modified</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $f = rand(0, 50);
                        foreach (InventoryUom::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $data) {
                            $data->dtRecordformat = date("d-m-Y H:i:s", strtotime($data->dtRecord));
                            $data->dtModifformat = date("d-m-Y H:i:s", strtotime($data->dtModified));
                            $arrData = array($data);
                            $tamp = myEscapeStringData($arrData);
                            $tamp = myEncryptJavaScriptText($tamp, $f);
                            ?>
                            <tr>
                                <td>{{$data->inventory->InventoryName}}</td>
                                <td>{{$data->uom->UomID}}</td>
                                @if($data->Default == "0")
                                <td>-</td>
                                @else
                                <td>{{'Default'}}</td>
                                @endif
                                <td class='right'>{{($data->Value)}}</td>
                                <td>{{$data->UserRecord." ".date( "d-m-Y H:i:s", strtotime($data->dtRecord))}}</td>
                                @if($data->UserModified == "0")
                                <td>-</td>
                                @else
                                <td>{{$data->UserModified." ".date( "d-m-Y H:i:s", strtotime($data->dtModified))}}</td>
                                @endif
                                <td class="text-center">
                                    <button id="btn-{{$data->InternalID}}" data-target="#m_inventoryUomUpdate" data-all='{{$tamp}}'
                                            data-toggle="modal" role="dialog"
                                            class="btn btn-pure-xs btn-xs btn-edit">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </button>
                                    <button data-target="#m_inventoryUomDelete" data-internal="{{$data->InternalID}}"  data-toggle="modal" role="dialog"
                                            data-id="{{$data->InternalID}}" data-name='{{$data->inventory->InventoryName.' - '.$data->uom->UomID}}' class="btn btn-pure-xs btn-xs btn-delete">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>

            </div><!---end tableadd-->
        </div><!---- end div tabwrap---->
    </div><!---end primcontent-->
</div><!---end wrapjour-->


@stop

@section('modal')
<div class="modal fade bs-example-modal-lg" id="m_inventoryUom" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action" id="form-insert">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Insert Inventory Uom</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="margbot10">
                                    <input type='hidden' name='jenis' value='insertInventoryUom'>
                                </div>
                                <div class="margbot10">
                                    <label for="Default">Default *</label>
                                </div>
                                <div class="margbot10">
                                    <div class="radio-inline">
                                        <input type="radio" class="radio-tipe" id="tipeNoDefault" name="Default" value="0" checked="checked">Not default
                                    </div>
                                    <div class="radio-inline">
                                        <input type="radio" class="radio-tipe" id="tipeDefault" name="Default" value="1">Default
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="Inventory">Inventory *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="area-theme" name="InventoryInternalID">
                                        @foreach(Inventory::where("CompanyInternalID", Auth::user()->Company->InternalID)->get() as $data);
                                        <option value="{{$data->InternalID}}">{{$data->InventoryName}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="Uom">Uom *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="input-theme" name="UomInternalID">
                                        @foreach(Uom::where("CompanyInternalID", Auth::user()->Company->InternalID)->get() as $data);
                                        <option value="{{$data->InternalID}}">{{$data->UomID}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="Value">Value *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" type="text" class="numajaDesimal" id="value" name="Value" maxlength="" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea class="area-theme" style="resize:none;"  name="remark" id="remark" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green" id="btn-submit">Submit</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>


<div class="modal fade" id="m_inventoryUomUpdate" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action" id="form-update">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Inventory Uom</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="margbot10">
                                    <input type="hidden" value="" id="idUpdate" name="InternalID">
                                    <input type="hidden" value="updateInventoryUom" id="jenisUpdate" name="jenis">
                                </div>
                                <div class="margbot10">
                                    <label for="Default">Default *</label>
                                </div>
                                <div class="margbot10">
                                    <div class="radio-inline">
                                        <input type="radio" class="radio-tipe" id="tipeNoDefaultUpdate" name="Default" value="0" checked="checked">Not default
                                    </div>
                                    <div class="radio-inline">
                                        <input type="radio" class="radio-tipe" id="tipeDefaultUpdate" name="Default" value="1">Default
                                    </div>
                                </div>
                                <div class="margbot10">
                                    <label for="Inventory">Inventory *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="input-theme" name="InventoryInternalID" id="inventoryInternalIDUpdate">
                                        @foreach(Inventory::where("CompanyInternalID", Auth::user()->Company->InternalID)->get() as $data);
                                        <option value="{{$data->InternalID}}">{{$data->InventoryName}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="Uom">Uom *</label>
                                </div>
                                <div class="margbot10">
                                    <select class="input-theme" name="UomInternalID" id="uomInternalIDUpdate">
                                        @foreach(Uom::where("CompanyInternalID", Auth::user()->Company->InternalID)->get() as $data);
                                        <option value="{{$data->InternalID}}">{{$data->UomID}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="margbot10">
                                    <label for="Value">Value *</label>
                                </div>
                                <div class="margbot10">
                                    <input class="input-theme" type="text" class="numajaDesimal" id="valueUpdate" name="Value" maxlength="" data-validation="required">
                                </div>
                                <div class="margbot10">
                                    <label for="remark">Remarks *</label>
                                </div>
                                <div class="margbot10">
                                    <textarea class="area-theme" style="resize:none;" name="remark" id="remarkUpdate" maxlength="1000" data-validation="required"></textarea>
                                </div>
                                <div class="margbot10">
                                    <small>Created by <span id="createdDetail"></span></small><br>
                                    <small>Modified by <span id="modifiedDetail"></span></small>
                                </div>
                                <div class="margbot10">
                                    <div class="required">
                                        * Required
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green" id="btn-update">Update</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
                </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade" id="m_inventoryUomDelete" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" class="action">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Delete Inventory Uom</h4>
                </div>
                <div class="modal-body">
                    <div class="coa-form">
                        <ul>
                            <input type="hidden" value="" id="idDelete" name="InternalID">
                            <input type="hidden" value="deleteInventoryUom" id="jenisDelete" name="jenis">
                            <p>Are you sure want to delete <span id="deleteName"></span>?</p>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green">Yes</button>
                    <button type="button" class="btn btn-pure" data-dismiss="modal">Cancel</button>
                </div>
            </form>      
        </div>
    </div>  
</div>
@stop
<?php
$s = myEncryptJavaScript(InventoryUom::select('InternalID')->where('CompanyInternalID', Auth::user()->Company->InternalID)->get(), $f);
?>
@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script type="text/javascript">
var a = '<?php echo $s; ?>';
var b = <?php echo $f; ?>;
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-master/inventoryUom.js')}}"></script>
@stop
