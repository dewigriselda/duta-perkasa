@extends('template.header-footer')

@section('title')
User Matrix
@stop

@section('css')
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<style>
    .help-block {
        margin-left: 22.5% !important;
    }
    input[type="checkbox"]{
        height: 15px;
        width: 15px;
    }
    hr{
        margin-bottom: 10px !important;
        margin-top: 5px !important;
    }
</style>
@stop

@section('nav')

@stop

@section('content')
@if(Session::get('messages') == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@if(isset($messages))
@if($messages == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> User matrix has been updated.
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontentnopadd">
        <div class="primcontent"> 
            <div class="btnnest hidden-xs"> 
                <div class="btn-group bread margr30min" role="group">
                    @if(Auth::User()->CompanyInternalID == -1)
                    <a href="{{route('showUser')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                    @else
                    <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                    @endif
                    <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                    <a href="{{route('showUser')}}" type="button" class="btn btn-sm btn-pure bread-arrow">User</a>
                    <a href="{{route('showUserMatrix', $user->UserID)}}" type="button" class="btn btn-sm btn-pure">User Matrix {{ucfirst($user->UserName)}}</a>
                </div>
            </div>
            
            <div class="bread-xs visible-xs hidden-sm hidden-md hidden-lg">
                <p class="text-center">
                    @if(Auth::User()->CompanyInternalID == -1)
                    <a href="{{route('showUser')}}" >{{Config::get('companyHeader.header_company');}}</a> /
                    @else
                    <a href="{{route('showDashboard')}}" >{{Config::get('companyHeader.header_company');}}</a> /
                    @endif
                    <a >{{ucfirst($toogle)}}</a> /
                    <a href="{{route('showUser')}}" >User</a> /
                    <a href="{{route('showUserMatrix', $user->UserID)}}" >User Matrix {{ucfirst($user->UserName)}}</a>
                </p>
            </div>
            
        </div>
        <div class="row">
            <div class="col-md-12">
                <form action="" method="post" class="actiondefault" id="form-insert" enctype="multipart/form-data">
                    <div class="tabwrap">
                        <div class="tabhead">
                            <h4 class="headtitle">User Matrix {{ucfirst($user->UserName)}}</h4>
                        </div>
                        <div class="tableadd"> 
                            <div class="col-md-12">
                                <div class="row">
                                    <label class="col-sm-2 control-label" for="checkAll"> Check All </label>
                                    <input id="checkAll" class="form-control pull-left" type="checkbox" >
                                </div>
                                <hr>
                                <div class="row">
                                    <input type='hidden' name='jenis' value='insertUserMatrix'>
                                    {{''; $tipe = "";}}
                                    {{'';$notIn = explode(',',Auth::user()->Company->Package->MatrixNotIn);
                                        $dataMatrixLoop = Matrix::whereNotIn('MatrixID', $notIn)->orderBy(DB::raw('RIGHT(Type,7)'),'asc')->get();}}
                                    @foreach($dataMatrixLoop as $data)
                                    @if($tipe == '')
                                    {{''; $tipe = $data->Type;}}
                                    <div class="col-md-3" >
                                        <div class="panel panel-default">
                                            <div class="panel-heading">{{ucfirst($data->Type)}} <input class="pull-right matrix" type="checkbox" id="check{{ucfirst($data->Type)}}"></div>
                                            <div class="panel-body">
                                                @endif

                                                @if($tipe != $data->Type)
                                            </div>
                                        </div>
                                    </div>
                                    {{''; $tipe = $data->Type;}}
                                    <div class="col-md-3">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">{{ucfirst($data->Type)}} <input class="pull-right matrix" type="checkbox" id="check{{ucfirst($data->Type)}}"></div>
                                            <div class="panel-body">
                                                @endif
                                                @if($data->Type == 'accounting')
                                                @if($data->MatrixID == "AM01")
                                                <label class="col-sm-9 control-label" style="padding-right: 0px !important;" for="{{'';$data->MatrixID}}"> {{$data->MatrixName}} </label>
                                                <div class="col-sm-3"><input class="form-control accounting matrix" type="checkbox" {{UserDetail::checkDetail($data->InternalID, $user->InternalID)}} name="matrix[]" value="{{$data->InternalID}}"></div>
                                                @else
                                                <label class="col-sm-9 control-label" for="{{'';$data->MatrixID}}"> {{$data->MatrixName}} </label>
                                                <div class="col-sm-3"><input class="form-control accounting matrix" type="checkbox" {{UserDetail::checkDetail($data->InternalID, $user->InternalID)}} name="matrix[]" value="{{$data->InternalID}}"></div>
                                                @endif
                                                @elseif($data->Type == 'master')
                                                <label class="col-sm-9 control-label" for="{{'';$data->MatrixID}}"> {{$data->MatrixName}} </label>
                                                <div class="col-sm-3"><input class="form-control master matrix" type="checkbox" {{UserDetail::checkDetail($data->InternalID, $user->InternalID)}} name="matrix[]" value="{{$data->InternalID}}"></div>
                                                @elseif($data->Type == 'setting')
                                                <label class="col-sm-9 control-label" for="{{'';$data->MatrixID}}"> {{$data->MatrixName}} </label>
                                                <div class="col-sm-3"><input class="form-control setting matrix" type="checkbox" {{UserDetail::checkDetail($data->InternalID, $user->InternalID)}} name="matrix[]" value="{{$data->InternalID}}"></div>
                                                @elseif($data->Type == 'transaction')
                                                <label class="col-sm-9 control-label" for="{{'';$data->MatrixID}}"> {{$data->MatrixName}} </label>
                                                <div class="col-sm-3"><input class="form-control transaction matrix" type="checkbox" {{UserDetail::checkDetail($data->InternalID, $user->InternalID)}} name="matrix[]" value="{{$data->InternalID}}"></div>
                                                @elseif($data->Type == 'utility')
                                                <label class="col-sm-9 control-label" for="{{'';$data->MatrixID}}"> {{$data->MatrixName}} </label>
                                                <div class="col-sm-3"><input class="form-control utility matrix" type="checkbox" {{UserDetail::checkDetail($data->InternalID, $user->InternalID)}} name="matrix[]" value="{{$data->InternalID}}"></div>
                                                @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="required">
                                    * Required
                                </div>
                            </div>
                        </div><!---- end div tableadd---->  
                    </div><!---- end div tabwrap----> 
                    <div class="btnnest pull-right">
                        <button type="submit" class="btn btn-green btn-sm btn-save" id="btn-save"> Save </button>
                    </div>
                </form>
            </div>   
        </div>
    </div>
</div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script src="{{Asset('js/entry-js-master/userMatrix.js')}}"></script>
@stop