@extends('template.header-footer')

@section('title')
Purchase
@stop

@section('css')
<link rel="stylesheet" href="{{Asset('css/chosenCustom.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<link rel="stylesheet" href="{{Asset('morris.css')}}">
@stop

@section('nav')

@stop

@section('content')
@if(myCheckIsEmpty('Supplier;Warehouse;Currency;Inventory;Default'))
<div class="alert alert-warning alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Warning!</strong> You must at least have one master Supplier, Currency, Warehouse, Default COA and Inventory to insert purchase.
</div>
@endif
@if(Session::get('messages') == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest"> 
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showPurchase')}}" type="button" class="btn btn-sm btn-pure">Purchase</a>
            </div>
            <div class="btn-group margr5">
                <a href="{{Route('purchaseNew')}}">
                    <button type="button" <?php if (myCheckIsEmpty('Supplier;Warehouse;Currency;Inventory;Default')) echo 'disabled'; ?> class="btn btn-green btn-sm dropdown-toggle " aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
                </a>
            </div>
            <button id="search-button"  <?php if (myCheckIsEmpty('Purchase')) echo 'disabled'; ?>   class="btn btn-green btn-sm margr5"><span class="glyphicon glyphicon-search"></span> Search </button>
            @if(checkModul('O04'))
            <button type="button" class="btn btn-green margr5" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rSummary">
                <span class="glyphicon glyphicon-file"></span> Summary Report</button>
            <button type="button" class="btn btn-green" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rDetail">
                <span class="glyphicon glyphicon-file"></span> Detail Report</button>
            @endif
        </div>
        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Search</h4>
            </div>
            <div class="tableadd">     
                <ul class="searchmenu">
                    <form method="GET" action="">
                        <li><label for="typePayment">Payment Type</label>
                            <br>
                            <select name="typePayment" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Cash</option>
                                <option value="1">Credit</option>  
                            </select>
                        </li>
                        <li><label for="typeTax">Tax Type</label>
                            <br>
                            <select name="typeTax" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Non Tax</option>
                                <option value="1">Tax</option>  
                            </select>
                        </li>
                        <li><label for="date">Start Date</label>
                            <br><input id="startDate" name="startDate" type="text" autocomplete="off">
                        </li>
                        <li><label for="date">End Date</label>
                            <br><input id="endDate" name="endDate" type="text" autocomplete="off">
                        </li>
                        <li><br>
                            <button type="submit" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-search"></span> Search </button>
                        </li>
                        <li><br>
                            <button id="cancel" type="reset"  class="btn btn-pure btn-sm"><span class="glyphicon glyphicon-remove"></span> Cancel </button>
                        </li>
                    </form>
                </ul>
            </div><!---- end div tableadd---->
        </div><!---- end div tabwrap---->
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
<div class="wrapjour">
    <div class="primcontentnopadd">
        <div class="leftrow pull-left">
            <div class="tabwrap">
                <div class="tabhead">
                    <h4 class="headtitle">Last Ten Purchase</h4>
                </div>
                <div class="tableadd overhide"> 
                    @if(Count(PurchaseHeader::getTopTen()) > 0)
                    <div class="headinv new" id="graph">
                    </div><!---- end div new---->
                    @else
                    <b><center>There is no purchase.</center></b>
                    @endif  
                </div><!---- end div tableadd---->   
            </div><!---- end div tabwrap---->                       
        </div><!---- end div leftrow----> 
        <div class="rightrow pull-right">
            <div class="tabwrap">
                <div class="tabhead">
                    <h4 class="headtitle">Top Ten Supplier From Each Last Ten Purchase</h4>
                </div>
                <div class="tableadd ovaerhide"> 
                    @if(Count(Coa6::supplierTop10()) > 0)
                    @if(Count(PurchaseHeader::getTopTen()) <= 0)
                    <b><center>There is no purchase.</center></b>
                    @else
                    <div class="headinv new" id="donut">
                    </div><!---- end div new---->
                    @endif
                    @else
                    <b><center>There is no supplier.</center></b>
                    @endif   
                </div><!---- end div tableadd---->   
            </div><!---- end div tabwrap---->                       
        </div><!---- end div rigthrow---->  
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->

@stop

@section('modal')
<div class="modal fade" id="r_summary" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Summary Purchase</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="" target="_blank">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='summaryPurchase'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateReport" data-validation="required">
                            </li>
                        </ul>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>      
        </div>
    </div>  
</div>
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script><script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script>
    var purchaseDataBackup = '<?php echo Route('purchaseDataBackup',Input::get('typePayment').'---;---'.Input::get('typeTax').'---;---'.Input::get('startDate').'---;---'.Input::get('endDate')) ?>';
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-pembelian/purchase.js')}}"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.2/raphael-min.js"></script>
<script src="{{Asset('morris.js')}}" type="text/javascript"></script>
<script>
//morris js............................................................

<?php if (Count(PurchaseHeader::getTopTen()) > 0) { ?>
    Morris.Bar({
    element: 'graph',
            data: [
    <?php foreach (PurchaseHeader::getTopTen() as $data) { ?>
                {y: '<?php echo date("d-m-Y", strtotime($data->PurchaseDate)); ?>', a: '<?php echo $data->hasil ?>'},
    <?php } ?>
            ],
            xkey: 'y',
            xLabelMargin: 0,
            xLabelAngle: 60,
            ykeys: ['a'],
            labels: ['Purchase'],
            resize: true
    });
<?php } if (Count(Coa6::supplierTop10()) > 0) { ?>
    Morris.Donut({
    element: 'donut',
            data: [
    <?php
    foreach (Coa6::supplierTop10() as $data) {
        $dataText = explode('---;---', $data);
        $supplier = Coa6::find($dataText[1]);
        ?>
                {value: <?php echo $dataText[0]; ?>, label: '<?php echo $supplier->ACC6Name; ?>', formatted: 'Rp.<?php echo number_format($dataText[0], '2', '.', ','); ?>'},
    <?php } ?>
            ],
            formatter: function (x, data) {
            return data.formatted;
            },
            resize: true
    });
<?php } ?>
</script>
@stop