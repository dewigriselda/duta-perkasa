@extends('template.header-footer')

@section('title')
Purchase Return
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
@stop

@section('nav')

@stop

@section('content')
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest"> 
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showPurchaseReturn')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Purchase Return</a>
                <a href="{{route('purchaseReturnDetail', $header->PurchaseReturnID)}}" type="button" class="btn btn-sm btn-pure">Detail {{$header->PurchaseReturnID}}</a>
            </div>
            <div class="btn-group margr5">
                <button type="button" class="btn btn-green btn-sm dropdown-toggle" data-target="#insertReturn" data-toggle="modal" role="dialog"
                        aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
            </div>
            <button id="search-button"  <?php if (myCheckIsEmpty('PurchaseReturn')) echo 'disabled'; ?> class="btn btn-green btn-sm margr5"><span class="glyphicon glyphicon-search"></span> Search </button>
            @if(checkModul('O04'))
            <button type="button" class="btn btn-green margr5" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rSummary">
                <span class="glyphicon glyphicon-file"></span> Summary Report</button>
            <button type="button" class="btn btn-green margr5" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rDetail">
                <span class="glyphicon glyphicon-file"></span> Detail Report</button>
            @endif
            <a href="{{Route('purchaseReturnUpdate',$header->PurchaseReturnID)}}">
                <button id="btn-{{$header->PurchaseReturnID}}-update"
                        class="btn btn-green btn-sm margr5">
                    <span class="glyphicon glyphicon-edit"></span> Edit
                </button>
            </a>
            @if(checkModul('O04'))
            <a href="{{Route('purchaseReturnPrint',$header->PurchaseReturnID)}}" target='_blank' style="margin-right: 0px !important;">
                <button id="btn-{{$header->PurchaseReturnID}}-print"
                        class="btn btn-green btn-sm ">
                    <span class="glyphicon glyphicon-print"></span> Print
                </button>
            </a>
            @endif
        </div>
        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Search</h4>
            </div>
            <div class="tableadd">     
                <ul class="searchmenu">
                    <form method="GET" action="{{Route('showPurchaseReturn')}}">
                        <li><label for="typePayment">Payment Type</label>
                            <br>
                            <select name="typePayment" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Cash</option>
                                <option value="1">Credit</option>  
                            </select>
                        </li>
                        <li><label for="typeTax">Tax Type</label>
                            <br>
                            <select name="typeTax" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Non Tax</option>
                                <option value="1">Tax</option>  
                            </select>
                        </li>
                        <li><label for="date">Start Date</label>
                            <br><input id="startDate" name="startDate" type="text" autocomplete="off">
                        </li>
                        <li><label for="date">End Date</label>
                            <br><input id="endDate" name="endDate" type="text" autocomplete="off">
                        </li>
                        <li><br>
                            <button type="submit" class="btn btn-green btn-sm">Search <span class="glyphicon glyphicon-search"></span></button>
                        </li>
                        <li><br>
                            <button id="cancel" type="reset"  class="btn btn-pure btn-sm">Cancel <span class="glyphicon glyphicon-remove"></span></button>
                        </li>
                    </form>
                </ul>
            </div><!---- end div tableadd---->
        </div><!---- end div tabwrap---->

        <div class="tabwrap">
            <div class="tabhead">
                <h4 class="headtitle">{{'Purchase Return '.$header->PurchaseReturnID}}</h4>
            </div>
            <div class="tableadd"> 
                <div class="headinv new">
                    <ul class="pull-left">
                        <li>
                            <label for="date">Date</label>
                            <span>{{date( "d-m-Y", strtotime($header->PurchaseReturnDate))}}</span>
                        </li>
                        <li>
                            <label for="supplier">Supplier</label>
                            <span><?php
                                $coa6 = PurchaseReturnHeader::find($header->InternalID)->coa6;
                                echo $coa6->ACC6ID . ' ' . $coa6->ACC6Name
                                ?></span>
                        </li>
                        <li>
                            <label for="longTerm">Payment</label>
                            @if($header->isCash == 0)
                            <span>{{'Cash'}}</span>
                            @else
                            <span>{{'Credit'}}</span>
                            @endif
                        </li>
                        <li>
                            <label for="warehouse">Warehouse</label>
                            <span>{{$header->Warehouse->WarehouseName}}</span>
                        </li>
                    </ul>
                    <ul class="pull-right">
                        <li>
                            <label for="currency">Currency</label>
                            <span>{{$header->Currency->CurrencyName}}</span>
                        </li>
                        <li>
                            <label for="rate">Rate</label>
                            <span>{{number_format($header->CurrencyRate,'2','.',',')}}</span>
                        </li>
                        <li>
                            <label for="VAT">VAT</label>
                            @if($header->VAT == 0)
                            <span>{{'Non Tax'}}</span>
                            @else
                            <span>{{'Tax'}}</span>
                            @endif
                        </li>
                        <li>
                            <label for="">Remark</label>
                            <span>{{$header->Remark}}</span>
                        </li>
                    </ul>
                </div>
                <div class="padrl10">
                    <table class="table master-data " id="table-purchaseReturn" >
                        <thead>
                            <tr>
                                <th>Inventory</th>
                                <th>Qty</th>
                                <th>Price</th>
                                <th>Disc (%)</th>
                                <th>Disc</th>
                                <th>Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($detail) > 0)
                            <?php
                            $total = 0;
                            $totalVAT = 0;
                            ?>
                            @foreach($detail as $data)
                            <tr>
                                <td class="left">{{'';$inventory = Inventory::find($data->InventoryInternalID); echo $inventory->InventoryID.' '.$inventory->InventoryName}}</td>
                                <td class="right">{{number_format($data->Qty,'0','.',',')}}</td>
                                <td class="right">{{number_format($data->Price,'2','.',',')}}</td>
                                <td class="right">{{$data->Discount.''}}</td>
                                <td class="right">{{number_format($data->DiscountNominal,'2','.',',')}}</td>
                                {{'';$totalVAT += $data->VAT}}
                                <td class="right">{{number_format($data->SubTotal,'2','.',',');$total += $data->SubTotal}}</td>
                            </tr>
                            @endforeach
                            @if($totalVAT != 0)
                            {{'';$totalVAT = $totalVAT - $header->DiscountGlobal*0.1;}}
                            @endif
                            @else
                            <tr>
                                <td colspan="6">There is no inventory registered in this purchase return.</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>

                    <table class="pull-left"> 
                        <tr>
                            <td></td>
                        </tr>
                        <tr>
                            <td><p>Created by {{User::where('UserID', $header->UserRecord)->first()->UserName.' '.date('d-m-Y H:i:s', strtotime($header->dtRecord))}}</p></td>
                        </tr>
                        <tr>
                            @if($header->UserModified != '0')
                            <td><p>Modified by {{User::where('UserID', $header->UserModified)->first()->UserName.' '.date('d-m-Y H:i:s', strtotime($header->dtModified))}}</p></td>
                            @else
                            <td><p>Modified by -</p></td>
                            @endif
                        </tr>
                    </table>
                    @if(count($detail) > 0)

                    <table class="pull-right"> 
                        <tr>
                            <td><h5 class="right margr10 h5total"><b>Total</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="total">{{number_format($total,'2','.',',')}}</b></h5></td>
                        </tr>
                        <tr >
                            <td><h5 class="right margr10 h5total"><b>Discount</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total new"><b>{{number_format($header->DiscountGlobal, '2', '.',',')}}</b></h5></td>
                        </tr>
                        <tr>
                            <td><h5 class="right margr10 h5total"><b>Grand Total</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="grandTotal">{{number_format(($total-$header->DiscountGlobal),'2','.',',')}}</b></h5></td>
                        </tr>

                        <tr>
                            <td><h5 class="right margr10 h5total hidevat"><b>Tax</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="tax">{{number_format($totalVAT,'2','.',',')}}</b></h5></td>
                        </tr>
                        <tr>
                            <td><h5 class="right margr10 h5total hidevat"><b>Grand Total(tax)</b></h5></td>
                            <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b id="grandTotalAfterTax">{{number_format($header->GrandTotal,'2','.',',')}}</b></h5></td>
                        </tr>
                    </table>

                    @endif
                </div><!---- end div padrl10---->         
            </div><!---- end div tableadd---->   
        </div><!---- end div tabwrap---->                 
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->

@stop


@section('modal')
<div class="modal fade" id="insertReturn" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insert Purchase Return</h4>
            </div>
            {{'';$hitung = 0;}}
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action">
                        <ul>
                            <input type="hidden" value="insertPurchaseReturn" id="jenisReturn" name="jenis">
                            <li>
                                <label for="purchase">Purchase ID</label> *
                            </li>
                            <input class="input-theme margbot10" type="text" id="searchText" title="Type Name or ID then 'Enter'" placeholder="Type Name or ID then 'Enter'">
                            <li id="selectResult">

                            </li>
                        </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="btn-add-sr" @if($hitung==0){{'disabled'}}@endif class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>      
        </div>
    </div>  
</div>

<div class="modal fade" id="r_summary" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Summary Purchase</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="" target="_blank">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='summaryPurchase'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateReport" data-validation="required">
                            </li>
                        </ul>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>      
        </div>
    </div>  
</div>
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script><script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script>
var getResultSearchPR = "<?php echo Route("getResultSearchPR") ?>";
var purchaseReturnDataBackup = '<?php echo Route('purchaseReturnDataBackup', Input::get('typePayment') . '---;---' . Input::get('typeTax') . '---;---' . Input::get('startDate') . '---;---' . Input::get('endDate')) ?>';
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-pembelian/purchaseReturn.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
@stop