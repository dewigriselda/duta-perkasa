@extends('template.header-footer')

@section('title')
Purchase
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<style>
    .help-block {
        margin-left: 30% !important;
    }
    .headinv .chosen-single{
        width: 200px !important;
    }
    .headinv .chosen-drop{
        width: 200px !important;
    }
</style>
@stop

@section('nav')

@stop

@section('content')
@if(isset($messages))
@if($messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Purchase has been updated.
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest"> 
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showPurchase')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Purchase</a>
                <a href="{{route('purchaseUpdate', $header->PurchaseID)}}" type="button" class="btn btn-sm btn-pure">Update {{$header->PurchaseID}}</a>
            </div>
            <div class="btn-group margr5">
                <a href="{{Route('purchaseNew')}}">
                    <button type="button" class="btn btn-green btn-sm dropdown-toggle " aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
                </a>
            </div>
            <button id="search-button" <?php if (myCheckIsEmpty('Purchase')) echo 'disabled'; ?>  class="btn btn-green btn-sm margr5"> <span class="glyphicon glyphicon-search"></span> Search </button>
            @if(checkModul('O04'))
            <button type="button" class="btn btn-green margr5" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rSummary">
                <span class="glyphicon glyphicon-file"></span> Summary Report</button>
            <button type="button" class="btn btn-green" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rDetail">
                <span class="glyphicon glyphicon-file"></span> Detail Report</button>
            @endif
        </div>
        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Search</h4>
            </div>
            <div class="tableadd">     
                <ul class="searchmenu">
                    <form method="GET" action="{{Route('showPurchase')}}">
                        <li><label for="typePayment">Payment Type</label>
                            <br>
                            <select name="typePayment" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Cash</option>
                                <option value="1">Credit</option>  
                            </select>
                        </li>
                        <li><label for="typeTax">Tax Type</label>
                            <br>
                            <select name="typeTax" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Non Tax</option>
                                <option value="1">Tax</option>  
                            </select>
                        </li>
                        <li><label for="date">Start Date</label>
                            <br><input id="startDate" name="startDate" type="text" autocomplete="off">
                        </li>
                        <li><label for="date">End Date</label>
                            <br><input id="endDate" name="endDate" type="text" autocomplete="off">
                        </li>
                        <li><br>
                            <button type="submit" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-search"></span> Search </button>
                        </li>
                        <li><br>
                            <button id="cancel" type="reset"  class="btn btn-pure btn-sm"><span class="glyphicon glyphicon-remove"></span>  Cancel </button>
                        </li>
                    </form>
                </ul>
            </div><!---- end div tableadd---->
        </div><!---- end div tabwrap---->
        <form method="POST" action="">
            <input type='hidden' name='PurchaseInternalID' value='{{$header->InternalID}}'>
            <div class="tabwrap">
                <div class="tabhead">
                    <h4 class="headtitle">Purchase <span id="purchaseID">{{$header->PurchaseID}}</span></h4>
                </div>
                <div class="tableadd"> 
                    <div class="headinv new">
                        <ul @if(checkModul('O05')) class="pull-left" style="width: 360px;" @else class="pull-left"  @endif>
                             <li>
                                <label for="coa6">Supplier *</label>
                                <select class="chosen-select" id="coa6" style="" name="coa6">
                                    @foreach(Coa6::where("Type", "s")->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $coa6)
                                    @if($coa6->InternalID == $header->ACC6InternalID)
                                    <option selected="selected" value="{{$coa6->InternalID}}">
                                        {{$coa6->ACC6ID.' '.$coa6->ACC6Name}}
                                    </option>
                                    @else
                                    <option value="{{$coa6->InternalID}}">
                                        {{$coa6->ACC6ID.' '.$coa6->ACC6Name}}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>
                            </li>      
                            <li>
                                <label for="payment">Payment *</label>
                                @if($header->isCash == 0)  
                                <input type="radio" class="radio-tipe" id="paymentCash" name="isCash" value="0" checked="checked">Cash
                                <input type="radio" class="radio-tipe" id="paymentCredit" name="isCash" value="1">Credit      
                                @else           
                                <input type="radio" class="radio-tipe" id="paymentCash" name="isCash" value="0">Cash
                                <input type="radio" class="radio-tipe" id="paymentCredit" name="isCash" value="1" checked="checked">Credit      
                                @endif               
                            </li>                         
                            <li>
                                <label for="longTerm">Long Term *</label>
                                @if($header->isCash == 0)   
                                <input type="text" name="longTerm" style="width: 200px;" id="longTerm" value="0" disabled>
                                @else
                                <input type="text" name="longTerm" style="width: 200px;" id="longTerm"  value='{{$header->LongTerm}}'>
                                @endif
                            </li>
                            <li>
                                <label for="slip">Slip number *</label>
                                <?php
                                $slipInternal = '';
                                $journal = JournalHeader::where('TransactionID', '=', $header->PurchaseID)->where('CompanyInternalID', Auth::user()->Company->InternalID)->get();
                                if (count($journal) > 0) {
                                    $slipInternal = $journal[0]->SlipInternalID;
                                } else {
                                    $slipInternal = '';
                                }
                                if ($header->isCash == 0) {
                                    $slipDefault = '';
                                } else {
                                    $slipDefault = 'Disabled = "true"';
                                }
                                ?>
                                <select class="chosen-select" id="slip" style="" name="slip"  {{$slipDefault}}>
                                    @foreach(Slip::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $slip)
                                    @if($slip->InternalID == $slipInternal)
                                    <option selected="selected" value="{{$slip->InternalID}}">
                                        {{$slip->SlipID.' '.$slip->SlipName}}
                                    </option>
                                    @else
                                    <option value="{{$slip->InternalID}}">
                                        {{$slip->SlipID.' '.$slip->SlipName}}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>
                            </li>
                        </ul>
                        <ul @if(checkModul('O05')) class="pull-left" style="width: 360px;" @else class="pull-right"  @endif>
                             <li>
                                <label for="warehouse">Warehouse *</label>
                                <select class="chosen-select choosen-modal warehouse" id="warehouseHeader" name="warehouse">
                                    @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $war)
                                    @if($war->InternalID == $header->WarehouseInternalID)
                                    <option selected="selected" id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                        {{$war->WarehouseName;}}
                                    </option>
                                    @else
                                    <option id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                        {{$war->WarehouseName;}}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label for="currency">Currency *</label>
                                <select class="chosen-select choosen-modal currency" id="currencyHeader" name="currency">
                                    @foreach(Currency::orderBy('Default','desc')->where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $cur)
                                    @if($cur->InternalID == $header->CurrencyInternalID)
                                    <option selected="selected" id="cur{{$cur->InternalID}}" value="{{$cur->InternalID.'---;---'.$cur->CurrencyName.'---;---'.$cur->Rate.'---;---'.$cur->Default}}">
                                        {{$cur->CurrencyName;}}
                                    </option>
                                    @else
                                    <option id="cur{{$cur->InternalID}}" value="{{$cur->InternalID.'---;---'.$cur->CurrencyName.'---;---'.$cur->Rate.'---;---'.$cur->Default}}">
                                        {{$cur->CurrencyName;}}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label for="rate">Rate *</label>
                                <input type="text" style="width: 200px;" class="maxWidth rate numajaDesimal" name="rate" maxlength="" id="rate" value="{{$header->CurrencyRate}}" data-validation="required">
                            </li>
                            <li>
                                <label for="vat">VAT *</label>
                                @if($header->VAT == 0) 
                                <input style="width:15px; height: 15px;" type="checkbox" name="vat" id="vat" value="1"> Tax 10%
                                @else
                                <input style="width:15px; height: 15px;" type="checkbox" checked name="vat" id="vat" value="1"> Tax 10%
                                @endif
                            </li>
                            <li>
                                <label for="remark">Remark *</label>
                                <textarea name="remark" class="new-textarea-small" id="remark" data-validation="required" style="width: 200px">{{$header->Remark}}</textarea>
                            </li>
                            @if(!checkModul('O05'))
                            <li>
                                <div class="required">
                                    * Required
                                </div>
                            </li>
                            @endif
                        </ul>
                        @if(checkModul('O05'))
                        <ul class="pull-left" style="width: 360px;">
                            <li>
                                <label for="transactiontype">Transaction</label>
                                <select class="chosen-select choosen-modal currency" id="transactionType" name="TransactionType">
                                    <option value="1" {{($header->TransactionType == 1 ? 'selected' : '')}}> For who is not collect PPN </option>
                                    <option value="2" {{($header->TransactionType == 2 ? 'selected' : '')}}> For Chamberlain </option>
                                    <option value="3" {{($header->TransactionType == 3 ? 'selected' : '')}}> Except Chamberlain </option>
                                    <option value="4" {{($header->TransactionType == 4 ? 'selected' : '')}}> DPP other value </option>
                                    <option value="6" {{($header->TransactionType == 6 ? 'selected' : '')}}> Other handover, include handover to foreigner tourist in the event of VAT refund </option>
                                    <option value="7" {{($header->TransactionType == 7 ? 'selected' : '')}}> Handover PPN is not collect</option>
                                    <option value="8" {{($header->TransactionType == 8 ? 'selected' : '')}}> Handover PPN Freed</option>
                                    <option value="9" {{($header->TransactionType == 9 ? 'selected' : '')}}> Handover Assets (Pasal 16D UU PPN)</option>
                                </select>
                            </li>
                            <li>
                                <label for="replacement">Replacement</label>
                                @if($header->Replacement == 1)
                                <input style="width:15px; height: 15px;" checked="true" type="checkbox" name="Replacement" id="replacement" value="1"> Tax Replacement
                                @else
                                <input style="width:15px; height: 15px;" type="checkbox" name="Replacement" id="replacement" value="1"> Tax Replacement
                                @endif
                            </li>
                            <li>
                                <label for="numbertax">Tax Number</label>
                                <input type="hidden" name="TaxNumber" value="{{$header->TaxNumber}}" id="numberTax">
                                <?php $taxNumberSplit = explode('-', $header->TaxNumber); ?>
                                <?php $taxNumber1 = explode('.', $taxNumberSplit[0]); ?>
                                <?php $taxNumber2 = explode('.', $taxNumberSplit[1]); ?>
                                <input type="text" class="numaja autoTab" style="width: 40px;" maxlength="3" id="numberTax1"  value="{{$taxNumber1[0]}}"> .
                                <input type="text" class="numaja autoTab" style="width: 40px;" maxlength="3" id="numberTax2"  value="{{$taxNumber1[1]}}"> -
                                <input type="text" class="numaja autoTab" style="width: 30px;" maxlength="2" id="numberTax3"  value="{{$taxNumber2[0]}}"> .
                                <input type="text" class="numaja autoTab" style="width: 75px;" maxlength="8" id="numberTax4"  value="{{$taxNumber2[1]}}">
                            </li>
                            <li>
                                <label for="taxmonth">Tax Month</label>
                                <select class="chosen-select choosen-modal currency" id="taxMonth" name="TaxMonth">
                                    @for($aa = 1; $aa<=12; $aa++)
                                    <option value="{{$aa}}" {{($header->TaxMonth == $aa ? 'selected' : '')}}>{{date('F',strtotime('2015-'.$aa.'-01'))}}</option>
                                    @endfor
                                </select>
                            </li>
                            <li>
                                <label for="taxyear">Tax Year</label>
                                {{''; $year = date('Y'); $mulai = 2012;}}
                                <select class="chosen-select choosen-modal currency" id="taxYear" name="TaxYear">
                                    @while($mulai <= $year)
                                    @if( $header->TaxYear == $mulai )
                                    <option selected="true" value="{{$mulai}}">{{$mulai}}</option>
                                    @else
                                    <option value="{{$mulai}}">{{$mulai}}</option>
                                    @endif
                                    {{'';$mulai++;}}
                                    @endwhile
                                </select>
                            </li>
                            <li>
                                <div class="required">
                                    * Required
                                </div>
                            </li>
                        </ul>
                        @endif
                    </div>
                    <div class="tableadd journupdate">
                        <table class="table master-data" id="table-purchase" style="table-layout:fixed;">
                            <thead>
                                <tr>
                                    <th style="width: 25%;">Inventory</th>
                                    <th style="width: 10%;">Qty</th>
                                    <th style="width: 10%;">Price</th>
                                    <th style="width: 10%;">Disc (%)</th>
                                    <th style="width: 10%;">Disc</th>
                                    <th style="width: 10%;">Subtotal</th>
                                    <th style="width: 20%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $barisTerakhir = 0; ?>
                                @if(count($detail) > 0)
                                @foreach($detail as $data)
                                <tr id="row{{$barisTerakhir}}">
                                    <td class="chosen-transaction">
                                        <select class="chosen-select choosen-modal left" id="inventory-{{$barisTerakhir}}" name="inventory[]">
                                            @foreach(Inventory::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $inventory)
                                            @if($inventory->InternalID == $data->InventoryInternalID)
                                            <option selected="selected" id="inventory{{$inventory->InternalID}}" value="{{$inventory->InternalID}}">
                                                {{$inventory->InventoryID}} 
                                                {{" ".$inventory->InventoryName}}
                                            </option>
                                            @else
                                            <option id="inventory{{$inventory->InternalID}}" value="{{$inventory->InternalID}}">
                                                {{$inventory->InventoryID}} 
                                                {{" ".$inventory->InventoryName}}
                                            </option>
                                            @endif
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth qty right numaja input-theme" name="qty[]" maxlength="11" min="1" id="price-{{$barisTerakhir}}-qty" value="{{number_format($data->Qty,'0','.',',')}}">
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth price right numajaDesimal input-theme" name="price[]" maxlength="" id="price-{{$barisTerakhir}}" value="{{number_format($data->Price,'2','.',',')}}">
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth discount right numajaDesimal input-theme" name="discount[]" id="price-{{$barisTerakhir}}-discount" value="{{$data->Discount}}">
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth discountNominal right numajaDesimal input-theme" name="discountNominal[]" id="price-{{$barisTerakhir}}-discountNominal" value="{{number_format($data->DiscountNominal,'2','.',',')}}">
                                    </td>
                                    <td id="price-{{$barisTerakhir}}-qty-hitung" class="right subtotal">
                                        0.00
                                    </td>
                                    <td>
                                        @if($barisTerakhir == 0)
                                        -
                                        @else
                                        <button class="btn btn-pure-xs btn-xs btn-deleteRow" type="button" data="row{{$barisTerakhir}}"><span class="glyphicon glyphicon-trash"></span></button>
                                        @endif
                                    </td>
                                </tr>
                                {{'';$barisTerakhir++;}}
                                @endforeach
                                @else
                                <tr id="row0">
                                    <td class="chosen-transaction">
                                        <select class="chosen-select choosen-modal left" id="inventory-0" name="inventory[]">
                                            @foreach(Inventory::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $inventory)
                                            <option id="inventory{{$inventory->InternalID}}" value="{{$inventory->InternalID}}">
                                                {{$inventory->InventoryID}} 
                                                {{" ".$inventory->InventoryName}}
                                            </option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth qty right" name="qty[]" maxlength="11" min="1" id="price-0-qty" value="1">
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth price right numajaDesimal input-theme" name="price[]" maxlength="" id="price-0" value="0.00">
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth discount right numajaDesimal input-theme" name="discount[]" id="price-0-discount" value="0.00">
                                    </td>
                                    <td class="text-right">
                                        <input type="text" class="maxWidth discountNominal numajaDesimal right input-theme" name="discountNominal[]" id="price-0-discountNominal" value="0.00">
                                    </td>
                                    <td id="price-0-qty-hitung" class="right subtotal">
                                        0.00
                                    </td>
                                    <td>
                                        -
                                    </td>
                                </tr>
                                {{'';$barisTerakhir++;}}
                                @endif
                            </tbody>
                        </table>
                        <input type="hidden" name="grandTotalValue" value="0" id="grandTotalValue">


                        <table class="pull-left"> 
                            <tr>
                                <td>
                                    <button type="button" class="btn btn-green btn-sm" id="btn-addRow"><span class="glyphicon glyphicon-plus"></span> Add row</button>
                                </td>
                            </tr>
                            <tr>
                                <td><br></td>
                            </tr>
                            <tr>
                                <td><p>Created by {{User::where('UserID', $header->UserRecord)->first()->UserName.' '.date('d-m-Y H:i:s', strtotime($header->dtRecord))}}</p></td>
                            </tr>
                            <tr>
                                @if($header->UserModified != '0')
                                <td><p>Modified by {{User::where('UserID', $header->UserModified)->first()->UserName.' '.date('d-m-Y H:i:s', strtotime($header->dtModified))}}</p></td>
                                @else
                                <td><p>Modified by -</p></td>
                                @endif
                            </tr>
                        </table>
                        <table class="pull-right"> 
                            <tr>
                                <td><h5 class="right margr10 h5total"><b>Total</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b id="total"></b></h5></td>
                            </tr>
                            <tr >
                                <td><h5 class="right margr10 h5total"><b>Discount</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                                <td><h5 class="right margr10 h5total new" style="width: 150px;"><input type="text" style="width: 120px;" class="maxWidth right numajaDesimal discountGlobal" name="DiscountGlobal" maxlength="" id="discountGlobal" value="{{number_format($header->DiscountGlobal, '2', '.', ',')}}"></h5></td>
                            </tr>
                            <tr >
                                <td><h5 class="right margr10 h5total"><b>Down Payment</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                                <td><h5 class="right margr10 h5total new" style="width: 150px;"><input type="text" style="width: 120px;" class="maxWidth right numajaDesimal downPayment" name="DownPayment" maxlength="" id="dp" value="{{number_format($header->DownPayment, '2', '.', ',')}}"></h5></td>
                            </tr>
                            <tr>
                                <td><h5 class="right margr10 h5total"><b>Grand Total</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b id="grandTotal"></b></h5></td>
                            </tr>

                            <tr>
                                <td><h5 class="right margr10 h5total hidevat"><b>Tax</b></h5></td>
                                <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b id="tax"></b></h5></td>
                            </tr>
                            <tr>
                                <td><h5 class="right margr10 h5total hidevat"><b>Grand Total(tax)</b></h5></td>
                                <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b id="grandTotalAfterTax"></b></h5></td>
                            </tr>
                        </table>
                    </div><!---- end div tableadd---->      
                </div><!---- end div tableadd---->   
            </div><!---- end div tabwrap---->  
            <div class="btnnest pull-right">
                <button class="btn btn-green btn-sm btn-save" id="btn-save"> Save </button>
            </div>
        </form>
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop

@section('modal')
<div class="modal fade" id="r_summary" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Summary Purchase</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="" target="_blank">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='summaryPurchase'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateReport" data-validation="required">
                            </li>
                        </ul>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>      
        </div>
    </div>  
</div>
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNewJournal.jquery.js')}}" type="text/javascript"></script>
<script>
var textSelect = '<select class="chosen-select choosen-modal" id="" style="" name="inventory[]">' +
<?php foreach (Inventory::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $inventory) { ?> '<option id="inventory<?php echo $inventory->InternalID ?>" value="<?php echo $inventory->InternalID ?>">' +
            '<?php echo $inventory->InventoryID . " " . $inventory->InventoryName ?>' + '</option>' +<?php } ?>
'</select>';
var baris = '<?php echo $barisTerakhir; ?>';
</script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script>
    var purchaseDataBackup = '<?php echo Route('purchaseDataBackup',Input::get('typePayment').'---;---'.Input::get('typeTax').'---;---'.Input::get('startDate').'---;---'.Input::get('endDate')) ?>';
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-pembelian/purchase.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-pembelian/purchaseUpdate.js')}}"></script>
@stop