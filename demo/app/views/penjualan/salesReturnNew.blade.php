@extends('template.header-footer')

@section('title')
Sales Return
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<style>
    .help-block {
        margin-left: 30% !important;
    }
</style>
@stop

@section('nav')

@stop

@section('content')
@if(isset($messages))
@if($messages == 'gagalInsert')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesInsert')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> New sales return has been inserted.
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest"> 
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showSalesReturn')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Sales Return</a>
                <a href="{{route('salesReturnNew',$header->SalesID)}}" type="button" class="btn btn-sm btn-pure">New Sales Return from {{$header->SalesID}}</a>
            </div>
            <div class="btn-group margr5">
                @if(myCheckIsEmpty('Sales'))
                <button type="button" disabled="true" class="btn btn-green btn-sm dropdown-toggle" data-target="#insertReturn" data-toggle="modal" role="dialog"
                        aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
                @else 
                <button type="button"  class="btn btn-green btn-sm dropdown-toggle" data-target="#insertReturn" data-toggle="modal" role="dialog"
                        aria-expanded="false"><span class="glyphicon glyphicon-plus"></span> New </button>
                @endif

            </div>
            <button id="search-button"  <?php if (myCheckIsEmpty('SalesReturn')) echo 'disabled'; ?> class="btn btn-green btn-sm margr5"><span class="glyphicon glyphicon-search"></span> Search </button>
            @if(checkModul('O04'))
            <button type="button" class="btn btn-green margr5" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rSummary">
                <span class="glyphicon glyphicon-file"></span> Summary Report</button>
            <button type="button" class="btn btn-green" data-target="#r_summary" data-toggle="modal" role="dialog" id="btn-rDetail">
                <span class="glyphicon glyphicon-file"></span> Detail Report</button>
            @endif
        </div>
        <div class="tabwrap" id="searchwrap">
            <div class="tabhead">
                <h4 class="headtitle">Search</h4>
            </div>
            <div class="tableadd">     
                <ul class="searchmenu">
                    <form method="GET" action="">
                        <li><label for="typePayment">Payment Type</label>
                            <br>
                            <select name="typePayment" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Cash</option>
                                <option value="1">Credit</option>  
                            </select>
                        </li>
                        <li><label for="typeTax">Tax Type</label>
                            <br>
                            <select name="typeTax" style="width: 100px">
                                <option value="-1">All</option>
                                <option value="0">Non Tax</option>
                                <option value="1">Tax</option>  
                            </select>
                        </li>
                        <li><label for="date">Start Date</label>
                            <br><input id="startDate" name="startDate" type="text" autocomplete="off">
                        </li>
                        <li><label for="date">End Date</label>
                            <br><input id="endDate" name="endDate" type="text" autocomplete="off">
                        </li>
                        <li><br>
                            <button type="submit" class="btn btn-green btn-sm"><span class="glyphicon glyphicon-search"></span> Search </button>
                        </li>
                        <li><br>
                            <button id="cancel" type="reset"  class="btn btn-pure btn-sm"><span class="glyphicon glyphicon-remove"></span> Cancel </button>
                        </li>
                    </form>
                </ul>
            </div><!---- end div tableadd---->
        </div><!---- end div tabwrap---->
        <form method="POST" action="" id='form-insertSales'>
            <input type='hidden' name='SalesInternalID' value='{{$header->InternalID}}'>
            <div class="tabwrap">
                <div class="tabhead">
                    @if($header->isCash == 0)
                    <h4 class="headtitle">Return Sales <span id="salesID">{{$header->SalesID}}</span></h4>
                    @else
                    <h4 class="headtitle">Return Sales <span id="salesID">{{$header->SalesID}}</span></h4>
                    @endif
                </div>
                <div class="tableadd"> 
                    <div class="headinv new">
                        <ul class="pull-left">
                            <li>
                                <label for="date">Date *</label>
                                <input id="date" name="date" type="text" autocomplete="off" data-validation="required">
                            </li>
                            <li>
                                <label for="customer">Customer</label>
                                <span><?php
                                    $coa6 = SalesHeader::find($header->InternalID)->coa6;
                                    echo $coa6->ACC6ID . ' ' . $coa6->ACC6Name
                                    ?></span>
                            </li>
                            <li>
                                <label for="longTerm">Payment</label>
                                @if($header->isCash == 0)
                                <span>{{'Cash'}}</span>
                                @else
                                <span>{{'Credit'}}</span>
                                @endif
                            </li>
                            @if($header->isCash != 0)
                            <li>
                                <label for="longTerm">Due Date</label>
                                <span>{{date( "d-m-Y", strtotime("+".$header->LongTerm." day",strtotime($header->SalesDate)))}}</span>
                            </li>
                            @endif
                            <li>
                                <label for="warehouse">Warehouse *</label>
                                <select class="chosen-select choosen-modal currency" id="warehouseHeader" name="warehouse">
                                    @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $war)
                                    <option id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                        {{$war->WarehouseName;}}
                                    </option>
                                    @endforeach
                                </select>
                            </li>
                        </ul>
                        <ul class="pull-right">
                            <li>
                                <label for="currency">Currency</label>
                                <span>{{'';$currency = Currency::find($header->CurrencyInternalID); $currencyName = $currency->CurrencyName; echo $currencyName}}</span>
                            </li>
                            <li>
                                <label for="rate">Rate</label>
                                <span>{{number_format($header->CurrencyRate,'2','.',',')}}</span>
                            </li>
                            <li>
                                <label for="VAT">VAT</label>
                                @if($header->VAT == 0)
                                <span>{{'Non Tax'}}</span>
                                <input type="hidden" id="taxSales" value="0">
                                @else
                                <span>{{'Tax'}}</span>
                                <input type="hidden" id="taxSales" value="1">
                                @endif
                            </li>
                            <li>
                                <label for="remark">Remark *</label>
                                <textarea name="remark" id="remark" data-validation="required"></textarea>
                            </li>
                        </ul>
                    </div>
                    <div class="tableadd journupdate">
                        <table class="table master-data" id="table-sales">
                            <thead>
                                <tr>
                                    <th width="15%">Inventory</th>
                                    <th width="6%">Qty</th>
                                    <th width="8%">Price</th>
                                    <th width="8%">Disc (%)</th>
                                    <th width="10%">Disc</th>
                                    <th width="10%">Subtotal</th>
                                    <th width="10%">Max Qty to Return</th>
                                    <th width="10%">Return</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $barisTerakhir = 0; ?>
                                @if(count($detail) > 0)
                                <?php
                                $total = 0;
                                $totalVAT = 0;
                                ?>
                                @foreach($detail as $data)
                                <?php
                                $sumSales = SalesReturnHeader::getSumReturn($data->InventoryInternalID, $header->SalesID, $data->InternalID);
                                if ($sumSales == '') {
                                    $sumSales = '0';
                                }
                                ?>
                                <tr id="row{{$barisTerakhir}}">
                                    <td class="left">{{'';$inventory = Inventory::find($data->InventoryInternalID); echo $inventory->InventoryID.' '.$inventory->InventoryName}}</td>
                                    <td class="right" id="qtySales-{{$barisTerakhir}}">{{number_format($data->Qty,'0','.',',')}}</td>
                                    <td class="right">{{number_format($data->Price,'2','.',',')}}</td>
                                    <td class="right">{{$data->Discount.''}}</td>
                                    <td class="right">{{number_format($data->DiscountNominal,'2','.',',')}}</td>
                                    {{'';$totalVAT += $data->VAT}}
                                    <td class="right" id="subtotalSales-{{$barisTerakhir}}">{{number_format($data->SubTotal,'2','.',',');$total += $data->SubTotal}}</td>
                                    <td class="right">{{number_format(($data->Qty - $sumSales),'0','.',',')}}</td>
                                    <td class="text-right">
                                        <input type="hidden" class="maxWidth right" name="InternalDetail[]" value="{{$data->InternalID}}">
                                        <input type="hidden" class="maxWidth right" name="inventory[]" value="{{$data->InventoryInternalID}}">
                                        <input type="hidden" class="maxWidth right" name="qty[]" value="{{$data->Qty-$sumSales}}">
                                        <input type="hidden" class="maxWidth right" name="price[]" value="{{$data->Price}}">
                                        <input type="hidden" class="maxWidth right" name="discount[]" value="{{$data->Discount}}">
                                        <input type="hidden" class="maxWidth right" name="discountNominal[]" value="{{$data->DiscountNominal}}">
                                        <input type="text" class="maxWidth right returnSales numaja quantitySales" id="quantitySales-{{$barisTerakhir}}" name="returnSales[]" min="0" max="{{$data->Qty-$sumSales}}" value="0">
                                    </td>
                                </tr>
                                {{'';$barisTerakhir++;}}
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="8">There is no inventory registered in this sales.</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        <input type="hidden" name="grandTotalValue" value="0" id="grandTotalValue">
                        <table class="pull-right"> 
                            <tr>
                                <td><h5 class="right margr10 h5total"><b>Total</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b id="total"></b></h5></td>
                            </tr>
                            <tr >
                            <input type="hidden" id="discountGlobalMax" value="{{($header->DiscountGlobal - SalesReturnHeader::getSumDiscountGlobalSalesReturn($header->SalesID))}}">
                            <td><h5 class="right margr10 h5total"><b>Discount (Max {{number_format(($header->DiscountGlobal - SalesReturnHeader::getSumDiscountGlobalSalesReturn($header->SalesID)),'0','.',',')}})</b></h5></td>
                            <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                            <td><h5 class="right margr10 h5total new" style="width: 150px;"><input type="text" style="width: 120px;" class="maxWidth right numajaDesimal discountGlobal" name="DiscountGlobal" maxlength="" id="discountGlobal" value="0"></h5></td>
                            </tr>
                            <tr>
                                <td><h5 class="right margr10 h5total"><b>Grand Total</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b>:</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b id="grandTotal"></b></h5></td>
                            </tr>

                            <tr>
                                <td><h5 class="right margr10 h5total hidevat"><b>Tax</b></h5></td>
                                <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b id="tax"></b></h5></td>
                            </tr>
                            <tr>
                                <td><h5 class="right margr10 h5total hidevat"><b>Grand Total(tax)</b></h5></td>
                                <td><h5 class="right margr10 h5total hidevat"><b>:</b></h5></td>
                                <td><h5 class="right margr10 h5total"><b id="grandTotalAfterTax"></b></h5></td>
                            </tr>
                        </table>
                    </div><!---- end div tableadd---->      
                </div><!---- end div tableadd---->   
            </div><!---- end div tabwrap---->  
            <div class="btnnest pull-right">
                <button class="btn btn-green btn-sm btn-save" id="btn-save"> Save </button>
            </div>
        </form>
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop

@section('modal')
<div class="modal fade" id="insertReturn" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insert Sales Return</h4>
            </div>
                {{'';$hitung = 0;}}
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action">
                        <ul>
                            <input type="hidden" value="insertSalesReturn" id="jenisReturn" name="jenis">
                            <li>
                                <label for="sales">Sales ID</label> *
                            </li>
                            
                            <input class="input-theme margbot10" type="text" id="searchText" title="Type Name or ID then 'Enter'" placeholder="Type Name or ID then 'Enter'">
                            <li id="selectResult">

                            </li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btn-add-sr" @if($hitung==0){{'disabled'}}@endif class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>      
        </div>
    </div>  
</div>
<div class="modal fade" id="r_summary" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titleReport">Summary Report</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="" target="_blank">
                        <ul>
                            <input type='hidden' name='jenis' id="jenisReport" value='summarySales'>
                            <li>
                                <label for="sDate">Start Date</label> *
                            </li>
                            <li>
                                <input type="text" name="sDate" id="startDateReport" data-validation="required">
                            </li>
                            <li>
                                <label for="eDate">End Date</label> *
                            </li>
                            <li>
                                <input type="text" name="eDate" id="endDateReport" data-validation="required">
                            </li>
                        </ul>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <small class="pull-left note30Pages">Note : Best process is less than 20 pages</small>
                <button type="submit" id="btn-report-transaction" class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>      
        </div>
    </div>  
</div>
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNewJournal.jquery.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script>
        var getResultSearchSR = "<?php echo Route("getResultSearchSR") ?>";
    var salesReturnDataBackup = '<?php echo Route('salesReturnDataBackup',Input::get('typePayment').'---;---'.Input::get('typeTax').'---;---'.Input::get('startDate').'---;---'.Input::get('endDate')) ?>';
</script>
<script>
var tanggalHariIni = '<?php echo date('d') . '-' . date('m') . '-' . date('Y') ?>';
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-penjualan/salesReturn.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-penjualan/salesReturnNew.js')}}"></script>
<script>
</script>
@stop