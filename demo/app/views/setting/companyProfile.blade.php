@extends('template.header-footer')

@section('title')
company
@stop

@section('css')
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<style>
    .help-block {
        margin-left: 18.5% !important;
    }
</style>
@stop

@section('nav')

@stop

@section('content')
@if(Session::get('messages') == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@if(Session::get('messages') == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Company has been updated.
</div>
@endif
@if(isset($messages))
@if($messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'gagalUpload')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> There is something wrong with upload file, Please check picture type and size.
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontentnopadd">
        <div class="primcontent"> 
            <div class="btnnest hidden-xs"> 
                <div class="btn-group bread margr30min" role="group">
                    <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                    <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                    <a href="{{route('settCompany')}}" type="button" class="btn btn-sm btn-pure">Setting Company</a>
                </div>
            </div>

            <div class="bread-xs visible-xs hidden-sm hidden-md hidden-lg">
                <p class="text-center"><a href="{{route('showDashboard')}}">{{Config::get('companyHeader.header_company');}}</a> / <a>{{ucfirst($toogle)}}</a> / <a href="{{route('settCompany')}}">Setting Company</a></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form action="" method="post" class="actioncompany" id="form-insert" enctype="multipart/form-data">
                    <div class="tabwrap cutout">
                        <div class="tabhead">
                            <h4 class="headtitle">Company</h4>
                        </div>
                        <div class="tableadd"> 
                            <div class="row paddrowcomp">
                                <div class="col-md-12 bordpictbott margbot10">
                                    <div class="photocomp">
                                        @if(Auth::user()->Company->Logo == NULL)
                                        <img src="{{Asset('/img/briefbig.png')}}" max-width="705" height="200">
                                        @else
                                        <img src="{{Asset(Auth::user()->Company->Logo)}}" max-width="705" height="200">
                                        @endif
                                    </div>
                                    <input type="file" name="Logo" class="incomp">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <input type='hidden' name='jenis' value='updateProfileCompany'>
                                    <div class="margbot10">
                                        <label for="CompanyName">Name *</label>
                                        <input class="input-theme" type="text" name="CompanyName" id="name" maxlength="200" data-vadivdation="required" value="{{Auth::user()->Company->CompanyName}}">
                                    </div>
                                    <div class="margbot10">
                                        <label for="address" >Address</label>
                                        <textarea  class="area-theme" style="resize:none;" name="Address" maxlength="1000">{{Auth::user()->Company->Address}}</textarea>
                                    </div>
                                    <div class="margbot10">
                                        <label for="region">Region</label>
                                        <select class="input-theme" class="chosen-select" id="region" style="" name="Region">
                                            @foreach(Region::all() as $region)
                                            @if($region->InternalID == Auth::user()->Company->RegionInternalID)
                                            <option selected="selected" value="{{$region->InternalID}}">
                                                {{$region->RegionName}}
                                            </option>
                                            @else
                                            <option value="{{$region->InternalID}}">
                                                {{$region->RegionName}}
                                            </option>
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="margbot10">
                                        <label for="city">City</label>
                                        <input class="input-theme" type="text" name="City" maxlength="1000" value="{{Auth::user()->Company->City}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="margbot10">
                                        <label for="email">Email</label>
                                        <input class="input-theme" type="text" name="Email" id="email" data-vadivdation="email" maxlength="200" value="{{Auth::user()->Company->Email}}">
                                    </div>
                                    <div class="margbot10">
                                        <div id="npwpin" class="npwpwrapcomp">
                                            <div class="col-xs-12 nopaddl nopaddr"><label for="taxID">TaxID</label></div>
                                            <input type="hidden" name="taxID" value="" id="taxID">
                                            <input class="wd28px autoTab numaja themed-input" type="text" name="taxID1" id="taxID1" maxlength="2"
                                                   data-validation="length" data-validation-length="min2" 
                                                   data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                            <input class="wd40px autoTab numaja themed-input" type="text" name="taxID2" id="taxID2" maxlength="3"
                                                   data-validation="length" data-validation-length="min3" 
                                                   data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                            <input class="wd40px autoTab numaja themed-input" type="text" name="taxID3" id="taxID3" maxlength="3"
                                                   data-validation="length" data-validation-length="min3" 
                                                   data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                            <input class="wd20px autoTab numaja themed-input" type="text" name="taxID4" id="taxID4" maxlength="1"
                                                   data-validation="length" data-validation-length="min1" 
                                                   data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> - 
                                            <input class="wd40px autoTab numaja themed-input" type="text" name="taxID5" id="taxID5" maxlength="3"
                                                   data-validation="length" data-validation-length="min3" 
                                                   data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information."> . 
                                            <input class="wd40px autoTab numaja themed-input" type="text" name="taxID6" id="taxID6" maxlength="3"
                                                   data-validation="length" data-validation-length="min3" 
                                                   data-validation-optional="true" data-validation-error-msg="If you fill this field please give a complete information.">
                                        </div>
                                    </div>
                                    <div class="margbot10">
                                        <label for="phone">Phone</label>
                                        <input class="input-theme" type="text" name="Phone" maxlength="200" value="{{Auth::user()->Company->Phone}}">
                                    </div>
                                    <div class="margbot10">
                                        <label for="fax">Fax</label>
                                        <input class="input-theme" type="text" name="Fax" maxlength="200" value="{{Auth::user()->Company->Fax}}">
                                    </div>
                                </div>
                                <div class="required">
                                    * Required
                                </div>
                            </div>
                        </div>
                    </div><!---- end div tabwrap---->   
                    <div class="btnnest pull-right">
                        <button class="btn btn-green btn-sm btn-save" id="btn-save"> Save </button>
                    </div>
                </form>
            </div><!---- end div tabwrap----> 
        </div>   
    </div>
</div>
</div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop
<?php
$f = rand(0, 50);
$c = myEncryptJavaScript(Company::select('Email')->get(), $f);
?>
@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script>
var taxID = '<?php echo Auth::user()->Company->TaxID; ?>';
var a = '<?php echo $c; ?>';
var b = <?php echo $f; ?>;
var emailLama = '<?php echo Auth::user()->Company->Email; ?>';
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-setting/companyProfile.js')}}"></script>
@stop