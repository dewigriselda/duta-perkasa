@extends('template.header-footer')

@section('title')
profile
@stop

@section('css')
<link rel="stylesheet" href="{{Asset('css/chosenCustom.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<style>
    #form-insert .help-block {
        margin-left: 22.5% !important;
    }
</style>
@stop

@section('nav')

@stop

@section('content')
@if(Session::get('messages') == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@if(Session::get('messages') == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> User has been updated.
</div>
@endif
@if(Session::get('messages') == 'suksesUpdatePassword')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> User password has been updated.
</div>
@endif
@if(isset($messages))
@if($messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'gagalPassword')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Old password didn't match.
</div>
@endif
@if($messages == 'accessDenied')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Access Denied!</strong> Your action is forbidden.
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontentnopadd">
        <div class="primcontent"> 
            <div class="btnnest hidden-xs"> 
                <div class="btn-group bread margr30min" role="group">
                    <a href="{{route('showDashboard')}}" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                    <a class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                    <a href="{{route('settProfile')}}" class="btn btn-sm btn-pure">Setting Profile</a>
                </div>
            </div>

            <div class="bread-xs visible-xs hidden-sm hidden-md hidden-lg">
                <p class="text-center"><a href="{{route('showDashboard')}}">{{Config::get('companyHeader.header_company');}}</a> / <a>{{ucfirst($toogle)}}</a> / <a href="{{route('settProfile')}}">Setting Profile</a></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form action="" method="post" class="actionprofile" id="form-insert" enctype="multipart/form-data">
                    <div class="tabwrap">
                        <div class="tabhead">
                            <h4 class="headtitle">Profile</h4>
                        </div>
                        <div class="tableadd"> 
                            <div class="row">
                                <div class="col-md-3 col-md-offset-1 bordpict">
                                    <div class="photo">
                                        @if(Auth::user()->Picture == NULL)
                                        <img src="{{Asset('/img/profile.png')}}" width="160" height="180">
                                        @else
                                        <img src="{{Asset(Auth::user()->Picture)}}" width="160" height="180">
                                        @endif
                                    </div> 
                                    <input type="file" name="ProfilePicture" class="margl10">
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="margbot10">
                                                <input type='hidden' name='jenis' value='updateProfile'>
                                            </div>
                                            <div class="margbot10">
                                                <label for="UserID">User ID *</label> 
                                                <span>{{Auth::user()->UserID}}</span>
                                            </div>
                                            <div class="margbot10">
                                                <label for="Name">Name *</label> 
                                                <input class="input-theme" type="text" name="Name" id="name" maxlength="200" data-validation="required" value="{{Auth::user()->UserName}}">
                                            </div>
                                            <div class="margbot10">
                                                <label for="Email">Email *</label> 
                                                <input class="input-theme" type="text" name="Email" id="email" maxlength="200" data-validation="email" value="{{Auth::user()->Email}}">
                                            </div>
                                            <div class="margbot10">
                                                <label for="Phone">Phone *</label> 
                                                <input class="input-theme" type="text" name="Phone" id="phone" maxlength="200" data-validation="required" value="{{Auth::user()->Phone}}">
                                            </div>
                                            <div class="margbot10">
                                                <label for="PasswordButton">Change Password </label> 
                                                <button data-target="#changePassword" data-toggle="modal" role="dialog" 
                                                        type='button' class="btn btn-pure-xs btn-xs">
                                                    Change Password
                                                </button>
                                            </div>
                                            <div class="margbot10">
                                                <div class="required">
                                                    * Required
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!---- end div tableadd---->  
                    </div><!---- end div tabwrap----> 
                    <div class="btnnest pull-right">
                        <button class="btn btn-green btn-sm btn-save" id="btn-save"> Save </button>
                    </div>
                </form>
            </div>   
        </div>
    </div>
</div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop
@section('modal')
<div class="modal fade" id="changePassword" role="dialog">
    <div class="modal-dialog modal-mid">
        <div class="modal-content modal-mid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Change Password</h4>
            </div>
            <div class="modal-body">
                <div class="coa-form">
                    <form action="" method="post" class="action" id="form-password">
                        <ul>
                            <input type='hidden' name='jenis' value='updatePassword'>
                            <li>
                                <label for="Password">Old Password *</label> 
                                <input type="password" name="PasswordLama" id="passwordLama" maxlength="16" data-validation="required">
                            </li>
                            <li>
                                <label for="Password">New Password *</label> 
                                <input type="password" name="Password_confirmation" id="password" maxlength="16" data-validation="required">
                            </li>
                            <li>
                                <label for="cPassword">Confirm Password *</label> 
                                <input type="password" name="Password" id="cpassword" maxlength="16" data-validation="confirmation">
                            </li>
                        </ul>
                </div>
                <div class="required">
                    * Required
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="btn-detail-report" class="btn btn-green">Submit</button>
                <button type="button" class="btn btn-pure" data-dismiss="modal">Close</button>
            </div>
            </form>      
        </div>
    </div>  
</div>
@stop
@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script src="{{Asset('js/chosenNoHide.jquery.js')}}" type="text/javascript"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<?php
$f = rand(0, 50);
$c = myEncryptJavaScript(User::select('Email')->get(), $f);
?>
<!--<script type="text/javascript" src="{{Asset('js/entry-js-master/user.js')}}"></script>-->
<script type="text/javascript">
var a = '<?php echo $c; ?>';
var b = <?php echo $f; ?>;
var emailLama = '<?php echo Auth::user()->Email; ?>';
</script>
<script type="text/javascript" src="{{Asset('js/entry-js-setting/profile.js')}}"></script>
@stop