<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Salmon Accounting</title>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400' rel='stylesheet' type='text/css'>
        <link rel="shortcut icon" href="{{Asset('fav.ico')}}" type="image/x-icon" />
        <link rel="stylesheet" href="{{Asset('lib/bootstrap/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{Asset('css/jquery.mCustomScrollbar.css')}}" />
        <link rel="stylesheet" href="{{Asset('css/kustom.css')}}">
        <script src="{{Asset('lib/bootstrap/js/jquery-1.11.1.min.js')}}"></script>
        <script src="{{Asset('lib/bootstrap/js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{Asset('js/jquery-ui.js')}}"></script>
        <script src="{{Asset('lib/bootstrap/js/jquery-ui.min.js')}}"></script>
        <script src="{{Asset('lib/bootstrap/js/jquery.easing.min.js')}}"></script>
        @yield('css')
    </head>
    <body>
        <div class="top-nav">
            <div class="logo">
                <h4>{{Config::get('companyHeader.header_company');}} Accounting</h4>
            </div> 
            <p id="logout" >Hello {{Auth::user()->UserName.' '.Auth::user()->Company->CompanyName;}} |
                @if(Auth::user()->CompanyInternalID != -1)
                <a href="{{Route('showContactAgent')}}">Contact Us</a> | 
                @endif
                <a href="{{Route('logout')}}"><span class="glyphicon glyphicon-lock"></span> Logout</a></p>
        </div>
        <div class="side-nav" style="padding-bottom: 90px;">
            <ul class="side-menu">
                <li class="agent"style="font-weight: 700;font-size: 15px; cursor: pointer; padding-left: 10px !important;">Agent <span class="caret"></span></li>
                <ul class="agentchild" > 
                    <a href="{{Route('showHomeAgent')}}">
                        <li class="<?php if ($aktif == 'homeAgent') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-tasks"></span> Company </p></li></a>
                    <a href="{{Route('showCommissionAgent')}}">
                        <li class="<?php if ($aktif == 'commissionAgent') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-tasks"></span> Commission </p></li></a>
                    <a href="{{Route('showWithdrawAgent')}}">
                        <li class="<?php if ($aktif == 'withdrawAgent') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-tasks"></span> Withdraw </p></li></a>
                    <a href="{{Route('showProfileAgent')}}">
                        <li class="<?php if ($aktif == 'profileAgent') echo 'li-aktif' ?>"><p><span class="glyphicon glyphicon-tasks"></span> Profile </p></li></a>
                </ul>
            </ul>
        </div>
        <div class="top-nav2">
            @yield('nav')
            <button type="button" class="btn btn-sm btn-green hamb">
                <span class="glyphicon glyphicon-menu-hamburger"></span>
            </button>
        </div>
    </div>
    <div class="wrap">
        @yield('content')
    </div>
    @yield('modal')

    <script src="{{Asset('lib/bootstrap/js/jquery.easing.min.js')}}"></script>
    <script type="text/javascript" src="{{Asset('js/jquery-ui.js')}}"></script>
    <script src="{{Asset('lib/bootstrap/js/jquery-ui.min.js')}}"></script>
    <script src="{{Asset('js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    <script>
var us = '<?php echo Auth::user()->Company->InternalID ?>';
var toogle = '<?php echo $toogle; ?>';
    </script>
    <script src="{{Asset('js/template-agent.js')}}"></script>
    @yield('js')
</body>
</html>