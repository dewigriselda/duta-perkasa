@extends('template.header-footer')

@section('title')
Transfer
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{Asset('lib/bootstrap/css/jquery.dataTables.css')}}">
<link rel="stylesheet" href="{{Asset('css/chosen.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery.appendGrid-1.5.1.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{Asset('css/jquery-ui.structure.min.css')}}">
<link rel="styl esheet" href="{{Asset('css/jquery-ui.theme.min.css')}}">
<style>
    .help-block {
        margin-left: 30% !important;
    }
</style>
@stop

@section('nav')

@stop

@section('content')
@if(isset($messages))
@if($messages == 'gagalUpdate')
<div class="alert alert-danger alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Failed!</strong> Data entry error occurred.
    @if(isset($error))
    <ul style="margin-left: 2%">
        @foreach($error->all('<li>:message</li>') as $message)
        {{$message}}
        @endforeach
    </ul>
    @endif
</div>
@endif
@if($messages == 'suksesUpdate')
<div class="alert alert-success alert-dismissible" role="alert" style="width: 98%; margin: 1%">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Transfer has been updated.
</div>
@endif
@endif
<div class="wrapjour">
    <div class="primcontent">
        <div class="btnnest"> 
            <div class="btn-group bread" role="group">
                <a href="{{route('showDashboard')}}" type="button" class="btn btn-sm btn-pure bread-arrow">{{Config::get('companyHeader.header_company');}}</a>
                <a type="button" class="btn btn-sm btn-pure bread-arrow">{{ucfirst($toogle)}}</a>
                <a href="{{route('showTransfer')}}" type="button" class="btn btn-sm btn-pure bread-arrow">Transfer</a>
                <a href="{{route('transferUpdate',$header->TransferID)}}" type="button" class="btn btn-sm btn-pure">Update {{$header->TransferID}}</a>
            </div>
            <div class="btn-group margr5">
                <a href="{{Route('transferNew')}}">
                    <button type="button" class="btn btn-green btn-sm dropdown-toggle " aria-expanded="false"><span class="glyphicon glyphicon-plus"></span>New </button>
                </a>
            </div>
        </div>
        <form method="POST" action="">
            <input type='hidden' name='TransferInternalID' value='{{$header->InternalID}}'>
            <div class="tabwrap">
                <div class="tabhead">
                    <h4 class="headtitle">Transfer <span id="transferID">{{$header->TransferID}}</span></h4>
                </div>
                <div class="tableadd"> 
                    <div class="headinv new">
                        <ul class="pull-left">
                            
                            <li>
                                <label for="warehouseSource">Source *</label>
                                <select class="chosen-select choosen-modal warehouse" id="warehouseHeader" name="warehouseSource">
                                    @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $war)
                                    @if($war->InternalID == $header->WarehouseInternalID)
                                    <option selected="selected" id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                        {{$war->WarehouseName;}}
                                    </option>
                                    @else
                                    <option id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                        {{$war->WarehouseName;}}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <label for="warehouseDestiny">Destiny *</label>
                                <select class="chosen-select choosen-modal warehouse" id="warehouseHeader" name="warehouseDestiny">
                                    @foreach(Warehouse::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $war)
                                    @if($war->InternalID == $header->WarehouseDestinyInternalID)
                                    <option selected="selected" id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                        {{$war->WarehouseName;}}
                                    </option>
                                    @else
                                    <option id="war{{$war->InternalID}}" value="{{$war->InternalID}}">
                                        {{$war->WarehouseName;}}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>
                            </li>
                        </ul>
                        <ul class="pull-right">
                            <li>
                                <label for="remark">Remark *</label>
                                <textarea name="remark" id="remark" data-validation="required">{{$header->Remark}}</textarea>
                            </li>
                            <li>
                                <div class="required">
                                    * Required
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="tableadd journupdate">
                        <table class="table master-data" id="table-transfer">
                            <thead>
                                <tr>
                                    <th width="15%">Inventory</th>
                                    <th width="10%">Quantity Transfer</th>
                                    <th width="5%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $barisTerakhir = 0; ?>
                                @if(count($detail) > 0)
                                @foreach($detail as $data)
                                <tr id="row{{$barisTerakhir}}">
                                    <td class="">
                                        <select class="chosen-select choosen-modal left" id="inventory-{{$barisTerakhir}}" name="inventory[]">
                                            @foreach(Inventory::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $inventory)
                                            @if($inventory->InternalID == $data->InventoryInternalID)
                                            <option selected="selected" id="inventory{{$inventory->InternalID}}" value="{{$inventory->InternalID}}">
                                                {{$inventory->InventoryID}} 
                                                {{" ".$inventory->InventoryName}}
                                            </option>
                                            @else
                                            <option id="inventory{{$inventory->InternalID}}" value="{{$inventory->InternalID}}">
                                                {{$inventory->InventoryID}} 
                                                {{" ".$inventory->InventoryName}}
                                            </option>
                                            @endif
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="text-right">
                                        <input type="number" class="maxWidth qty right" name="qty[]" maxlength="11" min="1" id="price-{{$barisTerakhir}}-qty" value="{{$data->Qty}}">
                                    </td>
                                    <td>
                                        @if($barisTerakhir == 0)
                                        -
                                        @else
                                        <button class="btn btn-pure-xs btn-xs btn-deleteRow" type="button" data="row{{$barisTerakhir}}"><span class="glyphicon glyphicon-trash"></span></button>
                                        @endif
                                    </td>
                                </tr>
                                {{'';$barisTerakhir++;}}
                                @endforeach
                                @else
                                <tr id="row0">
                                    <td class="">
                                        <select class="chosen-select choosen-modal left" id="inventory-0" name="inventory[]">
                                            @foreach(Inventory::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $inventory)
                                            <option id="inventory{{$inventory->InternalID}}" value="{{$inventory->InternalID}}">
                                                {{$inventory->InventoryID}} 
                                                {{" ".$inventory->InventoryName}}
                                            </option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="text-right">
                                        <input type="number" class="maxWidth qty right" name="qty[]" maxlength="11" min="1" id="price-0-qty" value="1">
                                    </td>
                                    <td>
                                        -
                                    </td>
                                </tr>
                                {{'';$barisTerakhir++;}}
                                @endif
                            </tbody>
                        </table>
                        <button type="button" class="btn btn-green btn-sm" id="btn-addRow"><span class="glyphicon glyphicon-plus"></span> Add row</button>
                    </div><!---- end div tableadd---->      
                </div><!---- end div tableadd---->   
            </div><!---- end div tabwrap---->  
            <div class="btnnest pull-right">
                <button class="btn btn-green btn-sm btn-save" id="btn-save"> Save </button>
            </div>
        </form>
    </div><!---- end div primcontent--->
</div><!---- end div wrapjour---->
@stop

@section('js')
<script type="text/javascript" src="{{Asset('js/searchJs.js')}}"></script>
<script type="text/javascript" src="{{Asset('lib/bootstrap/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/jquery.appendGrid-1.5.1.js')}}"></script>
<script src="{{Asset('js/chosenNewJournal.jquery.js')}}" type="text/javascript"></script>
<script>
var textSelect = '<select class="chosen-select choosen-modal" id="" style="" name="inventory[]">' +
<?php foreach (Inventory::where('CompanyInternalID', Auth::user()->Company->InternalID)->get() as $inventory) { ?> '<option id="inventory<?php echo $inventory->InternalID ?>" value="<?php echo $inventory->InternalID ?>">' +
            '<?php echo $inventory->InventoryID . " " . $inventory->InventoryName ?>' + '</option>' +<?php } ?>
'</select>';
var baris = '<?php echo $barisTerakhir; ?>';
</script>
<script type="text/javascript" src="{{Asset('js/formatUang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/keyPressuang.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-transfer/transfer.js')}}"></script>
<script type="text/javascript" src="{{Asset('js/entry-js-transfer/transferUpdate.js')}}"></script>
<script src="{{Asset('jquery-validation/form-validator/jquery.form-validator.js')}}"></script>
<script>
$.validate();
</script>
@stop