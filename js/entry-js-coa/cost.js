$(document).ready(function () {
    $(".btn-edit").click(function () {
        var data = $(this).data('all');
        data = decryptDataID(data, b);
        $('#nameUpdate').val(searchData("CostName", data));
        $('#remarkUpdate').val(searchData("Remark", data));
        $('#idUpdate').val(searchData("InternalID", data));
        document.getElementById('createdDetail').innerHTML = searchData("UserRecord", data) + " " + searchData("dtRecordformat", data);
        if (searchData("UserModified", data) == "0") {
            document.getElementById('modifiedDetail').innerHTML = '-';
        } else {
            document.getElementById('modifiedDetail').innerHTML = searchData("UserModified", data) + " " + searchData("dtModifformat", data);
        }

        $("#type" + searchData("Flag", data)).attr('selected', 'selected');
        $("#coa" + searchData("coaID", data)).attr('selected', 'selected');
        $("#cur" + searchData("CurrencyInternalID", data)).attr('selected', 'selected');

        $('#coaUpdate').trigger("chosen:updated");
        $('#currencyUpdate').trigger("chosen:updated");
    });
    $(".btn-delete").click(function () {
        $('#idDelete').val($(this).data('internal'));
    });
    $('#startDateReport').datepicker();
    $('#endDateReport').datepicker();
    $("#startDateReport").datepicker("option", "dateFormat", 'dd-mm-yy');
    $("#endDateReport").datepicker("option", "dateFormat", 'dd-mm-yy');
    $('#endDateReport, #startDateReport').change(function () {
        if ($('#startDateReport').val() == '') {
            $('#startDateReport').val($('#endDateReport').val());
        } else if ($('#endDateReport').val() == '') {
            $('#endDateReport').val($('#startDateReport').val());
        } else if (dateCheckHigher($('#startDateReport').val(), $('#endDateReport').val()) == 'start') {
            $('#endDateReport').val($('#startDateReport').val());
        }
    });
    $("#btn-rCostMU").click(function () {
        $('#jenisReport').val('costMU');
        document.getElementById('titleReport').innerHTML = 'Cost Report MU';
    });
    $("#btn-rCost").click(function () {
        $('#jenisReport').val('cost');
        document.getElementById('titleReport').innerHTML = 'Cost Report';
    });
    $("#btn-report-transaction").click(function () {
        if ($('#startDateReport').val() == '' && $('#endDateReport').val() == '') {
            var tanggal = new Date();
            var tanggalText = tanggal.getDate() + '-' + (tanggal.getMonth() + 1) + '-' + tanggal.getFullYear();
            $('#startDateReport').val(tanggalText);
            $('#endDateReport').val($('#startDateReport').val());
        } else if ($('#startDateReport').val() == '') {
            $('#startDateReport').val($('#endDateReport').val());
        } else if ($('#endDateReport').val() == '') {
            $('#endDateReport').val($('#startDateReport').val());
        } else if (dateCheckHigher($('#startDateReport').val(), $('#endDateReport').val()) == 'start') {
            $('#endDateReport').val($('#startDateReport').val());
        }
    });
    $('#example').dataTable({
        columnDefs: [{
                targets: [0],
                orderData: [0, 1]
            }, {
                targets: [1],
                orderData: [1, 0]
            }, {
                targets: [4],
                orderData: [4, 0]
            }]
    });
});
function gagal() {
    if ($('#spanErrorID')) {
        $('#spanErrorID').remove();
    }
    $('#costID').parent('div').append('<span class="help-block form-error" id="spanErrorID">Cost ID has already been taken</span>');
    $('#costID').parent('div').removeClass('has-success');
    $('#costID').parent('div').addClass('has-error');
    $('#costID').css("border-color", "rgb(169, 68, 66)");
}
function sukses() {
    $('#spanErrorID').remove();
    $('#costID').parent('div').removeClass('has-error');
    $('#costID').parent('div').addClass('has-success');
    $('#costID').css("border-color", "");
}
function validationID(dataID, status, r) {
    dataID = decryptDataID(dataID, r);
    var search = $('#costID').val();
    if (search == '') {
        return false;
    } else if (searchID(search, dataID, 'CostID')) {
        gagal();
        if (status == 1) {
            return false; // Will stop the submission of the form
        }
    } else {
        sukses();
        if (status == 1) {
            return true; //form submit
        }
    }
}
$.validate({
    form: '#form-update'
});

var config = {
    '.chosen-select': {}
};
for (var selector in config) {
    $(selector).chosen(config[selector]);
}
$.validate({
    form: '#form-insert',
    onError: function () {
        var dataID = a;
        validationID(dataID, 1, b);
    },
    onSuccess: function () {
        var dataID = a;
        return validationID(dataID, 1, b);
    }
});
$("#costID").blur(function () {
    var dataID = a;
    validationID(dataID, 0, b);
});