var config = {'.chosen-select': {}};
for (var selector in config) {
    $(selector).chosen({
        search_contains: true
    });
}
$(document).ready(function () {
    $("#usetax").change(function () {
        if (this.checked) {
            var topup = parseFloat(removePeriod($("#downpayment").val(), ','));
            topup2 = Math.round(topup * 1.1);
            $("#grandTotalValue").val(topup);
            $("#downpayment").val(addPeriod(topup2, ','));
        } else {
            var topup2 = parseFloat(removePeriod($("#downpayment").val(), ','));
            topup = Math.round(topup2 / 1.1);
            $("#grandTotalValue").val(topup);
            $("#downpayment").val(addPeriod(topup, ','));
        }
    });
    $('#paymentType').change(function () {
        if ($('#paymentType').val() == '1') {
            $('.salesorder').hide();
            $('.totalimpor').hide();
            $('.purchaseorder').hide();
            $('#amount').empty();
        } else {
            if ($('#customerTopup').is(':checked') == true) {
                $('.salesorder').show();
            } else {
                $('.purchaseorder').show();

                var supplierID = $("#supplierID").val();
                $.post(checkPO, {supplier: supplierID, cash: $('#paymentType').val()}).done(function (data) {
                    var html1 = ''
                    $.each(data, function (index, val) {
                        var date = new Date(val['PurchaseOrderDate']);
                        html1 += "<option value='" + val['InternalID'] + "'>" + val['PurchaseOrderID'] + "|" + date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear() + "</option>";
                    });
                    $("#purchaseOrderID").html(html1);
                    if ($("#purchaseOrderID").val() != null) {
                        $.post(checkImportPO, {po: $("#purchaseOrderID").val()}).done(function (impor) {
                            if (impor == "lokal") {
                                $('.totalimpor').hide();
                            } else {
                                $('.totalimpor').show();
                            }
                        });
                    }
                    if ($('#paymentType').val() == 0) {
                        if ($("#purchaseOrderID").val() === null) {
                        } else {
                            changeAmountPurchase();
                        }
                    } else if ($('#paymentType').val() == 2) {
                        changeAmountDPPurchase();
                    }
                });
            }
        }

    });

    var customerID = $("#customerID").val();
    $.post(checkSO, {customer: customerID, cash: $('#paymentType').val()}).done(function (data) {
        var html1 = ''
        $.each(data, function (index, val) {
            var date = new Date(val['SalesOrderDate']);
            html1 += "<option value='" + val['InternalID'] + "'>" + val['SalesOrderID'] + "|" + date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear() + "</option>";
        });
        $("#salesOrderID").html(html1);
//        if ($("#paymentTypeUpdate").val() == 0 || $("#paymentTypeUpdate").val() == 2) {
//            if ($("#salesOrderID").val() !== null) {
//                $.post(getSODP, {InternalID: $("#salesOrderID").val()}).done(function (data) {
//                    $("#grandTotal").val(Math.ceil(data));
//                });
//            }
//        }
        if ($('#paymentType').val() == 0) {
            if ($("#salesOrderID").val() === null) {

            } else {
                console.log($("#salesOrderID").val());
                changeAmountSales();
            }
//                $('.cbd').show();
        } else if ($('#paymentType').val() == 2) {
            changeAmountDPSales();
//                $('.cbd').hide();
        }
    });
    $("#customerID").change(function () {
        $.post(checkSO, {customer: $(this).val(), cash: $('#paymentType').val()}).done(function (data) {
//           console.log(data);
            var html1 = ''
            $.each(data, function (index, val) {
                var date = new Date(val['SalesOrderDate']);
                html1 += "<option value='" + val['InternalID'] + "'>" + val['SalesOrderID'] + "|" + date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear() + "</option>";
            });
            $("#salesOrderID").html(html1);
            if ($('#paymentType').val() == 0) {
                if ($("#salesOrderID").val() === null) {

                } else {
                    console.log($("#salesOrderID").val());
                    changeAmountSales();
                }
//                $('.cbd').show();
            } else if ($('#paymentType').val() == 2) {
                changeAmountDPSales();
//                $('.cbd').hide();
            }
        });
    });

    $("#supplierID").change(function () {
        $.post(checkPO, {supplier: $(this).val(), cash: $('#paymentType').val()}).done(function (data) {
//           console.log(data);
            var html1 = ''
            $.each(data, function (index, val) {
                var date = new Date(val['PurchaseOrderDate']);
                html1 += "<option value='" + val['InternalID'] + "'>" + val['PurchaseOrderID'] + "|" + date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear() + "</option>";
            });
            $("#purchaseOrderID").html(html1);

            if ($("#purchaseOrderID").val() != null) {
                $.post(checkImportPO, {po: $("#purchaseOrderID").val()}).done(function (impor) {
                    if (impor == "lokal") {
                        $('.totalimpor').hide();
                    } else {
                        $('.totalimpor').show();
                    }
                });
            }
            if ($('#paymentType').val() == 0) {
                if ($("#purchaseOrderID").val() === null) {

                } else {
                    changeAmountPurchase();
                }
            } else if ($('#paymentType').val() == 2) {
                changeAmountDPPurchase();
            }
        });
    });

    $("#paymentType").change(function () {
        if ($(this).val() == '0' || $(this).val() == '2') {
            if ($('#customerTopup').is(':checked') == true) {
                $.post(checkSO, {customer: $("#customerID").val(), cash: $(this).val()}).done(function (data) {
//           console.log(data);
                    var html1 = ''
                    $.each(data, function (index, val) {
                        var date = new Date(val['SalesOrderDate']);
                        html1 += "<option value='" + val['InternalID'] + "'>" + val['SalesOrderID'] + "|" + date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear() + "</option>";
                    });
                    $("#salesOrderID").html(html1);

                    if ($('#paymentType').val() == 0) {
                        if ($("#salesOrderID").val() === null) {

                        } else {
                            console.log($("#salesOrderID").val());
                            changeAmountSales();
                        }
//                $('.cbd').show();
                    } else if ($('#paymentType').val() == 2) {
                        changeAmountDPSales();
//                $('.cbd').hide();
                    }
                });
            } else {
                $.post(checkPO, {supplier: $(this).val(), cash: $('#paymentType').val()}).done(function (data) {
//           console.log(data);
                    var html1 = ''
                    $.each(data, function (index, val) {
                        var date = new Date(val['PurchaseOrderDate']);
                        html1 += "<option value='" + val['InternalID'] + "'>" + val['PurchaseOrderID'] + "|" + date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear() + "</option>";
                    });
                    $("#purchaseOrderID").html(html1);

                    if ($("#purchaseOrderID").val() != null) {
                        $.post(checkImportPO, {po: $("#purchaseOrderID").val()}).done(function (impor) {
                            if (impor == "lokal") {
                                $('.totalimpor').hide();
                            } else {
                                $('.totalimpor').show();
                            }
                        });
                    }

                    if ($('#paymentType').val() == 0) {
                        if ($("#purchaseOrderID").val() === null) {

                        } else {
                            changeAmountPurchase();
                        }
                    } else if ($('#paymentType').val() == 2) {
                        changeAmountDPPurchase();
                    }
                });
            }
        }
    });

    $("#vat").click(function (e) {
        var vat = 0;
        if (document.getElementById("vat").checked == true) {
//        vat = 0.1 * parseFloat(removePeriod($("#downpayment").val(), ','));
            vat = 0;
        } else {
            vat = 0;
        }

        if ($("#paymentType").val() == 2) {
            var dp = parseFloat(removePeriod($("#downpayment").val(), ',')) + vat;
            console.log(dp);
            var grandTotal = parseFloat(removePeriod($("#grandTotalCek").val(), ','));
            console.log($("#grandTotalValue").val());
//        if (dp > grandTotal) {
//            alert("Down Payment exceeds Sales Order's Grand Total");
//            $(this).removeAttr('checked');
//
//        } else {
            validate();
//        }
        } else {
            validate();
        }
    });

    $("#downpayment").blur(function () {
        if ($("#paymentType").val() == 2) {
            var dp = parseFloat(removePeriod($(this).val(), ','));
            console.log(dp);
            var grandTotal = parseFloat(removePeriod($("#grandTotalCek").val(), ','));
            console.log(grandTotal);
        }
        validate();
    });

    function validate() {
        var total = document.getElementById("downpayment").value;
        var tamp = total.split(',').join('');
        if (document.getElementById("vat").checked == true) {
            var tax = parseFloat(tamp) * 0.1;
            tax = Math.floor(tax);

        } else {
            tax = 0;
        }
        console.log(tax);
        var grandTotal = parseFloat(tamp) + parseFloat(tax);
        if (document.getElementById('vat').checked) {
            grandTotal = (grandTotal * 100) / 100;
            var tax1 = (grandTotal / 1.1) * 0.1;
            grandTotal = grandTotal - tax1;
            grandTotal = Math.ceil(grandTotal);
            console.log(grandTotal);
            $(".dpAfterTax_txt").show();
            $("#grandTotalValue").val(grandTotal);
            document.getElementById("dpAfterTax_txt").innerHTML = 'Top Up Amount(Tax) :';
            document.getElementById("dpAfterTax").innerHTML = addPeriod(grandTotal, ',');

        } else {
            $("#grandTotalValue").val(tamp);
            $(".dpAfterTax_txt").show();
            document.getElementById("dpAfterTax").innerHTML = total;
            document.getElementById("dpAfterTax_txt").innerHTML = 'Top Up Amount :';
        }
    }

    function changeAmountSales() {
        if ($("#salesOrderID").val() !== null) {
            $.post(getSOAmount, {id: $("#salesOrderID").val()}).done(function (data) {
                data = parseFloat(removePeriod(data, ','));
                data = Math.ceil(data);
                data = addPeriod(data, ',');
                $("#grandTotal").val(data);
                $("#downpayment").val(data);
                $("#amount").html("(" + data + ")");
                $("#grandTotalValue").val(removePeriod(data, ','));
            });
        }
    }
    function changeAmountDPSales() {
        if ($("#salesOrderID").val() !== null) {
            $.post(getSOAmountDp, {id: $("#salesOrderID").val()}).done(function (data) {
                data = parseFloat(removePeriod(data, ','));
                data = Math.round(data);
                data = addPeriod(data, ',');
                $("#grandTotal").val(data);
                $("#grandTotalCek").val(data);
                $("#downpayment").val(data);
                $("#amount").html("(" + data + ")");
                $("#grandTotalValue").val(data);
                $("#grandTotalCek").val(data);
            });
        }
    }

    function changeAmountPurchase() {
        if ($("#purchaseOrderID").val() !== null) {
            $.post(getPOAmount, {id: $("#purchaseOrderID").val()}).done(function (data) {
                data = parseFloat(removePeriod(data, ','));
                data = Math.round(data);
                data = addPeriod(data, ',');
                $("#grandTotal").val(data);
                $("#downpayment").val(data);
                $("#amount").html("(" + data + ")");
                $("#grandTotalValue").val(removePeriod(data, ','));
            });
        }
    }
    function changeAmountDPPurchase() {
        if ($("#purchaseOrderID").val() !== null) {
            $.post(getPOAmountDp, {id: $("#purchaseOrderID").val()}).done(function (data) {
                data = parseFloat(removePeriod(data, ','));
                data = Math.ceil(data);
                data = addPeriod(data, ',');
                $("#grandTotal").val(data);
                $("#grandTotalCek").val(data);
                $("#downpayment").val(data);
                $("#amount").html("(" + data + ")");
                $("#grandTotalValue").val(data);
                $("#grandTotalCek").val(data);
            });
        }
    }

    $("#salesOrderID").change(function () {
//        $.post(getSODP, {InternalID: $(this).val()}).done(function (data) {
//            $("#grandTotal").val(data);
        if ($("#paymentType").val() == 0) {
            changeAmountSales();
        } else if ($("#paymentType").val() == 2) {
            changeAmountDPSales();
        }
//        })
    });

    $("#purchaseOrderID").change(function () {
        $.post(checkImportPO, {po: $("#purchaseOrderID").val()}).done(function (impor) {
            if (impor == "lokal") {
                $('.totalimpor').hide();
            } else {
                $('.totalimpor').show();
            }
        });

        if ($("#paymentType").val() == 0) {
            changeAmountPurchase();
        } else if ($("#paymentType").val() == 2) {
            changeAmountDPPurchase();
        }
    });

    if ($('#customerTopup').is(':checked') == true) {
        $(".customer").show();
        $(".supplier").hide();
        $('.purchaseorder').hide();
        $('.totalimpor').hide();
        if ($('#paymentType').val() == '1') {
            $('.salesorder').hide();
        } else {
            $('.salesorder').show();
            if ($('#paymentType').val() == '0') {
                $('.cbd').show();
            } else {
                $('.cbd').hide();
            }
        }
    } else {
        $(".customer").hide();
        $(".supplier").show();
        $('.salesorder').hide();
        $('.totalimpor').hide();
        if ($('#paymentType').val() == '1') {
            $('.purchaseorder').hide();
        } else {
            $('.purchaseorder').show();
            if ($('#paymentType').val() == '0') {
                $('.cbd').show();
            } else {
                $('.cbd').hide();
            }
        }
    }

    $(".autoTab2").keyup(function (e) {
        var tamp = $(this).val();
        var length = document.getElementById($(this).attr('id')).maxLength;
        if (tamp.length == length)
        {
            var inputs = $(this).closest('form').find(':input');
            inputs.eq(inputs.index(this) + 1).focus();
        }
        $('#numberTax').val($('#numberTax1').val() + "." + $('#numberTax2').val() + "-" + $('#numberTax3').val() + "." + $('#numberTax4').val());
    });
    $(".autoTab2").focus(function (e) {
        $(this).val("");
//        $('#numberTax').val($('#numberTax1').val() + "." + $('#numberTax2').val() + "-" + $('#numberTax3').val() + "." + $('#numberTax4').val());
    });
//    $(".autoTab2").keyup(function (e) {
//        var tamp = $(this).val();
//        var length = document.getElementById($(this).attr('id')).maxLength;
//        if (tamp.length == length)
//        {
//            var inputs = $(this).closest('form').find(':input');
//            inputs.eq(inputs.index(this) + 1).focus();
//        }
//        $('#numberTaxUpdate').val($('#numberTax1Update').val() + "." + $('#numberTax2Update').val() + "-" + $('#numberTax3Update').val() + "." + $('#numberTax4Update').val());
//    });

    $("#customerTopup,#supplierTopup").change(function () {
        if ($('#customerTopup').is(':checked') == true) {

//            $('#supplierTopup').removeAttr('checked');
//            $('#customerTopup').attr('checked', 'checked');
            $(".customer").show();
            $(".supplier").hide();

        } else {
//            $('#customerTopup').removeAttr('checked');
//            $('#supplierTopup').attr('checked', 'checked');
            $(".customer").hide();
            $(".supplier").show();
        }
    });

    $('#date').datepicker();
    $("#date").datepicker("option", "dateFormat", 'dd-mm-yy');
    $('#date').val(tanggalHariIni);

    var currency = $("#currencyHeader").val().split('---;---');
    if (currency[3] == '1') {
        $("#rate").prop('readonly', true);
        $("#rate").css('background-color', '#eee');
    } else {
        $("#rate").prop('readonly', false);
        $("#rate").css('background-color', '');
    }

    $("#currencyHeader").change(function () {
        var currencyHeader = $("#currencyHeader").val().split('---;---');
        $("#rate").val(addPeriod(currencyHeader[2], ','));
        var rate = $("#rate").val();
//        $(".price").each(function (i) {
//            changeTotal($(this).attr('id'))
//        });
        var currency = $("#currencyHeader").val().split('---;---');
        if (currency[3] == '1') {
            $("#rate").prop('readonly', true);
            $("#rate").css('background-color', '#eee');
        } else {
            $("#rate").prop('readonly', false);
            $("#rate").css('background-color', '');
        }
    });

    $("#selisihbayar").change(function () {
        console.log(parseFloat($('#grandTotalValue').val()));
        console.log(parseFloat($('#selisihbayar').val()));
        console.log(parseFloat(removePeriod($('#grandTotal').val(), ",")));
        if ((parseFloat(removePeriod($('#grandTotalValue').val(), ",")) + parseFloat(removePeriod($('#selisihbayar').val(), ","))) == parseFloat(removePeriod($('#grandTotal').val(), ","))) {
            $("#btn-save").removeAttr('disabled');
        } else {
            alert('Pembayaran yang dimasukkan dan Jumlah yang dibutuhkan tidak sesuai.');
            $("#btn-save").attr('disabled', 'disabled');
        }
    });
});