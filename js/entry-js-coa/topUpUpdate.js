var config = {'.chosen-select': {}};
for (var selector in config) {
    $(selector).chosen({
        search_contains: true
    });
}
$(document).ready(function () {
    $("#usetax").change(function () {
        if (this.checked) {
            var topup = parseFloat(removePeriod($("#downpayment").val(), ','));
            topup2 = topup * 1.1;
            $("#grandTotalValue").val(topup);
            $("#downpayment").val(addPeriod(topup2, ','));
        } else {
            var topup2 = parseFloat(removePeriod($("#downpayment").val(), ','));
            topup = topup2 / 1.1;
            $("#grandTotalValue").val(topup);
            $("#downpayment").val(addPeriod(topup, ','));
        }
    });
    if (taxx.trim() != 'null') {
        if (taxx != ".-.") {
            console.log(taxx);
            $('#numberTax1').val((taxx).substring(0, 3));
            $('#numberTax2').val((taxx).substring(4, 7));
            $('#numberTax3').val((taxx).substring(8, 10));
            $('#numberTax4').val((taxx).substring(11, 19));
        } else {
            $('#numberTax1').val("");
            $('#numberTax2').val("");
            $('#numberTax3').val("");
            $('#numberTax4').val("");
        }
    } else {
        $('#numberTax1').val("");
        $('#numberTax2').val("");
        $('#numberTax3').val("");
        $('#numberTax4').val("");
    }

    $('#paymentType').change(function () {
        if ($('#paymentType').val() == '1') {
            $('.salesorder').hide();
            $('.purchaseorder').hide();
            $('#amount').empty();
        } else {
            if ($('#customerTopup').is(':checked') == true) {
                $('.salesorder').show();
            } else {
                $('.purchaseorder').show();
            }
        }
    });

    var customerID = $("#customerID").val();
    $.post(checkSO, {customer: customerID, cash: $('#paymentType').val()}).done(function (data) {
        var html1 = ''
        $.each(data, function (index, val) {
            var date = new Date(val['SalesOrderDate']);
            html1 += "<option value='" + val['InternalID'] + "'>" + val['SalesOrderID'] + "|" + date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear() + "</option>";
        });
        $("#salesOrderID").html(html1);
//        if ($("#paymentTypeUpdate").val() == 0 || $("#paymentTypeUpdate").val() == 2) {
//            if ($("#salesOrderID").val() !== null) {
//                $.post(getSODP, {InternalID: $("#salesOrderID").val()}).done(function (data) {
//                    $("#grandTotal").val(Math.ceil(data));
//                });
//            }
//        }
        if ($('#paymentType').val() == 0) {
            if ($("#salesOrderID").val() === null) {

            } else {
                console.log($("#salesOrderID").val());
                changeAmountSales();
            }
//                $('.cbd').show();
        } else if ($('#paymentType').val() == 2) {
            changeAmountDPSalesUpdate();
//                $('.cbd').hide();
        }
    });
    var supplierID = $("#supplierID").val();
    $.post(checkPO, {supplier: supplierID, cash: $('#paymentType').val()}).done(function (data) {
        var html1 = ''
        $.each(data, function (index, val) {
            var date = new Date(val['PurchaseOrderDate']);
            html1 += "<option value='" + val['InternalID'] + "'>" + val['PurchaseOrderID'] + "|" + date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear() + "</option>";
        });
        $("#purchaseOrderID").html(html1);
//        if ($("#paymentTypeUpdate").val() == 0 || $("#paymentTypeUpdate").val() == 2) {
//            if ($("#salesOrderID").val() !== null) {
//                $.post(getSODP, {InternalID: $("#salesOrderID").val()}).done(function (data) {
//                    $("#grandTotal").val(Math.ceil(data));
//                });
//            }
//        }
        if ($('#paymentType').val() == 0) {
            if ($("#purchaseOrderID").val() === null) {

            } else {
                console.log($("#purchaseOrderID").val());
                changeAmountPurchase();
            }
//                $('.cbd').show();
        } else if ($('#paymentType').val() == 2) {
            changeAmountDPPurchaseUpdate();
//                $('.cbd').hide();
        }
    });
//    if ($('#paymentType').val() == 0) {
////        if ($("#salesOrderID").val() === null) {
////
////        } else {
////            console.log($("#salesOrderID").val());
//        changeAmountSales();
////        }
////                $('.cbd').show();
//    } else if ($('#paymentType').val() == 2) {
//        changeAmountDPSales();
////                $('.cbd').hide();
//    }
//    $("#customerID").change(function () {
//        $.post(checkSO, {customer: $(this).val(), cash: $('#paymentType').val()}).done(function (data) {
////           console.log(data);
//            var html1 = ''
//            $.each(data, function (index, val) {
//                var date = new Date(val['SalesOrderDate']);
//                if (salesorder == val['InternalID']) {
//                    html1 += "<option selected value='" + val['InternalID'] + "'>" + val['SalesOrderID'] + "|" + date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear() + "</option>";
//                } else {
//                    html1 += "<option value='" + val['InternalID'] + "'>" + val['SalesOrderID'] + "|" + date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear() + "</option>";
//                }
//            });
//            $("#salesOrderID").html(html1);
//            if ($('#paymentType').val() == 0) {
//                if ($("#salesOrderID").val() === null) {
//
//                } else {
//                    console.log($("#salesOrderID").val());
//                    changeAmountSales();
//                }
////                $('.cbd').show();
//            } else if ($('#paymentType').val() == 2) {
//                changeAmountDPSales();
////                $('.cbd').hide();
//            }
//        });
//    });

    $("#paymentType").change(function () {
        if ($(this).val() == '0' || $(this).val() == '2') {
            $.post(checkSO, {customer: $("#customerID").val(), cash: $(this).val()}).done(function (data) {
//           console.log(data);
                var html1 = ''
                $.each(data, function (index, val) {
                    var date = new Date(val['SalesOrderDate']);
                    html1 += "<option value='" + val['InternalID'] + "'>" + val['SalesOrderID'] + "|" + date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear() + "</option>";
                });
                $("#salesOrderID").html(html1);

                if ($('#paymentType').val() == 0) {
                    if ($("#salesOrderID").val() === null) {

                    } else {
                        console.log($("#salesOrderID").val());
                        changeAmountSales();
                    }
//                $('.cbd').show();
                } else if ($('#paymentType').val() == 2) {
                    changeAmountDPSales();
//                $('.cbd').hide();
                }
            });
        }
    });

    function changeAmountSales() {
        if ($("#salesOrderID").val() !== null) {
            $.post(getSOAmount, {id: $("#salesOrderID").val(), topup: $("#InternalID").val()}).done(function (data) {
                data = parseFloat(removePeriod(data, ','));
                data = Math.ceil(data);
                data = addPeriod(data, ',');
                $("#grandTotal").val(data);
                $("#downpayment").val(data);
                $("#amount").html("(" + data + ")");
                $("#grandTotalValue").val(removePeriod(data, ','));
            });
        }
    }
    function changeAmountDPSales() {
        if ($("#salesOrderID").val() !== null) {
            $.post(getSOAmountDp, {id: $("#salesOrderID").val(), topup: $("#InternalID").val()}).done(function (data) {
                data = parseFloat(removePeriod(data, ','));
                data = Math.round(data);
                data = addPeriod(data, ',');
                $("#grandTotal").val(data);
                $("#grandTotalCek").val(data);
                $("#downpayment").val(data);
                $("#amount").html("(" + data + ")");
                $("#grandTotalValue").val(data);
                $("#grandTotalCek").val(data);
            });
        }
    }
    function changeAmountDPSalesUpdate() {
        if ($("#salesOrderID").val() !== null) {
            $.post(getSOAmountDp, {id: $("#salesOrderID").val(), topup: $("#InternalID").val()}).done(function (data) {
                data = parseFloat(removePeriod(data, ','));
                data = Math.round(data);
                data = addPeriod(data, ',');
//                $("#grandTotal").val(data);
                $("#grandTotalCek").val(data);
//                $("#downpayment").val(data);
                $("#amount").html("(" + data + ")");
                $("#grandTotalValue").val(data);
                $("#grandTotalCek").val(data);
            });
        }
    }
    
    function changeAmountPurchase() {
        if ($("#purchaseOrderID").val() !== null) {
            $.post(getPOAmount, {id: $("#purchaseOrderID").val(), topup: $("#InternalID").val()}).done(function (data) {
                data = parseFloat(removePeriod(data, ','));
                data = Math.round(data);
                data = addPeriod(data, ',');
                $("#grandTotal").val(data);
                $("#downpayment").val(data);
                $("#amount").html("(" + data + ")");
                $("#grandTotalValue").val(removePeriod(data, ','));
            });
        }
    }
    function changeAmountDPPurchase() {
        if ($("#purchaseOrderID").val() !== null) {
            $.post(getPOAmountDp, {id: $("#purchaseOrderID").val(), topup: $("#InternalID").val()}).done(function (data) {
                data = parseFloat(removePeriod(data, ','));
                data = Math.round(data);
                data = addPeriod(data, ',');
                $("#grandTotal").val(data);
                $("#grandTotalCek").val(data);
                $("#downpayment").val(data);
                $("#amount").html("(" + data + ")");
                $("#grandTotalValue").val(data);
                $("#grandTotalCek").val(data);
            });
        }
    }
    function changeAmountDPPurchaseUpdate() {
        if ($("#purchaseOrderID").val() !== null) {
            $.post(getPOAmountDp, {id: $("#purchaseOrderID").val(), topup: $("#InternalID").val()}).done(function (data) {
                data = parseFloat(removePeriod(data, ','));
                data = Math.ceil(data);
                data = addPeriod(data, ',');
//                $("#grandTotal").val(data);
                $("#grandTotalCek").val(data);
//                $("#downpayment").val(data);
                $("#amount").html("(" + data + ")");
                $("#grandTotalValue").val(data);
                $("#grandTotalCek").val(data);
            });
        }
    }

    $("#salesOrderID").change(function () {
//        $.post(getSODP, {InternalID: $(this).val()}).done(function (data) {
//            $("#grandTotal").val(data);
        if ($("#paymentType").val() == 0) {
            changeAmountSales();
        } else if ($("#paymentType").val() == 2) {
            changeAmountDPSales();
        }
//        })
    });

    if (tipe == "customer") {
        $(".customer").show();
        $(".supplier").hide();
        $('.purchaseorder').hide();
        if ($('#paymentType').val() == '1') {
            $('.salesorder').hide();
        } else {
            $('.salesorder').show();
            if ($('#paymentType').val() == '0') {
                $('.cbd').show();
            } else {
                $('.cbd').hide();
            }
        }
    } else {
        $(".customer").hide();
        $(".supplier").show();
        $('.salesorder').hide();
        if ($('#paymentType').val() == '1') {
            $('.purchaseorder').hide();
        } else {
            $('.purchaseorder').show();
            if ($('#paymentType').val() == '0') {
                $('.cbd').show();
            } else {
                $('.cbd').hide();
            }
        }
    }

    $(".autoTab2").keyup(function (e) {
        var tamp = $(this).val();
        var length = document.getElementById($(this).attr('id')).maxLength;
        if (tamp.length == length)
        {
            var inputs = $(this).closest('form').find(':input');
            inputs.eq(inputs.index(this) + 1).focus();
        }
        $('#numberTax').val($('#numberTax1').val() + "." + $('#numberTax2').val() + "-" + $('#numberTax3').val() + "." + $('#numberTax4').val());
    });
    $(".autoTab2").focus(function (e) {
        $(this).val("");
//        $('#numberTax').val($('#numberTax1').val() + "." + $('#numberTax2').val() + "-" + $('#numberTax3').val() + "." + $('#numberTax4').val());
    });
//    $(".autoTab2").keyup(function (e) {
//        var tamp = $(this).val();
//        var length = document.getElementById($(this).attr('id')).maxLength;
//        if (tamp.length == length)
//        {
//            var inputs = $(this).closest('form').find(':input');
//            inputs.eq(inputs.index(this) + 1).focus();
//        }
//        $('#numberTaxUpdate').val($('#numberTax1Update').val() + "." + $('#numberTax2Update').val() + "-" + $('#numberTax3Update').val() + "." + $('#numberTax4Update').val());
//    });

    $("#customerTopup,#supplierTopup").change(function () {
        if ($('#customerTopup').is(':checked') == true) {

//            $('#supplierTopup').removeAttr('checked');
//            $('#customerTopup').attr('checked', 'checked');
            $(".customer").show();
            $(".supplier").hide();

        } else {
//            $('#customerTopup').removeAttr('checked');
//            $('#supplierTopup').attr('checked', 'checked');
            $(".customer").hide();
            $(".supplier").show();
        }
    });

    $('#date').datepicker();
    $("#date").datepicker("option", "dateFormat", 'dd-mm-yy');
    $('#date').val(tanggalHariIni);

    var currency = $("#currencyHeader").val().split('---;---');
    if (currency[3] == '1') {
        $("#rate").prop('readonly', true);
        $("#rate").css('background-color', '#eee');
    } else {
        $("#rate").prop('readonly', false);
        $("#rate").css('background-color', '');
    }

    $("#currencyHeader").change(function () {
        var currencyHeader = $("#currencyHeader").val().split('---;---');
        $("#rate").val(addPeriod(currencyHeader[2], ','));
        var rate = $("#rate").val();
//        $(".price").each(function (i) {
//            changeTotal($(this).attr('id'))
//        });
        var currency = $("#currencyHeader").val().split('---;---');
        if (currency[3] == '1') {
            $("#rate").prop('readonly', true);
            $("#rate").css('background-color', '#eee');
        } else {
            $("#rate").prop('readonly', false);
            $("#rate").css('background-color', '');
        }
    });
});