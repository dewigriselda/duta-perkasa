$(document).ready(function () {
    //=================function=====================
    //====================================function====================================
    $("#btn-addRow").removeAttr("disabled");

    var dataUom;
    //get uom
    $.post(getUomThisInventory, {id: $('#inventory-0').val()}).done(function (data2) {
        $("#uom-0").html(data2);
        dataUom = data2;
    });
    $.post(getStockInventorySource, {InventoryInternalID: $("#inventory-0").val(), warehouse: $("#warehouseHeaderSource").val()}).done(function (stock) {
        var split = stock.split("---;---");
        $("#price-0-stocksource").html(split[0]);
        $("#stockCurrent-0").val(split[0].split(" ")[0]);
    });
    $.post(getStockInventoryDestiny, {InventoryInternalID: $("#inventory-0").val(), warehouse: $("#warehouseHeaderDestiny").val()}).done(function (stock) {
        var split = stock.split("---;---");
        $("#price-0-stockdestiny").html(split[0]);
    });
    //uom inventory
    $(".inventory").change(function () {
        var split = $(this).attr('id').split('-');
        var inventoryInternalID = $(this).val();
        $.post(getUomThisInventory, {id: $(this).val()}).done(function (data) {
            $("#uom-" + split[1]).html(data);
        });

        $.post(getStockInventorySource, {InventoryInternalID: $("#inventory-" + split[1]).val(), warehouse: $("#warehouseHeaderSource").val()}).done(function (stock) {
            var splitt = stock.split("---;---");
            $("#price-" + split[1] + "-stocksource").html(splitt[0]);
            $("#stockCurrent-" + split[1]).val(splitt[0].split(" ")[0]);
        });
        $.post(getStockInventoryDestiny, {InventoryInternalID: $("#inventory-" + split[1]).val(), warehouse: $("#warehouseHeaderDestiny").val()}).done(function (stock) {
            var splitt = stock.split("---;---");
            $("#price-" + split[1] + "-stockdestiny").html(splitt[0]);
        });
    });
});