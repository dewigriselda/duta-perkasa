function addPeriodInventory(nStr, add)
{
    nStr += '';
    x = nStr.split(add);
    x1 = x[0];
    x2 = x.length > 1 ? add + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + add + '$2');
    }
    return x1 + x2;
}

window.updateAttach = function (element) {
    var data = $(element).data('all');
    var internalID = searchData("InternalID", data);
    selectedInventory = internalID;
//    if (searchData("InventoryInternalID", data) != 'null') {
//        $('#childInv').hide();
//    } else {
//        $.post(getInventoryChild, {id: internalID, query: ''}).done(function (data) {
//            $("#selectInventoryUpdate").html(data);
//        });
//        $('#childInv').show();
//    }
    $.post(setInventoryChild, {id: selectedInventory}).done(function (data) {
        var baris = 1;
        $('#table-inventory-update').find("tr:gt(1)").remove();
        $.each(data, function (index, value) {
            $('#table-inventory-update tr:last').after('<tr id="row' + baris + '">' +
                    '<td class="chosen-uom">' +
                    '<input type="hidden" class="inventory" style="width: 100px" id="inventory-' + baris + '" style="" name="InventoryInternalID[]" value="' + value["InvInternalID"] + '">' +
                    value["InventoryName"] +
                    '</td>' +
                    '<td class="chosen-uom">' +
                    '<input type="hidden" class="inventory" style="width: 100px" style="" name="inventoryUom[]" value="' + value["UOMInternalID"] + '">' +
                    value["UomID"] +
                    '</td>' +
                    '<td class="chosen-uom">' +
                    '<input type="hidden" class="inventory" style="width: 100px" style="" name="qty[]" value="' + value["Qty"] + '">' +
                    value["Qty"] +
                    '</td>' +
                    '<td><button class="btn btn-pure-xs btn-xs btn-deleteRow-update" type="button" data="row' + baris + '"><span class="glyphicon glyphicon-trash"></span></button>' + '</td>' +
                    '</tr>');
            baris++;
        });
        $(".btn-deleteRow-update").click(function () {
            if ($('#' + $(this).attr('data')).length > 0) {
                document.getElementById($(this).attr('data')).remove()
            }
        });
    });
    $('#varietyInternalIDUpdate').val(searchData("VarietyInternalID", data));
    $('#brandInternalIDUpdate').val(searchData("BrandInternalID", data));
//        document.getElementById("palenUpdate" + searchData("Palen", data)).checked = true;
//        document.getElementById("konsinyasiUpdate" + searchData("Konsinyasi", data)).checked = true;
    $('#nameUpdate').val(searchData("InventoryName", data));
    $('#powerUpdate').val(searchData("Power", data));
    $('#leadTimeUpdate').val(searchData("LeadTime", data));
    $('#UsageUpdate').val(searchData("UsageInventory", data));
    $('#maxStockUpdate').val(addPeriodInventory(searchData("MaxStock", data), ","));
    $('#minStockUpdate').val(addPeriodInventory(searchData("MinStock", data), ","));
    barcodeLama = searchData("BarcodeCode", data);
    $('#barcodeCode1Update').val(searchData("BarcodeCode", data));
    $('#remarkUpdate').val(searchData("Remark", data));
    $('#descriptionUpdate').val(searchData("Description", data));
    $('#idUpdate').val(searchData("InternalID", data));
    document.getElementById('createdDetail').innerHTML = searchData("UserRecord", data) + " " + searchData("dtRecordformat", data);
    if (searchData("UserModified", data) == "0") {
        document.getElementById('modifiedDetail').innerHTML = '-';
    } else {
        document.getElementById('modifiedDetail').innerHTML = searchData("UserModified", data) + " " + searchData("dtModifformat", data);
    }

    $("#type" + searchData("InventoryTypeInternalID", data)).attr('selected', 'selected');
    $('#typeUpdate').trigger("chosen:updated");

    $('#varietyInternalIDUpdate').trigger("chosen:updated");
    $('#brandInternalIDUpdate').trigger("chosen:updated");
};
window.deleteAttach = function (element) {
    $('#idDelete').val($(element).data('internal'));
    $('#deleteName').html($(element).data('name'));
};
window.stockAttach = function (element) {
    $('#idStock').val($(element).data('internal'));
};
window.printAttach = function (element) {
    var data = $(element).data('all');
    $('.inventoryIDPrint').html(searchData("InventoryID", data));
    $('.barcodePrint').html(searchData("BarcodeCode", data));
    $.post(getInventoryQRCode, {id: searchData("InternalID", data)}).done(function (data) {
        $('.qrcode').html(data);
    });
};
var barcodeLama = '';
$(document).ready(function () {
    $("#searchInventory").keydown(function (event) {
        if (event.keyCode == 13) { //enter
            event.preventDefault();
            $.post(getResultSearch, {id: $("#searchInventory").val()}).done(function (data) {
                $("#selectInventory").html(data);
            });
        }
    });
    $("#searchInventoryUpdate").keydown(function (event) {
        if (event.keyCode == 13) { //enter
            event.preventDefault();
            $.post(getInventoryChild, {id: selectedInventory, query: $("#searchInventoryUpdate").val()}).done(function (data) {
                $("#selectInventoryUpdate").html(data);
            });
        }
    });
    $("#varietyInternalID").change(function () {
        $.post(generateInventoryID, {idVar: $("#varietyInternalID").val(), idBrand: $("#brandInternalID").val()}).done(function (data) {
            $("#inventoryID").val(data.replace(/ /g, ''));
        });
    });

    $("#brandInternalID").change(function () {
        $.post(generateInventoryID, {idVar: $("#varietyInternalID").val(), idBrand: $("#brandInternalID").val()}).done(function (data) {
            $("#inventoryID").val(data.replace(/ /g, ''));
        });
    });

    $("#maxStockUpdate,#minStockUpdate").blur(function () {
        if (parseFloat(removePeriod($("#maxStockUpdate").val(), ',')) < parseFloat(removePeriod($("#minStockUpdate").val(), ','))) {
            $("#minStockUpdate").val($("#maxStockUpdate").val());
        }
    });

    $("#max,#min").blur(function () {
        if (parseFloat(removePeriod($("#max").val(), ',')) < parseFloat(removePeriod($("#min").val(), ','))) {
            $("#min").val($("#max").val());
        }
    });

    $(".btn-insert").click(function () {
        $.post(generateInventoryID, {idVar: $("#varietyInternalID").val(), idBrand: $("#brandInternalID").val()}).done(function (data) {
            $("#inventoryID").val(data.replace(/ /g, ''));
        });
    });

    $('#startDate').datepicker();
    $('#endDate').datepicker();
    $('#startDateStock').datepicker();
    $('#endDateStock').datepicker();
    $('#date').datepicker();
    $('#date1').datepicker();
    $("#startDate").datepicker("option", "dateFormat", 'dd-mm-yy');
    $("#endDate").datepicker("option", "dateFormat", 'dd-mm-yy');
    $("#startDateStock").datepicker("option", "dateFormat", 'dd-mm-yy');
    $("#endDateStock").datepicker("option", "dateFormat", 'dd-mm-yy');
    $("#date").datepicker("option", "dateFormat", 'dd-mm-yy');
    $("#date1").datepicker("option", "dateFormat", 'dd-mm-yy');
    $('#endDate, #startDate').change(function () {
        if ($('#startDate').val() == '') {
            $('#startDate').val($('#endDate').val());
        } else if ($('#endDate').val() == '') {
            $('#endDate').val($('#startDate').val());
        } else if (dateCheckHigher($('#startDate').val(), $('#endDate').val()) == 'start') {
            $('#endDate').val($('#startDate').val());
        }
    });
    $('#endDateStock, #startDateStock').change(function () {
        if ($('#startDateStock').val() == '') {
            $('#startDateStock').val($('#endDateStock').val());
        } else if ($('#endDateStock').val() == '') {
            $('#endDateStock').val($('#startDateStock').val());
        } else if (dateCheckHigher($('#startDateStock').val(), $('#endDateStock').val()) == 'start') {
            $('#endDateStock').val($('#startDateStock').val());
        }
    });
    $("#btn-detail-report").click(function () {
        if ($('#startDate').val() == '' && $('#endDate').val() == '') {
            var tanggal = new Date();
            var tanggalText = tanggal.getDate() + '-' + (tanggal.getMonth() + 1) + '-' + tanggal.getFullYear();
            $('#startDate').val(tanggalText);
            $('#endDate').val($('#startDate').val());
        } else if ($('#startDate').val() == '') {
            $('#startDate').val($('#endDate').val());
        } else if ($('#endDate').val() == '') {
            $('#endDate').val($('#startDate').val());
        } else if (dateCheckHigher($('#startDate').val(), $('#endDate').val()) == 'start') {
            $('#endDate').val($('#startDate').val());
        }
    });
    $("#btn-stock-report").click(function () {
        if ($('#startDateStock').val() == '' && $('#endDateStock').val() == '') {
            var tanggal = new Date();
            var tanggalText = tanggal.getDate() + '-' + (tanggal.getMonth() + 1) + '-' + tanggal.getFullYear();
            $('#startDateStock').val(tanggalText);
            $('#endDateStock').val($('#startDateStock').val());
        } else if ($('#startDateStock').val() == '') {
            $('#startDateStock').val($('#endDateStock').val());
        } else if ($('#endDateStock').val() == '') {
            $('#endDateStock').val($('#startDateStock').val());
        } else if (dateCheckHigher($('#startDateStock').val(), $('#endDateStock').val()) == 'start') {
            $('#endDateStock').val($('#startDateStock').val());
        }
    });
    $("#btn-summary-report").click(function () {
        if ($('#date').val() == '') {
            var tanggal = new Date();
            var tanggalText = tanggal.getDate() + '-' + (tanggal.getMonth() + 1) + '-' + tanggal.getFullYear();
            $('#date').val(tanggalText);
        }
    });
    $("#btn-buffer-stock-report").click(function () {
        if ($('#date1').val() == '') {
            var tanggal = new Date();
            var tanggalText = tanggal.getDate() + '-' + (tanggal.getMonth() + 1) + '-' + tanggal.getFullYear();
            $('#date1').val(tanggalText);
        }
    });
    $('#example').dataTable({
        "draw": 10,
        "processing": true,
        "serverSide": true,
        stateSave: true,
        "ajax": inventoryDataBackup
    });

    $("#inventoryID").keypress(function (e) {
        if (e.keyCode == '34' || e.keyCode == '39') {
            e.preventDefault();
        }
    });
});
function PrintDiv() {
    var divToPrint = document.getElementById('divToPrint');
    var popupWin = window.open('', '_blank', 'width=300,height=300');
    popupWin.document.open();
    popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
    popupWin.document.close();
    popupWin.close();
}



var config = {
    '.chosen-select': {}
};

for (var selector in config) {
    $(selector).chosen({
        search_contains: true
    });
}

$.validate({
    form: '#form-insert',
    onError: function () {
        return validationID(1);
    },
    onSuccess: function () {
        return validationID(1);
    }
});

$.validate({
    form: '#form-update',
    onError: function () {
        return validationIDBarcodeUpdate(1);
    },
    onSuccess: function () {
        return validationIDBarcodeUpdate(1);
    }
});
$("#inventoryID").blur(function () {
    validationID(0);
});

$("#barcodeCode").blur(function () {
    validationIDBarcode(0);
});
$("#barcodeCodeUpdate").blur(function () {
    validationIDBarcodeUpdate(0);
});

function validationID(status) {
    var search = $('#inventoryID').val();
    if (search == '') {
        return false;
    } else {
        var uomBig = $("#UoMBig").val();
        var uomMedium = $("#UoMMedium").val();
        var uomDefault = $("#UoMDefault").val();
        var valBig = $("#ValueBig").val();
        var valMedium = $("#ValueMedium").val();
        if ((uomBig == uomMedium && valBig != "" && valMedium != "") || (uomBig == uomDefault && valBig != "") || (uomMedium == uomDefault && valMedium != "")) {
            alert("Inventory with this UOM already exists");
            return false;
        } else {
            $.post(checkInventoryID, {id: search}).done(function (data) {
                if (data.trim() == "0") {
                    sukses();
                    if (status == 1) {
                        return true; //form submit
                    }
                } else {
                    gagal();
                    if (status == 1) {
                        return false; // Will stop the submission of the form
                    }
                }
            });
        }
    }
}

function validationIDBarcode(status) {
//    var search = $('#barcodeCode').val();
//    if (search == '') {
//        return false;
//    } else {
//        $.post(checkInventoryHasBarcode, {id: search}).done(function (data) {
//            if (data.trim() == "0") {
//                suksesBarcode("barcodeCode");
//                if (status == 1) {
//                    return true; //form submit
//                }
//            } else {
//                gagalBarcode("barcodeCode", 'other inventory');
//                if (status == 1) {
//                    return false; // Will stop the submission of the form
//                }
//            }
//        });
//    }
    return true;
}
function validationIDBarcodeUpdate(status) {
//    var search = $('#barcodeCodeUpdate').val();
//    if (search == '') {
//        return false;
//    } else {
//        $.post(checkInventoryHasBarcodeUpdate, {id: search}).done(function (data) {
//            if (data.trim() == "0") {
//                suksesBarcode("barcodeCodeUpdate");
//                if (status == 1) {
//                    return true; //form submit
//                }
//            } else {
//                gagalBarcode("barcodeCodeUpdate", 'other inventory');
//                if (status == 1) {
//                    return false; // Will stop the submission of the form
//                }
//            }
//        });
//    }
    return true;
}
$(".numajaDesimal").keypress(function (e) {
    if ((e.charCode >= 48 && e.charCode <= 57) || (e.charCode == 0) || (e.charCode == 46))
        return true;
    else
        return false;
});
//function error success
function gagalBarcode(id, data) {
    if ($('#spanBarcodeID')) {
        $('#spanBarcodeID').remove();
    }
    $('#' + id).parent('div').append('<span class="help-block form-error" id="spanBarcodeID">Barcode ID has already been taken by ' + data + ' </span>');
    $('#' + id).parent('div').removeClass('has-success');
    $('#' + id).parent('div').addClass('has-error');
    $('#' + id).css("border-color", "rgb(169, 68, 66)");
}
function suksesBarcode(id) {
    $('#spanBarcodeID').remove();
    $('#' + id).parent('div').removeClass('has-error');
    $('#' + id).parent('div').addClass('has-success');
    $('#' + id).css("border-color", "");
}

function gagal() {
    if ($('#spanErrorID')) {
        $('#spanErrorID').remove();
    }
    $('#inventoryID').parent('div').append('<span class="help-block form-error" id="spanErrorID">Inventory ID has already been taken</span>');
    $('#inventoryID').parent('div').removeClass('has-success');
    $('#inventoryID').parent('div').addClass('has-error');
    $('#inventoryID').css("border-color", "rgb(169, 68, 66)");
}
function sukses() {
    $('#spanErrorID').remove();
    $('#inventoryID').parent('div').removeClass('has-error');
    $('#inventoryID').parent('div').addClass('has-success');
    $('#inventoryID').css("border-color", "");
}