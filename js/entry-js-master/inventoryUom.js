$("#searchInventory").keydown(function (event) {
    if (event.keyCode == 13) { //enter
        event.preventDefault();
        $.post(getResultSearch, {id: $("#searchInventory").val()}).done(function (data) {
            $("#selectInventory").html(data);
        });
    }
});
window.updateAttach = function (element) {
    var data = $(element).data('all');
    $('#inventoryInternalIDUpdate').val(searchData("InventoryInternalID", data));
    $('#uomInternalIDUpdate').val(searchData("UomInternalID", data));
    $('#updateName').html($(element).data('name'));
    $('#purchasePriceUpdate').val(addPeriod(searchData("purchasePrice", data), ","));
    $('#priceAUpdate').val(addPeriod(searchData("PriceA", data), ","));
    $('#priceBUpdate').val(addPeriod(searchData("PriceB", data), ","));
    $('#priceCUpdate').val(addPeriod(searchData("PriceC", data), ","));
    $('#priceDUpdate').val(addPeriod(searchData("PriceD", data), ","));
    $('#priceEUpdate').val(addPeriod(searchData("PriceE", data), ","));

    $('#remarkUpdate').val(searchData("Remark", data));
    $('#valueUpdate').val(addPeriod(searchData("Value", data), ',').split('.')[0]);
    $('#idUpdate').val(searchData("InternalID", data));
    document.getElementById('createdDetail').innerHTML = searchData("UserRecord", data) + " " + searchData("dtRecordformat", data);
    if (searchData("UserModified", data) == "0") {
        document.getElementById('modifiedDetail').innerHTML = '-';
    } else {
        document.getElementById('modifiedDetail').innerHTML = searchData("UserModified", data) + " " + searchData("dtModifformat", data);
    }
    if (searchData("Default", data) == 1) {
        $('#valueUpdate').attr("readonly", true);
        $('#valueUpdate').attr("style", "background-color:#ddd");
        document.getElementById("tipeNoDefaultUpdate").checked = false;
        document.getElementById("tipeDefaultUpdate").checked = true;
        $('#tipeNoDefaultUpdate').removeAttr('checked');
        $('#tipeDefaultUpdate').attr('checked', 'checked');
    } else {
        $('#valueUpdate').attr("readonly", false);
        $('#valueUpdate').attr("style", "background-color:#fff");
        document.getElementById("tipeDefaultUpdate").checked = false;
        document.getElementById("tipeNoDefaultUpdate").checked = true;
        $('#tipeDefaultUpdate').removeAttr('checked');
        $('#tipeNoDefaultUpdate').attr('checked', 'checked');
        $.ajax({
            type: "POST",
            url: cekgantivalue,
            data: {inventoryID: searchData("InventoryInternalID", data), uomID: searchData("UomInternalID", data)},
            success: function (hasil) {
                if (hasil == 'false') {
                    $('#valueUpdate').attr("readonly", true);
                    $('#valueUpdate').attr("style", "background-color:#ddd");
                }
            },
            error: function (hasil) {
//                alert("error");
            }
        });
    }

    $('#inventoryInternalIDUpdate').trigger("chosen:updated");
    $('#uomInternalIDUpdate').trigger("chosen:updated");
};
window.deleteAttach = function (element) {
    $('#idDelete').val($(element).data('internal'));
    $('#deleteName').html($(element).data('name'));
};
$(document).ready(function () {
    function addPeriod(nStr, add)
    {
        nStr += '';
        xtamp = nStr.split('.');
        x = xtamp[0].split(add);
        x1 = x[0];
        x2 = x.length > 1 ? add + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + add + '$2');
        }
        return x1 + x2 + '.' + xtamp[1];
    }

    $("#tipeDefault,#tipeNoDefault,#tipeDefaultUpdate,#tipeNoDefaultUpdate").change(function () {
        if ($('#tipeDefault').attr('checked') == 'checked') {
            $('#tipeDefault').removeAttr('checked');
            $('#tipeNoDefault').attr('checked', 'checked');
            $('#value').attr("readonly", false);
            $('#value').attr("style", "background-color:#fff");
        } else {
            $('#tipeNoDefault').removeAttr('checked');
            $('#tipeDefault').attr('checked', 'checked');
            $('#value').val(1);
            $('#value').attr("readonly", true);
            $('#value').attr("style", "background-color:#ddd");
        }
        if ($('#tipeDefaultUpdate').attr('checked') == 'checked') {
            $('#tipeDefaultUpdate').removeAttr('checked');
            $('#tipeNoDefaultUpdate').attr('checked', 'checked');
            $('#valueUpdate').attr("readonly", false);
            $('#valueUpdate').attr("style", "background-color:#fff");
        } else {
            $('#tipeNoDefaultUpdate').removeAttr('checked');
            $('#tipeDefaultUpdate').attr('checked', 'checked');
            $('#valueUpdate').val(1);
            $('#valueUpdate').attr("readonly", true);
            $('#valueUpdate').attr("style", "background-color:#ddd");
        }
    });
    $(".btn-edit").click(function () {
    });

    var config = {
        '.chosen-select': {}
    };

    for (var selector in config) {
        $(selector).chosen({
            search_contains: true
        });
    }

    $(".btn-delete").click(function () {
    });

    $("#btn-tambah").click(function () {
        $('#value').val(removePeriod($('#value').val(), ','));
    });

    $("#btn-ubah").click(function () {
        $('#valueUpdate').val(removePeriod($('#valueUpdate').val(), ','));
    });





    $('#example').dataTable({
        "draw": 10,
        "processing": true,
        "serverSide": true,
        stateSave: true,
        "ajax": inventoryUomDataBackup
    });
//    var table = $('#example').dataTable();
//    table.fnPageChange(1, true); // 2 = 2-1 di datatabel. Dengan kata lain data tabel mulai 1 adalah index ke 0
//
//    var table = $('#example').DataTable();
//    var info = table.page.info();
//    table.on('search.dt', function () {
//        alert(table.search());
//        alert(info.page);
//        alert(info.len());
//    });

//function gagal() {
//    if ($('#spanErrorID')) {
//        $('#spanErrorID').remove();
//    }
//    $('#currencyID').parent('li').append('<span class="help-block form-error" id="spanErrorID">Currency ID has already been taken</span>');
//    $('#currencyID').parent('li').removeClass('has-success');
//    $('#currencyID').parent('li').addClass('has-error');
//    $('#currencyID').css("border-color", "rgb(169, 68, 66)");
//}
//function sukses() {
//    $('#spanErrorID').remove();
//    $('#currencyID').parent('li').removeClass('has-error');
//    $('#currencyID').parent('li').addClass('has-success');
//    $('#currencyID').css("border-color", "");
//}
    $.validate({
        form: '#form-update',
        onError: function () {
            if (validationInvUomUpdate(1)) {
                return true;
            } else {
                return false;
            }
        },
        onSuccess: function () {
            if (validationInvUomUpdate(1)) {
                return true;
            } else {
                return false;
            }
        }
    });
    $.validate({
        form: '#form-insert',
        onError: function () {
            if (validationInvUom(1)) {
                return true;
            } else {
                return false;
            }
        },
        onSuccess: function () {
            if (validationInvUom(1)) {
                return true;
            } else {
                return false;
            }
        }
    });

    function gagal() {
        if ($('#spanErrorID')) {
            $('#spanErrorID').remove();
        }
        $('#divError').parent('div').append('<span class="help-block form-error" id="spanErrorID">Inventory with this UOM is already exist.</span>');
        $('#divError').parent('div').removeClass('has-success');
        $('#divError').parent('div').addClass('has-error');
        $('#divError').css("border-color", "rgb(169, 68, 66)");
    }
    function sukses() {
        $('#spanErrorID').remove();
        $('#divError').parent('div').removeClass('has-error');
        $('#divError').parent('div').addClass('has-success');
        $('#divError').css("border-color", "");
    }
    function gagalUpdate() {
        if ($('#spanErrorID')) {
            $('#spanErrorID').remove();
        }
        $('#divErrorUpdate').parent('div').append('<span class="help-block form-error" id="spanErrorID">Inventory with this UOM is already exist.</span>');
        $('#divErrorUpdate').parent('div').removeClass('has-success');
        $('#divErrorUpdate').parent('div').addClass('has-error');
        $('#divErrorUpdate').css("border-color", "rgb(169, 68, 66)");
    }
    function suksesUpdate() {
        $('#spanErrorID').remove();
        $('#divErrorUpdate').parent('div').removeClass('has-error');
        $('#divErrorUpdate').parent('div').addClass('has-success');
        $('#divErrorUpdate').css("border-color", "");
    }

    function validationInvUom(status) {
        $.post(checkInvUom, {idInv: $("#inventoryInternalID").val(), idUom: $("#uomInternalID").val()}).done(function (data) {
            if (data.trim() == "0") {
                sukses();
                if (status == 1) {
                    return true; //form submit
                }
            } else {
                gagal();
                if (status == 1) {
                    return false; // Will stop the submission of the form
                }
            }
        });
        return true;
    }

    function validationInvUomUpdate(status) {
        $.post(checkInvUomUpdate, {idInvUom: $("#idUpdate").val(), idInv: $("#inventoryInternalIDUpdate").val(), idUom: $("#uomInternalIDUpdate").val()}).done(function (data) {
            if (data.trim() == "0") {
                suksesUpdate();
                if (status == 1) {
                    return true; //form submit
                }
            } else {
                gagalUpdate();
                if (status == 1) {
                    return false; // Will stop the submission of the form
                }
            }
        });
        return true;
    }
//$("#currencyID").blur(function () {
//    var dataID = a;
//    validationID(dataID, 0, b);
//});
    $("#btn-submit").click(function () {
        $('#value').val(removePeriod($('#value').val(), ','));
    });
    $("#btn-update").click(function () {
        $('#valueUpdate').val(removePeriod($('#valueUpdate').val(), ','));
    });
});