$(document).ready(function () {
    window.updateNotes = function (element) {
        var data = $(element).data('all');

        $('#toUpdate').val(searchData("To", data));
        $('#descriptionUpdate').val(searchData("Description", data));
        $('#remarkUpdate').val(searchData("Remark", data));
        $('#idUpdate').val(searchData("InternalID", data));
        document.getElementById('createdDetail').innerHTML = searchData("UserRecord", data) + " " + searchData("dtRecordformat", data);
        if (searchData("UserModified", data) == "0") {
            document.getElementById('modifiedDetail').innerHTML = '-';
        } else {
            document.getElementById('modifiedDetail').innerHTML = searchData("UserModified", data) + " " + searchData("dtModifformat", data);
        }
    };
    window.deleteNotes = function (element) {
        $('#idDelete').val($(element).data('internal'));
        $('#deleteName').text($(element).data('id'));
    };
    window.doneNotes = function (element) {
        $('#idDone').val($(element).data('internal'));
    };
    $(".to").autocomplete({
        source: availableTags
    });
    $(".to").autocomplete("option", "appendTo", ".formNotesInsert");
    $('#example').dataTable({
        "order": [[3, "desc"]],
        "draw": 10,
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": notesDataBackup,
        }
    });
});
function gagal() {
    if ($('#spanErrorID')) {
        $('#spanErrorID').remove();
    }
    $('#notesID').parent('div').append('<span class="help-block form-error" id="spanErrorID">Notes ID has already been taken</span>');
    $('#notesID').parent('div').removeClass('has-success');
    $('#notesID').parent('div').addClass('has-error');
    $('#notesID').css("border-color", "rgb(169, 68, 66)");
}
function sukses() {
    $('#spanErrorID').remove();
    $('#notesID').parent('div').removeClass('has-error');
    $('#notesID').parent('div').addClass('has-success');
    $('#notesID').css("border-color", "");
}
function validationID(dataID, status, r) {
    dataID = decryptDataID(dataID, r);
    var search = $('#notesID').val();
    if (search == '') {
        return false;
    } else if (searchID(search, dataID, 'SalesManID')) {
        gagal();
        if (status == 1) {
            return false; // Will stop the submission of the form
        }
    } else {
        sukses();
        if (status == 1) {
            return true; //form submit
        }
    }
}

$.validate({
});
