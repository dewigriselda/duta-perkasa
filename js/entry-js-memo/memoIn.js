var config = {
    '.chosen-select': {}
};
for (var selector in config) {
    $(selector).chosen({
        search_contains: true
    });
}
$(document).ready(function () {
    
    $('#ExampleMemoIn').dataTable({
        "draw": 10,
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url":memoInDataBackup,
        }
    });
    
    
    $("#searchInventory").keydown(function (event) {
        if (event.keyCode == 13) { //enter
            event.preventDefault();
            $.post(getSearchResultInventoryMemoIn, {id: $("#searchInventory").val()}).done(function (data) {
                $("#selectInventory").html(data);
            });
        }
    });
window.deleteAttach = function (element) {
        $('#idDelete').val($(element).data('internal'));
        $('#deleteName').text($(element).data('name'));
    };
    $('#example').dataTable({
        "order":[[1,"desc"]],
        columnDefs: [{
                targets: [0],
                orderData: [0, 1]
            }, {
                targets: [1],
                orderData: [1, 0]
            }, {
                targets: [4],
                orderData: [4, 0]
            }]
    });
});