var config = {'.chosen-select': {}};
for (var selector in config) {
    $(selector).chosen({
        search_contains: true
    });
}
$(document).ready(function () {
    //packing list lama
//    $.post(getShippingList, {customer: $("#customer").val()}).done(function (data) {
//        $("#shippingDiv").html(data);
//    });
//    $("#customer").change(function (event) {
//        $.post(getShippingList, {customer: $("#customer").val()}).done(function (data) {
//            $("#shippingDiv").html(data);
//        });
//    });

//packing list baru
    $("#searchSalesOrder2").keydown(function (event) {
        console.log('a');
        if (event.keyCode == 13) { //enter
            event.preventDefault();
            $.post(getResultSearchSO2, {id: $("#searchSalesOrder2").val()}).done(function (data) {
                $("#selectSalesOrder2").html(data);
            });
        }
    });

    $('#user-toggle').click(function () {
        if ($('#drop-user').hasClass('open')) {
            $('#user-toggle').removeClass('user-focus')
        } else {
            $('#user-toggle').addClass('user-focus')
        }
    });
    $('#searchwrap').hide();
    $('#search-button').click(function () {
        $('#searchwrap').slideToggle(300)
    });
    $('#cancel').click(function () {
        $('#searchwrap').slideUp(300)
    });
    $('#startDate').datepicker();
    $('#endDate').datepicker();
    $("#startDate").datepicker("option", "dateFormat", 'dd-mm-yy');
    $("#endDate").datepicker("option", "dateFormat", 'dd-mm-yy');
    $('.appd').find('a.chosen-single').each(function () {
        $(this).addClass('chosenapp')
    });
    var d = new Date();
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    var date = day + "-" + month + "-" + year;
    var startDate = "01" + "-" + month + "-" + year;
    $("#startDate").val(startDate);
    $("#endDate").val(date);
    $('#endDate, #startDate').change(function () {
        if ($('#startDate').val() == '') {
            $('#startDate').val($('#endDate').val())
        } else if ($('#endDate').val() == '') {
            $('#endDate').val($('#startDate').val())
        } else if (dateCheckHigher($('#startDate').val(), $('#endDate').val()) == 'start') {
            $('#endDate').val($('#startDate').val())
        }
    });
    $('#startDateReport').datepicker();
    $('#endDateReport').datepicker();
    $("#startDateReport").datepicker("option", "dateFormat", 'dd-mm-yy');
    $("#endDateReport").datepicker("option", "dateFormat", 'dd-mm-yy');
    $('#packingdate').datepicker();
    $("#packingdate").datepicker("option", "dateFormat", 'dd-mm-yy');
    var tanggal = new Date();
    var tanggalText = tanggal.getDate() + '-' + (tanggal.getMonth() + 1) + '-' + tanggal.getFullYear();
    $('#packingdate').val(tanggalText);
    $('#endDateReport, #startDateReport').change(function () {
        if ($('#startDateReport').val() == '') {
            $('#startDateReport').val($('#endDateReport').val())
        } else if ($('#endDateReport').val() == '') {
            $('#endDateReport').val($('#startDateReport').val())
        } else if (dateCheckHigher($('#startDateReport').val(), $('#endDateReport').val()) == 'start') {
            $('#endDateReport').val($('#startDateReport').val())
        }
    });

    window.deleteAttach = function (element) {
        $('#idDelete').val($(element).data('internal'));
    };
    window.printAttach = function (element) {
        $('#internalID').val($(element).data('internal'));
    }

    $("#btn-add-sl").click(function () {
        if ($("#shippinglist").val() == '' || $("#shippinglist").val() == null)
            alert("You must have minimal 1 Shipping");
        else
            $("#form-sl").submit();
    });
    $("#btn-rSummary").click(function () {
        $('#jenisReport').val('summaryShipping');
        document.getElementById('titleReport').innerHTML = 'Summary Report'
    });
    $("#btn-rDetail").click(function () {
        $('#jenisReport').val('detailShipping');
        document.getElementById('titleReport').innerHTML = 'Detail Report'
    });
    $("#btn-report-transaction").click(function () {
        if ($('#startDateReport').val() == '' && $('#endDateReport').val() == '') {
            var tanggal = new Date();
            var tanggalText = tanggal.getDate() + '-' + (tanggal.getMonth() + 1) + '-' + tanggal.getFullYear();
            $('#startDateReport').val(tanggalText);
            $('#endDateReport').val($('#startDateReport').val())
        } else if ($('#startDateReport').val() == '') {
            $('#startDateReport').val($('#endDateReport').val())
        } else if ($('#endDateReport').val() == '') {
            $('#endDateReport').val($('#startDateReport').val())
        } else if (dateCheckHigher($('#startDateReport').val(), $('#endDateReport').val()) == 'start') {
            $('#endDateReport').val($('#startDateReport').val())
        }
    });
    $('body').on('click', '.btn-toggle-detail', function () {
        if ($(this).attr('data-toshow') == 'show') {
            $('.' + $(this).attr('data')).hide();
            $(this).attr('data-toshow', 'hide');
        } else {
            $('.' + $(this).attr('data')).show();
            $(this).attr('data-toshow', 'show');
        }
    });

    $('body').on('click', '.btn-toggle-spec', function () {
        if ($(this).attr('data-toshow') == 'show') {
            $('.' + $(this).attr('data')).hide();
            $(this).attr('data-toshow', 'hide');
        } else {
            $('.' + $(this).attr('data')).show();
            $(this).attr('data-toshow', 'show');
        }
    });
    $('#packing').dataTable({
        "order": [[1, "desc"]],
        "draw": 10,
        "processing": true,
        "serverSide": true,
        "ajax": packingDataBackup
    })
});