$(document).ready(function () {
//    alert(submit);
    $("#searchInventory").keydown(function (event) {
        if (event.keyCode == 13) { //enter
            event.preventDefault();
            $.post(getSearchResultInventoryForSO, {id: $("#searchInventory").val()}).done(function (data) {
                $("#selectInventory").html(data);
                hitungStock();
                $("#btn-addRow").removeAttr("disabled");
            });
        }
    });

    function hitungStock() {
        var warehouse = $('#warehouse-0').val();
        var inventory = $('#inventory-0').val();
        $.post(hitungStockInventoryWarehouseTanpaPacking, {warehouse: warehouse, inventory: inventory, packing: $('#packingid').val()}).done(function (data) {
            $("#price-0-stock").html(data);
        });
    }

    function hitungSisaStock(baris) {
        //kalau inventory saja, parcel tidak
        if ($("#inventory-" + baris).val().split('---;---')[1] == 'inventory') {
            var idbarang = $("#inventory-" + baris).val().split('---;---')[0];
            var iddesc = $("#inventory-" + baris).val().split('---;---')[3];
            //ngecek barang diwarehouse itu yang udh dimasukkan masih ckup ga, misal di warehouse ada 40
            //udah masukkan 30, masukkan berikutnya max 10
            var a = $("#price-" + baris + "-stock").html();
            a = a.replace(' PCS', '');
            var stokwarehouse = parseInt(a);
            var warehouse = $("#warehouse-" + baris).val();
            var tamp = 0;
            var tamptnpalast = 0;
            $(".qty").each(function (i) {
                //ambil baris
                var id = $(this).attr('id');
                id = id.replace('price-', '');
                id = id.replace('-qty', '');
                //yg di baris 0 diabaikan
                if (id != 0 && id != '0') {
                    console.log("id: " + id);
                    if ($(this).attr('warehouse') == warehouse && $(this).attr('idbarang') == idbarang) {
                        tamp += parseInt($(this).val());
                        if (id != baris)
                            tamptnpalast += parseInt($(this).val());
                    }
                }
            });
            console.log("total yg sudah dipakai: " + tamptnpalast);
            console.log("input: " + parseInt($('#price-' + baris + '-qty').val()));
            console.log("stokwarehouse: " + stokwarehouse);
            var qty = 0;
            if ((tamptnpalast + parseInt($('#price-' + baris + '-qty').val())) <= stokwarehouse)
                qty = parseInt($('#price-' + baris + '-qty').val());
            else
                qty = stokwarehouse - tamptnpalast;
            var sisabarang = parseInt($("#sisa" + iddesc + "_" + idbarang).html()) - qty;


            //untuk ngecek kalau sisa yang kurang msih lbih kecil dari qty input
//            var balik = parseInt($('#price-' + baris + '-qty').val());
            var balik = qty;
            if (sisabarang < 0) {
                sisabarang = 0;
                balik = parseInt($("#sisa" + iddesc + "_" + idbarang).html());
            }
            $("#sisa" + iddesc + "_" + idbarang).html(sisabarang);
            return balik;
        }
    }
    function hitungSisaStockDelete(idbarang, qty, iddesc) {
//        console.log($("#sisa" + iddesc + "_" + idbarang).html());
        var sisabarang = parseInt($("#sisa" + iddesc + "_" + idbarang).html()) + parseInt(qty);
//        console.log(sisabarang);
        $("#sisa" + iddesc + "_" + idbarang).html(sisabarang);
    }

    function resetStok(idbarang, qty, iddesc, old) {
        var sisabarang = parseInt($("#sisa" + iddesc + "_" + idbarang).html()) + parseInt(old);
        $("#sisa" + iddesc + "_" + idbarang).html(sisabarang);
    }

    $.post(hitungStockInventoryWarehouseTanpaPacking, {warehouse: $('#warehouse-0').val(), inventory: $('#inventory-0').val(), packing: $('#packingid').val()}).done(function (stock) {
        var split = stock.split("---;---");
        $("#price-0-stock").html(split[0]);
    });

    $(document).on('change', '.inventory', function () {
        var id = $(this).attr("id").split("-");
        var warehouse = $('#warehouse-' + id[1]).val();
        var inventory = $('#inventory-' + id[1]).val();
        var tampVar = $(this).find('option:selected').attr('id');
        console.log(this);
        if (tampVar.indexOf("inventory") >= 0) {
            //get stock
            $.post(hitungStockInventoryWarehouseTanpaPacking, {warehouse: warehouse, inventory: inventory, packing: $('#packingid').val()}).done(function (stock) {
                var split = stock.split("---;---");
                $("#price-" + id[1] + "-stock").html(split[0]);
//                document.getElementById("price-" + id[1] + "-stock").setAttribute("data-content", "Inventory stock remains : " + split[0] + " <br/>");
//                        document.getElementById("price-" + id[1] + "-stock").setAttribute("data-content", "Inventory stock remains : " + split[0] + " <br/> Similar Product : <br/>" + split[1]);
//                document.getElementById("price-" + id[1] + "-stock").title = "Information Inventory";
//                $("#price-" + id[1] + "-stock").popover({trigger: 'hover'})
            });
        } else if (tampVar.indexOf("parcel") >= 0) {
//            alert('das');
            //found parcel
            $("#price-" + id[1] + "-stock").html("-");
//            $("#uom-" + id[1]).attr("readonly", true);
//            $("#uom-" + id[1]).html("<option value='0'>-</option>");
//            $("#uom-" + id[1]).trigger("chosen:updated");
//            var value = $(this).val().split("---;---");
            //get price parcel
//            $.post(getPriceThisParcel, {id: value[0]}).done(function (data) {
//                $("#price-" + id[1]).val(addPeriod(data.trim(), ","));
//                changeTotal($("#price-" + id[1]).attr("id"));
//            });
        }
    });

    $(".warehouse").change(function () {
        //cek kalau inventory udah ada baru dicek
        if ($("#inventory-0").length != 0) {
            //kalau dichange pas lagi ksong inventory ada error js
            var id = $(this).attr("id").split("-");
            var warehouse = $('#warehouse-' + id[1]).val();
            var inventory = $('#inventory-' + id[1]).val();
            var tampVar = $('#inventory-0').find('option:selected').attr('id');
            if (tampVar.indexOf("inventory") >= 0) {
                //get stock
                $.post(hitungStockInventoryWarehouseTanpaPacking, {warehouse: warehouse, inventory: inventory, packing: $('#packingid').val()}).done(function (stock) {
                    var split = stock.split("---;---");
                    $("#price-" + id[1] + "-stock").html(split[0]);
                });
            } else if (tampVar.indexOf("parcel") >= 0) {
                //found parcel
                $("#price-" + id[1] + "-stock").html("-");
            }
        }
    });
    $(".btn-deleteRow").off().click(function (e) {
//            $('#jumlahbarisdetail').val(parseInt($('#jumlahbarisdetail').val()) - 1);
//            alert($('#jumlahbarisdetail').val());
        if ($('#' + $(this).attr('data')).length > 0) {
            var angka = $(this).attr('data').replace('row', '');
            var inv = $('#inventory-' + angka).val();
            var qty = $('#price-' + angka + '-qty').val();
            document.getElementById($(this).attr('data')).remove();
            if (inv.split('---;---')[1] == 'inventory') {
                hitungSisaStockDelete(inv.split('---;---')[0], qty, inv.split('---;---')[3]);
            }
            submit--;
        }
//        alert(submit);
    });
    $('.qty').on('focusin', function () {
        $(this).data('old', $(this).val());
    });

    $(".qty").unbind('change');
    $(".qty").change(function () {
        var id = $(this).attr('id');
        id = id.replace('price-', '');
        id = id.replace('-qty', '');
        var qty = 0;
        if (id != 0) {
//                if ($(this).val() > 0)
//                    submit--;
            var angka = id;
            var inv = $('#inventory-' + angka).val();
            var qty = $('#price-' + angka + '-qty').val();
            if (inv.split('---;---')[1] == 'inventory') {
                resetStok(inv.split('---;---')[0], qty, inv.split('---;---')[3], $(this).data('old'));
            }
            qty = hitungSisaStock(id);
            $(this).val(qty);
//                if ($(this).val() > 0)
//                    submit++;
//                alert(submit);
        }
    });
    $("#btn-addRow").off().click(function () {
//        $('#jumlahbarisdetail').val(parseInt($('#jumlahbarisdetail').val()) + 1);
        $('#jumlahbarisdetail').val(baris);
        var i = $("#inventory-0").val();
//        alert($('#jumlahbarisdetail').val());
        $('#table-packing-header tr:last').after('<tr id="row' + baris + '">' +
                '<td class="chosen-uom">' +
                '<input type="hidden" class="warehouse" style="width: 100px" id="warehouse-' + baris + '" style="" name="warehouse' + baris + '" value="' + $('#warehouse-0').val() + '">' +
                $("#warehouse-0 option[value='" + $("#warehouse-0").val() + "']").text() +
                '</td>' +
                '<td class="chosen-uom">' +
                '<input type="hidden" class="inventory" style="width: 100px" id="inventory-' + baris + '" style="" name="inventory' + baris + '" value="' + $('#inventory-0').val() + '">' +
                $("#inventory-0 option[value='" + $("#inventory-0").val() + "']").text() +
                '</td>' +
                '<td class="text-right" id="price-' + baris + '-stock">' +
                $('#price-0-stock').text() +
                '</td>' +
                '<td class="text-right">' +
                '<input type="text" class="maxWidth qty right input-theme numaja" warehouse="' + $("#warehouse-0").val() + '" idbarang="' + i.split('---;---')[0] + '" name="qty' + baris + '" maxlength="11" min="1" value="1" id="price-' + baris + '-qty">' +
                '<input type="hidden" name="description' + baris + '[]" id="description-' + baris + '">' +
                '</td>' +
                '<td>' +
                '<button class="btn btn-pure-xs btn-xs btn-deleteRow" type="button" data="row' + baris + '"><span class="glyphicon glyphicon-trash"></span></button>' +
                '</td>' +
                '</tr>');
        $('#price-' + baris + '-qty').val($('#price-0-qty').val());
//        alert($('#inventory-0').val().split('---;---')[2]);
        $('#description-' + baris).val($('#inventory-0').val().split('---;---')[2]);

        if (i.split('---;---')[1] == 'inventory') {
            var qty = hitungSisaStock(baris);
            $('#price-' + baris + '-qty').val(qty);
        }
        $(".btn-deleteRow").off().click(function (e) {
//            $('#jumlahbarisdetail').val(parseInt($('#jumlahbarisdetail').val()) - 1);
//            alert($('#jumlahbarisdetail').val());
            if ($('#' + $(this).attr('data')).length > 0) {
                var angka = $(this).attr('data').replace('row', '');
                var inv = $('#inventory-' + angka).val();
                var qty = $('#price-' + angka + '-qty').val();
                document.getElementById($(this).attr('data')).remove();
                if (inv.split('---;---')[1] == 'inventory') {
                    hitungSisaStockDelete(inv.split('---;---')[0], qty, inv.split('---;---')[3]);
                }
                submit--;
            }
        });
        $(".numajaDesimal").keypress(function (e) {
            if ((e.charCode >= 48 && e.charCode <= 57) || (e.charCode == 0) || (e.charCode == 46))
                return true;
            else
                return false;
        });

//        if ($('#price-' + baris + '-qty').val() > 0)
        submit++;
//        $(".qty").change(function () {
//            var id = $(this).attr('id');
//            id = id.replace('price-', '');
//            id = id.replace('-qty', '');
//            var qty = 0;
//            if (id != 0) {
////                if ($(this).val() > 0)
////                    submit--;
//                var angka = id;
//                var inv = $('#inventory-' + angka).val();
//                var qty = $('#price-' + angka + '-qty').val();
//                if (inv.split('---;---')[1] == 'inventory') {
//                    hitungSisaStockDelete(inv.split('---;---')[0], qty, inv.split('---;---')[3]);
//                }
//                qty = hitungSisaStock(id);
//                $(this).val(qty);
////                if ($(this).val() > 0)
////                    submit++;
////                alert(submit);
//            }
//        });
        var config = {'.chosen-select': {}};
        for (var selector in config) {
            $(selector).chosen({
                search_contains: true
            });
        }
        $('.appd').find('a.chosen-single').each(function () {
            $(this).addClass('chosenapp');
            var added = $(this).after().addClass('chosenapp');
            added++;
            var end = $('td.appd:last').children().find('select').addClass('chosenapp');
            end++
        });
        $("#searchInventory").val("");
        $("#searchInventory").attr("tabindex", -1).focus();
        baris++;
//        autoChangeTotal();
    });

    $("#btn-save").click(function (e) {
        if (submit <= 0) {
            e.preventDefault();
//            alert(submit);
            alert('You must have minimal 1 item with quantity more than 0');
        } else {
            var tamp = 0;
            $(".qty").each(function (i) {
                //ambil baris
                var id = $(this).attr('id');
                id = id.replace('price-', '');
                id = id.replace('-qty', '');
//                console.log("id: "+id);
                //yg di baris 0 diabaikan
                if (id != 0) {
                    tamp += parseInt($(this).val());
                }
            });
            if (tamp <= 0) {
                e.preventDefault();
//                alert(tamp);
                alert('You must have minimal 1 item with quantity more than 0');
            }
        }
    });

    var tampTotal;
    function changeTotal(id) {
        var price = document.getElementById(id).value;
        price = removePeriod(price, ',');
        var diskon = $("#" + id + '-discount').val();
        var diskonNominal = removePeriod($("#" + id + '-discountNominal').val(), ',') * removePeriod($("#" + id + '-qty').val(), ',');
        var output = price * removePeriod($("#" + id + '-qty').val(), ',');
        diskon = diskon * output / 100;
        diskon = Math.round(diskon * 100) / 100;
        output = Math.round(output * 100) / 100;
        output = output - diskon - diskonNominal;
        output = Math.round(output * 100) / 100;
        output = addPeriodSales(output, ',');
        if (output.indexOf(".") == -1) {
            output = output + '.00';
        } else if (output.split('.')[1].length == 1) {
            output = output + '0';
        }
        document.getElementById(id + '-qty-hitung').innerHTML = output;
        hitungTotal()
    }
    function autoChangeTotal() {
        myFunctionduit();
        $("#currencyHeader").change(function () {
            var currencyHeader = $("#currencyHeader").val().split('---;---');
            $("#rate").val(addPeriod(currencyHeader[2], ','));
            var rate = $("#rate").val();
            $(".price").each(function (i) {
                changeTotal($(this).attr('id'))
            });
            var currency = $("#currencyHeader").val().split('---;---');
            if (currency[3] == '1') {
                $("#rate").prop('readonly', true);
                $("#rate").css('background-color', '#eee')
            } else {
                $("#rate").prop('readonly', false);
                $("#rate").css('background-color', '')
            }
            hitungTotal()
        });
        function addPeriodSales(nStr, add) {
            nStr += '';
            x = nStr.split(add);
            x1 = x[0];
            x2 = x.length > 1 ? add + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + add + '$2')
            }
            return x1 + x2
        }
        function changeTotal(id) {
            var price = document.getElementById(id).value;
            price = removePeriod(price, ',');
            var diskon = $("#" + id + '-discount').val();
            var diskonNominal = removePeriod($("#" + id + '-discountNominal').val(), ',') * removePeriod($("#" + id + '-qty').val(), ',');
            var output = price * removePeriod($("#" + id + '-qty').val(), ',');
            diskon = diskon * output / 100;
            diskon = Math.round(diskon * 100) / 100;
            output = Math.round(output * 100) / 100;
            output = output - diskon - diskonNominal;
            output = Math.round(output * 100) / 100;
            output = addPeriodSales(output, ',');
            if (output.indexOf(".") == -1) {
                output = output + '.00';
            } else if (output.split('.')[1].length == 1) {
                output = output + '0';
            }
            document.getElementById(id + '-qty-hitung').innerHTML = output;
            hitungTotal()
        }
        $("#rate").keyup(function () {
            var rate = $("#rate").val();
            $(".price").each(function (i) {
                changeTotal($(this).attr('id'))
            });
            hitungTotal()
        });
        $(".price").keyup(function (e) {
            if ($(this).val() == '') {
            } else {
                changeTotal($(this).attr('id'))
            }
            hitungTotal();
            if ($(this).attr('id') != "price-0") {
                hitungTotalDeskripsi($(this).parent().parent().attr("data-parent"));
            }
        });
        $(".price").change(function (e) {
            if ($(this).val() == '') {
            }
            var id = $(this).attr('id');
            var price = document.getElementById(id).value;
            if (id == "price-0") {
                $("#oriprice-0").text(price);
            }
            price = removePeriod(price, ',');
//            console.log(price);
            if (document.getElementById("vat").checked == true) {
//                console.log("masuk");
                if ($("#typeTax").val() == "Include") {
//                    console.log("masuk1");
                    var priceTax = price / 1.1 * 0.1;
                    price = price - priceTax;
                    document.getElementById(id).value = price.toFixed(2);
                }
            }
            changeTotal($(this).attr('id'));
            hitungTotal();
            if (id != "price-0") {
                hitungTotalDeskripsi($(this).parent().parent().attr("data-parent"));
            }
        });
        $(".qty").keyup(function (e) {
            if ($(this).val() == '') {
            } else {
                changeTotal($(this).attr('id').substring(0, $(this).attr('id').length - 4))
            }
            hitungTotal();
            hitungTotalDeskripsi($(this).parent().parent().attr("data-parent"));
        });
        $(".qty").change(function (e) {
            if ($(this).val() == '') {
            }
            changeTotal($(this).attr('id').substring(0, $(this).attr('id').length - 4));
            hitungTotal();
            hitungTotalDeskripsi($(this).parent().parent().attr("data-parent"));
        });
        $(".qty").blur(function (e) {
            if ($(this).val() == 0) {
                $(this).val('1')
            }
            changeTotal($(this).attr('id').substring(0, $(this).attr('id').length - 4));
            hitungTotal()
        });
        $(".qtyDescription").change(function (e) {
//            console.log($(this).parent().parent().attr("data-parent"));
//            console.log($(this).parent().attr("data-parent"));
//            console.log($(this).attr("data-parent"));
//            console.log($(this).attr("id"));
//            console.log($(this).parent().parent().attr("id"));
            hitungTotalDeskripsi($(this).parent().parent().attr("id"));
        });
        $(".qtyDescription").change(function (e) {
            hitungTotalDeskripsi($(this).parent().parent().attr("id"));
        });
        $(".discount").keyup(function (e) {
            if ($(this).val() > 100) {
                $(this).val('100')
            }
//            if ($(this).val() < 0) {
//                $(this).val('0')
//            }
            changeTotal($(this).attr('id').substring(0, $(this).attr('id').length - 9));
            hitungTotal();
            hitungTotalDeskripsi($(this).parent().parent().attr("data-parent"));
        });
        $(".discount").change(function (e) {
            if ($(this).val() == '') {
            }
            if ($(this).val() > 100) {
                $(this).val('100')
            }
//            if ($(this).val() < 0) {
//                $(this).val('0')
//            }
            changeTotal($(this).attr('id').substring(0, $(this).attr('id').length - 9));
            hitungTotal();
            hitungTotalDeskripsi($(this).parent().parent().attr("data-parent"));
        });
        $(".discount").blur(function (e) {
            if ($(this).val() == 0) {
                $(this).val('0')
            }
            if ($(this).val() > 100) {
                $(this).val('100')
            }
//            if ($(this).val() < 0) {
//                $(this).val('0')
//            }
            changeTotal($(this).attr('id').substring(0, $(this).attr('id').length - 9));
            hitungTotal()
        });
        $(".discountNominal").keyup(function (e) {
            if ($(this).val() == '') {
            }
            changeTotal($(this).attr('id').substring(0, $(this).attr('id').length - 16));
            hitungTotal();
            hitungTotalDeskripsi($(this).parent().parent().attr("data-parent"));
        });
        $(".discountNominal").change(function (e) {
            if ($(this).val() == '') {
            }
            changeTotal($(this).attr('id').substring(0, $(this).attr('id').length - 16));
            hitungTotal();
            hitungTotalDeskripsi($(this).parent().parent().attr("data-parent"));
        });
        $(".discountNominal").blur(function (e) {
            if ($(this).val() == '') {
            }
            changeTotal($(this).attr('id').substring(0, $(this).attr('id').length - 16));
            hitungTotal()
        });
        $("#discountGlobal").keyup(function (e) {
            $(".price").each(function (i) {
                changeTotal($(this).attr('id'))
            });
            hitungTotal();
        });
        $("#discountGlobal").change(function (e) {
            $(".price").each(function (i) {
                changeTotal($(this).attr('id'))
            });
            hitungTotal()
        });
        $("#discountGlobal").blur(function (e) {
            $(".price").each(function (i) {
                changeTotal($(this).attr('id'))
            });
            hitungTotal()
        });
        hitungTotal()
    }

    function addPeriodSales(nStr, add) {
        nStr += '';
        x = nStr.split(add);
        x1 = x[0];
        x2 = x.length > 1 ? add + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + add + '$2')
        }
        return x1 + x2
    }
    function hitungTotal() {
        var total = 0;
        $(".subtotal").each(function (i) {
            var jumlah = document.getElementById($(this).attr('id')).innerHTML;
            total += parseFloat(removePeriod(jumlah, ','))
        });
        total = Math.round(total * 100) / 100;
        var diskon = removePeriod($("#discountGlobal").val(), ',');
        document.getElementById("total").innerHTML = addPeriodSales(total.toFixed(2), ',');

        if ($("#paymentCredit").attr('checked') == 'checked') {
            if (tampTotal != (total - diskon).toFixed(2)) {
                tampTotal = (total - diskon).toFixed(2);
                $.post(checkRecieveable, {total: tampTotal, coa6: $("#coa6_input").val()}).done(function (data) {
                    var tamp = data.split('---;---');
                    if (tamp[0] == "1") {
                        $("#alert_piutang").removeClass("none");
                        $("#hutang").html(addPeriod(tamp[2], ","));
                        $("#credit_limit").html(addPeriod(tamp[1], ","));
                    } else {
                        $("#alert_piutang").addClass("none");
                    }
                });
            }
        }

        document.getElementById("grandTotal").innerHTML = (addPeriodSales((total - diskon).toFixed(2), ','));
        var tamp = total - diskon;
        if (document.getElementById("vat").checked == true) {
            var tax = tamp / 10;
            tax = Math.round(tax * 100) / 100;
            document.getElementById("tax").innerHTML = addPeriodSales(tax.toFixed(2), ',');
            var grandTotal = tamp + tax;
            grandTotal = Math.round(grandTotal * 100) / 100;
            document.getElementById("grandTotalAfterTax").innerHTML = addPeriodSales(grandTotal.toFixed(2), ',');
            $("#grandTotalValue").val(grandTotal)
        } else {
            var tax = 0;
            $("#grandTotalValue").val(tamp);
            document.getElementById("tax").innerHTML = addPeriodSales(tax.toFixed(2), ',');
            document.getElementById("grandTotalAfterTax").innerHTML = addPeriodSales(tamp.toFixed(2), ',');
        }
        return total
    }

    function hitungTotalDeskripsi(row) {
//        alert(row);
        var rowDeskripsi = row.replace('rowDescription', '');
//        alert('[data-parent="rowDescription' + rowDeskripsi + '"]');
        var total = 0;
        $('[data-parent="rowDescription' + rowDeskripsi + '"]').find('td.subtotal').each(function (i) {
            var jumlah = document.getElementById($(this).attr('id')).innerHTML;
            total += parseFloat(removePeriod(jumlah, ','))
//            alert(total);
        });
        $("#priceDescription-" + rowDeskripsi + "-qty-hitung").text(addPeriod(total.toFixed(2), ','));
        var qty = parseFloat(removePeriod($('#priceDescription-' + rowDeskripsi + '-qty').val(), ","));
        var price = total / qty;
//        alert(total);
        $('#priceDescription-' + rowDeskripsi).val(addPeriod(price.toFixed(2), ','));
    }

    var config = {'.chosen-select': {}};
    for (var selector in config) {
        $(selector).chosen({
            search_contains: true
        });
    }
    $('.appd').find('a.chosen-single').each(function () {
        $(this).addClass('chosenapp');
        var added = $(this).after().addClass('chosenapp');
        added++;
        var end = $('td.appd:last').children().find('select').addClass('chosenapp');
        end++
    });

    $("#coa6_input").change(function () {
        hitungTotal();
        $.post(checkRecieveable, {total: tampTotal, coa6: $("#coa6_input").val()}).done(function (data) {
            var tamp = data.split('---;---');
            if (tamp[0] == "1") {
                $("#alert_piutang").removeClass("none");
                $("#hutang").html(addPeriod(tamp[2], ","));
                $("#credit_limit").html(addPeriod(tamp[1], ","));
            } else {
                $("#alert_piutang").addClass("none");
            }
        });
    });
    $("#paymentCredit").change(function () {
        hitungTotal();
        $.post(checkRecieveable, {total: tampTotal, coa6: $("#coa6_input").val()}).done(function (data) {
//            alert(data);
            var tamp = data.split('---;---');
            if (tamp[0] == "1") {
                $("#alert_piutang").removeClass("none");
                $("#hutang").html(addPeriod(tamp[2], ","));
                $("#credit_limit").html(addPeriod(tamp[1], ","));
            } else {
                $("#alert_piutang").addClass("none");
            }
        });
    });
    $("#paymentCash").change(function () {
        $("#alert_piutang").addClass("none");
    });

    $(".qty").keyup(function () {
        var id = $(this).attr("id").split("-");
        var tampVar = $("#inventory-" + id[1]).val();
        if (tampVar.indexOf("inventory") >= 0) {

        }
    });
//    $(".btn-deleteRow").click(function () {
//        if ($('#' + $(this).attr('data')).length > 0) {
//            document.getElementById($(this).attr('data')).remove();
//        }
//        hitungTotal();
//        hitungTotalDeskripsi($(this).parent().parent().attr("data-parent"));
//    });

    $("#paymentCash,#paymentCredit").change(function () {
        if ($('#paymentCash').attr('checked') == 'checked') {
            $('#paymentCash').removeAttr('checked');
            $('#paymentCredit').attr('checked', 'checked');
            $('#longTerm').prop('disabled', false);
            $('#slip').prop('disabled', true);
            $('#slip').trigger("chosen:updated")
        } else {
            $('#paymentCredit').removeAttr('checked');
            $('#paymentCash').attr('checked', 'checked');
            $('#longTerm').val(0);
            $('#longTerm').prop('disabled', true);
            $('#slip').prop('disabled', false);
            $('#slip').trigger("chosen:updated")
        }
    });
    $("#longTerm").blur(function () {
        if ($('#longTerm').val() == '' || $('#longTerm').val() < 0) {
            $('#longTerm').val(0)
        }
    });
//    var currency = $("#currencyHeader").val().split('---;---');
//    if (currency[3] == '1') {
//        $("#rate").prop('readonly', true);
//        $("#rate").css('background-color', '#eee')
//    } else {
//        $("#rate").prop('readonly', false);
//        $("#rate").css('background-color', '')
//    }
    $.validate();
//    $("#date").change(function () {
//        var cariText = '';
//        $('#salesOrderID').load(cariS, {"date": $("#date").val()})
//    });
    $("#typeTax").change(function () {
        if ($("#typeTax").val() == "Include") {
            //price
            var p = removePeriod($("#price-0").val(), ",");
            p = p / 1.1;
            $("#price-0").val(addPeriod(p.toFixed(2), ","));
        }
    });
    $(function () {
        $('#vat').change(function () {
            $('.hidevat').toggle(this.checked);
            checkTax();
        }).change();
    });
});