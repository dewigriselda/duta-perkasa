var config = {'.chosen-select': {}};
for (var selector in config) {
    $(selector).chosen({
        search_contains: true
    });
}
$(document).ready(function () {
    $("#searchSalesOrder").keydown(function (event) {
        if (event.keyCode == 13) { //enter
            event.preventDefault();
            $.post(getResultSearchSO, {id: $("#searchSalesOrder").val()}).done(function (data) {
                $("#selectSalesOrder").html(data);
            });
        }
    });

    $('#user-toggle').click(function () {
        if ($('#drop-user').hasClass('open')) {
            $('#user-toggle').removeClass('user-focus')
        } else {
            $('#user-toggle').addClass('user-focus')
        }
    });
    $('#searchwrap').hide();
    $('#search-button').click(function () {
        $('#searchwrap').slideToggle(300)
    });
    $('#cancel').click(function () {
        $('#searchwrap').slideUp(300)
    });
    $('#startDate').datepicker();
    $('#endDate').datepicker();
    $("#startDate").datepicker("option", "dateFormat", 'dd-mm-yy');
    $("#endDate").datepicker("option", "dateFormat", 'dd-mm-yy');
    $('.appd').find('a.chosen-single').each(function () {
        $(this).addClass('chosenapp')
    });
    var d = new Date();
    var day = d.getDate();
    var month = d.getMonth() + 1;
    if (month < 10)
        month = "0" + month;
    var year = d.getFullYear();
    var date = day + "-" + month + "-" + year;
    var startDate = "01" + "-" + month + "-" + year;
    $("#startDate").val(startDate);
    $("#endDate").val(date);
    $('#endDate, #startDate').change(function () {
        if ($('#startDate').val() == '') {
            $('#startDate').val($('#endDate').val())
        } else if ($('#endDate').val() == '') {
            $('#endDate').val($('#startDate').val())
        } else if (dateCheckHigher($('#startDate').val(), $('#endDate').val()) == 'start') {
            $('#endDate').val($('#startDate').val())
        }
    });
    $('#startDateReport').datepicker();
    $('#endDateReport').datepicker();
    $("#startDateReport").datepicker("option", "dateFormat", 'dd-mm-yy');
    $("#endDateReport").datepicker("option", "dateFormat", 'dd-mm-yy');
    $('#endDateReport, #startDateReport').change(function () {
        if ($('#startDateReport').val() == '') {
            $('#startDateReport').val($('#endDateReport').val())
        } else if ($('#endDateReport').val() == '') {
            $('#endDateReport').val($('#startDateReport').val())
        } else if (dateCheckHigher($('#startDateReport').val(), $('#endDateReport').val()) == 'start') {
            $('#endDateReport').val($('#startDateReport').val())
        }
    });

    $('#startDateTaxReport').datepicker();
    $('#endDateTaxReport').datepicker();
    $("#startDateTaxReport").datepicker("option", "dateFormat", 'dd-mm-yy');
    $("#endDateTaxReport").datepicker("option", "dateFormat", 'dd-mm-yy');
    $('#endDateTaxReport, #startDateTaxReport').change(function () {
        if ($('#startDateTaxReport').val() == '') {
            $('#startDateTaxReport').val($('#endDateTaxReport').val())
        } else if ($('#endDateTaxReport').val() == '') {
            $('#endDateTaxReport').val($('#startDateTaxReport').val())
        } else if (dateCheckHigher($('#startDateTaxReport').val(), $('#endDateTaxReport').val()) == 'start') {
            $('#endDateTaxReport').val($('#startDateTaxReport').val())
        }
    });
    
    $('#startDateSalesReport').datepicker();
    $('#endDateSalesReport').datepicker();
    $("#startDateSalesReport").datepicker("option", "dateFormat", 'dd-mm-yy');
    $("#endDateSalesReport").datepicker("option", "dateFormat", 'dd-mm-yy');
    $('#endDateSalesReport, #startDateSalesReport').change(function () {
        if ($('#startDateSalesReport').val() == '') {
            $('#startDateSalesReport').val($('#endDateSalesReport').val())
        } else if ($('#endDateSalesReport').val() == '') {
            $('#endDateSalesReport').val($('#startDateSalesReport').val())
        } else if (dateCheckHigher($('#startDateSalesReport').val(), $('#endDateSalesReport').val()) == 'start') {
            $('#endDateSalesReport').val($('#startDateSalesReport').val())
        }
    });
    $('#startDateCusmanagerReport').datepicker();
    $('#endDateCusmanagerReport').datepicker();
    $("#startDateCusmanagerReport").datepicker("option", "dateFormat", 'dd-mm-yy');
    $("#endDateCusmanagerReport").datepicker("option", "dateFormat", 'dd-mm-yy');
    $('#endDateCusmanagerReport, #startDateCusmanagerReport').change(function () {
        if ($('#startDateCusmanagerReport').val() == '') {
            $('#startDateCusmanagerReport').val($('#endDateCusmanagerReport').val())
        } else if ($('#endDateCusmanagerReport').val() == '') {
            $('#endDateCusmanagerReport').val($('#startDateCusmanagerReport').val())
        } else if (dateCheckHigher($('#startDateCusmanagerReport').val(), $('#endDateCusmanagerReport').val()) == 'start') {
            $('#endDateCusmanagerReport').val($('#startDateCusmanagerReport').val())
        }
    });

    window.deleteAttach = function (element) {
        $('#idDelete').val($(element).data('internal'));
    };
    window.printAttach = function (element) {
        $('#internalID').val($(element).data('internal'));
    }

    $("#btn-rSummary").click(function () {
        $('#jenisReport').val('summarySales');
        document.getElementById('titleReport').innerHTML = 'Summary Report'
    });
    $("#btn-rDetail").click(function () {
        $('#jenisReport').val('detailSales');
        document.getElementById('titleReport').innerHTML = 'Detail Report'
    });
    $("#btn-report-transaction").click(function () {
        if ($('#startDateReport').val() == '' && $('#endDateReport').val() == '') {
            var tanggal = new Date();
            var tanggalText = tanggal.getDate() + '-' + (tanggal.getMonth() + 1) + '-' + tanggal.getFullYear();
            $('#startDateReport').val(tanggalText);
            $('#endDateReport').val($('#startDateReport').val())
        } else if ($('#startDateReport').val() == '') {
            $('#startDateReport').val($('#endDateReport').val())
        } else if ($('#endDateReport').val() == '') {
            $('#endDateReport').val($('#startDateReport').val())
        } else if (dateCheckHigher($('#startDateReport').val(), $('#endDateReport').val()) == 'start') {
            $('#endDateReport').val($('#startDateReport').val())
        }
    });

    $('body').on('click', '.btn-toggle-detail', function () {
        if ($(this).attr('data-toshow') == 'show') {
            $('.' + $(this).attr('data')).hide();
            $(this).attr('data-toshow', 'hide');
        } else {
            $('.' + $(this).attr('data')).show();
            $(this).attr('data-toshow', 'show');
        }
    });

    $('body').on('click', '.btn-toggle-spec', function () {
        if ($(this).attr('data-toshow') == 'show') {
            $('.' + $(this).attr('data')).hide();
            $(this).attr('data-toshow', 'hide');
        } else {
            $('.' + $(this).attr('data')).show();
            $(this).attr('data-toshow', 'show');
        }
    });

    $('#example').dataTable({
        "order": [[ 1, "desc" ]],
        "draw": 10,
        "processing": true,
        "serverSide": true,
        "ajax": salesDataBackup
    })
});