//document ready biasa
$(document).ready(function () {
    var tampTotal;
    var config = {
        '.chosen-select': {}
    };
    for (var selector in config) {
        $(selector).chosen({
            search_contains: true
        });
    }
    var print = $("#print").val()
    if (typeof print !== 'undefined') {
        window.open(print);
    }
    $.validate();

    $('#searchwrap').hide();
    $('#search-button').click(function () {
        $('#searchwrap').slideToggle(300)
    });
    $('#cancel').click(function () {
        $('#searchwrap').slideUp(300)
    });
    $("#searchInventory").keydown(function (event) {
        if (event.keyCode == 13) { //enter
            event.preventDefault();
            $.post(getSearchResultInventoryForSO, {id: $("#searchInventory").val()}).done(function (data) {
                $("#selectInventory").html(data);
            });
        }
    });
    $("#searchSalesOrder").keydown(function (event) {
        if (event.keyCode == 13) { //enter
            event.preventDefault();
            $.post(getResultSearchSO, {id: $("#searchSalesOrder").val()}).done(function (data) {
                $("#selectSalesOrder").html(data);
            });
        }
    });
    $('#startDateTaxReport').datepicker();
    $('#endDateTaxReport').datepicker();
    $("#startDateTaxReport").datepicker("option", "dateFormat", 'dd-mm-yy');
    $("#endDateTaxReport").datepicker("option", "dateFormat", 'dd-mm-yy');
    $('#endDateTaxReport, #startDateTaxReport').change(function () {
        if ($('#startDateTaxReport').val() == '') {
            $('#startDateTaxReport').val($('#endDateTaxReport').val())
        } else if ($('#endDateTaxReport').val() == '') {
            $('#endDateTaxReport').val($('#startDateTaxReport').val())
        } else if (dateCheckHigher($('#startDateTaxReport').val(), $('#endDateTaxReport').val()) == 'start') {
            $('#endDateTaxReport').val($('#startDateTaxReport').val())
        }
    });
    $('#date').datepicker();
    $("#date").datepicker("option", "dateFormat", 'dd-mm-yy');

    $('#startDate').datepicker();
    $('#endDate').datepicker();
    $("#startDate").datepicker("option", "dateFormat", 'dd-mm-yy');
    $("#endDate").datepicker("option", "dateFormat", 'dd-mm-yy');
    $('.appd').find('a.chosen-single').each(function () {
        $(this).addClass('chosenapp')
    });
    $('#endDate, #startDate').change(function () {
        if ($('#startDate').val() == '') {
            $('#startDate').val($('#endDate').val())
        } else if ($('#endDate').val() == '') {
            $('#endDate').val($('#startDate').val())
        } else if (dateCheckHigher($('#startDate').val(), $('#endDate').val()) == 'start') {
            $('#endDate').val($('#startDate').val())
        }
    });
    $('#startDateReport').datepicker();
    $('#endDateReport').datepicker();
    $("#startDateReport").datepicker("option", "dateFormat", 'dd-mm-yy');
    $("#endDateReport").datepicker("option", "dateFormat", 'dd-mm-yy');
    $('#endDateReport, #startDateReport').change(function () {
        if ($('#startDateReport').val() == '') {
            $('#startDateReport').val($('#endDateReport').val())
        } else if ($('#endDateReport').val() == '') {
            $('#endDateReport').val($('#startDateReport').val())
        } else if (dateCheckHigher($('#startDateReport').val(), $('#endDateReport').val()) == 'start') {
            $('#endDateReport').val($('#startDateReport').val())
        }
    });
    window.deleteAttach = function (element) {
        $('#idDelete').val($(element).data('internal'));
    };
    window.closeAttach = function (element) {
        $('#idClose').val($(element).data('internal'));
    };
    window.approveAttach = function (element) {
        $('#idApprove').val($(element).data('internal'));
        $('#approveName').text($(element).data('name'));
    };
    window.printAttach = function (element) {
        $('#internalID').val($(element).data('internal'));
    };
    $("#btn-rSummary").click(function () {
        $('#jenisReport').val('summarySalesOrder');
        document.getElementById('titleReport').innerHTML = 'Summary Report'
    });
    $("#btn-rDetail").click(function () {
        $('#jenisReport').val('detailSalesOrder');
        document.getElementById('titleReport').innerHTML = 'Detail Report'
    });
    $("#btn-report-transaction").click(function () {
        if ($('#startDateReport').val() == '' && $('#endDateReport').val() == '') {
            var tanggal = new Date();
            var tanggalText = tanggal.getDate() + '-' + (tanggal.getMonth() + 1) + '-' + tanggal.getFullYear();
            $('#startDateReport').val(tanggalText);
            $('#endDateReport').val($('#startDateReport').val())
        } else if ($('#startDateReport').val() == '') {
            $('#startDateReport').val($('#endDateReport').val())
        } else if ($('#endDateReport').val() == '') {
            $('#endDateReport').val($('#startDateReport').val())
        } else if (dateCheckHigher($('#startDateReport').val(), $('#endDateReport').val()) == 'start') {
            $('#endDateReport').val($('#startDateReport').val())
        }
    });
    $('body').on('click', '.btn-toggle-detail', function () {
        if ($(this).attr('data-toshow') == 'show') {
            $('.' + $(this).attr('data')).hide();
            $(this).attr('data-toshow', 'hide');
        } else {
            $('.' + $(this).attr('data')).show();
            $(this).attr('data-toshow', 'show');
        }
    });

    $('body').on('click', '.btn-toggle-spec', function () {
        if ($(this).attr('data-toshow') == 'show') {
            $('.' + $(this).attr('data')).hide();
            $(this).attr('data-toshow', 'hide');
        } else {
            $('.' + $(this).attr('data')).show();
            $(this).attr('data-toshow', 'show');
        }
    });

    $('#example').dataTable({
        "draw": 10,
        "processing": true,
        "serverSide": true,
        "ajax": salesDataBackup
    })


    //==============var global =============
    var baris = 1;
    //=============/var global =============

    function discountMaster(urutan) {
        var subTotal = 0;

        //hitung sub total
        //sini

        var id = $('#inventory-' + urutan).attr("id");
        var tampID = id.split("-");
        subTotal += parseFloat(removePeriod($("#price-" + tampID[1] + "-qty-hitung").html(), ","));
        //hitung sub total

        //hasil rule adalah qty
        var date = $("#date").val().split("-");
        $.post(getDiscountInventory, {inventory: $('#inventory-' + urutan).val(), date: date[2] + '-' + date[1] + '-' + date[0]}).done(function (data) {
            if (data != 0) {
                var tamp = data.split("---;---");
                var qty = removePeriod($("#price-" + tampID[1] + "-qty").val(), ",");
                //bandingkan qtynya jika qty nya memenuhi maka diskon jalan
                if (parseFloat(qty) >= parseFloat(tamp[0])) {
                    //lebih besar sama dengan maka diskon berlaku 
                    $("#price-" + tampID[1] + "-discount").val(tamp[1]);
                    changeTotal("price-" + tampID[1]);
                } else {
                    $("#price-" + tampID[1] + "-discount").val(0);
                    changeTotal("price-" + tampID[1]);
                }
            } else {
                $("#price-" + tampID[1] + "-discount").val(0);
                changeTotal("price-" + tampID[1]);
            }
        });
        discountTotal();
    }

    function discountMasterAll() {
        $(".inventory").each(function (i) {
            var urutan = $(this).attr('id').split('-')[1];
            var subTotal = 0;
            //hitung sub total
            //sini
            var id = $('#inventory-' + urutan).attr("id");
            var tampID = id.split("-");
            subTotal += parseFloat(removePeriod($("#price-" + tampID[1] + "-qty-hitung").html(), ","));
            //hitung sub total

            //hasil rule adalah qty
            var date = $("#date").val().split("-");
            $.post(getDiscountInventory, {inventory: $('#inventory-' + urutan).val(), date: date[2] + '-' + date[1] + '-' + date[0]}).done(function (data) {
                if (data != 0) {
                    var tamp = data.split("---;---");
                    var qty = removePeriod($("#price-" + tampID[1] + "-qty").val(), ",");
                    //bandingkan qtynya jika qty nya memenuhi maka diskon jalan
                    if ($("#price-" + tampID[1] + "-discount").val() == 0) {
                        if (parseFloat(qty) >= parseFloat(tamp[0])) {
                            //lebih besar sama dengan maka diskon berlaku 
                            $("#price-" + tampID[1] + "-discount").val(tamp[1]);
                            changeTotal("price-" + tampID[1]);
                        } else {
                            $("#price-" + tampID[1] + "-discount").val(0);
                            changeTotal("price-" + tampID[1]);
                        }
                    }
                } else {
                    if ($("#price-" + tampID[1] + "-discount").val() == 0) {
                        $("#price-" + tampID[1] + "-discount").val(0);
                        changeTotal("price-" + tampID[1]);
                    }
                }
            });
        });
        discountTotal();
    }

    function discountTotal() {
        var subTotal = 0;
        $(".subtotal").each(function (i) {
            var jumlah = document.getElementById($(this).attr('id')).innerHTML;
            subTotal += parseFloat(removePeriod(jumlah, ','));
        });
        subTotal = Math.round(subTotal * 100) / 100;
        //get total discount jika ada
        var date = $("#date").val().split("-");
        $.post(getDiscountForTotal, {date: date[2] + '-' + date[1] + '-' + date[0], customer: $("#coa6").val()}).done(function (data) {
            var tamp = data.split("---;---");
            if (tamp[0] != 'AA') {
                //bandingkan subtotal
                if (parseFloat(subTotal) >= parseFloat(tamp[0])) {
                    //lebih besar sama dengan maka diskon berlaku 
                    //sini
                    var totalSetelahDiscount = subTotal * (tamp[1] / 100);
                    var totalSetelahDiscount2 = totalSetelahDiscount * (tamp[2] / 100);
                    if (totalSetelahDiscount2 == 0) {
                        totalSetelahDiscount2 = totalSetelahDiscount;
                    }
                    $("#discountGlobal").val(addPeriodSales(totalSetelahDiscount2, ','));
                } else {
                    var totalSetelahDiscount2 = subTotal * (tamp[2] / 100);
                    $("#discountGlobal").val(addPeriodSales(totalSetelahDiscount2, ','));
                }
            } else {
                var totalSetelahDiscount2 = subTotal * (tamp[1] / 100);
                $("#discountGlobal").val(addPeriodSales(totalSetelahDiscount2, ','));
            }
            hitungTotal();
        });
    }

    //===================function===============
    if ($('#paymentCash').attr('checked') == 'checked') {
        $('#longTerm').val(0);
        $('#longTerm').prop('disabled', true);
        $('#slip').prop('disabled', false);
        $('#slip').trigger("chosen:updated");
        $('.cash').css("display", "");
        $('.credit').css("display", "none");
    } else {
        $('#longTerm').prop('disabled', false);
        $('#slip').prop('disabled', true);
        $('#slip').trigger("chosen:updated");
        $('.credit').css("display", "");
        $('.cash').css("display", "none");
    }
    $("#paymentCash,#paymentCredit").change(function () {
        if ($('#paymentCredit').attr('checked') == 'checked') {
            $('.cash').css("display", "");
            $('.credit').css("display", "none");
        } else {
            $('.credit').css("display", "");
            $('.cash').css("display", "none");
        }
    });
//
//semua yang merupakan auto change
    function autoChangeTotal() {
        //untuk cari total
        myFunctionduit();
        //currency berubah
        $("#currencyHeader").change(function () {
            var currencyHeader = $("#currencyHeader").val().split('---;---');
            $("#rate").val(addPeriod(currencyHeader[2], ','));
            var rate = $("#rate").val();
            $(".price").each(function (i) {
                changeTotal($(this).attr('id'))
            });
            var currency = $("#currencyHeader").val().split('---;---');
            if (currency[3] == '1') {
                $("#rate").prop('readonly', true);
                $("#rate").css('background-color', '#eee');
            } else {
                $("#rate").prop('readonly', false);
                $("#rate").css('background-color', '');
            }
            hitungTotal();
        });
        //add period tanpa remove
        function addPeriodSales(nStr, add)
        {
            nStr += '';
            x = nStr.split(add);
            x1 = x[0];
            x2 = x.length > 1 ? add + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + add + '$2');
            }
            return x1 + x2;
        }
        //change subtotal function
        function changeTotal(id) {
            var price = document.getElementById(id).value;
            price = removePeriod(price, ',');
            var diskon = $("#" + id + '-discount').val();
            var diskonNominal = removePeriod($("#" + id + '-discountNominal').val(), ',') * removePeriod($("#" + id + '-qty').val(), ',');
            var output = price * removePeriod($("#" + id + '-qty').val(), ',');
            diskon = diskon * output / 100;
            //pengenapan diskon 2 angka belakang koma.
            diskon = Math.round(diskon * 100) / 100;
            //pengenapan price 2 angka belakang koma.
            output = Math.round(output * 100) / 100;
            output = output - diskon - diskonNominal;
            //pengenapan price 2 angka belakang koma.
            output = Math.round(output * 100) / 100;
            output = addPeriodSales(output, ',');
            if (output.indexOf(".") == -1) {
                output = output + '.00';
            } else if (output.split('.')[1].length == 1) {
                output = output + '0';
            }
            document.getElementById(id + '-qty-hitung').innerHTML = output;
            hitungTotal();
        }
        //kurs berubah
        $("#rate").keyup(function () {
            var rate = $("#rate").val();
            $(".price").each(function (i) {
                changeTotal($(this).attr('id'))
            });
            discountMasterAll();
            hitungTotal();
        });
        $(".price").keyup(function (e) {
            if ($(this).val() == '') {
            } else {
                changeTotal($(this).attr('id'));
            }
            discountMasterAll();
            hitungTotal();
        });
        $(".price").change(function (e) {
            if ($(this).val() == '') {
            }
            changeTotal($(this).attr('id'));
            discountMasterAll();
            hitungTotal();
        });
        $(".qty").keyup(function (e) {
            if ($(this).val() == '') {
            } else {
                changeTotal($(this).attr('id').substring(0, $(this).attr('id').length - 4));
            }
            discountMasterAll();
            hitungTotal();
        });
        $(".qty").change(function (e) {
            if ($(this).val() == '') {
            }
            changeTotal($(this).attr('id').substring(0, $(this).attr('id').length - 4));
            discountMasterAll();
            hitungTotal();
        });
        $(".qty").blur(function (e) {
            if ($(this).val() == 0) {
                $(this).val('1');
            }
            changeTotal($(this).attr('id').substring(0, $(this).attr('id').length - 4));
            discountMasterAll();
            hitungTotal();
        });
        $(".discount").keyup(function (e) {
            if ($(this).val() > 100) {
                $(this).val('100');
            }
            if ($(this).val() < 0) {
                $(this).val('0');
            }
            changeTotal($(this).attr('id').substring(0, $(this).attr('id').length - 9));
            discountTotal();
            hitungTotal();
        });
        $(".discount").change(function (e) {
            if ($(this).val() > 100) {
                $(this).val('100');
            }
            if ($(this).val() < 0) {
                $(this).val('0');
            }
            changeTotal($(this).attr('id').substring(0, $(this).attr('id').length - 9));
            discountTotal();
            hitungTotal();
        });
        $(".discount").blur(function (e) {
            if ($(this).val() > 100) {
                $(this).val('100');
            }
            if ($(this).val() < 0) {
                $(this).val('0');
            }
            changeTotal($(this).attr('id').substring(0, $(this).attr('id').length - 9));
            discountTotal();
            hitungTotal();
        });
        $(".discountNominal").keyup(function (e) {
            changeTotal($(this).attr('id').substring(0, $(this).attr('id').length - 16));
            discountTotal();
            hitungTotal();
        });
        $(".discountNominal").change(function (e) {
            changeTotal($(this).attr('id').substring(0, $(this).attr('id').length - 16));
            discountTotal();
            hitungTotal();
        });
        $(".discountNominal").blur(function (e) {
            changeTotal($(this).attr('id').substring(0, $(this).attr('id').length - 16));
            discountTotal();
            hitungTotal();
        });
        $("#discountGlobal").keyup(function (e) {
            $(".price").each(function (i) {
                changeTotal($(this).attr('id'));
            });
            hitungTotal();
        });
        $("#discountGlobal").change(function (e) {
            $(".price").each(function (i) {
                changeTotal($(this).attr('id'));
            });
            hitungTotal();
        });
        $("#discountGlobal").blur(function (e) {
            $(".price").each(function (i) {
                changeTotal($(this).attr('id'));
            });
            hitungTotal();
        });

        hitungTotal();
    }

//change price
    $(".inventory").change(function () {
        //harus ambil uom baru dlu
        var split = $(this).attr('id').split('-');

        var uom = 0;
        var inventory = 0;
        var price = 0;
        var tampInventory = 0;
        var arrInventory;
        tampInventory = $(this).attr('id');
        var tampVar = $(this).find('option:selected').attr('id');

        if (tampVar.indexOf("inventory") >= 0) {
            //found inventory
            $.post(getUomThisInventorySalesOrder, {id: $(this).val()}).done(function (data) {
                $("#uom-" + split[1]).attr("readonly", false);
                $("#uom-" + split[1]).html(data);

                //tambahan
                price = $("#priceCategory").val();
                inventory = $("#" + tampInventory).val();
                arrInventory = tampInventory.split("-");
                uom = $("#uom-" + arrInventory[1]).val();
                $.post(getPriceCategoryThisInventory, {PriceID: price, InventoryID: inventory, UomID: uom, urutan: arrInventory[1]}).done(function (data) {
                    var arrData = data.split("---;---");
                    if (arrData[0] != 0) {
                        $("#price-" + arrData[1]).val(addPeriodSales(arrData[0].trim(), ","));
                        changeTotal($("#price-" + arrData[1]).attr("id"));
                    }
                    discountMaster(arrData[1]);
                    hitungTotal();
                });
                //tambahan
            });
        } else if (tampVar.indexOf("parcel") >= 0) {
            //found parcel
            $("#uom-" + split[1]).attr("readonly", true);
            $("#uom-" + split[1]).html("<option value='0'>-</option>");
            $("#uom-" + split[1]).trigger("chosen:updated");
            var value = $(this).val().split("---;---");
            //get price parcel
            $.post(getPriceThisParcel, {id: value[0]}).done(function (data) {
                $("#price-" + split[1]).val(addPeriod(data.trim(), ","));
                changeTotal($("#price-" + split[1]).attr("id"));
            });
        }
    });

    $(".uom").change(function () {
        var uom = 0;
        var inventory = 0;
        var price = 0;
        var tampUom = 0;
        var arrUom;

        price = $("#priceCategory").val();
        tampUom = $(this).attr('id');
        uom = $("#" + tampUom).val();
        arrUom = tampUom.split("-");
        inventory = $("#inventory-" + arrUom[1]).val();

        $.post(getPriceCategoryThisInventory, {PriceID: price, InventoryID: inventory, UomID: uom, urutan: arrUom[1]}).done(function (data) {
            var arrData = data.split("---;---");
            if (arrData[0] != 0) {
                $("#price-" + arrData[1]).val(addPeriodSales(arrData[0].trim(), ","));
                changeTotal($("#price-" + arrData[1]).attr("id"));
            }
            discountMasterAll();
            hitungTotal();
        });
    });
//change price

//perhitungan total debet dan kredit
    function hitungTotal() {
        var total = 0;
        $(".subtotal").each(function (i) {
            var jumlah = document.getElementById($(this).attr('id')).innerHTML;
            total += parseFloat(removePeriod(jumlah, ','));
        });
        total = Math.round(total * 100) / 100;
        var diskon = removePeriod($("#discountGlobal").val(), ',');
        document.getElementById("total").innerHTML = addPeriodSales(total.toFixed(2), ',');

        if ($("#paymentCredit").attr('checked') == 'checked') {

            if (tampTotal != (total - diskon).toFixed(2)) {
                tampTotal = (total - diskon).toFixed(2);
                $.post(checkRecieveable, {total: tampTotal, coa6: $("#coa6").val()}).done(function (data) {
                    var tamp = data.split('---;---');
                    if (tamp[0] == "1") {
                        $("#alert_piutang").removeClass("none");
                        $("#hutang").html(addPeriod(tamp[2], ","));
                        $("#credit_limit").html(addPeriod(tamp[1], ","));
                    } else {
                        $("#alert_piutang").addClass("none");
                    }
                });
            }
        }

        document.getElementById("grandTotal").innerHTML = (addPeriodSales((total - diskon).toFixed(2), ','));
        var tamp = total - diskon;
        if (document.getElementById("vat").checked == true) {
            var tax = tamp / 10;
            tax = Math.round(tax * 100) / 100;
            document.getElementById("tax").innerHTML = addPeriodSales(tax.toFixed(2), ',');
            var grandTotal = tamp + tax;
            grandTotal = Math.round(grandTotal * 100) / 100;
            document.getElementById("grandTotalAfterTax").innerHTML = addPeriodSales(grandTotal.toFixed(2), ',');
            $("#grandTotalValue").val(grandTotal);
        } else {
            $("#grandTotalValue").val(tamp);
            document.getElementById("tax").innerHTML = '';
            document.getElementById("grandTotalAfterTax").innerHTML = '';
        }
        return total;
    }

    function addPeriodSales(nStr, add)
    {
        nStr += '';
        x = nStr.split(add);
        x1 = x[0];
        x2 = x.length > 1 ? add + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + add + '$2');
        }
        return x1 + x2;
    }
//==================/function===============



    $('.appd').find('a.chosen-single').each(function () {
        $(this).addClass('chosenapp');
        var added = $(this).after().addClass('chosenapp');
        added++;
        var end = $('td.appd:last').children().find('select').addClass('chosenapp');
        end++;
    });
    $('#date').datepicker();
    $("#date").datepicker("option", "dateFormat", 'dd-mm-yy');
    $('#date').val(tanggalHariIni);
    autoChangeTotal();
    hitungTotal();
    $("#vat").change(function (e) {
        hitungTotal();
    });

//==================change price==================
//change subtotal function
    function changeTotal(id) {
        var price = document.getElementById(id).value;
        price = removePeriod(price, ',');
        var diskon = $("#" + id + '-discount').val();
        var diskonNominal = removePeriod($("#" + id + '-discountNominal').val(), ',') * removePeriod($("#" + id + '-qty').val(), ',');
        var output = price * removePeriod($("#" + id + '-qty').val(), ',');
        diskon = diskon * output / 100;
        //pengenapan diskon 2 angka belakang koma.
        diskon = Math.round(diskon * 100) / 100;
        //pengenapan price 2 angka belakang koma.
        output = Math.round(output * 100) / 100;
        output = output - diskon - diskonNominal;
        //pengenapan price 2 angka belakang koma.
        output = Math.round(output * 100) / 100;
        output = addPeriodSales(output, ',');
        if (output.indexOf(".") == -1) {
            output = output + '.00';
        } else if (output.split('.')[1].length == 1) {
            output = output + '0';
        }
        document.getElementById(id + '-qty-hitung').innerHTML = output;
        hitungTotal();
    }

    $("#priceCategory").change(function () {
        var uom = 0;
        var inventory = 0;
        var price = 0;
        var tampInventory = 0;
        var arrInventory;

        price = $(this).val();



        if (price != 0) {
            $(".inventory").each(function (i) {
                tampInventory = $(this).attr('id');
                inventory = $("#" + tampInventory).val();
                arrInventory = tampInventory.split("-");
                uom = $("#uom-" + arrInventory[1]).val();
                var tampVar = $(this).find('option:selected').attr('id');
                if (tampVar.indexOf("inventory") >= 0) {

                    $.post(getPriceCategoryThisInventory, {PriceID: price, InventoryID: inventory, UomID: uom, urutan: arrInventory[1]}).done(function (data) {
                        var arrData = data.split("---;---");
                        $("#price-" + arrData[1]).val(addPeriodSales(arrData[0].trim(), ","));
                        changeTotal($("#price-" + arrData[1]).attr("id"));
                        discountMasterAll();
                        hitungTotal();
                    });
                }
            });

        }
    });
//====================change price========================

    $("#paymentCash,#paymentCredit").change(function () {
        if ($('#paymentCash').attr('checked') == 'checked') {
            $('#paymentCash').removeAttr('checked');
            $('#paymentCredit').attr('checked', 'checked');
            $('#longTerm').prop('disabled', false);
            $('#slip').prop('disabled', true);
            $('#slip').trigger("chosen:updated");
        } else {
            $('#paymentCredit').removeAttr('checked');
            $('#paymentCash').attr('checked', 'checked');
            $('#longTerm').val(0);
            $('#longTerm').prop('disabled', true);
            $('#slip').prop('disabled', false);
            $('#slip').trigger("chosen:updated");
        }
    });
    $("#longTerm").blur(function () {
        if ($('#longTerm').val() == '' || $('#longTerm').val() < 0) {
            $('#longTerm').val(0);
        }
    });
    var currency = $("#currencyHeader").val().split('---;---');
    if (currency[3] == '1') {
        $("#rate").prop('readonly', true);
        $("#rate").css('background-color', '#eee');
    } else {
        $("#rate").prop('readonly', false);
        $("#rate").css('background-color', '');
    }

//Insert row waktu button add
    $("#btn-addRow").click(function () {
        var cur = $('#currencyHeader').val().split('---;---');
        $('#table-salesOrder tr:last').after('<tr id="row' + baris + '">' +
                '<td class="chosen-uom" >' +
                '<div id="price-range-' + baris + '">' +
                '<input type="hidden" class="inventory" style="width: 100px" id="inventory-' + baris + '" style="" name="inventory[]" value="' + $('#inventory-0').val() + '">' +
                $("#inventory-0 option[value='" + $("#inventory-0").val() + "']").text() +
                '</div></td>' +
                '<td>' +
                '<select id="uom-' + baris + '" name="uom[]" class="input-theme uom">' + $('#uom-0').html() + '</select>' +
                '</td>' +
                '<td class="text-right">' +
                '<input type="text" class="maxWidth qty right input-theme" name="qty[]" maxlength="11" min="1" value="1" id="price-' + baris + '-qty">' +
                '</td>' +
                '<td class="text-right">' +
                '<input type="text" class="maxWidth price right numajaDesimal input-theme" name="price[]" maxlength="" value="0.00" id="price-' + baris + '">' +
                '</td>' +
                '<td class="text-right">' +
                '<input type="text" ' + statDiscount + ' class="maxWidth discount right numajaDesimal input-theme" name="discount[]" min="0" max="100" id="price-' + baris + '-discount" value="0.00">' +
                '</td>' +
                '<td class="text-right">' +
                '<input type="text" ' + statDiscount + ' class="maxWidth discountNominal right numajaDesimal input-theme" name="discountNominal[]" id="price-' + baris + '-discountNominal" value="0.00">' +
                '</td>' +
                '<td class="right subtotal" id="price-' + baris + '-qty-hitung">' +
                '0.00' +
                '</td>' +
                '<td>' +
                '<button class="btn btn-pure-xs btn-xs btn-deleteRow" type="button" data="row' + baris + '"><span class="glyphicon glyphicon-trash"></span></button>' +
                '</td>' +
                '</tr>'
                );

        //TAMBAHAN
//        var price = $("#priceCategory").val();
//        var inventory = $("#inventory-" + baris).val();
//        var uom = $("#uom-" + baris).val();
//        $.post(getPriceCategoryThisInventory, {PriceID: price, InventoryID: inventory, UomID: uom, urutan: baris}).done(function (data) {
//            var arrData = data.split("---;---");
//            if (arrData[0] != 0) {
//                $("#price-" + arrData[1]).val(addPeriodSales(arrData[0].trim(), ","));
//                changeTotal($("#price-" + arrData[1]).attr("id"));
//            }
//            discountMaster(arrData[1]);
//            hitungTotal();
//        });
        //TAMBAHAN

        $('#uom-' + baris).val($('#uom-0').val());
        var tampVar = $("#inventory-0").find('option:selected').attr('id');
        $('#price-' + baris + '-qty').val($('#price-0-qty').val());
        $('#price-' + baris).val($('#price-0').val());
        $('#price-' + baris + '-discount').val($('#price-0-discount').val());
        $('#price-' + baris + '-discountNominal').val($('#price-0-discountNominal').val());
        $('#price-' + baris + '-qty-hitung').text($('#price-0-qty-hitung').text());

        //button delete row
        $(".btn-deleteRow").click(function () {
            if ($('#' + $(this).attr('data')).length > 0) {
                document.getElementById($(this).attr('data')).remove();
            }
            discountMasterAll();
            hitungTotal();
        });
        $(".numajaDesimal").keypress(function (e) {
            if ((e.charCode >= 48 && e.charCode <= 57) || (e.charCode == 0) || (e.charCode == 46))
                return true;
            else
                return false;
        });

        $(".numajaDesimal").blur(function (e) {
            if ($(this).val() != '') {
                var value = removePeriod($(this).val(), ',');
                var hasil = parseFloat(value).toFixed(2);
                $(this).val(addPeriod(hasil, ','));
            }
        });

        //select untuk element baru
        var config = {
            '.chosen-select': {}};
        for (var selector in config) {
            $(selector).chosen({
                search_contains: true
            });
        }
        $('.appd').find('a.chosen-single').each(function () {
            $(this).addClass('chosenapp');
            var added = $(this).after().addClass('chosenapp');
            added++;
            var end = $('td.appd:last').children().find('select').addClass('chosenapp');
            end++;
        });
        baris++;

        //function utk element baru
        autoChangeTotal();
    });

//uom inventory
    /*
     $(".inventory").change(function () {
     var split = $(this).attr('id').split('-');
     $.post(getUomThisInventory, {id: $(this).val()}).done(function (data) {
     $("#uom-" + split[1]).html(data);
     });
     });
     */

//change price
    $(".inventory").change(function () {
        //harus ambil uom baru dlu
        var split = $(this).attr('id').split('-');

        var uom = 0;
        var inventory = 0;
        var price = 0;
        var tampInventory = 0;
        var arrInventory;
        tampInventory = $(this).attr('id');
        var tampVar = $(this).find('option:selected').attr('id');

        if (tampVar.indexOf("inventory") >= 0) {
            //found inventory
            $.post(getUomThisInventorySalesOrder, {id: $(this).val()}).done(function (data) {
                $("#uom-" + split[1]).attr("readonly", false);
                $("#uom-" + split[1]).html(data);

                //tambahan
                price = $("#priceCategory").val();
                inventory = $("#" + tampInventory).val();
                arrInventory = tampInventory.split("-");
                uom = $("#uom-" + arrInventory[1]).val();
                $.post(getPriceCategoryThisInventory, {PriceID: price, InventoryID: inventory, UomID: uom, urutan: arrInventory[1]}).done(function (data) {
                    var arrData = data.split("---;---");
                    if (arrData[0] != 0) {
                        $("#price-" + arrData[1]).val(addPeriodSales(arrData[0].trim(), ","));
                        changeTotal($("#price-" + arrData[1]).attr("id"));
                    }
                    discountMaster(arrData[1]);
                    hitungTotal();
                });
                //tambahan
            });
        } else if (tampVar.indexOf("parcel") >= 0) {
            //found parcel 
            $("#uom-" + split[1]).attr("readonly", true);
            $("#uom-" + split[1]).html("<option value='0'>-</option>");
            $("#uom-" + split[1]).trigger("chosen:updated");
            var value = $(this).val().split("---;---");
            //get price parcel
            $.post(getPriceThisParcel, {id: value[0]}).done(function (data) {
                $("#price-" + split[1]).val(addPeriod(data.trim(), ","));
                changeTotal($("#price-" + split[1]).attr("id"));
            });

        }
    });

    $("#coa6").change(function () {
        hitungTotal();
        $.post(checkRecieveable, {total: tampTotal, coa6: $("#coa6").val()}).done(function (data) {
            var tamp = data.split('---;---');
            if (tamp[0] == "1") {
                $("#alert_piutang").removeClass("none");
                $("#hutang").html(addPeriod(tamp[2], ","));
                $("#credit_limit").html(addPeriod(tamp[1], ","));
            } else {
                $("#alert_piutang").addClass("none");
            }
        });
    });
    $("#paymentCredit").change(function () {
        hitungTotal();
        $.post(checkRecieveable, {total: tampTotal, coa6: $("#coa6").val()}).done(function (data) {
            var tamp = data.split('---;---');
            if (tamp[0] == "1") {
                $("#alert_piutang").removeClass("none");
                $("#hutang").html(addPeriod(tamp[2], ","));
                $("#credit_limit").html(addPeriod(tamp[1], ","));
            } else {
                $("#alert_piutang").addClass("none");
            }
        });
    });
    $("#paymentCash").change(function () {
        $("#alert_piutang").addClass("none");
    });

    $("#date").change(function () {
        var cariText = '';
        $('#salesOrderID').load(cariS, {
            "date": $("#date").val()
        });
        discountMasterAll();
    });
    $(function () {
        $('#vat').change(function () {
            $('.hidevat').toggle(this.checked);
        }).change(); //ensure visible state matches initially
    });
});