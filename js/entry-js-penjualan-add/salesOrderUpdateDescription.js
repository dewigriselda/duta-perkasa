$(document).ready(function () {
    description = JSON.parse(description);
     
    function changeTotalDescription(id) {
        var price = document.getElementById(id).value;
        price = removePeriod(price, ',');
        var diskon = $("#" + id + '-discount').val();
        var diskonNominal = removePeriod($("#" + id + '-discountNominal').val(), ',') * removePeriod($("#" + id + '-qty').val(), ',');
        var output = price * removePeriod($("#" + id + '-qty').val(), ',');
        diskon = diskon * output / 100;
        diskon = Math.round(diskon * 100) / 100;
        output = Math.round(output * 100) / 100;
        output = output - diskon - diskonNominal;
        output = Math.round(output * 100) / 100;
        output = addPeriodDescription(output, ',');
        if (output.indexOf(".") == -1) {
            output = output + '.00';
        } else if (output.split('.')[1].length == 1) {
            output = output + '0';
        }
        $("#" + id + "-qty-hitung").html(output);
        hitungTotalDescription()
    }
    function autoChangeTotalDescription() {
        myFunctionduit();
        $("#currencyHeader").change(function () {
            var currencyHeader = $("#currencyHeader").val().split('---;---');
            $("#rate").val(addPeriod(currencyHeader[2], ','));
            var rate = $("#rate").val();
            $(".priceDescription").each(function (i) {
                changeTotalDescription($(this).attr('id'))
            });
            var currency = $("#currencyHeader").val().split('---;---');
            if (currency[3] == '1') {
                $("#rate").prop('readonly', true);
                $("#rate").css('background-color', '#eee')
            } else {
                $("#rate").prop('readonly', false);
                $("#rate").css('background-color', '')
            }
            hitungTotalDescription()
        });
        function addPeriodDescription(nStr, add) {
            nStr += '';
            x = nStr.split(add);
            x1 = x[0];
            x2 = x.length > 1 ? add + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + add + '$2')
            }
            return x1 + x2
        }
        function changeTotalDescription(id) {
            var price = document.getElementById(id).value;
            price = removePeriod(price, ',');
            var diskon = $("#" + id + '-discount').val();
            var diskonNominal = removePeriod($("#" + id + '-discountNominal').val(), ',') * removePeriod($("#" + id + '-qty').val(), ',');
            var output = price * removePeriod($("#" + id + '-qty').val(), ',');
            diskon = diskon * output / 100;
            diskon = Math.round(diskon * 100) / 100;
            output = Math.round(output * 100) / 100;
            output = output - diskon - diskonNominal;
            output = Math.round(output * 100) / 100;
            output = addPeriodDescription(output, ',');
            if (output.indexOf(".") == -1) {
                output = output + '.00';
            } else if (output.split('.')[1].length == 1) {
                output = output + '0';
            }

            $("#" + id + "-qty-hitung").html(output);
            hitungTotalDescription();
        }
        $("#rate").keyup(function () {
            var rate = $("#rate").val();
            $(".priceDescription").each(function (i) {
                changeTotalDescription($(this).attr('id'))
            });
            hitungTotalDescription()
        });
        $(".priceDescription").keyup(function (e) {
            if ($(this).val() == '') {

            } else {

                changeTotalDescription($(this).attr('id'))
            }
            hitungTotalDescription()
        });
        $(".priceDescription").change(function (e) {
            if ($(this).val() == '') {
            }
            changeTotalDescription($(this).attr('id'));
            hitungTotalDescription()
        });
        $(".qtyDescription").keyup(function (e) {
            if ($(this).val() == '') {
            } else {
                changeTotalDescription($(this).attr('id').substring(0, $(this).attr('id').length - 4))
            }
            hitungTotalDescription()
        });
        $(".qtyDescription").change(function (e) {
            if ($(this).val() == '') {
            }
            changeTotalDescription($(this).attr('id').substring(0, $(this).attr('id').length - 4));
            hitungTotalDescription()
        });
        $(".qtyDescription").blur(function (e) {
//            if ($(this).val() == 0) {
//                $(this).val('1')
//            }
            changeTotalDescription($(this).attr('id').substring(0, $(this).attr('id').length - 4));
            hitungTotalDescription()
        });
        $(".discountDescription").keyup(function (e) {
            if ($(this).val() > 100) {
                $(this).val('100')
            }
//            if ($(this).val() < 0) {
//                $(this).val('0')
//            }
            changeTotalDescription($(this).attr('id').substring(0, $(this).attr('id').length - 9));
            hitungTotalDescription()
        });
        $(".discountDescription").change(function (e) {
            if ($(this).val() == '') {
            }
            if ($(this).val() > 100) {
                $(this).val('100')
            }
//            if ($(this).val() < 0) {
//                $(this).val('0')
//            }
            changeTotalDescription($(this).attr('id').substring(0, $(this).attr('id').length - 9));
            hitungTotalDescription()
        });
        $(".discountDescription").blur(function (e) {
            if ($(this).val() == 0) {
                $(this).val('0')
            }
            if ($(this).val() > 100) {
                $(this).val('100')
            }
//            if ($(this).val() < 0) {
//                $(this).val('0')
//            }
            changeTotalDescription($(this).attr('id').substring(0, $(this).attr('id').length - 9));
            hitungTotalDescription()
        });
        $(".discountNominalDescription").keyup(function (e) {
            if ($(this).val() == '') {
            }
            changeTotalDescription($(this).attr('id').substring(0, $(this).attr('id').length - 16));
            hitungTotalDescription()
        });
        $(".discountNominalDescription").change(function (e) {
            if ($(this).val() == '') {
            }
            changeTotalDescription($(this).attr('id').substring(0, $(this).attr('id').length - 16));
            hitungTotalDescription()
        });
        $(".discountNominalDescription").blur(function (e) {
            if ($(this).val() == '') {
            }
            changeTotalDescription($(this).attr('id').substring(0, $(this).attr('id').length - 16));
            hitungTotalDescription()
        });
        hitungTotalDescription()
    }

    function addPeriodDescription(nStr, add) {
        nStr += '';
        x = nStr.split(add);
        x1 = x[0];
        x2 = x.length > 1 ? add + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + add + '$2')
        }
        return x1 + x2
    }
    function hitungTotalDescription() {
//        var total = 0;
//        $(".subtotalDescription").each(function (i) {
//            var jumlah = document.getElementById($(this).attr('id')).innerHTML;
//            total += parseFloat(removePeriod(jumlah, ','))
//        });
//        total = Math.round(total * 100) / 100;
//        return total

        var total = 0;
        $(".subtotalDescription").each(function (i) {
            var jumlah = document.getElementById($(this).attr('id')).innerHTML;
            total += parseFloat(removePeriod(jumlah, ','))
        });
        total = Math.round(total * 100) / 100;
        var diskon = removePeriod($("#discountGlobal").val(), ',');
        document.getElementById("total").innerHTML = addPeriodDescription(total.toFixed(2), ',');
        document.getElementById("grandTotal").innerHTML = (addPeriodDescription((total - diskon).toFixed(2), ','));
        var tamp = total - diskon;
        if (document.getElementById("vat").checked == true) {
            var tax = tamp / 10;
            tax = Math.floor((tax * 100) / 100);
//            tax = Math.round(tax * 100) / 100;
            document.getElementById("tax").innerHTML = addPeriodDescription(tax.toFixed(2), ',');
            var grandTotal = tamp + tax;
            grandTotal = Math.round(grandTotal * 100) / 100;
            document.getElementById("grandTotalAfterTax").innerHTML = addPeriodDescription(grandTotal.toFixed(2), ',');
            $("#grandTotalValue").val(grandTotal)
        } else {
            $("#grandTotalValue").val(tamp);
            document.getElementById("tax").innerHTML = '';
            document.getElementById("grandTotalAfterTax").innerHTML = ''
        }
        return total
    }
    var config = {'.chosen-select': {}};
    for (var selector in config) {
        $(selector).chosen({
            search_contains: true
        });
    }
    $('.appd').find('a.chosen-single').each(function () {
        $(this).addClass('chosenapp');
        var added = $(this).after().addClass('chosenapp');
        added++;
        var end = $('td.appd:last').children().find('select').addClass('chosenapp');
        end++
    });
    $('#date').datepicker();
    $("#date").datepicker("option", "dateFormat", 'dd-mm-yy');
    autoChangeTotalDescription();
    hitungTotalDescription();
    $("#vat").change(function (e) {
        hitungTotalDescription()
    });

    $(".qtyDescription").keyup(function () {
        hitungTotalDescription();
    });
    $(".btn-deleteRow-Description").click(function () {
//            alert('#' + $(this).attr('data'));
        if ($('#' + $(this).attr('data')).length > 0) {
            $(".description option[value='" + $(this).attr('data') + "']").remove();
            $("." + $(this).attr('data')).remove();
            $("." + $(this).attr('text')).remove();
//                $(".description option[value='" + $('#' + $(this).attr('data')).find('td:nth-child(1) input').val() + "']").remove();
            document.getElementById($(this).attr('data')).remove();
        }

        var trBodyLength = $('#table-salesorder-description tbody tr');

        if ($(trBodyLength).length < 1) {
            $('#no-in-description').show();
        }
        hitungTotalDescription();
    });
    $("#btn-addRow-description").click(function () {

        $('#no-in-description').hide();

        var cur = $('#currencyHeader').val().split('---;---');
//        $('#table-quotation-description tr:last').after('<tr id="rowDescription' + baris + '">' +

        $('#table-salesorder-description tbody').append('<tr style="background: #F1FFDB;" id="rowDescription' + baris + '">' +
//                '<td>' +
//                '<input type="text" class="inventoryDescription input-theme" name="inventoryDescription[]" id="inventoryDescription-' + baris + '">' +
//                '</td>' +
                '<td class="chosen-uom">' +
                '<input type="hidden" class="inventoryDescription" style="width: 100px" id="inventoryDescription-' + baris + '" style="" name="inventoryDescription[]">' +
                $('#InventoryDescription-0').val() +
                '</td>' +
                '<td>' +
                '<input type="text" class="uomDescription input-theme" name="uomDescription[]" id="uomDescription-' + baris + '">' +
                '</td>' +
                '<td>-</td>' +
                '<td class="text-right">' +
                '<input type="text" class="maxWidth qtyDescription right input-theme" name="qtyDescription[]" id="priceDescription-' + baris + '-qty">' +
                '</td>' +
                '<td>' +
                '<input type="text" class="maxWidth priceDescription right numajaDesimal input-theme" name="priceDescription[]" maxlength="" value="0.00" id="priceDescription-' +
                baris + '">' + '</td>'
                + '<td class="text-right">' +
                '<input disabled type="text" class="maxWidth discountDescription right input-theme numajaDesimal" name="discountDescription[]" min="0" max="100" id="priceDescription-' + baris + '-discount" value="0.00">' +
                '</td>' +
                '<td class="text-right">' +
                '<input disabled type="text" class="maxWidth discountNominalDescription right numajaDesimal input-theme" name="discountNominalDescription[]" id="priceDescription-' + baris + '-discountNominal" value="0.00">' +
                '</td>' +
                '<td class="right subtotalDescription" id="priceDescription-' + baris + '-qty-hitung">' + '0.00' + '</td>' + '<td>' +
                '<button class="btn btn-pure-xs btn-xs btn-deleteRow-Description margr5" type="button" data="rowDescription' + baris + '" text="rowSpec' + baris + '"><span class="glyphicon glyphicon-trash"></span></button>' +
                '<button class="btn btn-pure-xs btn-xs btn-toggle-detail margr5" type="button" data-toshow="show" data="rowDescription' + baris + '"><span class="glyphicon glyphicon-eye-open"></span></button>' +
                '<!--<button class="btn btn-pure-xs btn-xs btn-toggle-spec" type="button" data-toshow="show" data="rowSpec' + baris + '"><span class="glyphicon glyphicon-comment"></span></button>-->' +
                '</td></tr>' +
                '<tr id="rowSpec' + baris + '" style="display:none;"><td colspan="9" class="rowSpec' + baris + '"><textarea onkeyup="textAreaAdjust(this)" name="spesifikasi[]" style="overflow:hidden; width: 100%;" class="input-theme rowSpec' + baris + '"></textarea></td</tr>');


        $('#inventoryDescription-' + baris).val($('#InventoryDescription-0').val());
        $('#uomDescription-' + baris).val($('#uomDescription-0').val());
        $('#priceDescription-' + baris + '-qty').val($('#priceDescription-0-qty').val());
        $('#priceDescription-' + baris).val($('#priceDescription-0').val());
        $('#priceDescription-' + baris + '-discount').val($('#priceDescription-0-discount').val());
        $('#priceDescription-' + baris + '-discountNominal').val($('#priceDescription-0-discountNominal').val());
        $('#priceDescription-' + baris + '-qty-hitung').text($('#priceDescription-0-qty-hitung').text());


        $(".btn-deleteRow-Description").click(function () {
//            alert('#' + $(this).attr('data'));
            if ($('#' + $(this).attr('data')).length > 0) {
                $(".description option[value='" + $(this).attr('data') + "']").remove();
                $("." + $(this).attr('data')).remove();
                $("." + $(this).attr('text')).remove();
//                $(".description option[value='" + $('#' + $(this).attr('data')).find('td:nth-child(1) input').val() + "']").remove();
                document.getElementById($(this).attr('data')).remove();
            }

            var trBodyLength = $('#table-salesorder-description tbody tr');

            if ($(trBodyLength).length < 1) {
                $('#no-in-description').show();
            }

            hitungTotalDescription();
        });
        $(".numajaDesimal").keypress(function (e) {
            if ((e.charCode >= 48 && e.charCode <= 57) || (e.charCode == 0) || (e.charCode == 46))
                return true;
            else
                return false
        });

        $(".uomDescription").change(function () {
            hitungTotalDescription();
        });

        var a = $('#InventoryDescription-0').val();
        $(".description").append('<option value="rowDescription' + baris + '">' + a + '</option>');

        $(".qtyDescription").keyup(function () {
            hitungTotalDescription();
        });
        
        description.push('~' + baris);
        console.log(description);
        baris++;
        autoChangeTotalDescription()
    });
//    $('body').on('click', '.btn-toggle-detail', function () {
//        if ($(this).attr('data-toshow') == 'show') {
//            $('.' + $(this).attr('data')).hide();
//            $(this).attr('data-toshow', 'hide');
//        } else {
//            $('.' + $(this).attr('data')).show();
//            $(this).attr('data-toshow', 'show');
//        }
//    });
//
//    $('body').on('click', '.btn-toggle-spec', function () {
//        if ($(this).attr('data-toshow') == 'show') {
//            $('.' + $(this).attr('data')).hide();
//            $(this).attr('data-toshow', 'hide');
//        } else {
//            $('.' + $(this).attr('data')).show();
//            $(this).attr('data-toshow', 'show');
//        }
//    });

    window.textAreaAdjust = function (o) {
        o.style.height = "1px";
        o.style.height = (25 + o.scrollHeight) + "px";
    };
});




