var baris = 1;
$(".autoTab").focus(function (e) {
    $(this).val("");
});
$(".autoTab").keyup(function (e) {
    var tamp = $(this).val();
    var length = document.getElementById($(this).attr('id')).maxLength;
    if (tamp.length == length)
    {
        var inputs = $(this).closest('form').find(':input');
        inputs.eq(inputs.index(this) + 1).focus();
    }
    $('#numberTax').val($('#numberTax1').val() + "." + $('#numberTax2').val() + "-" + $('#numberTax3').val() + "." + $('#numberTax4').val());
});
//document ready biasa
$(document).ready(function () {
    var config = {
        '.chosen-select': {}
    };
    for (var selector in config) {
        $(selector).chosen({
            search_contains: true
        });
    }
    $.validate({
        form: '#form-updateShippingAdd',
        onSuccess: function () {
            var zero = 0;
            $(".addShipping").each(function (i) {
                if (removePeriod($(this).val(), ',') > 0) {
                    zero = 1;
                }
            });
            if (zero == 0) {
                alert('Shipping must have at least one inventory with quantity value more than zero.');
                return false;
            } else {
                return true;
            }
        }
    });
    $("#form-updateShippingAdd").submit(function (e) {
        //cek stok bener
        var x = 0;
        $(".addShipping").each(function (i) {
            var angkaa = this.id;
            var angka = angkaa.substring(6);
            var uu = angka.substring(angka.length, angka.length - 4);
            angka = angka.replace(uu, "");
//            alert(angka);
//            alert($("#stockCurrent-" + angka).val());
//        alert(removePeriod($(this).val(), ','));
//            alert($("#stockCurrent-" + angka.substring(6, 7)).val());
            if (parseFloat(removePeriod($(this).val(), ',')) > $("#stockCurrent-" + angka).val()) {
                x = 1;
            }
        });
        if (x == 1) {
            alert("Qty tidak bisa melebihi stok barang yang ada.");
            e.preventDefault();
        }
//        e.preventDefault();
    });

    $(".vehicle").autocomplete({
        source: availableTags
    });
    $(".driver").autocomplete({
        source: availableTags2
    });
    $(".vehicle").autocomplete("option", "appendTo", ".formshipping");
    $(".driver").autocomplete("option", "appendTo", ".formshipping");

    $("#warehouseHeader").change(function (e) {
        $.post(currentStockShipping, {internalID: $("#salesOrderInternalID").val(), warehouse: $(this).val(),shipping: $("#shippingInternalID").val()}).done(function (data) {
            $('.currentStock').each(function (e) {
                var id = $(this).attr("id");
                var idLoop = id.split("-");
                $("#currentStock-" + idLoop[1]).html(data[idLoop[1] - 1]);
                $("#stockCurrent-" + idLoop[1]).val(data[idLoop[1] - 1]);
            });
        });
    });
});
function addPeriodShippingAddUpdate(nStr, add)
{
    nStr += '';
    x = nStr.split(add);
    x1 = x[0];
    x2 = x.length > 1 ? add + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + add + '$2');
    }
    return x1 + x2;
}