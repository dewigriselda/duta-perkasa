$(document).ready(function () {
    var print = $("#print").val()
    if (typeof print !== 'undefined') {
        window.open(print);
    }

//    alert($("#coa6_input").val());
//    alert($("#QuotationInternalID").val());
    $.post(getCustomerManager, {id: $("#coa6_input").val(), update: $("#QuotationInternalID").val()}).done(function (data) {
        $("#customermanagerdiv").html(data);
    });
    $.post(getCustomerCP, {id: $("#coa6_input").val(), update: $("#QuotationInternalID").val()}).done(function (data) {
        $("#customercpdiv").html(data);
    });
    $("#coa6_input").change(function (e) {
//        alert($("#coa6_input").val());
//        alert($("#QuotationInternalID").val());
        $.post(getCustomerManager, {id: $("#coa6_input").val(), update: $("#QuotationInternalID").val()}).done(function (data) {
            $("#customermanagerdiv").html(data);
        });
        $.post(getCustomerCP, {id: $("#coa6_input").val(), update: $("#QuotationInternalID").val()}).done(function (data) {
            $("#customercpdiv").html(data);
        });
    });



    function autoChangeTotal() {
        myFunctionduit();
        function addPeriod(nStr, add) {
            nStr += '';
            x = nStr.split(add);
            x1 = x[0];
            x2 = x.length > 1 ? add + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + add + '$2')
            }
            return x1 + x2
        }
        $("#currencyHeader").change(function () {
            var currencyHeader = $("#currencyHeader").val().split('---;---');
            $("#rate").val(addPeriod(currencyHeader[2], ','));
            var rate = $("#rate").val();
            $(".price").each(function (i) {
                changeTotal($(this).attr('id'))
            });
            var currency = $("#currencyHeader").val().split('---;---');
            if (currency[3] == '1') {
                $("#rate").prop('readonly', true);
                $("#rate").css('background-color', '#eee')
            } else {
                $("#rate").prop('readonly', false);
                $("#rate").css('background-color', '')
            }
            hitungTotal()
        });
        function changeTotal(id) {
            var price = document.getElementById(id).value;
            price = removePeriod(price, ',');
            var diskon = $("#" + id + '-discount').val();
            var diskonNominal = removePeriod($("#" + id + '-discountNominal').val(), ',') * removePeriod($("#" + id + '-qty').val(), ',');
            var output = price * removePeriod($("#" + id + '-qty').val(), ',');
            diskon = diskon * output / 100;
            diskon = Math.round(diskon * 100) / 100;
            output = Math.round(output * 100) / 100;
            output = output - diskon - diskonNominal;
            output = Math.round(output * 100) / 100;
            output = addPeriod(Math.ceil(output), ',');
            if (output.indexOf(".") == -1) {
                output = output + '.00';
            } else if (output.split('.')[1].length == 1) {
                output = output + '0';
            }
            document.getElementById(id + '-qty-hitung').innerHTML = output;
            hitungTotal()
        }
        $("#rate").keyup(function () {
            var rate = $("#rate").val();
            $(".price").each(function (i) {
                changeTotal($(this).attr('id'))
            });
            hitungTotal();
            hitungTotalDeskripsi($(this).parent().parent().attr("data-parent"));
        });
//        $(".price").keyup(function (e) {
////            if ($(this).val() == '') {
////            } else {
//            changeTotal($(this).attr('id'))
////            }
//            hitungTotal();
//            hitungTotalDeskripsi($(this).parent().parent().attr("data-parent"));
//        });
        $(".price").change(function (e) {
            if ($(this).val() == '') {
            }
            var id = $(this).attr('id');
            var price = document.getElementById(id).value;
            if (id == "price-0") {
                $("#oriprice-0").text(price);
            }
            price = removePeriod(price, ',');
            console.log(price);
            if (document.getElementById("vat").checked == true) {
//                console.log("masuk");
                if ($("#typeTax").val() == "Include") {
//                    console.log("masuk1");
                    var priceTax = price / 1.1 * 0.1;
                    price = price - priceTax;
                    document.getElementById(id).value = price.toFixed(2);
                } else {
                    $("#price-0").val($("#oriprice-0").text());
                }
            }

            changeTotal($(this).attr('id'));
            hitungTotal();
            if (id != "price-0") {
                hitungTotalDeskripsi($(this).parent().parent().attr("data-parent"));
            }
        });
//        $(".qty").keyup(function (e) {
////            if ($(this).val() == '') {
////            } else {
//            changeTotal($(this).attr('id').substring(0, $(this).attr('id').length - 4))
////            }
//            hitungTotal();
//            hitungTotalDeskripsi($(this).parent().parent().attr("data-parent"));
//        });
        $(".qty").change(function (e) {
            if ($(this).val() == '') {
            }
            changeTotal($(this).attr('id').substring(0, $(this).attr('id').length - 4));
            hitungTotal();
            hitungTotalDeskripsi($(this).parent().parent().attr("data-parent"));
        });
//        $(".qty").blur(function (e) {
////            if ($(this).val() == 0) {
////                $(this).val('1')
////            }
//            changeTotal($(this).attr('id').substring(0, $(this).attr('id').length - 4));
//            hitungTotal();
//        });
        $(".qtyDescription").change(function (e) {
//            console.log($(this).parent().parent().attr("data-parent"));
//            console.log($(this).parent().attr("data-parent"));
//            console.log($(this).attr("data-parent"));
//            console.log($(this).attr("id"));
//            console.log($(this).parent().parent().attr("id"));
            hitungTotalDeskripsi($(this).parent().parent().attr("id"));
        });
        $(".qtyDescription").change(function (e) {
            hitungTotalDeskripsi($(this).parent().parent().attr("id"));
        });
//        $(".discount").keyup(function (e) {
//            if ($(this).val() > 100) {
//                $(this).val('100')
//            }
////            if ($(this).val() < 0) {
////                $(this).val('0')
////            }
//            changeTotal($(this).attr('id').substring(0, $(this).attr('id').length - 9));
//            hitungTotal();
//            hitungTotalDeskripsi($(this).parent().parent().attr("data-parent"));
//        });
        $(".discount").change(function (e) {
            if ($(this).val() == '') {
            }
            if ($(this).val() > 100) {
                $(this).val('100')
            }


            changeTotal($(this).attr('id').substring(0, $(this).attr('id').length - 9));
            hitungTotal();
            hitungTotalDeskripsi($(this).parent().parent().attr("data-parent"));
        });
//        $(".discount").blur(function (e) {
////            if ($(this).val() == 0) {
////                $(this).val('0')
////            }
//            if ($(this).val() > 100) {
//                $(this).val('100')
//            }
////            if ($(this).val() < 0) {
////                $(this).val('0')
////            }
//            changeTotal($(this).attr('id').substring(0, $(this).attr('id').length - 9));
//            hitungTotal();
////            hitungTotalDeskripsi($(this).parent().parent().attr("data-parent"));
//        });
//        $(".discountNominal").keyup(function (e) {
//            if ($(this).val() == '') {
//            }
//            changeTotal($(this).attr('id').substring(0, $(this).attr('id').length - 16));
//            hitungTotal();
//            hitungTotalDeskripsi($(this).parent().parent().attr("data-parent"));
//        });
        $(".discountNominal").change(function (e) {
            if ($(this).val() == '') {
            }
            var diskonNominal = removePeriod($(this).val(), ',');
            console.log("diskon" + diskonNominal);
            if (document.getElementById("vat").checked == true) {
                console.log("masuk Nominal");
//                if ($("#typeTax").val() == "Include") {
//                    console.log("masuk1 Nominal");
//                    var dNominalTax = diskonNominal / 1.1 * 0.1;
//                    diskonNominal = diskonNominal - dNominalTax;
//                    $(this).val(diskonNominal.toFixed(2));
//                }
            }

            changeTotal($(this).attr('id').substring(0, $(this).attr('id').length - 16));
            hitungTotal();
            hitungTotalDeskripsi($(this).parent().parent().attr("data-parent"));
        });
//        $(".discountNominal").blur(function (e) {
//            if ($(this).val() == '') {
//            }
//            changeTotal($(this).attr('id').substring(0, $(this).attr('id').length - 16));
//            hitungTotal();
//        });
        $("#discountGlobal").keyup(function (e) {
            if (parseFloat(removePeriod($("#discountGlobal").val(), ',')) > parseFloat(removePeriod($("#total").text(), ','))) {
                alert("Diskon tidak boleh melebihi total.");
                $("#discountGlobal").val(parseFloat(removePeriod($("#total").text(), ',')));
            } else {
                var dis = Math.floor(removePeriod($(this).val(), ','));
                $("#discountGlobal").val(dis);
                $(".price").each(function (i) {
                    changeTotal($(this).attr('id'))
                });
                hitungTotal()
            }
        });
        $("#discountGlobal").change(function (e) {
            $(".price").each(function (i) {
                changeTotal($(this).attr('id'))
            });
            hitungTotal();
        });
        $("#discountGlobal").blur(function (e) {
            $(".price").each(function (i) {
                changeTotal($(this).attr('id'))
            });
            hitungTotal();
        });
        hitungTotal();
    }
    function hitungTotal() {
        var total = 0;
        $(".subtotal").each(function (i) {
            var jumlah = document.getElementById($(this).attr('id')).innerHTML;
            total += parseFloat(removePeriod(jumlah, ','))
        });
        total = Math.round(total * 100) / 100;
        var diskon = removePeriod($("#discountGlobal").val(), ',');
        document.getElementById("total").innerHTML = addPeriod(total.toFixed(2), ',');
        document.getElementById("grandTotal").innerHTML = (addPeriod((total - diskon).toFixed(2), ','));
        var tamp = total - diskon;
        if (document.getElementById("vat").checked == true) {
            var tax = tamp / 10;
            tax = Math.floor((tax * 100) / 100);
//            tax = Math.round(tax * 100) / 100;
            document.getElementById("tax").innerHTML = addPeriod(Math.floor(tax).toFixed(2), ',');
            var grandTotal = tamp + tax;
            grandTotal = Math.round(grandTotal * 100) / 100;
            document.getElementById("grandTotalAfterTax").innerHTML = addPeriod(grandTotal.toFixed(2), ',');
            $("#grandTotalValue").val(grandTotal)
        } else {
            $("#grandTotalValue").val(tamp);
            document.getElementById("tax").innerHTML = '';
            document.getElementById("grandTotalAfterTax").innerHTML = ''
        }
        return total
    }
    function hitungTotalDeskripsi(row) {
//        alert(row);
        var rowDeskripsi = row.replace('rowDescription', '');
//        alert('[data-parent="rowDescription' + rowDeskripsi + '"]');
        var total = 0;
        $('[data-parent="rowDescription' + rowDeskripsi + '"]').find('td.subtotal').each(function (i) {
//            var jumlah = document.getElementById($(this).attr('id')).innerHTML;
//            total += parseFloat(removePeriod(jumlah, ','))
//            alert(total);

            var angka = $(this).attr('id').replace("price-", "");
            angka = angka.replace("-qty-hitung", "");

            var jumlah = $("#price-" + angka).val();
            jumlah = parseFloat(removePeriod(jumlah, ','));

            var qty = $("#price-" + angka + "-qty").val();
            qty = parseFloat(removePeriod(qty, ','));

            total += jumlah * qty;
        });
        $("#priceDescription-" + rowDeskripsi + "-qty-hitung").text(addPeriod(Math.ceil(total).toFixed(2), ','));
        var qty = parseFloat(removePeriod($('#priceDescription-' + rowDeskripsi + '-qty').val(), ","));
        var price = total / qty;
        $('#priceDescription-' + rowDeskripsi).val(addPeriod(price.toFixed(2), ','));
    }
    function addPeriod(nStr, add) {
        nStr += '';
        x = nStr.split(add);
        x1 = x[0];
        x2 = x.length > 1 ? add + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + add + '$2')
        }
        return x1 + x2
    }
    var config = {'.chosen-select': {}};
    for (var selector in config) {
        $(selector).chosen({
            search_contains: true
        });
    }
    $('.appd').find('a.chosen-single').each(function () {
        $(this).addClass('chosenapp');
        var added = $(this).after().addClass('chosenapp');
        added++;
        var end = $('td.appd:last').children().find('select').addClass('chosenapp');
        end++
    });
    $('#date').datepicker();
    $("#date").datepicker("option", "dateFormat", 'dd-mm-yy');
    $("#date").val(copydate);
    autoChangeTotal();
    hitungTotal();
    $("#vat").change(function (e) {
        hitungTotal()
    });
    $(".btn-deleteRow").click(function () {
        if ($('#' + $(this).attr('data')).length > 0) {
            document.getElementById($(this).attr('data')).remove()
        }
        hitungTotal();
        hitungTotalDeskripsi($(this).parent().parent().attr("data-parent"));
    });
    function changeTotal(id) {
        var price = document.getElementById(id).value;
        price = removePeriod(price, ',');
        var diskon = $("#" + id + '-discount').val();
        var diskonNominal = removePeriod($("#" + id + '-discountNominal').val(), ',') * removePeriod($("#" + id + '-qty').val(), ',');
        var output = price * removePeriod($("#" + id + '-qty').val(), ',');
        diskon = diskon * output / 100;
        diskon = Math.round(diskon * 100) / 100;
        output = Math.round(output * 100) / 100;
        output = output - diskon - diskonNominal;
        output = Math.round(output * 100) / 100;
        output = addPeriod(Math.ceil(output), ',');
        if (output.indexOf(".") == -1) {
            output = output + '.00';
        } else if (output.split('.')[1].length == 1) {
            output = output + '0';
        }
        document.getElementById(id + '-qty-hitung').innerHTML = output;
        hitungTotal()
    }

    $(".price").each(function (i) {
        changeTotal($(this).attr('id'))
    });

    $(".stock").each(function (i) {
        var id = $(this).attr('id').split('-');
        $("#price-" + id[1] + "-stock").popover({trigger: 'hover'});
    });

    var dataUom;
    //get uom
    $.post(getUomThisInventory, {id: $('#inventory-0').val()}).done(function (data2) {
        $("#uom-0").html(data2);
        dataUom = data2;
    });

    $(".inventory").change(function () {
        var id = $(this).attr("id").split("-");
        var tampVar = $(this).find('option:selected').attr('id');
        if (tampVar.indexOf("inventory") >= 0) {
            //get uom
            $.post(getUomThisInventory, {id: $('#inventory-' + id[1]).val()}).done(function (data2) {
                $("#uom-" + id[1]).html(data2);
                //get stock
                $.post(getStockInventorySO, {InventoryInternalID: $("#inventory-" + id[1]).val()}).done(function (stock) {
                    var split = stock.split("---;---");
                    $("#price-" + id[1] + "-stock").html(split[0]);
                    document.getElementById("price-" + id[1] + "-stock").setAttribute("data-content", "Inventory stock remains : " + split[0] + " <br/>");
                    document.getElementById("price-" + id[1] + "-stock").title = "Information Inventory";
                    $("#price-" + id[1] + "-stock").popover({trigger: 'hover'});
                });
            });
        } else if (tampVar.indexOf("parcel") >= 0) {
            //found parcel
            $("#price-" + id[1] + "-stock").html("-");
            $("#uom-" + id[1]).attr("readonly", true);
            $("#uom-" + id[1]).html("<option value='0'>-</option>");
            $("#uom-" + id[1]).trigger("chosen:updated");
            var value = $(this).val().split("---;---");
            //get price parcel
            $.post(getPriceThisParcel, {id: value[0]}).done(function (data) {
                $("#price-" + id[1]).val(addPeriod(data.trim(), ","));
                changeTotal($("#price-" + id[1]).attr("id"));
            });
        }

    });

    $(".uom").change(function () {
        var id = $(this).attr("id").split("-");
        hitungTotal();
    });

    $(".qty").keyup(function () {
        var id = $(this).attr("id").split("-");
        hitungTotal();
    });

    arrCounter = $.map(JSON.parse(arrCount), function (value, index) {
        return [value];
    });
    arrInv = $.map(JSON.parse(arrInven), function (value, index) {
        return [value];
    });
    $("#btn-addRow").click(function () {
        var id = "rowDescription" + $('#description-0').val().replace('rowDescription', '');
        if (typeof arrCounter[id] === 'undefined') {
            arrCounter[id] = 0;
        }
        if (typeof arrInv[id] === 'undefined') {
            arrInv[id] = [];
        }
        if (arrInv[id].includes($("#inventory-0").val())) {
            alert("Barang ini sudah ada");
        } else {
            var cur = $('#currencyHeader').val().split('---;---');
            var numberPattern = /\d+/g;
            var valueSelectDesc = $('#description-0').val().match(numberPattern)[0];
            valueSelectDesc = description.indexOf('~' + valueSelectDesc) + 1;
//            console.log(valueSelectDesc);

            $('#rowSpec' + $('#description-0').val().replace('rowDescription', '')).after('<tr data-parent="rowDescription' + $('#description-0').val().replace('rowDescription', '') + '" id="row' + baris + '" class="rowDescription' + valueSelectDesc + '">' +
                    '<td class="chosen-uom">' +
                    '<input type="hidden" class="inventory" style="width: 100px" id="inventory-' + baris + '" style="" name="inventory' + valueSelectDesc + '[]" value="' + $('#inventory-0').val() + '">' +
                    $("#inventory-0 option[value='" + $("#inventory-0").val() + "']").text() +
                    '</td>' +
                    '<td>' +
                    '<select id="uom-' + baris + '" name="uom' + valueSelectDesc + '[]" class="input-theme uom">' + $('#uom-0').html() + '</select>' +
                    '</td>' +
                    '<td class="text-right">' +
                    $('#price-0-stock').text() +
                    '</td>' +
//                '<td>' +
//                '<select id="description-' + baris + '" name="description[]" class="input-theme description">' + $('#description-0').html() + '</select>' +
//                '</td>' +
                    '<td class="text-right">' +
                    '<input type="text" class="maxWidth qty right input-theme" name="qty' + valueSelectDesc + '[]" maxlength="11" min="1" value="1" id="price-' + baris + '-qty">' +
                    '</td>' +
                    '<td class="text-right">' +
                    '<input type="text" class="maxWidth price right numajaDesimal input-theme" name="price' + valueSelectDesc + '[]" maxlength="" value="0.00" id="price-' +
                    baris + '">' + '</td>'
                    + '<td class="text-right">' +
                    '<input type="text" class="maxWidth discount right input-theme numajaDesimal" name="discount' + valueSelectDesc + '[]" min="0" max="100" id="price-' + baris + '-discount" value="0.00">' +
                    '</td>' +
                    '<td class="text-right">' +
                    '<input type="text" class="maxWidth discountNominal right numajaDesimal input-theme" name="discountNominal' + valueSelectDesc + '[]" id="price-' + baris + '-discountNominal" value="0.00">' +
                    '</td>' +
                    '<td class="right subtotal" id="price-' + baris + '-qty-hitung">' + '0.00' + '</td>' + '<td>' +
                    '<button class="btn btn-pure-xs btn-xs btn-deleteRow" type="button" data="row' + baris + '" barang="inventory-' + baris + '"><span class="glyphicon glyphicon-trash"></span></button>' + '</td>' +
                    '</tr>');
            $('#uom-' + baris).val($('#uom-0').val());
            //$('#description-' + baris).val($('#description-0').val());
            $('#price-' + baris + '-qty').val($('#price-0-qty').val());
            $('#price-' + baris).val($('#price-0').val());
            $('#price-' + baris + '-discount').val($('#price-0-discount').val());
            $('#price-' + baris + '-discountNominal').val($('#price-0-discountNominal').val());
            $('#price-' + baris + '-qty-hitung').text($('#price-0-qty-hitung').text());
            hitungTotalDeskripsi($('#description-0').val());
            $(".btn-deleteRow").click(function () {
                arrInv[id].splice($.inArray($('#' + $(this).attr('barang')).val(), arrInv[id]), 1);
                if ($('#' + $(this).attr('data')).length > 0) {
                    document.getElementById($(this).attr('data')).remove();
                }
                hitungTotal();
                hitungTotalDeskripsi($(this).parent().parent().attr("data-parent"));
            });
            $(".numajaDesimal").keypress(function (e) {
                if ((e.charCode >= 48 && e.charCode <= 57) || (e.charCode == 0) || (e.charCode == 46))
                    return true;
                else
                    return false;
            });


            $(".qty").keyup(function () {
                hitungTotal();
            });
            var config = {'.chosen-select': {}};
            for (var selector in config) {
                $(selector).chosen({
                    search_contains: true
                });
            }
            $('.appd').find('a.chosen-single').each(function () {
                $(this).addClass('chosenapp');
                var added = $(this).after().addClass('chosenapp');
                added++;
                var end = $('td.appd:last').children().find('select').addClass('chosenapp');
                end++
            });
            $("#searchInventory").val("");
            $("#searchInventory").attr("tabindex", -1).focus();
            baris++;
            autoChangeTotal();
            arrInv[id][arrCounter[id]] = $("#inventory-0").val();
        }
        arrCounter[id]++;
    });

    $("#rate").val(addPeriod($("#rate").val(), ','));
    $("#paymentCash,#paymentCredit").change(function () {
        if ($('#paymentCash').attr('checked') == 'checked') {
            $('#paymentCash').removeAttr('checked');
            $('#paymentCredit').attr('checked', 'checked');
            $('#longTerm').prop('disabled', false);
            $('#slip').prop('disabled', true);
            $('#slip').trigger("chosen:updated")
        } else {
            $('#paymentCredit').removeAttr('checked');
            $('#paymentCash').attr('checked', 'checked');
            $('#longTerm').val(0);
            $('#longTerm').prop('disabled', true);
            $('#slip').prop('disabled', false);
            $('#slip').trigger("chosen:updated")
        }
    });
    $("#longTerm").blur(function () {
        if ($('#longTerm').val() == '' || $('#longTerm').val() < 0) {
            $('#longTerm').val(0)
        }
    });
    var currency = $("#currencyHeader").val().split('---;---');
    if (currency[3] == '1') {
        $("#rate").prop('readonly', true);
        $("#rate").css('background-color', '#eee')
    } else {
        $("#rate").prop('readonly', false);
        $("#rate").css('background-color', '')
    }
});
function checkTax() {
    if (document.getElementById("vat").checked == true) {
        $("#type").removeAttr("style");
    } else {
        $("#type").attr("style", "Display:none");
    }
}

//divcustomer = customer lama
//divcustomer2 = customer baru
function checkCustomer() {
    if ($('#checkcuslama').is(':checked')) {
        $("#divcustomer").removeAttr("style");
        $("#divcustomer2").attr("style", "Display:none");
    } else {
        $("#divcustomer").attr("style", "Display:none");
        $("#divcustomer2").removeAttr("style");
    }
}

$("#typeTax").change(function () {
    if ($("#typeTax").val() == "Include") {
        //price
        var p = removePeriod($("#price-0").val(), ",");
        p = p / 1.1;
        $("#price-0").val(addPeriod(p.toFixed(2), ","));
        changeTotal("price-0");
    } else {
        $("#price-0").val($("#oriprice-0").text());
        changeTotal("price-0");
    }
});

$.validate();
$(function () {
    $('#vat').change(function () {
        $('.hidevat').toggle(this.checked);
        checkTax();
    }).change();
    $('#checkcuslama').change(function () {
        checkCustomer();
    }).change();
    $('#checkcusbaru').change(function () {
        checkCustomer();
    }).change();
});