$(document).ready(function () {

    //uom inventory
    $(".inventory").change(function () {
        var split = $(this).attr('id').split('-');
        $.post(getUomThisInventory, {id: $(this).val()}).done(function (data) {
            $("#uom-" + split[1]).html(data);
        });
    });

    var config = {'.chosen-select': {}};
    for (var selector in config) {
        $(selector).chosen({
            search_contains: true
        });
    }
    $('.appd').find('a.chosen-single').each(function () {
        $(this).addClass('chosenapp');
        var added = $(this).after().addClass('chosenapp');
        added++;
        var end = $('td.appd:last').children().find('select').addClass('chosenapp');
        end++
    });
    $('#date').datepicker();
    $("#date").datepicker("option", "dateFormat", 'dd-mm-yy');
    $('#date').val(tanggalHariIni)
    $.validate();
    $("#date").change(function () {
        var cariText = '';
        $('#transferID').load(a, {"date": $("#date").val()})
    });

    $("#warehouseHeaderSource").change(function () {
        $('.currentStockSource').each(function (e) {
            var id = $(this).attr("id");
            var idLoop = id.split("-");
            $.post(getStockInventorySource, {InventoryInternalID: $("#inventory-" + idLoop[1]).val(), warehouse: $("#warehouseHeaderSource").val()}).done(function (stock) {
                var split = stock.split("---;---");
                var stokk = split[0].split(" ");
                var stoknih = parseInt(stokk[0]);
                $("currentStock-" + idLoop[1]).val(stoknih);
                $("#price-" + idLoop[1] + "-stocksource").html(split[0] + '<input type="hidden" id="stocks-' + idLoop[1] + '" name="curstocksource[]" value="' + stoknih + '">');
            });
        });
    });
    $("#warehouseHeaderDestiny").change(function () {
        $('.currentStockDestiny').each(function (e) {
            var id = $(this).attr("id");
            var idLoop = id.split("-");
            $.post(getStockInventoryDestiny, {InventoryInternalID: $("#inventory-" + idLoop[1]).val(), warehouse: $("#warehouseHeaderDestiny").val()}).done(function (stock) {
                var split = stock.split("---;---");
                var stokk = split[0].split(" ");
                var stoknih = parseInt(stokk[0]);
                $("#price-" + idLoop[1] + "-stockdestiny").html(split[0] + '<input type="hidden" id="stockd-' + idLoop[1] + '" name="curstockdestiny[]" value="' + stoknih + '">');
            });
        });
    });

    var dataUom;
//get uom
    $.post(getUomThisInventory, {id: $('#inventory-0').val()}).done(function (data2) {
        $("#uom-0").html(data2);
        dataUom = data2;
    });
    $(".btn-deleteRow").click(function () {
        if ($('#' + $(this).attr('data')).length > 0) {
            document.getElementById($(this).attr('data')).remove()
        }
//            hitungTotal()
    });
    $("#btn-addRow").click(function () {
        if (parseFloat(removePeriod($('#price-0-qty').val(), ',')) > $("#stocks-0").val()) {
            alert("Qty tidak boleh melebihi stok");
        } else {
            $('#table-transfer tr:last').after('<tr id="row' + baris + '">' +
                    '<td class="chosen-uom">' +
                    '<input type="hidden" class="inventory" style="width: 100px" id="inventory-' + baris + '" style="" name="inventory[]" value="' + $('#inventory-0').val() + '">' +
                    $("#inventory-0 option[value='" + $("#inventory-0").val() + "']").text() +
                    '</td>' +
                    '<td>' +
                    '<select id="uom-' + baris + '" name="uom[]" class="input-theme uom">' + $('#uom-0').html() + '</select>' +
                    '</td>' +
                    '<td id="price-' + baris + '-stocksource" class="currentStockSource">' +
                    '<input type="hidden" id="stocks-' + baris + '" name="curstocksource[]" value="">' +
                    $('#price-0-stocksource').text() +
                    '</td>' +
                    '<td id="price-' + baris + '-stockdestiny" class="currentStockDestiny">' +
                    '<input type="hidden" id="stockd-' + baris + '" name="curstockdestiny[]" value="">' +
                    $('#price-0-stockdestiny').text() +
                    '</td>' +
                    '<td class="text-right" width="10%">' + '<input type="number" class="maxWidth qty right" name="qty[]" maxlength="11" min="1" value="1" id="price-' + baris + '-qty">' + '</td>' +
                    '<td width="5%">' + '<button class="btn btn-pure-xs btn-xs btn-deleteRow" type="button" data="row' + baris + '"><span class="glyphicon glyphicon-trash"></span></button>' + '</td>' +
                    '</tr>');
            var stokk1 = $('#price-0-stocksource').text().split(" ");
            var stoknih1 = parseInt(stokk1[0]);
            $('#stocks-' + baris).val(stoknih1);
            var stokk2 = $('#price-0-stockdestiny').text().split(" ");
            var stoknih2 = parseInt(stokk2[0]);
            $('#stockd-' + baris).val(stoknih2);
            $('#uom-' + baris).val($('#uom-0').val());
//        if ($('#price-0-qty').val() <= stoknih1) {
            $('#price-' + baris + '-qty').val($('#price-0-qty').val());
//        } else {
//            $('#price-' + baris + '-qty').val(stoknih1);
//            alert("Qty tidak boleh melebihi stok yang ada");
//        }
            $(".btn-deleteRow").click(function () {
                if ($('#' + $(this).attr('data')).length > 0) {
                    document.getElementById($(this).attr('data')).remove()
                }
//            hitungTotal()
            });
            $(".numajaDesimal").keypress(function (e) {
                if ((e.charCode >= 48 && e.charCode <= 57) || (e.charCode == 0) || (e.charCode == 46))
                    return true;
                else
                    return false
            });
//        //uom inventory
//        $(".inventory").change(function () {
//            var split = $(this).attr('id').split('-');
//            $.post(getUomThisInventory, {id: $(this).val()}).done(function (data) {
//                $("#uom-" + split[1]).html(data);
//            });
//        });
            var config = {'.chosen-select': {}};
            for (var selector in config) {
                $(selector).chosen({
                    search_contains: true
                });
            }
            $('.appd').find('a.chosen-single').each(function () {
                $(this).addClass('chosenapp');
                var added = $(this).after().addClass('chosenapp');
                added++;
                var end = $('td.appd:last').children().find('select').addClass('chosenapp');
                end++
            });
            $("#searchInventory").val("");
            $("#searchInventory").attr("tabindex", -1).focus();
            baris++;

            $(".qty").change(function (e) {
//            var id = $(this).attr('id');
//            id = id.replace('price-', '');
//            id = id.replace('-qty', '');
//            if ($('#price-' + id + '-qty').val() <= $('#stocks-' + id).val()) {
////            $('#price-' + id + '-qty').val($('#price-0-qty').val());
//            } else {
//                $('#price-' + id + '-qty').val($('#stocks-' + id).val());
//                alert("Qty tidak boleh melebihi stok yang ada");
//            }
            });
        }
    });


});