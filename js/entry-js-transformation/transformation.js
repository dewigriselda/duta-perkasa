var config = {
    '.chosen-select': {}
};
for (var selector in config) {
    $(selector).chosen({
        search_contains: true
    });
}
$(document).ready(function () {
    $("#searchSalesOrder").keydown(function (event) {
        if (event.keyCode == 13) { //enter
            event.preventDefault();
            $.post(getResultSearchSOTransfer, {id: $("#searchSalesOrder").val()}).done(function (data) {
                $("#selectSalesOrder").html(data);
            });
        }
    });
    $("#searchInventory").keydown(function (event) {
        if (event.keyCode == 13) { //enter
            event.preventDefault();
            $.post(getSearchResultInventoryTransformation, {id: $("#searchInventory").val()}).done(function (data) {
                $("#selectInventory").html(data);
            });
        }
    });
    $("#searchInventory2").keydown(function (event) {
        if (event.keyCode == 13) { //enter
            event.preventDefault();
            $.post(getSearchResultInventoryTransformation2, {id: $("#searchInventory2").val()}).done(function (data) {
                $("#selectInventory2").html(data);
            });
        }
    });

//    $(".btn-delete").click(function () {
//        $('#idDelete').val($(this).data('internal'));
//    });

    $('#startDateReport').datepicker();
    $('#endDateReport').datepicker();
    $("#startDateReport").datepicker("option", "dateFormat", 'dd-mm-yy');
    $("#endDateReport").datepicker("option", "dateFormat", 'dd-mm-yy');
    $('#endDateReport, #startDateReport').change(function () {
        if ($('#startDateReport').val() == '') {
            $('#startDateReport').val($('#endDateReport').val())
        } else if ($('#endDateReport').val() == '') {
            $('#endDateReport').val($('#startDateReport').val())
        } else if (dateCheckHigher($('#startDateReport').val(), $('#endDateReport').val()) == 'start') {
            $('#endDateReport').val($('#startDateReport').val())
        }
    });

    $("#btn-rDetail").click(function () {
        $('#jenisReport').val('detailReport');
        document.getElementById('titleReport').innerHTML = 'Detail Report'
    });
    $("#btn-report-transaction").click(function () {
        if ($('#startDateReport').val() == '' && $('#endDateReport').val() == '') {
            var tanggal = new Date();
            var tanggalText = tanggal.getDate() + '-' + (tanggal.getMonth() + 1) + '-' + tanggal.getFullYear();
            $('#startDateReport').val(tanggalText);
            $('#endDateReport').val($('#startDateReport').val())
        } else if ($('#startDateReport').val() == '') {
            $('#startDateReport').val($('#endDateReport').val())
        } else if ($('#endDateReport').val() == '') {
            $('#endDateReport').val($('#startDateReport').val())
        } else if (dateCheckHigher($('#startDateReport').val(), $('#endDateReport').val()) == 'start') {
            $('#endDateReport').val($('#startDateReport').val())
        }
    });

    window.deleteAttach = function (element) {
        $('#idDelete').val($(element).data('internal'));
        $('#deleteName').text($(element).data('name'));
    };

    $('#example').dataTable({
        "draw": 10,
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": transformationDataBackup,
        },
        "order": [[1, "desc"]],
        columnDefs: [{
                targets: [0],
                orderData: [0, 1]
            }, {
                targets: [1],
                orderData: [1, 0]
            }, {
                targets: [3],
                orderData: [3, 0]
            }]
    });
});