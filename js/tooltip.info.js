(function ($) {
    $(document).ready(function () {

        var spanID = $('#purchaseIDText').text().trim();
        var spanIDFirst = spanID.split(' ')[0];

        $('#purchaseIDText').text(spanIDFirst + '...');

        window.getSalesIDText = function () {
            return spanID;
        };

        $('#btn-info-purchaseID').attr('data-content', window.getSalesIDText());
        $('#btn-info-purchaseID').popover();

    });
})(jQuery);